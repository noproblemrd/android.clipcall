package it.clipcall.common.environment;


import com.google.android.gms.wallet.WalletConstants;

public class Environment {
    public final static String BaseUrl = "http://10.0.0.96/MobileAPI/api/";
    //public final static String BaseUrl = "http://marketplaceqa.noproblemppc.com/mobileapiqa/api/";

    public final static String StripeRedirectUrl = "https://mobileapi.clipcall.it/stripe/Index";

    public final static String StripeClientId = "ca_7cwmQtgVqDU8XUlgG6nRYHxjavisAVK8";

    public final static String StripPublishedKey = "pk_test_RSfJJASb8Ex0iFcovO7MR2DS";
    //public final static String StripPublishedKey = "pk_live_6MFhsuj7F0VZLHUyi0QqFYkt";

    public final static int WalletEnvironment = WalletConstants.ENVIRONMENT_TEST;

    public final static boolean IsDebug = true;

    public final static boolean AppSeeEnabled = false;

    public final static boolean AnalyticsEnabled = false;
}
