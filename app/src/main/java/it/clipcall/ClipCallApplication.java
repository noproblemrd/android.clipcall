package it.clipcall;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.EApplication;

import javax.inject.Inject;

import it.clipcall.infrastructure.bootstrappingExtensions.IBootstrappingExtension;
import it.clipcall.infrastructure.di.components.ApplicationComponent;
import it.clipcall.infrastructure.di.components.DaggerApplicationComponent;
import it.clipcall.infrastructure.di.modules.ApplicationModule;
import it.clipcall.infrastructure.ui.dialogs.SnackbarService;

@EApplication
public class ClipCallApplication extends Application{

    private ApplicationComponent compositionRootComponent;
    private Activity currentActivity;

    @Inject
    IBootstrappingExtension bootstrappingExtension;


    @Override
    public void onCreate() {
        super.onCreate();
        compositionRootComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        compositionRootComponent.inject(this);
        bootstrappingExtension.initialize();
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public ApplicationComponent getComponent() {
        return compositionRootComponent;
    }


    public void showMessage(String message, final Runnable action, boolean autoHide){
        if(currentActivity == null)
            return;



        View decorView = currentActivity.getWindow().getDecorView();
        if(decorView == null)
            return;

        View view = decorView.findViewById(R.id.root);
        //View view = decorView.getRootView();
        if(view == null)
            return;



        final View.OnClickListener listener = new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                action.run();
            }
        };

        int length;
        if(autoHide){
            length = Snackbar.LENGTH_LONG;
        }else{
            length = Snackbar.LENGTH_INDEFINITE;
        }

        SnackbarService snackbarService = new SnackbarService(currentActivity);
        snackbarService.show(view,message,length,listener);
    }


    public boolean isInForeground() {
        return  currentActivity != null;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
    }

    public Activity getCurrentActivity(){
        return this.currentActivity;
    }
}