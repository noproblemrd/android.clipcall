package it.clipcall.consumer.payment.activities;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;

import com.kennyc.view.MultiStateView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.payment.models.PaymentProfessional;
import it.clipcall.consumer.payment.models.PaymentProject;
import it.clipcall.consumer.payment.presenters.ConsumerPayYourProPresenter;
import it.clipcall.consumer.payment.views.IConsumerPayYourProView;
import it.clipcall.consumer.payment.widgets.ProfessionalToPayItemView_;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.recyclerviews.EmptyItemSelectedListener;
import it.clipcall.infrastructure.viewModel.support.recycleviews.ItemSelectedListener;
import it.clipcall.infrastructure.viewModel.support.recycleviews.RecyclerViewAdapterBase;
import jp.wasabeef.recyclerview.animators.BaseItemAnimator;

@EActivity(R.layout.consumer_pay_your_pro_activity)
public class ConsumerPayYourProActivity extends BaseActivity implements IHasToolbar, IConsumerPayYourProView {
    @ViewById
    Toolbar toolbar;


    private final ILogger logger = LoggerFactory.getLogger(ConsumerPayYourProActivity.class.getSimpleName());

    @Inject
    ConsumerPayYourProPresenter presenter;

    @ViewById
    RecyclerView professionalsRecyclerView;

    @ViewById
    SwipeRefreshLayout professionalsSwipeRefreshLayout;

    SwipeRefreshLayout emptyRefreshLayout;

    @ViewById
    MultiStateView multiStateView;

    RecyclerViewAdapterBase professionalsAdapter;

    @AfterViews
    void afterViews() {
        presenter.bindView(this);

        professionalsSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadProfessionals();
            }
        });


        View emptyViewContainer = multiStateView.getView(MultiStateView.VIEW_STATE_EMPTY);
        emptyRefreshLayout = (SwipeRefreshLayout) emptyViewContainer.findViewById(R.id.emptyProfessionlsRefreshLayout);

        Button button = (Button) emptyViewContainer.findViewById(R.id.shootVideoButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.createNewProjectTapped();
            }
        });

        emptyRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadProfessionals();
            }
        });

        final ItemSelectedListener<PaymentProfessional> projectSelectedListener = new EmptyItemSelectedListener<PaymentProfessional>() {
            @Override
            public void onItemSelected(PaymentProfessional item, Bundle uiData) {
                professionalSelected(item, uiData);
            }
        };

        professionalsAdapter = new RecyclerViewAdapterBase(new RecyclerViewAdapterBase.IViewBuilder() {
            @Override
            public View build(int viewType) {
                return ProfessionalToPayItemView_.build(ConsumerPayYourProActivity.this);
            }
        }, projectSelectedListener);


        professionalsRecyclerView.setAdapter(professionalsAdapter);
        BaseItemAnimator animator = new jp.wasabeef.recyclerview.animators.FadeInAnimator();
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        professionalsRecyclerView.setItemAnimator(animator);
        professionalsRecyclerView.setLayoutManager(new LinearLayoutManager(ConsumerPayYourProActivity.this));

        initialize();
    }

    @Background
    void professionalSelected(PaymentProfessional item, Bundle uiData){
        presenter.professionalSelected(item, uiData);
    }

    @Background
    void loadProfessionals(){
        presenter.loadProfessionals();
    }

    @Background
    void initialize(){
        presenter.initialize();
    }

    @Override
    public String getViewTitle() {
        return "CHOOSE PRO TO PAY";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }

    @UiThread
    @Override
    public void showProfessionals(List<PaymentProfessional> professionals) {

        if(Lists.isNullOrEmpty(professionals))
            multiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
        else
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);

        professionalsAdapter.setItems(professionals);
    }

    @UiThread
    @Override
    public void showLoadingIndicator(boolean show) {
       if(show){
           emptyRefreshLayout.setRefreshing(true);
           professionalsSwipeRefreshLayout.setRefreshing(true);
       }else{
           emptyRefreshLayout.setRefreshing(false);
           professionalsSwipeRefreshLayout.setRefreshing(false);
       }
    }

    @UiThread
    @Override
    public void showLoadingProfessionalProjects(boolean isLoading) {
        if(isLoading){
            multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        }else {
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        }
    }

    @UiThread
    @Override
    public void onProjectsRetrieved(PaymentProfessional professional, List<PaymentProject> projectsOfProfessional, Bundle uiData) {
        presenter.navigateToProfessionalProjects(professional, projectsOfProfessional, uiData);
    }
}
