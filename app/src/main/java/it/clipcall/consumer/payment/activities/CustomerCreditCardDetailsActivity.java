package it.clipcall.consumer.payment.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.stripe.android.model.Token;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import it.clipcall.R;
import it.clipcall.consumer.payment.presenters.CustomerAddCreditCardPresenter;
import it.clipcall.consumer.projects.models.ProfessionalAccount;
import it.clipcall.consumer.projects.views.ICustomerAddCreditCardView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.ui.permissions.EmptyPermissionsHandler;

@EActivity(R.layout.customer_add_credit_card_activity)
public class CustomerCreditCardDetailsActivity extends BaseActivity implements IHasToolbar, ICustomerAddCreditCardView {



    private final int SCAN_REQUEST_CODE = 77;

    @ViewById
    Toolbar toolbar;

    @ViewById
    TextView chargeAmountTextView;

    @ViewById
    EditText creditCardNumberEditText;

    @ViewById
    EditText creditCardExpirationMonthEditText;

    @ViewById
    EditText creditCardExpirationYearEditText;

    @ViewById
    EditText emailEditText;

    @ViewById
    EditText cvvEditText;

    @ViewById
    Button payProButton;

    @ViewById
    ProgressBar payProProgressBar;

    @ViewById
    EditText fullNameEditText;

    @ViewById
    TextView scanCodeTextView;

    @ViewById
    ImageView scanCodeImageView;

    @ViewById
    TextView sendingProTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @ViewById
    CoordinatorLayout root;

    @Inject
    CustomerAddCreditCardPresenter presenter;


    @Click(R.id.eulaTextView)
    void eulaTapped(){
        presenter.navigateToEula();
    }

    @Click({R.id.scanCodeTextView, R.id.scanCodeImageView})
    void onScanCodeTapped(){

        presenter.requestPaymentCardScanPermissions(new EmptyPermissionsHandler(){

            @Override
            public void handleAllPermissionsGranted() {
                Intent scanIntent = new Intent(CustomerCreditCardDetailsActivity.this, CardIOActivity.class);

                // customize these values to suit your needs.
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false); // default: true

                // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
                startActivityForResult(scanIntent, SCAN_REQUEST_CODE);
            }
        });



    }


    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        presenter.initialize();
    }

    @Override
    public String getViewTitle() {
        return "CREDIT CARD DETAILS";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }

    @Override
    public void showInvalidFullName() {
        fullNameEditText.setError("Full name is required");
    }

    @TextChange(R.id.creditCardExpirationMonthEditText)
    public void creditCardExpirationMonthChange(CharSequence text, TextView hello, int before, int start, int count){
        if(text == null)
            return;

        Integer value = tryParse(text.toString());
        if(value == null)
            return;

        presenter.setExpirationMonth(value);
    }

    @TextChange(R.id.creditCardExpirationYearEditText)
    public void creditCardExpirationYearChange(CharSequence text, TextView hello, int before, int start, int count){
        if(text == null)
            return;

        Integer value = tryParse(text.toString());
        if(value == null)
            return;

        presenter.setExpirationYear(value);
    }

   private Integer tryParse(String value){
       try {
           return Integer.parseInt(value);
       } catch (NumberFormatException e) {
           return null;
       }
   }
    @TextChange(R.id.fullNameEditText)
    void onFullNameChanged(CharSequence text, TextView textView){
        presenter.setFullName(text+"");
    }

    @TextChange(R.id.creditCardNumberEditText)
    public void creditCardNumberTextChange(CharSequence text, TextView hello, int before, int start, int count){
        if(text != null)
            presenter.setCreditCardNumber(text.toString());
    }

    @TextChange(R.id.cvvEditText)
    public void cvvTextChanged(CharSequence text, TextView hello, int before, int start, int count){
        if(text != null)
            presenter.setCvv(text.toString());
    }

    @TextChange(R.id.emailEditText)
    public void emailTextChanged(CharSequence text, TextView hello, int before, int start, int count){
        if(text != null)
            presenter.setEmail(text.toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != SCAN_REQUEST_CODE)
            return;

        if(data == null)
            return;

        boolean hasExtra = data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT);
        if(hasExtra == false)
            return;

        CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
        if(scanResult == null)
            return;

        creditCardNumberEditText.setText(scanResult.getRedactedCardNumber());

        if (scanResult.isExpiryValid()) {
            creditCardExpirationMonthEditText.setText(scanResult.expiryMonth+"");
            creditCardExpirationYearEditText.setText(scanResult.expiryYear+"");
        }
        if (scanResult.cvv != null) {
            cvvEditText.setText(scanResult.cvv+"");
        }
    }

    @Click(R.id.payProButton)
    public void payTheProTapped(){
        setProcessingPaymentRequest(true);
        presenter.payThePro();
    }


    @Background
    void processPayment(Token token){
        presenter.processPayment(token);
    }

    @UiThread
    @Override
    public void showInvalidCreditCard() {

        setProcessingPaymentRequest(false);
    }

    @UiThread
    @Override
    public void showSuccessMessage(String message) {
        setProcessingPaymentRequest(false);
        Snackbar snackbar = Snackbar.make(root, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @UiThread
    @Override
    public void setDepositDetails(double amount, ProfessionalAccount professionalAccount) {
        String format = amount >= 1 ? "$%.2f" : "$0%.2f";
        String chargeAmountText = String.format( format, amount );
        chargeAmountTextView.setText(chargeAmountText);

        if(professionalAccount != null){
            String proText = String.format("You are sending %s", professionalAccount.getProfessionalName());
            sendingProTextView.setText(proText);
        }
    }

    @UiThread
    @Override
    public void showFailureMessage(String message) {
        setProcessingPaymentRequest(false);
        Snackbar snackbar = Snackbar.make(root, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @UiThread
    @Override
    public void showInvalidCreditCardNumber() {
        creditCardNumberEditText.setError("Invalid credit card number");
    }

    @UiThread
    @Override
    public void showInvalidExpirationMonth() {
        creditCardExpirationMonthEditText.setError("Invalid expiration month");
    }

    @UiThread
    @Override
    public void showInvalidExpirationYear() {
        creditCardExpirationYearEditText.setError("Invalid expiration year");
    }

    @UiThread
    @Override
    public void showInvalidCvv() {
        cvvEditText.setError("Invalid CVV");

    }

    @UiThread
    @Override
    public void showInvalidEmail() {
        emailEditText.setError("Invalid email address");
    }

    @Override
    public void showEmptyEmail() {
        emailEditText.setError("Email is required");
    }

    @Override
    public void tokenGenerated(Token token) {
        processPayment(token);
    }


    void setProcessingPaymentRequest(boolean isProcessing){
        if(isProcessing){
            scanCodeImageView.setEnabled(false);
            scanCodeTextView.setEnabled(false);
            payProButton.setVisibility(View.GONE);
            payProProgressBar.setVisibility(View.VISIBLE);
        }else{
            scanCodeImageView.setEnabled(true);
            scanCodeTextView.setEnabled(true);
            payProButton.setVisibility(View.VISIBLE);
            payProProgressBar.setVisibility(View.GONE);
        }
    }

}
