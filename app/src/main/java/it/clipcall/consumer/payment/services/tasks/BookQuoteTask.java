package it.clipcall.consumer.payment.services.tasks;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.common.chat.models.IConversation;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.common.chat.services.ChatParticipantFactory;
import it.clipcall.common.chat.support.factories.ChatMessageFactoryProvider;
import it.clipcall.consumer.payment.services.PaymentManager;
import it.clipcall.consumer.profile.services.CustomerProfileManager;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.consumer.projects.services.ProjectsManager;
import it.clipcall.infrastructure.tasks.ITask3;
import it.clipcall.professional.leads.models.Quote;
import it.clipcall.professional.validation.services.AuthenticationManager;


@Singleton
public class BookQuoteTask implements ITask3<ProjectEntity, ProfessionalRef,Quote, Boolean> {

    private final CustomerProfileManager profileManager;
    private final ChatManager chatManager;
    private final ProjectsManager projectsManager;
    private final AuthenticationManager authenticationManager;
    private final ChatParticipantFactory chatParticipantFactory;
    private final PaymentManager paymentManager;


    @Inject
    public BookQuoteTask(CustomerProfileManager profileManager, ChatManager chatManager, ProjectsManager projectsManager, AuthenticationManager authenticationManager, ChatParticipantFactory chatParticipantFactory, PaymentManager paymentManager){

        this.profileManager = profileManager;
        this.chatManager = chatManager;
        this.projectsManager = projectsManager;
        this.authenticationManager = authenticationManager;
        this.chatParticipantFactory = chatParticipantFactory;
        this.paymentManager = paymentManager;
    }

    @Override
    public Boolean execute(ProjectEntity project, final ProfessionalRef professional, Quote quote) {
        String customerId = authenticationManager.getCustomerId();

        project.markOtherProsAsLost(professional);

        ChatParticipant sender = chatParticipantFactory.buildConsumerChatParticipant(project);
        ChatParticipant receiver =  chatParticipantFactory.buildProfessionalChatParticipant(professional);

        IConversation conversationWithParticipant = chatManager.getConversationWithParticipant(sender, receiver, professional.getLeadAccountId());
        if(conversationWithParticipant == null){
            conversationWithParticipant = chatManager.createConversation(sender,receiver, professional.getLeadAccountId());
        }

        if(conversationWithParticipant == null){
            return Boolean.FALSE;
        }

        String messageData = String.format("Booked the service upon quote #%02d", quote.getNumber());
        MessageDescription messageDescription = ChatMessageFactoryProvider.getInstance().createMessageDescription(MessageType.BookQuote, messageData);
        chatManager.sendMessage(sender,receiver, professional.getLeadAccountId(), messageDescription);
        return Boolean.TRUE;
    }
}
