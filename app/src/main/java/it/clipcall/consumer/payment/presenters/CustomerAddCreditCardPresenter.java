package it.clipcall.consumer.payment.presenters;

import com.google.common.base.Strings;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;

import javax.inject.Inject;

import it.clipcall.common.environment.Environment;
import it.clipcall.common.permissions.ClipCallPermissionsManager;
import it.clipcall.consumer.payment.controllers.CustomerCreditCardController;
import it.clipcall.consumer.payment.models.PaymentDetails;
import it.clipcall.consumer.projects.models.ProfessionalAccount;
import it.clipcall.consumer.projects.views.ICustomerAddCreditCardView;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.ui.permissions.IPermissionsHandler;

@PerActivity
public class CustomerAddCreditCardPresenter extends PresenterBase<ICustomerAddCreditCardView> {

    private final CustomerCreditCardController controller;
    private final RoutingService routingService;
    private final ClipCallPermissionsManager clipCallPermissionsManager;
    private final RouteParams routeParams;

    private Boolean routeToHome;
    private ProfessionalAccount professionalAccount;

    @Inject
    public CustomerAddCreditCardPresenter(CustomerCreditCardController controller, RoutingService routingService, ClipCallPermissionsManager clipCallPermissionsManager, RouteParams routeParams) {
        this.controller = controller;
        this.routingService = routingService;
        this.clipCallPermissionsManager = clipCallPermissionsManager;
        this.routeParams = routeParams;
        this.professionalAccount = routeParams.getParam("projectProfessional");
        this.routeToHome = routeParams.getParam("routeToHome");
        this.controller.resetCardDetails();
    }

    @Override
    public void initialize() {
        super.initialize();
        double amount = controller.getAmount();
        view.setDepositDetails(amount, professionalAccount);
    }


    @RunOnUiThread
    public void payThePro() {

        controller.buildCard();

        boolean isValid = true;

        if(!controller.isValidFullName()){
            view.showInvalidFullName();
            isValid = false;
        }

        if(!controller.isValidCreditCardNumber()){
            view.showInvalidCreditCardNumber();
            isValid = false;
        }

        if(!controller.isValidExpirationMonth()){
            view.showInvalidExpirationMonth();
            isValid = false;
        }

        if(!controller.isValidExpirationYear()){
            view.showInvalidExpirationYear();
            isValid = false;
        }

        if(!controller.isValidCvv()){
            view.showInvalidCvv();
            isValid = false;
        }

        if(controller.isEmptyEmail()){
            view.showEmptyEmail();
            isValid = false;
        }

        if(!controller.isValidEmail()){
            view.showInvalidEmail();
            isValid = false;
        }


        boolean isValidCreditCard = isValid && controller.isValidCard() ;
        if(!isValidCreditCard) {
            view.showInvalidCreditCard();
            return;
        }

        Card card = controller.getCard();
        Stripe stripe = null;
         try {
            stripe = new Stripe(Environment.StripPublishedKey);
            stripe.createToken(
                card,
                new TokenCallback() {
                    public void onSuccess(Token token) {
                      view.tokenGenerated(token);
                    }

                    public void onError(Exception error) {
                        // Show localized error message
                        view.showInvalidCreditCard();
                        return;
                    }
                });
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }

        }


    public void processPayment(Token token){
        PaymentDetails paymentDetails = controller.processPayment(professionalAccount, token);
        if(paymentDetails == null){
            view.showFailureMessage("Payment was declined.");
            return;
        }

        NavigationContext context = new NavigationContext(view.getRoutingContext());

        routeParams.reset();
        routeParams.setParam("paymentDetails",paymentDetails);
        if(!Strings.isNullOrEmpty(professionalAccount.getProjectId())){
            routeParams.setParam("projectId", professionalAccount.getProjectId());
        }

        routeParams.setParam("advertiserId", professionalAccount.getProfessionalId());
        if(routeToHome != null && routeToHome){
            routeParams.setParam("routeToHome",routeToHome);
        }

/*        context.bundle = new Bundle();
        context.bundle.putParcelable("paymentDetails", Parcels.wrap(paymentDetails));
        context.addParameter("paymentId", paymentDetails.id);
        context.addParameter("projectId", projectProfessional.getAccountId());
        context.addParameter("advertiserId", projectProfessional.getProfessionalId());*/
        routingService.routeTo(Routes.ConsumerPaymentInvoiceRoute,context);
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.controller.setCardNumber(creditCardNumber);
    }

    public void setCvv(String cvv) {
        this.controller.setCvv(cvv);
    }

    public void setExpirationYear(Integer expirationYear) {
        this.controller.setExpirationYear(expirationYear);
    }

    public void setExpirationMonth(Integer expirationMonth) {
        this.controller.setExpirationMonth(expirationMonth);
    }

    public void setEmail(String email) {
        this.controller.setEmail(email);
    }

    public void requestPaymentCardScanPermissions(IPermissionsHandler permissionsHandler) {
        clipCallPermissionsManager.requestPaymentCardScanPermissions(permissionsHandler);
    }

    public void navigateToEula() {
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.addParameter("url","https://storage.googleapis.com/clipcallterms/ClipCall_EULA.pdf");
        navigationContext.addParameter("title","EULA");
        routingService.routeTo(Routes.ConsumerTermsOfServiceRoute, navigationContext);
    }

    public void setFullName(String fullName) {
        this.controller.setFullName(fullName);
    }
}
