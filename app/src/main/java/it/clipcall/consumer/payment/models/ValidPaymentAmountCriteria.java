package it.clipcall.consumer.payment.models;

import com.google.common.base.Strings;

import it.clipcall.infrastructure.support.criterias.ICriteria;

/**
 * Created by dorona on 05/06/2016.
 */
public class ValidPaymentAmountCriteria implements ICriteria<String> {
    @Override
    public boolean isSatisfiedBy(String candidate) {
        if(Strings.isNullOrEmpty(candidate))
            return false;

        try{
            double amount = Double.parseDouble(candidate);
            return amount >= 0.5;
        }
        catch (NumberFormatException e){
            return false;
        }
    }
}