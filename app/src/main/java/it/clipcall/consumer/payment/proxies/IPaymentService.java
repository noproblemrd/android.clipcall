package it.clipcall.consumer.payment.proxies;

import java.util.List;

import it.clipcall.consumer.payment.PaymentRequest;
import it.clipcall.consumer.payment.models.PaymentDetails;
import it.clipcall.consumer.payment.models.PaymentRef;
import it.clipcall.infrastructure.ProcessPaymentResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by dorona on 18/02/2016.
 */
public interface IPaymentService {


    @POST("customers/{customerId}/leads/{leadAccountId}/payments")
    Call<ProcessPaymentResponse> processPayment(@Path("customerId")String customerId, @Path("leadAccountId")String leadAccountId, @Body PaymentRequest request);

    @POST("v2/customers/{customerId}/leads/{leadAccountId}/payments/{quoteId}/cc")
    Call<ProcessPaymentResponse> processQuotePayment(@Path("customerId")String customerId, @Path("leadAccountId")String leadAccountId, @Path("quoteId")String quoteId, @Body PaymentRequest request);

    @POST("v2/customers/{customerId}/leads/{leadAccountId}/payments/{quoteId}")
    Call<ProcessPaymentResponse> rechargeCustomer(@Path("customerId")String customerId, @Path("leadAccountId")String leadAccountId, @Path("quoteId")String quoteId);


    @GET("customers/{customerId}/leads/{leadAccountId}/payments/{paymentId}")
    Call<PaymentDetails>  getPaymentDetails(@Path("customerId")String customerId, @Path("leadAccountId") String leadAccountId, @Path("paymentId") String paymentId);

    @GET("customers/{customerId}/leads/{leadAccountId}/payments")
    Call<List<PaymentRef>>  getCustomerPayments(@Path("customerId")String customerId, @Path("leadAccountId") String leadAccountId);
}
