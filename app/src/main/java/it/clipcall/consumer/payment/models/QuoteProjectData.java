package it.clipcall.consumer.payment.models;

import java.io.Serializable;

import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.professional.leads.models.Quote;

/**
 * Created by dorona on 28/06/2016.
 */
public class QuoteProjectData implements Serializable{

    public Quote quote;

    public ProjectEntity project;

    public String leadAccountId;

}
