package it.clipcall.consumer.payment.viewModel.views;

import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.leads.models.Quote;

/**
 * Created by dorona on 21/07/2016.
 */

public interface IQuoteBookedView extends IView {


    void showBookedQuote(ProfessionalRef professional, Quote quote);
}
