package it.clipcall.consumer.payment.controllers;

import com.google.common.base.Strings;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.payment.models.PaymentDetails;
import it.clipcall.consumer.payment.services.PaymentManager;
import it.clipcall.consumer.projects.models.Advertiser;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.consumer.projects.services.ProjectsManager;

@Singleton
public class PaymentInvoiceController {

    private final PaymentManager paymentManager;
    private final AuthenticationManager authenticationManager;
    private final ProjectsManager projectsManager;

    @Inject
    public PaymentInvoiceController(PaymentManager paymentManager, AuthenticationManager authenticationManager, ProjectsManager projectsManager) {
        this.paymentManager = paymentManager;
        this.authenticationManager = authenticationManager;
        this.projectsManager = projectsManager;
    }

    public PaymentDetails getPaymentDetails(String projectId, String advertiserId, String paymentId) {

        if(Strings.isNullOrEmpty(projectId))
            return null;

        if(Strings.isNullOrEmpty(advertiserId))
            return null;

        if(Strings.isNullOrEmpty(paymentId))
            return null;


        ProjectEntityBase project = projectsManager.getById(projectId);
        if(project == null)
            return null;

        ProjectEntity entity = (ProjectEntity)project;

        Advertiser advertiser = entity.getAdvertiserById(advertiserId);
        if(advertiser == null)
            return null;

        String leadAccountId = advertiser.getLeadAccountId();


        if(authenticationManager.isCustomerAuthenticated() == false)
            return null;

        String customerId = authenticationManager.getCustomerId();
        PaymentDetails paymentDetails =  paymentManager.getPaymentDetails(customerId,leadAccountId,paymentId);
        return paymentDetails;

    }

    public Advertiser getProjectProfessional(String projectId, String advertiserId) {
        ProjectEntityBase project = projectsManager.getById(projectId);
        if(project == null)
            return null;

        ProjectEntity projectEntity = (ProjectEntity)project;
        Advertiser professional = projectEntity.getProfessional(advertiserId);
        return professional;
    }
}
