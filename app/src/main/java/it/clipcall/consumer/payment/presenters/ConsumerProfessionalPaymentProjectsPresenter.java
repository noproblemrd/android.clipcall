package it.clipcall.consumer.payment.presenters;

import android.os.Bundle;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.consumer.payment.controllers.PaymentCoordinator;
import it.clipcall.consumer.payment.models.PaymentProfessional;
import it.clipcall.consumer.payment.models.PaymentProject;
import it.clipcall.consumer.payment.views.IConsumerProfessionalPaymentProjectsView;
import it.clipcall.consumer.projects.models.ProfessionalAccount;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;

@PerActivity
public class ConsumerProfessionalPaymentProjectsPresenter extends PresenterBase<IConsumerProfessionalPaymentProjectsView> {

    private final RouteParams routeParams;
    private final RoutingService routingService;
    private final PaymentCoordinator paymentCoordinator;

    private PaymentProfessional professional;
    private List<PaymentProject> projects;

    @Inject
    public ConsumerProfessionalPaymentProjectsPresenter(PaymentCoordinator paymentCoordinator, RouteParams routeParams, RoutingService routingService) {
        this.paymentCoordinator = paymentCoordinator;
        this.routeParams = routeParams;
        this.routingService = routingService;
        this.professional = routeParams.getParam("professional");
        this.projects = routeParams.getParam("professionalProjects");
    }

    public PaymentProfessional getProfessional(){
        return professional;
    }
    @Override
    public void initialize() {
        view.showProfessionalProjects(projects);
    }



    public void projectSelected(PaymentProject project, Bundle uiData) {
        RoutingContext routingContext = view.getRoutingContext();
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = routingContext;

        ProfessionalAccount professionalAccount = new ProfessionalAccount(project.getLeadAccountId(),professional.getSupplierId(),professional.getSupplierName());
        routeParams.reset();
        routeParams.setParam("projectProfessional", professionalAccount);
        routeParams.setParam("routeToHome", new Boolean(true));
        routingService.routeTo(Routes.PayTheProRoute, navigationContext);
    }

    public void loadProjects() {
        view.showLoadingIndicator(true);
        projects = paymentCoordinator.getProjectsOfProfessional(professional);
        view.showProfessionalProjects(projects);
        view.showLoadingIndicator(false);
    }
}
