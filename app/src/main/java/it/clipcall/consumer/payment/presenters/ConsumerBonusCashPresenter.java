package it.clipcall.consumer.payment.presenters;

import javax.inject.Inject;

import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.consumer.payment.views.IConsumerBonusCashView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.professional.profile.services.IDiscountClaimer;

@PerActivity
public class ConsumerBonusCashPresenter extends PresenterBase<IConsumerBonusCashView> {

    private final RouteParams routeParams;
    private final RoutingService routingService;
    private final IDiscountClaimer discountClaimer;
    private final EventAggregator eventAggregator;

    @Inject
    public ConsumerBonusCashPresenter(IDiscountClaimer discountClaimer, RouteParams routeParams, RoutingService routingService, EventAggregator eventAggregator) {
        this.discountClaimer = discountClaimer;
        this.routeParams = routeParams;
        this.routingService = routingService;
        this.eventAggregator = eventAggregator;
    }

    @Override
    public void initialize() {
        eventAggregator.publish(new ApplicationEvent(ConsumerEventNames.MenuBillingBonus));
        boolean isClaimed = discountClaimer.isDiscountClaimed();
        view.showDiscountClaimed(isClaimed);

    }

    public void claimDiscount() {
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();
        routingService.routeTo(Routes.ConsumerClaimDiscountRoute, navigationContext);


       /* view.showClaimeDiscountRequestInProgress(true);
        boolean discountClaimed = paymentCoordinator.claimDiscount();
        view.showDiscountClaimed(discountClaimed);*/
    }

    public void findProfessional() {
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();
        routingService.routeTo(Routes.ConsumerFindProRoute, navigationContext);
    }
}
