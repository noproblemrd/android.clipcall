package it.clipcall.consumer.payment.services.converters;

import it.clipcall.consumer.projects.models.QuoteHistoryItem;
import it.clipcall.infrastructure.converters.IConverter;
import it.clipcall.professional.leads.models.Quote;

/**
 * Created by dorona on 14/07/2016.
 */
public class QuoteHistoryItemToQuoteConverter implements IConverter<QuoteHistoryItem,Quote> {



    @Override
    public Quote convert(QuoteHistoryItem quoteHistoryItem) {
        Quote quote = new Quote();
        quote.setDescription(quoteHistoryItem.getDescription());
        quote.setCreatedOn(quoteHistoryItem.getDate());
        quote.setStartJobAt(quoteHistoryItem.getStartJobAt());
        quote.setPrice(quoteHistoryItem.getPrice());
        quote.setNumber(quoteHistoryItem.getNumber());
        quote.setStatus(quoteHistoryItem.getStatus());
        quote.setStatusReason(quoteHistoryItem.getStatusReason());
        return quote;
    }
}
