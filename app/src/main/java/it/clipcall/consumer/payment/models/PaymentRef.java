package it.clipcall.consumer.payment.models;

/**
 * Created by dorona on 22/02/2016.
 */
public class PaymentRef {

    public double amount;
    public String id;
    public String createDate;
    public String receiptNumber;
    public String description;
    public String customerName;
    public String customerPhoneNumber;
    public String last4Digits;
}