package it.clipcall.consumer.payment.controllers;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.profile.models.FacebookProfile;
import it.clipcall.consumer.profile.services.CustomerProfileManager;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class ConsumerClaimDiscountController {

    private final CustomerProfileManager profileManager;
    private final AuthenticationManager authenticationManager;

    @Inject
    public ConsumerClaimDiscountController(AuthenticationManager authenticationManager, CustomerProfileManager profileManager) {
        this.profileManager = profileManager;
        this.authenticationManager = authenticationManager;
    }

    public boolean updateCustomerProfile(FacebookProfile facebookProfile) {
        if(!authenticationManager.isCustomerAuthenticated())
            return false;

        String customerId = authenticationManager.getCustomerId();
        boolean updated = profileManager.updateCustomerProfile(customerId, facebookProfile);
        return updated;
    }
}
