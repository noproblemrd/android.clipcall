package it.clipcall.consumer.payment.services.messageHandlers;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.ClipCallApplication;
import it.clipcall.common.activities.SplashActivity_;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.payment.models.QuoteProjectData;
import it.clipcall.consumer.projects.controllers.ConsumerProjectsController;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.messageHandlers.IMessageHandler;
import it.clipcall.infrastructure.notifications.NotificationContext;
import it.clipcall.infrastructure.notifications.SimpleNotifier;
import it.clipcall.infrastructure.repositories.Repository2;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.infrastructure.support.dates.DateFormatter;
import it.clipcall.professional.leads.models.NewQuoteData;
import it.clipcall.professional.leads.models.Quote;
import it.clipcall.professional.leads.models.eQuoteStatus;
import it.clipcall.professional.leads.models.eQuoteStatusReason;
import it.clipcall.professional.validation.services.AuthenticationManager;


@Singleton
public class ConsumerAcceptQuoteMessageHandler implements IMessageHandler {


    private final RoutingService routingService;
    private final Repository2 repository;
    private final SimpleNotifier simpleNotifier;
    private final AuthenticationManager authenticationManager;
    private final RouteParams routeParams;
    private final ConsumerProjectsController consumerProjectsController;
    private final EventAggregator eventAggregator;

    @Inject
    public ConsumerAcceptQuoteMessageHandler(RoutingService routingService, Repository2 repository, SimpleNotifier simpleNotifier, AuthenticationManager authenticationManager, RouteParams routeParams, ConsumerProjectsController consumerProjectsController, EventAggregator eventAggregator){
        this.routingService = routingService;
        this.repository = repository;
        this.simpleNotifier = simpleNotifier;
        this.authenticationManager = authenticationManager;
        this.routeParams = routeParams;

        this.consumerProjectsController = consumerProjectsController;
        this.eventAggregator = eventAggregator;
    }


    @Override
    public boolean canHandlerMessage(String message) {
        boolean canHandle =  "NEW_QUOTE".equals(message.toUpperCase());
        return canHandle;
    }

    @Override
    public void handleForegroundMessage(String from, Bundle data, final Activity currentActivity) {

        final String message = data.getString("message");





        final NewQuoteData quoteData = getQuote(data);
        final ClipCallApplication application = (ClipCallApplication) currentActivity.getApplication();
        application.showMessage(message, new Runnable() {
            @Override
            public void run() {
                AsyncTask.THREAD_POOL_EXECUTOR.execute(new Runnable() {
                    @Override
                    public void run() {
                        if(authenticationManager.isInProfessionalMode()){
                            authenticationManager.switchToConsumerMode();
                        }

                        List<ProjectEntityBase> customerProjects = consumerProjectsController.getCustomerProjects(false);
                        ProjectEntityBase project = Lists.firstOrDefault(customerProjects, new ICriteria<ProjectEntityBase>() {
                            @Override
                            public boolean isSatisfiedBy(ProjectEntityBase candidate) {
                                return candidate.getId().equals(quoteData.leadId);
                            }
                        });

                        eventAggregator.publish(new ApplicationEvent(ConsumerEventNames.UserPushQuote ,quoteData.quote));

                        if(project != null){
                            routeParams.setParam("project",project);
                            routeParams.setParam("routeBackToHome", true);
                            routeParams.setParam("quote",quoteData.quote);
                            routeParams.setParam("leadAccountId",quoteData.leadAccountId);
                            NavigationContext navigationContext = new NavigationContext(new RoutingContext(currentActivity));
                            eventAggregator.publish(new ApplicationEvent(ConsumerEventNames.UserQuoteLandOn ,quoteData.quote));
                            routingService.routeTo(Routes.ConsumerQuoteDetailsRoute,navigationContext);
                        }
                    }
                });


            }
        }, true);






    }

    private NewQuoteData getQuote(Bundle data) {

        final String leadId = data.getString("leadId");
        String leadAccountId = data.getString("leadAccountId");
        String quoteId = data.getString("quoteId");
        NumberFormat format = NumberFormat.getInstance(Locale.US);
        double quotePrice = 0;
        try {
            Number number  = format.parse(data.getString("quotePrice"));
            quotePrice = number.doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        String quoteCreateDate = data.getString("quoteCreateDate");
        String quoteDescription = data.getString("quoteDescription");
        String quoteStartDate = data.getString("quoteStartJobAt");
        int quoteNumber = Integer.parseInt(data.getString("quoteNumber"));
        eQuoteStatus quoteStatus = eQuoteStatus.valueOf(data.getString("quoteStatus"));
        eQuoteStatusReason quoteStatusReason = eQuoteStatusReason.valueOf(data.getString("quoteStatusReason"));

        Quote quote = new Quote();
        quote.setPrice(quotePrice);
        quote.setDescription(quoteDescription);
        quote.setStatus(quoteStatus);
        quote.setStatusReason(quoteStatusReason);
        quote.setCreatedOn(DateFormatter.toDate(quoteCreateDate));
        quote.setStartJobAt(DateFormatter.toDate(quoteStartDate));
        quote.setId(quoteId);
        quote.setNumber(quoteNumber);

        NewQuoteData newQuoteData = new NewQuoteData();
        newQuoteData.leadId = leadId;
        newQuoteData.leadAccountId = leadAccountId;
        newQuoteData.quote = quote;

        return newQuoteData;
    }

    @Override
    public void handleBackgroundMessage(String from, Bundle data, final Application application) {
       /* Intent intent = new Intent(application.getApplicationContext(), ConsumerIncomingVideoCallActivity_.class);
        NewQuoteData parcelable = getQuote(data);
        Bundle bundle =  new Bundle();
        bundle.putParcelable("remoteServiceCallContext", parcelable);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(bundle);

        application.startActivity(intent);*/
        final NewQuoteData quoteData = getQuote(data);

        final NotificationContext notificationContext = new NotificationContext();
        notificationContext.title = data.getString("title");
        notificationContext.message = data.getString("message");
        notificationContext.bigContentTitle = data.getString("title");
        notificationContext.bigPictureImageUrl = data.getString("businessProfileImageUrl");
        notificationContext.contentTitle = data.getString("title");
        notificationContext.smallPictureImageUrl = data.getString("businessProfileImageUrl");
        notificationContext.summaryText = data.getString("message");
        notificationContext.notificationId = 17;
        notificationContext.notificationTag = quoteData.leadAccountId;



        AsyncTask.THREAD_POOL_EXECUTOR.execute(new Runnable() {
            @Override
            public void run() {
                if(authenticationManager.isInProfessionalMode()){
                    authenticationManager.switchToConsumerMode();
                }

                List<ProjectEntityBase> customerProjects = consumerProjectsController.getCustomerProjects(false);
                ProjectEntityBase project = Lists.firstOrDefault(customerProjects, new ICriteria<ProjectEntityBase>() {
                    @Override
                    public boolean isSatisfiedBy(ProjectEntityBase candidate) {
                        return candidate.getId().equals(quoteData.leadId);
                    }
                });

                if(project != null){
                    Intent intent = new Intent(application.getApplicationContext(), SplashActivity_.class);
                    QuoteProjectData quoteProjectData = new QuoteProjectData();
                    quoteProjectData.quote = quoteData.quote;
                    quoteProjectData.project = (ProjectEntity) project;
                    quoteProjectData.leadAccountId = quoteData.leadAccountId;
                    repository.update(quoteProjectData);
                    notificationContext.intent = intent;
                    simpleNotifier.notify(notificationContext);
                }
            }
        });
    }
}
