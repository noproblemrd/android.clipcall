package it.clipcall.consumer.payment.fragments;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.payment.presenters.ConsumerBonusCashPresenter;
import it.clipcall.consumer.payment.views.IConsumerBonusCashView;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.routing.models.RoutingContext;


@EFragment(R.layout.consumer_bonus_cash_fragment)
public class ConsumerBonusCashFragment extends BaseFragment implements IConsumerBonusCashView {

    @Inject
    ConsumerBonusCashPresenter presenter;

    @ViewById
    ViewGroup consumerProfileContainer;

    @ViewById
    ProgressBar loadingSpinnerProgressBar;

    @ViewById
    Button claimDiscountButton;

    @ViewById
    ViewGroup rootContainer;

    @AfterViews
    void consumerProfileViewLoaded(){
        presenter.bindView(this);
    }

    @Override
    protected String getTitle() {
        return "CLAIM $50 DISCOUNT";
    }

    @Click( {R.id.claimDiscountButton})
    void claimDiscountTapped(){
        presenter.claimDiscount();
    }

    @Override
    public RoutingContext getRoutingContext() {
        return new RoutingContext(getActivity());
    }


    @Override
    public void showClaimeDiscountRequestInProgress(boolean isInProgress) {

        if(isInProgress){
            claimDiscountButton.setVisibility(View.INVISIBLE);
            loadingSpinnerProgressBar.setVisibility(View.VISIBLE);
        }else{
            claimDiscountButton.setVisibility(View.VISIBLE);
            loadingSpinnerProgressBar.setVisibility(View.GONE);
        }

    }

    @Override
    public void showDiscountClaimed(boolean isDiscountClaimed) {
        if(isDiscountClaimed){
            loadingSpinnerProgressBar.setVisibility(View.GONE);
            claimDiscountButton.setVisibility(View.GONE);
            Snackbar.make(rootContainer,"Congratulations. You have claimed your $50 discount.",Snackbar.LENGTH_INDEFINITE).setAction("FIND A PRO", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.findProfessional();
                }
            }).show();
        }else{
            claimDiscountButton.setVisibility(View.VISIBLE);
            loadingSpinnerProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    protected int getMenuItemId() {
        return R.id.bonusCashMenuItem;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.initialize();
    }
}
