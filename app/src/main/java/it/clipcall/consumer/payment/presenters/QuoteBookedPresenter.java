package it.clipcall.consumer.payment.presenters;

import javax.inject.Inject;

import it.clipcall.consumer.payment.viewModel.views.IQuoteBookedView;
import it.clipcall.consumer.projects.models.ProfessionalAccount;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.professional.leads.models.Quote;

@PerActivity
public class QuoteBookedPresenter extends PresenterBase<IQuoteBookedView> {

    private final RouteParams routeParams;
    private final RoutingService routingService;
    private final EventAggregator eventAggregator;
    private final ProfessionalRef professional;

    private Quote quote;




    @Inject
    public QuoteBookedPresenter(RouteParams routeParams, RoutingService routingService, EventAggregator eventAggregator){
        this.routeParams = routeParams;
        this.routingService = routingService;
        this.eventAggregator = eventAggregator;
        this.professional = routeParams.getParam("professional");
        this.quote = routeParams.getParam("quote");
    }

    @Override
    public void initialize() {
        view.showBookedQuote(professional,quote);
    }

    public void navigateBack() {
        routingService.routeToHome(view.getRoutingContext());
    }
}
