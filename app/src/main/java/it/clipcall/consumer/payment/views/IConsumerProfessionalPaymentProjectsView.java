package it.clipcall.consumer.payment.views;

import java.util.List;

import it.clipcall.consumer.payment.models.PaymentProject;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by micro on 1/20/2016.
 */
public interface IConsumerProfessionalPaymentProjectsView extends IView {


    void showProfessionalProjects( List<PaymentProject> projects);

    void showLoadingIndicator(boolean show);

    void showLoadingProfessionalProjects(boolean isLoading);
}
