package it.clipcall.consumer.payment.services.converters;

import it.clipcall.consumer.projects.models.QuoteHistoryItem;
import it.clipcall.infrastructure.converters.IConverter;
import it.clipcall.professional.leads.models.Quote;

/**
 * Created by dorona on 14/07/2016.
 */
public class QuoteToQuoteHistoryItemConverter implements IConverter<Quote,QuoteHistoryItem> {
    @Override
    public QuoteHistoryItem convert(Quote quote) {
        final QuoteHistoryItem item = new QuoteHistoryItem();
        item.setStartJobAt(quote.getStartJobAt());
        item.setDescription(quote.getDescription());
        item.setPrice(quote.getPrice());
        item.setNumber(quote.getNumber());
        item.setDate(quote.getCreatedOn());
        item.setStatus(quote.getStatus());
        item.setStatusReason(quote.getStatusReason());
        item.setId(quote.getId());

        return item;

    }
}
