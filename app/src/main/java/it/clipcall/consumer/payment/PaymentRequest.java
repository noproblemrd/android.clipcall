package it.clipcall.consumer.payment;

import com.stripe.android.model.Token;

import it.clipcall.consumer.payment.models.Payment;

/**
 * Created by dorona on 18/02/2016.
 */
public class PaymentRequest {

    public PaymentRequest(Payment payment, Token token, String name) {
        this.payment = payment;
        this.token = token;
        this.name = name;
    }

    Payment payment;
    String name;
    Token token;

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
