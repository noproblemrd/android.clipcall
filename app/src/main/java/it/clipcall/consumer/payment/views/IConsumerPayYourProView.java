package it.clipcall.consumer.payment.views;

import android.os.Bundle;

import java.util.List;

import it.clipcall.consumer.payment.models.PaymentProfessional;
import it.clipcall.consumer.payment.models.PaymentProject;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by micro on 1/20/2016.
 */
public interface IConsumerPayYourProView extends IView {


    void showProfessionals(List<PaymentProfessional> professionals);

    void showLoadingIndicator(boolean show);

    void showLoadingProfessionalProjects(boolean isLoading);

    void onProjectsRetrieved(PaymentProfessional professional, List<PaymentProject> projectsOfProfessional, Bundle uiData);
}
