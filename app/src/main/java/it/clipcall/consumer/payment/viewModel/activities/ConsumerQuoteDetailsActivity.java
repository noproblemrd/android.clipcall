package it.clipcall.consumer.payment.viewModel.activities;

import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.venmo.view.TooltipView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.payment.domainModel.services.QuoteDateFormatter;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.payment.presenters.ConsumerQuoteDetailsPresenter;
import it.clipcall.consumer.payment.viewModel.views.IConsumerQuoteDetailsView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.converters.UsCurrencyFormatter;
import it.clipcall.infrastructure.support.dates.DateFormatter;
import it.clipcall.professional.leads.models.Quote;

@EActivity(R.layout.consumer_quote_details_activity)
public class ConsumerQuoteDetailsActivity extends BaseActivity implements IConsumerQuoteDetailsView, IHasToolbar{


    @Inject
    UsCurrencyFormatter usCurrencyFormatter;

    @Inject
    ConsumerQuoteDetailsPresenter presenter;

    @Inject
    QuoteDateFormatter quoteDateFormatter;

    @ViewById
    Toolbar toolbar;

    @ViewById
    TextView quotePriceTextView;

    @ViewById
    TooltipView toolTipView;

    @ViewById
    ImageView bookedImageView;


    @ViewById
    ViewGroup rootCoordinatorLayout;

    @ViewById
    ViewGroup bookQuoteContainer;


    @ViewById
    TextView startDateTextView;

    @ViewById
    TextView quoteDescriptionTextView;

    @ViewById
    TextView notReadyToBookTextView;

    @ViewById
    ProgressBar bookQuoteProgressBar;

    @ViewById
    Button bookQuoteButton;

    @Override
    public String getViewTitle() {
        return "Quote Details";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        presenter.navigateBack();
        return true;
    }

    @Click(R.id.notReadyToBookTextView)
    void onNotReadyToBookYetTapped(){
        presenter.navigateToChat();
    }

    @Override
    public void onBackPressed() {
        onBackTapped();
    }

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        presenter.initialize();

        rootCoordinatorLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition(rootCoordinatorLayout);
                }
                toolTipView.setVisibility(View.GONE);

                return true;
            }
        });
    }

    @Click(R.id.bookQuoteButton)
    void sendQuoteTapped(){
        presenter.bookQuote();
    }

    @Override
    public void showQuote(ProjectEntityBase project, Quote quote, boolean isProMode) {
        quotePriceTextView.setText(usCurrencyFormatter.format(quote.getPrice()));
        startDateTextView.setText(quoteDateFormatter.format(quote));

        quoteDescriptionTextView.setText(quote.getDescription());

        if(quote.isBooked()){
            bookedImageView.setVisibility(View.VISIBLE);
            toolTipView.setVisibility(View.INVISIBLE);
            bookQuoteButton.setVisibility(View.INVISIBLE);
            notReadyToBookTextView.setVisibility(View.INVISIBLE);
            return;
        }


        if(isProMode || !quote.isActive()){
            bookQuoteContainer.setVisibility(View.INVISIBLE);
        }else{
            bookedImageView.setVisibility(View.INVISIBLE);
            bookQuoteButton.setVisibility(View.VISIBLE);
            notReadyToBookTextView.setVisibility(View.VISIBLE);
        }



        toolbar.setTitle("Quote "+ String.format("%02d", quote.getNumber()));

        String subtitle = String.format("%s // %s", project.getName(), DateFormatter.toLocalDate(quote.getCreatedOn()));
        toolbar.setSubtitle(subtitle);
    }

    @Override
    public void showBookingInProgress(boolean isInProgress) {
        if(isInProgress){
            bookQuoteButton.setVisibility(View.INVISIBLE);
            bookQuoteProgressBar.setVisibility(View.VISIBLE);
        }else{
            bookQuoteButton.setVisibility(View.VISIBLE);
            bookQuoteProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(rootCoordinatorLayout,message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void close() {
        finish();
    }
}