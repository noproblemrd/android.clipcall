package it.clipcall.consumer.payment.controllers;

import com.google.common.base.Strings;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import java.util.regex.Matcher;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.payment.models.Payment;
import it.clipcall.consumer.payment.models.PaymentDetails;
import it.clipcall.consumer.payment.models.ValidPaymentAmountCriteria;
import it.clipcall.consumer.payment.services.PaymentManager;
import it.clipcall.consumer.projects.models.Advertiser;
import it.clipcall.consumer.projects.models.BonusInfo;
import it.clipcall.consumer.projects.models.ProfessionalAccount;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.consumer.projects.services.ProjectsManager;
import it.clipcall.infrastructure.support.strings.Strings2;
import it.clipcall.professional.leads.models.Quote;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class CustomerCreditCardController {

    private final PaymentManager paymentManager;
    private final AuthenticationManager authenticationManager;
    private final ProjectsManager projectsManager;

    private double amount;
    private String cvv;
    private Integer expirationMonth;
    private Integer expirationYear;
    private String cardNumber;



    private Card card;
    private String email;
    private String fullName;

    @Inject
    public CustomerCreditCardController(PaymentManager paymentManager, AuthenticationManager authenticationManager, ProjectsManager projectsManager) {
        this.paymentManager = paymentManager;
        this.authenticationManager = authenticationManager;
        this.projectsManager = projectsManager;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public Integer getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(Integer expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public Integer getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(Integer expirationYear) {
        this.expirationYear = expirationYear;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }



    public void buildCard(){
        card = new Card.Builder(this.cardNumber,this.expirationMonth,this.expirationYear,this.cvv).build();
    }


    public boolean isValidCard(){

        if(card == null)
            return false;

        boolean isValidCard = card.validateCard();
        return isValidCard;
    }

    public boolean isValidCreditCardNumber(){
        return card.validateNumber();
    }

    public boolean isValidFullName(){
        return !Strings2.isNullOrWhiteSpace(fullName);
    }

    public boolean isValidExpirationMonth(){
        return card.validateExpMonth();
    }

    public boolean isValidExpirationYear(){
        return card.validateExpYear();
    }

    public boolean isValidCvv(){
        return card.validateCVC();
    }

    public boolean isEmptyEmail(){
        return Strings2.isNullOrWhiteSpace(email);
    }

    public boolean isValidEmail(){
        if(Strings.isNullOrEmpty(email))
            return true;
        Matcher matcher = android.util.Patterns.EMAIL_ADDRESS.matcher(email);
        return matcher.matches();
    }

    public Card getCard() {
        return card;
    }

    public PaymentDetails processPayment(ProfessionalAccount professionalAccount, Token token) {

        if(Strings.isNullOrEmpty(professionalAccount.getAccountId()))
            return null;

        if(Strings.isNullOrEmpty(professionalAccount.getProfessionalId()))
            return null;

        if(token == null)
            return null;



        if(authenticationManager.isCustomerAuthenticated() == false)
            return null;

        String customerId = authenticationManager.getCustomerId();

        Payment payment = new Payment();
        payment.setAmount(getAmount());
        payment.setReceiptEmail(email);
        PaymentDetails paymentDetails =  paymentManager.processPaymentToken(customerId, professionalAccount.getAccountId(),payment, token, fullName);
        return paymentDetails;

    }

    public Advertiser getAdvertiser(String projectId, String advertiserId){
        ProjectEntityBase project = projectsManager.getById(projectId);
        if(project == null)
            return null;

        ProjectEntity entity = (ProjectEntity)project;
        Advertiser advertiser = entity.getAdvertiserById(advertiserId);
        return advertiser;

    }

    public double getAmount() {
        return amount;
    }

    public boolean isValidAmount(String amount){
        return new ValidPaymentAmountCriteria().isSatisfiedBy(amount);
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void resetCardDetails() {
        cardNumber = "";
        cvv = "";
        email = "";
        card = null;
        expirationMonth = 0;
        expirationYear = 0;
        fullName = null;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName(){
        return fullName;
    }

    public PaymentDetails processPayment(ProfessionalRef professional, Token token, Quote quote) {
        String customerId = authenticationManager.getCustomerId();

        Payment payment = new Payment();
        payment.setAmount(getAmount());
        payment.setReceiptEmail(email);
        PaymentDetails paymentDetails =  paymentManager.processPaymentQuote(customerId, professional.getLeadAccountId(),payment, token, fullName, quote);
        return paymentDetails;
    }

    public PaymentDetails rechargeCustomer(ProfessionalRef professionalAccount, Quote quote){
        String customerId = authenticationManager.getCustomerId();
        PaymentDetails paymentDetails = paymentManager.rechargeCustomer(customerId,professionalAccount.getLeadAccountId(),quote);
        return paymentDetails;
    }

    public BonusInfo getBonusEntitled(String leadAccountId) {
        String customerId = authenticationManager.getCustomerId();
        BonusInfo bonus = paymentManager.getBonusEntitled(customerId, leadAccountId);
        return bonus;
    }
}
