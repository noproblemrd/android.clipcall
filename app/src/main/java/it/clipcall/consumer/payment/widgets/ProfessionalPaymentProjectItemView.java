package it.clipcall.consumer.payment.widgets;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import it.clipcall.R;
import it.clipcall.consumer.payment.models.PaymentProject;
import it.clipcall.infrastructure.support.recyclerviews.ActionInvokerListener;
import it.clipcall.infrastructure.viewModel.support.recycleviews.IActionBindable;
import it.clipcall.infrastructure.viewModel.support.recycleviews.IBindable;
import it.clipcall.infrastructure.viewModel.support.recycleviews.ItemSelectedListener;


@EViewGroup(R.layout.professional_payment_project_item)
public class ProfessionalPaymentProjectItemView extends LinearLayout implements IBindable<PaymentProject>, IActionBindable<PaymentProject> {

    @ViewById
    ViewGroup projectContainer;


    @ViewById
    TextView categoryTextView;

    @ViewById
    TextView createDateTextView;

    @ViewById
    TextView addressTextView;

    @ViewById
    ImageView profileLogoImageView;


    public ProfessionalPaymentProjectItemView(Context context) {
        this(context,null);
    }

    public ProfessionalPaymentProjectItemView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ProfessionalPaymentProjectItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void bind(final PaymentProject project, int position, final ItemSelectedListener itemSelectedListener) {
        Picasso.with(getContext()).load(project.getPicPreviewUrl())
                .into(profileLogoImageView);
        categoryTextView.setText(project.getCategory());
        addressTextView.setText(project.getAddressOrZipCode());

        String pattern ="dd MMM yyyy, hh:mm aa";
        DateFormat formatter = new SimpleDateFormat(pattern);
        String formattedDate = formatter.format(project.getDate());
        createDateTextView.setText(formattedDate);
        projectContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                itemSelectedListener.onItemSelected(project, null);
            }
        });

    }
    @Override
    public void bindAction(PaymentProject entity, int position, ActionInvokerListener listener) {

    }
}
