package it.clipcall.consumer.payment.activities;

import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.kennyc.view.MultiStateView;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.payment.models.PaymentDetails;
import it.clipcall.consumer.payment.presenters.PaymentInvoicePresenter;
import it.clipcall.consumer.projects.views.IPaymentInvoiceView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;


@EActivity(R.layout.payment_invoice_activity)
public class PaymentInvoiceActivity extends BaseActivity implements IHasToolbar, IPaymentInvoiceView {

    @ViewById
    Toolbar toolbar;

    @Inject
    PaymentInvoicePresenter presenter;

    @ViewById
    TextView chargeAmountTextView;

    @ViewById
    TextView customerNameTextView;

    @ViewById
    TextView creditCardLast4DigitsTextView;

    @ViewById
    TextView totalChargeTextView;

    @ViewById
    TextView descriptionTextView;


    @ViewById
    ImageView creditCardImageView;

    @ViewById
    MultiStateView multiStateView;

    @Override
    public String getViewTitle() {
        return "";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        initialize();
    }




    @Background
    void initialize(){
        presenter.initialize();
    }

    @Override
    public boolean onBackTapped() {
        onBackPressed();
        return false;
    }

    @UiThread
    @Override
    public void showInvoice(PaymentDetails paymentDetails) {
        String receiptTitle;
        if(Strings.isNullOrEmpty(paymentDetails.receiptNumber)){
            receiptTitle = "Receipt";
        }
        else {
            receiptTitle = "Receipt #" +paymentDetails.receiptNumber;
        }
        toolbar.setTitle(receiptTitle);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        toolbar.setSubtitle(dateFormat.format(paymentDetails.createDate));

        customerNameTextView.setText(paymentDetails.customerName);

        DecimalFormat df = new DecimalFormat("#.00");
        chargeAmountTextView.setText("$"+ df.format(paymentDetails.amount));
        totalChargeTextView.setText("$"+df.format(paymentDetails.amount));
        creditCardLast4DigitsTextView.setText(paymentDetails.last4Digits);

        descriptionTextView.setText(paymentDetails.description);


        String cardType = paymentDetails.cardType;
        if(Strings.isNullOrEmpty(cardType))
            return;


        if(cardType.toUpperCase().equals("VISA")){
            Picasso.with(this).load(R.drawable.visa_card).into(creditCardImageView);
        }else if(cardType.toUpperCase().equals("MASTERCARD")){
            Picasso.with(this).load(R.drawable.mastercard_card).into(creditCardImageView);
        }else if(cardType.toUpperCase().equals("AMERICAN EXPRESS")){
            Picasso.with(this).load(R.drawable.american_express_card).into(creditCardImageView);
        }else if(cardType.toUpperCase().equals("DISCOVER")){
            Picasso.with(this).load(R.drawable.discover_card).into(creditCardImageView);
        }
        else if(cardType.toUpperCase().equals("JCB")){
            Picasso.with(this).load(R.drawable.jcb_card).into(creditCardImageView);
        }
        else if(cardType.toUpperCase().equals("DINERS CLUB")){
            Picasso.with(this).load(R.drawable.diners_card).into(creditCardImageView);
        }
    }

    @UiThread
    @Override
    public void showInProgress(boolean isInProgress) {
        if(isInProgress)
            multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        else
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
    }


    @Override
    public void onBackPressed() {
        presenter.navigateToProjectAdvertiser();
    }
}
