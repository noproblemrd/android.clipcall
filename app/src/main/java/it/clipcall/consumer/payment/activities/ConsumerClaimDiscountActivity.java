package it.clipcall.consumer.payment.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.widget.Toolbar;

import com.facebook.login.widget.LoginButton;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.payment.presenters.ConsumerClaimDiscountPresenter;
import it.clipcall.consumer.payment.views.IConsumerClaimDiscountView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;

@EActivity(R.layout.consumer_claim_discount_activity)
public class ConsumerClaimDiscountActivity extends BaseActivity implements IHasToolbar, IConsumerClaimDiscountView {

    private ProgressDialog progressDialog;

    private final ILogger logger = LoggerFactory.getLogger(ConsumerClaimDiscountActivity.class.getSimpleName());

    @ViewById
    Toolbar toolbar;

    @Inject
    ConsumerClaimDiscountPresenter presenter;

    @ViewById
    LoginButton facebookLoginButton;


    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        facebookLoginButton.setReadPermissions("public_profile", "email");
        presenter.registerFacebookLoginButton(facebookLoginButton);
        presenter.initialize();
    }

    @Override
    public String getViewTitle() {
        return "CLAIM $50 DISCOUNT";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.setFacebookActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void close() {
        finish();
    }

    @Override
    public void showLoadingFacebookProfile(boolean show) {
        if(show){
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
            return;
        }

        if(progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }

    }
}
