package it.clipcall.consumer.payment.presenters;

import javax.inject.Inject;

import it.clipcall.consumer.payment.controllers.CustomerCreditCardController;
import it.clipcall.consumer.projects.models.BonusInfo;
import it.clipcall.consumer.projects.models.ProfessionalAccount;
import it.clipcall.consumer.projects.views.IBuyTheProView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;

@PerActivity
public class PayTheProPresenter extends PresenterBase<IBuyTheProView> {

    private final CustomerCreditCardController controller;
    private final RoutingService routingService;
    private final RouteParams routeParams;
    private final ProfessionalAccount professionalAccount;
    private Boolean routeToHome;
    private BonusInfo bonusEntitled;

    @Inject
    public PayTheProPresenter(CustomerCreditCardController controller, RoutingService routingService, RouteParams routeParams) {
        this.controller = controller;
        this.routingService = routingService;
        this.routeParams = routeParams;
        this.professionalAccount = routeParams.getParam("projectProfessional");
        this.routeToHome = routeParams.getParam("routeToHome");
    }

    @Override
    public void initialize() {
        view.showPayedProfessional(professionalAccount);

        this.bonusEntitled = controller.getBonusEntitled(professionalAccount.getAccountId());
        if(bonusEntitled.isHasBonus() && bonusEntitled.getBonus() > 0){
            view.showBonus(bonusEntitled.getBonus());
        }
    }

    public void navigateToEula(){
       /* NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.addParameter("url","https://storage.googleapis.com/clipcallterms/ClipCall_EULA.pdf");
        navigationContext.addParameter("title","EULA");
        routingService.routeTo(Routes.ConsumerTermsOfServiceRoute, navigationContext);*/


        routeParams.reset();
        routeParams.setParam("url","http://www.clipcall.it/main/guarantee-policy/");
        routeParams.setParam("title","TERMS AND CONDITIONS");
        NavigationContext navigationContext  = new NavigationContext(view.getRoutingContext());
        routingService.routeTo(Routes.TermsAndConditionsApplyRoute, navigationContext);
    }


    public void payWithCreditCard(String amount) {
        if(!this.controller.isValidAmount(amount)){
            view.showInvalidAmount();
            return;
        }

        final Double doubleAmount = Double.parseDouble(amount);
        controller.setAmount(doubleAmount);

        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        routeParams.reset();
        routeParams.setParam("projectProfessional", professionalAccount);
        if(routeToHome != null && routeToHome){
            routeParams.setParam("routeToHome",routeToHome);
        }

        routingService.routeTo(Routes.ConsumerAddCreditCardRoute, navigationContext);
    }

    public void setChargeAmount(double chargeAmount) {
        this.controller.setAmount(chargeAmount);
        if(!bonusEntitled.isHasBonus() || bonusEntitled.getBonus() <= 0)
            return;


        double bonusToShow  = chargeAmount == 0 ? bonusEntitled.getBonus() : Math.min(chargeAmount, bonusEntitled.getBonus());
        view.showBonus(bonusToShow);
    }

    public void payWithAndroidPay(String amount) {

        if(!this.controller.isValidAmount(amount)){
            view.showInvalidAmount();
            return;
        }

        final Double doubleAmount = Double.parseDouble(amount);
        controller.setAmount(doubleAmount);
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        routeParams.reset();
        routeParams.setParam("projectProfessional", professionalAccount);
        routingService.routeTo(Routes.ConsumerAndroidPayActivity, navigationContext);
    }
}
