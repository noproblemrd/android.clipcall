package it.clipcall.consumer.payment.presenters;

import android.os.Bundle;

import org.parceler.Parcels;

import javax.inject.Inject;

import it.clipcall.consumer.payment.controllers.PaymentInvoiceController;
import it.clipcall.consumer.payment.models.PaymentDetails;
import it.clipcall.consumer.projects.models.Advertiser;
import it.clipcall.consumer.projects.views.IPaymentInvoiceView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;

@PerActivity
public class PaymentInvoicePresenter extends PresenterBase<IPaymentInvoiceView> {

    private final PaymentInvoiceController controller;
    private final RoutingService routingService;
    private String projectId;
    private String advertiserId;
    private PaymentDetails paymentDetails;
    Boolean routeToHome;

    @Inject
    public PaymentInvoicePresenter(PaymentInvoiceController controller, RoutingService routingService, RouteParams routeParams) {
        this.controller = controller;
        this.routingService = routingService;
        this.paymentDetails = routeParams.getParam("paymentDetails");
        this.projectId = routeParams.getParam("projectId");
        this.advertiserId = routeParams.getParam("advertiserId");
        this.routeToHome = routeParams.getParam("routeToHome");
    }

    @Override
    public void initialize() {
        view.showInvoice(paymentDetails);
    }

    public void navigateToProjectAdvertiser() {

        if(routeToHome != null  && routeToHome){
            routingService.routeToHome(view.getRoutingContext());
            return;
        }
        NavigationContext context = new NavigationContext();
        context.routingContext = view.getRoutingContext();
        context.addParameter("projectId",projectId);
        context.addParameter("advertiserId",advertiserId);
        context.addParameter("landOnHistory", "true");
        Advertiser professional = controller.getProjectProfessional(projectId, advertiserId);
        context.bundle = new Bundle();
        context.bundle.putParcelable("context", Parcels.wrap(professional));
        context.navigateBack = true;
        routingService.routeTo(Routes.ConsumerAdvertiserRoute, context);
    }

    public void setPaymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }
}
