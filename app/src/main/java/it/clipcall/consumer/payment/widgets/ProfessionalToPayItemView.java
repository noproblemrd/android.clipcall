package it.clipcall.consumer.payment.widgets;


import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import it.clipcall.R;
import it.clipcall.consumer.payment.models.PaymentProfessional;
import it.clipcall.infrastructure.support.recyclerviews.ActionInvokerListener;
import it.clipcall.infrastructure.viewModel.support.recycleviews.IActionBindable;
import it.clipcall.infrastructure.viewModel.support.recycleviews.IBindable;
import it.clipcall.infrastructure.viewModel.support.recycleviews.ItemSelectedListener;

@EViewGroup(R.layout.professional_to_pay_item)
public class ProfessionalToPayItemView extends LinearLayout implements IBindable<PaymentProfessional>, IActionBindable<PaymentProfessional> {


    @ViewById
    ViewGroup professionalContainer;

    @ViewById
    TextView clipCallLoveCountTextView;


    @ViewById
    TextView nameTextView;

    @ViewById
    ImageView profileLogoImageView;






    public ProfessionalToPayItemView(Context context) {
        this(context,null);
    }

    public ProfessionalToPayItemView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ProfessionalToPayItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void bind(final PaymentProfessional professional, int position, final ItemSelectedListener itemSelectedListener) {
        Picasso.with(getContext()).load(professional.getSupplierImageUrl())
                .into(profileLogoImageView);
        nameTextView.setText(professional.getSupplierName());
        clipCallLoveCountTextView.setText(professional.getAllLove()+"");


        professionalContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final String transitionName = String.format("ImageTransition%s", professional.getSupplierId());
                selectProfessional(transitionName, itemSelectedListener,professional);
            }
        });

    }

    private void selectProfessional(String transitionName, ItemSelectedListener itemSelectedListener, PaymentProfessional professional) {
        Bundle uiBundle = new Bundle();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Activity activity = (Activity) getContext();
            profileLogoImageView.setTransitionName(transitionName);
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(activity,profileLogoImageView, transitionName);
            uiBundle = options.toBundle();
        }

        itemSelectedListener.onItemSelected(professional, uiBundle);
    }

    @Override
    public void bindAction(PaymentProfessional entity, int position, ActionInvokerListener listener) {

    }
}
