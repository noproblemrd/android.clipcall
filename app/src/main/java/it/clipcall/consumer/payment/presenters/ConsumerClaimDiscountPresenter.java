package it.clipcall.consumer.payment.presenters;

import android.content.Intent;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import javax.inject.Inject;

import it.clipcall.common.thirdParty.FacebookService;
import it.clipcall.common.thirdParty.IFacebookProfileCallback;
import it.clipcall.consumer.payment.views.IConsumerClaimDiscountView;
import it.clipcall.consumer.profile.models.FacebookProfile;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.professional.profile.services.IDiscountClaimer;

@PerActivity
public class ConsumerClaimDiscountPresenter extends PresenterBase<IConsumerClaimDiscountView> implements FacebookCallback<LoginResult>,IFacebookProfileCallback {
    private final FacebookService facebookService;
    private final IDiscountClaimer discountClaimer;

    @Inject
    public ConsumerClaimDiscountPresenter(FacebookService facebookService, IDiscountClaimer discountClaimer) {
        this.facebookService = facebookService;
        this.discountClaimer = discountClaimer;
    }

    @Override
    public void initialize() {
        facebookService.start();
    }

    public void updateCustomerProfile(FacebookProfile facebookProfile) {
        boolean updated = discountClaimer.updateCustomerProfile(facebookProfile);
        if(updated){
            discountClaimer.claimDiscount();
            view.close();
        }
    }

    @RunOnUiThread
    public void registerFacebookLoginButton(LoginButton facebookLoginButton) {
        facebookService.registerFacebookLoginButton(facebookLoginButton, this);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        view.showLoadingFacebookProfile(true);
        facebookService.getProfile(loginResult.getAccessToken(),this);
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {

    }

    @Override
    public void onSuccess(FacebookProfile facebookProfile) {
        view.showLoadingFacebookProfile(false);
        this.updateCustomerProfile(facebookProfile);
    }

    public void setFacebookActivityResult(int requestCode, int resultCode, Intent data) {
        facebookService.setFacebookActivityResult(requestCode,resultCode,data);
    }
}