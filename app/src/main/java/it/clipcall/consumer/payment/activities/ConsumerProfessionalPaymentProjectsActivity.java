package it.clipcall.consumer.payment.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.kennyc.view.MultiStateView;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.payment.models.PaymentProfessional;
import it.clipcall.consumer.payment.models.PaymentProject;
import it.clipcall.consumer.payment.presenters.ConsumerProfessionalPaymentProjectsPresenter;
import it.clipcall.consumer.payment.views.IConsumerProfessionalPaymentProjectsView;
import it.clipcall.consumer.payment.widgets.ProfessionalPaymentProjectItemView_;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.recyclerviews.EmptyItemSelectedListener;
import it.clipcall.infrastructure.viewModel.support.recycleviews.ItemSelectedListener;
import it.clipcall.infrastructure.viewModel.support.recycleviews.RecyclerViewAdapterBase;
import jp.wasabeef.recyclerview.animators.BaseItemAnimator;

@EActivity(R.layout.consumer_professional_payment_projects_activity)
public class ConsumerProfessionalPaymentProjectsActivity extends BaseActivity implements IHasToolbar, IConsumerProfessionalPaymentProjectsView {
    @ViewById
    Toolbar toolbar;

    private final ILogger logger = LoggerFactory.getLogger(ConsumerProfessionalPaymentProjectsActivity.class.getSimpleName());

    @Inject
    ConsumerProfessionalPaymentProjectsPresenter presenter;

    @ViewById
    RecyclerView professionalsRecyclerView;

    @ViewById
    ImageView profileLogoImageView;

    @ViewById
    TextView proNameTextView;

    @ViewById
    SwipeRefreshLayout professionalsSwipeRefreshLayout;

    SwipeRefreshLayout emptyRefreshLayout;

    @ViewById
    MultiStateView multiStateView;

    RecyclerViewAdapterBase professionalsAdapter;


    @AfterViews
    void afterViews() {

        presenter.bindView(this);
        PaymentProfessional professional = presenter.getProfessional();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String transitionName = String.format("ImageTransition%s",professional.getSupplierId());
            profileLogoImageView.setTransitionName(transitionName);
            logger.debug("setting transition with name: %s to profileLogoImageView", transitionName);
        }
        Picasso.with(this).load(professional.getSupplierImageUrl()).into(profileLogoImageView);
        proNameTextView.setText(professional.getSupplierName());

        professionalsSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadProjects();
            }
        });


        View emptyViewContainer = multiStateView.getView(MultiStateView.VIEW_STATE_EMPTY);
        emptyRefreshLayout = (SwipeRefreshLayout) emptyViewContainer.findViewById(R.id.emptyProfessionlsRefreshLayout);
        emptyRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadProjects();
            }
        });

        final ItemSelectedListener<PaymentProject> projectSelectedListener = new EmptyItemSelectedListener<PaymentProject>() {
            @Override
            public void onItemSelected(PaymentProject item, Bundle uiData) {
                presenter.projectSelected(item, uiData);
            }
        };

        professionalsAdapter = new RecyclerViewAdapterBase(new RecyclerViewAdapterBase.IViewBuilder() {
            @Override
            public View build(int viewType) {
                return ProfessionalPaymentProjectItemView_.build(ConsumerProfessionalPaymentProjectsActivity.this);
            }
        }, projectSelectedListener);


        professionalsRecyclerView.setAdapter(professionalsAdapter);
        BaseItemAnimator animator = new jp.wasabeef.recyclerview.animators.FadeInAnimator();
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        professionalsRecyclerView.setItemAnimator(animator);
        professionalsRecyclerView.setLayoutManager(new LinearLayoutManager(ConsumerProfessionalPaymentProjectsActivity.this));

        initialize();
    }

    @Background
    void loadProjects(){
        presenter.loadProjects();
    }


    @Background
    void initialize(){
        presenter.initialize();
    }

    @Override
    public String getViewTitle() {
        return "CHOOSE PROJECT";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }

    @UiThread
    @Override
    public void showProfessionalProjects(List<PaymentProject> projects) {




        if(Lists.isNullOrEmpty(projects))
            multiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
        else
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);

        professionalsAdapter.setItems(projects);
    }

    @UiThread
    @Override
    public void showLoadingIndicator(boolean show) {
       if(show){
           emptyRefreshLayout.setRefreshing(true);
           professionalsSwipeRefreshLayout.setRefreshing(true);
       }else{
           emptyRefreshLayout.setRefreshing(false);
           professionalsSwipeRefreshLayout.setRefreshing(false);
       }
    }

    @UiThread
    @Override
    public void showLoadingProfessionalProjects(boolean isLoading) {
        if(isLoading){
            multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        }else {
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        }
    }
}
