package it.clipcall.consumer.payment.services;

import com.stripe.android.model.Token;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.payment.PaymentRequest;
import it.clipcall.consumer.payment.models.Payment;
import it.clipcall.consumer.payment.models.PaymentDetails;
import it.clipcall.consumer.payment.models.PaymentRef;
import it.clipcall.consumer.payment.proxies.IPaymentService;
import it.clipcall.consumer.projects.models.BonusInfo;
import it.clipcall.consumer.projects.models.CustomerPaymentMethod;
import it.clipcall.consumer.projects.services.ICustomerProjectsService;
import it.clipcall.infrastructure.ProcessPaymentResponse;
import it.clipcall.infrastructure.support.network.RetrofitCallProcessor;
import it.clipcall.professional.leads.models.Quote;
import retrofit2.Call;
import retrofit2.Response;

@Singleton
public class PaymentManager {

    private CustomerPaymentMethod lastPaymentMethod;
    private final IPaymentService paymentService;
    private final ICustomerProjectsService customerProjectsService;

    @Inject
    public PaymentManager(IPaymentService paymentService, ICustomerProjectsService customerProjectsService) {
        this.paymentService = paymentService;
        this.customerProjectsService = customerProjectsService;
    }

    public PaymentDetails processPaymentToken(String customerId, String leadAccountId, Payment payment, Token token, String name){
        try {

            PaymentRequest request = new PaymentRequest(payment, token, name);
            Response<ProcessPaymentResponse> response = paymentService.processPayment(customerId,leadAccountId, request).execute();
            if(response == null)
                return null;

            ProcessPaymentResponse statusResponse = response.body();
            if(statusResponse == null)
                return null;

            boolean success = "OK".equals(statusResponse.status.toUpperCase());
            if(success == false)
                return null;

            return statusResponse.paymentDetails;

        } catch (IOException e) {
            return null;
        }
    }

    public PaymentDetails processPaymentQuote(String customerId, String leadAccountId, Payment payment, Token token, String name, Quote quote){
        PaymentRequest request = new PaymentRequest(payment, token, name);
        Call<ProcessPaymentResponse> processPaymentCall = paymentService.processQuotePayment(customerId,leadAccountId,quote.getId(), request);
        ProcessPaymentResponse processPaymentResponse = RetrofitCallProcessor.processCall(processPaymentCall);
        if(processPaymentResponse == null || processPaymentResponse.status == null)
            return null;

        boolean success = "OK".equals(processPaymentResponse.status.toUpperCase());
        if(success == false)
            return null;

        return processPaymentResponse.paymentDetails;
    }


    public PaymentDetails rechargeCustomer(String customerId, String leadAccountId, Quote quote){
        Call<ProcessPaymentResponse> processPaymentCall = paymentService.rechargeCustomer(customerId,leadAccountId,quote.getId());
        ProcessPaymentResponse processPaymentResponse = RetrofitCallProcessor.processCall(processPaymentCall);
        if(processPaymentResponse == null || processPaymentResponse.status == null)
            return null;

        boolean success = "OK".equals(processPaymentResponse.status.toUpperCase());
        if(success == false)
            return null;

        return processPaymentResponse.paymentDetails;
    }


    public PaymentDetails getPaymentDetails(String customerId, String leadAccountId, String paymentId){
        try {
            Response<PaymentDetails> response = paymentService.getPaymentDetails(customerId,leadAccountId, paymentId).execute();
            if(response == null)
                return null;

            PaymentDetails paymentDetails = response.body();
            return paymentDetails;
        } catch (IOException e) {
            return null;
        }
    }

    public List<PaymentRef> getCustomerPayments(String customerId, String leadAccountId){
        try {
            Response<List<PaymentRef>> response = paymentService.getCustomerPayments(customerId,leadAccountId).execute();
            if(response == null)
                return null;

            List<PaymentRef> payments = response.body();
            if(payments == null)
                return new ArrayList<>();

            return payments;
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    public CustomerPaymentMethod getLastPaymentMethod(String customerId, String leadAccountId, Quote quote) {
        if( lastPaymentMethod != null)
            return lastPaymentMethod;

        Call<CustomerPaymentMethod> bookQuoteCall = customerProjectsService.bookQuote(customerId, leadAccountId, quote.getId());
        lastPaymentMethod = RetrofitCallProcessor.processCall(bookQuoteCall);
        return lastPaymentMethod;
    }

    public void setLastPaymentMethod(CustomerPaymentMethod lastPaymentMethod) {
        this.lastPaymentMethod = lastPaymentMethod;
    }

    public BonusInfo getBonusEntitled(String customerId, String leadAccountId) {
        Call<BonusInfo> bookQuoteCall = customerProjectsService.getBonusEntitled(customerId, leadAccountId);
        BonusInfo bonusInfo = RetrofitCallProcessor.processCall(bookQuoteCall);
        if(bonusInfo == null){
            bonusInfo = new BonusInfo();
            bonusInfo.setBonus(0);
            bonusInfo.setHasBonus(false);
        }
        return bonusInfo;
    }
}
