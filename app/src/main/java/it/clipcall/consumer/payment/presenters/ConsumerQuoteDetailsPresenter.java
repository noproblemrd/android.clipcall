package it.clipcall.consumer.payment.presenters;

import javax.inject.Inject;

import it.clipcall.common.chat.services.tasks.NavigateToChatTask;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.payment.controllers.ConsumerQuoteController;
import it.clipcall.consumer.payment.services.tasks.BookQuoteTask;
import it.clipcall.consumer.payment.viewModel.views.IConsumerQuoteDetailsView;
import it.clipcall.consumer.projects.models.CustomerPaymentMethod;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.professional.leads.models.Quote;

@PerActivity
public class ConsumerQuoteDetailsPresenter  extends PresenterBase<IConsumerQuoteDetailsView> {

    private final RouteParams routeParams;
    private final RoutingService routingService;
    private final EventAggregator eventAggregator;
    private final NavigateToChatTask navigateToChatTask;
    private final ConsumerQuoteController controller;

    private final BookQuoteTask bookQuoteTask;

    private Quote quote;

    Boolean routeBackToHome;
    private String leadAccountId;
    private ProjectEntityBase project;



    @Inject
    public ConsumerQuoteDetailsPresenter(RouteParams routeParams, RoutingService routingService, EventAggregator eventAggregator, NavigateToChatTask navigateToChatTask, ConsumerQuoteController controller, BookQuoteTask bookQuoteTask){
        this.routeParams = routeParams;
        this.routingService = routingService;
        this.eventAggregator = eventAggregator;
        this.navigateToChatTask = navigateToChatTask;
        this.controller = controller;
        this.bookQuoteTask = bookQuoteTask;
        this.quote = routeParams.getParam("quote");
        this.project = routeParams.getParam("project");
        this.leadAccountId = routeParams.getParam("leadAccountId");
        this.routeBackToHome = routeParams.getParam("routeBackToHome");
    }

    @Override
    public void initialize() {
        boolean isInProMode = controller.isInProMode();
        view.showQuote(project, quote, isInProMode);
    }

    public void bookQuote() {

        eventAggregator.publish(new ApplicationEvent(ConsumerEventNames.UserQuoteBookNow ,quote));

        view.showBookingInProgress(true);

        ProjectEntity consumerProject  = (ProjectEntity) project;
        ProfessionalRef professionalRef = Lists.firstOrDefault(consumerProject.getAdvertisers(), new ICriteria<ProfessionalRef>() {
            @Override
            public boolean isSatisfiedBy(ProfessionalRef candidate) {
                return candidate.getLeadAccountId().equals(leadAccountId);
            }
        });

        if(professionalRef == null){
            view.showBookingInProgress(false);
            return;
        }

        routeParams.setParam("professional", professionalRef);
        routeParams.setParam("quote", quote);
        routeParams.setParam("project", project);

        CustomerPaymentMethod lastPaymentMethod = controller.getLastPaymentMethod(professionalRef.getLeadAccountId(), quote);
        Route route;
        if(lastPaymentMethod != null && lastPaymentMethod.isActive()){
            routeParams.setParam("lastPaymentMethod", lastPaymentMethod);
            route = Routes.ConsumerPaymentDetailsRoute;;
        }else{
            route = Routes.ConsumerCreditCardDetailsdRoute;
        }

        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        routingService.routeTo(route, navigationContext);
        view.showBookingInProgress(false);
        //TODO - move code
        /**
        quote.setStatus(eQuoteStatus.Closed);
        quote.setStatusReason(eQuoteStatusReason.Booked);
        view.showBookingInProgress(false);
        boolean isInProMode = controller.isInProMode();
        view.showQuote(project, quote, isInProMode);
        view.showMessage("Thank you. Job booked!!");
         **/
    }

    public void navigateBack() {
        if(routeBackToHome != null && routeBackToHome.booleanValue())
        {
            routingService.routeToHome(view.getRoutingContext());
            return;
        }

        view.close();
    }

    public void navigateToChat() {
        ProjectEntity consumerProject  = (ProjectEntity) project;
        ProfessionalRef professionalRef = Lists.firstOrDefault(consumerProject.getAdvertisers(), new ICriteria<ProfessionalRef>() {
            @Override
            public boolean isSatisfiedBy(ProfessionalRef candidate) {
                return candidate.getLeadAccountId().equals(leadAccountId);
            }
        });

        if(professionalRef == null)
            return;

        eventAggregator.publish(new ApplicationEvent(ConsumerEventNames.UserQuoteChat ,quote));

        navigateToChatTask.execute(consumerProject,professionalRef,view.getRoutingContext());
    }
}
