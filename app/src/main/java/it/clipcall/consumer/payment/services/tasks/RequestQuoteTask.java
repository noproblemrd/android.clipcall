package it.clipcall.consumer.payment.services.tasks;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.common.chat.models.IConversation;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.common.chat.services.ChatParticipantFactory;
import it.clipcall.common.chat.support.factories.ChatMessageFactoryProvider;
import it.clipcall.consumer.profile.services.CustomerProfileManager;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.consumer.projects.services.ProjectsManager;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.infrastructure.tasks.ITask2;
import it.clipcall.professional.validation.services.AuthenticationManager;


@Singleton
public class RequestQuoteTask implements ITask2<ProjectEntity, ProfessionalRef, Boolean> {

    private final CustomerProfileManager profileManager;
    private final ChatManager chatManager;
    private final ProjectsManager projectsManager;
    private final AuthenticationManager authenticationManager;
    private final ChatParticipantFactory chatParticipantFactory;


    @Inject
    public RequestQuoteTask(CustomerProfileManager profileManager, ChatManager chatManager, ProjectsManager projectsManager, AuthenticationManager authenticationManager, ChatParticipantFactory chatParticipantFactory){

        this.profileManager = profileManager;
        this.chatManager = chatManager;
        this.projectsManager = projectsManager;
        this.authenticationManager = authenticationManager;
        this.chatParticipantFactory = chatParticipantFactory;
    }

    @Override
    public Boolean execute(ProjectEntity project, final ProfessionalRef professional) {
        String customerId = authenticationManager.getCustomerId();
        boolean success = projectsManager.requestQuote(customerId, professional.getLeadAccountId());
        if(!success){
            return Boolean.FALSE;
        }

        ProfessionalRef professionalRef = Lists.firstOrDefault(project.getAdvertisers(), new ICriteria<ProfessionalRef>() {
            @Override
            public boolean isSatisfiedBy(ProfessionalRef candidate) {
                return candidate.getLeadAccountId().equals(professional.getLeadAccountId());
            }
        });

        if(professionalRef == null)
            return Boolean.FALSE;

        professionalRef.setQuoteRequested(true);

        ChatParticipant sender = chatParticipantFactory.buildConsumerChatParticipant(project);
        ChatParticipant receiver =  chatParticipantFactory.buildProfessionalChatParticipant(professionalRef);

        IConversation conversationWithParticipant = chatManager.getConversationWithParticipant(sender, receiver, professionalRef.getLeadAccountId());
        if(conversationWithParticipant == null){
            conversationWithParticipant = chatManager.createConversation(sender,receiver, professional.getLeadAccountId());
        }

        if(conversationWithParticipant == null){
            return Boolean.FALSE;
        }

        String messageData = "Please send me a quote for my project";
        MessageDescription messageDescription = ChatMessageFactoryProvider.getInstance().createMessageDescription(MessageType.RequestQuote, messageData);
        chatManager.sendMessage(sender,receiver, professional.getLeadAccountId(), messageDescription);
        return Boolean.TRUE;
    }
}
