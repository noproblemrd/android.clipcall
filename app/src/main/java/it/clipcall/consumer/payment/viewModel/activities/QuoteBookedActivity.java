package it.clipcall.consumer.payment.viewModel.activities;

import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.payment.domainModel.services.QuoteDateFormatter;
import it.clipcall.consumer.payment.presenters.QuoteBookedPresenter;
import it.clipcall.consumer.payment.viewModel.views.IQuoteBookedView;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.converters.UsCurrencyFormatter;
import it.clipcall.infrastructure.resources.ResourceManager;
import it.clipcall.infrastructure.support.dates.DateFormatter;
import it.clipcall.professional.leads.models.Quote;

@EActivity(R.layout.quote_booked_activity)
public class QuoteBookedActivity extends BaseActivity implements IQuoteBookedView, IHasToolbar{


    @Inject
    UsCurrencyFormatter usCurrencyFormatter;

    @Inject
    QuoteBookedPresenter presenter;

    @Inject
    QuoteDateFormatter quoteDateFormatter;

    @Inject
    ResourceManager resourceManager;

    @ViewById
    Toolbar toolbar;

    @ViewById
    TextView notificationMessageTextView;

    @ViewById
    TextView quotePriceTextView;

    @ViewById
    TextView startOnTextView;




    @Override
    public String getViewTitle() {
        return "Quote Details";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        presenter.navigateBack();
        return true;
    }

    @Override
    public void onBackPressed() {
        onBackTapped();
    }

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        presenter.initialize();
    }

    @Override
    public void showBookedQuote(ProfessionalRef professional, Quote quote) {
        startOnTextView.setText(quoteDateFormatter.format(quote));
        quotePriceTextView.setText(usCurrencyFormatter.format(quote.getPrice()));
        String message = resourceManager.getString(R.string.quote_booked_notification_message,professional.getSupplierName());
        notificationMessageTextView.setText(message);
        toolbar.setTitle("Quote "+ String.format("%02d", quote.getNumber()));
        String subtitle = String.format("%s // %s", professional.getSupplierName(), DateFormatter.toLocalDate(quote.getCreatedOn()));
        toolbar.setSubtitle(subtitle);
    }
}