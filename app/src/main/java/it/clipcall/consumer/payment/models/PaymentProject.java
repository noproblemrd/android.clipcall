package it.clipcall.consumer.payment.models;

import com.google.common.base.Strings;

import java.util.Date;

/**
 * Created by dorona on 24/05/2016.
 */
public class PaymentProject {
    private String leadAccountId;
    private Date date;
    private String category;
    private String videoUrl;
    private String picPreviewUrl;
    private String zipcode;
    private String fullAddress;


    public PaymentProject(){

    }


    public String getLeadAccountId() {
        return leadAccountId;
    }

    public void setLeadAccountId(String leadAccountId) {
        this.leadAccountId = leadAccountId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getPicPreviewUrl() {
        return picPreviewUrl;
    }

    public void setPicPreviewUrl(String picPreviewUrl) {
        this.picPreviewUrl = picPreviewUrl;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getAddressOrZipCode(){
        String address = getFullAddress();
        if(!Strings.isNullOrEmpty(address))
            return address;

        return getZipcode();
    }
}
