package it.clipcall.consumer.payment.models;

public class PaymentProfessional {

    private String supplierId;
    private String supplierName;
    private String supplierImageUrl;
    private int myLove;
    private int allLove;

    public PaymentProfessional(){

    }

    public PaymentProfessional(String supplierId, String supplierName, String supplierImageUrl, int myLove, int allLove){
        this.supplierId = supplierId;
        this.supplierName = supplierName;
        this.supplierImageUrl = supplierImageUrl;
        this.myLove = myLove;
        this.allLove = allLove;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierImageUrl() {
        return supplierImageUrl;
    }

    public void setSupplierImageUrl(String supplierImageUrl) {
        this.supplierImageUrl = supplierImageUrl;
    }

    public int getMyLove() {
        return myLove;
    }

    public void setMyLove(int myLove) {
        this.myLove = myLove;
    }

    public int getAllLove() {
        return allLove;
    }

    public void setAllLove(int allLove) {
        this.allLove = allLove;
    }

}