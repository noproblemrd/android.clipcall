package it.clipcall.consumer.payment.controllers;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.payment.services.PaymentManager;
import it.clipcall.consumer.profile.models.CustomerProfile;
import it.clipcall.consumer.profile.services.CustomerProfileManager;
import it.clipcall.consumer.projects.models.CustomerPaymentMethod;
import it.clipcall.consumer.projects.services.ProjectsManager;
import it.clipcall.professional.leads.models.Quote;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class ConsumerQuoteController {

    private final PaymentManager paymentManager;
    private final AuthenticationManager authenticationManager;
    private final ProjectsManager projectsManager;
    private final CustomerProfileManager customerProfileManager;


    @Inject
    public ConsumerQuoteController(PaymentManager paymentManager, AuthenticationManager authenticationManager, ProjectsManager projectsManager, CustomerProfileManager customerProfileManager) {
        this.paymentManager = paymentManager;
        this.authenticationManager = authenticationManager;
        this.projectsManager = projectsManager;
        this.customerProfileManager = customerProfileManager;
    }




    public boolean isInProMode() {
        return authenticationManager.isInProfessionalMode();
    }

    public CustomerProfile getConsumerProfile() {
        String customerId = authenticationManager.getCustomerId();
        CustomerProfile profile =  customerProfileManager.getCustomerProfile(customerId);
        profile.setPhoneNumber(authenticationManager.getPhoneNumber());
        return profile;
    }

    public String getConsumerPhoneNumber() {
        return authenticationManager.getPhoneNumber();
    }

    public String getCustomerId() {
        return authenticationManager.getCustomerId();
    }

    public CustomerPaymentMethod getLastPaymentMethod(String leadAccountId, Quote quote) {
        String customerId = authenticationManager.getCustomerId();
        return paymentManager.getLastPaymentMethod(customerId, leadAccountId, quote);
    }

    public void setLastPaymentMethod(CustomerPaymentMethod lastPaymentMethod) {
        this.paymentManager.setLastPaymentMethod(lastPaymentMethod);
    }
}
