package it.clipcall.consumer.payment.viewModel.support;

import com.google.common.base.Strings;

import it.clipcall.R;
import it.clipcall.infrastructure.support.collections.IFunction;

/**
 * Created by dorona on 01/08/2016.
 */

public class CardResourceResolver implements IFunction<String, Integer> {
    @Override
    public Integer apply(String input) {
        if(Strings.isNullOrEmpty(input))
            return R.drawable.visa_card;

        String upperCaseInput = input.toUpperCase();
        if(upperCaseInput.equals("VISA")){
            return R.drawable.visa_card;
        }else if(upperCaseInput.equals("MASTERCARD")){
            return R.drawable.mastercard_card;
        }else if(upperCaseInput.equals("AMERICAN EXPRESS")){
            return R.drawable.american_express_card;
        }else if(upperCaseInput.equals("DISCOVER")){
            return R.drawable.discover_card;
        }
        else if(upperCaseInput.equals("JCB")){
            return R.drawable.jcb_card;
        }
        else if(upperCaseInput.equals("DINERS CLUB")){
            return R.drawable.diners_card;
        }
        return R.drawable.visa_card;
    }
}
