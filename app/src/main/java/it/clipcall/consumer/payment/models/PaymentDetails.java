package it.clipcall.consumer.payment.models;

import org.parceler.Parcel;

import java.util.Date;

/**
 * Created by dorona on 22/02/2016.
 */
@Parcel
public class PaymentDetails {

    public double amount;
    public String id;
    public String cardType;
    public Date createDate;
    public String receiptNumber;
    public String description;
    public String customerName;
    public String customerPhoneNumber;
    public String last4Digits;
}