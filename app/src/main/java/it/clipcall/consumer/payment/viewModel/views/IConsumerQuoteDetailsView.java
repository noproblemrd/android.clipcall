package it.clipcall.consumer.payment.viewModel.views;

import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.leads.models.Quote;

/**
 * Created by dorona on 16/03/2016.
 */
public interface IConsumerQuoteDetailsView extends IView {


    void showQuote(ProjectEntityBase project, Quote quote, boolean isProMode);

    void showBookingInProgress(boolean isInProgress);

    void showMessage(String message);

    void close();
}
