package it.clipcall.consumer.payment.presenters;

import android.os.Bundle;

import org.simple.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.consumer.payment.controllers.PaymentCoordinator;
import it.clipcall.consumer.payment.models.PaymentProfessional;
import it.clipcall.consumer.payment.models.PaymentProject;
import it.clipcall.consumer.payment.views.IConsumerPayYourProView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;

@PerActivity
public class ConsumerPayYourProPresenter extends PresenterBase<IConsumerPayYourProView> {

    private final RouteParams routeParams;
    private final RoutingService routingService;
    private final PaymentCoordinator paymentCoordinator;

    @Inject
    public ConsumerPayYourProPresenter(PaymentCoordinator paymentCoordinator, RouteParams routeParams,  RoutingService routingService) {
        this.paymentCoordinator = paymentCoordinator;
        this.routeParams = routeParams;
        this.routingService = routingService;
    }

    @Override
    public void initialize() {
        List<PaymentProfessional> professionals = routeParams.getParam("professionals");
        view.showProfessionals(professionals);

    }

    public void navigateToTermsOfService(){
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        routingService.routeTo(Routes.ConsumerTermsOfServiceRoute, navigationContext);
    }

    public void navigateToBuyerProtection() {
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        routingService.routeTo(Routes.ConsumerBuyerProtectionRoute, navigationContext);
    }

    public void payPro() {

    }

    public void professionalSelected(PaymentProfessional professional, Bundle uiData) {
        //view.showLoadingProfessionalProjects(true);
        List<PaymentProject> projectsOfProfessional = paymentCoordinator.getProjectsOfProfessional(professional);
        view.onProjectsRetrieved(professional,projectsOfProfessional,uiData);
    }

    public void loadProfessionals() {
        view.showLoadingIndicator(true);
        List<PaymentProfessional> consumerProfessionals = paymentCoordinator.getConsumerProfessionals();
        view.showProfessionals(consumerProfessionals);
        view.showLoadingIndicator(false);
    }

    public void navigateToProfessionalProjects(PaymentProfessional professional, List<PaymentProject> projectsOfProfessional, Bundle uiData) {
        routeParams.reset();
        routeParams.setParam("professional",professional);
        routeParams.setParam("professionalProjects", projectsOfProfessional);
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.uiBundle = uiData;
        routingService.routeTo(Routes.ConsumerProfessionalPaymentProjectsRoute, navigationContext);
        //view.showLoadingProfessionalProjects(false);
    }

    public void createNewProjectTapped() {
        EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.CREATE_NEW_PROJECT));

        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();
        routingService.routeTo(Routes.ConsumerFindProRoute, navigationContext);
    }
}
