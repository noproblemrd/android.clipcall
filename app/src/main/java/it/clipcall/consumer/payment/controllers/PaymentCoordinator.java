package it.clipcall.consumer.payment.controllers;


import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.consumer.payment.models.PaymentProfessional;
import it.clipcall.consumer.payment.models.PaymentProject;
import it.clipcall.consumer.projects.services.ProjectsManager;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class PaymentCoordinator {

    private final ProjectsManager projectsManager;
    private final AuthenticationManager authenticationManager;
    private final EventAggregator eventAggregator;

    @Inject
    public PaymentCoordinator(ProjectsManager projectsManager, AuthenticationManager authenticationManager, EventAggregator eventAggregator){

        this.projectsManager = projectsManager;
        this.authenticationManager = authenticationManager;
        this.eventAggregator = eventAggregator;
    }


    public List<PaymentProfessional> getConsumerProfessionals(){
        String userId = authenticationManager.getUserId();
        if(Strings.isNullOrEmpty(userId))
            return new ArrayList<>();

        List<PaymentProfessional> professionals = projectsManager.getConsumerProfessionals(userId);
        return professionals;
    }

    public boolean isDiscountClaimed(){
        String userId = authenticationManager.getUserId();
        if(Strings.isNullOrEmpty(userId))
            return false;
        boolean isDiscountClaimed = projectsManager.isDiscountClaimed(userId);
        return isDiscountClaimed;
    }


    public boolean claimDiscount(boolean beforePhoneVerification){
        String userId = authenticationManager.getUserId();
        if(Strings.isNullOrEmpty(userId))
            return false;
        boolean isDiscountClaimed = projectsManager.claimDiscount(userId);
       if(isDiscountClaimed){
           Map<String,Object> properties = new HashMap<>();
           properties.put("_APP_ISCLAIMED", isDiscountClaimed);
           properties.put("_APP_ISCLAIMEDBEFOREREGISTRATION", beforePhoneVerification);
           eventAggregator.publish(new ApplicationEvent(ConsumerEventNames.ClaimBonus,properties));
       }
        return isDiscountClaimed;
    }

    public List<PaymentProject> getProjectsOfProfessional(PaymentProfessional professional){
        String userId = authenticationManager.getUserId();
        if(Strings.isNullOrEmpty(userId))
            return new ArrayList<>();

        List<PaymentProject> professionalProjects = projectsManager.getProjectsOfProfessional(userId, professional);
        return professionalProjects;

    }


}
