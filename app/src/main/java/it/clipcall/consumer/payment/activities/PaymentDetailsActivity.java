package it.clipcall.consumer.payment.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.payment.presenters.PaymentDetailsPresenter;
import it.clipcall.consumer.projects.models.CustomerPaymentMethod;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.views.IPaymentDetailsView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.converters.UsCurrencyFormatter;
import it.clipcall.infrastructure.support.dates.DateFormatter;
import it.clipcall.infrastructure.ui.dialogs.SnackbarService;
import it.clipcall.professional.leads.models.Quote;

@EActivity(R.layout.payment_details_activity)
public class PaymentDetailsActivity extends BaseActivity implements IHasToolbar, IPaymentDetailsView {


    @Inject
    UsCurrencyFormatter usCurrencyFormatter;

    @Inject
    SnackbarService snackbarService;

    @ViewById
    Toolbar toolbar;

    @ViewById
    TextView creditCardLast4DigitsTextView;

    @ViewById
    Button payProButton;

    @ViewById
    ProgressBar payProProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @ViewById
    CoordinatorLayout root;

    @Inject
    PaymentDetailsPresenter presenter;

    @ViewById
    ImageView creditCardImageView;


    @Click(R.id.eulaTextView)
    void eulaTapped(){
        presenter.navigateToEula();
    }

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        presenter.initialize();
    }

    @Override
    public String getViewTitle() {
        return "CREDIT CARD DETAILS";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }


    @Click(R.id.payProButton)
    public void payTheProTapped(){

        presenter.payThePro();
    }

    @Click(R.id.changePaymentMethodButton)
    void onChangePaymentMethodTapped(){
        presenter.changePaymentMethod();
    }

    @Override
    public void showQuote(Quote quote, ProfessionalRef professional, CustomerPaymentMethod lastPaymentMethod) {
      /*  String formattedPrice = usCurrencyFormatter.format(quote.getPrice());
        chargeAmountTextView.setText(formattedPrice);
        if(professionalAccount != null){
            String proText = String.format("You are sending %s", professionalAccount.getProfessionalName());
            sendingProTextView.setText(proText);
        }*/

        creditCardLast4DigitsTextView.setText(lastPaymentMethod.getLast4());

        toolbar.setTitle("Quote "+ String.format("%02d", quote.getNumber()));
        String subtitle = String.format("%s // %s", professional.getSupplierName(), DateFormatter.toLocalDate(quote.getCreatedOn()));
        toolbar.setSubtitle(subtitle);


        String cardType = lastPaymentMethod.getBrand();
        if(Strings.isNullOrEmpty(cardType))
            return;


        if(cardType.toUpperCase().equals("VISA")){
            Picasso.with(this).load(R.drawable.visa_card).into(creditCardImageView);
        }else if(cardType.toUpperCase().equals("MASTERCARD")){
            Picasso.with(this).load(R.drawable.mastercard_card).into(creditCardImageView);
        }else if(cardType.toUpperCase().equals("AMERICAN EXPRESS")){
            Picasso.with(this).load(R.drawable.american_express_card).into(creditCardImageView);
        }else if(cardType.toUpperCase().equals("DISCOVER")){
            Picasso.with(this).load(R.drawable.discover_card).into(creditCardImageView);
        }
        else if(cardType.toUpperCase().equals("JCB")){
            Picasso.with(this).load(R.drawable.jcb_card).into(creditCardImageView);
        }
        else if(cardType.toUpperCase().equals("DINERS CLUB")){
            Picasso.with(this).load(R.drawable.diners_card).into(creditCardImageView);
        }


    }

    @Override
    public void showFailureMessage(String message) {
        snackbarService.show(root, message, Snackbar.LENGTH_LONG);
    }

    public void setProcessingPaymentRequest(boolean isProcessing){
        if(isProcessing){
            payProButton.setVisibility(View.GONE);
            payProProgressBar.setVisibility(View.VISIBLE);
        }else{
            payProButton.setVisibility(View.VISIBLE);
            payProProgressBar.setVisibility(View.GONE);
        }
    }

}
