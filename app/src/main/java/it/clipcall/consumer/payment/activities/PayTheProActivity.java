package it.clipcall.consumer.payment.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.google.common.base.Strings;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;

import java.text.NumberFormat;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.environment.Environment;
import it.clipcall.consumer.payment.presenters.PayTheProPresenter;
import it.clipcall.consumer.projects.models.ProfessionalAccount;
import it.clipcall.consumer.projects.views.IBuyTheProView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.resources.ResourceManager;

@EActivity(R.layout.pay_the_pro_activity)
public class PayTheProActivity extends BaseActivity implements IHasToolbar, IBuyTheProView,  GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @ViewById
    Toolbar toolbar;

    @Inject
    ResourceManager resourceManager;

    @Inject
    PayTheProPresenter presenter;

    @ViewById
    Button androidPayButton;

    @ViewById
    TextView advertiserNameTextView;

    @ViewById
    EditText amountEditText;

    @ViewById
    TextView entitledBonusTextView;

    @Click(R.id.termsOfServiceTextView)
    void onTermsOfServiceTapped(){
        presenter.navigateToEula();
    }


    private GoogleApiClient googleApiClient;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wallet.API, new Wallet.WalletOptions.Builder()
                        .setEnvironment(Environment.WalletEnvironment)
                        .setTheme(WalletConstants.THEME_LIGHT)
                        .build())
                .build();
    }




    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        PendingResult<BooleanResult> readyToPay = Wallet.Payments.isReadyToPay(googleApiClient);
        readyToPay.setResultCallback(
                new ResultCallback<BooleanResult>() {
                    @Override
                    public void onResult(@NonNull BooleanResult booleanResult) {
                        if (booleanResult.getStatus().isSuccess()) {
                            if (!booleanResult.getValue()) {
                                androidPayButton.setEnabled(false);
                            }
                        } else {
                            androidPayButton.setEnabled(false);
                        }
                    }
                });

        presenter.initialize();
    }

    @Override
    public String getViewTitle() {
        return "PAY THE PRO";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }

    @Click(R.id.androidPayButton)
    void onPayWithAndroidPayTapped(){
        presenter.payWithAndroidPay(amountEditText.getText()+"");
    }


    @Click(R.id.payWithCreditCardButton)
    void onPayWithCreditCardTapped(){
        presenter.payWithCreditCard(amountEditText.getText()+"");
    }


    @TextChange(R.id.amountEditText)
    void creditCardTextChange(CharSequence text, TextView hello, int before, int start, int count){
        if(text == null)
            return;

        if(Strings.isNullOrEmpty(text.toString())){
            presenter.setChargeAmount(0);
            return;
        }


        try{
            double amount = Double.parseDouble(text.toString());
            presenter.setChargeAmount(amount);
        }
        catch (NumberFormatException e){
            presenter.setChargeAmount(0);
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("Android pay", "onConnected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("Android pay", "onConnectionSuspended"+ i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("Android pay", "onConnectionFailed"+ connectionResult.toString());
    }

    @Override
    public void showInvalidAmount() {
        amountEditText.setError("Amount must be at least 50 cents");
    }

    @Override
    public void showPayedProfessional(ProfessionalAccount professionalAccount) {
        advertiserNameTextView.setText(professionalAccount.getProfessionalName());
    }

    @Override
    public void showBonus(double bonusAmount) {
        entitledBonusTextView.setVisibility(View.VISIBLE);
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        String amount = formatter.format(bonusAmount);
        String message = resourceManager.getString(R.string.payment_bonus_entitled, amount);
        entitledBonusTextView.setText(message);
    }
}
