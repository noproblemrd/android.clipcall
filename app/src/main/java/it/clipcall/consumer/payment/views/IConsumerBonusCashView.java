package it.clipcall.consumer.payment.views;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by micro on 1/20/2016.
 */
public interface IConsumerBonusCashView extends IView {


    void showClaimeDiscountRequestInProgress(boolean isInProgress);

    void showDiscountClaimed(boolean isDiscountClaimed);

}
