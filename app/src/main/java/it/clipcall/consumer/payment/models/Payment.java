package it.clipcall.consumer.payment.models;

/**
 * Created by dorona on 18/02/2016.
 */
public class Payment {

    private double amount;

    public String getReceiptEmail() {
        return receiptEmail;
    }

    public void setReceiptEmail(String receiptEmail) {
        this.receiptEmail = receiptEmail;
    }

    private String receiptEmail;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
