package it.clipcall.consumer.payment.presenters;

import javax.inject.Inject;

import it.clipcall.consumer.payment.controllers.CustomerCreditCardController;
import it.clipcall.consumer.payment.models.PaymentDetails;
import it.clipcall.consumer.payment.services.tasks.BookQuoteTask;
import it.clipcall.consumer.projects.models.CustomerPaymentMethod;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.consumer.projects.views.IPaymentDetailsView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.professional.leads.models.Quote;

@PerActivity
public class PaymentDetailsPresenter extends PresenterBase<IPaymentDetailsView> {

    private final CustomerCreditCardController controller;
    private final RoutingService routingService;
    private final RouteParams routeParams;
    private final BookQuoteTask bookQuoteTask;

    private Boolean routeToHome;
    private ProfessionalRef professional;
    private Quote quote;
    private CustomerPaymentMethod lastPaymentMethod;
    private ProjectEntity project;

    @Inject
    public PaymentDetailsPresenter(CustomerCreditCardController controller, RoutingService routingService, RouteParams routeParams, BookQuoteTask bookQuoteTask) {
        this.controller = controller;
        this.routingService = routingService;
        this.routeParams = routeParams;
        this.professional = routeParams.getParam("professional");
        this.quote = routeParams.getParam("quote");
        this.routeToHome = routeParams.getParam("routeToHome");
        this.lastPaymentMethod = routeParams.getParam("lastPaymentMethod");
        this.project = routeParams.getParam("project");
        this.bookQuoteTask = bookQuoteTask;
    }

    @Override
    public void initialize() {
        view.showQuote(quote, professional, lastPaymentMethod);
    }


    public void payThePro() {
        view.setProcessingPaymentRequest(true);
        PaymentDetails paymentDetails = controller.rechargeCustomer(professional, quote);
        view.setProcessingPaymentRequest(false);
        if(paymentDetails == null){
            view.showFailureMessage("Payment was declined.");
            return;
        }

        bookQuoteTask.execute(project, professional, quote);

        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        routingService.routeTo(Routes.ConsumerQuoteBookedRoute,navigationContext);
        return;

   }
    public void navigateToEula() {
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.addParameter("url","https://storage.googleapis.com/clipcallterms/ClipCall_EULA.pdf");
        navigationContext.addParameter("title","EULA");
        routingService.routeTo(Routes.ConsumerTermsOfServiceRoute, navigationContext);
    }

    public void changePaymentMethod() {
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        routingService.routeTo(Routes.ConsumerCreditCardDetailsdRoute,navigationContext);
    }
}
