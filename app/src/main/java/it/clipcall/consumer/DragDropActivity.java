package it.clipcall.consumer;

import android.animation.ObjectAnimator;
import android.content.ClipData;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.CycleInterpolator;
import android.widget.ImageView;

import it.clipcall.R;

public class DragDropActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drag_drop_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        final ImageView sourceView = (ImageView) findViewById(R.id.sourceView);


        sourceView.setOnLongClickListener(new View.OnLongClickListener() {

            private boolean mDragInProgress;

            @Override
            public boolean onLongClick(View v) {
                ClipData data =
                        ClipData.newPlainText("DragData", (String)v.getTag());
                mDragInProgress =
                        v.startDrag(data, new View.DragShadowBuilder(v),
                                (Object)v, 0);
                Log.v((String) v.getTag(),
                        "starting drag? " + mDragInProgress);
                return true;
            }
        });

        sourceView.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                String dotTAG = "TAG";
// get event values to work with
                int action = event.getAction();
                float x = event.getX();
                float y = event.getY();

                switch(action) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        Log.v(dotTAG, "drag started. X: " + x + ", Y: " + y);
                        break;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        //sourceView.setX(x);
                        //sourceView.setY(y);
                        Log.v(dotTAG, "drag proceeding… At: " + x + ", " + y);
                        break;
                    case DragEvent.ACTION_DRAG_ENTERED:
                        Log.v(dotTAG, "drag entered. At: " + x + ", " + y);
                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        Log.v(dotTAG, "drag exited. At: " + x + ", " + y);
                        break;
                    case DragEvent.ACTION_DROP:
                        Log.v(dotTAG, "drag dropped. At: " + x + ", " + y);
                        break;
                    case DragEvent.ACTION_DRAG_ENDED:
                        Log.v(dotTAG, "drag ended. Success? " + event.getResult());
                        break;
                    default:
                        Log.v(dotTAG, "some other drag action: " + action);
                        break;
                }
                return true;
            }
        });

        ViewGroup destinationView = (ViewGroup)findViewById(R.id.destinationView);
        destinationView.setOnDragListener(new View.OnDragListener() {

                                              private static final String DROPTAG = "DropTarget";
                                              private int dropCount = 0;
                                              private ObjectAnimator anim;


                                              @Override
                                              public boolean onDrag(View v, DragEvent event) {
                                                  int action = event.getAction();
                                                  boolean result = true;
                                                  switch (action) {
                                                      case DragEvent.ACTION_DRAG_STARTED:
                                                          Log.v(DROPTAG, "drag started in dropTarget");
                                                          break;
                                                      case DragEvent.ACTION_DRAG_ENTERED:
                                                          Log.v(DROPTAG, "drag entered dropTarget");
                                                          anim = ObjectAnimator.ofFloat(
                                                                  (Object) v, "alpha", 1f, 0.5f);
                                                          anim.setInterpolator(new CycleInterpolator(40));
                                                          anim.setDuration(30 * 1000); // 30 seconds
                                                          anim.start();
                                                          break;

                                                      case DragEvent.ACTION_DRAG_EXITED:
                                                          Log.v(DROPTAG, "drag exited dropTarget");
                                                          if (anim != null) {
                                                              anim.end();
                                                              anim = null;
                                                          }
                                                          break;
                                                      case DragEvent.ACTION_DRAG_LOCATION:
                                                          Log.v(DROPTAG, "drag proceeding in dropTarget: " +
                                                                  event.getX() + ", " + event.getY());
                                                          break;
                                                      case DragEvent.ACTION_DROP:
                                                          Log.v(DROPTAG, "drag drop in dropTarget");
                                                          if (anim != null) {
                                                              anim.end();
                                                              anim = null;
                                                          }
                                                          ClipData data = event.getClipData();
                                                          Log.v(DROPTAG, "Item data is " +
                                                                  data.getItemAt(0).getText());
                                                          dropCount++;
                                                          String message = dropCount + " drop";
                                                          if (dropCount > 1)
                                                              message += "s";

                                                          break;
                                                      case DragEvent.ACTION_DRAG_ENDED:
                                                          Log.v(DROPTAG, "drag ended in dropTarget");
                                                          if (anim != null) {
                                                              anim.end();
                                                              anim = null;
                                                          }
                                                          break;
                                                      default:
                                                          Log.v(DROPTAG, "other action in dropzone: " +
                                                                  action);
                                                          result = false;
                                                  }
                                                  return result;
                                              }
                                          }

        );

            setSupportActionBar(toolbar);

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener()

                                   {
                                       @Override
                                       public void onClick(View view) {
                                           Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                                                   .setAction("Action", null).show();
                                       }
                                   }

            );
        }

    }
