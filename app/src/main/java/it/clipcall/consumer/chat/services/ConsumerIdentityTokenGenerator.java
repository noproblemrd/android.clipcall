package it.clipcall.consumer.chat.services;

import it.clipcall.common.chat.services.IIdentityTokenGenerator;
import it.clipcall.consumer.chat.services.IChatService;
import it.clipcall.consumer.projects.models.createChatTokenRequest;
import retrofit2.Response;

/**
 * Created by dorona on 01/03/2016.
 */
public class ConsumerIdentityTokenGenerator implements IIdentityTokenGenerator {

    private final IChatService chatService;

    public ConsumerIdentityTokenGenerator(IChatService chatService){
        this.chatService = chatService;
    }


    @Override
    public String generateIdentityToken(String userId, String nonce) {

        createChatTokenRequest request = new createChatTokenRequest();
        request.nonce = nonce;

        try {
            Response<String> response = chatService.createChatToken(userId, request).execute();
            if(response == null)
                return null;

            String token = response.body();
            return token;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}
