package it.clipcall.consumer.social.support;

import android.app.Activity;
import android.content.Intent;

import it.clipcall.R;
import me.kentin.yeti.listener.OnShareListener;

/**
 * Created by dorona on 19/01/2016.
 */
public class ShareWithFriendsListener extends OnShareListener {


    private final Activity parentActivity;

    public ShareWithFriendsListener(Activity parentActivity) {
        this.parentActivity = parentActivity;
    }

    @Override
    public boolean shareWithFacebook(Intent intent) {
        return false;
    }

    @Override
    public boolean shareWithTwitter(Intent intent) {

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.removeExtra(Intent.EXTRA_TEXT);
        intent.putExtra(Intent.EXTRA_TEXT, "BOOM this is what's actually going to be shared.");

        parentActivity.startActivity(intent);
        return true; // true = you have changed the intent, prevent the system from firing the old intent
    }

    @Override
    public boolean shareWithGooglePlus(Intent intent) {
        return false; // false = let the system handle it the usual way
    }

    @Override
    public boolean shareWithEmail(Intent intent) {
        String emailMessage = parentActivity.getString(R.string.share_with_friends_email);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.removeExtra(Intent.EXTRA_TEXT);
        intent.putExtra(Intent.EXTRA_TEXT, emailMessage);
        intent.putExtra(Intent.EXTRA_SUBJECT,"You've got to try ClipCall");

        parentActivity.startActivity(intent);


        return true;
    }

    @Override
    public boolean shareWithSms(Intent intent) {

        String smsMessage = parentActivity.getString(R.string.share_with_friends_sms);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.removeExtra(Intent.EXTRA_TEXT);
        intent.putExtra(Intent.EXTRA_TEXT, smsMessage);
        intent.putExtra(Intent.EXTRA_TITLE, "You've got to try ClipCall");

        parentActivity.startActivity(intent);


        return true;
    }

    @Override
    public boolean shareWithOther(Intent intent) {
        return false;
    }
}
