package it.clipcall.consumer.settings.presenters;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.profile.models.CustomerProfile;
import it.clipcall.consumer.profile.models.CustomerSettings;
import it.clipcall.consumer.settings.controllers.CustomerSettingsController;
import it.clipcall.consumer.settings.views.ICustomerSettingsView;
import it.clipcall.infrastructure.presenters.PresenterBase;

/**
 * Created by dorona on 17/01/2016.
 */
public class CustomerSettingsPresenter extends PresenterBase<ICustomerSettingsView> {


    private final CustomerSettingsController controller;

    @Inject
    public CustomerSettingsPresenter(CustomerSettingsController controller) {
        this.controller = controller;
    }

    @Override
    public void initialize() {
        super.initialize();
        CustomerSettings settings = controller.getCustomerSettings();
        if(view == null)
            return;

        if(settings != null)
        {
            view.setRecordMyCalls(settings.isRecordMyCalls());
        }
    }

    public void updateRecordMyCalls(boolean shouldRecordMyCalls){

        boolean updated = controller.updateRecordMyCalls(shouldRecordMyCalls);
        if(updated){
            view.setRecordMyCalls(shouldRecordMyCalls);
        }
    }
}
