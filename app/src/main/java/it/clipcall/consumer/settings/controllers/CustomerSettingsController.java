package it.clipcall.consumer.settings.controllers;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.consumer.profile.models.CustomerSettings;
import it.clipcall.consumer.profile.services.CustomerProfileManager;

/**
 * Created by dorona on 17/01/2016.
 */
@Singleton
public class CustomerSettingsController {

    private final AuthenticationManager authenticationManager;
    private final CustomerProfileManager profileManager;

    @Inject
    public CustomerSettingsController(AuthenticationManager authenticationManager, CustomerProfileManager profileManager) {
        this.authenticationManager = authenticationManager;
        this.profileManager = profileManager;
    }


    public CustomerSettings getCustomerSettings(){
        if(!authenticationManager.isCustomerAuthenticated())
            return null;

        String customerId = authenticationManager.getCustomerId();
        CustomerSettings settings = profileManager.getCustomerSettings(customerId);
        return settings;
    }

    public boolean updateRecordMyCalls(boolean recordMyCalls) {
        if(!authenticationManager.isCustomerAuthenticated())
            return false;

        String customerId = authenticationManager.getCustomerId();
        CustomerSettings settings = new CustomerSettings();
        settings.setRecordMyCalls(recordMyCalls);

        boolean updatedSuccessfully = profileManager.updateCustomerSettings(customerId,settings);
        return updatedSuccessfully;

    }
}
