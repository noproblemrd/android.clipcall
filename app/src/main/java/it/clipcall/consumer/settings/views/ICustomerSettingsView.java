package it.clipcall.consumer.settings.views;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 17/01/2016.
 */
public interface ICustomerSettingsView extends IView {

    void setRecordMyCalls(boolean shouldRecordMyCalls);
}
