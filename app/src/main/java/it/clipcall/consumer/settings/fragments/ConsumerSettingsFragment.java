package it.clipcall.consumer.settings.fragments;

import android.widget.CompoundButton;
import android.widget.Switch;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.settings.presenters.CustomerSettingsPresenter;
import it.clipcall.consumer.settings.views.ICustomerSettingsView;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.routing.models.RoutingContext;

@EFragment(R.layout.consumer_fragment_settings)
public class ConsumerSettingsFragment extends BaseFragment implements ICustomerSettingsView {

    @Inject
    CustomerSettingsPresenter presenter;

    @ViewById
    Switch recordMyCallsSwitch;

    @AfterViews
    void afterAboutViewLoaded(){
        presenter.bindView(this);
        initialize();
    }

    @Background
    void initialize(){
        presenter.initialize();
    }


    @Override
    public void setRecordMyCalls(boolean shouldRecordMyCalls) {
        recordMyCallsSwitch.setChecked(shouldRecordMyCalls);
    }

    @Override
    public RoutingContext getRoutingContext() {
        return null;
    }

    @CheckedChange(R.id.recordMyCallsSwitch)
    void toggleRecordMyCalls(CompoundButton recordMyCallSwitch, boolean isChecked){
        updateRecordMyCalls(isChecked);
    }

    @Background
    void updateRecordMyCalls(boolean shouldRecordMyCalls){
        presenter.updateRecordMyCalls(shouldRecordMyCalls);
    }

    @Override
    protected String getTitle() {
        return "Settings";
    }

    @Override
    protected int getMenuItemId() {
        return R.id.nav_settings_consumer;
    }
}
