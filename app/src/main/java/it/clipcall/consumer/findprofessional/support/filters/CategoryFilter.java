package it.clipcall.consumer.findprofessional.support.filters;


import android.widget.ArrayAdapter;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import it.clipcall.professional.profile.models.Category;

public class CategoryFilter extends Filter {

    private final ArrayAdapter<Category> adapter;
    private final List<Category> availableCategories;

    public CategoryFilter(ArrayAdapter<Category> adapter, List<Category> availableCategories){
        this.adapter = adapter;
        this.availableCategories = availableCategories;
    }


    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        final FilterResults filterResults = new FilterResults();

        if(constraint == null)
        {
            filterResults.values = new ArrayList<>( availableCategories);
            filterResults.count =  availableCategories.size();
            return filterResults;
        }

        String value = (String)constraint;
        String loweredValue = value.toLowerCase();



        Set<Category> categoryNamesStartingWithPrefix = new HashSet<>();
        Set<Category> categoryValuesStartingWithPrefix = new HashSet<>();
        Set<Category> categoriesNotStartingWithPrefix = new HashSet<>();

        for(Category c: availableCategories)
        {
            String loweredCategoryName = c.getCategoryName().toLowerCase();
            String loweredKeywordName = c.getValue().toLowerCase();
            if(loweredCategoryName.startsWith(loweredValue))
            {
                categoryNamesStartingWithPrefix.add(c);
            }
            else if (loweredKeywordName.startsWith(loweredValue))
            {
                categoryValuesStartingWithPrefix.add(c);
            }

            else if(loweredCategoryName.contains(loweredValue) || loweredKeywordName.contains(loweredValue)){
                categoriesNotStartingWithPrefix.add(c);
            }
        }
        CategoryByNameComparator comparator = new CategoryByNameComparator(loweredValue);
        List<Category> categoryWithNameStartingWithPrefix = asSortedList(categoryNamesStartingWithPrefix, comparator);


        List<Category> result = new ArrayList<>();
        result.addAll(categoryWithNameStartingWithPrefix);
        result.addAll(categoryValuesStartingWithPrefix);
        result.addAll(categoriesNotStartingWithPrefix);

        filterResults.values = result;
        filterResults.count = result.size();

        return filterResults;
    }

    public static <T> List<T> asSortedList(Collection<T> c, Comparator<T> comparator) {
        List<T> list = new ArrayList<T>(c);
        java.util.Collections.sort(list, comparator);
        return list;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.clear();
        if(results.count == 0)
        {
            adapter.notifyDataSetInvalidated();
            return;
        }

        if(results.values instanceof Set<?>){
            adapter.addAll((Set<Category>) results.values);
        }else{
            adapter.addAll((List<Category>) results.values);
        }


        if (results.count > 0) {
            adapter.notifyDataSetChanged();
        } else {
            adapter.notifyDataSetInvalidated();
        }
    }

    @Override
    public CharSequence convertResultToString(final Object resultValue) {
        return resultValue == null ? "" : ((Category) resultValue).getCategoryName();
    }



}
