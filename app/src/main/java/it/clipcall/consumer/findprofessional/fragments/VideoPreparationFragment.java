package it.clipcall.consumer.findprofessional.fragments;

import android.view.ViewGroup;
import android.widget.TextView;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.simple.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

import it.clipcall.R;

@EFragment(R.layout.video_preparation_fragment)
public class VideoPreparationFragment extends VideoRecordingBaseFragment {

    @ViewById
    CircularProgressBar downCounterCircularProgressBar;

    @ViewById
    TextView countDownTextView;

    @ViewById
    ViewGroup countDownContainer;

    private   Timer timer;
    private int countDownCounter = 3;


    @AfterViews
    void init(){
        int animationDuration = 3100; // 2500ms = 2,5s
        timer = new Timer();
        countDownCounter = 3;

        downCounterCircularProgressBar.setProgressWithAnimation(100, animationDuration);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (countDownCounter < 0) {
                    releaseTimer();
                    ///EventBus.getDefault().post(VideoPreparationFragment.this,"countDownCompleted");
                    listener.onRecordingCountDownCompleted();
                    return;
                }

                updateCountDownCounter(countDownCounter);
                countDownCounter--;

            }
        }, 0, 1000);
    }


    @UiThread
    public void updateCountDownCounter(int countDownCounter) {
        countDownTextView.setText(countDownCounter + "");
    }

    void releaseTimer(){
        if(timer != null)
        {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public boolean canHandleBackPressed() {
        return true;
    }

    @Override
    public void handleBackPressed() {

    }
}