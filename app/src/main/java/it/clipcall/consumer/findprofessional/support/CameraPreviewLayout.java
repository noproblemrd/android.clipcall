package it.clipcall.consumer.findprofessional.support;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.io.IOException;
import java.util.List;

/**
 * A simple wrapper around a Camera and a SurfaceView that renders a centered preview of the Camera
 * to the surface. We need to center the SurfaceView because not all devices have cameras that
 * support preview sizes at the same aspect ratio as the device's display.
 */
public class CameraPreviewLayout extends FrameLayout implements SurfaceHolder.Callback
{
    private final String TAG = "Preview";

    public SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;
    Camera.Size previewSize;
    List<Camera.Size> supportedPreviewSizes;
    private  Camera camera;

    public CameraPreviewLayout(Context context)
    {
        super(context);

        surfaceView = new SurfaceView(context);
        addView(surfaceView);

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

    }

    public void setCamera(Camera camera)
    {
        this.camera = camera;
        if (this.camera != null)
        {
            supportedPreviewSizes = this.camera.getParameters().getSupportedPreviewSizes();
            requestLayout();
        }
    }

    public void switchCamera(Camera camera)
    {
       setCamera(camera);
       try
       {
           camera.setPreviewDisplay(surfaceHolder);
       }
       catch (IOException exception)
       {
           Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
       }
       Camera.Parameters parameters = camera.getParameters();
       parameters.setPreviewSize(previewSize.width, previewSize.height);
       requestLayout();

       camera.setParameters(parameters);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        // We purposely disregard child measurements because act as a
        // wrapper to a SurfaceView that centers the camera preview instead
        // of stretching it.
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        setMeasuredDimension(width, height);

        if (supportedPreviewSizes != null)
        {
            previewSize = getOptimalPreviewSize(supportedPreviewSizes, width, height);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        if (camera == null)
        {
            super.onLayout(changed,  l, t, r, b);
            return;
        }
        if (changed && getChildCount() > 0)
        {
            final View child = getChildAt(0);

            final int width = r - l;
            final int height = b - t;

            int previewWidth = width;
            int previewHeight = height;

            if (previewSize != null)
            {
                int rotation = ((Activity) surfaceView.getContext()).getWindowManager().getDefaultDisplay().getRotation();
                if (rotation == 1 || rotation == 3)
                {
                previewWidth = previewSize.width;
                previewHeight = previewSize.height;
            }
                else
                {
                    previewWidth = previewSize.height;
                    previewHeight = previewSize.width;
                }

            }

            if (width * previewHeight < height * previewWidth)
            {
                final int scaledChildWidth = previewWidth * height / previewHeight;
                child.layout((width - scaledChildWidth) / 2, 0, (width + scaledChildWidth) / 2, height);
            }
            else
            {
                final int scaledChildHeight = previewHeight * width / previewWidth;
                child.layout(0, (height - scaledChildHeight) / 2, width, (height + scaledChildHeight) / 2);
            }

            // Center the child SurfaceView within the parent.
//            if (width * previewHeight > height * previewWidth)
//            {
//                final int scaledChildWidth = previewWidth * height / previewHeight;
//                child.layout((width - scaledChildWidth) / 2, 0, (width + scaledChildWidth) / 2, height);
//            }
//            else
//            {
//                final int scaledChildHeight = previewHeight * width / previewWidth;
//                child.layout(0, (height - scaledChildHeight) / 2, width, (height + scaledChildHeight) / 2);
//            }
        }
    }


    public void surfaceCreated(SurfaceHolder holder)
    {

        // The Surface has been created, acquire the camera and tell it where
        // to draw.
        try
        {
            if (camera != null)
            {
                camera.setPreviewDisplay(holder);
            }
        }
        catch (IOException exception)
        {
            Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder)
    {
        // Surface will be destroyed when we return, so stop the preview.
        if (camera != null)
        {
            try
            {
                camera.stopPreview();
                camera.release();
                camera = null;
            }
            catch (Throwable t){}
        }
    }


    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h)
    {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) w / h;
        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        // Try to find an size match aspect ratio and size
        for (Camera.Size size : sizes)
        {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff)
            {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // Cannot find the one match the aspect ratio, ignore the requirement
        if (optimalSize == null)
        {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes)
            {
                if (Math.abs(size.height - targetHeight) < minDiff)
                {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h)
    {
        // Now that the size is known, set up the camera parameters and begin
        // the preview.
            try
            {
                Camera.Parameters parameters = camera.getParameters();
                parameters.setPreviewSize(previewSize.width, previewSize.height);
                requestLayout();

                camera.setParameters(parameters);
                int rotation = 90 * ((Activity) surfaceView.getContext()).getWindowManager().getDefaultDisplay().getRotation();
                setCameraDisplayOrientation(rotation, 0 , camera);
                camera.startPreview();
            } catch (Throwable t)
            {
                if (getChildCount() > 0 )
                {
                    ViewGroup.LayoutParams lp = getChildAt(0).getLayoutParams();
                    this.setLayoutParams(new LayoutParams(lp.width, lp.height));
                    requestLayout();
                }
            }
    }

    public  static int setCameraDisplayOrientation(int rotation, int cameraID, Camera camera)
    {
        if (camera != null)
        {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(cameraID, info);
            int result;
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
            {
                result = (info.orientation + rotation) % 360;
                result = (360 - result) % 360;  // compensate the mirror

            }
            else
            {
                result = (info.orientation - rotation + 360) % 360;

            }
            try
            {
                camera.setDisplayOrientation(result);
            }
            catch(Throwable t)
            {
                //Log.e(TAG, "setDisplayOrientation error :" + result + " " + t.toString());
            }
            return result;
        }
        return 0;
    }

    public void resetCamera() {
        camera = null;
    }
}
