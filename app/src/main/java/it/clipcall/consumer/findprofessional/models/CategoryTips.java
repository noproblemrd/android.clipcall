package it.clipcall.consumer.findprofessional.models;

import java.util.List;

/**
 * Created by micro on 12/4/2015.
 */
public class CategoryTips {

    private String categoryId;
    private List<Tip> tips;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public List<Tip> getTips() {
        return tips;
    }

    public void setTips(List<Tip> tips) {
        this.tips = tips;
    }
}
