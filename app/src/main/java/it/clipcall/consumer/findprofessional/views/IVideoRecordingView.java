package it.clipcall.consumer.findprofessional.views;

import java.util.List;

import it.clipcall.consumer.findprofessional.models.Tip;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by micro on 1/12/2016.
 */
public interface IVideoRecordingView extends IView {


    void updateVideoRecordingTime(long totalTimeRecorded);

    void setCategoryTips(List<Tip> categoryTips);

    void onFlashToggled(boolean isOn);
}
