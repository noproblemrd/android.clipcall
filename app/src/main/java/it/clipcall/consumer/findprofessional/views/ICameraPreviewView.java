package it.clipcall.consumer.findprofessional.views;

import java.util.List;

import it.clipcall.consumer.findprofessional.models.Tip;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by micro on 1/12/2016.
 */
public interface ICameraPreviewView extends IView {

    void onStartRecording();

    void updateCategoryTips(List<Tip> categoryTips);
}
