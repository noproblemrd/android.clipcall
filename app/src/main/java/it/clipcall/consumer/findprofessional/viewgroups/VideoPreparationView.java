package it.clipcall.consumer.findprofessional.viewgroups;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.EViewGroup;

import it.clipcall.R;

@EViewGroup(R.layout.video_preparation_fragment)
public class VideoPreparationView extends RelativeLayout{
    public VideoPreparationView(Context context) {
        super(context);
    }

    public VideoPreparationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoPreparationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
