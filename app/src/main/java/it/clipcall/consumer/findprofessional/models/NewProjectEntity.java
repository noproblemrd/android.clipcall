package it.clipcall.consumer.findprofessional.models;

import java.io.Serializable;

/**
 * Created by dorona on 26/01/2016.
 */
public class NewProjectEntity implements Serializable {

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    private String supplierName;

    public boolean isPrivate() {
        return isPrivate;
    }

    private boolean isPrivate;

    public long getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(long videoDuration) {
        this.videoDuration = videoDuration;
    }

    public String getVideoLeadUrl() {
        return videoLeadUrl;
    }

    public void setVideoLeadUrl(String videoLeadUrl) {
        this.videoLeadUrl = videoLeadUrl;
    }

    public String getPreviewPicUrl() {
        return previewPicUrl;
    }

    public void setPreviewPicUrl(String previewPicUrl) {
        this.previewPicUrl = previewPicUrl;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }


    private String[] favoriteSupplierIds;

    private long videoDuration;

    private String videoLeadUrl;

    private String previewPicUrl;

    private String categoryId;

    private String value;

    private String zipCode;

    private String categoryName;

    private String fullAddress;

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String[] getFavoriteSupplierIds() {
        return favoriteSupplierIds;
    }

    public void setFavoriteSupplierIds(String[] favoriteSupplierIds) {
        this.favoriteSupplierIds = favoriteSupplierIds;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }
}
