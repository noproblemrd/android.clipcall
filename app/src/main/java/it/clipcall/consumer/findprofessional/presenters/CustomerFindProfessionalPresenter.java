package it.clipcall.consumer.findprofessional.presenters;

/**
 * Created by dorona on 01/02/2016.
 */
/**
public class CustomerFindProfessionalPresenter extends FindProfessionalPresenter {
    private ExclusiveProRef exclusivePro;

    public CustomerFindProfessionalPresenter(FindProfessionalController controller, RoutingService routingService, PermissionsService permissionsService) {
        super(controller, routingService, permissionsService);
    }

    @Override
    public void startVideoCore(boolean shouldPromptExclusive, boolean isPrivate) {
        if(exclusivePro == null){
            exclusivePro = controller.getExclusiveProInSelectedCategory();
        }

        if(exclusivePro != null && shouldPromptExclusive){
            view.promptSendingToExclusiveAdvertiser(exclusivePro);
            return;
        }

        TempProjectData projectData = new TempProjectData();

        if(exclusivePro != null && isPrivate){
            projectData.setSupplierId(exclusivePro.getAccountId());
            projectData.setSupplierName(exclusivePro.getAccountName());
        }
        boolean createdProjectSuccessfully = controller.createTemporaryProject(projectData);
        if(createdProjectSuccessfully){
            navigateToVideo();
        }
    }

    @Override
    public void initialize() {
        super.initialize();

        view.setTooltipText(R.string.show_us_what_you_need_tooltip_text);
    }

    @Override
    public void updateLastCategory(Category category) {
        super.updateLastCategory(category);
        exclusivePro = controller.getExclusiveProInSelectedCategory();

    }
}
**/