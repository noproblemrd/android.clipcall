package it.clipcall.consumer.findprofessional.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ProgressBar;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.common.base.Strings;
import com.venmo.view.TooltipView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.simple.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.consumer.findprofessional.customviews.ExclusiveProSubmissionView;
import it.clipcall.consumer.findprofessional.customviews.ExclusiveProSubmissionView_;
import it.clipcall.consumer.findprofessional.models.AddressEntity2;
import it.clipcall.consumer.findprofessional.models.ExclusiveProRef;
import it.clipcall.consumer.findprofessional.presenters.FindProfessionalPresenter;
import it.clipcall.consumer.findprofessional.support.filters.CategoryFilter;
import it.clipcall.consumer.findprofessional.views.IFindProfessionalView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.commands.EmptyCallBack;
import it.clipcall.infrastructure.commands.PickGoogleAddressCommand;
import it.clipcall.infrastructure.commands.RequestPermissionsCommand;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.maps.entities.PlaceData;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.professional.profile.models.Category;
import me.drakeet.materialdialog.MaterialDialog;

@EFragment(R.layout.fragment_find_professional)
public class ConsumerFindProfessionalFragment extends BaseFragment implements IFindProfessionalView {


    @ViewById
    EditText zipCodeAutoCompleteTextView;

    @ViewById(R.id.categoryAutoCompleteTextView)
    AutoCompleteTextView categoryAutoCompleteTextView;

    @ViewById
    TooltipView toolTipView;

    @Inject
    FindProfessionalPresenter presenter;

    @ViewById
    Button clipCallButton;

    @ViewById
    ProgressBar zipCodeProgressBar;

    @ViewById
    ProgressBar categoryProgressBar;

    @Click(R.id.clipCallButton)
    void clipCall(){
        RequestPermissionsCommand command = new RequestPermissionsCommand(getActivity(),"Camera","We need you permissions to record a video",
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO);
        command.execute(new EmptyCallBack<Void>(){
            @Override
            public void onSuccess(Void result) {
                clipCallCore();
            }
        });
   }

    @Background
    void clipCallCore(){
        EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.ClipCallItTapped));
        boolean shouldPromptExclusive = true;
        boolean isPrivate = true;
        presenter.startVideoRecording(shouldPromptExclusive, isPrivate);
    }

    @AfterViews
    void calledAfterInjection() {
        presenter.bindView(this);
        presenter.initialize();
    }

    @Background
    void clearLastAddress() {
        presenter.clearLastAddress();
    }


    @UiThread
    void showTooltip(){

        if(zipCodeAutoCompleteTextView.getText() == null)
            return;


        if(Strings.isNullOrEmpty(zipCodeAutoCompleteTextView.getText().toString()))
            return;

        if(categoryAutoCompleteTextView.getText() == null)
            return;


        if(Strings.isNullOrEmpty(categoryAutoCompleteTextView.getText().toString()))
            return;

        if(toolTipView.getVisibility() == View.VISIBLE)
            return;

        clipCallButton.setEnabled(true);
        clipCallButton.requestFocus();

        toolTipView.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.ZoomIn)
                .duration(700)
                .playOn(toolTipView);
    }

    boolean isSelectingAddress = false;

    @Click(R.id.zipCodeAutoCompleteTextView)
    void focusChangedOnHelloTextView() {
        if(isSelectingAddress)
            return;

        isSelectingAddress = true;

        selectAddress();
    }


    private void selectAddress() {
        PickGoogleAddressCommand command = new PickGoogleAddressCommand(getActivity());
        command.execute(new EmptyCallBack<PlaceData>(){
            @Override
            public void onSuccess(PlaceData result) {
                isSelectingAddress = false;
                onAddressSelected(result);
            }

            @Override
            public void onFailure() {
                isSelectingAddress = false;
            }
        });
    }

    @Background
    void onAddressSelected(PlaceData place){
        presenter.updateLastAddress(place);
        Log.d("Foo",place.address);
    }



    @Override
    public RoutingContext getRoutingContext() {
        Activity activity =  getActivity();
        if(!(activity instanceof BaseActivity))
            return null;

        BaseActivity baseActivity = (BaseActivity) activity;
        return baseActivity.getRoutingContext();
    }

    @Background
    void startVideo(boolean shouldPromptExclusive, boolean isPrivate){
        presenter.startVideoCore(shouldPromptExclusive, isPrivate);
    }

    @Override
    public void setRecentlySelectedAddress(AddressEntity2 address) {
        if(address == null)
            return;

        zipCodeAutoCompleteTextView.setText(address.getAddress());
        showTooltip();
    }

    @Override
    public void promptSendingToExclusiveAdvertiser(ExclusiveProRef exclusivePro) {

        final MaterialDialog materialDialog = new MaterialDialog(getActivity());
        ExclusiveProSubmissionView exclusiveProView = ExclusiveProSubmissionView_.build(getActivity());
        materialDialog.setCanceledOnTouchOutside(true);
        materialDialog
                .setContentView(exclusiveProView)
                .setBackgroundResource(R.drawable.pro_dialog_rounded_background)
                        .setPositiveButton("Yes", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                materialDialog.dismiss();
                                boolean shouldPromptExclusive = false;
                                boolean isPrivate = true;
                                startVideo(shouldPromptExclusive, isPrivate);
                            }
                })
                .setNegativeButton("No", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        materialDialog.dismiss();
                        boolean isPrivate = false;
                        boolean shouldPromptExclusive = false;
                        startVideo(shouldPromptExclusive, isPrivate);

                    }
                });
        exclusiveProView.setMessageResource(R.string.exclusive_pro_in_category);
        exclusiveProView.setExclusivePro(exclusivePro);
        materialDialog.show();

    }

    @Override
    public void setTooltipText(int resourceId){
        toolTipView.setText(resourceId);
    }



    @Override
    public void showOrHideZipCodeProgress(boolean show) {
        int visibility = show ? View.VISIBLE : View.INVISIBLE;
        zipCodeProgressBar.setVisibility(visibility);
    }


    @Override
    public void showRequiredCategory(boolean requestFocus) {
        categoryAutoCompleteTextView.setError("You must enter a category");
        if(requestFocus)
            categoryAutoCompleteTextView.requestFocus();
    }


    @Override
    public void showRequiredZipCode(boolean requestFocus) {
        zipCodeAutoCompleteTextView.setError("You must enter an address");
        if(requestFocus)
            zipCodeAutoCompleteTextView.requestFocus();
    }


    @Override
    public void onAddressUpdated(String address) {
        zipCodeAutoCompleteTextView.setText(address);
        showTooltip();
    }


    @Override
    public void setAvailableCategories(final List<Category> availableCategories) {
        if(availableCategories == null)
            return;

        ArrayAdapter adapter = new ArrayAdapter(this.getContext(), android.R.layout.simple_list_item_1,new ArrayList<Category>()){
            private final Filter filter = new CategoryFilter(this, availableCategories);
            @Override
            public Filter getFilter() {
                return filter;
            }
        };

        categoryAutoCompleteTextView.setAdapter(adapter);
        categoryAutoCompleteTextView.setThreshold(1);
        categoryAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(Strings.isNullOrEmpty(s+"") || s.length() == 1){
                    clearLastCategory();
                }
            }
        });

        categoryAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Category selectedCategory = (Category) categoryAutoCompleteTextView.getAdapter().getItem(position);
                updateLastCategory(selectedCategory);

                if (zipCodeAutoCompleteTextView.getText() == null && Strings.isNullOrEmpty(zipCodeAutoCompleteTextView.getText().toString())) {
                    zipCodeAutoCompleteTextView.requestFocus();
                    return;
                }

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(categoryAutoCompleteTextView.getWindowToken(), 0);

                showTooltip();
            }
        });
    }

    @Background
    void clearLastCategory(){
        presenter.clearLastCategory();
    }

    @Background
    void updateLastCategory(Category category){
        presenter.updateLastCategory(category);
    }

    @Override
    protected String getTitle() {
        return "Find a professional";
    }

    @Override
    protected int getMenuItemId() {
        return R.id.nav_find_pro_consumer;
    }
}
