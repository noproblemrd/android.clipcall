package it.clipcall.consumer.findprofessional.activities;

import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.findprofessional.presenters.SendingVideoRecordingPresenter;
import it.clipcall.consumer.findprofessional.support.ui.RadarView;
import it.clipcall.consumer.findprofessional.views.ISendingVideoRecordingView;
import it.clipcall.infrastructure.activities.BaseActivity;

@EActivity(R.layout.submit_project_to_advertisers_activity)
public class SubmitProjectToAdvertisersActivity extends BaseActivity  implements ISendingVideoRecordingView {

    @Inject
    SendingVideoRecordingPresenter presenter;


    @ViewById
    RadarView locatingProsRadarView;

    @ViewById
    TextView waitingTextView;


    @Extra("postValidation")
    boolean postValidation;

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        locatingProsRadarView.setShowCircles(true);
        locatingProsRadarView.startAnimation();
        initialize();
    }


    @Background
    void initialize(){
        presenter.initialize();
    }

    @Click(R.id.locatingProsButton)
    void onContinueTapped(){

    }

    @UiThread
    @Override
    public void setLocatingText(int resourceId) {
        waitingTextView.setText(resourceId);
    }
}
