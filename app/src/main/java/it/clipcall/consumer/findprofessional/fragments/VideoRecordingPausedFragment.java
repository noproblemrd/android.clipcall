package it.clipcall.consumer.findprofessional.fragments;

import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.venmo.view.TooltipView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.findprofessional.presenters.VideoRecordingPausedPresenter;
import it.clipcall.consumer.findprofessional.views.IVideoRecordingPausedView;
import it.clipcall.infrastructure.routing.models.RoutingContext;

@EFragment(R.layout.video_recording_paused_fragment)
public class VideoRecordingPausedFragment extends VideoRecordingBaseFragment implements IVideoRecordingPausedView {

    @ViewById
    ProgressBar progressBar;

    @ViewById
    TooltipView submitVideoToolTipView;

    @Inject
    VideoRecordingPausedPresenter presenter;

    @ViewById
    TextView reviewTextView;

    @ViewById
    TextView redoTextView;

    @ViewById
    TextView recordingTimeTextView;

    @ViewById
    ProgressBar loadingSpinner;


    @ViewById
    ProgressBar submitVideoLoadingSpinner;

    @ViewById
    Button sendItButton;

    private long currentRecordingTime;

    private Timer timer;


    @ViewById
    Button playToggleButton;

    int totalRetries = 10;

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        playToggleButton.setText("Continue");
        SpannableString redoContent = new SpannableString("Redo");
        redoContent.setSpan(new UnderlineSpan(), 0, redoContent.length(), 0);
        redoTextView.setText(redoContent);

        SpannableString reviewContent = new SpannableString("Review");
        reviewContent.setSpan(new UnderlineSpan(), 0, reviewContent.length(), 0);
        reviewTextView.setText(reviewContent);

        initialize();

    }

    @Background
    void initialize(){
        presenter.initialize();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.shutDown();
    }

    @Click(R.id.playToggleButton)
    void onContinueTapped(){
        listener.onContinueRecordingRequested(currentRecordingTime);
    }


    @Click(R.id.sendItButton)
    void sendVideoTapped(){

        sendItButton.setVisibility(View.INVISIBLE);
        submitVideoLoadingSpinner.setVisibility(View.VISIBLE);

        if(presenter.canSubmitVideo()){
            listener.onSubmitRecordingRequested();
            reset();
            return;
        }

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.d("SubmitVideo", "waiting for video to become available");
                totalRetries--;
                if(totalRetries < 0){
                    listener.onSubmitRecordingRequested();//TODO show error
                    reset();
                    return;
                }
                if(presenter.canSubmitVideo()){
                    listener.onSubmitRecordingRequested();
                    reset();
                    return;
                }

            }
        }, 0, 1000);
    }


    @UiThread
    void reset() {
        currentRecordingTime = 0;
        totalRetries = 10;
        sendItButton.setVisibility(View.VISIBLE);
        submitVideoLoadingSpinner.setVisibility(View.GONE);
        if(timer != null){
            timer.cancel();
            timer = null;
        }
    }


    @Click(R.id.reviewTextView)
    void onReviewVideoTapped(){
        if(loadingSpinner.getVisibility() == View.VISIBLE)
            return;

        loadingSpinner.setVisibility(View.VISIBLE);
        reviewTextView.setEnabled(false);
        redoTextView.setEnabled(false);
        listener.onReviewRecordingRequested();

    }

    @Click(R.id.redoTextView)
    void onRedoVideoTapped(){
        if(loadingSpinner.getVisibility() == View.VISIBLE)
            return;

        reviewTextView.setEnabled(false);
        redoTextView.setEnabled(false);
        loadingSpinner.setVisibility(View.VISIBLE);

        reset();
        listener.onRedoRecordingRequested();
    }


    @Override
    public void onRecordingPaused(long currentRecordingTime) {
        this.currentRecordingTime = currentRecordingTime;

        if(currentRecordingTime >= maxRecordingTime){
            playToggleButton.setVisibility(View.GONE);
        }

        int progress = (int) currentRecordingTime*100/ maxRecordingTime;
        final int finalProgress = Math.min(100,progress);
        progressBar.setProgress(finalProgress);

        int minutes = (int) currentRecordingTime / 60;
        int seconds = (int)currentRecordingTime % 60;

        String minutesFormatted = String.format("%02d", minutes);
        String secondsFormatted = String.format("%02d", seconds);
        recordingTimeTextView.setText(minutesFormatted + ":" + secondsFormatted);
    }

    @Override
    public boolean canHandleBackPressed() {
        return true;
    }

    @Override
    public void handleBackPressed() {
        listener.onCancelRecordingRequested();
    }

    @Override
    public RoutingContext getRoutingContext() {
        return new RoutingContext(getActivity());
    }


    @UiThread
    @Override
    public void setLocatingText(int resourceId) {
        submitVideoToolTipView.setText(resourceId);
        submitVideoToolTipView.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.ZoomIn)
                .duration(700)
                .playOn(submitVideoToolTipView);
    }

    @UiThread
    @Override
    public void setLocatingText(int resourceId, String proName) {

        String formattedMessage = getString(resourceId);
        String message = String.format(formattedMessage, proName);
        submitVideoToolTipView.setText(message);
        submitVideoToolTipView.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.ZoomIn)
                .duration(700)
                .playOn(submitVideoToolTipView);
    }

    @Override
    public void onActivityPause() {
        loadingSpinner.setVisibility(View.INVISIBLE);
        reviewTextView.setEnabled(true);
        redoTextView.setEnabled(true);
    }
}