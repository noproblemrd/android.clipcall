package it.clipcall.consumer.findprofessional.models.events;

/**
 * Created by dorona on 26/01/2016.
 */
public class VideoDataAvailableEvent extends VideoEvent {

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getPreviewImageUrl() {
        return previewImageUrl;
    }

    private final String videoUrl;
    private final String previewImageUrl;

    public VideoDataAvailableEvent(String videoUrl, String previewImageUrl) {
        this.videoUrl = videoUrl;
        this.previewImageUrl = previewImageUrl;
    }
}
