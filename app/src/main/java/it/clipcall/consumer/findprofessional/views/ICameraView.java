package it.clipcall.consumer.findprofessional.views;

import android.hardware.Camera;

import it.clipcall.consumer.findprofessional.models.ExclusivePro;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 06/01/2016.
 */
public interface ICameraView extends IView {

    void onCameraOpened(Camera camera);

    void onCameraReleased();

    void previewVideoRecording(String fullpath);

    void showLoadingProfessionalForInvitation(String invitationId);

    void showProfessional(ExclusivePro pro);

    void showInvitationInvalid();

}
