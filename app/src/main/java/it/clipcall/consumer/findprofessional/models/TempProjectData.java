package it.clipcall.consumer.findprofessional.models;

/**
 * Created by dorona on 01/02/2016.
 */
public class TempProjectData {

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    private String supplierId;

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    private String supplierName;
}
