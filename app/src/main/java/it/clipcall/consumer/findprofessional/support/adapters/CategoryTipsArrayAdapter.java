package it.clipcall.consumer.findprofessional.support.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import it.clipcall.R;
import it.clipcall.consumer.findprofessional.models.Tip;

/**
 * Created by dorona on 07/01/2016.
 */
public class CategoryTipsArrayAdapter extends ArrayAdapter<Tip> {

    public CategoryTipsArrayAdapter(Context context, int resource, List<Tip> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.consumer_video_tooltip_item, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.tooltipText);
        Button tooltipIndexButton = (Button) rowView.findViewById(R.id.tooltipIndexButton);
        tooltipIndexButton.setText(Integer.toString(position + 1));
        Tip tip = getItem(position);
        textView.setText(tip.getValue());
        return rowView;
    }
}
