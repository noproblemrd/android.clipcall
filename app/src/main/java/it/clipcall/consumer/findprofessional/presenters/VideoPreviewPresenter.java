package it.clipcall.consumer.findprofessional.presenters;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.professional.profile.models.Category;
import it.clipcall.consumer.findprofessional.models.Tip;
import it.clipcall.consumer.findprofessional.services.ProfessionalsManager;
import it.clipcall.consumer.findprofessional.views.ICameraPreviewView;
import it.clipcall.infrastructure.presenters.PresenterBase;

/**
 * Created by dorona on 13/01/2016.
 */
public class VideoPreviewPresenter extends PresenterBase<ICameraPreviewView> {
    private final ProfessionalsManager professionalsManager;

    @Inject
    public VideoPreviewPresenter(ProfessionalsManager professionalsManager) {

        this.professionalsManager = professionalsManager;
    }

    public void initialize() {

        Category category = professionalsManager.getLastCategoryOrFromProject();
        if(category == null)
            return;


        List<Tip> categoryTips = professionalsManager.getCategoryTips(category.getCategoryId());
        view.updateCategoryTips(categoryTips);
    }


}
