package it.clipcall.consumer.findprofessional.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.google.common.base.Strings;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import java.io.File;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.consumer.findprofessional.customviews.ExclusiveProSubmissionView;
import it.clipcall.consumer.findprofessional.customviews.ExclusiveProSubmissionView_;
import it.clipcall.consumer.findprofessional.fragments.VideoRecordingBaseFragment;
import it.clipcall.consumer.findprofessional.models.ExclusivePro;
import it.clipcall.consumer.findprofessional.models.ExclusiveProRef;
import it.clipcall.consumer.findprofessional.models.events.VideoDataAvailableEvent;
import it.clipcall.consumer.findprofessional.models.events.VideoRecordingFinishedEvent;
import it.clipcall.consumer.findprofessional.models.events.VideoRecordingStartedEvent;
import it.clipcall.consumer.findprofessional.presenters.CameraPresenter;
import it.clipcall.consumer.findprofessional.support.CameraPreviewLayout;
import it.clipcall.consumer.findprofessional.support.IVideoRecordingListener;
import it.clipcall.consumer.findprofessional.support.adapters.VideoPagerAdapter;
import it.clipcall.consumer.findprofessional.views.ICameraView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.commands.EmptyCallBack;
import it.clipcall.infrastructure.commands.RequestPermissionsCommand;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import me.drakeet.materialdialog.MaterialDialog;


@EActivity(R.layout.camera_preview)
public class CameraPreviewActivity extends BaseActivity implements IVideoRecordingListener, ICameraView
{
    @Inject
    CameraPresenter presenter;

    ProgressDialog progressDialog;

    private CameraPreviewLayout cameraPreviewLayout;

    private VideoPagerAdapter videoPagerAdapter;

    private final Handler  handler = new Handler(Looper.getMainLooper());

    @Extra
    String invitationId;

    @ViewById(R.id.content_layout)
    ViewGroup contentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    void afterViews(){
        RequestPermissionsCommand command = new RequestPermissionsCommand(this,"Camera","We need you permissions to record a video", Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO);
        command.execute(new EmptyCallBack<Void>(){
            @Override
            public void onSuccess(Void result) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startPreview();
                    }
                },200);
            }

            @Override
            public void onFailure() {
                cancelInvitation();
            }
        });
    }

    private void cancelInvitation() {
        presenter.clearInvitation();
        if(!Strings.isNullOrEmpty(invitationId))
        {
            presenter.bindView(this);
            presenter.routeToHome();
        }

        finish();
    }

    private void startPreview(){
        presenter.setCameraPermissionsGranted(true);
        presenter.bindView(this);
        videoPagerAdapter = new VideoPagerAdapter(getSupportFragmentManager());
        cameraPreviewLayout = new CameraPreviewLayout(this);
        presenter.initialize();
        contentLayout.removeView(cameraPreviewLayout);
        contentLayout.addView(cameraPreviewLayout, 0);
        videoPagerAdapter.reset();
        videoPagerAdapter.next();

        if(!Strings.isNullOrEmpty(invitationId)){
            presenter.loadProfessionalWithInvitation(invitationId);
        }

        presenter.openCameraOrStartPreviewAndContinueRecording();
    }



    @Override
    protected void onResume()
    {
        super.onResume();
        if(presenter.isCameraPermissionsGranted())
            presenter.openCameraOrStartPreviewAndContinueRecording();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if(presenter.isCameraPermissionsGranted()){
            presenter.setIsActivityPaused(true);
            videoPagerAdapter.onActivityPause();
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.releaseResources();
    }

    @Override
    public RoutingContext getRoutingContext() {
        return new RoutingContext(this);
    }

    @Override
    public void onCameraOpened(Camera camera) {
        cameraPreviewLayout.setCamera(camera);
    }

    @UiThread
    @Override
    public void onCameraReleased() {
        cameraPreviewLayout.setCamera(null);
    }

    @UiThread
    @Override
    public void previewVideoRecording(String fullpath) {
        Log.d("Foo", "UiThread.previewVideoRecording"+ fullpath);
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(fullpath)), "video/mp4");
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        this.startActivity(intent);
    }


    @UiThread
    @Override
    public void showLoadingProfessionalForInvitation(String invitationId) {
        if(progressDialog == null)
            progressDialog = new ProgressDialog(this);

        progressDialog.setTitle("");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        //progressDialog.setOnCancelListener(this);
        progressDialog.show();
    }

    @UiThread
    @Override
    public void showProfessional(final ExclusivePro pro) {
        if(progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }

        final MaterialDialog materialDialog = new MaterialDialog(this);
        ExclusiveProSubmissionView exclusiveProView = ExclusiveProSubmissionView_.build(this);
        materialDialog.setCanceledOnTouchOutside(true);
        materialDialog
                .setContentView(exclusiveProView)
                .setBackgroundResource(R.drawable.pro_dialog_rounded_background)
                .setPositiveButton("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        materialDialog.dismiss();
                        boolean shouldPromptExclusive = false;
                        boolean isPrivate = true;
                        //startVideo(shouldPromptExclusive, isPrivate);
                    }
                });

        ExclusiveProRef proRef = new ExclusiveProRef();
        proRef.setAccountId(pro.getSupplierId());
        proRef.setAccountName(pro.getSupplierName());
        proRef.setImageUrl(pro.getSupplierImage());
        exclusiveProView.setMessageResource(R.string.exclusive_pro_from_invitation);
        exclusiveProView.setExclusivePro(proRef);
        materialDialog.show();
    }

    @UiThread
    @Override
    public void showInvitationInvalid() {
        if(progressDialog != null){
            progressDialog.hide();
            progressDialog = null;
        }
    }


    @Override
    public void onStartRecordingRequested() {
        presenter.startPreview(this, cameraPreviewLayout.surfaceView);
        cameraPreviewLayout.resetCamera();
        videoPagerAdapter.next();
    }

    @UiThread
    @Override
    public void onRecordingCountDownCompleted() {
        presenter.startVideoRecording();
        videoPagerAdapter.next();
    }

    @Override
    public void onPauseRecordingRequested(long currentRecordingTime) {
        presenter.pauseVideoRecording();
        videoPagerAdapter.next();
        onRecordingPaused(currentRecordingTime);
    }

    @Override
    public void onContinueRecordingRequested(long currentRecordingTime) {
        presenter.continueVideoRecording();
        videoPagerAdapter.previous();
        onRecordingContinued(currentRecordingTime);
    }

    @Override
    public void onRedoRecordingRequested() {
        presenter.initialize();
        contentLayout.removeView(cameraPreviewLayout);
        contentLayout.addView(cameraPreviewLayout, 0);
        presenter.openCameraOrStartPreviewAndContinueRecording();

        videoPagerAdapter.reset();
        videoPagerAdapter.next();
    }

    @Override
    public void onReviewRecordingRequested() {
        presenter.reviewTempProject(this);
    }

    @Override
    public void onSubmitRecordingRequested() {
        presenter.submitVideoRecording();
    }

    @Override
    public void onCancelRecordingRequested() {
        final MaterialDialog materialDialog = new MaterialDialog(this);
        materialDialog.setTitle("Cancel video")
                .setMessage("Are you sure you want to cancel the video recording?")
                .setPositiveButton("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.FIND_PRO_CLOSE_VIDEO_RECORDING));
                        materialDialog.dismiss();
                        finish();
                    }
                })
                .setNegativeButton("CANCEL", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        materialDialog.dismiss();
                    }
                });

        materialDialog.show();
    }

    @Override
    public void onRecordingExpired() {
        presenter.pauseVideoRecording();
        videoPagerAdapter.next();
        onRecordingPaused(120);
    }

    @Override
    public void onCancelRecordingFromPreviewRequested() {

        presenter.clearInvitation();
        if(!Strings.isNullOrEmpty(invitationId))
            presenter.routeToHome();

        finish();
    }

    @UiThread(delay=100)
    void onRecordingPaused(long currentRecordingTime) {
        videoPagerAdapter.onRecordingPaused(currentRecordingTime);
    }

    @UiThread(delay=100)
    void onRecordingContinued(long currentRecordingTime) {
        videoPagerAdapter.onRecordingContinued(currentRecordingTime);
    }



    @Override
    public void onBackPressed() {
        VideoRecordingBaseFragment currentFragment = videoPagerAdapter.getCurrentFragment();
        if(currentFragment == null)
        {
            super.onBackPressed();
            return;
        }

        boolean canHandleBackPressed = currentFragment.canHandleBackPressed();
        if(canHandleBackPressed == false){
            super.onBackPressed();
            return;
        }

        currentFragment.handleBackPressed();
    }

    @Subscriber void onVideoRecordingFinishedEvent(VideoRecordingFinishedEvent videoEvent) {
        videoPagerAdapter.onRecordingFinished();
    }

    @Subscriber
    void onVideoDataAvailable(VideoDataAvailableEvent videoEvent){
        presenter.updateTempVideoData(videoEvent.getVideoUrl(), videoEvent.getPreviewImageUrl());
    }

    @Subscriber
    void onRecordingStarted(VideoRecordingStartedEvent videoEvent){
       videoPagerAdapter.onRecordingStarted();
    }

}