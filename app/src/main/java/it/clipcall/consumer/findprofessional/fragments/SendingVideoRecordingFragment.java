package it.clipcall.consumer.findprofessional.fragments;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.findprofessional.presenters.SendingVideoRecordingPresenter;
import it.clipcall.consumer.findprofessional.views.ISendingVideoRecordingView;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.routing.models.RoutingContext;

@EFragment(R.layout.sending_video_recording_fragment)
public class SendingVideoRecordingFragment extends BaseFragment implements ISendingVideoRecordingView {

    @Inject
    SendingVideoRecordingPresenter presenter;

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
    }

    @Click(R.id.locatingProsButton)
    void onContinueTapped(){
    }

    @Override
    public RoutingContext getRoutingContext() {
        return new RoutingContext(getActivity());
    }

    @Override
    public void setLocatingText(int resourceId) {

    }
}