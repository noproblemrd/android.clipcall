package it.clipcall.consumer.findprofessional.models;

import java.io.Serializable;

/**
 * Created by dorona on 02/05/2016.
 */
public class AddressEntity2 implements Serializable {

    private String zipCode;

    private String address;

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
