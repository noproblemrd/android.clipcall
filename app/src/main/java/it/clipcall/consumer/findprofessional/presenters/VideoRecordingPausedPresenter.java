package it.clipcall.consumer.findprofessional.presenters;

import org.simple.eventbus.EventBus;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.findprofessional.controllers.VideoRecordingController;
import it.clipcall.consumer.findprofessional.models.NewProjectEntity;
import it.clipcall.consumer.findprofessional.views.IVideoRecordingPausedView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.services.RoutingService;

/**
 * Created by dorona on 26/01/2016.
 */
@PerActivity
public class VideoRecordingPausedPresenter extends PresenterBase<IVideoRecordingPausedView> {


    private final RoutingService routingService;
    private final VideoRecordingController controller;

    @Inject
    public VideoRecordingPausedPresenter(RoutingService routingService, VideoRecordingController controller) {
        this.routingService = routingService;
        this.controller = controller;
    }

    @Override
    public void initialize() {
        super.initialize();
        EventBus.getDefault().register(this);


        NewProjectEntity project = controller.getProject();
        if (project.isPrivate()) {
            view.setLocatingText(R.string.send_video_tooltip_single_pro_text, project.getSupplierName());
        }else{
            view.setLocatingText(R.string.send_video_tooltip_text );
        }
    }


    @Override
    public void shutDown() {
        super.shutDown();
        EventBus.getDefault().unregister(this);
    }
    public boolean canSubmitVideo() {
        return controller.canSubmitVideo();
    }

}


