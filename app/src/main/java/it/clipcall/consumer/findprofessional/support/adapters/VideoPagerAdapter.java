package it.clipcall.consumer.findprofessional.support.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.R;
import it.clipcall.consumer.findprofessional.fragments.SendingVideoRecordingFragment_;
import it.clipcall.consumer.findprofessional.fragments.VideoPreparationFragment_;
import it.clipcall.consumer.findprofessional.fragments.VideoPreviewFragment_;
import it.clipcall.consumer.findprofessional.fragments.VideoRecordingBaseFragment;
import it.clipcall.consumer.findprofessional.fragments.VideoRecordingFragment_;
import it.clipcall.consumer.findprofessional.fragments.VideoRecordingPausedFragment_;

/**
 * Created by micro on 1/25/2016.
 */
public class VideoPagerAdapter{
    private final FragmentManager fragmentManager;

    private final List<Fragment> fragments = new ArrayList<>();
    private int currentIndex = -1;

    public VideoPagerAdapter(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;

        fragments.add(VideoPreviewFragment_.builder().build());
        fragments.add(VideoPreparationFragment_.builder().build());
        fragments.add(VideoRecordingFragment_.builder().build());
        fragments.add(VideoRecordingPausedFragment_.builder().build());
        fragments.add(SendingVideoRecordingFragment_.builder().build());
    }


    public boolean next(){
        if(currentIndex >= fragments.size())
            return false;


        FragmentTransaction transaction = fragmentManager.beginTransaction();
        Fragment fragment = fragments.get(++currentIndex);
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.commit();
        return true;
    }

    public boolean previous(){

        if(currentIndex <= 0)
            return false;

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        Fragment fragment = fragments.get(--currentIndex);
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.commit();
        return true;

    }

    public VideoRecordingBaseFragment getCurrentFragment() {
        return (VideoRecordingBaseFragment) fragmentManager.findFragmentById(R.id.fragmentContainer);
    }

    public void reset(){
        currentIndex = -1;
    }

    public void onActivityPause() {
        VideoRecordingBaseFragment fragment = getCurrentFragment();
        fragment.onActivityPause();
    }

    public void onRecordingPaused(long currentRecordingTime) {
        VideoRecordingBaseFragment fragment = getCurrentFragment();
        fragment.onRecordingPaused(currentRecordingTime);
    }

    public void onRecordingContinued(long currentRecordingTime) {
        VideoRecordingBaseFragment fragment = getCurrentFragment();
        fragment.onRecordingContinued(currentRecordingTime);
    }

    public void onRecordingFinished() {
        VideoRecordingBaseFragment fragment = getCurrentFragment();
        fragment.onRecordingFinished();
    }

    public void onRecordingStarted() {
        VideoRecordingBaseFragment fragment = getCurrentFragment();
        fragment.onRecordingStarted();
    }
}
