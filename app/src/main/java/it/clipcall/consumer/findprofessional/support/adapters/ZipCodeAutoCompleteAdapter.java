package it.clipcall.consumer.findprofessional.support.adapters;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.professional.profile.support.AddressAssist;

/**
 * Created by dorona on 01/12/2015.
 */
public class ZipCodeAutoCompleteAdapter extends ArrayAdapter<Address> implements Filterable {

    private LayoutInflater layoutInflater;
    private Geocoder geocoder;
    private StringBuilder stringBuilder = new StringBuilder();

    public ZipCodeAutoCompleteAdapter(Context context, int resource, Address[] objects) {
        super(context, resource, objects);
    }

    private String typedZipCode = null;

    public ZipCodeAutoCompleteAdapter(final Context context) {
        super(context, -1);
        layoutInflater = LayoutInflater.from(context);
        geocoder = new Geocoder(context);
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        final TextView tv;
        if (convertView != null) {
            tv = (TextView) convertView;
        } else {
            tv = (TextView) layoutInflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        }

        tv.setText(createFormattedAddressFromAddress(getItem(position)));
        return tv;
    }

    private String createFormattedAddressFromAddress(final Address address) {
        //return address.getPostalCode();
       /* stringBuilder.setLength(0);
        final int addressLineSize = address.getMaxAddressLineIndex();
        for (int i = 0; i < addressLineSize; i++) {
            String addressLine = address.getAddressLine(i);
            String[] addressParts = addressLine.split(" ");
            String postalCode = addressParts[addressParts.length-1];
            if(postalCode.contains(typedZipCode) && !postalCode.contains("#")){
                stringBuilder.append(addressLine);
            }

        }
        return stringBuilder.toString();*/
        String formattedAddress = AddressAssist.getFormattedAddress(address);
        /*ArrayList<String> addressFragments = new ArrayList<String>();
        for(int i = 0; i < address.getMaxAddressLineIndex(); i++) {
            addressFragments.add(address.getAddressLine(i));
        }
        String formattedAddress = TextUtils.join(", ",addressFragments);*/
        return formattedAddress;
    }

    @Override
    public Filter getFilter() {
        Filter myFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(final CharSequence constraint) {
                List<Address> addressList = new ArrayList<>();
                if (constraint != null) {
                    try {

                        String value = (String)constraint;
                        typedZipCode = value;
                        if(!value.toLowerCase().contains("us"))
                            value += ", US";

                        List<Address> addresses = geocoder.getFromLocationName(value, 7);
                        if(addresses != null){

                           for(Address address: addresses){
                              /* if(address.getPostalCode() != null && address.getPostalCode().contains((String)constraint)){
                                   addressList.add(address);
                               }*/
                               addressList.add(address);
                           }


                        }
                    } catch (Exception e) {
                    }
                }
                final FilterResults filterResults = new FilterResults();
                filterResults.values = addressList;
                filterResults.count = addressList.size();

                return filterResults;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(final CharSequence constraint, final FilterResults results) {
                clear();
                for (Address address : (List<Address>) results.values) {
                    add(address);
                }
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }

            @Override
            public CharSequence convertResultToString(final Object resultValue) {
                return resultValue == null ? "" :
                         AddressAssist.getFormattedAddress(((Address)resultValue));
            }
        };
        return myFilter;
    }
}
