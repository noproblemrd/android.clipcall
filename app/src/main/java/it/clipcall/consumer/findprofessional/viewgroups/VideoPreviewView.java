package it.clipcall.consumer.findprofessional.viewgroups;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.EViewGroup;

import it.clipcall.R;

@EViewGroup(R.layout.video_preview_fragment)
public class VideoPreviewView extends RelativeLayout {
    public VideoPreviewView(Context context) {
        super(context);
    }

    public VideoPreviewView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoPreviewView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
