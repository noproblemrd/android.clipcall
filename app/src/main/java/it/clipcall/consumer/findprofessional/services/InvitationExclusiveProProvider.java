package it.clipcall.consumer.findprofessional.services;

import com.google.common.base.Strings;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.invitations.services.BranchIoManager;
import it.clipcall.consumer.findprofessional.models.ExclusiveProRef;
import it.clipcall.professional.profile.models.Category;

@Singleton
public class InvitationExclusiveProProvider implements IExclusiveProfessionalProvider{

    private final BranchIoManager branchIoManager;
    private final ProfessionalsManager professionalsManager;

    @Inject
    public InvitationExclusiveProProvider(BranchIoManager branchIoManager, ProfessionalsManager professionalsManager){

        this.branchIoManager = branchIoManager;
        this.professionalsManager = professionalsManager;
    }

    @Override
    public ExclusiveProRef getExclusivePro() {
        String invitationId = branchIoManager.getInvitationId();
        if(Strings.isNullOrEmpty(invitationId))
            return null;

        Category lastCategory = professionalsManager.getLastCategory();
        if(lastCategory == null)
            return null;

        ExclusiveProRef exclusivePro = professionalsManager.getExclusiveProForInvitationInCategory(invitationId, lastCategory);
        return exclusivePro;
    }
}
