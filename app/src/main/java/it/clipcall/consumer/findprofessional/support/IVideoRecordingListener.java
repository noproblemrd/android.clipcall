package it.clipcall.consumer.findprofessional.support;

/**
 * Created by dorona on 28/01/2016.
 */
public interface IVideoRecordingListener {


    void onStartRecordingRequested();

    void onRecordingCountDownCompleted();

    void onPauseRecordingRequested(long currentRecordingTime);

    void onContinueRecordingRequested(long currentRecordingTime);

    void onRedoRecordingRequested();

    void onReviewRecordingRequested();

    void onSubmitRecordingRequested();

    void onCancelRecordingRequested();

    void onRecordingExpired();

    void onCancelRecordingFromPreviewRequested();
}
