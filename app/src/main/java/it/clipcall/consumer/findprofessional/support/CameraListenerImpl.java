package it.clipcall.consumer.findprofessional.support;

import android.util.Log;

import com.emoze.tildaLib.intrf.ELCPCameraListener;

import org.simple.eventbus.EventBus;

import it.clipcall.consumer.findprofessional.models.events.VideoCameraConnectedEvent;
import it.clipcall.consumer.findprofessional.models.events.VideoCameraCriticalErrorEvent;
import it.clipcall.consumer.findprofessional.models.events.VideoCameraSurfaceDestroyedEvent;
import it.clipcall.consumer.findprofessional.models.events.VideoRecordingFinishedEvent;
import it.clipcall.consumer.findprofessional.models.events.VideoRecordingStartedEvent;

public class CameraListenerImpl implements ELCPCameraListener {


    private final String Tag = "CameraListenerImpl";

    private final EventBus eventBus = EventBus.getDefault();

    @Override
    public void OnCameraError(int i) {
        Log.d(Tag, "OnCameraError" );
        eventBus.post(new VideoRecordingStartedEvent());
    }

    @Override
    public void OnCameraRecordingStarted() {
        Log.d(Tag, "OnCameraRecordingStarted" );
        eventBus.post(new VideoRecordingStartedEvent());
    }

    @Override
    public void OnCameraSurfaceDestroyed() {
        Log.d(Tag, "OnCameraSurfaceDestroyed" );
        eventBus.post(new VideoCameraSurfaceDestroyedEvent());
    }

    @Override
    public void OnCameraRecordingFinished() {
        Log.d(Tag, "OnCameraRecordingFinished" );
        eventBus.post(new VideoRecordingFinishedEvent());
    }

    @Override
    public boolean IsContinue() {
        Log.d(Tag, "IsContinue" );
        return false;
    }

    @Override
    public void OnCameraCriticalError() {
        Log.d(Tag, "OnCameraCriticalError" );
        eventBus.post(new VideoCameraCriticalErrorEvent());
    }

    @Override
    public void OnCameraSegmentSent(boolean cameraSessionStartSent) {
        Log.d(Tag, "OnCameraSegmentSent: cameraSessionStartSent is" +  cameraSessionStartSent);
    }

    @Override
    public void CameraConnected() {
        Log.d(Tag, "CameraConnected" );
        eventBus.post(new VideoCameraConnectedEvent());
    }

    @Override
    public void PreviewReady(boolean b, String s) {

    }
}
