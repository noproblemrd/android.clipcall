package it.clipcall.consumer.findprofessional.fragments;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.findprofessional.models.LogTag;
import it.clipcall.consumer.findprofessional.models.Tip;
import it.clipcall.consumer.findprofessional.presenters.VideoRecordingPresenter;
import it.clipcall.consumer.findprofessional.views.IVideoRecordingView;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.ui.text.TextSliderLayout;
@EFragment(R.layout.video_recording_fragment)
public class VideoRecordingFragment extends VideoRecordingBaseFragment implements IVideoRecordingView {

    @ViewById
    ProgressBar progressBar;

    @ViewById
    Button playToggleButton;

    @Inject
    VideoRecordingPresenter presenter;


    @ViewById
    TextView recordingTimeTextView;

    @ViewById
    TextSliderLayout tipsContainer;


    @ViewById
    ImageView flipCameraImageView;

    @ViewById
    ImageView toggleFlashImageView;
    
    //Drawable flashOnDrawable;
    
    //Drawable flashOffDrawable;

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.shutDown();
    }

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        playToggleButton.setText("Pause");
        TextSliderLayout.SliderListener sliderListener = new TextSliderLayout.SliderListener(){

            @Override
            public void onFinished() {
                hideTips();
            }
        };
        ///flashOnDrawable = new ImageDrawable(getActivity(), MaterialCommunityIcons.mdi_flash).colorRes(android.R.color.white);
        //flashOffDrawable = new IconDrawable(getActivity(), MaterialCommunityIcons.mdi_flash_off).colorRes(android.R.color.white);
        //flipCameraImageView.setImageDrawable(new IconDrawable(getActivity(), MaterialCommunityIcons.mdi_camera_switch).colorRes(android.R.color.white));
        toggleFlashImageView.setImageResource(R.drawable.new_pre_live_flash_off);
        tipsContainer.setSliderListener(sliderListener);
        presenter.startRecordingIfNotStartedAlready();
        presenter.initialize();
    }


    @UiThread
    void hideTips(){
        if(tipsContainer != null){
            tipsContainer.setVisibility(View.GONE);
        }

    }


    @Click(R.id.playToggleButton)
    void onPauseTapped(){
        presenter.pauseVideoRecording();
        long currentRecordingTime = presenter.getCurrentRecordingTime();
        listener.onPauseRecordingRequested(currentRecordingTime);
    }

    @Click(R.id.flipCameraImageView)
    void onFlipCameraTapped(){
        presenter.flipCamera();
    }

    @Override
    public RoutingContext getRoutingContext() {
        return null;
    }

    @Click(R.id.toggleFlashImageView)
    void onToggleFlashTapped(){
        presenter.toggleFlash();
    }

    @UiThread
    @Override
    public void updateVideoRecordingTime(long totalTimeRecorded) {
        if(totalTimeRecorded > maxRecordingTime)
        {
            listener.onRecordingExpired();
            return;
        }

        Log.d(LogTag.value, "updating video recording time with total time of: " + totalTimeRecorded);
        int progress = (int) totalTimeRecorded*100/ maxRecordingTime;
        progressBar.setProgress(progress);

        int minutes = (int) totalTimeRecorded / 60;
        int seconds = (int)totalTimeRecorded % 60;

        String minutesFormatted = String.format("%02d", minutes);
        String secondsFormatted = String.format("%02d", seconds);
        recordingTimeTextView.setText(minutesFormatted + ":" + secondsFormatted);
    }

    @UiThread
    @Override
    public void setCategoryTips(List<Tip> categoryTips) {
        List<String> tips = new ArrayList<>();

        for(Tip tip: categoryTips){
            tips.add(tip.getValue());
        }

        tipsContainer.setTextItems(tips);
        tipsContainer.start();
    }

    @UiThread
    @Override
    public void onFlashToggled(boolean isOn) {
        if(isOn){
            toggleFlashImageView.setImageResource(R.drawable.new_pre_live_flash_on);

        }else{
            toggleFlashImageView.setImageResource(R.drawable.new_pre_live_flash_off);
        }
        toggleFlashImageView.invalidate();
    }

    @Override
    public void onRecordingStarted() {
        presenter.onRecordingStarted();
    }


    @Override
    public void onRecordingContinued(long currentRecordingTime) {
        presenter.onRecordingContinued(currentRecordingTime);
    }

    @Override
    public void onActivityPause() {
        onPauseTapped();
    }

    @Override
    public boolean canHandleBackPressed() {
        return true;
    }

    @Override
    public void handleBackPressed() {
        listener.onCancelRecordingRequested();
    }
}