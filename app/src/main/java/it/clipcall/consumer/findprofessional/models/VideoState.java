package it.clipcall.consumer.findprofessional.models;

import android.content.Context;

import it.clipcall.consumer.findprofessional.presenters.CameraPresenter;
import it.clipcall.consumer.findprofessional.support.CameraPreviewLayout;
import it.clipcall.consumer.findprofessional.views.ICameraView;

/**
 * Created by micro on 1/11/2016.
 */
public abstract class VideoState {

    protected final ICameraView view;
    protected final CameraPresenter presenter;

    public VideoState(ICameraView view, CameraPresenter cameraPresenter) {
        this.view = view;
        this.presenter = cameraPresenter;
    }

   public abstract void activate(Context context, CameraPreviewLayout cameraPreviewLayout);

    public abstract void nextState();
}
