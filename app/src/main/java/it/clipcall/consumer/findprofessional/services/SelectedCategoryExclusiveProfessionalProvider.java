package it.clipcall.consumer.findprofessional.services;

import com.google.common.base.Strings;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.invitations.services.BranchIoManager;
import it.clipcall.consumer.findprofessional.models.ExclusiveProRef;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by dorona on 14/04/2016.
 */
@Singleton
public class SelectedCategoryExclusiveProfessionalProvider implements IExclusiveProfessionalProvider {

    private final AuthenticationManager authenticationManager;
    private final BranchIoManager branchIoManager;
    private final ProfessionalsManager professionalsManager;

    @Inject
    public SelectedCategoryExclusiveProfessionalProvider(AuthenticationManager authenticationManager, BranchIoManager branchIoManager, ProfessionalsManager professionalsManager){
        this.authenticationManager = authenticationManager;

        this.branchIoManager = branchIoManager;
        this.professionalsManager = professionalsManager;
    }

    @Override
    public ExclusiveProRef getExclusivePro() {
        String customerId = authenticationManager.getCustomerId();
        if(Strings.isNullOrEmpty(customerId))
            return null;

        Category lastCategory = professionalsManager.getLastCategory();
        if(lastCategory == null)
            return null;

        ExclusiveProRef exclusivePro = professionalsManager.getExclusiveProInCategory(customerId, lastCategory);
        return exclusivePro;
    }
}
