package it.clipcall.consumer.findprofessional.fragments;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.venmo.view.TooltipView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.findprofessional.models.Tip;
import it.clipcall.consumer.findprofessional.presenters.VideoPreviewPresenter;
import it.clipcall.consumer.findprofessional.support.adapters.CategoryTipsArrayAdapter;
import it.clipcall.consumer.findprofessional.views.ICameraPreviewView;
import it.clipcall.infrastructure.routing.models.RoutingContext;

@EFragment(R.layout.video_preview_fragment)
public class VideoPreviewFragment extends VideoRecordingBaseFragment implements ICameraPreviewView{

    @ViewById
    ViewGroup tipsContainer;

    @ViewById
    ListView toolTipsListView;

    @ViewById
    ViewGroup recordingButtonContainer;
    @ViewById
    TooltipView toolTipView;
    @ViewById
    Button recordingButton;


    @Inject
    VideoPreviewPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.bindView(this);
    }

    @AfterViews
    void afterViews(){
        toolTipView.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.ZoomIn)
                .duration(700)
                .playOn(toolTipView);
        toolTipsListView.setVisibility(View.INVISIBLE);

        presenter.initialize();
    }

    @Click(R.id.recordingButton)
    void startRecording(){
        listener.onStartRecordingRequested();
//        presenter.startVideoRecording();
//        EventBus.getDefault().post(this,"startRecordingTapped");
    }

    @Override
    public void onStartRecording() {

    }

    @UiThread
    @Override
    public void updateCategoryTips(List<Tip> categoryTips) {
        if(toolTipsListView != null){
            toolTipsListView.setVisibility(View.VISIBLE);
            CategoryTipsArrayAdapter tipsAdapter = new CategoryTipsArrayAdapter(getActivity(),R.layout.consumer_video_tooltip_item, categoryTips);
            toolTipsListView.setAdapter(tipsAdapter);
        }
    }

    @Override
    public RoutingContext getRoutingContext() {
        return null;
    }

    @Override
    public boolean canHandleBackPressed() {
        return true;
    }

    @Override
    public void handleBackPressed() {
        listener.onCancelRecordingFromPreviewRequested();
    }
}
