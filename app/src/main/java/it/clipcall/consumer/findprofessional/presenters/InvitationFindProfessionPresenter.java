package it.clipcall.consumer.findprofessional.presenters;

/**
 * Created by dorona on 01/02/2016.
 */
/**
public class InvitationFindProfessionPresenter extends FindProfessionalPresenter{


    private ExclusiveProRef exclusivePro;

    public InvitationFindProfessionPresenter(FindProfessionalController controller, RoutingService routingService, PermissionsService permissionsService) {
        super(controller, routingService, permissionsService);
    }

    @Override
    public void initialize() {
        view.showOrHideCategoryProgress(true);
        super.initialize();
        view.setTooltipText(R.string.show_us_what_you_need_exclusive_pro_tooltip_text);
        updateExclusiveProCategory();
        view.showOrHideCategoryProgress(false);
    }

    @Override
    public void startVideoCore(boolean shouldPromptExclusive, boolean isPrivate) {

        this.exclusivePro = controller.getExclusiveProForInvitationInSelectedCategory();
        if(exclusivePro != null && shouldPromptExclusive){
            EventBus.getDefault().post(new MixPanelEvent(ConsumerEventNames.ClipCallItInvitation, exclusivePro));
            view.promptSendingToExclusiveAdvertiser(exclusivePro);
            return;
        }

        TempProjectData projectData = new TempProjectData();

        if(exclusivePro != null && isPrivate){
            projectData.setSupplierId(exclusivePro.getAccountId());
            projectData.setSupplierName(exclusivePro.getAccountName());

        }
        boolean createdProjectSuccessfully = controller.createTemporaryProject(projectData);
        if(createdProjectSuccessfully){
            navigateToVideo();
        }
    }




    @Override
    public void updateLastCategory(Category category) {
        super.updateLastCategory(category);
        exclusivePro = controller.getExclusiveProForInvitationInSelectedCategory();

    }

    private void updateExclusiveProCategory() {
        final ExclusivePro exclusivePro = controller.getExclusivePro();
        if(exclusivePro == null)
            return;

        this.exclusivePro = controller.getExclusiveProForInvitationInSelectedCategory();

        final List<Category> availableCategories = controller.getAvailableCategories();

        for(int i = 0; i < availableCategories.size(); ++i ){
            Category category = availableCategories.get(i);
            if(category.getCategoryId().equals(exclusivePro.getCategoryId())){
                controller.updateLastCategory(category);
                view.setRecentlySelectedCategory(category, i);
                view.setTooltipText(R.string.show_us_what_you_need_exclusive_pro_tooltip_text);
                return;
            }
        }
    }
}
**/