package it.clipcall.consumer.findprofessional.controllers;

import com.google.common.base.Strings;

import org.modelmapper.ModelMapper;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.invitations.models.eInvitationType;
import it.clipcall.common.invitations.services.BranchIoManager;
import it.clipcall.common.services.MetadataProvider;
import it.clipcall.consumer.findprofessional.models.AddressEntity2;
import it.clipcall.consumer.findprofessional.models.ExclusivePro;
import it.clipcall.consumer.findprofessional.models.ExclusiveProRef;
import it.clipcall.consumer.findprofessional.models.TempProjectData;
import it.clipcall.consumer.findprofessional.services.ProfessionalsManager;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by dorona on 24/01/2016.
 */
@Singleton
public class FindProfessionalController {

    private final MetadataProvider metadataProvider;
    private final ModelMapper modelMapper;
    private final AuthenticationManager authenticationManager;
    private final BranchIoManager  branchIoManager;
    private final ProfessionalsManager professionalsManager;


    private ExclusivePro exclusivePro;


    @Inject
    public FindProfessionalController(MetadataProvider metadataProvider, BranchIoManager branchIoManager, ProfessionalsManager professionalsManager, ModelMapper modelMapper, AuthenticationManager authenticationManager) {
        this.metadataProvider = metadataProvider;
        this.branchIoManager = branchIoManager;
        this.professionalsManager = professionalsManager;
        this.modelMapper = modelMapper;
        this.authenticationManager = authenticationManager;
    }

    public List<Category> getAvailableCategories() {
        return metadataProvider.getAvailableCategories();
    }

    public ExclusivePro getExclusivePro(){

        boolean hasInvitation = branchIoManager.hasInvitation(eInvitationType.Video);
        if(!hasInvitation)
            return null;

        String invitationId = branchIoManager.getInvitationId();
        if(Strings.isNullOrEmpty(invitationId))
            return null;

        ExclusivePro exclusivePro = professionalsManager.getExclusivePro(invitationId);
        this.exclusivePro =  exclusivePro;
        return this.exclusivePro;

        //return professionalsManager.getExclusivePro();
    }


    public void updateLastCategory(Category category) {
        professionalsManager.updateLastCategory(category);
    }



    public AddressEntity2 getLastAddress(){
        AddressEntity2 addressEntity = professionalsManager.getLastAddress();
        return addressEntity;
    }

    public boolean createTemporaryProject(TempProjectData projectData) {
        boolean success = professionalsManager.createTemporaryProject(projectData);
        return success;
    }


    public ExclusiveProRef getExclusiveProInSelectedCategory() {

       /* ExclusiveProRef pro = new ExclusiveProRef();
        pro.setAccountName("RegisQA Mobile New 126");
        pro.setAccountId("c6d73b19-a5d8-47a8-9f89-1e4579cf7a61");
        pro.setImageUrl("https://s3.amazonaws.com/mobileclipcall2/qa_supplier/c6d73b19-a5d8-47a8-9f89-1e4579cf7a61/Logo_429.png");
        pro.setInvitationId("7c2b67d9-6868-4612-9219-a788ccf1569d");
        return pro;*/

       String customerId = authenticationManager.getCustomerId();
        if(Strings.isNullOrEmpty(customerId))
            return null;

        Category lastCategory = professionalsManager.getLastCategory();
        if(lastCategory == null)
            return null;

        ExclusiveProRef exclusivePro = professionalsManager.getExclusiveProInCategory(customerId, lastCategory);
        return exclusivePro;
    }

    public ExclusiveProRef getExclusiveProForInvitationInSelectedCategory() {

        String invitationId = branchIoManager.getInvitationId();
        if(Strings.isNullOrEmpty(invitationId))
            return null;

        Category lastCategory = professionalsManager.getLastCategory();
        if(lastCategory == null)
            return null;

        ExclusiveProRef exclusivePro = professionalsManager.getExclusiveProForInvitationInCategory(invitationId, lastCategory);
        return exclusivePro;
    }

    public void clearLastCategory() {
        professionalsManager.clearLastCategory();
    }

    public Category getLastCategory(){
        return professionalsManager.getLastCategory();
    }

    public void clearLastAddress() {
        professionalsManager.clearLastAddress();
    }

    public void updateLastAddress(AddressEntity2 addressEntity) {
        professionalsManager.updateLastAddress(addressEntity);
    }
}
