package it.clipcall.consumer.findprofessional.controllers;

import com.google.common.base.Strings;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.common.invitations.services.BranchIoManager;
import it.clipcall.consumer.findprofessional.models.NewProjectEntity;
import it.clipcall.consumer.findprofessional.services.ProfessionalsManager;

/**
 * Created by dorona on 26/01/2016.
 */
@Singleton
public class VideoRecordingController {


    private final AuthenticationManager authenticationManager;
    private final ProfessionalsManager professionalsManager;

    private final BranchIoManager branchIoManager;

    @Inject
    public VideoRecordingController(AuthenticationManager authenticationManager, BranchIoManager branchIoManager, ProfessionalsManager professionalsManager) {
        this.authenticationManager = authenticationManager;
        this.branchIoManager = branchIoManager;
        this.professionalsManager = professionalsManager;
    }

    public NewProjectEntity getProject() {
        return professionalsManager.getTemporaryProject();
    }

    public boolean submitProject() {
        boolean customerAuthenticated = authenticationManager.isCustomerAuthenticated();
        if(customerAuthenticated == false)
            return false;

        String customerId = authenticationManager.getCustomerId();
        String projectId = professionalsManager.submitProject(customerId);
        if(Strings.isNullOrEmpty(projectId))
            return false;


        professionalsManager.clearTempProject();
        return true;

    }

    public boolean canSubmitVideo(){
        return professionalsManager.canSubmitVideo();
    }

    public boolean setExclusiveProForInvitation() {
        boolean customerAuthenticated = authenticationManager.isCustomerAuthenticated();
        if(customerAuthenticated == false)
            return false;

        String customerId = authenticationManager.getCustomerId();
        if(Strings.isNullOrEmpty(customerId))
            return false;


        String invitationId = branchIoManager.getInvitationId();
        if(Strings.isNullOrEmpty(invitationId))
            return false;

        branchIoManager.clearInvitation();
        boolean success = professionalsManager.setExclusiveProForInvitation(customerId, invitationId);
        return success;

    }

    public void clearTempProject() {
        professionalsManager.clearTempProject();;
    }
}
