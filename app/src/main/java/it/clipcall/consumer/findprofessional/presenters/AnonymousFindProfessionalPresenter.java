package it.clipcall.consumer.findprofessional.presenters;

/**
 * Created by dorona on 01/02/2016.
 */
/**
public class AnonymousFindProfessionalPresenter extends FindProfessionalPresenter {


    public AnonymousFindProfessionalPresenter(FindProfessionalController controller, RoutingService routingService, PermissionsService permissionsService) {
        super(controller, routingService, permissionsService);
    }

    @Override
    public void initialize() {
        super.initialize();

        view.setTooltipText(R.string.show_us_what_you_need_tooltip_text);
    }

    @Override
    public void startVideoCore(boolean shouldPromptExclusive, boolean isPrivate) {
        TempProjectData projectData = new TempProjectData();
        boolean createdProjectSuccessfully = controller.createTemporaryProject(projectData);
        if(createdProjectSuccessfully){
            navigateToVideo();
        }

    }
}
*/