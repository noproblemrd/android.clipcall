package it.clipcall.consumer.findprofessional.presenters;

import android.util.Log;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import it.clipcall.consumer.findprofessional.models.NewProjectEntity;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.consumer.findprofessional.models.LogTag;
import it.clipcall.consumer.findprofessional.models.Tip;
import it.clipcall.consumer.findprofessional.models.events.VideoRecordingPausedEvent;
import it.clipcall.consumer.findprofessional.services.ClipCallManager;
import it.clipcall.consumer.findprofessional.services.ProfessionalsManager;
import it.clipcall.consumer.findprofessional.views.IVideoRecordingView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.services.RoutingService;

@PerActivity
public class VideoRecordingPresenter extends PresenterBase<IVideoRecordingView> {


    private final ClipCallManager clipCallManager;
    private final ProfessionalsManager professionalsManager;
    private final RoutingService routingService;

    private Timer timer;

    private long totalTimeRecorded;

    @Inject
    public VideoRecordingPresenter(ClipCallManager clipCallManager,ProfessionalsManager professionalsManager,  RoutingService routingService) {
        this.clipCallManager = clipCallManager;
        this.professionalsManager = professionalsManager;
        this.routingService = routingService;
    }

    @RunOnUiThread
    public void startRecordingIfNotStartedAlready(){
        if(!clipCallManager.isRecordingStarted()){
            clipCallManager.startRecording();
        }
    }

    @Override
    public void initialize() {
        EventBus.getDefault().register(this);
        Category category = professionalsManager.getLastCategoryOrFromProject();
        if(category == null)
            return;


        NewProjectEntity temporaryProject = professionalsManager.getTemporaryProject();
        if(temporaryProject == null)
            return;


        List<Tip> categoryTips = professionalsManager.getCategoryTips(category.getCategoryId());
        view.setCategoryTips(categoryTips);


    }

    @Override
    public void shutDown() {
        super.shutDown();
        EventBus.getDefault().unregister(this);
    }

    @RunOnUiThread
    public void flipCamera() {
        clipCallManager.flipCamera();
    }

    @RunOnUiThread
    public void toggleFlash() {
        boolean isOn = clipCallManager.setFlashOnOff();
        view.onFlashToggled(isOn);
    }

//    public void pauseVideoRecording() {
//        clipCallManager.pauseVideo();
//
//        EventBus.getDefault().post(this, "sendVideoTapped");
//    }


//    @Subscribe
//    void onVideoPreviewingFinished(VideoPreviewingCompletedEvent videoEvent){
//        Log.d(LogTag.value,"video previewing completed event caught by VideoRecordingPresenter" );
//        Log.d(LogTag.value,"about to start video recording" );
//        clipCallManager.startRecording();
//    }


    public void onRecordingStarted(){
        totalTimeRecorded = 0;
        Log.d(LogTag.value,"video recording started event caught by VideoRecordingPresenter" );
        releaseTimer();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                totalTimeRecorded++;
                view.updateVideoRecordingTime(totalTimeRecorded);
            }
        }, 0, 1000);
    }


    public void onRecordingContinued(long currentRecordingTime){
        Log.d(LogTag.value,"video recording continued event caught by VideoRecordingPresenter" );
        totalTimeRecorded = currentRecordingTime;
        releaseTimer();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.d("videoRecordingPresenter", "timer activated");
                totalTimeRecorded++;
                view.updateVideoRecordingTime(totalTimeRecorded);
            }
        }, 0, 1000);
    }


    private final TimerTask timerTast = new TimerTask() {
        @Override
        public void run() {
            Log.d("videoRecordingPresenter", "timer activated");
            totalTimeRecorded++;
            view.updateVideoRecordingTime(totalTimeRecorded);
            if(totalTimeRecorded > 120){
                releaseTimer();

            }
        }
    };

    @Subscriber
    void onRecordingPaused(VideoRecordingPausedEvent videoEvent){
        Log.d(LogTag.value,"video recording paused event caught by VideoRecordingPresenter" );

        releaseTimer();
    }


    void releaseTimer(){
        if(timer != null)
        {
            timer.cancel();
            timer = null;
        }
    }

    public void pauseVideoRecording() {
        releaseTimer();
    }

    public long getCurrentRecordingTime() {
        return this.totalTimeRecorded;
    }
}
