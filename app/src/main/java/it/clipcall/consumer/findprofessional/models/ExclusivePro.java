package it.clipcall.consumer.findprofessional.models;

/**
 * Created by dorona on 31/01/2016.
 */
public class ExclusivePro {

    private String supplierId;
    private String supplierName;
    private String text;
    private String supplierImage;
    private Integer invitationType;
    private Integer checkInStatus;
    private String categoryId;
    private String categoryName;

    /**
     *
     * @return
     * The supplierName
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     *
     * @param supplierName
     * The supplierName
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    /**
     *
     * @return
     * The text
     */
    public String getText() {
        return text;
    }

    /**
     *
     * @param text
     * The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     *
     * @return
     * The supplierImage
     */
    public String getSupplierImage() {
        return supplierImage;
    }

    /**
     *
     * @param supplierImage
     * The supplierImage
     */
    public void setSupplierImage(String supplierImage) {
        this.supplierImage = supplierImage;
    }

    /**
     *
     * @return
     * The invitationType
     */
    public Integer getInvitationType() {
        return invitationType;
    }

    /**
     *
     * @param invitationType
     * The invitationType
     */
    public void setInvitationType(Integer invitationType) {
        this.invitationType = invitationType;
    }

    /**
     *
     * @return
     * The checkInStatus
     */
    public Integer getCheckInStatus() {
        return checkInStatus;
    }

    /**
     *
     * @param checkInStatus
     * The checkInStatus
     */
    public void setCheckInStatus(Integer checkInStatus) {
        this.checkInStatus = checkInStatus;
    }

    /**
     *
     * @return
     * The categoryId
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     *
     * @param categoryId
     * The categoryId
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     *
     * @return
     * The categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     *
     * @param categoryName
     * The categoryName
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        supplierId = supplierId;
    }
}
