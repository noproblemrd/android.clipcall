package it.clipcall.consumer.findprofessional.services;

import it.clipcall.consumer.findprofessional.models.ExclusiveProRef;

/**
 * Created by dorona on 14/04/2016.
 */
public interface IExclusiveProfessionalProvider {



    ExclusiveProRef getExclusivePro();
}
