package it.clipcall.consumer.findprofessional.views;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by micro on 1/12/2016.
 */
public interface ISendingVideoRecordingView extends IView {


    void setLocatingText(int resourceId);
}
