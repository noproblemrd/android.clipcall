package it.clipcall.consumer.findprofessional.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import it.clipcall.R;
import it.clipcall.consumer.findprofessional.models.ExclusiveProRef;
import it.clipcall.infrastructure.ui.images.CircleTransform;

@EViewGroup(R.layout.exclusive_pro_submission_layout)
public class ExclusiveProSubmissionView extends FrameLayout {

    @ViewById
    TextView exclusiveProTextView;

    @ViewById
    ImageView exclusiveProImageView;
    private int messageResourceId;

    public ExclusiveProSubmissionView(Context context) {
        this(context, null);
    }

    public ExclusiveProSubmissionView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExclusiveProSubmissionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public void setExclusivePro(ExclusiveProRef exclusivePro){
        String formattedMessage = getContext().getResources().getString(messageResourceId);
        String message = String.format(formattedMessage, exclusivePro.getAccountName());
        exclusiveProTextView.setText(message);

        Picasso.with(getContext()).load(exclusivePro.getImageUrl())
                .fit()
                .transform(new CircleTransform())
                .into(exclusiveProImageView);
    }

    public void setMessageResource(int messageResource) {
        this.messageResourceId = messageResource;
    }
}
