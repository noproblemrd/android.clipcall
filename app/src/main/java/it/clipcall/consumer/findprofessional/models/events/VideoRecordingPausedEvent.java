package it.clipcall.consumer.findprofessional.models.events;

/**
 * Created by dorona on 26/01/2016.
 */
public class VideoRecordingPausedEvent extends VideoEvent {
    public VideoRecordingPausedEvent(long recordingTime) {
        this.recordingTime = recordingTime;
    }

    public long recordingTime;
}
