package it.clipcall.consumer.findprofessional.models;

/**
 * Created by dorona on 01/02/2016.
 */
public class ExclusiveProRef {

    private String accountName;
    private String accountId;
    private String imageUrl;
    private String invitationId;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
       this.accountId = accountId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
    public String getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(String invitationId) {
        this.invitationId = invitationId;
    }




}
