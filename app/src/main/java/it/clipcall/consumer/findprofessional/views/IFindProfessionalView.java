package it.clipcall.consumer.findprofessional.views;

import java.util.List;

import it.clipcall.consumer.findprofessional.models.AddressEntity2;
import it.clipcall.consumer.findprofessional.models.ExclusiveProRef;
import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.profile.models.Category;

/**
 * Created by dorona on 24/01/2016.
 */
public interface IFindProfessionalView  extends IView{
    void setAvailableCategories(List<Category> availableCategories);

    void setRecentlySelectedAddress(AddressEntity2 lastAddress);

    void promptSendingToExclusiveAdvertiser(ExclusiveProRef exclusivePro);

    void setTooltipText(int resourceId);



    void showOrHideZipCodeProgress(boolean show);

    void showRequiredCategory(boolean requestFocus);

    void showRequiredZipCode(boolean requestFocus);

    void onAddressUpdated(String address);

}
