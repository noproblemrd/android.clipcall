package it.clipcall.consumer.findprofessional.support.filters;

import java.util.Comparator;

import it.clipcall.professional.profile.models.Category;

/**
 * Created by micro on 2/2/2016.
 */
public  class CategoryByNameComparator implements Comparator<Category> {


    private final String prefix;

    public CategoryByNameComparator(String prefix){

        this.prefix = prefix;
    }
    @Override
    public int compare(Category lhs, Category rhs) {
        return lhs.getCategoryName().toLowerCase().compareTo(rhs.getCategoryName().toLowerCase());
    }
}