package it.clipcall.consumer.findprofessional.models;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by dorona on 24/01/2016.
 */
public class AddressEntity implements Serializable{




    private Locale mLocale;

    private String mFeatureName;
    private HashMap<Integer, String> mAddressLines;

    public Locale getLocale() {
        return mLocale;
    }

    public void setLocale(Locale locale) {
        mLocale = locale;
    }

    public String getFeatureName() {
        return mFeatureName;
    }

    public void setFeatureName(String featureName) {
        mFeatureName = featureName;
    }

    public HashMap<Integer, String> getAddressLines() {
        return mAddressLines;
    }

    public void setAddressLines(HashMap<Integer, String> addressLines) {
        mAddressLines = addressLines;
    }

    public int getMaxAddressLineIndex() {
        return mMaxAddressLineIndex;
    }

    public void setMaxAddressLineIndex(int maxAddressLineIndex) {
        mMaxAddressLineIndex = maxAddressLineIndex;
    }

    public String getAdminArea() {
        return mAdminArea;
    }

    public void setAdminArea(String adminArea) {
        mAdminArea = adminArea;
    }

    public String getSubAdminArea() {
        return mSubAdminArea;
    }

    public void setSubAdminArea(String subAdminArea) {
        mSubAdminArea = subAdminArea;
    }

    public String getLocality() {
        return mLocality;
    }

    public void setLocality(String locality) {
        mLocality = locality;
    }

    public String getSubLocality() {
        return mSubLocality;
    }

    public void setSubLocality(String subLocality) {
        mSubLocality = subLocality;
    }

    public String getThoroughfare() {
        return mThoroughfare;
    }

    public void setThoroughfare(String thoroughfare) {
        mThoroughfare = thoroughfare;
    }

    public String getSubThoroughfare() {
        return mSubThoroughfare;
    }

    public void setSubThoroughfare(String subThoroughfare) {
        mSubThoroughfare = subThoroughfare;
    }

    public String getPremises() {
        return mPremises;
    }

    public void setPremises(String premises) {
        mPremises = premises;
    }

    public String getPostalCode() {
        return mPostalCode;
    }

    public void setPostalCode(String postalCode) {
        mPostalCode = postalCode;
    }

    public String getCountryCode() {
        return mCountryCode;
    }

    public void setCountryCode(String countryCode) {
        mCountryCode = countryCode;
    }

    public String getCountryName() {
        return mCountryName;
    }

    public void setCountryName(String countryName) {
        mCountryName = countryName;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public boolean isHasLatitude() {
        return mHasLatitude;
    }

    public void setHasLatitude(boolean hasLatitude) {
        mHasLatitude = hasLatitude;
    }

    public boolean isHasLongitude() {
        return mHasLongitude;
    }

    public void setHasLongitude(boolean hasLongitude) {
        mHasLongitude = hasLongitude;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    private int mMaxAddressLineIndex = -1;
    private String mAdminArea;
    private String mSubAdminArea;
    private String mLocality;
    private String mSubLocality;
    private String mThoroughfare;
    private String mSubThoroughfare;
    private String mPremises;
    private String mPostalCode;
    private String mCountryCode;
    private String mCountryName;
    private double mLatitude;
    private double mLongitude;
    private boolean mHasLatitude = false;
    private boolean mHasLongitude = false;
    private String mPhone;
    private String mUrl;
}
