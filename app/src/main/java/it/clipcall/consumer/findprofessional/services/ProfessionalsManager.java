package it.clipcall.consumer.findprofessional.services;

import com.google.common.base.Strings;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.findprofessional.models.AddressEntity2;
import it.clipcall.infrastructure.support.network.RetrofitCallProcessor;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.consumer.findprofessional.models.AddressEntity;
import it.clipcall.consumer.findprofessional.models.ExclusivePro;
import it.clipcall.consumer.findprofessional.models.ExclusiveProRef;
import it.clipcall.consumer.findprofessional.models.NewProjectEntity;
import it.clipcall.consumer.findprofessional.models.TempProjectData;
import it.clipcall.consumer.findprofessional.models.Tip;
import it.clipcall.consumer.projects.services.ICustomerProjectsService;
import it.clipcall.infrastructure.CreateProjectStatusResponse;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.infrastructure.repositories.Repository2;
import retrofit2.Call;
import retrofit2.Response;


/**
 * Created by micro on 12/4/2015.
 */
@Singleton
public class ProfessionalsManager {


    private final HashMap<String,List<Tip>> categoryTips;
    private final Map<String,ExclusiveProRef> categoryExclusivePros = new HashMap<>();

    private final List<Tip> defaultCategoryTips = new ArrayList<>();

    private final Repository2 repository;
    private final ICustomerProjectsService customerProjectsService;

    @Inject
    public ProfessionalsManager( Repository2 repository, ICustomerProjectsService customerProjectsService){
        this.repository = repository;
        this.customerProjectsService = customerProjectsService;
        this.categoryTips = new HashMap<>();
    }


    public void setDefaultCategoryTips(String[] categoryTips){
        for(String tipValue: categoryTips){
            Tip tip = new Tip();
            tip.setValue(tipValue);
            tip.setTitle(tipValue);
            defaultCategoryTips.add(tip);
        }
    }

    public boolean createTemporaryProject(TempProjectData tempProjectData){
        Category lastCategory = getLastCategory();
        if(lastCategory == null)
            return false;

        AddressEntity2 lastAddress = getLastAddress();
        if(lastAddress == null)
            return false;

        NewProjectEntity project = new NewProjectEntity();
        project.setCategoryId(lastCategory.getCategoryId());
        project.setCategoryName(lastCategory.getCategoryName());
        project.setValue(lastCategory.getValue());
        project.setZipCode(lastAddress.getZipCode());
        project.setFullAddress(lastAddress.getAddress());

        if (!Strings.isNullOrEmpty(tempProjectData.getSupplierId())) {
            project.setIsPrivate(true);
            project.setFavoriteSupplierIds(new String[]{tempProjectData.getSupplierId()});
            project.setSupplierName(tempProjectData.getSupplierName());
        }

        NewProjectEntity existingProject = repository.getSingle(NewProjectEntity.class);
        if(existingProject!=null)
            repository.delete(existingProject);

        repository.add(project);
        return true;
    }

    public void updateTemporaryProjectWithVideoUrl(String videoUrl, String  videoPreviewUrl, int videoDurationInSeconds){
        NewProjectEntity existingProject = repository.getSingle(NewProjectEntity.class);
        if(existingProject == null)
            return;

        existingProject.setVideoLeadUrl(videoUrl);
        existingProject.setPreviewPicUrl(videoPreviewUrl);
        existingProject.setVideoDuration(videoDurationInSeconds);
        repository.update(existingProject);
    }


    public void updateLastCategory(Category category){
        Category existingCategory  = repository.getSingle(Category.class);
        if(existingCategory != null){
            repository.delete(existingCategory);
        }
        repository.add(category);
    }

    public Category getLastCategory(){
        Category lastCategory  = repository.getSingle(Category.class);
        return lastCategory;
    }




    public List<Tip> getCategoryTips(String categoryId){

        try {
            if(categoryTips.containsKey(categoryId))
            {
                List<Tip> tips  = this.categoryTips.get(categoryId);
                return tips;
            }

            Response<List<Tip>> tipsResponse = customerProjectsService.getCategoryTips(categoryId).execute();
            if(tipsResponse == null)
                return defaultCategoryTips;


            List<Tip> tips = tipsResponse.body();
            if(tips == null || tips.size() == 0)
                return defaultCategoryTips;


            categoryTips.put(categoryId, tips);

            return tips;

        } catch (IOException e) {
            return defaultCategoryTips;
        }
    }



    public AddressEntity2 getLastAddress(){
        return repository.getSingle(AddressEntity2.class);
    }

    public NewProjectEntity getTemporaryProject() {
        NewProjectEntity project = repository.getSingle(NewProjectEntity.class);
        return project;
    }

    public String submitProject(String customerId) {
        NewProjectEntity project = repository.getSingle(NewProjectEntity.class);
        if(project == null)
            return null;

        Call<CreateProjectStatusResponse> createProjectCall = customerProjectsService.createProject(customerId, project);
        CreateProjectStatusResponse createProjectStatusResponse = RetrofitCallProcessor.processCall(createProjectCall);
        if(createProjectStatusResponse == null)
            return null;

        if(Strings.isNullOrEmpty(createProjectStatusResponse.status))
            return null;

        if(!createProjectStatusResponse.status.toUpperCase().equals("OK"))
            return null;

        return createProjectStatusResponse.leadId;

    }

    public void clearTempProject() {
        NewProjectEntity temporaryProject = getTemporaryProject();
        if(temporaryProject == null)
            return;


        temporaryProject.setPreviewPicUrl(null);
        temporaryProject.setVideoLeadUrl(null);
        temporaryProject.setVideoDuration(0);

        repository.update(temporaryProject);
    }

    public ExclusivePro getExclusivePro(String invitationId) {
        Call<ExclusivePro> exclusiveProCall = customerProjectsService.getExclusivePro(invitationId);
        ExclusivePro exclusivePro = RetrofitCallProcessor.processCall(exclusiveProCall);
        return exclusivePro;
    }

    public ExclusiveProRef getExclusiveProForInvitationInCategory(String invitationId, Category category) {
        try {
            Response<ExclusiveProRef> createProjectResponse = customerProjectsService.getExclusiveProForCategory(invitationId, category.getCategoryId()).execute();
            if(createProjectResponse == null)
                return null;

            ExclusiveProRef exclusivePro = createProjectResponse.body();
            if(exclusivePro == null)
                return null;

            return exclusivePro;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ExclusiveProRef getExclusiveProInCategory(String customerId, Category category) {
        Call<List<ExclusiveProRef>> exclusiveProsCall = customerProjectsService.getCustomerExclusiveProfessionals(customerId, category.getCategoryId());
        List<ExclusiveProRef> exclusivePros = RetrofitCallProcessor.processCall(exclusiveProsCall);
        if(exclusivePros == null)
            return null;

        if(exclusivePros.size() == 0)
            return null;

        return exclusivePros.get(0);
    }

    public boolean setExclusiveProForInvitation(String customerId, String invitationId) {
        Call<StatusResponse> statusResponseCall = customerProjectsService.setExclusiveProForInvitation(customerId, invitationId);
        boolean success = RetrofitCallProcessor.processStatusCall(statusResponseCall);
        return success;
    }

    public void clearLastCategory() {
        Category existingCategory  = repository.getSingle(Category.class);
        if(existingCategory != null){
            repository.delete(existingCategory);
        }
    }

    public void clearLastAddress() {
        AddressEntity currentAddress = repository.getSingle(AddressEntity.class);
        if(currentAddress != null){
            repository.delete(currentAddress);
        }
    }

    public boolean canSubmitVideo() {
        NewProjectEntity temporaryProject = getTemporaryProject();
        if(temporaryProject == null)
            return false;

        return !Strings.isNullOrEmpty(temporaryProject.getVideoLeadUrl());
    }

    public Category getLastCategoryOrFromProject() {
        Category lastCategory = getLastCategory();
        if(lastCategory != null)
           return lastCategory;

        NewProjectEntity project = getTemporaryProject();
        if(project == null)
            return null;

        Category category = new Category();
        category.setCategoryId(project.getCategoryId());
        category.setCategoryName(project.getCategoryName());
        return category;
    }

    public void updateLastAddress(AddressEntity2 addressEntity) {
        AddressEntity2 currentAddress = repository.getSingle(AddressEntity2.class);
        if(currentAddress != null){
            repository.delete(currentAddress);
        }

        repository.add(addressEntity);
    }
}
