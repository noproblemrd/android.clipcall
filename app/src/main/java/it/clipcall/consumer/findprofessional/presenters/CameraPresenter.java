package it.clipcall.consumer.findprofessional.presenters;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceView;

import com.emoze.tildaLib.Database.AlbumItemInfo;
import com.emoze.tildaLib.intrf.ELCPCameraListener;
import com.emoze.tildaLib.intrf.ELCPClientBroadcastListener;
import com.google.gson.Gson;

import org.simple.eventbus.EventBus;

import javax.inject.Inject;

import it.clipcall.consumer.findprofessional.controllers.CameraController;
import it.clipcall.consumer.findprofessional.models.ExclusivePro;
import it.clipcall.consumer.findprofessional.models.events.VideoDataAvailableEvent;
import it.clipcall.consumer.findprofessional.models.events.VideoRecordingFinishedEvent;
import it.clipcall.consumer.findprofessional.models.events.VideoRecordingStartedEvent;
import it.clipcall.consumer.findprofessional.views.ICameraView;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;

@PerActivity
public class CameraPresenter extends PresenterBase<ICameraView> implements ELCPClientBroadcastListener, ELCPCameraListener {
    private final String TAG= "EMOZE";
    private final CameraController controller;
    private final RoutingService routingService;
    private final Gson gson;
    private boolean isCameraOpened = false;


    private boolean isCameraPermissionsGranted = false;

    private boolean isActivityPaused;

    @Inject
    public CameraPresenter(CameraController controller, RoutingService routingService, Gson gson) {
        this.controller = controller;
        this.routingService = routingService;
        this.gson = gson;
    }


    @RunOnUiThread
    public void initialize(){
        controller.initialize();
    }

    @RunOnUiThread
    public void openCamera(){
        if(!isCameraPermissionsGranted)
            return;

        if(isCameraOpened)
            return;

        Log.d(TAG, "openCamera: ");
        Camera camera = controller.openCamera();
        isCameraOpened = true;

        view.onCameraOpened(camera);
    }

    @RunOnUiThread
    public void startPreviewAndContinueRecording() {
        if(!isCameraPermissionsGranted)
            return;

        Log.d(TAG, "startPreviewAndContinueRecording: ");
       controller.startPreviewAndContinueRecording();
    }

    public void stopRecording()
    {
        Log.d(TAG, "stopRecording: ");
        controller.stopRecording();
    }

    @RunOnUiThread
    public void startPreview(Context context, SurfaceView surfaceView)
    {
        if(!isCameraPermissionsGranted)
            return;

        Log.d(TAG, "startPreview: ");
        controller.initializeClient(context, null, this);
        controller.initializeCameraClient(surfaceView, this);
    }

    @Override
    public void AsyncBroadcastResponse(AlbumItemInfo albumItemInfo, int status) {
        String albumInfoJson = gson.toJson(albumItemInfo);

        Log.d(TAG, "AsyncBroadcastResponse: status - " + status + ", albumInfoJson - " + albumInfoJson);
        if(status == 0){

            EventBus.getDefault().post(new VideoDataAvailableEvent(albumItemInfo.URL,albumItemInfo.PreviewURL));
        }
    }

    @Override
    public void OnCameraError ( final int error)
    {
        Log.d(TAG, "OnCameraError: errorCode - " + error);
    }

    @Override
    public void OnCameraRecordingStarted ()
    {
        Log.i(TAG, "OnCameraRecordingStarted");
        EventBus.getDefault().post(new VideoRecordingStartedEvent());

    }

    @Override
    public void OnCameraSurfaceDestroyed ()
    {
        Log.i(TAG, "OnCameraSurfaceDestroyed");
    }

    @Override
    public void OnCameraRecordingFinished ()
    {
        Log.d(TAG, "OnCameraRecordingFinished: ");
        EventBus.getDefault().post(new VideoRecordingFinishedEvent());
    }

    @Override
    public boolean IsContinue ()
    {
        return isActivityPaused;
    }

    @Override
    public void OnCameraCriticalError ()
    {
        Log.d(TAG, "OnCameraCriticalError: ");

    }

    @Override
    public void OnCameraSegmentSent ( boolean cameraSessionStartSent)
    {
        Log.d(TAG, "OnCameraSegmentSent: ");
    }

    @Override
    public void CameraConnected ()
    {
        Log.d(TAG, "CameraConnected: ");
        controller.setCameraConnected();
    }

    @Override
    public void PreviewReady(boolean success, final String fullpath)
    {
        Log.d(TAG," PreviewReady - success: "+ success + " full path: "+ fullpath);
        if(!success)
            return;

        view.previewVideoRecording(fullpath);
    }

    @RunOnUiThread
    public void openCameraOrStartPreviewAndContinueRecording() {
        if(!isCameraPermissionsGranted)
            return;

        Log.d(TAG, "openCameraOrStartPreviewAndContinueRecording: ");
        if(isActivityPaused)
            startPreviewAndContinueRecording();
        else
            openCamera();


        isActivityPaused = false;
    }


    @RunOnUiThread
    public void startVideoRecording() {
        Log.d(TAG, "startVideoRecording: ");
        controller.startVideoRecording();
    }

    @RunOnUiThread
    public void pauseVideoRecording() {
        Log.d(TAG, "pauseVideoRecording: ");
        controller.pauseVideo();
    }

    @RunOnUiThread
    public void continueVideoRecording() {
        Log.d(TAG, "continueVideoRecording: ");
        controller.continueVideoRecording();
    }

    @RunOnUiThread
    public void submitVideoRecording() {
        Log.d(TAG, "submitVideoRecording: ");
        controller.stopVideoRecording();
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();
        routingService.routeTo(Routes.ConsumerSubmitProjectToAdvertiserRoute, navigationContext);
    }

    public void updateTempVideoData(String videoUrl, String previewImageUrl) {
        Log.d(TAG, "updateTempVideoData: ");
        controller.updateTempVideoData(videoUrl, previewImageUrl);
    }

    @RunOnUiThread
    public void setIsActivityPaused(boolean isActivityPaused) {
        Log.d(TAG, "setIsActivityPaused: ");
        this.isActivityPaused = isActivityPaused;
    }
    @RunOnUiThread
    public void releaseResources() {
        Log.d(TAG, "releaseResources: ");
        isActivityPaused = false;
        controller.releaseResources();
    }
    @RunOnUiThread
    public void reviewTempProject(Activity context) {
        Log.d(TAG, "reviewTempProject: ");
        boolean success = controller.prepareForPreviewVideo();

    }


    public boolean isCameraPermissionsGranted() {
        return isCameraPermissionsGranted;
    }
    @RunOnUiThread
    public void setCameraPermissionsGranted(boolean cameraPermissionsGranted) {
        this.isCameraPermissionsGranted = cameraPermissionsGranted;
    }

    public void loadProfessionalWithInvitation(String invitationId) {
        view.showLoadingProfessionalForInvitation(invitationId);
        ExclusivePro pro  = controller.getProfessionalForInvitation(invitationId);
        if(pro == null){
            view.showInvitationInvalid();
            return;
        }

        boolean createdProjectSuccessfully = controller.createTemporaryProject(pro);
        if(createdProjectSuccessfully)
            view.showProfessional(pro);

    }

    public void routeToHome() {
        routingService.routeToHome(view.getRoutingContext());
    }

    public void clearInvitation() {
        controller.clearInvitation();
    }
}
