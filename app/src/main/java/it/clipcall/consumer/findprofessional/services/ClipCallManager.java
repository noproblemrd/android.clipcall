package it.clipcall.consumer.findprofessional.services;

import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceView;

import com.amazonaws.regions.Regions;
import com.emoze.tildaLib.Database.AlbumItemInfo;
import com.emoze.tildaLib.ELCPCamera;
import com.emoze.tildaLib.ELCPClient;
import com.emoze.tildaLib.Media.Camera.CameraClientBase;
import com.emoze.tildaLib.datatypes.CloudLocation;
import com.emoze.tildaLib.exceptions.ELCPGenException;
import com.emoze.tildaLib.intrf.ELCPCameraListener;
import com.emoze.tildaLib.intrf.ELCPClientBroadcastListener;
import com.emoze.tildaLib.intrf.ELCPClientListener;

import org.simple.eventbus.EventBus;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.findprofessional.models.events.VideoRecordingContinuedEvent;
import it.clipcall.consumer.findprofessional.models.events.VideoRecordingPausedEvent;

@Singleton
public class ClipCallManager {
    private final String amazonAccessPath = "d1wc6cu8i70qi6.cloudfront.net";
    private final String amazonBucketName = "mobileclipcall2";
    private final String amazonBucketRootFolder = "ClipCallConsumersVideos";

    private static final String TAG = "EMOZE";

    int numberOfCameras;
    long lastRecordingTime;
    private Camera camera;


    int cameraCurrentlyLocked;
    int defaultCameraId;

    private ELCPCamera elcpCamera = null;

    private boolean isCameraConnected = false;


    private ELCPClient elcpClient = null;

    private boolean isRecordingStarted = false;

    private AlbumItemInfo currentAlbum = null;

    @Inject
    public ClipCallManager() {
    }

    public void initialize() {

        Log.d(TAG, "initialize");

        releaseResources();

        numberOfCameras = Camera.getNumberOfCameras();
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        for (int i = 0; i < numberOfCameras; i++)
        {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
            {
                defaultCameraId = i;
            }
        }

        currentAlbum = new AlbumItemInfo();
        currentAlbum.ID = new Date().getTime();
        currentAlbum.MediaType = AlbumItemInfo.MEDIA_TYPE_HLS;
        currentAlbum.UserID = 16;
    }

    public void openCamera() {
        Log.d(TAG, "openCamera: ");
        camera = Camera.open();
        cameraCurrentlyLocked = defaultCameraId;

    }

    public Camera getCamera() {
        return camera;
    }

    public void releaseCamera() {
        Log.d(TAG, "releaseCamera: ");
        if (camera != null)
        {
            camera.release();
            camera = null;
        }
    }

    public void startPreviewAndContinueRecording() {
        Log.d(TAG, "startPreviewAndContinueRecording: ");
        if (elcpCamera != null)
        {

           try{
               elcpCamera.StartPreview();
           }
           catch (Exception e){
               Log.e(TAG, "startPreviewAndContinueRecording: ",e );
           }

            try{
                elcpCamera.ContinueRecord();
                isRecordingStarted = true;
            }
            catch (Exception e){
                Log.e(TAG, "startPreviewAndContinueRecording: ",e );
            }

        }
    }

    public void startRecording() {
        Log.d(TAG, "startRecording: ");
        if (isCameraConnected)
        {
            elcpCamera.StartRecord(ELCPClient.ORIENTATION_PORTRET);
            isRecordingStarted = true;
        }
    }

    public void stopRecording() {
        Log.d(TAG, "stopRecording: ");
        if (elcpCamera != null)
        {
            lastRecordingTime = elcpCamera.GetRecordingTime();
            elcpCamera.CloseSession();
            elcpCamera = null;
            isRecordingStarted = false;
        }
    }

    public void setCameraConnected() {
        Log.d(TAG, "setCameraConnected: ");
        isCameraConnected = true;
    }

    public String getCurrentUrl() {
        Log.d(TAG, "getCurrentUrl: ");
        if(elcpCamera != null)
            return elcpCamera.GetCurrentUrl();
        return "";
    }

    public void pauseVideo() {
        Log.d(TAG, "pauseVideo: ");
        if (elcpCamera != null)
        {
            if (elcpCamera.GetState() == CameraClientBase.CAMERA_STARTED)
            {
                elcpCamera.Pause();
                EventBus.getDefault().post(new VideoRecordingPausedEvent(elcpCamera.GetRecordingTime()));

            }
        }
    }

    public boolean isRecordingStarted(){
        return isRecordingStarted;
    }

    public boolean setFlashOnOff() {
        if (elcpCamera != null)
            return elcpCamera.SetFlashOnOff();
        return false;
    }

    public void setMute() {
        if (elcpCamera != null)
            elcpCamera.SetMute();
    }

    public boolean flipCamera() {
        if (elcpCamera != null)
            return elcpCamera.FlipCamera();
        return false;
    }

    public boolean isBackCamera() {
        if (elcpCamera != null)
            return elcpCamera.IsBackCamera();
        return false;
    }

    public long getRecordingTime() {
        if (elcpCamera != null)
            return elcpCamera.GetRecordingTime();
        return lastRecordingTime;
    }

    public boolean isInPause() {
        Log.d(TAG, "isInPause: ");
        if (elcpCamera != null)
            return elcpCamera.IsInPause();
        return false;
    }

    public void resumeCamera() {
        Log.d(TAG, "resumeCamera: ");
        if (elcpCamera != null)
        {
            elcpCamera.Resume();
            EventBus.getDefault().post(new VideoRecordingContinuedEvent());

        }
    }

    public int getCurrentColorEffect() {
        if (elcpCamera != null)
            return elcpCamera.GetCurrentColorEffect();
        return 0;
    }

    public void setColorEffect(int colorEffect) {
        if (elcpCamera != null)
            elcpCamera.SetColorEffect(colorEffect);
    }

    public boolean getHdMode() {
        if (elcpCamera != null)
            return elcpCamera.GetHDMode();
        return false;
    }

    public boolean getFlashOnOff() {
        if (elcpCamera != null)
            return elcpCamera.GetFlashOnOff();
        return false;
    }

    public boolean changeHdMode() {
        if (elcpCamera != null)
            return elcpCamera.ChangeHDMode();
        return false;
    }

    public int getBitRate() {
        if (elcpCamera != null)
            return elcpCamera.GetBitrate();
        return 0;
    }

    public Point getResolution() {
        if (elcpCamera != null)
            return elcpCamera.GetResolution();
        else
            return new Point(0, 0);
    }

    public int getFrameRate() {
        if (elcpCamera != null)
            return elcpCamera.GetFrameRate();
        else
            return 0;
    }

    public boolean getMute() {
        if (elcpCamera != null)
            return elcpCamera.GetMute();
        else
            return false;
    }

    public void initializeClient(Context context, ELCPClientListener clientListener, ELCPClientBroadcastListener broadcastListener) {
        Log.d(TAG, "initializeClient: ");

        if (elcpClient == null)
        {
            try
            {
                CloudLocation cloudLocation = new CloudLocation(amazonBucketName, amazonBucketRootFolder, amazonAccessPath , Regions.US_EAST_1, false);
                elcpClient = new ELCPClient(ELCPClient._MANNER_OF_USE.AS_EXTERNAL_SDK,15 /*userId*/, "152425364565635"/*deviceId*/, context, clientListener, broadcastListener , "sGrp45vnVf45m2", cloudLocation);

            } catch (ELCPGenException e)
            {
                e.printStackTrace();
            }
        }

    }


    public void initializeCameraClient(SurfaceView surfaceView, ELCPCameraListener cameraListener) {
        Log.d(TAG, "initializeCameraClient: ");
        if (elcpCamera == null)
        {
            isCameraConnected = false;

            try
            {
                elcpCamera = elcpClient.GetCamera(surfaceView, camera, true, ELCPClient.ORIENTATION_PORTRET , currentAlbum, cameraListener);
                elcpCamera.StartPreview();
            }
            catch (ELCPGenException e)
            {
                Log.e("Foo", "GetCamera EXEPTION " + e.toString());
            }
        }
    }

    private void resetData() {
        Log.d(TAG, "resetData: ");

        numberOfCameras = 0;
        lastRecordingTime = 0;
        cameraCurrentlyLocked = 0;
        defaultCameraId = 0;
        isCameraConnected = false;
        isRecordingStarted = false;
        currentAlbum = null;
    }

    private void releaseElcpClient() {
        Log.d(TAG, "releaseElcpClient: ");
        if(elcpClient !=  null){
            elcpClient = null;
        }
    }

    private void releaseElcpCamera() {
        Log.d(TAG, "releaseElcpCamera: ");
        if(elcpCamera != null){
            elcpCamera.CloseSession();
            elcpCamera = null;
        }
    }

    public void releaseResources() {
        Log.d(TAG, "releaseResources: ");
        releaseCamera();
        releaseElcpClient();
        releaseElcpCamera();
        resetData();
    }

    public boolean prepareForPreviewVideo() {
        Log.d(TAG, "prepareForPreviewVideo: ");
        if(!isInPause())
            return false;
        if (elcpCamera == null)
            return false;

        elcpCamera.PrepareForPreviewVideo();
        return true;
    }
}
