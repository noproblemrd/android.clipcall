package it.clipcall.consumer.findprofessional.fragments;

import android.app.Activity;
import android.content.Context;

import it.clipcall.consumer.findprofessional.support.IVideoRecordingListener;
import it.clipcall.infrastructure.fragments.BaseFragment;

/**
 * Created by dorona on 28/01/2016.
 */
public abstract class VideoRecordingBaseFragment extends BaseFragment {

    protected IVideoRecordingListener listener;

    protected final int maxRecordingTime = 120;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (IVideoRecordingListener)context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public void onRecordingContinued(long currentRecordingTime){

    }

    public void onRecordingPaused(long currentRecordingTime){

    }

    public void onRecordingStarted(){

    }

    public void onRecordingFinished() {

    }

    public void onActivityPause(){

    }

    public abstract boolean canHandleBackPressed();

    public void handleBackPressed(){

    }
}
