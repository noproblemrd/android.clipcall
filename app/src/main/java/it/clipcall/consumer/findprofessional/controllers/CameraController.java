package it.clipcall.consumer.findprofessional.controllers;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceView;

import com.emoze.tildaLib.intrf.ELCPCameraListener;
import com.emoze.tildaLib.intrf.ELCPClientBroadcastListener;
import com.emoze.tildaLib.intrf.ELCPClientListener;
import com.google.common.base.Strings;

import org.simple.eventbus.EventBus;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.invitations.services.BranchIoManager;
import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.services.MetadataProvider;
import it.clipcall.consumer.findprofessional.models.ExclusivePro;
import it.clipcall.consumer.findprofessional.models.NewProjectEntity;
import it.clipcall.consumer.findprofessional.services.ClipCallManager;
import it.clipcall.consumer.findprofessional.services.ProfessionalsManager;
import it.clipcall.infrastructure.repositories.Repository2;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by dorona on 28/01/2016.
 */
@Singleton
public class CameraController {

    private final String TAG= "EMOZE";
    private final ClipCallManager clipCallManager;
    private final ProfessionalsManager professionalsManager;
    private final AuthenticationManager authenticationManager;
    private final MetadataProvider metadataProvider;
    private final BranchIoManager branchIoManager;
    private final Repository2 repository;

    @Inject
    public CameraController(ClipCallManager clipCallManager,
                            ProfessionalsManager professionalsManager,
                            AuthenticationManager authenticationManager,
                            MetadataProvider metadataProvider,
                            BranchIoManager branchIoManager,
                            Repository2 repository) {
        this.clipCallManager = clipCallManager;
        this.professionalsManager = professionalsManager;
        this.authenticationManager = authenticationManager;
        this.metadataProvider = metadataProvider;
        this.branchIoManager = branchIoManager;
        this.repository = repository;
    }

    public void initialize() {

        clipCallManager.initialize();
    }

    public Camera openCamera() {
        clipCallManager.openCamera();
        Camera camera = clipCallManager.getCamera();
        return camera;
    }

    public void releaseCamera() {
        clipCallManager.releaseCamera();
    }

    public void startPreviewAndContinueRecording() {
        clipCallManager.startPreviewAndContinueRecording();
    }

    public void stopRecording() {
        clipCallManager.stopRecording();
    }

    public void initializeClient(Context context,ELCPClientListener elcpClientListener, ELCPClientBroadcastListener elcpClientBroadcastListener) {
        clipCallManager.initializeClient(context, elcpClientListener, elcpClientBroadcastListener);
    }

    public void initializeCameraClient(SurfaceView surfaceView, ELCPCameraListener elcpCameraListener) {
        clipCallManager.initializeCameraClient(surfaceView, elcpCameraListener);
    }

    public void setCameraConnected() {
        clipCallManager.setCameraConnected();
    }

    public void pauseVideo() {
        EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.FIND_PRO_PAUSE_VIDEO_RECORDING));
        clipCallManager.pauseVideo();
    }

    public void resumeCamera() {
        clipCallManager.resumeCamera();
    }

    public int getCurrentColorEffect() {
        return clipCallManager.getCurrentColorEffect();
    }

    public boolean isInPaused() {
        return clipCallManager.isInPause();
    }

    public void startVideoRecording() {
        if(!clipCallManager.isRecordingStarted()){
            EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.FIND_PRO_START_VIDEO_RECORDING));
            clipCallManager.startRecording();
        }
    }



    public void continueVideoRecording() {
        if(clipCallManager.isInPause()){
            EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.FIND_PRO_CONTINUE_VIDEO_RECORDING));
            clipCallManager.resumeCamera();
        }

    }

    public void stopVideoRecording() {
        EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.FIND_PRO_SENDING_VIDEO));
        clipCallManager.stopRecording();
    }

    public boolean canSubmitVideo(){
        return professionalsManager.canSubmitVideo();
    }

    public void updateTempVideoData(String videoUrl, String previewImageUrl) {
        long videoDurationInMilliseconds = clipCallManager.getRecordingTime();
        int videoDurationInSeconds = (int) videoDurationInMilliseconds / 1000;
        professionalsManager.updateTemporaryProjectWithVideoUrl(videoUrl, previewImageUrl, videoDurationInSeconds);
    }

    public void releaseResources() {
        clipCallManager.releaseResources();
    }

    public boolean prepareForPreviewVideo() {
        Log.d(TAG, "Preparing for previewing video");
        boolean success = clipCallManager.prepareForPreviewVideo();
        Log.d(TAG, "Preparing for previewing video - success: "+ success);
        return success;
    }

    public ExclusivePro getProfessionalForInvitation(String invitationId) {
        if(Strings.isNullOrEmpty(invitationId))
            return null;

        ExclusivePro exclusivePro = professionalsManager.getExclusivePro(invitationId);
        return exclusivePro;
    }


    public boolean createTemporaryProject(ExclusivePro professional) {
        NewProjectEntity project = new NewProjectEntity();
        project.setCategoryId(professional.getCategoryId());
        project.setCategoryName(professional.getCategoryName());
        //project.setValue(category.getValue());
        project.setIsPrivate(true);
        //project.setZipCode(lastAddress.getPostalCode());
        project.setFavoriteSupplierIds(new String[]{professional.getSupplierId()});
        project.setSupplierName(professional.getSupplierName());
        NewProjectEntity existingProject = repository.getSingle(NewProjectEntity.class);
        if(existingProject!=null)
            repository.delete(existingProject);

        repository.add(project);
        return true;
    }

    public void clearInvitation() {
        branchIoManager.clearInvitation();
    }
}
