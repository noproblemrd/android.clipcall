package it.clipcall.consumer.findprofessional.presenters;

import org.simple.eventbus.EventBus;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.consumer.findprofessional.controllers.VideoRecordingController;
import it.clipcall.consumer.findprofessional.models.NewProjectEntity;
import it.clipcall.consumer.findprofessional.views.ISendingVideoRecordingView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.services.RoutingService;

/**
 * Created by dorona on 25/01/2016.
 */
@PerActivity
public class SendingVideoRecordingPresenter extends PresenterBase<ISendingVideoRecordingView> {

    private final RoutingService routingService;
    private final VideoRecordingController controller;

    @Inject
    public SendingVideoRecordingPresenter(RoutingService routingService, VideoRecordingController controller) {

        this.routingService = routingService;
        this.controller = controller;
    }

    @Override
    public void initialize() {
        NewProjectEntity project = controller.getProject();
        if (project.isPrivate()) {
            view.setLocatingText(R.string.location_single_pro);
            controller.setExclusiveProForInvitation();
        }else{
            view.setLocatingText(R.string.location_pros_text);
        }

        boolean submittedSuccessfully = controller.submitProject();
        controller.clearTempProject();
        if(submittedSuccessfully){
            EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.FIND_PRO_LAND_ON_PROJECTS_SUCCESS, project));

            routingService.routeToHome(view.getRoutingContext());
        }
        else{
            EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.FIND_PRO_LAND_ON_PROJECTS_FAILURE, project));
            routingService.routeToHome(view.getRoutingContext());
        }
    }
}
