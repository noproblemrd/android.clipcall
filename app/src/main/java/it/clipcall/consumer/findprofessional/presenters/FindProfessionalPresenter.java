package it.clipcall.consumer.findprofessional.presenters;

import android.Manifest;
import android.location.Address;
import android.os.AsyncTask;

import org.simple.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.consumer.findprofessional.controllers.FindProfessionalController;
import it.clipcall.consumer.findprofessional.models.AddressEntity2;
import it.clipcall.consumer.findprofessional.models.ExclusiveProRef;
import it.clipcall.consumer.findprofessional.models.TempProjectData;
import it.clipcall.consumer.findprofessional.views.IFindProfessionalView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.maps.entities.PlaceData;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.AddressResolver;
import it.clipcall.infrastructure.ui.permissions.IPermissionsHandler;
import it.clipcall.infrastructure.ui.permissions.PermissionContext;
import it.clipcall.infrastructure.ui.permissions.PermissionsService;
import it.clipcall.professional.profile.models.Category;

@PerActivity
public class FindProfessionalPresenter extends PresenterBase<IFindProfessionalView> {
    protected final FindProfessionalController controller;
    protected final RoutingService routingService;
    private final PermissionsService permissionsService;

    private final AddressResolver addressResolver;

    ExclusiveProRef exclusivePro;

    @Inject
    public FindProfessionalPresenter(FindProfessionalController controller, RoutingService routingService, PermissionsService permissionsService, AddressResolver addressResolver) {
        this.controller = controller;
        this.routingService = routingService;
        this.permissionsService = permissionsService;
        this.addressResolver = addressResolver;
    }

    public void startVideoCore(boolean shouldPromptExclusive, boolean isPrivate){
        if(exclusivePro == null){
            exclusivePro = controller.getExclusiveProInSelectedCategory();
        }

        if(exclusivePro != null && shouldPromptExclusive){
            view.promptSendingToExclusiveAdvertiser(exclusivePro);
            return;
        }

        TempProjectData projectData = new TempProjectData();

        if(exclusivePro != null && isPrivate){
            projectData.setSupplierId(exclusivePro.getAccountId());
            projectData.setSupplierName(exclusivePro.getAccountName());
        }
        boolean createdProjectSuccessfully = controller.createTemporaryProject(projectData);
        if(createdProjectSuccessfully){
            navigateToVideo();
        }
    }

    @Override
    public void initialize() {
        final List<Category> availableCategories = controller.getAvailableCategories();
        view.setAvailableCategories(availableCategories);

        view.setTooltipText(R.string.show_us_what_you_need_tooltip_text);
        controller.clearLastCategory();

        view.showOrHideZipCodeProgress(true);
        getRecentAddress();
        view.showOrHideZipCodeProgress(false);
    }

    private void getRecentAddress() {

        AddressEntity2 lastAddress = controller.getLastAddress();
        if(lastAddress == null)
            return;

        view.setRecentlySelectedAddress(lastAddress);
    }

    public void updateLastCategory(Category category) {
        EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.ChooseCategory,category));
        controller.updateLastCategory(category);
    }


    protected void navigateToVideo(){
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();
        routingService.routeTo(Routes.ConsumerStartVideoProjectRoute,navigationContext);
    }

    public void clearLastCategory() {
        controller.clearLastCategory();
    }

    public void startVideoRecording(final boolean shouldPromptExclusive, final boolean isPrivate){
        int totalErrors = 0;
        boolean requestFocus;
        Category category = controller.getLastCategory();
        if(category == null)
        {
            totalErrors++;
            requestFocus = totalErrors == 1;
            view.showRequiredCategory(requestFocus);
            return;
        }

        AddressEntity2 address = controller.getLastAddress();
        if(address == null){
            totalErrors++;
            requestFocus = totalErrors == 1;
            view.showRequiredZipCode(requestFocus);
            return;
        }

        PermissionContext permissionContext = new PermissionContext("Permissions required","We need your permissions to record the video", Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        permissionsService.requestPermissions(permissionContext, new IPermissionsHandler() {
            @Override
            public void handleAllPermissionsGranted() {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        startVideoCore(shouldPromptExclusive, isPrivate);
                    }
                });
            }

            @Override
            public void handlePermissionDenied() {

            }
        });
    }

    public void clearLastAddress() {
        controller.clearLastAddress();
    }



    public void updateLastAddress(PlaceData place) {
        Address address = addressResolver.resolveAddress(place.latLng);
        if(address == null)
        {
            return;
        }

        AddressEntity2 addressEntity = new AddressEntity2();
        addressEntity.setAddress(place.address+"");
        addressEntity.setZipCode(address.getPostalCode());
        controller.updateLastAddress(addressEntity);
        EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.ChooseZipCode,addressEntity));;
        view.onAddressUpdated(place.address);
    }
}