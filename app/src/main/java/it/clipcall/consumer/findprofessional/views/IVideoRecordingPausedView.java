package it.clipcall.consumer.findprofessional.views;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 26/01/2016.
 */
public interface IVideoRecordingPausedView extends IView {
    void setLocatingText(int resourceId);

    void setLocatingText(int resourceId, String proName);
}
