package it.clipcall.consumer.profile.services;

import android.net.Uri;

import com.google.common.base.Strings;


import java.io.File;
import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.environment.Environment;
import it.clipcall.common.models.Device;
import it.clipcall.common.models.events.ConsumerProfileUpdatedEvent;
import it.clipcall.consumer.profile.models.CustomerProfile;
import it.clipcall.consumer.profile.models.CustomerSettings;
import it.clipcall.consumer.profile.models.FacebookProfile;
import it.clipcall.consumer.profile.models.Logo;
import it.clipcall.consumer.profile.models.TokenData;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.infrastructure.domainModel.services.IDomainService;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.repositories.Repository2;
import it.clipcall.infrastructure.support.network.RetrofitCallProcessor;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

@Singleton
public class CustomerProfileManager implements IDomainService {

    private final ICustomerProfileService profileService;
    private final Repository2 repository;
    private final EventAggregator eventAggregator;

    private final ILogger logger = LoggerFactory.getLogger(CustomerProfileManager.class.getName());

    private CustomerProfile profile;

    @Inject
    public CustomerProfileManager(ICustomerProfileService profileService, Repository2 repository, EventAggregator eventAggregator) {
        this.profileService = profileService;
        this.repository = repository;
        this.eventAggregator = eventAggregator;
    }

    public CustomerSettings getCustomerSettings(String customerId){
        try{
            if(Strings.isNullOrEmpty(customerId))
                return null;

            Response<CustomerSettings> settingsResponse = profileService.getSettings(customerId).execute();
            if(settingsResponse == null)
                return null;

            CustomerSettings settings = settingsResponse.body();
            if(settings == null)
                return null;

            return settings;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean updateCustomerSettings(String customerId, CustomerSettings settings) {
        if(Strings.isNullOrEmpty(customerId))
            return false;

        Call<StatusResponse> statusResponseCall = profileService.updateSettings(customerId, settings);

        boolean success = RetrofitCallProcessor.processStatusCall(statusResponseCall);
        return success;
    }

    public CustomerProfile getCustomerProfile(String customerId){
        try{

            if(this.profile != null)
                return this.profile;

            if(Strings.isNullOrEmpty(customerId))
                return null;

            Response<CustomerProfile> profileResponse = profileService.getProfile(customerId).execute();
            if(profileResponse == null)
                return null;

            CustomerProfile profile = profileResponse.body();
            if(profile == null)
               return null;

            this.profile = profile;
            return this.profile;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    public boolean updateCustomerProfileLogo(String customerId, File imageFile){


        try {
            if(Strings.isNullOrEmpty(customerId))
                return false;

            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
            Response<Logo> logoResponse = profileService.updateProfileLogo(customerId,requestBody).execute();
            if(logoResponse == null)
            {
                return false;
            }

            Logo logo = logoResponse.body();
            if(logo == null){
                return false;
            }


            String url = logo.getUrl();
            if(Strings.isNullOrEmpty(url))
            {
                return false;
            }

            profile.setImageUrl(url);
            eventAggregator.publish(new ConsumerProfileUpdatedEvent(profile));
            return true;


        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateCustomerProfileLogo(String customerId, Uri profileImageUrl) {
        String oldImageUrl = profile.getImageUrl();
        profile.setImageUrl(profileImageUrl.toString());
        Call<StatusResponse> statusResponseCall = profileService.updateProfile(customerId, profile);
        boolean success = RetrofitCallProcessor.processStatusCall(statusResponseCall);
        if(!success){
            profile.setImageUrl(oldImageUrl);
            return false;
        }

        eventAggregator.publish(new ConsumerProfileUpdatedEvent(profile));
        return true;

    }


    public boolean updateCustomerName(String customerId, String customerName){
        if(Strings.isNullOrEmpty(customerId))
            return false;

        if(Strings.isNullOrEmpty(customerName))
            return false;

        String oldName = profile.getName();
        profile.setName(customerName);

        Call<StatusResponse> statusResponseCall = profileService.updateProfile(customerId, profile);
        boolean success = RetrofitCallProcessor.processStatusCall(statusResponseCall);
        if(!success){
            profile.setName(oldName);
            return false;
        }
        eventAggregator.publish(new ConsumerProfileUpdatedEvent(profile));

        return true;
    }

    public String updateDeviceData(String customerId, String gcmToken, String versionName) {
        Device device = new Device();
        device.deviceName = android.os.Build.DEVICE;
        device.uid = gcmToken;
        device.npAppVersion = versionName;
        device.isDebug = Environment.IsDebug;
        device.deviceOs = "ANDROID";
        device.osVersion = System.getProperty("os.version");

        try {
            Response<TokenData> response = profileService.updateDevice(customerId, device).execute();
            if(response == null)
                return null;

            TokenData tokenData = response.body();
            if(tokenData == null)
                return null;

            return tokenData.token;

        } catch (Exception e) {
            return null;
        }

    }

    public boolean updateCustomerProfile(String customerId, FacebookProfile facebookProfile) {

        logger.debug("About to update customer profile for customer with id: %s and facebook profile %s", customerId, facebookProfile);

        CustomerProfile clonedOldProfile = CustomerProfile.clone(profile);
        clonedOldProfile.setEmail(facebookProfile.email);
        clonedOldProfile.setImageUrl(facebookProfile.getImageUrl());
        clonedOldProfile.setName(facebookProfile.name);
        clonedOldProfile.setFacebookAccessToken(facebookProfile.facebookAccessToken);
        clonedOldProfile.setFb_id(facebookProfile.id);


        logger.debug("Updating cloned profile %s", clonedOldProfile);

        Call<StatusResponse> statusResponseCall = profileService.updateProfile(customerId, clonedOldProfile);
        boolean success = RetrofitCallProcessor.processStatusCall(statusResponseCall);
        if(success){
            logger.debug("Updated profile %s successfully", clonedOldProfile);
            profile = clonedOldProfile;
            eventAggregator.publish(new ConsumerProfileUpdatedEvent(profile));
            return true;
        }

        logger.debug("Failed up; %s", clonedOldProfile);

        return false;
    }

    public void saveFacebookProfile(FacebookProfile facebookProfile){
        repository.update(facebookProfile);
    }

    public FacebookProfile getFacebookProfile(){
        return repository.getSingle(FacebookProfile.class);
    }
}
