package it.clipcall.consumer.profile.models;

/**
 * Created by dorona on 18/01/2016.
 */
public class Logo {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
