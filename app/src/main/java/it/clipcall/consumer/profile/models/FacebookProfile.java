package it.clipcall.consumer.profile.models;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by dorona on 07/06/2016.
 */
public class FacebookProfile implements Serializable{
    public String id;
    public String name;
    @SerializedName("first_name")
    public String firstName;
    @SerializedName("last_name")
    public String lastName;
    public String email;
    public String gender;
    public String facebookAccessToken;
    ;

    public String getImageUrl(){
        return "https://graph.facebook.com/" + id + "/picture?width=200&height=150";
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
