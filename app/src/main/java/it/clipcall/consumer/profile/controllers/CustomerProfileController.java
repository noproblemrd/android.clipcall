package it.clipcall.consumer.profile.controllers;

import android.net.Uri;
import android.util.Log;

import com.google.common.base.Strings;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.payment.controllers.PaymentCoordinator;
import it.clipcall.consumer.profile.models.FacebookProfile;
import it.clipcall.infrastructure.gcm.services.GcmTokenProvider;
import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.consumer.profile.models.CustomerProfile;
import it.clipcall.consumer.profile.services.CustomerProfileManager;

/**
 * Created by dorona on 18/01/2016.
 */
@Singleton
public class CustomerProfileController {

    private static final String TAG = "ProfileController";
    private final AuthenticationManager authenticationManager;
    private final CustomerProfileManager profileManager;
    private final GcmTokenProvider gcmTokenProvider;
    private final PaymentCoordinator paymentCoordinator;

    @Inject
    public CustomerProfileController(AuthenticationManager authenticationManager, CustomerProfileManager profileManager, GcmTokenProvider gcmTokenProvider, PaymentCoordinator paymentCoordinator) {
        this.authenticationManager = authenticationManager;
        this.profileManager = profileManager;
        this.gcmTokenProvider = gcmTokenProvider;
        this.paymentCoordinator = paymentCoordinator;
    }


    public CustomerProfile getCustomerProfile(){
        if(!authenticationManager.isCustomerAuthenticated())
            return null;

        String customerId = authenticationManager.getCustomerId();
        CustomerProfile profile = profileManager.getCustomerProfile(customerId);
        profile.setPhoneNumber(authenticationManager.getPhoneNumber());
        return profile;
    }

    public boolean updateCustomerName(String customerName){
        if(!authenticationManager.isCustomerAuthenticated())
            return false;

        String customerId = authenticationManager.getCustomerId();
        boolean updated = profileManager.updateCustomerName(customerId, customerName);
        return updated;
    }

    public String getCustomerPhoneNumber() {
        return authenticationManager.getPhoneNumber();
    }


    public boolean updateCustomerDevice(String versionName){

        Log.d(TAG, "updateCustomerDevice: updating customer device with gcm token: "+gcmTokenProvider.getGcmToken());
        if(!authenticationManager.isCustomerAuthenticated())
            return false;

        String customerId = authenticationManager.getCustomerId();
        String gcmToken = gcmTokenProvider.getGcmToken();
        if(Strings.isNullOrEmpty(gcmToken))
            return false;

        String token = profileManager.updateDeviceData(customerId, gcmToken, versionName);
        if(Strings.isNullOrEmpty(token))
            return false;

        boolean updatedToken = authenticationManager.updateToken(token);
        return updatedToken;

    }


    public boolean updateCustomerProfileLogo(File file){
        if(!authenticationManager.isCustomerAuthenticated())
            return false;

        String customerId = authenticationManager.getCustomerId();
        boolean updated = profileManager.updateCustomerProfileLogo(customerId, file);
        return updated;

    }

    public boolean updateCustomerProfileLogo(Uri profilePictureUri) {
        if(!authenticationManager.isCustomerAuthenticated())
            return false;

        String customerId = authenticationManager.getCustomerId();
        boolean updated = profileManager.updateCustomerProfileLogo(customerId, profilePictureUri);
        return updated;
    }

    public boolean updateCustomerProfile(FacebookProfile facebookProfile) {
        if(!authenticationManager.isCustomerAuthenticated())
            return false;

        String customerId = authenticationManager.getCustomerId();
        boolean updated = profileManager.updateCustomerProfile(customerId, facebookProfile);
        return updated;

    }

    public boolean claimDiscount() {
        return paymentCoordinator.claimDiscount(false);
    }

    public String getCustomerId() {
        return authenticationManager.getCustomerId();
    }
}
