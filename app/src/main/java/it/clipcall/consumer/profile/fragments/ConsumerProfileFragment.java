package it.clipcall.consumer.profile.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.login.widget.LoginButton;
import com.google.common.base.Strings;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.File;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.models.Urls;
import it.clipcall.consumer.mainMenu.support.FabContext;
import it.clipcall.consumer.mainMenu.support.ICollapsingToolBarListener;
import it.clipcall.consumer.mainMenu.views.IConsumerMainMenuView;
import it.clipcall.consumer.profile.models.CustomerProfile;
import it.clipcall.consumer.profile.models.FacebookProfile;
import it.clipcall.consumer.profile.presenters.CustomerProfilePresenter;
import it.clipcall.consumer.profile.views.ICustomerProfileView;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.routing.models.FragmentDecoratorRoutingContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.support.images.BitmapAssistant;


@EFragment(R.layout.consumer_profile)
public class ConsumerProfileFragment extends BaseFragment implements ICustomerProfileView, View.OnClickListener {

    private ProgressDialog progressDialog;
    private final ILogger logger = LoggerFactory.getLogger(ConsumerProfileFragment.class.getSimpleName());

    @Inject
    CustomerProfilePresenter presenter;

    @ViewById
    EditText customerPhoneNumberEditText;
    @ViewById
    EditText consumerNameEditText;

    @ViewById
    EditText emailEditText;

    @ViewById
    LoginButton facebookLoginButton;

    @ViewById
    ViewGroup consumerProfileContainer;

    @AfterViews
    void consumerProfileViewLoaded(){
        presenter.bindView(this);
        facebookLoginButton.setReadPermissions("public_profile", "email");
        // If using in a fragment
        facebookLoginButton.setFragment(this);
        presenter.registerFacebookLoginButton(facebookLoginButton);
        presenter.initialize();
    }

    public void updateProfile(FacebookProfile facebookProfile) {
        consumerNameEditText.setText(facebookProfile.name);
        String emailText = Strings.isNullOrEmpty(facebookProfile.email) ? "N/A" : facebookProfile.email;
        String profilePictureUrl = facebookProfile.getImageUrl();
        updateProfileImage(profilePictureUrl);
        if(!Strings.isNullOrEmpty(emailText)){
            emailEditText.setVisibility(View.VISIBLE);
            emailEditText.setText(emailText);
        }else{
            emailEditText.setText("");
            emailEditText.setVisibility(View.GONE);
        }


        presenter.updateCustomerProfile(facebookProfile);
    }

    @Override
    protected String getTitle() {
        return "Profile";
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.setFacebookActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void setName(String customerName) {
        consumerNameEditText.setText(customerName);
    }


    @Override
    public void setProfileImage(final String imageUrl) {
        updateProfileImage(imageUrl);
    }

    private void updateProfileImage(String imageUrl){
        if(Strings.isNullOrEmpty(imageUrl))
        {
            imageUrl = Urls.ConsumerMissingProfileImageUrl;
        }

        Context context = getActivity() ;
        if(context instanceof IConsumerMainMenuView){
            IConsumerMainMenuView consumerMainMenuView =  (IConsumerMainMenuView)context;
            consumerMainMenuView.setProfileImage(imageUrl);
        }

        if(context  instanceof ICollapsingToolBarListener){
            ICollapsingToolBarListener collapsingToolBarListener = (ICollapsingToolBarListener) context;
            collapsingToolBarListener.showImage(imageUrl);
            FabContext fabContext = new FabContext();
            fabContext.drawableResourceId = R.drawable.ic_add_a_photo_white_24dp;
            fabContext.listener = ConsumerProfileFragment.this;
            collapsingToolBarListener.setAction(fabContext);
        }
    }

    private void updateProfileImage(Bitmap image){
        if(image == null)
            return;

        Context context = getActivity() ;
        if(context instanceof IConsumerMainMenuView){
            IConsumerMainMenuView consumerMainMenuView =  (IConsumerMainMenuView)context;
            consumerMainMenuView.setProfileImage(image);
        }

        if(context  instanceof ICollapsingToolBarListener){
            ICollapsingToolBarListener collapsingToolBarListener = (ICollapsingToolBarListener) context;
            collapsingToolBarListener.showImage(image);
            FabContext fabContext = new FabContext();
            fabContext.drawableResourceId = R.drawable.ic_add_a_photo_white_24dp;
            fabContext.listener = ConsumerProfileFragment.this;
            collapsingToolBarListener.setAction(fabContext);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.updateCustomerName();

        if(getContext() instanceof IConsumerMainMenuView){
            IConsumerMainMenuView consumerMainMenuView =  (IConsumerMainMenuView)getContext();
            consumerMainMenuView.setCustomerName(presenter.getCurrentCustomerName());
        }
    }


    @Override
    public void setPhoneNumber(String phoneNumber) {
        customerPhoneNumberEditText.setText(phoneNumber);
    }


    @Override
    public void showErrorSelectingProfileImage() {
        Snackbar.make(consumerProfileContainer, "Failed updating profile image. Please try a different image" ,Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showProfile(CustomerProfile customerProfile) {
        consumerNameEditText.setText(customerProfile.getName());
        if(Strings.isNullOrEmpty(customerProfile.getEmail())){
            emailEditText.setText("");
            emailEditText.setVisibility(View.GONE);
        }else{
            emailEditText.setVisibility(View.VISIBLE);
            emailEditText.setText(customerProfile.getEmail());
        }
        String emailText = Strings.isNullOrEmpty(customerProfile.getEmail()) ? "N/A" : customerProfile.getEmail();
        emailEditText.setText(emailText);
        customerPhoneNumberEditText.setText(customerProfile.getPhoneNumber());
        String profilePictureUrl = customerProfile.getImageUrl();

        updateProfileImage(profilePictureUrl);
    }

    @Override
    public void showPickedImage(Bitmap image) {
        updateProfileImage(image);
        File file = BitmapAssistant.convertToFile(image);
        presenter.updateCustomerProfileLogo(file);
    }

    @Override
    public void showLoadingFacebookProfile(boolean show) {
        if(show){
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
            return;
        }

        if(progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }

    }


    @Override
    public RoutingContext getRoutingContext() {
        return new FragmentDecoratorRoutingContext(getActivity(), this);
    }

    @TextChange(R.id.consumerNameEditText)
    void updateCurrentCustomerName(CharSequence text, TextView hello, int before, int start, int count){
        if(text != null)
            presenter.setCurrentCustomerName(text.toString());
    }

    @Override
    protected int getMenuItemId() {
        return R.id.nav_user_profile_consumer;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.stopFacebookService();
    }

    @Override
    public void onClick(View v) {
        presenter.pickImage();
    }
}