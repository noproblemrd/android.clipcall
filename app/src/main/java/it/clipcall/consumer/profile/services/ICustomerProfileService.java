package it.clipcall.consumer.profile.services;



import it.clipcall.common.models.Device;
import it.clipcall.consumer.profile.models.CustomerProfile;
import it.clipcall.consumer.profile.models.CustomerSettings;
import it.clipcall.consumer.profile.models.Logo;
import it.clipcall.consumer.profile.models.TokenData;
import it.clipcall.infrastructure.StatusResponse;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;


public interface ICustomerProfileService {

    @GET("customers/{customerId}/settings")
    Call<CustomerSettings> getSettings(@Path("customerId") String  customerId);


    @POST("customers/{customerId}/settings")
    Call<StatusResponse> updateSettings(@Path("customerId") String  customerId, @Body CustomerSettings settings);

    @POST("customers/{customerId}/profile")
    Call<StatusResponse> updateProfile(@Path("customerId")String customerId, @Body CustomerProfile profile);

    @GET("customers/{customerId}/profile")
    Call<CustomerProfile> getProfile(@Path("customerId")String customerId);


    @Multipart
    @POST("customers/{customerId}/profile/logo")
    Call<Logo> updateProfileLogo(@Path("customerId")String customerId, @Part("myfile\"; filename=\"audioData.png\" ") RequestBody file);

    @POST("customers/{customerId}/devices")
    Call<TokenData> updateDevice(@Path("customerId")String customerId, @Body Device device);
}
