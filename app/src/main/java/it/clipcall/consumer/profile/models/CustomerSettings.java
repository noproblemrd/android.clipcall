package it.clipcall.consumer.profile.models;

/**
 * Created by dorona on 17/01/2016.
 */
public class CustomerSettings {

    public boolean isRecordMyCalls() {
        return recordMyCalls;
    }

    public void setRecordMyCalls(boolean recordMyCalls) {
        this.recordMyCalls = recordMyCalls;
    }

    private boolean recordMyCalls;
}
