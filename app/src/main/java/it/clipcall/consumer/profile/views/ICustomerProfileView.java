package it.clipcall.consumer.profile.views;

import android.graphics.Bitmap;

import it.clipcall.consumer.profile.models.CustomerProfile;
import it.clipcall.consumer.profile.models.FacebookProfile;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 18/01/2016.
 */
public interface ICustomerProfileView extends IView {

    void setName(String customerName);
    void setProfileImage(String imageUrl);

    void setPhoneNumber(String phoneNumber);

    void showErrorSelectingProfileImage();

    void showProfile(CustomerProfile customerProfile);

    void showPickedImage(Bitmap image);

    void showLoadingFacebookProfile(boolean show);

    void updateProfile(FacebookProfile facebookProfile);
}
