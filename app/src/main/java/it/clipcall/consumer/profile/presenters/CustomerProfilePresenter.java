package it.clipcall.consumer.profile.presenters;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.common.base.Strings;

import java.io.File;

import javax.inject.Inject;

import it.clipcall.common.thirdParty.FacebookService;
import it.clipcall.common.thirdParty.IFacebookProfileCallback;
import it.clipcall.consumer.profile.controllers.CustomerProfileController;
import it.clipcall.consumer.profile.models.CustomerProfile;
import it.clipcall.consumer.profile.models.FacebookProfile;
import it.clipcall.consumer.profile.views.ICustomerProfileView;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.commands.EmptyCallBack;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.ui.images.services.ImagePickerService;

@PerActivity
public class CustomerProfilePresenter extends PresenterBase<ICustomerProfileView> implements  FacebookCallback<LoginResult>, IFacebookProfileCallback {

    private final CustomerProfileController controller;
    private final FacebookService facebookService;
    private final ImagePickerService imagePickerService;
    private String currentCustomerName;

    @Inject
    public CustomerProfilePresenter(CustomerProfileController controller, FacebookService facebookService, ImagePickerService imagePickerService) {
        this.controller = controller;
        this.facebookService = facebookService;
        this.imagePickerService = imagePickerService;
    }

    @Override
    public void initialize() {
        super.initialize();
        facebookService.start();
        CustomerProfile customerProfile = controller.getCustomerProfile();
        if(view == null)
            return;

        view.showProfile(customerProfile);
    }

    public void setCurrentCustomerName(String name){
        this.currentCustomerName = name;
    }

    public void updateCustomerName() {
        CustomerProfile profile = controller.getCustomerProfile();
        if(profile == null)
            return;

        if(Strings.isNullOrEmpty(currentCustomerName))
            return;

        if(currentCustomerName.equals(profile.getName()))
            return;

        this.controller.updateCustomerName(currentCustomerName);

    }

    public String getCurrentCustomerName(){
        return currentCustomerName;
    }

    public String getCustomerName(){
        CustomerProfile profile = controller.getCustomerProfile();
        if(profile == null)
            return "";

        return profile.getName();
    }


    public void updateCustomerProfileLogo(File file){
        if(file == null){

            view.showErrorSelectingProfileImage();
            return;
        }

        boolean updated = controller.updateCustomerProfileLogo(file);
        if(view == null)
            return;

        if(!updated)
        {
            return;
        }

       /* CustomerProfile customerProfile = controller.getCustomerProfile();
        view.setProfileImage(customerProfile.getImageUrl());*/
    }

    /*public void pickImageFromGallery(final int requestCode) {
        PermissionContext permissionContext = new PermissionContext("Permissions needed", "We need your permissions to select a profile image", Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissionsService.requestPermissions(permissionContext, new IPermissionsHandler() {
            @Override
            public void handleAllPermissionsGranted() {
                routeToGallery(requestCode);
            }

            @Override
            public void handlePermissionDenied() {

            }
        });
    }*/

 /*   private void routeToGallery(final int requestCode){
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.addParameter("requestCode",requestCode+"");
        routingService.routeTo(Routes.CommonPickImageFromGalleryRoute,navigationContext);
    }*/

    /*public void captureImageFromCamera(final int requestCode) {
        PermissionContext permissionContext = new PermissionContext("Permissions required", "We need your permissions to take a profile image",Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissionsService.requestPermissions(permissionContext, new IPermissionsHandler() {
            @Override
            public void handleAllPermissionsGranted() {
                routeToCameraCapture(requestCode);
            }

            @Override
            public void handlePermissionDenied() {

            }
        });
    }*/

   /* private void routeToCameraCapture(int requestCode) {
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.addParameter("requestCode",requestCode+"");
        routingService.routeTo(Routes.CommonPickImageFromCameraRoute,navigationContext);
    }*/

    public void updateCustomerProfileLogo(Uri profilePictureUri) {
        boolean updated = controller.updateCustomerProfileLogo(profilePictureUri);
    }

    public void updateCustomerProfile(FacebookProfile facebookProfile) {
        boolean updated = controller.updateCustomerProfile(facebookProfile);
        if(updated){
            controller.claimDiscount();
        }
    }

    @RunOnUiThread
    public void pickImage() {
        imagePickerService.pickImage(new EmptyCallBack<Bitmap>(){

            @Override
            public void onSuccess(Bitmap result) {
                imagePicked(result);
            }
        });
    }

    void imagePicked(Bitmap result) {
        view.showPickedImage(result);
    }


    public void setFacebookActivityResult(int requestCode, int resultCode, final Intent data){
        facebookService.setFacebookActivityResult(requestCode, resultCode, data);
    }

    public void stopFacebookService() {
        facebookService.stop();
    }

    @RunOnUiThread
    public void registerFacebookLoginButton(LoginButton facebookLoginButton) {
        facebookService.registerFacebookLoginButton(facebookLoginButton, this);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        view.showLoadingFacebookProfile(true);
        facebookService.getProfile(loginResult.getAccessToken(),this);
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {

    }

    @Override
    public void onSuccess(FacebookProfile facebookProfile) {
        view.showLoadingFacebookProfile(false);
        view.updateProfile(facebookProfile);
        this.updateCustomerProfile(facebookProfile);
    }
}
