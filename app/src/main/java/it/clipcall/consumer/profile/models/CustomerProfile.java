package it.clipcall.consumer.profile.models;


public class CustomerProfile {

    private String facebookAccessToken;
    private String name;
    private String email;
    private String fb_id;
    private String imageUrl;
    private String phoneNumber;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static CustomerProfile clone(CustomerProfile source){
        CustomerProfile result = new CustomerProfile();
        if(source == null){
            return result;
        }

        result.setName(source.getName());
        result.setImageUrl(source.getImageUrl());
        result.setEmail(source.getEmail());
        return result;
    }

    public String getFacebookAccessToken() {
        return facebookAccessToken;
    }

    public void setFacebookAccessToken(String facebookAccessToken) {
        this.facebookAccessToken = facebookAccessToken;
    }

    public String getFb_id() {
        return fb_id;
    }

    public void setFb_id(String fb_id) {
        this.fb_id = fb_id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}

