package it.clipcall.consumer.mainMenu.views;

import android.graphics.Bitmap;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 31/12/2015.
 */
public interface IConsumerMainMenuView extends IView{


    void setProfileImage(String imageUrl);

    void setCustomerName(String text);

    void switchingMode();

    void modeSwitched();

    void finish();

    void setProfileImage(Bitmap image);
}
