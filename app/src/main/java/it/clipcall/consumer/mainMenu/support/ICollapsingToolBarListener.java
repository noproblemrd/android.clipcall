package it.clipcall.consumer.mainMenu.support;

import android.graphics.Bitmap;

/**
 * Created by micro on 1/29/2016.
 */
public interface ICollapsingToolBarListener {

    void showImage(String url);

    void showImage(Bitmap bitmap);

    void setAction(FabContext context);
}
