package it.clipcall.consumer.mainMenu.support;

/**
 * Created by micro on 1/29/2016.
 */
public interface ITitleListener {

    void onItemActivated(int menuItemId);
    void onTitleChanged(String newTitle);
}
