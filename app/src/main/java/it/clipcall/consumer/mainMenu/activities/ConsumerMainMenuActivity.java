package it.clipcall.consumer.mainMenu.activities;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.design.internal.NavigationMenu;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.commit451.nativestackblur.NativeStackBlur;
import com.google.common.base.Strings;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import io.intercom.android.sdk.Intercom;
import it.clipcall.R;
import it.clipcall.consumer.mainMenu.presenters.ConsumerMainMenuPresenter;
import it.clipcall.consumer.mainMenu.support.FabContext;
import it.clipcall.consumer.mainMenu.support.IBadgeListener;
import it.clipcall.consumer.mainMenu.support.ICollapsingToolBarListener;
import it.clipcall.consumer.mainMenu.support.ITitleListener;
import it.clipcall.consumer.mainMenu.views.IConsumerMainMenuView;
import it.clipcall.consumer.social.support.ShareWithFriendsListener;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.routing.models.FragmentRoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.professional.mainMenu.support.ResizeViewOnOffsetChangedListener;
import me.drakeet.materialdialog.MaterialDialog;
import me.kentin.yeti.Yeti;
import me.kentin.yeti.listener.OnShareListener;


@EActivity(R.layout.activity_main_menu)
public class ConsumerMainMenuActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, ITitleListener, IConsumerMainMenuView, IBadgeListener, ICollapsingToolBarListener, DrawerLayout.DrawerListener {

    private static final int REQUEST_CODE_YETI = 10;
    private boolean doubleBackToExitPressedOnce = false;

    @ViewById
    ProgressBar modeSwitchProgressBar;

    @ViewById
    TextView toggleModeTextView;

    @ViewById
    CircularImageView circularImageView;

    private final Handler handler = new Handler() ;

    private final  OnShareListener shareListener = new ShareWithFriendsListener(this);

    @ViewById
    Toolbar toolbar;

    @ViewById
    TextView badgeTextView;

    @ViewById
    ViewGroup badgeContainer;

    @ViewById
    FloatingActionButton fab;

    @ViewById
    AppBarLayout appBarLayout;


    @ViewById
    FloatingActionButton editFab;

    @ViewById(R.id.rootContainer)
    DrawerLayout drawer;

    @ViewById(R.id.nav_view)
    it.clipcall.professional.mainMenu.support.NavigationView_ navigationView;

    @ViewById
    ImageView blurredImageView;

    private ResizeViewOnOffsetChangedListener resizeViewOnOffsetChangedListener;


    @Inject
    ConsumerMainMenuPresenter presenter;

    @AfterViews
    protected void OnCreateCore(){
        presenter.bindView(this);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        drawer.addDrawerListener(this);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setIsPostRegistration(presenter.isProfessionalRegistered());
        navigationView.setMode("Consumer");

        //getSupportActionBar().setIcon(R.drawable.ic_launcher);
        resizeViewOnOffsetChangedListener = new ResizeViewOnOffsetChangedListener(circularImageView);

        initialize();
    }

    @Background
    void initialize(){
        presenter.initialize();
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.rootContainer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.consumer_additional_menu_items, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id == R.id.nav_share_with_friends_consumer){
            NavigationContext navigationContext = new NavigationContext();
            navigationContext.routingContext = new RoutingContext(ConsumerMainMenuActivity.this);
            navigationContext.addParameter("requestCode",Integer.toString(REQUEST_CODE_YETI));
            presenter.routeTo(Routes.ConsumerShareWithFriends, navigationContext);
            return true;
        }

        selectNavigationItem(item);

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int menuItemId = item.getItemId();
        if(menuItemId == R.id.nav_rate_us_consumer){

            final MaterialDialog materialDialog = new MaterialDialog(this);
            materialDialog.setTitle("Rate us")
                    .setMessage("We'd really appreciate you taking the time to rate us and review our app in Google Play. Thanks!")
                    .setPositiveButton("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            materialDialog.dismiss();
                            NavigationContext navigationContext = new NavigationContext();
                            navigationContext.routingContext = new RoutingContext(ConsumerMainMenuActivity.this);
                            presenter.routeTo(Routes.ConsumerRateUsRoute, navigationContext);
                        }
                    })
                    .setNegativeButton("CANCEL", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            materialDialog.dismiss();
                        }
                    });

            materialDialog.show();


            return true;
        }



        if(menuItemId == R.id.support_menu_item){
            Intercom.client().displayConversationsList();
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

        if(menuItemId == R.id.payYourProMenuItem){

            presenter.payYourPro();
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }


        selectNavigationItem(item);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    void selectNavigationItem(MenuItem item){
        int id = item.getItemId();




        String routeName = id + "";
        RoutingContext routingContext =  new FragmentRoutingContext(getSupportFragmentManager(),R.id.menu_fragment_container, this);

        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = routingContext;
        presenter.routeTo(routeName, navigationContext);

    }

    @Background
    @Click(R.id.toggleModeTextView)
    void switchToProfessionalMode(){
        presenter.switchToProfessionalMode();
    }


    @Override
    protected void onResume() {
        super.onResume();
        presenter.switchToConsumerMode();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_YETI) {
            Yeti yeti = Yeti.with(this);
            yeti.result(data, shareListener);
        }
    }

    @Override
    public void onItemActivated(int menuItemId) {
        if(navigationView == null)
            return;
        Menu menu = navigationView.getMenu();
        if(menu == null)
            return;

        MenuItem item = menu.findItem(menuItemId);
        if(item == null)
        {
            for(MenuItemImpl menuItem: ((NavigationMenu) menu).getNonActionItems()){
                menuItem.setChecked(false);
            }
            return;
        };

        if(item != null){
            item.setChecked(true);
            navigationView.invalidate();
        }

    }

    @Override
    public void onTitleChanged(String newTitle) {
        ActionBar supportActionBar = getSupportActionBar();
        if(supportActionBar == null)
            return;

        supportActionBar.setTitle(newTitle.toUpperCase());
    }

    @UiThread
    @Override
    public void setProfileImage(String imageUrl) {
        navigationView.setProfileImage(imageUrl);
    }

    @UiThread
    @Override
    public void setCustomerName(String name) {
        navigationView.setName(name);

    }

    @UiThread
    @Override
    public void switchingMode() {
        toggleModeTextView.setVisibility(View.INVISIBLE);
        modeSwitchProgressBar.setVisibility(View.VISIBLE);
    }

    @UiThread
    @Override
    public void modeSwitched() {
        modeSwitchProgressBar.setVisibility(View.GONE);
        toggleModeTextView.setVisibility(View.VISIBLE);
    }

    @UiThread
    @Override
    public void setProfileImage(Bitmap image) {
        navigationView.setProfileImage(image);
    }

    @UiThread
    @Override
    public void showBadge(int value) {
        if(value <= 0){
            badgeContainer.setVisibility(View.GONE);
        }
        else{
            badgeContainer.setVisibility(View.VISIBLE);
            badgeTextView.setText(""+value);
        }
    }

    @UiThread
    @Override
    public void setBadgeVisibility(int visibility) {
        badgeContainer.setVisibility(visibility);
    }

    @Override
    public int getBadgeVisibility() {
        return badgeContainer.getVisibility();
    }


    @UiThread
    @Override
    public void showImage(String url) {

        if(Strings.isNullOrEmpty(url)){
            circularImageView.setVisibility(View.GONE);
            blurredImageView.setVisibility(View.GONE);
        }else{
            circularImageView.setVisibility(View.VISIBLE);
            blurredImageView.setVisibility(View.VISIBLE);
            Picasso.with(this).load(url).into(target);
        }

    }

    @UiThread
    @Override
    public void showImage(Bitmap bitmap) {
        if(bitmap == null){
            circularImageView.setVisibility(View.GONE);
            blurredImageView.setVisibility(View.GONE);
            return;
        }
        Drawable originalDrawable = new BitmapDrawable(getResources(),bitmap);
        circularImageView.setImageDrawable(originalDrawable);
        Bitmap destination = NativeStackBlur.process(bitmap, 5);
        Drawable destinationDrawable = new BitmapDrawable(getResources(),destination);
        blurredImageView.setImageDrawable(destinationDrawable);
    }

    @Override
    public void setAction(FabContext context) {
        if(context == null)
        {
            if(editFab != null){
                editFab.setVisibility(View.GONE);
                editFab.setOnClickListener(null);
            }

            if(appBarLayout != null){
                appBarLayout.removeOnOffsetChangedListener(resizeViewOnOffsetChangedListener);
            }

            return;
        }
        if(appBarLayout != null) {
            appBarLayout.addOnOffsetChangedListener(resizeViewOnOffsetChangedListener);
        }


        if(editFab != null){
            editFab.setVisibility(View.VISIBLE);
            editFab.setImageDrawable(ContextCompat.getDrawable(this, context.drawableResourceId));
            editFab.setOnClickListener(context.listener);

          /*  CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) editFab.getLayoutParams();
            layoutParams.setAnchorId(R.id.circularImageView);
            layoutParams.anchorGravity =  Gravity.RIGHT | Gravity.CENTER_VERTICAL;
            editFab.setLayoutParams(layoutParams);*/
        }

    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            BitmapDrawable drawable = new BitmapDrawable(getResources(),bitmap);
            Bitmap source = bitmap;
            Bitmap destination = NativeStackBlur.process(source, 5);
            Drawable destinationDrawable = new BitmapDrawable(getResources(),destination);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                blurredImageView.setImageDrawable(destinationDrawable);
                circularImageView.setImageDrawable(drawable);
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {

    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                this.getCurrentFocus().getWindowToken(),
                0
        );

    }
}
