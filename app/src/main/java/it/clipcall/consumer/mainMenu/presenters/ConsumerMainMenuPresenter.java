package it.clipcall.consumer.mainMenu.presenters;

import com.google.common.base.Strings;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.consumer.mainMenu.controllers.ConsumerMainMenuController;
import it.clipcall.consumer.mainMenu.views.IConsumerMainMenuView;
import it.clipcall.consumer.payment.controllers.PaymentCoordinator;
import it.clipcall.consumer.payment.models.PaymentProfessional;
import it.clipcall.consumer.profile.models.CustomerProfile;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;

@PerActivity
public class ConsumerMainMenuPresenter extends PresenterBase<IConsumerMainMenuView>{


    private final ConsumerMainMenuController controller;
    private final RoutingService routingService;
    private final RouteParams routeParams;
    private final PaymentCoordinator paymentCoordinator;
    private final EventAggregator eventAggregator;

    @Inject
    public ConsumerMainMenuPresenter(ConsumerMainMenuController controller, RoutingService routingService, RouteParams routeParams, PaymentCoordinator paymentCoordinator, EventAggregator eventAggregator){
        this.controller = controller;
        this.routingService = routingService;
        this.routeParams = routeParams;
        this.paymentCoordinator = paymentCoordinator;
        this.eventAggregator = eventAggregator;
    }

    @Override
    public void initialize() {
        super.initialize();
        CustomerProfile profile = this.controller.getCustomerProfile();
        String customerPhoneNumber = this.controller.getCustomerPhoneNumber();
        String text = customerPhoneNumber;

        if(profile != null){
            text = Strings.isNullOrEmpty(profile.getName()) ? customerPhoneNumber : profile.getName();
            view.setProfileImage(profile.getImageUrl());
        }else{
            view.setProfileImage((String)null);
        }
        view.setCustomerName(text);
    }

    public void routeTo(Route route, NavigationContext navigationContext) {
        routingService.routeTo(route, navigationContext);
    }

    public void routeTo(String routeName, NavigationContext navigationContext) {
        routingService.routeTo(routeName, navigationContext);
    }

    public void switchToProfessionalMode() {
        view.switchingMode();
        controller.switchToProfessionalMode();
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        Route route = Routes.ProfessionalProjectsRoute;
        routingService.routeTo(route, navigationContext);
        view.modeSwitched();
        if(controller.isProfessionalRegistered()){
            view.finish();
        }
    }

    public void switchToConsumerMode() {
        controller.switchToConsumerMode();
    }

    public boolean isProfessionalRegistered() {
        return controller.isProfessionalRegistered();
    }

    public void payYourPro() {
        List<PaymentProfessional> consumerProfessionals = paymentCoordinator.getConsumerProfessionals();
        routeParams.reset();
        routeParams.setParam("professionals", consumerProfessionals);
        NavigationContext navigationContext  = new NavigationContext(view.getRoutingContext());
        routingService.routeTo(Routes.ConsumerPayYourProRoute, navigationContext);
        eventAggregator.publish(new ApplicationEvent(ConsumerEventNames.MenuBillingPay));
    }
}
