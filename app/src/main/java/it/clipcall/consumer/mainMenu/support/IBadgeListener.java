package it.clipcall.consumer.mainMenu.support;

/**
 * Created by micro on 1/29/2016.
 */
public interface IBadgeListener {

    void showBadge(int value);

    void setBadgeVisibility(int visibility);

    public int getBadgeVisibility();

}
