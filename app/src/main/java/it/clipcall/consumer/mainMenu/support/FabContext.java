package it.clipcall.consumer.mainMenu.support;

import android.view.View;

/**
 * Created by dorona on 05/05/2016.
 */
public class FabContext {

    public int drawableResourceId;
    public View.OnClickListener listener;
}
