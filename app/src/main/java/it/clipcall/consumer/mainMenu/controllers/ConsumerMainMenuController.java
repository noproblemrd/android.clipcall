package it.clipcall.consumer.mainMenu.controllers;


import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.consumer.profile.models.CustomerProfile;
import it.clipcall.consumer.profile.services.CustomerProfileManager;

@Singleton
public class ConsumerMainMenuController {


    private final AuthenticationManager authenticationManager;
    private final CustomerProfileManager profileManager;

    @Inject
    public ConsumerMainMenuController(AuthenticationManager authenticationManager, CustomerProfileManager profileManager){
        this.authenticationManager = authenticationManager;
        this.profileManager = profileManager;
    }

    public CustomerProfile getCustomerProfile(){
        if(!authenticationManager.isCustomerAuthenticated())
            return null;

        String customerId = authenticationManager.getCustomerId();
        CustomerProfile profile = profileManager.getCustomerProfile(customerId);
        profile.setPhoneNumber(authenticationManager.getPhoneNumber());
        return profile;
    }

    public String getCustomerPhoneNumber() {
        return authenticationManager.getPhoneNumber();
    }

    public void switchToProfessionalMode(){
        authenticationManager.switchToProfessionalMode();
    }

    public void switchToConsumerMode() {
        authenticationManager.switchToConsumerMode();
    }

    public boolean isProfessionalRegistered() {
        return authenticationManager.isProfessionalRegistered();
    }
}
