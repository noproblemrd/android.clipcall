package it.clipcall.consumer.vidoechat.presenters;


import android.app.Activity;
import android.os.Bundle;

import org.parceler.Parcels;
import org.simple.eventbus.EventBus;

import javax.inject.Inject;

import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.common.permissions.ClipCallPermissionsManager;
import it.clipcall.consumer.vidoechat.controllers.ConsumerVideoChatController;
import it.clipcall.consumer.vidoechat.models.AcceptedInvitation;
import it.clipcall.consumer.vidoechat.models.RemoteServiceCallData;
import it.clipcall.consumer.vidoechat.models.VideoChatContext;
import it.clipcall.consumer.vidoechat.models.VideoInvitationData;
import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.consumer.vidoechat.views.IConsumerStartVideoChatView;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.ui.permissions.IPermissionsHandler;
import it.clipcall.infrastructure.ui.permissions.PermissionsService;

/**
 * Created by dorona on 28/12/2015.
 */
@PerActivity
public class ConsumerVideoChatPresenter extends PresenterBase<IConsumerStartVideoChatView> {

    private final ILogger logger = LoggerFactory.getLogger(ConsumerVideoChatPresenter.class.getSimpleName());

    private final Activity activity;
    private final ConsumerVideoChatController controller;
    private final RoutingService routingService;
    private final PermissionsService permissionsService;
    private final ClipCallPermissionsManager permissionsManager;
    private RemoteServiceCallData remoteServiceCallData;

    @Inject
    public ConsumerVideoChatPresenter(Activity activity,ConsumerVideoChatController controller, RoutingService routingService, PermissionsService permissionsService, ClipCallPermissionsManager permissionsManager){
        this.activity = activity;
        this.controller = controller;
        this.routingService = routingService;
        this.permissionsService = permissionsService;
        this.permissionsManager = permissionsManager;
    }

    public void requestPermissions(IPermissionsHandler permissionsHandler){
        logger.debug("requestPermissions");
        permissionsManager.requestVideoChatPermissions(permissionsHandler);
    }

    public void declineInvitation(){
        logger.debug("declineInvitation");
        controller.deleteInvitationFromCache();
        routingService.routeToHome(view.getRoutingContext());
    }


    public void acceptInvitation() {
        logger.debug("acceptInvitation");
        if(view == null){
            view = (IConsumerStartVideoChatView)activity;
        }
        view.showAcceptInvitationInProgress();
        EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.VID_CHAT_CONS_TAP_START,controller.getInvitationData()));

        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();

        if(!controller.isConsumerAuthenticated()){
            EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.VID_CHAT_CONS_TAP_START_REDIRECT2_VALID,controller.getInvitationData()));
            view.onAcceptingInvitationFailure();
            routingService.routeTo(Routes.ConsumerStartVideoChat2Route,navigationContext);
            return;
        }
        try {
            String invitationId = this.controller.getInvitationId();
            AcceptedInvitation acceptedInvitation = controller.acceptInvitation(invitationId);
            if(acceptedInvitation == null){
                view.onAcceptingInvitationFailure();
                return;
            }

            logger.debug("onCallAccepted with accepting invitation from %s", acceptedInvitation.invitationData.getSupplierName());


            acceptInvitationCore(acceptedInvitation);

        } catch (Exception e) {
            e.printStackTrace();
            view.onAcceptingInvitationFailure();

        }

    }

    @RunOnUiThread
    private void acceptInvitationCore(AcceptedInvitation acceptedInvitation){

        VideoInvitationData invitationData = acceptedInvitation.invitationData;
        VideoSessionData videoSessionData = acceptedInvitation.videoSessionData;
        remoteServiceCallData = new RemoteServiceCallData();
        remoteServiceCallData.apiKey = videoSessionData.getApiKey()+"";
        remoteServiceCallData.sessionId = videoSessionData.getSessionId();
        remoteServiceCallData.token = videoSessionData.getToken();
        remoteServiceCallData.phone = invitationData.getSupplierName();
        remoteServiceCallData.profileImage = invitationData.getSupplierImage();
        remoteServiceCallData.message = invitationData.getText();

        connectToVideoSessionInBackground();
        initializePublisher();
        navigateToVideoChat(acceptedInvitation);
    }

    private void connectToVideoSessionInBackground(){
        logger.debug("connectToVideoSessionInBackground");
        VideoSessionData videoSessionData = new VideoSessionData(Integer.parseInt(remoteServiceCallData.apiKey),remoteServiceCallData.token,  remoteServiceCallData.sessionId);
        controller.connectToVideoChatInBackground(videoSessionData);
        logger.debug("connectToVideoSessionInBackground - end");
    }

    private void initializePublisher(){
        logger.debug("initializePublisher");
        controller.initializePublisher(remoteServiceCallData.phone);
        logger.debug("initializePublisher - end");
    }

    private void navigateToVideoChat(AcceptedInvitation acceptedInvitation){
        logger.debug("navigateToVideoChat");
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();
        navigationContext.bundle = new Bundle();
        VideoChatContext videoChatContext = new VideoChatContext();
        videoChatContext.videoSessionData = acceptedInvitation.videoSessionData;
        videoChatContext.isInitiatingCall = true;
        videoChatContext.profileName = acceptedInvitation.invitationData.getSupplierName();
        videoChatContext.profileImageUrl = acceptedInvitation.invitationData.getSupplierImage();
        navigationContext.bundle.putParcelable("videoChatContext", Parcels.wrap(videoChatContext));


        routingService.routeTo(Routes.ConsumerVideoChatCalling, navigationContext);
        if(view != null){
            view.onStartingVideoCall();
        }

        logger.debug("navigateToVideoChat end");
    }

    @Override
    public void initialize() {
        String invitationId = this.controller.getInvitationId();

        VideoInvitationData videoInvitationData = controller.getVideoInvitationData(invitationId);
        if(videoInvitationData != null){
            EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.INVITATION_SHOWED, videoInvitationData));
            view.setAdvertiserName(videoInvitationData.getSupplierName());
            view.setAdvertiserProfileImage(videoInvitationData.getSupplierImage());
        }else{
            EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.INVITATION_EXPIRED, videoInvitationData));
            view.showInvitationExpiredMessage();
        }
    }

    public void invalidInvitation() {
        controller.deleteInvitationFromCache();
        routingService.routeToHome(view.getRoutingContext());
    }

    public void setRemoteServiceCallData(RemoteServiceCallData remoteServiceCallData) {
        this.remoteServiceCallData = remoteServiceCallData;
    }

}
