package it.clipcall.consumer.vidoechat.activities;

import android.content.DialogInterface;
import android.os.Parcelable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.parceler.Parcels;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.vidoechat.models.RemoteServiceCallData;
import it.clipcall.consumer.vidoechat.presenters.ConsumerVideoChatPresenter;
import it.clipcall.consumer.vidoechat.views.IConsumerStartVideoChatView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.ui.images.CircleTransform;
import it.clipcall.infrastructure.ui.permissions.EmptyPermissionsHandler;
import me.drakeet.materialdialog.MaterialDialog;

@EActivity(R.layout.activity_start_video_chat)
public class ConsumerStartVideoChatActivity extends BaseActivity implements IConsumerStartVideoChatView {

    private final ILogger logger = LoggerFactory.getLogger(ConsumerStartVideoChatActivity.class.getSimpleName());


    @Extra
    Parcelable remoteServiceCallContext;

    @ViewById
    ImageView profileImageView;

    @ViewById
    Toolbar toolbar;

    @ViewById
    TextView showAdvertiserTextView;

    @ViewById
    ImageView fab;

    @ViewById
    ProgressBar loadingSpinnerProgressBar;

    @Inject
    ConsumerVideoChatPresenter presenter;

    @AfterViews
    public void onViewLoaded() {
        logger.debug("onViewLoaded");

        this.setSupportActionBar(toolbar);
        this.presenter.bindView(this);
        showAdvertiserTextView.setVisibility(View.INVISIBLE);
        RemoteServiceCallData remoteServiceCallData = Parcels.unwrap(remoteServiceCallContext);
        if(remoteServiceCallData == null){
            logger.error("removeServiceCallData cannot be null");
        }
        presenter.setRemoteServiceCallData(remoteServiceCallData);
        initializeAdvertiserInfo();
    }

    @Background
    void initializeAdvertiserInfo(){
        logger.debug("initializeAdvertiserInfo");
        presenter.initialize();
    }



    @Click(R.id.fab)
    void startVideoCall(){
        logger.debug("startVideoCall");
        presenter.requestPermissions(new EmptyPermissionsHandler(){
            @Override
            public void handleAllPermissionsGranted() {
                logger.debug("permissions granted. accepting invitation");
                acceptInvitation();
            }
        });
    }

    @Background
    void acceptInvitation(){
        logger.debug("acceptInvitation");
        presenter.acceptInvitation();
    }


    @UiThread
    @Override
    public void setAdvertiserProfileImage(String profileImageUrl) {
        if(Strings.isNullOrEmpty(profileImageUrl)){
            Picasso.with(this).load(R.drawable.icons_profile_missing).transform(new CircleTransform()).into(profileImageView);
        }else{
            Picasso.with(this).load(profileImageUrl).placeholder(R.drawable.icons_profile_missing).transform(new CircleTransform()).into(profileImageView);
        }
    }

    @UiThread
    @Override
    public void setAdvertiserName(String advertiserName) {
        showAdvertiserTextView.setVisibility(View.VISIBLE);
        String text = String.format("Show \"%s\" what you need done",advertiserName);
        showAdvertiserTextView.setText(text);
    }

    @UiThread
    @Override
    public void showInvitationExpiredMessage() {
        final MaterialDialog materialDialog = new MaterialDialog(this);
        materialDialog.setTitle("Invalid invitation")
                .setMessage("Oops... Seems like the invitation is not valid anymore")
                .setPositiveButton("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        materialDialog.dismiss();
                        presenter.invalidInvitation();
                        finish();
                    }
                }).setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                presenter.invalidInvitation();
                finish();
            }
        });

        materialDialog.show();
    }

    @UiThread
    @Override
    public void onAcceptingInvitationFailure() {
        loadingSpinnerProgressBar.setVisibility(View.INVISIBLE);
        fab.setVisibility(View.VISIBLE);
    }

    @UiThread
    @Override
    public void onStartingVideoCall() {
        logger.debug("onStartingVideoCall");
        finish();
        overridePendingTransition(R.anim.down_from_top,R.anim.up_from_bottom);
    }

    @UiThread
    @Override
    public void showAcceptInvitationInProgress() {
        fab.setVisibility(View.INVISIBLE);
        loadingSpinnerProgressBar.setVisibility(View.VISIBLE);

    }





    @Override
    protected void onResume() {
        super.onResume();
        this.presenter.bindView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.presenter.unbindView();
    }

    @UiThread
    @Override
    public void onBackPressed() {
        presenter.declineInvitation();
    }
}
