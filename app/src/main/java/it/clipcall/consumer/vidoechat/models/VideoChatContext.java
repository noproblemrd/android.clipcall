package it.clipcall.consumer.vidoechat.models;

import org.parceler.Parcel;

@Parcel
public class VideoChatContext {
    public VideoSessionData videoSessionData;
    public boolean isInitiatingCall;
    public String profileImageUrl;
    public String profileName;
    public String invitationId;

}
