package it.clipcall.consumer.vidoechat.views;

import it.clipcall.consumer.vidoechat.models.RemoteServiceCallData;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 17/01/2016.
 */
public interface IConsumerIncomingVideoCallView extends IView {


    void showRemoteServiceCall(RemoteServiceCallData remoteServiceCallData);

    void showAcceptingCallInProgress();

    void showFailedAcceptingCall();

    void showOrHideAcceptCall(boolean show);
}
