package it.clipcall.consumer.vidoechat.models;

/**
 * Created by omega on 3/21/2016.
 */
public class AcceptedInvitation {
    public VideoSessionData videoSessionData;
    public VideoInvitationData invitationData;
}
