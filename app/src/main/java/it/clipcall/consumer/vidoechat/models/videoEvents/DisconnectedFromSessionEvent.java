package it.clipcall.consumer.vidoechat.models.videoEvents;

import com.opentok.android.Session;

/**
 * Created by dorona on 16/05/2016.
 */
public class DisconnectedFromSessionEvent extends SessionEvent {

    public DisconnectedFromSessionEvent(Session session){
        super(session);
    }
}
