package it.clipcall.consumer.vidoechat.models.videoEvents;

import com.opentok.android.Session;
import com.opentok.android.Stream;

/**
 * Created by dorona on 16/05/2016.
 */
public class StreamReceivedEvent extends SessionEvent {
    private final Stream stream;

    public StreamReceivedEvent(Session session, Stream stream) {
        super(session);
        this.stream = stream;
    }

    public Stream getStream() {
        return stream;
    }
}
