package it.clipcall.consumer.vidoechat.models;


import org.parceler.Parcel;

@Parcel
public class VideoSessionData {
    int apiKey;
    String sessionId;
    String token;

    public VideoSessionData(){

    }

    public VideoSessionData(int apiKey, String token, String sessionId){

        this.apiKey = apiKey;
        this.token = token;
        this.sessionId = sessionId;
    }

    public int getApiKey() {
        return apiKey;
    }

    public void setApiKey(int apiKey) {
        this.apiKey = apiKey;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
