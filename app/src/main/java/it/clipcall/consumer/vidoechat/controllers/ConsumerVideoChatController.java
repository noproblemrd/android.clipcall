package it.clipcall.consumer.vidoechat.controllers;

import com.google.common.base.Strings;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.invitations.services.IInvitationProvider;
import it.clipcall.common.validation.models.InvalidInvitationException;
import it.clipcall.common.validation.models.NotAuthenticatedException;
import it.clipcall.common.validation.models.OperationException;
import it.clipcall.consumer.vidoechat.models.AcceptedInvitation;
import it.clipcall.consumer.vidoechat.models.VideoInvitationData;
import it.clipcall.consumer.vidoechat.models.VideoProjectData;
import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.consumer.vidoechat.models.videoEvents.VideoSignal;
import it.clipcall.consumer.vidoechat.services.VideoChatCoordinator;
import it.clipcall.consumer.vidoechat.services.VideoChatManager;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by dorona on 28/12/2015.
 */
@Singleton
public class ConsumerVideoChatController {


    private final VideoChatManager videoChatManager;
    private final AuthenticationManager authenticationManager;
    private final IInvitationProvider invitationProvider;
    private final VideoChatCoordinator videoChatCoordinator;

    @Inject
    public ConsumerVideoChatController(VideoChatManager videoChatManager, AuthenticationManager authenticationManager, IInvitationProvider invitationProvider, VideoChatCoordinator videoChatCoordinator){
        this.videoChatManager = videoChatManager;
        this.authenticationManager = authenticationManager;
        this.invitationProvider = invitationProvider;
        this.videoChatCoordinator = videoChatCoordinator;
    }


    public AcceptedInvitation acceptInvitation(String invitationId) throws NotAuthenticatedException,InvalidInvitationException,OperationException, IOException {
        if(!authenticationManager.isCustomerAuthenticated())
            throw new NotAuthenticatedException();


        VideoInvitationData invitationData = videoChatManager.getVideoInvitationData(invitationId);
        if(invitationData == null)
            throw new InvalidInvitationException();


        String customerId = authenticationManager.getCustomerId();
        VideoSessionData videoSessionData = videoChatManager.startVideoChatConversation(customerId, invitationId);
        if(videoSessionData == null)
            throw new OperationException();

        AcceptedInvitation acceptedInvitation = new AcceptedInvitation();
        acceptedInvitation.invitationData = invitationData;
        acceptedInvitation.videoSessionData = videoSessionData;

        return acceptedInvitation;
    }

    public void connectToVideoChatInBackground(VideoSessionData videoSessionData){
        videoChatCoordinator.connectToSession(videoSessionData);
    }

    public VideoInvitationData getVideoInvitationData(String invitationId){
        try {
            VideoInvitationData videoInvitationData = videoChatManager.getVideoInvitationData(invitationId);
            return videoInvitationData;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isConsumerAuthenticated() {
        return authenticationManager.isCustomerAuthenticated();
    }

    public void deleteInvitationFromCache() {
        invitationProvider.clearInvitation();
    }

    public boolean uploadImage(File file) {
        if(!authenticationManager.isCustomerAuthenticated())
            return false;

        String invitationId = invitationProvider.getInvitationId();
        String customerId = authenticationManager.getCustomerId();
        String url  = videoChatManager.uploadVideoPreviewImage(customerId, file);
        if(Strings.isNullOrEmpty(url))
            return false;

        VideoProjectData data = new VideoProjectData();
        data.setVideoPreviewImageUrl(url);

        boolean success =  videoChatManager.updateVideoProjectData(customerId, invitationId, data);
        return success;
    }

    public JSONObject getInvitationData() {
        return invitationProvider.getInvitationData();
    }

    public String getInvitationId() {
        return invitationProvider.getInvitationId();
    }

    public boolean answerVideoChatCall(String invitationId) {
        String customerId = authenticationManager.getCustomerId();
        if(Strings.isNullOrEmpty(customerId))
            return false;

        boolean accepted = videoChatManager.answerVideoChatCall(customerId, invitationId);
        return accepted;
    }

    public void initializePublisher(String publisherName) {
        videoChatCoordinator.initializePublisher(publisherName);
    }

    public void sendVideoChatEvent(String eventType) {
        videoChatCoordinator.sendVideoChatEvent(eventType);
    }

    public void denyCall() {
        videoChatCoordinator.sendVideoChatEvent(VideoSignal.Canceled.getType());
    }
}
