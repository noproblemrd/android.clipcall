package it.clipcall.consumer.vidoechat.models;

import org.parceler.Parcel;

@Parcel
public class RemoteServiceCallData {
    public String phone;
    public String profileImage;
    public String invitationId;
    public String apiKey;
    public String sessionId;
    public String token;
    public String message;
    public String title;
    public String professionalId;

}
