package it.clipcall.consumer.vidoechat.services;

import android.app.Application;
import android.content.Context;

import com.opentok.android.BaseVideoRenderer;
import com.opentok.android.OpentokError;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Session;
import com.opentok.android.Stream;

import org.simple.eventbus.EventBus;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.consumer.vidoechat.models.VideoSessionEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.PublisherCreatedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.PublisherDestroyedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.PublisherErrorEvent;
import it.clipcall.consumer.vidoechat.support.BasicCustomVideoRenderer;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;

@Singleton
public class VideoChatPublisher implements Publisher.PublisherListener {

    private static final String TAG = "VideoChatPublisher";

    private final Context context;
    private final EventAggregator eventAggregator;
    private Publisher publisher;
    private BasicCustomVideoRenderer renderer;


    private  final ILogger logger  = LoggerFactory.getLogger(VideoChatPublisher.class.getSimpleName());

    @Inject
    public VideoChatPublisher(Application context, EventAggregator eventAggregator){

        this.context = context;
        this.eventAggregator = eventAggregator;
    }

    public void enableAudio(boolean enabled){
        publisher.setPublishAudio(enabled);
    }


    public void initializePublisher(String publisherName) {
        logger.debug("setVideoChatContext: initializing publisher with name  %s", publisherName);
        if(publisher != null)
        {
            logger.debug("setVideoChatContext: Failed initializing publisher with name %s. There is an active publisher", publisherName);
            return;
        }


        publisher = new Publisher(context, publisherName, Publisher.CameraCaptureResolution.MEDIUM, Publisher.CameraCaptureFrameRate.FPS_30);
        publisher.swapCamera();
        publisher.setAudioFallbackEnabled(false);
        publisher.setPublisherListener(this);
        renderer = new BasicCustomVideoRenderer(context);
        publisher.setRenderer(renderer);
        publisher.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);

        logger.debug("setVideoChatContext: publisher initialized successfully");
    }

    public void publish(Session session) {
        if (publisher == null) {
            logger.debug("publish: cannot publish to session. Publisher is null");
            return;
        }
        logger.debug("publish: publishing publisher with name %s to session with id %s", publisher.getName(), session.getSessionId());
        session.publish(publisher);
    }



    @Override
    public void onStreamCreated(PublisherKit publisher, Stream stream) {
        logger.debug("onStreamCreated: published successfully with streamId - "+stream.getStreamId() );
        EventBus.getDefault().post(new ApplicationEvent("VidChat-Cons-Vid-Published", new VideoSessionEvent(publisher.getSession().getSessionId())));
        eventAggregator.publish(new PublisherCreatedEvent(publisher, stream));
    }

    @Override
    public void onStreamDestroyed(PublisherKit publisher, Stream stream) {
        logger.debug("onStreamDestroyed: streamId -" + stream.getStreamId());
        //EventBus.getDefault().post(new ApplicationEvent("VidChat-Cons-Vid-Published", new VideoSessionEvent(publisher.getSession().getSessionId())));
        eventAggregator.publish(new PublisherDestroyedEvent(publisher, stream));
    }

    @Override
    public void onError(PublisherKit publisher, OpentokError exception) {
        logger.error("onError: exception - " + exception.getMessage());
        eventAggregator.publish(new PublisherErrorEvent(publisher,exception));
        //EventBus.getDefault().post(new ApplicationEvent("VidChat-Cons-Vid-Published", new VideoSessionEvent(publisher.getSession().getSessionId())));
    }

    public void swapCamera() {
        logger.debug("swapCamera");
        if(publisher != null)
            publisher.swapCamera();
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void releasePublisher() {
        logger.debug("releasePublisher");

        if(publisher != null){

            publisher.setPublisherListener(null);
            publisher = null;
            logger.debug("publisher released successfully");

        }

        if(renderer != null){
            renderer = null;
            logger.debug("renderer released successfully");
        }

        logger.debug("releasePublisher - done");
    }

    public File getVideoScreenshot() {

        if(renderer == null)
            return null;

        File file = renderer.getScreenshot();
       return file;
    }
}
