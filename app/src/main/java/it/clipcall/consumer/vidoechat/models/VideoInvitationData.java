package it.clipcall.consumer.vidoechat.models;

/**
 * Created by dorona on 28/12/2015.
 */
public class VideoInvitationData {
    private String supplierName ;
    private String text;
    private String supplierImage;

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSupplierImage() {
        return supplierImage;
    }

    public void setSupplierImage(String supplierImage) {
        this.supplierImage = supplierImage;
    }
}
