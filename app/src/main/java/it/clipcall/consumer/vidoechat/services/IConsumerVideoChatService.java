package it.clipcall.consumer.vidoechat.services;


import it.clipcall.consumer.profile.models.Logo;
import it.clipcall.consumer.vidoechat.models.VideoProjectData;
import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.infrastructure.responses.StartVideoChatConversationResponse;
import it.clipcall.infrastructure.responses.ValidateInvitationResponse;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by dorona on 14/12/2015.
 */
public interface IConsumerVideoChatService {
    @POST("videochat/{invitationId}")
    Call<ValidateInvitationResponse> validateInvitation(@Path("invitationId") String  invitationId);

    @POST("customers/{customerId}/videochat/{invitationId}/call")
    Call<StartVideoChatConversationResponse> startVideoChatConversation(@Path("customerId") String  customerId,@Path("invitationId") String  invitationId);


    @Multipart
    @POST("customers/{customerId}/images")
    Call<Logo> uploadVideoPreviewImage(@Path("customerId")String customerId, @Part("myfile\"; filename=\"audioData.png\" ") RequestBody file);


    @POST("customers/{customerId}/videochat/{invitationId}/project")
    Call<StatusResponse> updateProjectVideoPreviewImage(@Path("customerId")String customerId, @Path("invitationId") String  invitationId, @Body VideoProjectData projectData);


    @POST("customers/{customerId}/videochat/createvideochat/{leadAccountId}")
    Call<VideoSessionData> createVideoSession(@Path("customerId") String customerId, @Path("leadAccountId") String leadAccountId);


    @POST("customers/{customerId}/videochat/{invitationId}/answer")
    Call<StatusResponse> answerVideoChatCall(@Path("customerId") String customerId, @Path("invitationId") String invitationId);
}
