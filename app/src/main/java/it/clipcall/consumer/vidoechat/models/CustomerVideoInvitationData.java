package it.clipcall.consumer.vidoechat.models;

import it.clipcall.common.invitations.models.eInvitationType;

/**
 * Created by dorona on 28/12/2015.
 */
public class CustomerVideoInvitationData {
    private String invitationId ;
    private String message;

    public eInvitationType getInvitationType() {
        return invitationType;
    }

    public void setInvitationType(eInvitationType invitationType) {
        this.invitationType = invitationType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(String invitationId) {
        this.invitationId = invitationId;
    }

    private eInvitationType invitationType;


}
