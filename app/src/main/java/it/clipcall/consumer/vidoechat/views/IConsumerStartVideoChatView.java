package it.clipcall.consumer.vidoechat.views;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 28/12/2015.
 */
public interface IConsumerStartVideoChatView extends IView {

    void setAdvertiserProfileImage(String profileImageUrl);

    void setAdvertiserName(String advertiserName);

    void showInvitationExpiredMessage();

    void onAcceptingInvitationFailure();

    void onStartingVideoCall();

    void showAcceptInvitationInProgress();
}
