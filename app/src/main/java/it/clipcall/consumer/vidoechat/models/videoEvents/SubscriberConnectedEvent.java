package it.clipcall.consumer.vidoechat.models.videoEvents;

import com.opentok.android.SubscriberKit;

/**
 * Created by dorona on 16/05/2016.
 */
public class SubscriberConnectedEvent extends SubscriberEvent {

    public SubscriberConnectedEvent(SubscriberKit subscriberKit){
        super(subscriberKit);
    }

}
