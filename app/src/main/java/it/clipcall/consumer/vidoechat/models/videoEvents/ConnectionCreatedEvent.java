package it.clipcall.consumer.vidoechat.models.videoEvents;

import com.opentok.android.Connection;
import com.opentok.android.Session;

/**
 * Created by dorona on 16/05/2016.
 */
public class ConnectionCreatedEvent {

    private final Session session;
    private final Connection connection;

    public ConnectionCreatedEvent(Session session, Connection connection){

        this.session = session;
        this.connection = connection;
    }

    public Session getSession() {
        return session;
    }

    public Connection getConnection() {
        return connection;
    }
}
