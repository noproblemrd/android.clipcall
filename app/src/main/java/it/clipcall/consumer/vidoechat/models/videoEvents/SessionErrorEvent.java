package it.clipcall.consumer.vidoechat.models.videoEvents;

import com.opentok.android.OpentokError;
import com.opentok.android.Session;

/**
 * Created by dorona on 16/05/2016.
 */
public class SessionErrorEvent extends SessionEvent {
    private final OpentokError error;

    public SessionErrorEvent(Session session, OpentokError error) {
        super(session);
        this.error = error;
    }

    public OpentokError getError() {
        return error;
    }
}
