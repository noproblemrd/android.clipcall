package it.clipcall.consumer.vidoechat.presenters;

import android.app.Activity;

import com.opentok.android.Publisher;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.io.File;

import javax.inject.Inject;

import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.consumer.vidoechat.controllers.ConsumerVideoChatController;
import it.clipcall.consumer.vidoechat.models.VideoChatContext;
import it.clipcall.consumer.vidoechat.models.VideoSessionEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.PublisherCreatedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.PublisherDestroyedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.VideoChatAnsweredEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.VideoChatCanceledEvent;
import it.clipcall.consumer.vidoechat.services.VideoChatCoordinator;
import it.clipcall.consumer.vidoechat.views.IPublisherView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.presenters.PresenterBase;

@PerActivity
public class VideoPublisherPresenter extends PresenterBase<IPublisherView> {

    private final ILogger logger = LoggerFactory.getLogger(VideoPublisherPresenter.class.getSimpleName());

    private final Activity activity;
    private final ConsumerVideoChatController controller;

    private final VideoChatCoordinator videoChatCoordinator;
    private final EventAggregator eventAggregator;

    private VideoChatContext chatContext;

    @Inject
    public VideoPublisherPresenter(Activity activity, ConsumerVideoChatController controller, VideoChatCoordinator videoChatCoordinator, EventAggregator eventAggregator) {
        this.activity = activity;
        this.controller = controller;
        this.videoChatCoordinator = videoChatCoordinator;
        this.eventAggregator = eventAggregator;
        this.eventAggregator.subscribe(this);
    }

    public void swapCamera(){
        videoChatCoordinator.swapCamera();
    }


    @org.simple.eventbus.Subscriber
    void onVideoChatCanceled(VideoChatCanceledEvent event){
        if(event.isGeneratedByMe())
            return;

        shutDown();
    }

    @org.simple.eventbus.Subscriber(mode = ThreadMode.ASYNC)
    void onVideoChatAnswered(VideoChatAnsweredEvent event) {
       videoChatCoordinator.enablePublisherAudio();
    }

    public void initPublisher(VideoChatContext chatContext) {

        logger.debug("initPublisher with name %s and isInitiatingCall %s",chatContext.profileName, chatContext.isInitiatingCall+"");

        this.chatContext = chatContext;
        /*if(publisher != null)
            return;



        Log.i(LOGTAG, "Publisher - setVideoChatContext");


        publisher = new Publisher(context, chatContext.profileName, Publisher.CameraCaptureResolution.MEDIUM, Publisher.CameraCaptureFrameRate.FPS_30);
        publisher.swapCamera();
        publisher.setAudioFallbackEnabled(false);
        publisher.setPublisherListener(this);
        renderer = new BasicCustomVideoRenderer(context);
        publisher.setRenderer(renderer);
        publisher.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);*/


        Publisher publisher = videoChatCoordinator.getPublisher();
        if(publisher == null)
        {
            logger.debug("initPublisher failed. publisher is null");
            return;
        }
        view.attachPublisherView(publisher,  this.chatContext.isInitiatingCall);
        view.showPublisherSpinner();
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onPublisherCreated(PublisherCreatedEvent event) {
        logger.debug("Received event - PublisherCreatedEvent with stream is %s", event.getStream().getStreamId());
        view.hidePublisherSpinner();
        EventBus.getDefault().post(new ApplicationEvent("VidChat-Cons-Vid-Published", new VideoSessionEvent(event.getPublisherKit().getSession().getSessionId())));
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onPublisherDestroyed(PublisherDestroyedEvent event) {
        logger.debug("Received event - PublisherDestroyedEvent with stream is %s", event.getStream().getStreamId());
        eventAggregator.unSubscribe(this);;
    }


    @Override
    public void shutDown() {
        logger.debug("shutdown");
        super.shutDown();
        controller.deleteInvitationFromCache();
        if(view == null){
            view = (IPublisherView) activity;
        }

        view.removePublisherView();
        videoChatCoordinator.releasePublisher();

    }

    public void takeScreenshot() {
        logger.debug("takeScreenshot");

        File file  = videoChatCoordinator.getVideoScreenshot();

        if(file == null)
            return;

        controller.uploadImage(file);

    }
}
