package it.clipcall.consumer.vidoechat.views;

import com.opentok.android.Session;
import com.opentok.android.Stream;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 31/12/2015.
 */
public interface ISessionView extends IView{

    void showTotalTimeRecorded(long totalTimeInSeconds);

    void onSubscriberConnected(Session session, Stream stream);

    void onSubscriberDisconnected(Session session, Stream stream);

    void onVideoPreviewRequested();

    void showVideoActions(boolean shouldShow);

    void onDisconnectedFromSession();
}
