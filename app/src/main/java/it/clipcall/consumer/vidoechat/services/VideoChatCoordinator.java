package it.clipcall.consumer.vidoechat.services;


import com.opentok.android.Publisher;
import com.opentok.android.Session;

import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.consumer.vidoechat.models.videoEvents.ConnectedToSessionEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.StreamReceivedEvent;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;

@Singleton
public class VideoChatCoordinator {

    private static final String TAG = "VideoChatCoordinator";

    //private static final Logger logger = LoggerFactory.getLogger(VideoChatCoordinator.class.getSimpleName());
    private final ILogger logger = LoggerFactory.getLogger(VideoChatCoordinator.class.getSimpleName());
    private final EventAggregator eventAggregator;
    private final VideoChatPublisher videoChatPublisher;
    private final VideoChatSessionProvider videoChatSessionProvider;
    private final VideoChatSubscriber videoChatSubscriber;


    @Inject
    public VideoChatCoordinator(EventAggregator eventAggregator, VideoChatPublisher videoChatPublisher, VideoChatSessionProvider videoChatSessionProvider, VideoChatSubscriber videoChatSubscriber){
        this.eventAggregator = eventAggregator;
        this.videoChatPublisher = videoChatPublisher;
        this.videoChatSessionProvider = videoChatSessionProvider;
        this.videoChatSubscriber = videoChatSubscriber;
        this.eventAggregator.subscribe(this);
    }

    public void connectToSession(VideoSessionData videoSessionData){
        logger.debug("Connecting to session with api key: %s, session id: %s and token: %s", videoSessionData.getApiKey(), videoSessionData.getSessionId(), videoSessionData.getToken());
        videoChatSessionProvider.connectToSession(videoSessionData);
    }

    public void initializePublisher(String publisherName){
        logger.debug("initializing publisher with publisher name %s", publisherName);
        videoChatPublisher.initializePublisher(publisherName);
        videoChatPublisher.enableAudio(false);
    }


    @Subscriber(mode = ThreadMode.ASYNC)
    void onConnectedToSession(ConnectedToSessionEvent event){
        logger.debug("Received event: %s with session id: %s", "ConnectedToSession", event.getSession().getSessionId());
        videoChatPublisher.publish(event.getSession());
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onSubscriberCreated(StreamReceivedEvent event){
        logger.debug("Received event: %s with session id: %s", "SubscriberCreated", event.getSession().getSessionId());
        videoChatSubscriber.createSubscriber(event.getSession(),event.getStream());
    }

    public Session getSession() {
        return videoChatSessionProvider.getSession();

    }

    public void disconnectFromSession() {
       videoChatSessionProvider.disconnectFromSession();
    }

    public void firePendingEvents() {

    }

    public void swapCamera() {
        videoChatPublisher.swapCamera();
    }

    public Publisher getPublisher() {
        return videoChatPublisher.getPublisher();
    }

    public void releasePublisher() {
        videoChatPublisher.releasePublisher();
    }

    public File getVideoScreenshot() {
        return videoChatPublisher.getVideoScreenshot();
    }

    public boolean isSubscribedToVideo() {
        return videoChatSubscriber.isSubscribedToVideo();
    }

    public void releaseSubscriber() {
        videoChatSubscriber.releaseSubscriber();
    }

    public void sendVideoChatEvent(String eventType) {
        videoChatSessionProvider.publishEvent(eventType);
    }

    public com.opentok.android.Subscriber getSubscriber() {
        return videoChatSubscriber.getSubscriber();
    }

    public void enablePublisherAudio() {
        videoChatPublisher.enableAudio(true);
    }
}
