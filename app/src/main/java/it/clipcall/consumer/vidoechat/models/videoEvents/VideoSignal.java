package it.clipcall.consumer.vidoechat.models.videoEvents;

/**
 * Created by dorona on 17/05/2016.
 */
public class VideoSignal {



    public final static VideoSignal Answer = new VideoSignal("1000");
    public final static VideoSignal Canceled = new VideoSignal("2000");
    public final static VideoSignal CallEnded = new VideoSignal("3000");
    public final static VideoSignal Ready = new VideoSignal("4000");


    private final String type;

    private VideoSignal(String type){
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
