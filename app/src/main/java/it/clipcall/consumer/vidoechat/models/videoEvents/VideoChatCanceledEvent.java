package it.clipcall.consumer.vidoechat.models.videoEvents;

import com.opentok.android.Session;

public class VideoChatCanceledEvent extends SessionEvent {

    private final boolean isGeneratedByMe;

    public VideoChatCanceledEvent(Session session, boolean isGeneratedByMe) {
        super(session);
        this.isGeneratedByMe = isGeneratedByMe;
    }


    public boolean isGeneratedByMe() {
        return isGeneratedByMe;
    }
}
