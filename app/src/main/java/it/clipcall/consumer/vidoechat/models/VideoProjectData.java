package it.clipcall.consumer.vidoechat.models;

/**
 * Created by dorona on 11/02/2016.
 */
public class VideoProjectData {

    public String getVideoPreviewImageUrl() {
        return videoPreviewImageUrl;
    }

    public void setVideoPreviewImageUrl(String videoPreviewImageUrl) {
        this.videoPreviewImageUrl = videoPreviewImageUrl;
    }

    private String videoPreviewImageUrl;
}
