package it.clipcall.consumer.vidoechat.presenters;

import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import it.clipcall.common.permissions.ClipCallPermissionsManager;
import it.clipcall.consumer.vidoechat.models.VideoChatContext;
import it.clipcall.consumer.vidoechat.models.videoEvents.ConnectedToSessionEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.DisconnectedFromSessionEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.PublisherCreatedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.StreamReceivedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.SubscriberDestroyedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.VideoChatAnsweredEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.VideoChatCanceledEvent;
import it.clipcall.consumer.vidoechat.services.VideoChatCoordinator;
import it.clipcall.consumer.vidoechat.views.ISessionView;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.ui.permissions.IPermissionsHandler;

@PerActivity
public class VideoSessionPresenter extends PresenterBase<ISessionView> {

    int totalConnections = 0;

    private final ILogger logger = LoggerFactory.getLogger(VideoSessionPresenter.class.getSimpleName());

    private long totalTimeRecorded = 0;
    private Timer timer;
    private final RoutingService routingService;
    private final ClipCallPermissionsManager clipCallPermissionsManager;
    private final VideoChatCoordinator videoChatCoordinator;
    private final EventAggregator eventAggregator;
    private VideoChatContext chatContext;

    private boolean requestedPreviewImage = false;
    private boolean shouldShowCameraActions = true;
    private int totalTimeWithAction = 0;

    @Inject
    public VideoSessionPresenter(RoutingService routingService, ClipCallPermissionsManager clipCallPermissionsManager, VideoChatCoordinator videoChatCoordinator, EventAggregator eventAggregator) {
        this.routingService = routingService;
        this.clipCallPermissionsManager = clipCallPermissionsManager;
        this.videoChatCoordinator = videoChatCoordinator;
        this.eventAggregator = eventAggregator;
        this.eventAggregator.subscribe(this);
    }

    public void initSession(VideoChatContext context) {
        logger.debug("initSession");
        chatContext = context;

    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onConnectedToSession(ConnectedToSessionEvent event) {
        //view.onSessionConnected(event.getSession());
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onDisconnectedFromSession(DisconnectedFromSessionEvent event) {
        view.onDisconnectedFromSession();
        logger.debug("received event - DisconnectedFromSessionEvent from session with id: " + event.getSession().getSessionId());
        releaseTimer();

        requestedPreviewImage = false;
        eventAggregator.unSubscribe(this);;
        view.onDisconnectedFromSession();
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onSubscriberCreated(StreamReceivedEvent event){
        if(chatContext.isInitiatingCall)
        totalConnections++;
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onPublisherCreated(PublisherCreatedEvent event){
        totalConnections++;
    }


    @Subscriber(mode = ThreadMode.ASYNC)
    void onVideoChatAnswered(VideoChatAnsweredEvent event) {
        logger.debug("received event - VideoChatAnsweredEvent from session with id %s. isGeneratedByMe is %s",event.getSession().getSessionId(), event.isGeneratedByMe()+"");

        com.opentok.android.Subscriber subscriber = videoChatCoordinator.getSubscriber();
        if(subscriber == null){
            logger.debug("Cannot handle event VideoChatAnsweredEvent - subscriber is null");
            return;
        }


        view.onSubscriberConnected(event.getSession(), subscriber.getStream());
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {


                totalTimeRecorded++;
                logger.debug("activating timer");
                view.showTotalTimeRecorded(totalTimeRecorded);

                if (!requestedPreviewImage && totalTimeRecorded > 10) {
                    requestedPreviewImage = true;
                    view.onVideoPreviewRequested();
                }

                if (shouldShowCameraActions && totalTimeWithAction == 10) {
                    view.showVideoActions(false);
                }

                totalTimeWithAction++;

            }
        }, 0, 1000);
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onSubscriberDestroyed(SubscriberDestroyedEvent event) {
        logger.debug("received event - SubscriberDestroyedEvent from session with id %s  and stream is %s",event.getSession().getSessionId(), event.getStream().getStreamId());
        view.onSubscriberDisconnected(event.getSession(), event.getStream());

    }


   /* @Override
    public void onConnectionCreated(Session session, Connection connection) {

        Log.d(LOGTAG, "Session - Another client connected to this session. connection id:  : " + connection.getConnectionId());
    }

    @Override
    public void onConnectionDestroyed(Session session, Connection connection) {
        Log.d(LOGTAG, "Session - Another client disconnected from this session. connection id:  : " + connection.getConnectionId());
    }*/

    @Override
    public void shutDown() {
        logger.debug("shutDown");
        releaseTimer();
        disconnectFromSession();

    }

    @org.simple.eventbus.Subscriber
    void onVideoChatCanceled(VideoChatCanceledEvent event){
        if(event.isGeneratedByMe())
            return;

        shutDown();
    }

    public void releaseTimer() {
        logger.debug("releaseTimer");
        if (timer == null)
            return;

        timer.cancel();
        timer = null;
    }

    private void disconnectFromSession() {
        logger.debug("disconnectFromSession");;
        videoChatCoordinator.disconnectFromSession();
        releaseTimer();
        requestedPreviewImage = false;
    }


    public void toggleVideoActions() {
        totalTimeWithAction = 0;
        shouldShowCameraActions = !shouldShowCameraActions;
        view.showVideoActions(shouldShowCameraActions);
    }

    @RunOnUiThread
    public void requestVideoPermissions(IPermissionsHandler permissionsHandler) {
        clipCallPermissionsManager.requestVideoChatPermissions(permissionsHandler);
    }

    public void routeToHome() {
        logger.debug("routeToHome");;
        routingService.routeToHome(view.getRoutingContext());
    }
}
