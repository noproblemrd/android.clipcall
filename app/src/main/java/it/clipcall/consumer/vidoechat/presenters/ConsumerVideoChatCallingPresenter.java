package it.clipcall.consumer.vidoechat.presenters;

import it.clipcall.common.invitations.services.BranchIoManager;
import it.clipcall.consumer.vidoechat.controllers.ConsumerVideoChatController;
import it.clipcall.consumer.vidoechat.views.IConsumerStartVideoChatView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.services.RoutingService;


@PerActivity
public class ConsumerVideoChatCallingPresenter extends PresenterBase<IConsumerStartVideoChatView> {


    private final ConsumerVideoChatController controller;
    private final BranchIoManager branchIoManager;
    private final RoutingService routingService;

    public ConsumerVideoChatCallingPresenter(ConsumerVideoChatController controller, BranchIoManager branchIoManager, RoutingService routingService){
        this.controller = controller;
        this.branchIoManager = branchIoManager;
        this.routingService = routingService;
    }
}
