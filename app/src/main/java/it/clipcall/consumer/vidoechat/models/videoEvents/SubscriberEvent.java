package it.clipcall.consumer.vidoechat.models.videoEvents;

import com.opentok.android.SubscriberKit;

/**
 * Created by dorona on 16/05/2016.
 */
public abstract class SubscriberEvent {
    private final SubscriberKit subscriberKit;

    protected SubscriberEvent(SubscriberKit subscriberKit){

        this.subscriberKit = subscriberKit;
    }

    public SubscriberKit getSubscriberKit() {
        return subscriberKit;
    }
}
