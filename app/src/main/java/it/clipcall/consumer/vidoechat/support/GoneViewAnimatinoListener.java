package it.clipcall.consumer.vidoechat.support;

import android.view.View;
import android.view.animation.Animation;


/**
 * Created by dorona on 11/02/2016.
 */
public class GoneViewAnimatinoListener implements Animation.AnimationListener {
    private final View view;

    public GoneViewAnimatinoListener(View view) {

        this.view = view;
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        view.setVisibility(View.GONE);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
