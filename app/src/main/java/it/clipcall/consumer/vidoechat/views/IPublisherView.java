package it.clipcall.consumer.vidoechat.views;

import com.opentok.android.Publisher;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 31/12/2015.
 */
public interface IPublisherView extends IView{

    void attachPublisherView(Publisher publisher, boolean isInitiatingCall);

    void showPublisherSpinner();

    void hidePublisherSpinner();

    void removePublisherView();
}
