package it.clipcall.consumer.vidoechat.views;

import com.opentok.android.SubscriberKit;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 31/12/2015.
 */
public interface ISubscriberView extends IView {
   void showView(SubscriberKit subscriber, boolean isInitiatingCall);

   void addView(SubscriberKit subscriberKit);

   void onSubscriberDisconnected(SubscriberKit subscriberKit);

   void hideSubscriberSpinner();

   void showSubscriberSpinner();

   void removeSubscriberView();
}
