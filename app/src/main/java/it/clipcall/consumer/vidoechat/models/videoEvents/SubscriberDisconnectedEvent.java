package it.clipcall.consumer.vidoechat.models.videoEvents;

import com.opentok.android.SubscriberKit;

/**
 * Created by dorona on 16/05/2016.
 */
public class SubscriberDisconnectedEvent extends SubscriberEvent {
    public SubscriberDisconnectedEvent(SubscriberKit subscriberKit) {
        super(subscriberKit);
    }
}
