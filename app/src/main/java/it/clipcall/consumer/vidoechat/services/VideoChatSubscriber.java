package it.clipcall.consumer.vidoechat.services;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.opentok.android.OpentokError;
import com.opentok.android.Session;
import com.opentok.android.Stream;
import com.opentok.android.Subscriber;
import com.opentok.android.SubscriberKit;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.consumer.vidoechat.models.VideoSessionEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.SubscriberConnectedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.SubscriberDisconnectedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.SubscriberErrorEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.SubscriberVideoReadyEvent;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;

@Singleton
public class VideoChatSubscriber implements   SubscriberKit.SubscriberListener, Subscriber.VideoListener{

    private static final String TAG = "VideoChatSubscriber";
    private final Context context;
    private final EventAggregator eventAggregator;
    private Subscriber subscriber;

    private  final ILogger logger  = LoggerFactory.getLogger(VideoChatSubscriber.class.getSimpleName());

    @Inject
    public VideoChatSubscriber(Application context, EventAggregator eventAggregator){
        this.context = context;
        this.eventAggregator = eventAggregator;
    }

    @Override
    public void onDisconnected(SubscriberKit subscriberKit) {

        logger.debug("onDisconnected - subscriber disconnected with connection id %s", subscriberKit.getStream().getConnection().getConnectionId());

        eventAggregator.publish(new ApplicationEvent("VidChat-Cons-Adv-Disconnected", new VideoSessionEvent(subscriberKit.getSession().getSessionId())));
        eventAggregator.publish(new SubscriberDisconnectedEvent(subscriberKit));
    }

    @Override
    public void onError(SubscriberKit subscriberKit, OpentokError opentokError) {
        logger.error("onError - subscriber received an error "+ opentokError.getMessage());
        eventAggregator.publish(new SubscriberErrorEvent(subscriberKit));
    }

    @Override
    public void onConnected(SubscriberKit subscriberKit) {
        logger.debug("onConnected - subscriber connected with connection id %s", subscriberKit.getStream().getConnection().getConnectionId());
        eventAggregator.publish(new ApplicationEvent("VidChat-Cons-Adv-Connected", new VideoSessionEvent(subscriberKit.getSession().getSessionId())));
        eventAggregator.publish(new SubscriberConnectedEvent(subscriberKit));

    }

    @Override
    public void onVideoDisabled(SubscriberKit subscriber, String reason) {
        Log.i(TAG, "Subscriber - Video disabled:" + reason);
    }

    @Override
    public void onVideoEnabled(SubscriberKit subscriber, String reason) {
        Log.i(TAG,"Subscriber - Video enabled:" + reason);
    }

    @Override
    public void onVideoDisableWarning(SubscriberKit subscriber) {
        Log.i(TAG, "Subscriber - Video may be disabled soon due to network quality degradation. Add UI handling here.");
    }

    @Override
    public void onVideoDisableWarningLifted(SubscriberKit subscriber) {
        Log.i(TAG, "Subscriber -Video may no longer be disabled as stream quality improved. Add UI handling here.");
    }

    @Override
    public void onVideoDataReceived(SubscriberKit subscriber) {
        Log.i(TAG, "Subscriber - onVideoDataReceived");
        eventAggregator.publish(new SubscriberVideoReadyEvent(subscriber));
    }




    public void releaseSubscriber(){
        logger.debug("releaseSubscriber");
        if(subscriber == null)
        {
            logger.debug("releaseSubscriber failed. subscriber already released");
            return;
        }

        subscriber.setSubscriberListener(null);
        subscriber.setVideoStatsListener(null);
        subscriber = null;

        logger.debug("released subscriber successfully");
    }

    public void createSubscriber(Session session, Stream stream) {
        logger.debug("createSubscriber");
        if (subscriber != null)
        {
            logger.debug("createSubscriber failed. already active subscriber");
            return;
        }

        subscriber = new Subscriber(context, stream);
        subscriber.setSubscriberListener(this);
        subscriber.setVideoListener(this);
        session.subscribe(subscriber);
        logger.debug("createSubscriber - end");
    }

    public boolean isSubscribedToVideo() {
        return subscriber.getSubscribeToVideo();
    }

    public Subscriber getSubscriber() {
        logger.debug("getSubscriber - end");
        return subscriber;
    }
}
