package it.clipcall.consumer.vidoechat.presenters;


import android.os.Bundle;

import org.parceler.Parcels;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.vidoechat.controllers.ConsumerVideoChatController;
import it.clipcall.consumer.vidoechat.models.RemoteServiceCallData;
import it.clipcall.consumer.vidoechat.models.VideoChatContext;
import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.consumer.vidoechat.models.videoEvents.ConnectedToSessionEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.PublisherCreatedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.SubscriberConnectedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.VideoChatReadyEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.VideoSignal;
import it.clipcall.consumer.vidoechat.views.IConsumerIncomingVideoCallView;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.audio.SimpleMediaPlayer;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;


@PerActivity
public class ConsumerIncomingVideoCallPresenter extends PresenterBase<IConsumerIncomingVideoCallView> {

    private final SimpleMediaPlayer simpleMediaPlayer;
    private final RoutingService routingService;
    private final ConsumerVideoChatController controller;
    private final EventAggregator eventAggregator;

    RemoteServiceCallData remoteServiceCallData;

    @Inject
    public ConsumerIncomingVideoCallPresenter(SimpleMediaPlayer simpleMediaPlayer, RoutingService routingService, ConsumerVideoChatController controller, EventAggregator eventAggregator){
        this.simpleMediaPlayer = simpleMediaPlayer;
        this.routingService = routingService;
        this.controller = controller;
        this.eventAggregator = eventAggregator;
    }

    @RunOnUiThread
    @Override
    public void initialize() {
        view.showOrHideAcceptCall(false);
        simpleMediaPlayer.setAudioTrack(R.raw.video_chat_incoming_call_ring);
        simpleMediaPlayer.setIsLooping(true);
        simpleMediaPlayer.play();
        view.showRemoteServiceCall(remoteServiceCallData);
        eventAggregator.subscribe(this);

    }

    @RunOnUiThread
    public void connectToVideoSessionInBackground(){
        VideoSessionData videoSessionData = new VideoSessionData(Integer.parseInt(remoteServiceCallData.apiKey),remoteServiceCallData.token,  remoteServiceCallData.sessionId);
        controller.connectToVideoChatInBackground(videoSessionData);
    }

    @RunOnUiThread
    public void initializePublisher(){
        controller.initializePublisher(remoteServiceCallData.phone);
    }

    int totalConnections = 0;

    @Subscriber(mode = ThreadMode.ASYNC)
    void onConnectedToSession(ConnectedToSessionEvent event){

    }


    @Subscriber(mode = ThreadMode.ASYNC)
    void onSubscriberCreated(SubscriberConnectedEvent event){
        trySendingReadyEvent();
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onPublisherCreated(PublisherCreatedEvent event){
        trySendingReadyEvent();
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onVideoChatReady(VideoChatReadyEvent event){
        if(event.isGeneratedByMe()){
            view.showOrHideAcceptCall(true);
        }
    }

    private void trySendingReadyEvent() {
        totalConnections++;
        if(totalConnections == 2){
            controller.sendVideoChatEvent(VideoSignal.Ready.getType());
        }
    }

    @Override
    public void shutDown() {
        simpleMediaPlayer.stop();
        eventAggregator.unSubscribe(this);
    }

    public void setRemoteServiceCallData(RemoteServiceCallData remoteServiceCallData){
        this.remoteServiceCallData = remoteServiceCallData;
    }

    public void acceptCall() {

        view.showAcceptingCallInProgress();
        simpleMediaPlayer.stop();
        boolean callAnswered = controller.answerVideoChatCall(remoteServiceCallData.invitationId);
        if(!callAnswered)
        {
            view.showFailedAcceptingCall();
            return;
        }
        controller.sendVideoChatEvent(VideoSignal.Answer.getType());
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.bundle = new Bundle();
        VideoSessionData videoSessionData = new VideoSessionData(Integer.parseInt(remoteServiceCallData.apiKey),remoteServiceCallData.token,  remoteServiceCallData.sessionId);

        VideoChatContext videoChatContext = new VideoChatContext();
        videoChatContext.isInitiatingCall = false;
        videoChatContext.videoSessionData = videoSessionData;
        videoChatContext.profileName = remoteServiceCallData.phone;
        videoChatContext.profileImageUrl = remoteServiceCallData.profileImage;
        videoChatContext.invitationId = remoteServiceCallData.invitationId;

        navigationContext.bundle.putParcelable("videoChatContext", Parcels.wrap(videoChatContext));
        routingService.routeTo(Routes.ProfessionalVideoChatRoute, navigationContext);
    }

    public void denyCall() {

        controller.denyCall();

        routingService.routeToHome(view.getRoutingContext());

    }
}
