package it.clipcall.consumer.vidoechat.models.videoEvents;

import com.opentok.android.Session;

/**
 * Created by dorona on 17/05/2016.
 */
public class VideoChatReadyEvent extends SessionEvent {

    private final boolean isGeneratedByMe;

    public VideoChatReadyEvent(Session session, boolean isGeneratedByMe) {
        super(session);
        this.isGeneratedByMe = isGeneratedByMe;
    }


    public boolean isGeneratedByMe() {
        return isGeneratedByMe;
    }
}
