package it.clipcall.consumer.vidoechat.services;

import com.google.common.base.Strings;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.profile.models.Logo;
import it.clipcall.consumer.vidoechat.models.VideoInvitationData;
import it.clipcall.consumer.vidoechat.models.VideoProjectData;
import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.responses.StartVideoChatConversationResponse;
import it.clipcall.infrastructure.responses.ValidateInvitationResponse;
import it.clipcall.infrastructure.responses.eInvitationCheckInStatus;
import it.clipcall.infrastructure.support.network.RetrofitCallProcessor;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by dorona on 14/12/2015.
 */
@Singleton
public class VideoChatManager {

    private final IConsumerVideoChatService videoChatService;
    private VideoSessionData videoSessionData;

    private final ILogger logger = LoggerFactory.getLogger(VideoChatManager.class.getSimpleName());

    @Inject
    public VideoChatManager(IConsumerVideoChatService openTokService){
        this.videoChatService = openTokService;
    }


    public VideoSessionData startVideoChatConversation(String customerId, String invitationId){

        logger.debug("startVideoChatConversation with customerId %s and invitation id %s", customerId,invitationId);
        if(videoSessionData != null)
            return videoSessionData;

        try {
            Response<StartVideoChatConversationResponse> response = videoChatService.startVideoChatConversation(customerId, invitationId).execute();
            if(response == null)
                return null;

            StartVideoChatConversationResponse body = response.body();
            if(body == null)
                return null;

            videoSessionData = new VideoSessionData();
            videoSessionData.setApiKey(body.getApiKey());
            videoSessionData.setSessionId(body.getSessionId());
            videoSessionData.setToken(body.getToken());

            logger.debug("startVideoChatConversation - success. session id is %s", videoSessionData.getSessionId());

            return videoSessionData;
        } catch (IOException e) {
            logger.error("startVideoChatConversation - failed with message "+ e.getMessage(), e);
            return null;
        }

    }
    

    public VideoInvitationData getVideoInvitationData(String invitationId) throws IOException {
        logger.debug("getVideoInvitationData getting video invitation data for invation with id %s", invitationId);

        if(Strings.isNullOrEmpty(invitationId))
            return null;

        Response<ValidateInvitationResponse> response = videoChatService.validateInvitation(invitationId).execute();
        ValidateInvitationResponse validatedInvitation =  response.body();
        if (validatedInvitation == null)
            return null;




        if(!eInvitationCheckInStatus.success.equals(validatedInvitation.getCheckInStatus()))
            return null;

        VideoInvitationData data = new VideoInvitationData();
        data.setSupplierImage(validatedInvitation.getSupplierImage());
        data.setSupplierName(validatedInvitation.getSupplierName());
        data.setText(validatedInvitation.getText());

        return data;
    }

    public String uploadVideoPreviewImage(String customerId, File file) {
        try {
            if(Strings.isNullOrEmpty(customerId))
                return null;

            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            Response<Logo> logoResponse = videoChatService.uploadVideoPreviewImage(customerId, requestBody).execute();
            if(logoResponse == null)
            {
                return null;
            }

            Logo logo = logoResponse.body();
            if(logo == null){
                return null;
            }


            String url = logo.getUrl();
            return url;


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean updateVideoProjectData(String customerId, String invitationId, VideoProjectData projectData) {
        if(Strings.isNullOrEmpty(customerId))
            return false;

        if(Strings.isNullOrEmpty(invitationId))
            return false;

        try {
            videoChatService.updateProjectVideoPreviewImage(customerId, invitationId, projectData).execute();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public VideoSessionData createVideoSession(String customerId, String leadAccountId) {
        logger.debug("createVideoSession with customerId %s and leadAccountId %s", customerId, leadAccountId);
        Call<VideoSessionData> videoChatCall = videoChatService.createVideoSession(customerId, leadAccountId);
        VideoSessionData videoSessionData = RetrofitCallProcessor.processCall(videoChatCall);
        return videoSessionData;
    }

    public boolean answerVideoChatCall(String customerId, String invitationId) {
        logger.debug("answerVideoChatCall with customerId %s and invitationId %s", customerId, invitationId);
        Call<StatusResponse> statusResponseCall = videoChatService.answerVideoChatCall(customerId, invitationId);
        boolean success = RetrofitCallProcessor.processStatusCall(statusResponseCall);
        return success;
    }


}
