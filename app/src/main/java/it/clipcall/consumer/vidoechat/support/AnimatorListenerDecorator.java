package it.clipcall.consumer.vidoechat.support;


import android.view.View;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

/**
 * Created by micro on 1/11/2016.
 */
public class AnimatorListenerDecorator implements Animator.AnimatorListener {

    private final View view;
    private final Techniques techniques;
    private int defaultDurationInMilliseconds = 700;

    public AnimatorListenerDecorator(View view, Techniques techniques){

        this.view = view;
        this.techniques = techniques;
    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        YoYo.with(techniques)
                .duration(defaultDurationInMilliseconds)
                .playOn(view);
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }
}
