package it.clipcall.consumer.vidoechat.services;


import android.app.Application;
import android.content.Context;

import com.opentok.android.Connection;
import com.opentok.android.OpentokError;
import com.opentok.android.Session;
import com.opentok.android.Stream;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.consumer.vidoechat.models.videoEvents.ConnectedToSessionEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.DisconnectedFromSessionEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.SessionErrorEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.StreamReceivedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.SubscriberDestroyedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.VideoChatAnsweredEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.VideoChatCanceledEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.VideoChatReadyEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.VideoSignal;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;

@Singleton
public class VideoChatSessionProvider implements Session.ConnectionListener, Session.SessionListener, Session.SignalListener {


    private static final String TAG = "VideoChatSessionManager";
    private final Context context;
    private final EventAggregator eventAggregator;

    private Session session;

    private Stream stream;


    private  final ILogger logger  = LoggerFactory.getLogger(VideoChatSessionProvider.class.getSimpleName());

    @Inject
    public VideoChatSessionProvider(Application context, EventAggregator eventAggregator){

        this.context = context;
        this.eventAggregator = eventAggregator;
    }

    public void publishEvent(String eventType){

        logger.debug("publish event of type %s", eventType);
        session.sendSignal(eventType,"");
    }

    public void connectToSession(VideoSessionData videoSessionData){

        logger.debug("connectToSession: with session id %s", videoSessionData.getSessionId());

        if (session != null)
        {
            logger.debug("connectToSession: invalid state. session should have been null.");
        }

        session = new Session(context, videoSessionData.getApiKey()+"",videoSessionData.getSessionId());
        logger.debug("created session instance");
        session.setSessionListener(this);
        session.setConnectionListener(this);
        session.setSignalListener(this);
        logger.debug("set listeners on session instance, about to call connect");
        session.connect(videoSessionData.getToken());
        logger.debug("session.connect called");
        logger.debug("connectToSession: end");
    }



    /** Connection listener - start*/
    @Override
    public void onConnectionCreated(Session session, Connection connection) {
        logger.debug("onConnectionCreated: created connection with id %s",connection.getConnectionId());
    }

    @Override
    public void onConnectionDestroyed(Session session, Connection connection) {
        logger.debug("onConnectionDestroyed: destroyed connection with id - " + connection.getConnectionId());
    }

    public Session getSession() {
        return session;
    }

    public Stream getStream(){
        return stream;
    }

    /** Connection listener - end*/


    /** Session Listener - start*/
    @Override
    public void onConnected(Session session) {
        logger.debug("onConnected: connected successfully to session with id - " + session.getSessionId());
        eventAggregator.publish(new ConnectedToSessionEvent(session));
        this.session = session;

        //videoChatPublisher.publish(session);
    }

    @Override
    public void onDisconnected(Session session) {
        logger.debug("onDisconnected");
        if(this.session == null)
        {
            logger.debug("onDisconnected - session is already empty");
            return;
        }

        this.session.setSessionListener(null);
        this.session.setConnectionListener(null);
        this.session = null;

        eventAggregator.publish(new DisconnectedFromSessionEvent(session));
        logger.debug("onDisconnected: disconnected successfully from session with id - " + session.getSessionId());
    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {
        logger.debug("onStreamReceived: received stream with id - " + stream.getStreamId());
        this.stream = stream;
        eventAggregator.publish(new StreamReceivedEvent(session, stream));
        //videoChatSubscriber.initializeSubscriber(session,stream);

    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {
        eventAggregator.publish(new SubscriberDestroyedEvent(session, stream));
        logger.debug("onStreamDropped: dropped stream  with id - " + stream.getStreamId());

    }

    @Override
    public void onError(Session session, OpentokError opentokError) {
        logger.error("onError: received error - " + opentokError.getMessage());
        eventAggregator.publish(new SessionErrorEvent(session, opentokError));

    }

    /** Session Listener - end*/
    @Override
    public void onSignalReceived(Session session, String type, String data, Connection connection) {
        logger.debug("onSignalReceived: my session's connection id - " + session.getConnection().getConnectionId());
        logger.debug("onSignalReceived: connection's connection id - " + connection.getConnectionId());
        String myConnectionId = session.getConnection().getConnectionId();
        boolean isGeneratedByMe = myConnectionId.equals(connection.getConnectionId());
        if(type.equals(VideoSignal.Ready.getType())){
            VideoChatReadyEvent event = new VideoChatReadyEvent(session, isGeneratedByMe);
            logger.debug("publishing event %s. isGenerated by me: %s", "VideoChatReady", isGeneratedByMe+"");
            eventAggregator.publish(event);
            return;
        }else if(type.equals(VideoSignal.Answer.getType())){
            VideoChatAnsweredEvent event = new VideoChatAnsweredEvent(session, isGeneratedByMe);
            logger.debug("publishing event %s. isGenerated by me: %s", "VideoChatAnswered", isGeneratedByMe+"");
            eventAggregator.publish(event);
            return;
        }else if(type.equals(VideoSignal.Canceled.getType())){
            VideoChatCanceledEvent event = new VideoChatCanceledEvent(session, isGeneratedByMe);
            logger.debug("publishing event %s. isGenerated by me: %s", "VideoChatAnswered", isGeneratedByMe+"");
            eventAggregator.publish(event);
            return;
        }

        if (connection != null && connection.getConnectionId().equals(myConnectionId)) {
            // Signal received from another client
        }
    }


    public void disconnectFromSession() {
        logger.debug("disconnectFromSession");
        if(session == null)
        {
            logger.debug("disconnectFromSession - already disconnected");
            return;
        }
        //session.setSessionListener(null);
        //session.setConnectionListener(null);
        session.disconnect();
        logger.debug("disconnectFromSession - end");
    }
}
