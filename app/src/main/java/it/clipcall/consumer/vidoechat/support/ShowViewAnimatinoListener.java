package it.clipcall.consumer.vidoechat.support;

import android.view.View;
import android.view.animation.Animation;


/**
 * Created by dorona on 11/02/2016.
 */
public class ShowViewAnimatinoListener implements Animation.AnimationListener {
    private final View view;

    public ShowViewAnimatinoListener(View view) {

        this.view = view;
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
