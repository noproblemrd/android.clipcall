package it.clipcall.consumer.vidoechat.models.videoEvents;

import com.opentok.android.Session;

/**
 * Created by dorona on 16/05/2016.
 */
public class ConnectedToSessionEvent extends SessionEvent {

    public ConnectedToSessionEvent(Session session){
        super(session);
    }
}
