package it.clipcall.consumer.vidoechat.activities;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.transition.TransitionManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opentok.android.Publisher;
import com.opentok.android.Session;
import com.opentok.android.Stream;
import com.opentok.android.SubscriberKit;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.DrawableRes;
import org.parceler.Parcels;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.vidoechat.models.VideoChatContext;
import it.clipcall.consumer.vidoechat.presenters.VideoPublisherPresenter;
import it.clipcall.consumer.vidoechat.presenters.VideoSessionPresenter;
import it.clipcall.consumer.vidoechat.presenters.VideoSubscriberPresenter;
import it.clipcall.consumer.vidoechat.support.HideViewAnimatinoListener;
import it.clipcall.consumer.vidoechat.support.ShowViewAnimatinoListener;
import it.clipcall.consumer.vidoechat.views.IPublisherView;
import it.clipcall.consumer.vidoechat.views.ISessionView;
import it.clipcall.consumer.vidoechat.views.ISubscriberView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.ui.permissions.EmptyPermissionsHandler;

@EActivity(R.layout.activity_consumer_video_chat_calling)
public class ConsumerVideoChatCallingActivity extends BaseActivity implements ISubscriberView, IPublisherView, ISessionView {


    private final ILogger logger = LoggerFactory.getLogger(ConsumerVideoChatCallingActivity.class.getSimpleName());

    private int animationDuration = 400;

    @Extra
    Parcelable videoChatContext;

    @ViewById
    ImageView profileImageView;

    @ViewById
    TextView recordingTimeTextView;

    @ViewById
    RelativeLayout publisherViewContainer;

    @ViewById
    RelativeLayout subscriberViewContainer;

    @ViewById
    LinearLayout ringingContainer;

    @ViewById
    LinearLayout recordingContainer;

    @ViewById
    ImageView toggleCameraImageView;

    @ViewById
    ProgressBar loadingSpinner;

    @ViewById
    ProgressBar publisherLoadingSpinner;

    @ViewById
    ImageView fab;

    @ViewById
    View recordingIcon;

    @ViewById
    ViewGroup callInfoContainer;

    @ViewById
    ViewGroup videoFooterContainer;

    @DrawableRes(R.drawable.subscriber_video_background)
    Drawable subscriberVideobackground;

    @Inject
    VideoSubscriberPresenter subscriberPresenter;
    @Inject
    VideoPublisherPresenter publisherPresenter;
    @Inject
    VideoSessionPresenter sessionPresenter;

    @ViewById
    ViewGroup mainlayout;

    @ViewById
    TextView nameTextView;

    @ViewById
    ProgressBar loadingSpinnerProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
    }

    private int getPixelsFromDp(int dp){
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float fpixels = metrics.density * dp;
        int pixels = (int) (fpixels + 0.5f);
        return pixels;
    }

    @AfterViews
    void onViewsLoaded(){
        logger.debug("onViewsLoaded");
        ringingContainer.setVisibility(View.VISIBLE);
        recordingContainer.setVisibility(View.GONE);
        subscriberPresenter.bindView(this);
        publisherPresenter.bindView(this);
        sessionPresenter.bindView(this);

        final VideoChatContext chatContext = Parcels.unwrap(videoChatContext);
        nameTextView.setText(chatContext.profileName);

       /* if(chatContext.isInitiatingCall){
            RelativeLayout.LayoutParams publisherLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            publisherview.setLayoutParams(publisherLayoutParams);

            RelativeLayout.LayoutParams subscriberLayoutParams = new RelativeLayout.LayoutParams(getPixelsFromDp(100),getPixelsFromDp(140));
            int left = getPixelsFromDp(20);
            int top = getPixelsFromDp(20);
            int right = 0;
            int bottom = 0;
            subscriberLayoutParams.setMargins(left, top, right, bottom);
            subscriberViewContainer.setLayoutParams(subscriberLayoutParams);
        }*/

        sessionPresenter.requestVideoPermissions(new EmptyPermissionsHandler() {
            @Override
            public void handleAllPermissionsGranted() {

                logger.debug("initlalization. isInitiatingCall - ",chatContext.isInitiatingCall);

                sessionPresenter.initSession(chatContext);
                publisherPresenter.initPublisher( chatContext);
                subscriberPresenter.initializeSubscriber(chatContext);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        fab.setEnabled(true);
        loadingSpinnerProgressBar.setVisibility(View.INVISIBLE);
        fab.setVisibility(View.VISIBLE);
    }

    @Click(R.id.fab)
    void endCall(){
        logger.debug("endCall");
        fab.setEnabled(false);
        loadingSpinnerProgressBar.setVisibility(View.VISIBLE);
        fab.setVisibility(View.INVISIBLE);
        onDisconnected();
    }

    @UiThread
    @Override
    public void showView(SubscriberKit subscriber, boolean isInitiatingCall) {
        logger.debug("showView for subscriber with stream id %s. isInitiatingCall: %s", subscriber.getStream().getStreamId(), isInitiatingCall);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        final View subscriberView = subscriber.getView();
        subscriberView.setVisibility(View.INVISIBLE);
        if(subscriberView.getParent() != null){
            ViewGroup parentViewGroup = (ViewGroup) subscriberView.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
        subscriberView.setBackgroundResource(R.drawable.bordered);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            subscriberViewContainer.addView(subscriberView, layoutParams);
            TransitionManager.beginDelayedTransition(mainlayout);
            subscriberView.setVisibility(View.VISIBLE);

        }else{
            subscriberView.setVisibility(View.VISIBLE);
            subscriberViewContainer.addView(subscriberView, layoutParams);
            subscriberViewContainer.bringToFront();
            mainlayout.removeView(publisherViewContainer);
            mainlayout.addView(publisherViewContainer, 0);
        }

/*        subscriberViewContainer.bringToFront();
        mainlayout.removeView(publisherViewContainer);
        mainlayout.addView(publisherViewContainer, 0);*/

    }

    @UiThread
    @Override
    public void addView(SubscriberKit subscriberKit) {
        // mViewContainer is an Android View
    }

    @UiThread
    @Override
    public void onSubscriberDisconnected(SubscriberKit subscriberKit) {
        /*subscriberPresenter.removeSubscriber();
        onOtherSideDisconnected();*/
    }

    @UiThread
    @Override
    public void hideSubscriberSpinner() {
        loadingSpinner.setVisibility(View.GONE);
    }

    @UiThread
    @Override
    public void showSubscriberSpinner() {
        loadingSpinner.setVisibility(View.VISIBLE);
    }

    @UiThread
    @Override
    public void removeSubscriberView() {
        subscriberViewContainer.removeAllViews();
    }

    @UiThread
    @Override
    public void attachPublisherView(Publisher publisher, boolean isInitiatingCall) {
        logger.debug("attachPublisherView for publisher. isInitiatingCall: %s",  isInitiatingCall);
        View publisherView = publisher.getView();
        publisherViewContainer.removeView(publisherView);
        publisherViewContainer.addView(publisherView, 0);

        /*publisherView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionPresenter.toggleVideoActions();
            }
        });*/

        mainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionPresenter.toggleVideoActions();
            }
        });
    }

    @UiThread
    @Override
    public void showPublisherSpinner() {
        logger.debug("showPublisherSpinner");
        //publisherLoadingSpinner.setVisibility(View.VISIBLE);
    }

    @UiThread
    @Override
    public void hidePublisherSpinner() {
        logger.debug("hidePublisherSpinner");
        publisherLoadingSpinner.setVisibility(View.GONE);
    }

    @UiThread
    @Override
    public void removePublisherView() {
        logger.debug("removePublisherView");
        publisherViewContainer.removeAllViews();
    }

    private int dpToPx(int dp) {
        double screenDensity = this.getResources().getDisplayMetrics().density;
        return (int) (screenDensity * (double) dp);
    }

    @UiThread
    @Override
    public void showTotalTimeRecorded(final long totalTimeInSeconds) {
        int minutes = (int) (totalTimeInSeconds / (60));
        int seconds = (int) (totalTimeInSeconds % 60);
        String str = String.format("%d:%02d", minutes, seconds);
        recordingTimeTextView.setText(str);
    }


    @UiThread
    @Override
    public void onSubscriberConnected(Session session, Stream stream) {
        logger.debug("onSubscriberConnected with subscriber stream id: %s", stream.getStreamId());
        ringingContainer.setVisibility(View.GONE);
        recordingContainer.setVisibility(View.VISIBLE);
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.flash_animaiton);
        recordingIcon.startAnimation(myFadeInAnimation);
        subscriberPresenter.stopRinging();
        hidePublisherSpinner();
    }

    @UiThread
    @Override
    public void onSubscriberDisconnected(Session session, Stream stream) {
        logger.debug("onSubscriberDisconnected with subscriber stream id: %s", stream.getStreamId());
        onDisconnected();
    }


    @Background
    @Override
    public void onVideoPreviewRequested() {
        logger.debug("onVideoPreviewRequested");
        publisherPresenter.takeScreenshot();
    }

    @UiThread
    @Override
    public void showVideoActions(boolean shouldShow) {
        if(shouldShow){
            showHeader();
            showFooter();
        }else{
            hideHeader();
            hideFooter();
        }
    }

    @UiThread
    @Override
    public void onDisconnectedFromSession() {
        logger.debug("onDisconnectedFromSession");
        sessionPresenter.routeToHome();
    }

    private void showHeader(){
        if(callInfoContainer.getVisibility() != View.VISIBLE){
            ScaleAnimation anim = new ScaleAnimation(1, 1, 0, 1);
            anim.setDuration(animationDuration);
            anim.setAnimationListener(new ShowViewAnimatinoListener(callInfoContainer));
            callInfoContainer.startAnimation(anim);
        }

    }

    private void hideHeader(){
        if(callInfoContainer.getVisibility() != View.INVISIBLE){
            ScaleAnimation anim = new ScaleAnimation(1, 1, 1, 0);
            //callInfoContainer.setVisibility(View.INVISIBLE);
            anim.setDuration(animationDuration);
            anim.setAnimationListener(new HideViewAnimatinoListener(callInfoContainer));
            callInfoContainer.startAnimation(anim);
        }
    }

    private void showFooter(){
        if(videoFooterContainer.getVisibility() != View.VISIBLE){
            ScaleAnimation anim = new ScaleAnimation(1, 1, 0, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 1f);
            anim.setAnimationListener(new ShowViewAnimatinoListener(videoFooterContainer));
            anim.setDuration(animationDuration);
            videoFooterContainer.startAnimation(anim);
        }
    }

    private void hideFooter(){
        if(videoFooterContainer.getVisibility() != View.INVISIBLE){
            ScaleAnimation anim = new ScaleAnimation(1, 1, 1, 0, Animation.RELATIVE_TO_SELF, 1f, Animation.RELATIVE_TO_SELF, 1f);
            anim.setAnimationListener(new HideViewAnimatinoListener(videoFooterContainer));
            anim.setDuration(animationDuration);
            videoFooterContainer.startAnimation(anim);
        }
    }


    @Click(R.id.toggleCameraImageView)
    void swapCamera(){
        publisherPresenter.swapCamera();
    }

    @Override
    public void onBackPressed() {
        onDisconnected();
        super.onBackPressed();
    }


    void onDisconnected(){
        logger.debug("onDisconnected");
        subscriberPresenter.shutDown();
        publisherPresenter.shutDown();
        sessionPresenter.shutDown();
    }
}