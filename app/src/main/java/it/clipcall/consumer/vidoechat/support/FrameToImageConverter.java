package it.clipcall.consumer.vidoechat.support;

import android.graphics.Bitmap;
import android.os.Environment;

import com.opentok.android.BaseVideoRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/**
 * Created by dorona on 10/02/2016.
 */
public class FrameToImageConverter {

    public File ConvertToImage(BaseVideoRenderer.Frame frame){


        ByteBuffer bb = frame.getBuffer();
        bb.clear();

        int width = frame.getWidth();
        int height = frame.getHeight();
        int half_width = (width + 1) >> 1;
        int half_height = (height +1) >> 1;
        int y_size = width * height;
        int uv_size = half_width * half_height;

        byte []yuv = new byte[y_size + uv_size * 2];
        bb.get(yuv);
        int[] intArray = new int[width*height];

        // Decode Yuv data to integer array
        decodeYUV420(intArray, yuv, width, height);

        // Initialize the bitmap, with the replaced color
        Bitmap bmp = Bitmap.createBitmap(intArray, width, height, Bitmap.Config.ARGB_8888);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp, 400, 400, false);

        try {
            String path = Environment.getExternalStorageDirectory().toString();
            OutputStream fOutputStream = null;
            File file = new File(path, "opentok-capture-"+ System.currentTimeMillis() +".png");
            fOutputStream = new FileOutputStream(file);

            scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, fOutputStream);

            fOutputStream.flush();
            fOutputStream.close();

            return file;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    static public void decodeYUV420(int[] rgba, byte[] yuv420, int width, int height) {
        int half_width = (width + 1) >> 1;
        int half_height = (height +1) >> 1;
        int y_size = width * height;
        int uv_size = half_width * half_height;

        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {

                double y = (yuv420[j * width + i]) & 0xff;
                double v = (yuv420[y_size + (j >> 1) * half_width + (i>>1)]) & 0xff;
                double u = (yuv420[y_size + uv_size + (j >> 1) * half_width + (i>>1)]) & 0xff;

                double r;
                double g;
                double b;

                r = y + 1.402 * (u-128);
                g = y - 0.34414*(v-128) - 0.71414*(u-128);
                b = y + 1.772*(v-128);

                if (r < 0) r = 0;
                else if (r > 255) r = 255;
                if (g < 0) g = 0;
                else if (g > 255) g = 255;
                if (b < 0) b = 0;
                else if (b > 255) b = 255;

                int ir = (int)r;
                int ig = (int)g;
                int ib = (int)b;
                rgba[j * width + i] = 0xff000000 | (ir << 16) | (ig << 8) | ib;
            }
        }
    }
}
