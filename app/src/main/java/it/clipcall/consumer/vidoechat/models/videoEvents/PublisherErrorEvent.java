package it.clipcall.consumer.vidoechat.models.videoEvents;

import com.opentok.android.OpentokError;
import com.opentok.android.PublisherKit;

/**
 * Created by dorona on 16/05/2016.
 */
public class PublisherErrorEvent extends PublisherEvent {

    private final OpentokError error;

    public PublisherErrorEvent(PublisherKit publisherKit, OpentokError error) {
        super(publisherKit);
        this.error = error;
    }

    public OpentokError getError() {
        return error;
    }
}
