package it.clipcall.consumer.vidoechat.models.videoEvents;

import com.opentok.android.SubscriberKit;

/**
 * Created by dorona on 16/05/2016.
 */
public class SubscriberErrorEvent extends SubscriberEvent {
    public SubscriberErrorEvent(SubscriberKit subscriberKit) {
        super(subscriberKit);
    }
}
