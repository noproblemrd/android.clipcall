package it.clipcall.consumer.vidoechat.models.videoEvents;

import com.opentok.android.PublisherKit;

/**
 * Created by dorona on 16/05/2016.
 */
public abstract class PublisherEvent {

    private final PublisherKit publisherKit;

    public PublisherEvent(PublisherKit publisherKit){

        this.publisherKit = publisherKit;
    }

    public PublisherKit getPublisherKit() {
        return publisherKit;
    }
}
