package it.clipcall.consumer.vidoechat.models;

/**
 * Created by dorona on 10/01/2016.
 */
public class VideoSessionEvent {

    public VideoSessionEvent(String sessionId) {
        this.sessionId = sessionId;
    }

    public String sessionId;

}
