package it.clipcall.consumer.vidoechat.models.videoEvents;

import com.opentok.android.PublisherKit;
import com.opentok.android.Stream;

/**
 * Created by dorona on 16/05/2016.
 */
public class PublisherCreatedEvent extends PublisherEvent {
    private final Stream stream;

    public PublisherCreatedEvent(PublisherKit publisherKit, Stream stream) {
        super(publisherKit);
        this.stream = stream;
    }

    public Stream getStream() {
        return stream;
    }
}
