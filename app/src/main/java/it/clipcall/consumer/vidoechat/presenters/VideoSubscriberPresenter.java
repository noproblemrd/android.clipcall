package it.clipcall.consumer.vidoechat.presenters;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;

import com.opentok.android.BaseVideoRenderer;
import com.opentok.android.Subscriber;
import com.opentok.android.SubscriberKit;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.ThreadMode;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.consumer.vidoechat.models.VideoChatContext;
import it.clipcall.consumer.vidoechat.models.VideoSessionEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.SubscriberConnectedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.SubscriberDisconnectedEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.VideoChatAnsweredEvent;
import it.clipcall.consumer.vidoechat.models.videoEvents.VideoChatCanceledEvent;
import it.clipcall.consumer.vidoechat.services.VideoChatCoordinator;
import it.clipcall.consumer.vidoechat.views.ISubscriberView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.presenters.PresenterBase;

@PerActivity
public class VideoSubscriberPresenter extends PresenterBase<ISubscriberView>  {

    private final ILogger logger = LoggerFactory.getLogger(VideoSubscriberPresenter.class.getSimpleName());

    private MediaPlayer mediaPlayer;
    private final Activity activity;
    private final VideoChatCoordinator videoChatCoordinator;
    private final EventAggregator eventAggregator;
    private VideoChatContext chatContext;

    @Inject
    public VideoSubscriberPresenter(Activity activity, VideoChatCoordinator videoChatCoordinator, EventAggregator eventAggregator) {
        this.activity = activity;
        this.videoChatCoordinator = videoChatCoordinator;
        this.eventAggregator = eventAggregator;
        this.eventAggregator.subscribe(this);
    }

    @Override
    public void shutDown() {
        super.shutDown();
        releaseSubscriber();
        releaseMediaPlayer();
        //eventAggregator.unSubscribe(this);;
    }



    @org.simple.eventbus.Subscriber(mode = ThreadMode.ASYNC)
    void onSubscriberDisconnected(SubscriberDisconnectedEvent event) {
        EventBus.getDefault().post(new ApplicationEvent("VidChat-Cons-Adv-Disconnected", new VideoSessionEvent(event.getSubscriberKit().getSession().getSessionId())));
        logger.debug("Receieved event - SubscriberDisconnectedEvent with session id %s", event.getSubscriberKit().getSession().getSessionId());

        view.onSubscriberDisconnected(event.getSubscriberKit());
    }


   /* @Override
    public void onError(SubscriberKit subscriberKit, OpentokError opentokError) {
       // EventBus.getDefault().post(new MixPanelEvent("VidChat-Cons-Adv-Conn-Error", new VideoSessionEvent(subscriberKit.getSession().getSessionId())));
        Log.i(LOGTAG, "subscriber exception: " + opentokError.getMessage());
    }*/


    @org.simple.eventbus.Subscriber(mode = ThreadMode.ASYNC)
    void onSubscriberConnected(SubscriberConnectedEvent event) {
        /*logger.debug("Receieved event - SubscriberConnectedEvent with session id %s", event.getSubscriberKit().getSession().getSessionId());

        view.addView(event.getSubscriberKit());

        EventBus.getDefault().post(new ApplicationEvent("VidChat-Cons-Adv-Connected", new VideoSessionEvent(event.getSubscriberKit().getSession().getSessionId())));*/
    }

    @org.simple.eventbus.Subscriber
    void onVideoChatCanceled(VideoChatCanceledEvent event){
        if(event.isGeneratedByMe())
            return;

        shutDown();
    }


    @org.simple.eventbus.Subscriber(mode = ThreadMode.ASYNC)
    void onVideoChatAnswered(VideoChatAnsweredEvent event) {
        Subscriber subscriber = videoChatCoordinator.getSubscriber();
        logger.debug("Receieved event - VideoChatAnsweredEvent with session id %s", subscriber.getSession().getSessionId());
        view.hideSubscriberSpinner();
        attachSubscriberView(subscriber);
    }

    public void initializeSubscriber(VideoChatContext chatContext) {
        logger.debug("initializeSubscriber with invitationId %s and isInitiatingCall %s", chatContext.invitationId, chatContext.isInitiatingCall +"");

        this.chatContext = chatContext;

        if(mediaPlayer == null && chatContext.isInitiatingCall)
        {
            AudioManager am = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
            am.setStreamVolume(AudioManager.STREAM_MUSIC, am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
            logger.debug("Subscriber - starting ring");
            mediaPlayer = MediaPlayer.create(activity, R.raw.video_chat_outgoing_call_ring);
            mediaPlayer.setLooping(true);
            mediaPlayer.start();
        }
    }

    public void stopRinging(){
        logger.debug("Subscriber - stopping ring");
        if(mediaPlayer != null){

            mediaPlayer.stop();
            mediaPlayer = null;
        }
    }

    private void attachSubscriberView(SubscriberKit subscriber) {
        logger.debug("attachSubscriberView");
        subscriber.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);
        if(chatContext == null){
            logger.error("attachSubscriberView - chatContext cannot be null.");

        }

        boolean isInitiatingCall = chatContext != null ? chatContext.isInitiatingCall : false;
        view.showView(subscriber, isInitiatingCall);
    }

    private void releaseMediaPlayer(){
        logger.debug("releaseMediaPlayer");
        if(mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer = null;
        }
    }

    private void releaseSubscriber(){
        logger.debug("releaseSubscriber");

        if(view == null){
            view = (ISubscriberView) activity;
        }
        view.removeSubscriberView();

        videoChatCoordinator.releaseSubscriber();

    }
}
