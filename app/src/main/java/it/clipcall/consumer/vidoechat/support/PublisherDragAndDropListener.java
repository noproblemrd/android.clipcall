package it.clipcall.consumer.vidoechat.support;

import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by dorona on 05/01/2016.
 */
public class PublisherDragAndDropListener implements View.OnTouchListener, View.OnDragListener {

    @Override
    public boolean onDrag(View v, DragEvent event) {

//        if(event.getAction()==DragEvent.ACTION_DROP){
//            //we want to make sure it is dropped only to left and right parent view
//            View view = (View)event.getLocalState();
//
//            if(v.getId() == R.id.left_view || v.getId() == R.id.right_view){
//
//                ViewGroup source = (ViewGroup) view.getParent();
//                source.removeView(view);
//
//                LinearLayout target = (LinearLayout) v;
//                target.addView(view);
//            }
//            //make view visible as we set visibility to invisible while starting drag
//            view.setVisibility(View.VISIBLE);
//        }
        return true;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN){
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
            view.startDrag(null, shadowBuilder, view, 0);
            view.setVisibility(View.INVISIBLE);
            return true;
        }
        return false;
    }
}
