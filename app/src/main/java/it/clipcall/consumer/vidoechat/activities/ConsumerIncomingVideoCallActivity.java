package it.clipcall.consumer.vidoechat.activities;

import android.Manifest;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.parceler.Parcels;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.vidoechat.models.RemoteServiceCallData;
import it.clipcall.consumer.vidoechat.presenters.ConsumerIncomingVideoCallPresenter;
import it.clipcall.consumer.vidoechat.views.IConsumerIncomingVideoCallView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.aspects.SecureMethod;
import it.clipcall.infrastructure.ui.images.CircleTransform;

@EActivity(R.layout.professional_incoming_video_call_activity)
public class ConsumerIncomingVideoCallActivity extends BaseActivity implements IConsumerIncomingVideoCallView, IHasToolbar {

    @Extra
    Parcelable remoteServiceCallContext;

    @ViewById
    ProgressBar loadingSpinner;

    @ViewById
    ImageView profileImageView;

    @ViewById
    TextView phoneNumberTextView;

    @ViewById
    ImageView acceptPhoneImageView;


    @ViewById
    ViewGroup mainlayout;

    @ViewById
    Toolbar toolbar;

    @Inject
    ConsumerIncomingVideoCallPresenter presenter;

    @AfterViews
    void afterViews(){
        RemoteServiceCallData remoteServiceCallData = Parcels.unwrap(remoteServiceCallContext);
        presenter.setRemoteServiceCallData(remoteServiceCallData);
        presenter.bindView(this);
        presenter.initialize();
        presenter.connectToVideoSessionInBackground();
        presenter.initializePublisher();
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    @Background
    void initialize(){


    }

    @UiThread
    @Override
    public void showRemoteServiceCall(RemoteServiceCallData remoteServiceCallData) {
        phoneNumberTextView.setText(remoteServiceCallData.phone);
        if(Strings.isNullOrEmpty(remoteServiceCallData.profileImage)){
            Picasso.with(this).load(R.drawable.icons_profile_missing).transform(new CircleTransform()).into(profileImageView);
        }else{
            Picasso.with(this).load(remoteServiceCallData.profileImage).transform(new CircleTransform()).into(profileImageView);
        }
    }

    @UiThread
    @Override
    public void showAcceptingCallInProgress() {
        loadingSpinner.setVisibility(View.VISIBLE);
        acceptPhoneImageView.setVisibility(View.GONE);
    }

    @UiThread
    @Override
    public void showFailedAcceptingCall() {
        Snackbar.make(mainlayout,"Oops...Something went wrong. Failed accepting call.",Snackbar.LENGTH_LONG).show();
    }


    @UiThread
    @Override
    public void showOrHideAcceptCall(boolean show){
        if(show){
            loadingSpinner.setVisibility(View.GONE);
            acceptPhoneImageView.setVisibility(View.VISIBLE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }else{
            loadingSpinner.setVisibility(View.VISIBLE);
            acceptPhoneImageView.setVisibility(View.GONE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        loadingSpinner.setVisibility(View.GONE);
        acceptPhoneImageView.setVisibility(View.VISIBLE);
    }

    @Background
    void onDenyCallTapped(){
        presenter.denyCall();
        presenter.shutDown();
    }

    @SecureMethod(title="Permissions needed",message = "We need your permissions to start a video chat",permissions = {Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO})
    @Click(R.id.acceptPhoneImageView)
    void onAcceptCallTapped(){
        presenter.acceptCall();
    }


    @Override
    public void onBackPressed() {
        onDenyCallTapped();
    }

    @Override
    public String getViewTitle() {
        return "LIVE CLIPCALL";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        onDenyCallTapped();
        return true;
    }
}
