package it.clipcall.consumer.about.activities;

import android.os.Build;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ViewGroup;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;
import es.voghdev.pdfviewpager.library.remote.RemotePDFViewPager;
import it.clipcall.R;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.support.pdf.PdfTools;

@EActivity(R.layout.pdf_layout)
public class PdfActivity extends BaseActivity implements IHasToolbar,  DownloadFile.Listener  {

    private static final String TAG = "Error" ;

    @Extra("title")
    String title;

    @Extra("url")
    String url;

    @ViewById
    ViewGroup pdfViewContainer;


    @ViewById
    Toolbar toolbar;

    RemotePDFViewPager remotePDFViewPager;

    PDFPagerAdapter adapter;

    @AfterViews
    void afterViews(){
        if(Build.VERSION.SDK_INT  >= Build.VERSION_CODES.LOLLIPOP )
        {
            remotePDFViewPager = new RemotePDFViewPager(this,url, this);
            pdfViewContainer.addView(remotePDFViewPager);
        }else{
            PdfTools.showPDFUrl(this,url);
        }

   }

    @Override
    public String getViewTitle() {
        return title;
    }


    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }

    @Override
    public void onSuccess(String url, String destinationPath) {
        adapter = new PDFPagerAdapter(this, destinationPath);
        remotePDFViewPager.setAdapter(adapter);
    }

    @Override
    public void onFailure(Exception e) {

    }

    @Override
    public void onProgressUpdate(int progress, int total) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(adapter != null){
            try{
                adapter.close();
            }
            catch (Exception e){
                Log.e(TAG, "onDestroy: "+e.getMessage(),e );
            }
        }

    }
}
