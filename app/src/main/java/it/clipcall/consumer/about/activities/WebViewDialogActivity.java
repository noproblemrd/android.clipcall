package it.clipcall.consumer.about.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.about.presenters.WebViewPresenter;
import it.clipcall.consumer.about.views.IWebView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.annotations.OverrideDefaultStyle;

@EActivity(R.layout.webview_dialog_layout)
@OverrideDefaultStyle
public class WebViewDialogActivity extends BaseActivity implements IWebView {

    @ViewById
    WebView webView;

    @ViewById
    ViewGroup actionsContainer;


    @ViewById
    ProgressBar loadingSpinnerProgressBar;


    @Inject
    WebViewPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.webViewDialogStyle);
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @AfterViews
    void afterViews(){
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                loadingSpinnerProgressBar.setProgress(progress);
                if (progress == 100) {
                    loadingSpinnerProgressBar.setVisibility(View.GONE);

                } else {
                    loadingSpinnerProgressBar.setVisibility(View.VISIBLE);

                }
            }
        });

        presenter.bindView(this);
        presenter.initialize();
   }

    @UiThread
    @Override
    public void showDetails(String url, String title) {
        webView.loadUrl(url);
    }

    @Override
    public void showActions() {
        actionsContainer.setVisibility(View.VISIBLE);
    }

    @Click(R.id.declineButton)
    void declineTermsOfConditions(){
        setAccepted(false);
    }


    @Click(R.id.acceptButton)
    void acceptTermsOfConditions(){
        setAccepted(true);
    }

    private void setAccepted(boolean accepted){
        Intent intent = this.getIntent();
        intent.putExtra("accepted", accepted);
        this.setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        setAccepted(false);
    }
}
