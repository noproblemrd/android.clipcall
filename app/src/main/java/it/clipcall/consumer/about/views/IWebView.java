package it.clipcall.consumer.about.views;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 19/01/2016.
 */
public interface IWebView extends IView {
    void showDetails(String url, String title);

    void showActions();
}
