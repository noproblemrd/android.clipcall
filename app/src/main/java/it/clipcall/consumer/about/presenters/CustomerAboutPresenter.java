package it.clipcall.consumer.about.presenters;

import javax.inject.Inject;

import it.clipcall.consumer.about.views.ICustomerAboutView;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;


public class CustomerAboutPresenter extends PresenterBase<ICustomerAboutView> {

    private final RoutingService routingService;

    @Inject
    public CustomerAboutPresenter(RoutingService routingService) {
        this.routingService = routingService;
    }


    public void navigateToEula(){
        NavigationContext context = new NavigationContext();
        context.routingContext = view.getRoutingContext();
        context.addParameter("url","https://storage.googleapis.com/clipcallterms/ClipCall_EULA.pdf");
        context.addParameter("title","EULA");
        routingService.routeTo(Routes.ConsumerEulaRoute,context);
    }

    public void navigateToPrivacyPolicy(){
        NavigationContext context = new NavigationContext();
        context.routingContext = view.getRoutingContext();
        context.addParameter("url","https://storage.googleapis.com/clipcallterms/ClipCall_PrivacyPolicy.pdf");
        context.addParameter("title","PRIVACY POLICY");

        routingService.routeTo(Routes.ConsumerPrivacyPolicyRoute,context);
    }
}
