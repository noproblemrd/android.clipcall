package it.clipcall.consumer.about.presenters;

import javax.inject.Inject;

import it.clipcall.consumer.about.views.IWebView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.services.RoutingService;

@PerActivity
public class WebViewPresenter extends PresenterBase<IWebView> {
    private final RouteParams routeParams;
    private final RoutingService routingService;
    private final Boolean showActions;


    private String title;
    private String url;

    @Inject
    public WebViewPresenter(RouteParams routeParams, RoutingService routingService) {
        this.routeParams = routeParams;
        this.routingService = routingService;
        this.title = routeParams.getParam("title");
        this.url = routeParams.getParam("url");
        this.showActions = routeParams.getParam("showActions");
    }

    @Override
    public void initialize() {
        view.showDetails(url,title);
        if(showActions)
            view.showActions();

    }
}
