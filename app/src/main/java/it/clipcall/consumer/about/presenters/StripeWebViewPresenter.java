package it.clipcall.consumer.about.presenters;

import com.google.common.base.Strings;

import javax.inject.Inject;

import it.clipcall.common.environment.Environment;
import it.clipcall.consumer.about.views.IStripeWebView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.services.RoutingService;

@PerActivity
public class StripeWebViewPresenter extends PresenterBase<IStripeWebView> {
    private final RouteParams routeParams;
    private final RoutingService routingService;

    private String professionalId;


    @Inject
    public StripeWebViewPresenter(RouteParams routeParams, RoutingService routingService) {
        this.routeParams = routeParams;
        this.routingService = routingService;
        this.professionalId = routeParams.getParam("professionalId");
    }

    @Override
    public void initialize() {
        String url = "https://connect.stripe.com/oauth/authorize?response_type=code&client_id="+Environment.StripeClientId+"&scope=read_write&stripe_landing=login&redirect_uri=" + Environment.StripeRedirectUrl + "&state="+professionalId;
        //String url = "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_7cwmQtgVqDU8XUlgG6nRYHxjavisAVK8&scope=read_write&stripe_landing=login&redirect_uri=http%3A%2F%2F10.0.0.96%2FMobileAPI%2Fstripe%2Findex&state="+professionalId;
        view.showStripeLogin(url);
    }

    public void routeToHome() {
        routingService.routeToHome(view.getRoutingContext());
    }

    public void onRedirect(String input) {
        if(Strings.isNullOrEmpty(input) || "FALSE".equals(input.toUpperCase()))
        {
            view.showMessage("Failed logging in with your stripe account. Please try again later.");
            return;
        }

        view.showMessageAndRedirectToHome("Logged in sucessfully to stripe.");
    }
}
