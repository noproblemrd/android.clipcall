package it.clipcall.consumer.about.activities;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.about.presenters.WebViewPresenter;
import it.clipcall.consumer.about.views.IWebView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;

@EActivity(R.layout.webview_layout)
public class WebViewActivity extends BaseActivity implements IHasToolbar, IWebView {

    @ViewById
    WebView webView;


    @ViewById
    Toolbar toolbar;

    @ViewById
    ProgressBar loadingSpinnerProgressBar;


    @Inject
    WebViewPresenter presenter;

    @AfterViews
    void afterViews(){
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                loadingSpinnerProgressBar.setProgress(progress);
                if (progress == 100) {
                    loadingSpinnerProgressBar.setVisibility(View.GONE);

                } else {
                    loadingSpinnerProgressBar.setVisibility(View.VISIBLE);

                }
            }
        });

        presenter.bindView(this);
        presenter.initialize();

   }

    @Override
    public String getViewTitle() {
        return "";
    }


    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }

    @UiThread
    @Override
    public void showDetails(String url, String title) {
        webView.loadUrl(url);
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void showActions() {

    }
}
