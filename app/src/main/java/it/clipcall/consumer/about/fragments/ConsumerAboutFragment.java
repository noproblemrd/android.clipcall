package it.clipcall.consumer.about.fragments;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.about.presenters.CustomerAboutPresenter;
import it.clipcall.consumer.about.views.ICustomerAboutView;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.routing.models.RoutingContext;

@EFragment(R.layout.consumer_fragment_about)
public class ConsumerAboutFragment extends BaseFragment implements ICustomerAboutView {


    private boolean isNavigating;

    private final String[] actions = new String[]{
            "Eula",
            "Privacy Policy"
    };

    @Inject
    CustomerAboutPresenter presenter;

    @ViewById
    ListView actionsListView;


    @AfterViews
    void afterAboutViewLoaded(){
        presenter.bindView(this);
        actionsListView.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.about_nav_item, actions));
        isNavigating = false;

        actionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                navigateTo(actions[position]);
            }
        });
    }



    void navigateTo(String routeName){
        if(isNavigating)
            return;

        isNavigating = true;

        if(routeName.equals("Eula"))
        {
            presenter.navigateToEula();
            isNavigating = false;
            return;
        }

        if(routeName.equals("Privacy Policy")){
            presenter.navigateToPrivacyPolicy();
            isNavigating = false;
            return;
        }
    }

    @Override
    public RoutingContext getRoutingContext() {
        return new RoutingContext(getActivity());
    }

    @Override
    protected String getTitle() {
        return "About";
    }

    @Override
    protected int getMenuItemId() {
        return R.id.nav_about_consumer;
    }
}
