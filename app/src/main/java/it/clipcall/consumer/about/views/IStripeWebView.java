package it.clipcall.consumer.about.views;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 19/01/2016.
 */
public interface IStripeWebView extends IView {

    void showStripeLogin(String url);

    void showMessage(String message);

    void showMessageAndRedirectToHome(String message);
}
