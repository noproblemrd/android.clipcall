package it.clipcall.consumer.projects.services;


import java.util.List;

import it.clipcall.consumer.findprofessional.models.ExclusivePro;
import it.clipcall.consumer.findprofessional.models.ExclusiveProRef;
import it.clipcall.consumer.findprofessional.models.NewProjectEntity;
import it.clipcall.consumer.findprofessional.models.Tip;
import it.clipcall.consumer.payment.models.PaymentProfessional;
import it.clipcall.consumer.payment.models.PaymentProject;
import it.clipcall.consumer.projects.models.Advertiser;
import it.clipcall.consumer.projects.models.AdvertiserData;
import it.clipcall.consumer.projects.models.BonusInfo;
import it.clipcall.consumer.projects.models.CallHistoryItem;
import it.clipcall.consumer.projects.models.ConsumerDiscount;
import it.clipcall.consumer.projects.models.CustomerPaymentMethod;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.infrastructure.CreateProjectStatusResponse;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.professional.profile.models.Category;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ICustomerProjectsService {
    @GET("v3/customers/{customerId}/leads")
    Call<List<ProjectEntity>> getCustomerProjects(@Path("customerId") String  customerId, @Query("pageSize") int pageSize, @Query("supplierLeadStatus") String supplierLeadStatus, @Query("pageNumber") int pageNumber);


    @GET("customers/{customerId}/leads/{leadId}/advertisers")
    Call<List<Advertiser>> getProjectAdvertisers(@Path("customerId") String  customerId, @Path("leadId") String  leadId);

    @GET("customers/{customerId}/leads/{leadId}/advertisers/{advertiserId}/about")
    Call<AdvertiserData>  getAdvertiserData(@Path("customerId")String customerId, @Path("leadId") String leadId,  @Path("advertiserId") String advertiserId);


    @GET("customers/{customerId}/leads/{leadAccountId}/advertisers/{advertiserId}/calls/history")
    Call<List<CallHistoryItem>>  getAdvertiserCallHistory(@Path("customerId")String customerId,@Path("leadAccountId") String leadAccountId,   @Path("advertiserId") String  advertiserId);


    @GET("v5/customers/{customerId}/leads/{leadAccountId}/advertisers/{advertiserId}/history")
    Call<List<HistoryItem>>  getAdvertiserHistoryItems(@Path("customerId")String customerId, @Path("leadAccountId") String leadAccountId, @Path("advertiserId") String  advertiserId);
  


    @POST("customers/{customerId}/love/{advertiserId}")
    Call<StatusResponse> likeAdvertiser(@Path("customerId")String customerId,@Path("advertiserId") String  advertiserId);

    @POST("customers/{customerId}/connectandrecord/{incidentAccountId}")
    Call<StatusResponse>  callTheAdvertiser(@Path("customerId")String customerId,@Path("incidentAccountId") String incidentAccountId);


    @POST("customers/{customerId}/leads")
    Call<CreateProjectStatusResponse> createProject(@Path("customerId")String customerId, @Body NewProjectEntity project);

    @GET("customers/{customerId}/leads/{leadId}/advertisers")
    Call<List<Advertiser>> getAdvertisres(@Path("customerId") String  customerId, @Path("leadId") String  leadId);

    @GET("categories/GetCategoryKeywords")
    Call<List<Category>> getAvailableCategories();

    @GET("commontip/{categoryId}")
    Call<List<Tip>> getCategoryTips(@Path("categoryId") String  categoryId);

    @POST("videochat/{invitationId}")
    Call<ExclusivePro> getExclusivePro(@Path("invitationId")String invitationId);


    @GET("customers/{customerId}/leads/{categoryId}/favorite")
    Call<List<ExclusiveProRef>> getCustomerExclusiveProfessionals(@Path("customerId") String  customerId, @Path("categoryId") String  categoryId);

    @GET("videochat/{invitationId}/{categoryId}")
    Call<ExclusiveProRef> getExclusiveProForCategory(@Path("invitationId")String invitationId, @Path("categoryId") String categoryId);

    @POST("customers/{customerId}/videochat/{invitationId}/setfavorite")
    Call<StatusResponse> setExclusiveProForInvitation(@Path("customerId") String  customerId, @Path("invitationId") String  invitationId);

    @POST("customers/{customerId}/chat/{leadAccountId}/chatcreated")
    Call<StatusResponse> notifyChatCreated(@Path("customerId") String customerId, @Path("leadAccountId") String leadAccountId);


    @GET("customers/{customerId}/leads/connected")
    Call<List<PaymentProfessional>> getCustomerConnectedProfessionals(@Path("customerId") String  customerId);

    @GET("customers/{customerId}/leads/connected/{supplierId}")
    Call<List<PaymentProject>> getProfessionalProjects(@Path("customerId") String  customerId, @Path("supplierId") String  supplierId);

    @GET("customers/{customerId}/bonus")
    Call<ConsumerDiscount> isDiscountClaimed(@Path("customerId") String  customerId);

    @POST("customers/{customerId}/bonus")
    Call<StatusResponse> claimDiscount(@Path("customerId") String customerId);

    @POST("customers/{customerId}/leads/{leadAccountId}/quotes/request")
    Call<StatusResponse> requestQuote(@Path("customerId") String  customerId, @Path("leadAccountId") String leadAccountId);

    @POST("v2/customers/{customerId}/leads/{leadAccountId}/quotes/{quoteId}/book")
    Call<CustomerPaymentMethod> bookQuote(@Path("customerId") String  customerId, @Path("leadAccountId") String leadAccountId, @Path("quoteId") String quoteId);

    @GET("customers/{customerId}/bonus/{leadAccountId}/hasbonus")
    Call<BonusInfo> getBonusEntitled(@Path("customerId") String  customerId, @Path("leadAccountId") String leadAccountId);
}
