package it.clipcall.consumer.projects.models;

import com.google.common.base.Strings;

import java.io.Serializable;
import java.util.Date;

import it.clipcall.common.chat.models.IConversation;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.professional.leads.models.Quote;

/**
 * Created by dorona on 03/04/2016.
 */
public class ProfessionalRef implements Serializable {

    String leadAccountId;
    String supplierId;
    String customerId;
    String supplierName;
    String supplierIcon;
    int totalUnreadMessages;
    String leadAccountStatus;


    Advertiser advertiser;

    Date lastUnreadMessageDate;
    IMessage lastSentMessage;

    Quote lastQuote;

    boolean quoteRequested;



    public String getLeadAccountId() {
        return leadAccountId;
    }

    public void setLeadAccountId(String leadAccountId) {
        this.leadAccountId = leadAccountId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierIcon() {
        return supplierIcon;
    }

    public void setSupplierIcon(String supplierIcon) {
        this.supplierIcon = supplierIcon;
    }

    public void setTotalUnreadMessages(int totalUnreadMessages) {
        this.totalUnreadMessages = totalUnreadMessages;
    }



    public int getTotalUnreadMessages() {
        return totalUnreadMessages;
    }

    public Advertiser getAdvertiser() {
        return advertiser;
    }

    public void setAdvertiser(Advertiser advertiser) {
        this.advertiser = advertiser;
    }

    public void updateTotalUnreadMessages(ChatManager chatManager) {
        IConversation conversation  = chatManager.getConversationWithParticipant(getCustomerId(), getLeadAccountId());
        if(conversation == null)
            return;

        Integer proUnreadMessages = conversation.getTotalUnreadMessageCount();
        if(proUnreadMessages != null){
            setTotalUnreadMessages(proUnreadMessages);
        }
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void updateLastUnreadMessageDate(ChatManager chatManager, String receiverId) {
        if(isLost())
        {
            setLastUnreadMessageDate(null);
            return;
        }

        IConversation conversation = chatManager.getConversationWithParticipant(getCustomerId(), getLeadAccountId());
        if (conversation == null)
            return;

        IMessage lastUnreadMessage = conversation.getLastUnreadMessage(getCustomerId(), receiverId);
        if (lastUnreadMessage != null) {
            setLastUnreadMessageDate(lastUnreadMessage.getSentAt());
        }

        IMessage lastSentMessage = chatManager.getLastSentMessage(conversation, getCustomerId());
        if (lastSentMessage != null) {
            setLastSentMessage(lastSentMessage);
        }

    }

    public void setLastUnreadMessageDate(Date lastUnreadMessageDate) {
        this.lastUnreadMessageDate = lastUnreadMessageDate;
    }

    public Date getLastUnreadMessageDate() {
        return lastUnreadMessageDate;
    }

    public void setLastSentMessage(IMessage lastSentMessage) {
        this.lastSentMessage = lastSentMessage;
    }

    public IMessage getLastSentMessage() {
        return lastSentMessage;
    }

    public Quote getLastQuote() {
        return lastQuote;
    }

    public void setLastQuote(Quote lastQuote) {
        this.lastQuote = lastQuote;
    }



    public boolean canRequestQuote(){
        if(isLost())
            return false;

        if(isBooked())
            return false;

        if(isQuoteRequested())
            return false;

        if(getLastQuote() != null)
            return false;

        return true;
    }

    public boolean isQuoteRequested() {
        return quoteRequested;
    }

    public void setQuoteRequested(boolean quoteRequested) {
        this.quoteRequested = quoteRequested;
    }

    public boolean isBooked() {
        Quote lastQuote = getLastQuote();
        if(lastQuote == null)
            return false;

        return lastQuote.isBooked();
    }

    public boolean quoteIsPending() {
        Quote lastQuote = getLastQuote();
        if(lastQuote == null)
            return false;

        return lastQuote.isPending();
    }

    public void markAsLost() {
        setLeadAccountStatus("LOST");
        Quote lastQuote = getLastQuote();
        if(lastQuote != null)
            lastQuote.markAsLost();
    }

    public String getLeadAccountStatus() {
        return leadAccountStatus;
    }

    public void setLeadAccountStatus(String leadAccountStatus) {
        this.leadAccountStatus = leadAccountStatus;
    }

    public boolean isLost(){
        if(!Strings.isNullOrEmpty(leadAccountStatus) && leadAccountStatus.toUpperCase().equals("LOST"))
            return true;

        Quote lastQuote = getLastQuote();
        if(lastQuote == null)
            return false;

        return lastQuote.isLost();
    }
}
