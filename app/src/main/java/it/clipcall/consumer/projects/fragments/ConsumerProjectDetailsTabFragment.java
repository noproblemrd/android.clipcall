package it.clipcall.consumer.projects.fragments;

import it.clipcall.consumer.projects.models.ProfessionalData;
import it.clipcall.consumer.projects.views.IProjectAdvertiserView;
import it.clipcall.infrastructure.fragments.BaseFragment;


public abstract class ConsumerProjectDetailsTabFragment extends BaseFragment {

   IProjectAdvertiserView parentView;

   public void setParentView(IProjectAdvertiserView parentView) {
      this.parentView = parentView;
   }

   public abstract void setProjectDetails(ProfessionalData professionalData);
}
