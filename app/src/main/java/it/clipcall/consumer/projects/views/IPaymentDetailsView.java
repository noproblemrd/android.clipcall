package it.clipcall.consumer.projects.views;

import it.clipcall.consumer.projects.models.CustomerPaymentMethod;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.leads.models.Quote;

/**
 * Created by dorona on 05/01/2016.
 */
public interface IPaymentDetailsView extends IView {


    void showQuote(Quote quote, ProfessionalRef professional, CustomerPaymentMethod lastPaymentMethod);

    void setProcessingPaymentRequest(boolean isProcessing);

    void showFailureMessage(String message);
}
