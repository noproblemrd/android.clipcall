package it.clipcall.consumer.projects.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.kennyc.view.MultiStateView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.simple.eventbus.Subscriber;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.projects.models.ProfessionalData;
import it.clipcall.consumer.projects.presenters.ProjectAdvertiserAboutPresenter;
import it.clipcall.consumer.projects.views.IProjectAdvertiserAboutView;
import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import xyz.hanks.library.SmallBang;

@EFragment(R.layout.project_advertiser_about_fragment)
public class ProjectAdvertiserAboutFragment extends ConsumerProjectDetailsTabFragment implements IProjectAdvertiserAboutView {

    @FragmentArg
    String projectId;

    @FragmentArg
    String advertiserId;

    @ViewById
    TextView addressTextView;

    @ViewById
    TextView categoriesTextView;

    @ViewById
    TextView businessDescriptionTextView;

    @ViewById
    ImageView yourHeartImageView;

    @ViewById
    TextView yourHeartCountTextView;

    @ViewById
    TextView totalHeartsTextView;

    @ViewById
    ImageView chatImageView;

    @ViewById
    ImageView callProImageView;

    @ViewById
    ImageView remoteHouseCallImageView;

    @ViewById
    ProgressBar actionsProgressBar;

    @ViewById
    ViewGroup actionsContainer;

    @ViewById
    com.kennyc.view.MultiStateView multiStateView;

    @ViewById
    TextView badgeTextView;

    SmallBang smallBang;

    @ViewById
    ViewGroup requestQuoteContainer;

    @ViewById
    TextView requestQuoteTextView;

    @ViewById
    ProgressBar requestQuoteProgressBar;

    @ViewById
    ViewGroup aboutContainer;

    @Inject
    ProjectAdvertiserAboutPresenter presenter;


    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.shutDown();
    }

    @AfterViews
    public void afterViews(){
        this.presenter.bindView(this);
        this.smallBang = SmallBang.attach2Window(getActivity());
        initialize();
    }

    @Background
    void initialize(){
        presenter.setProjectId(projectId);
        presenter.setAdvertiserId(advertiserId);
        presenter.initialize();
    }

    @Click(R.id.yourHeartImageView)
    void onHeartTapped(){
        smallBang.bang(yourHeartImageView);
        likeAdvertiser();
    }

    @Click(R.id.requestQuoteTextView)
    void onRequestQuoteTapped(){
        presenter.requestQuote();
    }

    @Click(R.id.remoteHouseCallImageView)
    void onStartRemoteHouseCallTapped(){
        remoteHouseCallImageView.setEnabled(false);
        startRemoteHouseCall();
    }

    @Click(R.id.chatImageView)
    void onStartChatTapped(){
        startChat();
    }

    @Background
    void startChat() {
        presenter.navigateToChat();
    }

    @Click(R.id.callProImageView)
    void callProTapped(){
        callProImageView.setEnabled(false);
        callAdvertiser();
    }

    @Background
    void callAdvertiser(){
        presenter.callAdvertiser();
    }

    @Background
    void likeAdvertiser(){
        presenter.likeAdvertiser();
    }

    @Background
    void startRemoteHouseCall() {
        presenter.startRemoteHouseCall();
    }

    @Subscriber
    void onLiked(Boolean successfully){
        if(successfully){

        }
    }

    @Subscriber
    void onCalledAdvertiser(Boolean successfully){
        enableCallAdvertiser();
    }

    @UiThread
    void enableCallAdvertiser(){
        callProImageView.setEnabled(true);
    }


    @Click(R.id.meetingTextView)
    void addMeetingToCalendarTapped(){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("content://com.android.calendar/time"));
        startActivity(i);
    }

    @Click(R.id.payTheProTextView)
    void onPayTheProTapped(){
        presenter.payThePro();
    }


    @Override
    public RoutingContext getRoutingContext() {
      return new RoutingContext(getActivity());
    }


    @Override
    public void setAddress(String address) {
        addressTextView.setText(address);
    }

    @Override
    public void setBusinessDescription(String businessDescription) {
        businessDescriptionTextView.setText(businessDescription);
    }


    @Override
    public void setMyLoveCount(int myLoveCount) {
        yourHeartCountTextView.setText(myLoveCount+"");
        totalHeartsTextView.setText(String.format("You've sent %d hearts!", myLoveCount));
    }

    @Override
    public void setCategories(String formattedCategories) {
        categoriesTextView.setText(formattedCategories);
    }

    @Override
    public void startRemoteCallFailed() {
        enableActions(true);
    }

    @UiThread
    @Override
    public void setChatActionEnabled(boolean enabled) {
        chatImageView.setEnabled(enabled);
    }

    @Override
    public void showCannotStartVideoChat() {

    }

    @Override
    public void showProfessionalData() {
        multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
    }


    @Override
    public void showTotalUnreadMessages(int totalUnreadMessages) {
        if(totalUnreadMessages <= 0){
            badgeTextView.setVisibility(View.GONE);
        }else{
            badgeTextView.setVisibility(View.VISIBLE);
            badgeTextView.setText(""+totalUnreadMessages);
            YoYo.with(Techniques.Bounce)
                    .duration(700)
                    .playOn(badgeTextView);
        }
    }

    @Override
    public void notifyCallingProfessional() {
        if(parentView != null)
            parentView.notifyCallingProfessional();
    }

    @Override
    public void showRequestQuoteInProgress(boolean isInProgress) {
        if(isInProgress){
            requestQuoteTextView.setVisibility(View.INVISIBLE);
            requestQuoteProgressBar.setVisibility(View.VISIBLE);
        }else{
            requestQuoteProgressBar.setVisibility(View.GONE);
            requestQuoteTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showQuoteRequestWasSuccessful(boolean wasSuccessful) {
        if(wasSuccessful){
            requestQuoteTextView.setText("Quote Request sent");
            requestQuoteTextView.setOnClickListener(null);
            requestQuoteTextView.setTextColor(Color.BLACK);
            requestQuoteContainer.setVisibility(View.GONE);
        }else{
            Snackbar.make(aboutContainer,"Failed requesting quote. Please try again later",Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void showRequestQuote(boolean canRequestQuote) {
        if(canRequestQuote){
            requestQuoteContainer.setVisibility(View.VISIBLE);
        }else{
            requestQuoteContainer.setVisibility(View.GONE);
        }
    }


    @Background
    void connectToVideoChatInBackground(VideoSessionData videoSessionData){

    }

    @Click(R.id.addressTextView)
    void addressTapped(){
        Intent geoIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q="
                +addressTextView.getText().toString()));
        startActivity(geoIntent);
    }




    @UiThread
    public void enableActions(boolean enable){
      /*  callProImageView.setEnabled(enable);
        remoteHouseCallImageView.setEnabled(enable);
        chatImageView.setEnabled(enable);*/
        if(enable){
            showActions();
        }else{
            hideActions();
        }
    }

    @Override
    public void setProjectDetails(ProfessionalData professionalData) {
        if(presenter != null){
            presenter.setProfessionalData(professionalData);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("advertiserId",advertiserId);
        outState.putString("projectId",projectId);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null){
            this.projectId = savedInstanceState.getString("projectId");
            this.advertiserId = savedInstanceState.getString("advertiserId");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        enableActions(true);
        presenter.initialize();
        presenter.loadUnreadMessages();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.shutDown();
    }

    private void showActions(){
        actionsContainer.setVisibility(View.VISIBLE);
        actionsProgressBar.setVisibility(View.INVISIBLE);
        remoteHouseCallImageView.setEnabled(true);
        chatImageView.setEnabled(true);
        callProImageView.setEnabled(true);
    }

    private void hideActions(){
        actionsContainer.setVisibility(View.INVISIBLE);
        actionsProgressBar.setVisibility(View.VISIBLE);
        remoteHouseCallImageView.setEnabled(false);
        chatImageView.setEnabled(false);
        callProImageView.setEnabled(false);
    }
}