package it.clipcall.consumer.projects.controllers;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.consumer.projects.services.ProjectsManager;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by dorona on 20/01/2016.
 */
@Singleton
public class CustomerProjectDetailsController {

    private final ProjectsManager projectsManager;
    private final AuthenticationManager authenticationManager;
    private final ChatManager chatManager;

    @Inject
    public CustomerProjectDetailsController(ProjectsManager projectsManager, AuthenticationManager authenticationManager, ChatManager chatManager) {
        this.projectsManager = projectsManager;
        this.authenticationManager = authenticationManager;
        this.chatManager = chatManager;
    }


    public List<ProfessionalRef> getProjectAdvertisers(ProjectEntity project) {
        String customerId = authenticationManager.getCustomerId();
        List<ProfessionalRef> advertisers = projectsManager.getProjectAdvertisers(customerId, project);
        return advertisers;
    }

    public ProjectEntityBase getProjectDetails(String projectId) {
        ProjectEntityBase project = projectsManager.getById(projectId);
        if(project == null)
            return null;

        String userId = authenticationManager.getUserId();
        if(chatManager.isConnected(userId)) {
            project.updateUnreadMessageCount(chatManager);
            project.updateLastUnreadMessage(chatManager, userId);
        }

        return project;
    }

    public boolean requestForQuote(String id, String leadAccountId) {
        //TODO implement
        return true;
    }
}
