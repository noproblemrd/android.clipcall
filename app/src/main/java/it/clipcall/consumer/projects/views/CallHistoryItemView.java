package it.clipcall.consumer.projects.views;

import android.content.Context;
import android.media.MediaPlayer;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import it.clipcall.R;
import it.clipcall.consumer.projects.models.CallHistoryItem;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.support.IHistoryItemListener;
import it.clipcall.infrastructure.commands.PlayMediaFromUrlCommand;
import it.clipcall.infrastructure.support.buttons.AnimButton;
import it.clipcall.infrastructure.support.dates.DateFormatter;

@EViewGroup(R.layout.project_advertiser_call_history_item)
public class CallHistoryItemView extends HistoryItemView implements MediaPlayer.OnCompletionListener {

    @ViewById
    TextView recordingDateTextView;
    @ViewById
    AnimButton playAudioAnimButton;

    PlayMediaFromUrlCommand playMediaCommand;

    public CallHistoryItemView(Context context) {
        super(context);
    }

    @Override
    public void bind(HistoryItem historyItem, final IHistoryItemListener listener, boolean isProMode) {
        if(historyItem == null)
            return;


        final CallHistoryItem item = (CallHistoryItem) historyItem;
        prepareAudio(item);
        String formattedDate = DateFormatter.toLocalDate(item.getDate());
        recordingDateTextView.setText(formattedDate);
        playAudioAnimButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //listener.onItemTapped(item);
                if(playMediaCommand == null)
                    return;

                playMediaCommand.execute();
                if(playMediaCommand.isPlaying())
                    playAudioAnimButton.goToState(AnimButton.SECOND_STATE);
                else
                    playAudioAnimButton.goToState(AnimButton.FIRST_STATE);
            }
        });
    }


    @Background
    void prepareAudio(CallHistoryItem item){
        playMediaCommand = new PlayMediaFromUrlCommand(item.getUrl());
        playMediaCommand.setOnCompletionListener(this);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        updatePlayerOnCompletion();
    }

    @UiThread
    void updatePlayerOnCompletion(){
        playAudioAnimButton.goToState(AnimButton.FIRST_STATE);
        if(playMediaCommand != null){
            playMediaCommand.reset();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if(playMediaCommand != null){
            playMediaCommand.shutDown();
        }
    }
}
