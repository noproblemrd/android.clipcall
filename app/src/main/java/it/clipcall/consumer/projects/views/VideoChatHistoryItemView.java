package it.clipcall.consumer.projects.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Strings;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import it.clipcall.R;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.models.VideoHistoryItemBase;
import it.clipcall.consumer.projects.support.IHistoryItemListener;
import it.clipcall.infrastructure.support.dates.DateFormatter;
import me.drakeet.materialdialog.MaterialDialog;

@EViewGroup(R.layout.video_chat_history_item)
public class VideoChatHistoryItemView extends HistoryItemView {
    @ViewById
    TextView recordingDateTextView;
    @ViewById
    TextView remoteServiceCallTextView;

    @ViewById
    ImageView playVideoImageView;


    @ViewById
    ImageButton shareVideoImageButton;


    @ViewById
    ViewGroup shareVideoContainer;

    public VideoChatHistoryItemView(Context context) {
        super(context);
    }

    private int getColorByAttribute(int attributeValue) {
        TypedValue typedValue = new TypedValue();

        TypedArray a = getContext().obtainStyledAttributes(typedValue.data, new int[] { attributeValue});
        int color = a.getColor(0, 0);

        a.recycle();

        return color;
    }

    @Override
    public void bind(HistoryItem historyItem, final IHistoryItemListener listener, final boolean isProMode) {
        if (historyItem == null)
            return;

        final VideoHistoryItemBase item = (VideoHistoryItemBase) historyItem;
        String formattedDate = DateFormatter.toLocalDate(item.getDate());
        recordingDateTextView.setText(formattedDate);


        if(item.isProjectVideo()){
            remoteServiceCallTextView.setText("Recorded video");
        }


        if (!Strings.isNullOrEmpty(item.getUrl())) {

            if(isProMode){
                shareVideoContainer.setVisibility(VISIBLE);
            }else{
                shareVideoContainer.setVisibility(INVISIBLE);
            }

            playVideoImageView.setVisibility(VISIBLE);
            playVideoImageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemTapped(item);
                }
            });

            shareVideoImageButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    final MaterialDialog materialDialog = new MaterialDialog(VideoChatHistoryItemView.this.getContext());
                    materialDialog.setTitle("Share video")
                            .setMessage("Share video with other pros")
                            .setPositiveButton("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    materialDialog.dismiss();
                                    listener.onShareVideoTapped(item);

                                }
                            })
                            .setNegativeButton("CANCEL", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    materialDialog.dismiss();
                                }
                            });

                    materialDialog.show();
                }
            });
        } else {
            playVideoImageView.setVisibility(INVISIBLE);
            shareVideoContainer.setVisibility(INVISIBLE);
        }
    }



    void onShareVideoTapped() {

    }
}
