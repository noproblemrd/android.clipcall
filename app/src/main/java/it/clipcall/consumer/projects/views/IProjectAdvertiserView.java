package it.clipcall.consumer.projects.views;

import it.clipcall.consumer.projects.models.ProfessionalData;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 20/01/2016.
 */
public interface IProjectAdvertiserView extends IView {
    void showProfessionalDetails(ProfessionalData professionalData);

    void setTotalHeartCount(Integer allLove);

    void notifyCallingProfessional();
}
