package it.clipcall.consumer.projects.views;

import java.util.List;

import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by micro on 1/20/2016.
 */
public interface IProjectAdvertiserHistoryView extends IView {
    void showHistoryItems(List<HistoryItem> historyItems);
}
