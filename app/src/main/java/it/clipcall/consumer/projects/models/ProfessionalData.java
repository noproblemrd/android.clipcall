package it.clipcall.consumer.projects.models;

import java.util.List;

/**
 * Created by dorona on 20/01/2016.
 */
public class ProfessionalData {

    private AdvertiserData data;
    private ProjectEntity project;
    private Advertiser advertiser;

    public void setHistoryItems(List<HistoryItem> historyItems) {
        this.historyItems = historyItems;
    }

    private List<HistoryItem> historyItems;

    public List<CallHistoryItem> getCallHistoryItems() {
        return callHistoryItems;
    }

    public void setCallHistoryItems(List<CallHistoryItem> callHistoryItems) {
        this.callHistoryItems = callHistoryItems;
    }

    public AdvertiserData getData() {
        return data;
    }

    public void setData(AdvertiserData data) {
        this.data = data;
    }

    private List<CallHistoryItem> callHistoryItems;

    public void setProject(ProjectEntity project) {
        this.project = project;
    }

    public ProjectEntity getProject() {
        return project;
    }

    public void setAdvertiser(Advertiser advertiser) {
        this.advertiser = advertiser;
    }

    public Advertiser getAdvertiser() {
        return advertiser;
    }

    public List<HistoryItem> getHistoryItems() {
        return historyItems;
    }
}
