package it.clipcall.consumer.projects.models;

import java.util.List;

public class ProjectAdvertiser {

    private String projectId;

    private List<Advertiser> advertisers;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public List<Advertiser> getAdvertisers() {
        return advertisers;
    }

    public void setAdvertisers(List<Advertiser> advertisers) {
        this.advertisers = advertisers;
    }
}