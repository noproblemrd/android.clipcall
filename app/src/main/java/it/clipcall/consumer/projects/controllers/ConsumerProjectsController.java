package it.clipcall.consumer.projects.controllers;


import com.google.common.base.Strings;
import com.google.common.collect.Ordering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.projects.services.ProjectsManager;
import it.clipcall.infrastructure.gcm.services.GcmTokenProvider;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class ConsumerProjectsController {


    private final AuthenticationManager authenticationManager;
    private final ProjectsManager projectsManager;
    private final ChatManager chatManager;
    private final GcmTokenProvider gcmTokenProvider;

    @Inject
    public ConsumerProjectsController(AuthenticationManager authenticationManager, ProjectsManager projectsManager, ChatManager chatManager, GcmTokenProvider gcmTokenProvider) {
        this.authenticationManager = authenticationManager;
        this.projectsManager = projectsManager;
        this.chatManager = chatManager;
        this.gcmTokenProvider = gcmTokenProvider;
    }

    public List<ProjectEntityBase> getCustomerProjects(boolean forceReload) {
        if(!authenticationManager.isCustomerAuthenticated())
            return new ArrayList<>();


        String customerId = authenticationManager.getCustomerId();
        if(Strings.isNullOrEmpty(customerId))
            return new ArrayList<>();


        List<ProjectEntityBase> projects = projectsManager.getAll(customerId, forceReload);
        if(chatManager.isConnected(customerId)){
            for(ProjectEntityBase project: projects){
                project.updateUnreadMessageCount(chatManager);
                project.updateLastUnreadMessage(chatManager, customerId);
            }
        }

        Collections.sort(projects, new Ordering<ProjectEntityBase>() {
            @Override
            public int compare(ProjectEntityBase left, ProjectEntityBase right) {
                return right.getLastUpdateDate().compareTo(left.getLastUpdateDate());
            }
        });

        return projects;
    }
}
