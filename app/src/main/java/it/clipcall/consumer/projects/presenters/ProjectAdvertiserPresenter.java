package it.clipcall.consumer.projects.presenters;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import javax.inject.Inject;

import it.clipcall.consumer.projects.controllers.ProjectAdvertiserController;
import it.clipcall.consumer.projects.models.Advertiser;
import it.clipcall.consumer.projects.models.ProfessionalData;
import it.clipcall.consumer.projects.views.IProjectAdvertiserView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;

@PerActivity
public class ProjectAdvertiserPresenter extends PresenterBase<IProjectAdvertiserView> {

    private final ProjectAdvertiserController controller;

    @Inject
    public ProjectAdvertiserPresenter(ProjectAdvertiserController controller) {
        this.controller = controller;
    }

    @Override
    public void initialize() {
        super.initialize();
        EventBus.getDefault().register(this);
    }

    @Override
    public void shutDown() {
        super.shutDown();
        EventBus.getDefault().unregister(this);
    }

    public void getAdvertiserData(String projectId, String advertiserId){
        ProfessionalData professionalData = this.controller.getAdvertiserInfo(projectId, advertiserId);
        if(view == null)
            return;

        view.showProfessionalDetails(professionalData);
    }

    @Subscriber
    void onHeartCountChanaged(Advertiser advertiser){
        view.setTotalHeartCount(advertiser.getAllLove());
    }
}