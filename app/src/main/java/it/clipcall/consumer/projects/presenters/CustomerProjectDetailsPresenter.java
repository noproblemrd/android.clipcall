package it.clipcall.consumer.projects.presenters;

import android.os.Bundle;

import com.layer.sdk.changes.LayerChangeEvent;

import org.parceler.Parcels;
import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.common.chat.services.tasks.NavigateToChatTask;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.common.projects.models.ProjectVideoData;
import it.clipcall.consumer.payment.services.tasks.RequestQuoteTask;
import it.clipcall.consumer.projects.controllers.CustomerProjectDetailsController;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.consumer.projects.views.IProjectDetailsView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.professional.leads.models.Quote;

@PerActivity
public class CustomerProjectDetailsPresenter extends PresenterBase<IProjectDetailsView>{

    private final CustomerProjectDetailsController controller;
    private final RoutingService routingService;
    private final RouteParams routeParams;

    private final RequestQuoteTask requestQuoteTask;
    private final NavigateToChatTask navigateToChatTask;

    private final EventAggregator eventAggregator;

    private ProjectEntityBase project = null;

    @Inject
    public CustomerProjectDetailsPresenter(CustomerProjectDetailsController controller, RoutingService routingService, RouteParams routeParams, RequestQuoteTask requestQuoteTask, NavigateToChatTask navigateToChatTask, EventAggregator eventAggregator) {

        this.controller = controller;
        this.routingService = routingService;
        this.routeParams = routeParams;
        this.requestQuoteTask = requestQuoteTask;
        this.navigateToChatTask = navigateToChatTask;
        this.project = routeParams.getParam("project");
        this.eventAggregator = eventAggregator;
    }

    @Override
    public void initialize() {
        EventBus.getDefault().register(this);
        view.showProjectDetails(project);
    }


    @Override
    public void shutDown() {
        super.shutDown();
        EventBus.getDefault().unregister(this);
    }

    public void showAdvertiser(ProfessionalRef advertiser, Bundle bundle) {

        NavigationContext context = new NavigationContext();
        if(view != null){
            context.routingContext = view.getRoutingContext();
            context.addParameter("projectId",this.project.getId());
            context.addParameter("advertiserId",advertiser.getSupplierId());

            context.bundle = new Bundle();
            context.bundle.putParcelable("context", Parcels.wrap(advertiser.getAdvertiser()));
            context.uiBundle = bundle;
            routingService.routeTo(Routes.ConsumerAdvertiserRoute, context);
        }

    }

    public void executeAdvertiserAction(ProfessionalRef advertiser) {
        view.onAdvertiserActionExecuted(advertiser);

    }

    public void playVideo() {
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        ProjectVideoData projectVideoData = new ProjectVideoData();
        projectVideoData.zipCode = project.getAddressOrZipCode();
        projectVideoData.category = project.getCategoryName();
        projectVideoData.url = project.getVideoUrl();
        projectVideoData.createDate = project.getCreatedOn();

        navigationContext.bundle = new Bundle();
        navigationContext.bundle.putParcelable("context", Parcels.wrap(projectVideoData));
        navigationContext.addParameter("projectVideoUrl",project.getVideoUrl());
        routingService.routeTo(Routes.ProjectVideoRoute, navigationContext);
    }


    public void loadProjectProfessionals() {
        if(view != null)
            view.showLoadingIndicator();
        ProjectEntityBase project = controller.getProjectDetails(this.project.getId());
        if(project == null)
            return;

        ProjectEntity projectEntity = (ProjectEntity)project;
        List<ProfessionalRef> advertisers = controller.getProjectAdvertisers(projectEntity);
        if(view != null){
            view.setProjectProfessionals(advertisers);
            view.hideLoadingIndicator();
        }

    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onChatEvent(LayerChangeEvent event){
        ProjectEntityBase project = controller.getProjectDetails(this.project.getId());
        if(project == null)
            return;

        ProjectEntity entity = (ProjectEntity)project;
        List<ProfessionalRef> advertisers = controller.getProjectAdvertisers(entity);
        view.setProjectProfessionals(advertisers);
    }


    public void routeToHome() {
        routingService.routeToHome(view.getRoutingContext());
    }

    public void requestForQuote(ProfessionalRef professional) {
        eventAggregator.publish(new ApplicationEvent(ConsumerEventNames.ProjectAskQuote ,professional));
        boolean successful = requestQuoteTask.execute((ProjectEntity)project, professional);
        if(successful){
            try{
                Thread.sleep(1000);
            }
            catch (Exception e){

            }
            navigateToChatTask.execute((ProjectEntity)project,professional,view.getRoutingContext());
        }
    }

    public void bookQuote(ProfessionalRef professional) {
        Quote lastQuote = professional.getLastQuote();
        if(lastQuote == null)
            return;

        eventAggregator.publish(new ApplicationEvent(ConsumerEventNames.ProjectBookQuote ,lastQuote));

        routeParams.setParam("quote",lastQuote);
        routeParams.setParam("project",project);
        routeParams.setParam("leadAccountId",professional.getLeadAccountId());
        NavigationContext navContext = new NavigationContext(view.getRoutingContext());
        routingService.routeTo(Routes.ConsumerQuoteDetailsRoute,navContext);
    }
}
