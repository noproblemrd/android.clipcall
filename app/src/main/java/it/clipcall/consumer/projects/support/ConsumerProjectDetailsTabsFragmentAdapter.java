package it.clipcall.consumer.projects.support;

import android.support.v4.app.FragmentManager;

import it.clipcall.consumer.projects.fragments.ConsumerProjectDetailsTabFragment;
import it.clipcall.consumer.projects.models.ProfessionalData;
import it.clipcall.consumer.projects.views.IProjectAdvertiserView;
import it.clipcall.infrastructure.support.fragments.AbstractFragmentAdapter;
import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.leads.views.IProfessionalProjectDetailsView;

/**
 * Created by dorona on 29/02/2016.
 */
public class ConsumerProjectDetailsTabsFragmentAdapter extends AbstractFragmentAdapter<ConsumerProjectDetailsTabFragment> {
    public ConsumerProjectDetailsTabsFragmentAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public void setData(Object data) {
        ProfessionalData projectDetails = (ProfessionalData) data;
        for (int i = 0; i < getCount(); i++) {
            ConsumerProjectDetailsTabFragment fragment =  getItem(i);
            fragment.setProjectDetails(projectDetails);
        }

    }

    @Override
    public <T extends IView> void setParentView(T view) {
        if(view instanceof IProfessionalProjectDetailsView){
            IProjectAdvertiserView proProjectDetailsView = (IProjectAdvertiserView) view;
            setParentView(view);

        }
    }


    public void setParentView(IProjectAdvertiserView view){
        for (int i = 0; i < getCount(); i++) {
            ConsumerProjectDetailsTabFragment fragment =  getItem(i);
            fragment.setParentView(view);
        }
    }
}
