package it.clipcall.consumer.projects.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.mainMenu.support.IBadgeListener;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.consumer.projects.presenters.CustomerProjectsPresenter;
import it.clipcall.consumer.projects.views.IProjectsView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.routing.models.FragmentRoutingContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.support.recyclerviews.ActionInvokerListener;
import it.clipcall.infrastructure.viewModel.ItemsWidget;
import it.clipcall.infrastructure.viewModel.support.recycleviews.ItemSelectedListener;
import it.clipcall.infrastructure.viewModel.support.recycleviews.ItemsLoader;
import it.clipcall.infrastructure.viewModel.support.recycleviews.RecyclerViewAdapterBase;
import it.clipcall.professional.leads.views.ProfessionalProjectsItemView;
import it.clipcall.professional.leads.views.ProfessionalProjectsItemView_;

@EFragment(R.layout.consumer_projects_fragment)
public class ConsumerProjectsFragment extends BaseFragment implements IProjectsView,
                                                                      ItemsLoader<ProjectEntityBase>,
                                                                      ItemSelectedListener<ProjectEntityBase>,
                                                                      ActionInvokerListener<ProjectEntityBase>,
                                                                      RecyclerViewAdapterBase.IViewBuilder<ProjectEntityBase>{


    @ViewById
    ItemsWidget<ProjectEntityBase> projectsItemsWidget;

    @Inject
    CustomerProjectsPresenter presenter;


    @Background
    void getCustomerProjects(){
        presenter.getCustomerProjects(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.shutDown();
    }

    @AfterViews
    public void afterViews(){
        presenter.bindView(this);
        projectsItemsWidget.setItemSelectedListener(this);
        projectsItemsWidget.setItemsLoader(this);
        projectsItemsWidget.setActionInvokerListener(this);
        projectsItemsWidget.setViewBuilder(this);
        projectsItemsWidget.setEmptyStateView(R.layout.empty_projects_view);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.initialize();
        getCustomerProjects();
    }

    @Override
    public RoutingContext getRoutingContext() {
        Activity activity = getActivity();
        if(!(activity instanceof BaseActivity))
            return null;
        RoutingContext routingContext = ((BaseActivity) activity).getRoutingContext();
        return routingContext;
    }

    @Override
    public void setProjects(List<ProjectEntityBase> projects) {
        projectsItemsWidget.setItems(projects);
    }

    @Override
    public void setProjectsAfterInvalidating(final List<ProjectEntityBase> projects) {
        projectsItemsWidget.invalidateItems();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                projectsItemsWidget.setItems(projects);
            }
        }, 200);
    }

    @UiThread
    @Override
    public void onProjectActionExecuted(ProjectEntityBase project) {
        Toast.makeText(getActivity(),"calling the customer",Toast.LENGTH_LONG).show();
    }

    @Override
    public RoutingContext getFragmentRoutingContext() {
        RoutingContext routingContext = new FragmentRoutingContext(getFragmentManager(), R.id.menu_fragment_container, getActivity());
        return routingContext;
    }

    @UiThread
    @Override
    public void showTotalUnreadMessages(int totalUnreadMessages) {
        if(getContext() instanceof IBadgeListener){
            IBadgeListener listener = (IBadgeListener) getContext();
            listener.showBadge(totalUnreadMessages);
        }
    }

    @UiThread
    @Override
    public void setFilteredProjects(List<ProjectEntityBase> projects) {

    }


    @Override
    protected String getTitle() {
        return "My Projects";
    }

    @Override
    protected int getMenuItemId() {
        return R.id.nav_my_projects_consumer;
    }

    @Click(R.id.createNewProjecFab)
    void onCreateNewProjectTapped(){
        presenter.createNewProjectTapped();
    }

    @Override
    public void loadItems() {
        getCustomerProjects();
    }

    @Override
    public void onItemSelected(ProjectEntityBase item) {

    }

    @Override
    public void onItemSelected(ProjectEntityBase item, Bundle data, Bundle uiData) {
        presenter.projectSelected((ProjectEntity)item);
    }

    @Override
    public void onItemSelected(ProjectEntityBase item, Bundle uiData) {
        presenter.projectSelected((ProjectEntity)item);
    }

    @Override
    public void onInvokeAction(ProjectEntityBase item) {
        presenter.playVideo(item);
    }

    @Override
    public ProfessionalProjectsItemView build(int viewType) {
        return ProfessionalProjectsItemView_.build(getActivity());
    }
}
