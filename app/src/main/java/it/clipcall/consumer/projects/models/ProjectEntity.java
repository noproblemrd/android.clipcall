package it.clipcall.consumer.projects.models;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.infrastructure.support.collections.IAction;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.professional.leads.models.Quote;

public class ProjectEntity extends ProjectEntityBase implements Serializable {

    String leadId;
    Integer suppliersCount;
    String expertiseName;
    String supplierAvailabilityStatus;
    String availableAction;
    Boolean canReviveIncident;

    List<ProfessionalRef> leadAccountData;


    public int getTotalProfessionals(){
        if(Lists.isNullOrEmpty(leadAccountData))
            return 0;

        return leadAccountData.size();
    }

    @Override
    public boolean isExclusive() {
        return super.isExclusive() && !Lists.isNullOrEmpty(leadAccountData);
    }

    @Override
    public String getId() {
        return leadId;
    }

    @Override
    public String getCategoryName() {
        return getExpertiseName();
    }

    @Override
    public String getProfileUrl() {
        return "";
    }

    @Override
    public String getName() {
        return getSupplierName();
    }

    public Integer getSuppliersCount() {
        return suppliersCount;
    }

    public void setSuppliersCount(Integer suppliersCount) {
        this.suppliersCount = suppliersCount;
    }

    public String getExpertiseName() {
        return expertiseName;
    }

    public void setExpertiseName(String expertiseName) {
        this.expertiseName = expertiseName;
    }

    public String getSupplierAvailabilityStatus() {
        return supplierAvailabilityStatus;
    }

    public void setSupplierAvailabilityStatus(String supplierAvailabilityStatus) {
        this.supplierAvailabilityStatus = supplierAvailabilityStatus;
    }

    public String getAvailableAction() {
        return availableAction;
    }

    public void setAvailableAction(String availableAction) {
        this.availableAction = availableAction;
    }

    public Boolean getCanReviveIncident() {
        return canReviveIncident;
    }

    public void setCanReviveIncident(Boolean canReviveIncident) {
        this.canReviveIncident = canReviveIncident;
    }

    public void setAdvertisers(List<Advertiser> advertisers){
        List<ProfessionalRef> professionals = getLeadAccountData();
        for (final ProfessionalRef pro : professionals){
            Advertiser advertiser = Lists.firstOrDefault(advertisers, new ICriteria<Advertiser>() {
                @Override
                public boolean isSatisfiedBy(Advertiser candidate) {
                    return candidate.getId().equals(pro.getSupplierId());
                }
            });
            if(advertiser != null)
                pro.setAdvertiser(advertiser);
        }
    }

    public List<ProfessionalRef> getAdvertisers(){
        return getLeadAccountData();
    }

    public void setAdvertiserCallHistory(String advertiserId, List<CallHistoryItem> callHistoryItems) {
        Advertiser advertiser =  getAdvertiserById(advertiserId);
        if(advertiser == null)
            return;

        advertiser.setCallHistoryItems(callHistoryItems);
    }

    public void setAdvertiserHistoryItems(String advertiserId, List<HistoryItem> historyItems) {

        Advertiser advertiser =  getAdvertiserById(advertiserId);
        if(advertiser == null)
            return;

        advertiser.setHistoryItems(historyItems);

    }

    public Advertiser getAdvertiserById(String advertiserId){
        for(ProfessionalRef pro: getLeadAccountData())
            if(pro.getSupplierId().equals(advertiserId))
                return pro.getAdvertiser();

        return null;
    }

    public ProfessionalRef getProfessionalById(String advertiserId){
        for(ProfessionalRef pro: getLeadAccountData())
            if(pro.getSupplierId().equals(advertiserId))
                return pro;

        return null;
    }

    public void setAdvertiserData(String advertiserId, AdvertiserData advertiserData) {
        Advertiser advertiser = getAdvertiserById(advertiserId);
        if(advertiser == null)
            return;

        advertiser.setAdvertiserData(advertiserData);
    }

    public AdvertiserData getAdvertiserData(String advertiserId) {
        Advertiser advertiser = getAdvertiserById(advertiserId);
        if(advertiser == null)
            return null;

        AdvertiserData advertiserData =  advertiser.getAdvertiserData();
        return advertiserData;
    }
    public String getSupplierName() {
        if(Lists.isNullOrEmpty(leadAccountData))
            return "";

        return leadAccountData.get(0).getSupplierName();
    }

    @Override
    public String toString() {
        return this.getVideoUrl();
    }

    @Override
    public Quote getLastQuote() {
        return null;
    }

    @Override
    public Quote getLeadAccountLastQuote(final String leadAccountId) {
        ProfessionalRef professional = Lists.firstOrDefault(leadAccountData, new ICriteria<ProfessionalRef>() {
            @Override
            public boolean isSatisfiedBy(ProfessionalRef candidate) {
                return candidate.getLeadAccountId().equals(leadAccountId);
            }
        });

        if(professional == null)
            return null;

        return professional.getLastQuote();
    }

    @Override
    public boolean isBooked() {
        if( getStatus().getStatus().toUpperCase().equals("BOOKED"))
            return true;

        boolean isBooked = Lists.any(getAdvertisers(), new ICriteria<ProfessionalRef>() {
            @Override
            public boolean isSatisfiedBy(ProfessionalRef candidate) {
                return candidate.isBooked();
            }
        });
        return isBooked;
    }

    public Advertiser getProfessional(final String advertiserId) {
        ProfessionalRef professional = Lists.firstOrDefault(getLeadAccountData(), new ICriteria<ProfessionalRef>() {
            @Override
            public boolean isSatisfiedBy(ProfessionalRef candidate) {
                return candidate.getSupplierId().equals(advertiserId);
            }
        });
        return professional.getAdvertiser();
    }

    public List<ProfessionalRef> getLeadAccountData() {
        return leadAccountData;
    }

    public void setLeadAccountData(List<ProfessionalRef> leadAccountData) {
        this.leadAccountData = leadAccountData;
    }

    @Override
    public void updateUnreadMessageCount(ChatManager chatManager) {

        int totalUnreadMessages = 0;
        List<ProfessionalRef> leadAccountData = getLeadAccountData();
        if(Lists.isNullOrEmpty(leadAccountData))
            return;

        for(ProfessionalRef proRef: leadAccountData){
            proRef.updateTotalUnreadMessages(chatManager);
            totalUnreadMessages += proRef.getTotalUnreadMessages();
        }

        setTotalUnreadMessages(totalUnreadMessages);
    }

    @Override
    public void updateLastUnreadMessage(ChatManager chatManager, String participantId) {
        List<ProfessionalRef> leadAccountData = getLeadAccountData();
        if(Lists.isNullOrEmpty(leadAccountData))
            return;



        for(ProfessionalRef proRef: leadAccountData){
            proRef.updateLastUnreadMessageDate(chatManager, participantId);
        }

        for(ProfessionalRef proRef: leadAccountData){
            Date currentLastUnreadMessageDate = getLastUnreadMessageDate();
            Date lastUnreadMessageDate = proRef.getLastUnreadMessageDate();
            if(currentLastUnreadMessageDate == null && lastUnreadMessageDate != null){
                setLastUnreadMessageDate(lastUnreadMessageDate);
            }else if(currentLastUnreadMessageDate != null && lastUnreadMessageDate != null && currentLastUnreadMessageDate.compareTo(lastUnreadMessageDate)>0){
                setLastUnreadMessageDate(lastUnreadMessageDate);
            }
        }
    }

    @Override
    public boolean isNotHired(){
        return false;
    }


    public void markOtherProsAsLost(final ProfessionalRef professional) {
        List<ProfessionalRef> otherProfessionals = Lists.where(getAdvertisers(), new ICriteria<ProfessionalRef>() {
            @Override
            public boolean isSatisfiedBy(ProfessionalRef candidate) {
                return !candidate.getSupplierId().equals(professional.getSupplierId());
            }
        });

        Lists.forEach(otherProfessionals, new IAction<ProfessionalRef>() {
            @Override
            public void invoke(ProfessionalRef input) {
                input.markAsLost();
            }
        });
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }
}