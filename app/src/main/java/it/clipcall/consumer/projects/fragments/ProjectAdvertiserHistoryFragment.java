package it.clipcall.consumer.projects.fragments;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.kennyc.view.MultiStateView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.projects.models.CallHistoryItem;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.models.PaymentHistoryItem;
import it.clipcall.consumer.projects.models.ProfessionalData;
import it.clipcall.consumer.projects.models.QuoteHistoryItem;
import it.clipcall.consumer.projects.models.VideoHistoryItemBase;
import it.clipcall.consumer.projects.presenters.ProjectAdvertiserHistoryPresenter;
import it.clipcall.consumer.projects.support.HistoryItemAdapter;
import it.clipcall.consumer.projects.support.IHistoryItemListener;
import it.clipcall.consumer.projects.views.IProjectAdvertiserHistoryView;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.support.collections.Lists;

@EFragment(R.layout.project_advertiser_history_fragment)
public class ProjectAdvertiserHistoryFragment extends ConsumerProjectDetailsTabFragment implements IProjectAdvertiserHistoryView, IHistoryItemListener {

    @FragmentArg
    String projectId;

    @FragmentArg
    String advertiserId;

    @ViewById
    SwipeRefreshLayout swipeRefreshLayout;

    SwipeRefreshLayout emptyRefreshLayout;

    @ViewById
    RecyclerView callHistoryRecycleView;

    @Inject
    ProjectAdvertiserHistoryPresenter presenter;

    HistoryItemAdapter historyItemAdapter;

    @ViewById
    com.kennyc.view.MultiStateView multiStateView;

    @AfterViews
    public void afterViews(){

        View emptyViewContainer = multiStateView.getView(MultiStateView.VIEW_STATE_EMPTY);
        emptyRefreshLayout = (SwipeRefreshLayout) emptyViewContainer.findViewById(R.id.emptyProjectsRefreshLayout);
        emptyRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadHistoryItems();
            }
        });

        presenter.bindView(this);

        historyItemAdapter = new HistoryItemAdapter(getActivity(),this, false);
        //callHistoryListView.setAdapter(adapter);
        callHistoryRecycleView.setAdapter(historyItemAdapter);
        callHistoryRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                loadHistoryItems();

            }
        });


        /*adapter.setCallHistoryItemListener(new ProjectAdvertiserCallHistoryArrayAdapter.CallHistoryItemListener() {
            @Override
            public void onItemTapped(HistoryItem item) {

                if(item instanceof  CallHistoryItem)
                {
                    CallHistoryItem historyItem = (CallHistoryItem) item;
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse(historyItem.getCallRecordUrl()), "audio*//*");
                    startActivity(intent);
                }
                else if(item instanceof PaymentHistoryItem){
                    presenter.navigateToInvoice((PaymentHistoryItem)item);
                }
            }
        });*/
    }


    @Background
    void loadHistoryItems(){
        presenter.loadHistoryItems();
    }

    @Override
    public void setProjectDetails(ProfessionalData professionalData) {
        if(presenter != null)
             presenter.setProfessionalData(professionalData);
    }


    @Override
    public RoutingContext getRoutingContext() {
        return new RoutingContext(getActivity());
    }

    @UiThread
    public void showHistoryItems(List<HistoryItem> historyItems) {

        if(Lists.isNullOrEmpty(historyItems))
            multiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
        else
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);

        historyItemAdapter.setItems(historyItems);
        swipeRefreshLayout.setRefreshing(false);
        emptyRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemTapped(CallHistoryItem item) {

    }

    @Override
    public void onShareVideoTapped(VideoHistoryItemBase videoChatHistoryItem) {

    }

    @Override
    public void onItemTapped(VideoHistoryItemBase videoChatHistoryItem) {

        presenter.showVideoChat(videoChatHistoryItem);
    }

    @Override
    public void onItemTapped(PaymentHistoryItem videoChatHistoryItem) {
        presenter.navigateToPaymentInvoice(videoChatHistoryItem);
    }

    @Override
    public void onItemTapped(QuoteHistoryItem quoteHistoryItem) {
        presenter.showQuote(quoteHistoryItem);
    }
}