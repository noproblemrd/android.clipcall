package it.clipcall.consumer.projects.views;

import java.util.List;

import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 05/01/2016.
 */
public interface IProjectDetailsView extends IView {
    void onAdvertiserActionExecuted(ProfessionalRef advertiser);

    void updateProjectDetails(ProjectEntity project);

    void showLoadingIndicator();

    void hideLoadingIndicator();

    void setProjectProfessionals(List<ProfessionalRef> professionals);

    void showProjectDetails(ProjectEntityBase project);

    void requestForQuoteWasSuccessful(ProfessionalRef professional);
}
