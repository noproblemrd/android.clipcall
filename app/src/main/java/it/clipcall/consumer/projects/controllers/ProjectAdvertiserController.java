package it.clipcall.consumer.projects.controllers;

import com.google.common.base.Strings;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.common.chat.models.IConversation;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.profile.models.CustomerProfile;
import it.clipcall.consumer.profile.services.CustomerProfileManager;
import it.clipcall.consumer.projects.models.Advertiser;
import it.clipcall.consumer.projects.models.AdvertiserData;
import it.clipcall.consumer.projects.models.CallHistoryItem;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.models.ProfessionalData;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.consumer.projects.services.ProjectsManager;
import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.consumer.vidoechat.services.VideoChatCoordinator;
import it.clipcall.consumer.vidoechat.services.VideoChatManager;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by dorona on 20/01/2016.
 */
@Singleton
public class ProjectAdvertiserController {

    private final CustomerProfileManager profileManager;
    private final ChatManager chatManager;
    private final ProjectsManager projectsManager;
    private final AuthenticationManager authenticationManager;
    private final VideoChatManager videoChatManager;
    private final VideoChatCoordinator videoChatCoordinator;

    @Inject
    public ProjectAdvertiserController(ProjectsManager projectsManager,
                                       AuthenticationManager authenticationManager,
                                       VideoChatManager videoChatManager,
                                       CustomerProfileManager profileManager,
                                       ChatManager chatManager,
                                       VideoChatCoordinator videoChatCoordinator) {
        this.projectsManager = projectsManager;
        this.authenticationManager = authenticationManager;
        this.videoChatManager = videoChatManager;
        this.profileManager = profileManager;
        this.chatManager = chatManager;
        this.videoChatCoordinator = videoChatCoordinator;
    }

    public List<ProfessionalRef> getProjectAdvertisers(ProjectEntity project) {
        String customerId = authenticationManager.getCustomerId();
        List<ProfessionalRef> advertisers = projectsManager.getProjectAdvertisers(customerId, project);
        return advertisers;
    }

    public ProfessionalData getAdvertiserInfo(String projectId, String advertiserId) {
        String customerId = authenticationManager.getCustomerId();
        ProjectEntityBase project = projectsManager.getById(projectId);
        if(project == null)
            return null;
        ProjectEntity entity = (ProjectEntity)project;
        Advertiser advertiser = entity.getAdvertiserById(advertiserId);
        AdvertiserData advertiserData = projectsManager.getAdvertiserData(customerId, projectId, advertiserId);
        List<CallHistoryItem> callHistory = projectsManager.getAdvertiserCallHistory(customerId, projectId, advertiserId);
        List<HistoryItem> historyItems = projectsManager.getAdvertiserHistoryItems(customerId, projectId, advertiserId);

        ProfessionalData professionalData = new ProfessionalData();
        professionalData.setProject(entity);
        advertiser.setHistoryItems(historyItems);
        professionalData.setAdvertiser(advertiser);
        professionalData.setCallHistoryItems(callHistory);
        professionalData.setHistoryItems(historyItems);
        professionalData.setData(advertiserData);
        return professionalData;
    }

    public boolean likeAdvertiser(String projectId, String advertiserId) {
        String customerId = authenticationManager.getCustomerId();
        boolean success = projectsManager.likeAdvertiser(customerId, projectId, advertiserId);
        return success;
    }

    public boolean callAdvertiser(String projectId, String advertiserId) {
        String customerId = authenticationManager.getCustomerId();
        boolean success = projectsManager.callAdvertiser(customerId, projectId, advertiserId);
        return success;
    }

    public Advertiser getAdvertiser(String projectId, String advertiserId) {
        ProjectEntityBase project = projectsManager.getById(projectId);
        if(project == null)
            return null;

        ProjectEntity entity = (ProjectEntity)project;
        Advertiser advertiser =  entity.getAdvertiserById(advertiserId);
        return advertiser;

    }

    public List<HistoryItem> getHistoryItems(ProfessionalData professionalData){
        String customerId =  authenticationManager.getUserId();
        List<HistoryItem> historyItems = projectsManager.getAdvertiserHistoryItems(customerId, professionalData.getProject().getId(), professionalData.getAdvertiser().getId());
        return historyItems;
    }

    public VideoSessionData createVideoSession(String projectId, String advertiserId) {
        String customerId = authenticationManager.getCustomerId();
        ProjectEntityBase project = projectsManager.getById(projectId);
        if(project == null)
            return null;

        ProjectEntity entity = (ProjectEntity)project;
        Advertiser advertiser = entity.getAdvertiserById(advertiserId);
        if(advertiser == null)
            return null;

        String leadAccountId = advertiser.getLeadAccountId();
        VideoSessionData videoSession = videoChatManager.createVideoSession(customerId, leadAccountId);
        return videoSession;
    }


    public CustomerProfile getConsumerProfile() {
        String customerId = authenticationManager.getCustomerId();
        CustomerProfile profile =  profileManager.getCustomerProfile(customerId);
        profile.setPhoneNumber(authenticationManager.getPhoneNumber());
        return profile;
    }

    public String getConsumerPhoneNumber() {
        return authenticationManager.getPhoneNumber();
    }

    public String getCustomerNameOrPhoneNumber(){
        CustomerProfile consumerProfile = getConsumerProfile();
        if(consumerProfile == null)
            return getConsumerPhoneNumber();

        String name = consumerProfile.getName();
        if(!Strings.isNullOrEmpty(name))
            return name;

        return getConsumerPhoneNumber();
    }

    public String getCustomerId() {
        return authenticationManager.getCustomerId();
    }


    public ProfessionalRef getProfessional(String projectId, String advertiserId) {
        ProjectEntityBase project = projectsManager.getById(projectId);
        if(project == null)
            return null;

        ProjectEntity entity = (ProjectEntity)project;
        ProfessionalRef advertiser =  entity.getProfessionalById(advertiserId);
        if(advertiser == null)
            return null;

        advertiser.updateTotalUnreadMessages(chatManager);
        return advertiser;
    }

    public void connectToVideoChatInBackground(VideoSessionData videoSessionData) {
        videoChatCoordinator.connectToSession(videoSessionData);
    }

    public void initializePublisher(String consumerNameOrPhoneNumber) {
        videoChatCoordinator.initializePublisher(consumerNameOrPhoneNumber);
    }

    public boolean requestQuote(String projectId, ProfessionalRef professional) {
        //TODO implement
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }

    public IConversation getConversationWithParticipant(ChatParticipant sender, ChatParticipant receiver, String leadAccountId) {
        IConversation conversation = chatManager.getConversationWithParticipant(sender, receiver, leadAccountId);
        return conversation;
    }

    public IConversation createConversation(ChatParticipant sender, ChatParticipant receiver, String leadAccountId) {
        IConversation conversation = chatManager.createConversation(sender, receiver, leadAccountId);
        return conversation;
    }

    public void sendMessage(ChatParticipant sender, ChatParticipant receiver, String leadAccountId, MessageDescription messageDescription) {
        chatManager.sendMessage(sender, receiver, leadAccountId, messageDescription);
    }
}
