package it.clipcall.consumer.projects.support;


import android.content.Context;
import android.view.ViewGroup;

import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.views.AdvertiserItemView;
import it.clipcall.consumer.projects.views.AdvertiserItemView_;
import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.infrastructure.ui.lists.RecyclerViewAdapterBase;
import it.clipcall.infrastructure.ui.lists.ViewWrapper;

public class ProjectAdvertisersArrayAdapter extends RecyclerViewAdapterBase<ProfessionalRef, AdvertiserItemView> {


    private final Context context;

    private IProjectProfessionalListener advertiserListener;

    public ProjectAdvertisersArrayAdapter(Context context, IProjectProfessionalListener listener) {
        this.context = context;
        this.advertiserListener = listener;
    }

    @Override
    protected AdvertiserItemView onCreateItemView(ViewGroup parent, int viewType) {
        AdvertiserItemView result = AdvertiserItemView_.build(context);
        return result;
    }

    @Override
    public void onBindViewHolder(ViewWrapper<AdvertiserItemView> holder, int position) {
        AdvertiserItemView view = holder.getView();
        ProfessionalRef professional = items.get(position);
        view.bind(professional, advertiserListener);
    }

    public void requestForQuoteWasSuccessful(final ProfessionalRef professional) {
        int index = getItemIndexByCriteria(new ICriteria<ProfessionalRef>() {
            @Override
            public boolean isSatisfiedBy(ProfessionalRef candidate) {
                return candidate.getLeadAccountId().equals(professional.getLeadAccountId());
            }
        });

        if(index != -1){
            notifyItemChanged(index);
        }
    }
}
