package it.clipcall.consumer.projects.views;

import it.clipcall.consumer.payment.models.PaymentDetails;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 05/01/2016.
 */
public interface IPaymentInvoiceView extends IView {

   void showInvoice(PaymentDetails paymentDetails);

   void showInProgress(boolean isInProgress);
}
