package it.clipcall.consumer.projects.models;

import java.util.Date;

import it.clipcall.professional.leads.models.eQuoteStatus;
import it.clipcall.professional.leads.models.eQuoteStatusReason;

public class QuoteHistoryItem extends HistoryItem{



    Date startJobAt;

    String description;

    double price;

    int number;

    eQuoteStatus status;

    eQuoteStatusReason statusReason;

    public int getItemTypeValue(){
        return 3;
    }

    public Date getStartJobAt() {
        return startJobAt;
    }

    public void setStartJobAt(Date startJobAt) {
        this.startJobAt = startJobAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public eQuoteStatus getStatus() {
        return status;
    }

    public void setStatus(eQuoteStatus status) {
        this.status = status;
    }

    public eQuoteStatusReason getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(eQuoteStatusReason statusReason) {
        this.statusReason = statusReason;
    }

    public boolean isBooked() {
        return this.status.equals(eQuoteStatus.Closed) && statusReason.equals(eQuoteStatusReason.Booked);
    }
}