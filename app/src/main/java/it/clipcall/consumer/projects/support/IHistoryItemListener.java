package it.clipcall.consumer.projects.support;

import it.clipcall.consumer.projects.models.CallHistoryItem;
import it.clipcall.consumer.projects.models.PaymentHistoryItem;
import it.clipcall.consumer.projects.models.QuoteHistoryItem;
import it.clipcall.consumer.projects.models.VideoHistoryItemBase;

/**
 * Created by dorona on 30/03/2016.
 */
public interface IHistoryItemListener {
    void onItemTapped(CallHistoryItem item);
    void onShareVideoTapped(VideoHistoryItemBase videoChatHistoryItem);
    void onItemTapped(VideoHistoryItemBase videoChatHistoryItem);
    void onItemTapped(PaymentHistoryItem videoChatHistoryItem);
    void onItemTapped(QuoteHistoryItem quoteHistoryItem);
}
