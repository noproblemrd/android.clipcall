package it.clipcall.consumer.projects.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.text.DecimalFormat;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.models.PaymentHistoryItem;
import it.clipcall.consumer.projects.support.IHistoryItemListener;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.dates.DateFormatter;

@EViewGroup(R.layout.project_advertiser_payment_history_item)
public class PaymentHistoryItemView extends HistoryItemView {

    @ViewById
    TextView amountSentTextView;
    @ViewById
    TextView paymentCreateDateTextView;

    @Inject
    RoutingService routingService;

    @ViewById
    ViewGroup paymentDetailsContainer;


    public PaymentHistoryItemView(Context context) {
        super(context);
    }


    @Override
    public void bind(final HistoryItem historyItem, final IHistoryItemListener listener, boolean isProMode) {
        if(historyItem == null)
            return;

        final PaymentHistoryItem item = (PaymentHistoryItem) historyItem;
        paymentDetailsContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemTapped(item);
            }
        });



        DecimalFormat df = new DecimalFormat("#.00");
        String amountSentText;
        int textColor;
        if(isProMode){
            amountSentText = String.format("Payment received: $%s",df.format(item.getAmount()));
            textColor = ContextCompat.getColor(getContext(), R.color.disabled_link_color);

        }else{
            textColor = ContextCompat.getColor(getContext(), R.color.link_color);
            amountSentText = String.format("You sent $%s to your pro using %s",df.format(item.getAmount()),item.getCardType());
        }

        amountSentTextView.setTextColor(textColor);
        amountSentTextView.setText(amountSentText);

        String formattedDate = DateFormatter.toLocalDate(item.getDate());
        paymentCreateDateTextView.setText(formattedDate);
    }


}
