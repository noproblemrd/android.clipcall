package it.clipcall.consumer.projects.models;

import org.parceler.Parcel;

@Parcel
public class CallHistoryItem extends HistoryItem {

    String url;
    boolean isCallByMe;
    boolean isMissedCall;
    long duration;


    public String getUrl() {
        return url;
    }


    public void setUrl(String url) {
        this.url = url;
    }

    public long getDuration() {
        return duration;
    }


    public void setDuration(long duration) {
        this.duration = duration;
    }


    @Override
    public int getItemTypeValue()
    {
        return 0;
    }

    public boolean isCallByMe() {
        return isCallByMe;
    }

    public void setCallByMe(boolean callByMe) {
        isCallByMe = callByMe;
    }

    public boolean isMissedCall() {
        return isMissedCall;
    }

    public void setMissedCall(boolean missedCall) {
        isMissedCall = missedCall;
    }
}