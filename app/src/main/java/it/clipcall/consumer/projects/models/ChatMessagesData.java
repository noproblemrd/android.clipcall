package it.clipcall.consumer.projects.models;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.common.chat.models.ChatMessage;

/**
 * Created by dorona on 25/02/2016.
 */
public class ChatMessagesData {

    private final List<ChatMessage> messages = new ArrayList<>();

    private int unreadMessages;

    public void AddMessage(ChatMessage message){
        messages.add(message);
    }


    public List<ChatMessage> getMessages(){
        return messages;
    }


    public int getUnreadMessages() {
        return unreadMessages;
    }

    public void setUnreadMessages(int unreadMessages) {
        this.unreadMessages = unreadMessages;
    }
}
