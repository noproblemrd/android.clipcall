package it.clipcall.consumer.projects.models;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class Advertiser {
    String id;
    String customerId;
    String name;
    String imageUrl;
    String leadAccountId;
    double googleRating;
    double yelpRating;
    Integer myLove;
    Integer allLove;
    String googleUrl;
    String yelpUrl;

   List<CallHistoryItem> callHistoryItems;

   List<HistoryItem> historyItems;

   AdvertiserData advertiserData;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     * The imageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     *
     * @return
     * The leadAccountId
     */
    public String getLeadAccountId() {
        return leadAccountId;
    }

    /**
     *
     * @param leadAccountId
     * The leadAccountId
     */
    public void setLeadAccountId(String leadAccountId) {
        this.leadAccountId = leadAccountId;
    }

    /**
     *
     * @return
     * The googleRating
     */
    public double getGoogleRating() {
        return googleRating;
    }

    /**
     *
     * @param googleRating
     * The googleRating
     */
    public void setGoogleRating(double googleRating) {
        this.googleRating = googleRating;
    }

    /**
     *
     * @return
     * The yelpRating
     */
    public double getYelpRating() {
        return yelpRating;
    }

    /**
     *
     * @param yelpRating
     * The yelpRating
     */
    public void setYelpRating(double yelpRating) {
        this.yelpRating = yelpRating;
    }

    /**
     *
     * @return
     * The myLove
     */
    public Integer getMyLove() {
        return myLove;
    }

    /**
     *
     * @param myLove
     * The myLove
     */
    public void setMyLove(Integer myLove) {
        this.myLove = myLove;
    }

    /**
     *
     * @return
     * The allLove
     */
    public Integer getAllLove() {
        return allLove;
    }

    /**
     *
     * @param allLove
     * The allLove
     */
    public void setAllLove(Integer allLove) {
        this.allLove = allLove;
    }

    /**
     *
     * @return
     * The googleUrl
     */
    public String getGoogleUrl() {
        return googleUrl;
    }

    /**
     *
     * @param googleUrl
     * The googleUrl
     */
    public void setGoogleUrl(String googleUrl) {
        this.googleUrl = googleUrl;
    }

    /**
     *
     * @return
     * The yelpUrl
     */
    public String getYelpUrl() {
        return yelpUrl;
    }

    /**
     *
     * @param yelpUrl
     * The yelpUrl
     */
    public void setYelpUrl(String yelpUrl) {
        this.yelpUrl = yelpUrl;
    }

    public void setCallHistoryItems(List<CallHistoryItem> callHistoryItems) {
        this.callHistoryItems = callHistoryItems;
    }

    public List<CallHistoryItem> getCallHistoryItems() {
        return callHistoryItems;
    }

    public void setAdvertiserData(AdvertiserData advertiserData) {
        this.advertiserData = advertiserData;
    }

    public AdvertiserData getAdvertiserData() {
        return advertiserData;
    }

    public void incrementLove() {
        this.allLove++;
        this.myLove++;
    }

    public List<HistoryItem> getHistoryItems() {
        return historyItems;
    }

    public void setHistoryItems(List<HistoryItem> historyItems) {
        this.historyItems = historyItems;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
