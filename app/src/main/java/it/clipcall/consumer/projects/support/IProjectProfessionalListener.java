package it.clipcall.consumer.projects.support;

import android.os.Bundle;

import it.clipcall.consumer.projects.models.ProfessionalRef;

/**
 * Created by dorona on 10/04/2016.
 */
public interface IProjectProfessionalListener {
    void onAdvertiserTapped(ProfessionalRef professional, Bundle bundle);
    void onAdvertiserActionTapped(ProfessionalRef professional);

    void onBookQuoteTapped(ProfessionalRef professional);
    void onRequestQuoteTapped(ProfessionalRef professional);
}
