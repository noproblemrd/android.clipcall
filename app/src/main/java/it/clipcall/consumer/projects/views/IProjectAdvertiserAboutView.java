package it.clipcall.consumer.projects.views;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by micro on 1/20/2016.
 */
public interface IProjectAdvertiserAboutView extends IView {
    void setAddress(String address);
    void setBusinessDescription(String businessDescription);
    void setMyLoveCount(int myLoveCount);
    void setCategories(String formattedCategories);

    void startRemoteCallFailed();
    void enableActions(boolean enable);
    void setChatActionEnabled(boolean enabled);

    void showCannotStartVideoChat();

    void showProfessionalData();

    void showTotalUnreadMessages(int totalUnreadMessages);

    void notifyCallingProfessional();

    void showRequestQuoteInProgress(boolean isInProgress);

    void showQuoteRequestWasSuccessful(boolean wasSuccessful);

    void showRequestQuote(boolean canRequestQuote);
}
