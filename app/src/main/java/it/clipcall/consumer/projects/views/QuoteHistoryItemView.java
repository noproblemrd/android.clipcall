package it.clipcall.consumer.projects.views;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import it.clipcall.R;
import it.clipcall.common.payment.domainModel.services.QuoteDateFormatter;
import it.clipcall.consumer.payment.services.converters.QuoteHistoryItemToQuoteConverter;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.models.QuoteHistoryItem;
import it.clipcall.consumer.projects.support.IHistoryItemListener;
import it.clipcall.infrastructure.converters.UsCurrencyFormatter;
import it.clipcall.professional.leads.models.Quote;

@EViewGroup(R.layout.quote_history_item)
public class QuoteHistoryItemView extends HistoryItemView{

    @ViewById
    TextView quotePriceTextView;

    @ViewById
    TextView quoteStartDateTextView;

    @ViewById
    ImageView bookedImageView;

    @ViewById
    TextView quoteNumberTextView;

    @ViewById
    ViewGroup quoteInfoContainer;

    public QuoteHistoryItemView(Context context) {
        super(context);
    }

    @Override
    public void bind(HistoryItem historyItem, final IHistoryItemListener listener, boolean isProMode) {
        if(historyItem == null)
            return;

        final QuoteHistoryItem item = (QuoteHistoryItem) historyItem;
        quoteNumberTextView.setText("Quote "+ String.format("%02d", item.getNumber()));

        QuoteHistoryItemToQuoteConverter converter = new QuoteHistoryItemToQuoteConverter();
        Quote quote = converter.convert(item);
        QuoteDateFormatter quoteDateFormatter = new QuoteDateFormatter();
        quoteStartDateTextView.setText("Start " + quoteDateFormatter.format(quote));

        UsCurrencyFormatter formatter = new UsCurrencyFormatter();
        quotePriceTextView.setText(formatter.format(item.getPrice()));

        quoteInfoContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemTapped(item);
            }
        });

        if(item.isBooked()){
            bookedImageView.setVisibility(VISIBLE);
        }else{
            bookedImageView.setVisibility(INVISIBLE);
        }
    }
}