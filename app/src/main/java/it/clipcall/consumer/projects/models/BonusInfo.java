package it.clipcall.consumer.projects.models;

/**
 * Created by dorona on 04/08/2016.
 */

public class BonusInfo {

    boolean hasBonus;
    double bonus;

    public boolean isHasBonus() {
        return hasBonus;
    }

    public void setHasBonus(boolean hasBonus) {
        this.hasBonus = hasBonus;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
}
