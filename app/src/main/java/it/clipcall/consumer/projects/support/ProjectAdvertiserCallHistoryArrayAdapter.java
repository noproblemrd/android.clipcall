package it.clipcall.consumer.projects.support;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.views.HistoryItemView;

public class ProjectAdvertiserCallHistoryArrayAdapter extends ArrayAdapter<HistoryItem> {

    private CallHistoryItemListener listener;


    public ProjectAdvertiserCallHistoryArrayAdapter(Context context, int resource) {
        super(context, resource, new ArrayList<HistoryItem>());
    }

    public void setCallHistoryItemListener(CallHistoryItemListener listener){
        this.listener = listener;
    }

    public void setCallHistory(List<HistoryItem> callHistoryItems){
        this.clear();
        this.addAll(callHistoryItems);
        this.notifyDataSetChanged();

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        HistoryItemView result  = null;
      /*  HistoryItem historyItem = this.getItem(position);

        if(historyItem instanceof PaymentHistoryItem){
            result = PaymentHistoryItemView_.build(getContext());
        }
        else if(historyItem instanceof  CallHistoryItem){
            result = CallHistoryItemView_.build(getContext());
        }else if(historyItem instanceof VideoChatHistoryItem){
            result = VideoChatHistoryItemView_.build(getContext());
        }
        result.bind(historyItem, listener);*/
        return result;
    }


    public interface CallHistoryItemListener{
        void onItemTapped(HistoryItem item);
    }
}
