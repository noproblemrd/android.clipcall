package it.clipcall.consumer.projects.models;

/**
 * Created by dorona on 25/05/2016.
 */
public class ProfessionalAccount {


    public ProfessionalAccount(String accountId, String professionalId, String professionalName){
        this.accountId = accountId;
        this.professionalId = professionalId;
        this.professionalName = professionalName;
    }

    private String accountId;
    private String professionalId;
    private String professionalName;

    private String projectId;

    public String getAccountId() {
        return accountId;
    }

    public String getProfessionalId() {
        return professionalId;
    }

    public String getProfessionalName() {
        return professionalName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
}
