package it.clipcall.consumer.projects.activities;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.parceler.Parcels;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.projects.fragments.ProjectAdvertiserAboutFragment_;
import it.clipcall.consumer.projects.fragments.ProjectAdvertiserHistoryFragment_;
import it.clipcall.consumer.projects.models.Advertiser;
import it.clipcall.consumer.projects.models.AdvertiserData;
import it.clipcall.consumer.projects.models.ProfessionalData;
import it.clipcall.consumer.projects.presenters.ProjectAdvertiserPresenter;
import it.clipcall.consumer.projects.support.ConsumerProjectDetailsTabsFragmentAdapter;
import it.clipcall.consumer.projects.views.IProjectAdvertiserView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.ui.images.CircleTransform;


@EActivity(R.layout.project_advertiser_activity)
public class ProjectAdvertiserActivity extends BaseActivity implements IProjectAdvertiserView, IHasToolbar {

    private static final String STATE_PROJECT_ID = "PROJECT_ID_KEY";
    private static final String STATE_ADVERTISER_ID = "ADVERTISER_ID_KEY";
    private static final String STATE_CONTEXT = "CONTEXT_KEY";
    @Extra
    String projectId;

    @Extra
    String advertiserId;


    @Extra
    Parcelable context;

    @Extra
    String landOnHistory;

    @ViewById
    Toolbar toolbar;


    @ViewById
    TabLayout tabLayout;

    @ViewById
    ViewPager viewPager;

    @Inject
    ProjectAdvertiserPresenter presenter;

    @ViewById
    ImageView advertiserProfileLogoImageView;

    @ViewById
    TextView totalHeartCountTextView;

    @ViewById
    TextView advertiserNameTextView;

    @ViewById
    ImageView clipCallHeartImageView;



    @ViewById
    CollapsingToolbarLayout collapsingToolbarLayout;

    @ViewById
    ViewGroup headerContainer;

    @ViewById
    ViewGroup root;

    ConsumerProjectDetailsTabsFragmentAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null)
            return;

        projectId = savedInstanceState.getString(STATE_PROJECT_ID);
        advertiserId = savedInstanceState.getString(STATE_ADVERTISER_ID);
        context = savedInstanceState.getParcelable(STATE_CONTEXT);
    }

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        setupViewPager();
        Advertiser advertiser = Parcels.unwrap(context);
        if(advertiser == null)
            return;

        Picasso.with(getBaseContext()).load(advertiser.getImageUrl())
                .transform(new CircleTransform())
                .into(advertiserProfileLogoImageView);
        initialize();
    }

    @Background
    void initialize(){
        presenter.initialize();
        presenter.getAdvertiserData(projectId,advertiserId);
    }


    private void setupViewPager() {
        adapter = new ConsumerProjectDetailsTabsFragmentAdapter(getSupportFragmentManager());
        adapter.addFragment(ProjectAdvertiserAboutFragment_.builder().advertiserId(advertiserId).projectId(projectId).build(), "About");
        adapter.addFragment(ProjectAdvertiserHistoryFragment_.builder().advertiserId(advertiserId).projectId(projectId).build(), "Records");
        //adapter.addFragment(ProjectAdvertiserChatFragment_.builder().advertiserId).projectId(projectId).build(), "Chat"));
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        //viewPagerContainer.setFillViewport(true);
        int transparentColor = ContextCompat.getColor(this, android.R.color.transparent);
        collapsingToolbarLayout.setCollapsedTitleTextColor(transparentColor);
        collapsingToolbarLayout.setExpandedTitleColor(transparentColor);

        selectInitialTab();

    }

    @UiThread
    @Override
    public void showProfessionalDetails(ProfessionalData professionalData) {
        if(professionalData== null)
            return;

        Advertiser advertiser = professionalData.getAdvertiser();
        if(advertiser == null)
            return;

        /***
        Picasso.with(getBaseContext()).load(advertiser.getImageUrl())
                //.fit()
                .transform(new CircleTransform())
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        //ActivityCompat.startPostponedEnterTransition(ProjectAdvertiserActivity.this);
                        advertiserProfileLogoImageView.setImageBitmap(bitmap);
                        Palette.PaletteAsyncListener paletteListener = new Palette.PaletteAsyncListener() {
                            public void onGenerated(Palette palette) {

                                List<Palette.Swatch> swatches = palette.getSwatches();
                                int[] colors = new int[swatches.size()];
                                for(int i=0; i<swatches.size();++i)
                                {
                                    int color = swatches.get(i).getRgb();
                                    colors[i] = color;
                                }

                                // access palette colors here
                                //int defaultValue = 0x000000;
                                //int vibrant = palette.getVibrantColor(defaultValue);
                                //int vibrantLight = palette.getLightVibrantColor(defaultValue);
                                //int vibrantDark = palette.getDarkVibrantColor(defaultValue);
                               // int muted = palette.getMutedColor(defaultValue);
                                //int mutedLight = palette.getLightMutedColor(defaultValue);
                                //int mutedDark = palette.getDarkMutedColor(defaultValue);
                                //int colors[] = {vibrant , vibrantLight, vibrantDark };

                                //GradientDrawable g = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, colors);
                                //headerContainer.setBackground(g);

                            }
                        };

                        //Palette.from(bitmap).generate(paletteListener);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        //ActivityCompat.startPostponedEnterTransition(ProjectAdvertiserActivity.this);
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

        ***/

        toolbar.setTitle(advertiser.getName());
        AdvertiserData data = professionalData.getData();
        totalHeartCountTextView.setText(advertiser.getAllLove()+"");
        advertiserNameTextView.setText(advertiser.getName());

        adapter.setData(professionalData);
        adapter.setParentView(this);
    }

    @UiThread
    @Override
    public void setTotalHeartCount(Integer allLove) {
        totalHeartCountTextView.setText(allLove+"");
        YoYo.with(Techniques.Bounce)
                .duration(700)
                .playOn(clipCallHeartImageView);
    }

    @UiThread
    @Override
    public void notifyCallingProfessional() {
        Snackbar snackbar = Snackbar
                .make(root, "Connecting now. Keep phone nearby", Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.shutDown();
    }

    @Override
    public String getViewTitle() {
        return null;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }

    @UiThread
    void selectInitialTab(){
        if(!Strings.isNullOrEmpty(landOnHistory))
            tabLayout.getTabAt(1).select();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putString(STATE_PROJECT_ID, projectId);
        savedInstanceState.putString(STATE_ADVERTISER_ID, advertiserId);
        savedInstanceState.putParcelable(STATE_CONTEXT, context);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }
}
