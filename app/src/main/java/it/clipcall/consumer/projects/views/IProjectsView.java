package it.clipcall.consumer.projects.views;

import java.util.List;

import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 04/01/2016.
 */
public interface IProjectsView extends IView {

    void setProjects(List<ProjectEntityBase> projects);

    void setProjectsAfterInvalidating(List<ProjectEntityBase> projects);

    void onProjectActionExecuted(ProjectEntityBase project);

    RoutingContext getFragmentRoutingContext();

    void showTotalUnreadMessages(int totalUnreadMessages);

    void setFilteredProjects(List<ProjectEntityBase> projects);
}
