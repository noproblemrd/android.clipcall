package it.clipcall.consumer.projects.models;

/**
 * Created by dorona on 13/03/2016.
 */
public abstract class VideoHistoryItemBase extends HistoryItem {

    private String url;
    private Integer duration;
    private Integer callType;


    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }


    public Integer getCallType() {
        return callType;
    }

    public void setCallType(Integer callType) {
        this.callType = callType;
    }

    @Override
    public int getItemTypeValue()
    {
        return 2;
    }

    public abstract  boolean isProjectVideo();

}