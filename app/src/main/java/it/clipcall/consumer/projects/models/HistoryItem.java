package it.clipcall.consumer.projects.models;

import org.parceler.Parcel;

import java.util.Date;

@Parcel
public class HistoryItem {

    Date date;
    String itemType;
    String id;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }


    public int getItemTypeValue(){
        return -1;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
