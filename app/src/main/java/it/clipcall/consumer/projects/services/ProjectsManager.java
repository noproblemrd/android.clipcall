package it.clipcall.consumer.projects.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.payment.models.PaymentProfessional;
import it.clipcall.consumer.payment.models.PaymentProject;
import it.clipcall.consumer.projects.models.Advertiser;
import it.clipcall.consumer.projects.models.AdvertiserData;
import it.clipcall.consumer.projects.models.CallHistoryItem;
import it.clipcall.consumer.projects.models.ConsumerDiscount;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.network.RetrofitCallProcessor;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by dorona on 02/12/2015.
 */
@Singleton
public class ProjectsManager {

    private final List<ProjectEntityBase> projects = new ArrayList<>();;

    private final ICustomerProjectsService customerProjectsService;



    @Inject
    public ProjectsManager(ICustomerProjectsService customerProjectsService){
        this.customerProjectsService = customerProjectsService;
    }

    public List<ProjectEntityBase> getAll(String customerId, boolean forceReload){

        if(!forceReload && projects != null && projects.size() > 0){
            return projects;
        }

        projects.clear();
        String supplierLeadStatus = "Any";
        int pageSize = 100;
        int pageNumber = 0;
        try {
            Response<List<ProjectEntity>> execute = customerProjectsService.getCustomerProjects(customerId, pageSize, supplierLeadStatus, pageNumber).execute();
            if(execute == null)
                return projects;

            List<ProjectEntity> customerProjects = execute.body();
            if(customerProjects != null){
                projects.addAll(customerProjects);
            }
            return projects;
        } catch (IOException e) {
            return projects;
        }

    }

    public ProjectEntityBase getById(String id){
        for(ProjectEntityBase project: projects)
            if(project.getId().equals(id))
                return project;

        return null;
    }

    public AdvertiserData getAdvertiserData(String customerId, String projectId, String advertiserId){
        try {
            ProjectEntityBase project = getById(projectId);
            if(project == null)
                return null;
            ProjectEntity entity = (ProjectEntity)project;
            AdvertiserData advertiserData = entity.getAdvertiserData(advertiserId);
            if(advertiserData != null)
                return advertiserData;

            Response<AdvertiserData> execute = customerProjectsService.getAdvertiserData(customerId, projectId, advertiserId).execute();
            if (execute == null)
                return null;

            advertiserData = execute.body();
            if(advertiserData != null){
                ProjectEntity projectEntity = (ProjectEntity)project;
                projectEntity.setAdvertiserData(advertiserId, advertiserData);
            }
            return advertiserData;
        } catch (IOException e) {
            return null;
        }
    }

    public List<ProfessionalRef> getProjectAdvertisers(String customerId, ProjectEntity project){

        try {
            Response<List<Advertiser>> execute = customerProjectsService.getProjectAdvertisers(customerId, project.getId()).execute();
            if (execute == null)
                return new ArrayList<>();

            List<Advertiser> advertisers = execute.body();
            if(advertisers != null){
                project.setAdvertisers(advertisers);
            }
            return project.getAdvertisers();
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    public List<CallHistoryItem> getAdvertiserCallHistory(String customerId, String projectId, String advertiserId) {
        ProjectEntityBase project = getById(projectId);
        if(project == null)
            return new ArrayList<>();

        ProjectEntity entity = (ProjectEntity)project;

        Advertiser advertiser = entity.getAdvertiserById(advertiserId);
        if(advertiser == null)
            return new ArrayList<>();


        Call<List<CallHistoryItem>> callHistoryCall = customerProjectsService.getAdvertiserCallHistory(customerId, advertiser.getLeadAccountId(), advertiserId);
        List<CallHistoryItem> historyItems = RetrofitCallProcessor.processCall(callHistoryCall);
        if(Lists.isNullOrEmpty(historyItems))
            historyItems = new ArrayList<>();

        entity.setAdvertiserCallHistory(advertiserId, historyItems);
        return historyItems;
    }

    public List<HistoryItem> getAdvertiserHistoryItems(String customerId, String projectId, String advertiserId) {
        ProjectEntityBase project = getById(projectId);
        if(project == null)
            return new ArrayList<>();

        ProjectEntity entity = (ProjectEntity)project;
        Advertiser advertiser = entity.getAdvertiserById(advertiserId);
        if(advertiser == null)
            return new ArrayList<>();

        Call<List<HistoryItem>> historyItemsCall = customerProjectsService.getAdvertiserHistoryItems(customerId, advertiser.getLeadAccountId(), advertiserId);
        List<HistoryItem> historyItems = RetrofitCallProcessor.processCall(historyItemsCall);
        if(Lists.isNullOrEmpty(historyItems))
            historyItems = new ArrayList<>();
        entity.setAdvertiserHistoryItems(advertiserId, historyItems);
        return historyItems;
    }

    public boolean likeAdvertiser(String customerId, String projectId, String advertiserId){
        ProjectEntityBase project = getById(projectId);
        if(project == null)
            return false;

        ProjectEntity entity = (ProjectEntity)project;
        Advertiser advertiser = entity.getAdvertiserById(advertiserId);
        if(advertiser == null)
            return false;

        Call<StatusResponse> statusResponseCall = customerProjectsService.likeAdvertiser(customerId, advertiserId);
        boolean success =  RetrofitCallProcessor.processStatusCall(statusResponseCall);

        if(success){
            advertiser.incrementLove();
        }
        return success;
    }

    public boolean callAdvertiser(String customerId, String projectId, String advertiserId){
        ProjectEntityBase project = getById(projectId);
        if(project == null)
            return false;

        ProjectEntity entity = (ProjectEntity)project;
        Advertiser advertiser = entity.getAdvertiserById(advertiserId);
        if(advertiser == null)
            return false;

        Call<StatusResponse> statusResponseCall = customerProjectsService.callTheAdvertiser(customerId, advertiser.getLeadAccountId());
        boolean success = RetrofitCallProcessor.processStatusCall(statusResponseCall);
        return success;
    }

    public List<PaymentProfessional> getConsumerProfessionals(String customerId){
       /* List<PaymentProfessional> result = new ArrayList<>();

        result.add(new PaymentProfessional("8fe783cc-5e2c-404d-86eb-1fdac20227d1", "ofir 222","https://s3.amazonaws.com/mobileclipcall2/qa_supplier/8fe783cc-5e2c-404d-86eb-1fdac20227d1/Logo_logo_995.png",5,10));
        result.add(new PaymentProfessional("c6d73b19-a5d8-47a8-9f89-1e4579cf7a61", "Ofir iPhone 6","https://s3.amazonaws.com/mobileclipcall2/qa_supplier/c6d73b19-a5d8-47a8-9f89-1e4579cf7a61/Logo_731.png",2,14));
        result.add(new PaymentProfessional("f9530c36-7e00-4ed0-b1c3-3f8db4dda0a7", "RegisQA Mobile New 141","https://s3.amazonaws.com/mobileclipcall2/qa_supplier/f9530c36-7e00-4ed0-b1c3-3f8db4dda0a7/Logo_680.png",8,214));
        result.add(new PaymentProfessional("aa43b47c-1685-4bba-936a-4332a01c679f", "Aaaas", "https://s3.amazonaws.com/mobileclipcall2/qa_supplier/aa43b47c-1685-4bba-936a-4332a01c679f/Logo_64.png",8,214));

        return result;*/

        Call<List<PaymentProfessional>> customerConnectedProfessionalsCall = customerProjectsService.getCustomerConnectedProfessionals(customerId);
        List<PaymentProfessional> paymentProfessionals = RetrofitCallProcessor.processCall(customerConnectedProfessionalsCall);
        if(paymentProfessionals == null){
            return new ArrayList<>();
        }

        return paymentProfessionals;
    }


    public Advertiser getProjectAdvertiser( String projectId, String advertiserId) {
        ProjectEntityBase project = getById(projectId);
        if(project == null)
            return null;

        ProjectEntity entity = (ProjectEntity)project;
        Advertiser advertiser = entity.getAdvertiserById(advertiserId);
        return advertiser;
    }

    public List<PaymentProject> getProjectsOfProfessional(String userId, PaymentProfessional professional) {
      /*  List<PaymentProject> result = new ArrayList<>();

        PaymentProject project = new PaymentProject();
        project.setCategory("Plumber");
        project.setDate(new Date());
        project.setFullAddress("San Francisco");
        project.setVideoUrl("http://d1wc6cu8i70qi6.cloudfront.net/media/ClipCallConsumersVideos/1460459355/1460459355.m3u84");
        project.setPicPreviewUrl("https://s3.amazonaws.com/mobileclipcall2/3d48abe9-76e8-4d2f-af5d-c48525a32c95/2016-04-12_11_09_25_0000.png");
        project.setLeadAccountId("ab889be6-42f1-4cdb-808d-b41f66a37743");
        result.add(project);
        return result;*/
        Call<List<PaymentProject>> professionalProjectsCall = customerProjectsService.getProfessionalProjects(userId,professional.getSupplierId());
        List<PaymentProject> professionalProjects = RetrofitCallProcessor.processCall(professionalProjectsCall);
        if(professionalProjects == null){
            return new ArrayList<>();
        }
        return professionalProjects;
    }

    public boolean setChatCreated(String userId, String leadAccountId){
        Call<StatusResponse> statusResponseCall = customerProjectsService.notifyChatCreated(userId, leadAccountId);
        boolean notified = RetrofitCallProcessor.processStatusCall(statusResponseCall);
        return notified;
    }

    public boolean isDiscountClaimed(String userId) {
        Call<ConsumerDiscount> bonusClaimedCall = customerProjectsService.isDiscountClaimed(userId);
        ConsumerDiscount bonusClaimed = RetrofitCallProcessor.processCall(bonusClaimedCall);
        if(bonusClaimed == null)
            return false;

        return bonusClaimed.isConfirmed;
    }

    public boolean claimDiscount(String userId){
        Call<StatusResponse> claimDiscountCall = customerProjectsService.claimDiscount(userId);
        boolean claimedSuccessfully = RetrofitCallProcessor.processStatusCall(claimDiscountCall);
        return claimedSuccessfully;
    }



    public boolean requestQuote(String customerId, String projectId) {
        Call<StatusResponse> statusResponseCall = customerProjectsService.requestQuote(customerId, projectId);
        boolean success = RetrofitCallProcessor.processStatusCall(statusResponseCall);
        return success;
    }
}
