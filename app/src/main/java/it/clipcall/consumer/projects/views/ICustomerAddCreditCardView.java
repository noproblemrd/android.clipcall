package it.clipcall.consumer.projects.views;

import com.stripe.android.model.Token;

import it.clipcall.consumer.projects.models.ProfessionalAccount;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 05/01/2016.
 */
public interface ICustomerAddCreditCardView extends IView {

    void showInvalidCreditCard();

    void showSuccessMessage(String message);

    void setDepositDetails(double amount, ProfessionalAccount professionalAccount);

    void tokenGenerated(Token token);

    void showFailureMessage(String message);

    void showInvalidCreditCardNumber();

    void showInvalidExpirationMonth();

    void showInvalidExpirationYear();

    void showInvalidCvv();

    void showInvalidEmail();

    void showEmptyEmail();

    void showInvalidFullName();
}
