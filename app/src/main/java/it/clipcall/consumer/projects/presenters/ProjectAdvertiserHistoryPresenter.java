package it.clipcall.consumer.projects.presenters;

import android.os.Bundle;

import com.google.common.base.Strings;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.common.projects.models.ProjectVideoData;
import it.clipcall.consumer.payment.models.PaymentDetails;
import it.clipcall.consumer.payment.services.converters.QuoteHistoryItemToQuoteConverter;
import it.clipcall.consumer.projects.controllers.ProjectAdvertiserController;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.models.PaymentHistoryItem;
import it.clipcall.consumer.projects.models.ProfessionalData;
import it.clipcall.consumer.projects.models.QuoteHistoryItem;
import it.clipcall.consumer.projects.models.VideoHistoryItemBase;
import it.clipcall.consumer.projects.views.IProjectAdvertiserHistoryView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.professional.leads.models.Quote;

/**
 * Created by dorona on 20/01/2016.
 */
@PerActivity
public class ProjectAdvertiserHistoryPresenter extends PresenterBase<IProjectAdvertiserHistoryView> {

    private final ProjectAdvertiserController controller;
    private final RoutingService routingService;
    private final RouteParams routeParams;

    private ProfessionalData professionalData;



    @Inject
    public ProjectAdvertiserHistoryPresenter(ProjectAdvertiserController controller, RoutingService routingService, RouteParams routeParams) {
        this.controller = controller;
        this.routingService = routingService;
        this.routeParams = routeParams;
    }

    public void loadHistoryItems() {
        List<HistoryItem> historyItems = this.controller.getHistoryItems(professionalData);
        view.showHistoryItems(historyItems);
    }

    public void setProfessionalData(ProfessionalData professionalData) {
        this.professionalData = professionalData;
        List<HistoryItem> historyItems = professionalData.getHistoryItems();
        view.showHistoryItems(historyItems);
    }

    public void navigateToPaymentInvoice(PaymentHistoryItem item){
        NavigationContext context = new NavigationContext(view.getRoutingContext());
        /*context.addParameter("paymentId", item.getId());
        context.addParameter("projectId", professionalData.getProject().getId());
        context.addParameter("advertiserId", professionalData.getAdvertiser().getId());
        context.bundle = new Bundle();*/
        PaymentDetails paymentDetails = new PaymentDetails();
        paymentDetails.cardType = item.getCardType();
        paymentDetails.amount = item.getAmount();
        paymentDetails.createDate = item.getDate();
        paymentDetails.id = item.getId();
        paymentDetails.description = item.getDescription();
        paymentDetails.customerName =  controller.getCustomerNameOrPhoneNumber();
        paymentDetails.customerPhoneNumber = controller.getConsumerPhoneNumber();
        paymentDetails.last4Digits =  item.getLast4Digits();



        routeParams.reset();
        routeParams.setParam("paymentDetails",paymentDetails);
        routeParams.setParam("projectId",professionalData.getProject().getId());
        routeParams.setParam("advertiserId",professionalData.getAdvertiser().getId());


        //context.bundle.putParcelable("paymentDetails", Parcels.wrap(paymentDetails));

        routingService.routeTo(Routes.ConsumerPaymentInvoiceRoute,context);

    }

    public void showVideoChat(VideoHistoryItemBase videoChatHistoryItem) {
        if(Strings.isNullOrEmpty(videoChatHistoryItem.getUrl()))
            return;

        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        ProjectVideoData projectVideoData = new ProjectVideoData();
        projectVideoData.zipCode = professionalData.getProject().getAddressOrZipCode();
        projectVideoData.category = professionalData.getProject().getCategoryName();
        projectVideoData.url = videoChatHistoryItem.getUrl();
        projectVideoData.createDate = professionalData.getProject().getCreatedOn();

        navigationContext.bundle = new Bundle();
        navigationContext.bundle.putParcelable("context", Parcels.wrap(projectVideoData));
        routingService.routeTo(Routes.ProjectVideoRoute, navigationContext);
    }

    public void showQuote(QuoteHistoryItem quoteHistoryItem) {

        QuoteHistoryItemToQuoteConverter converter = new QuoteHistoryItemToQuoteConverter();
        Quote quote = converter.convert(quoteHistoryItem);
        routeParams.setParam("quote",quote);
        routeParams.setParam("project",professionalData.getProject());
        routeParams.setParam("quote",quote);
        routeParams.setParam("leadAccountId",professionalData.getAdvertiser().getLeadAccountId());

        NavigationContext navContext = new NavigationContext(view.getRoutingContext());
        routingService.routeTo(Routes.ConsumerQuoteDetailsRoute,navContext);
    }
}
