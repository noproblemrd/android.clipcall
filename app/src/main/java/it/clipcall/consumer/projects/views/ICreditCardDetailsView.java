package it.clipcall.consumer.projects.views;

import com.stripe.android.model.Token;

import it.clipcall.consumer.projects.models.ProfessionalAccount;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.leads.models.Quote;

/**
 * Created by dorona on 05/01/2016.
 */
public interface ICreditCardDetailsView extends IView {

    void showInvalidCreditCard();

    void showSuccessMessage(String message);

    void setDepositDetails(double amount, ProfessionalAccount professionalAccount);

    void tokenGenerated(Token token);

    void showFailureMessage(String message);

    void showInvalidCreditCardNumber();

    void showInvalidExpirationMonth();

    void showInvalidExpirationYear();

    void showInvalidCvv();

    void showInvalidEmail();

    void showEmptyEmail();

    void showQuote(Quote quote, ProfessionalRef professional);

    void showInvalidFullName();

}
