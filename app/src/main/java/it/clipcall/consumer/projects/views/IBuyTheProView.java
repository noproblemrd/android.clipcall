package it.clipcall.consumer.projects.views;

import it.clipcall.consumer.projects.models.ProfessionalAccount;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 05/01/2016.
 */
public interface IBuyTheProView extends IView {

    void showInvalidAmount();

    void showPayedProfessional(ProfessionalAccount professionalAccount);

    void showBonus(double bonusAmount);
}
