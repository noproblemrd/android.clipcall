package it.clipcall.consumer.projects.views;

import android.content.Context;
import android.widget.LinearLayout;

import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.support.IHistoryItemListener;

public abstract class HistoryItemView extends LinearLayout {

    public HistoryItemView(Context context) {
        super(context);
    }

    public abstract void bind(final HistoryItem callHistoryItem, final IHistoryItemListener listener, boolean isProMode);

}
