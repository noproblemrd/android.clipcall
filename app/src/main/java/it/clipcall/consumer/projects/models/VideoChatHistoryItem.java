package it.clipcall.consumer.projects.models;

/**
 * Created by dorona on 13/03/2016.
 */
public class VideoChatHistoryItem extends VideoHistoryItemBase {


    private Boolean isCallByMe;
    private Boolean isMissedCall;

    public Boolean getIsCallByMe() {
        return isCallByMe;
    }

    public void setIsCallByMe(Boolean isCallByMe) {
        this.isCallByMe = isCallByMe;
    }

    public Boolean getIsMissedCall() {
        return isMissedCall;
    }

    public void setIsMissedCall(Boolean isMissedCall) {
        this.isMissedCall = isMissedCall;
    }

    @Override
    public boolean isProjectVideo() {
        return false;
    }
}