package it.clipcall.consumer.projects.views;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.text.DecimalFormat;

import it.clipcall.R;
import it.clipcall.common.payment.domainModel.services.QuoteDateFormatter;
import it.clipcall.consumer.projects.models.Advertiser;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.support.IProjectProfessionalListener;
import it.clipcall.infrastructure.converters.UsCurrencyFormatter;
import it.clipcall.infrastructure.ui.images.CircleTransform;
import it.clipcall.professional.leads.models.Quote;

@EViewGroup(R.layout.project_advertiser_item)
public class AdvertiserItemView extends LinearLayout {


    @ViewById(R.id.avatar_imageview)
    ImageView advertiserImageView;


    @ViewById
    ViewGroup lastQuoteContainer;

    @ViewById
    TextView lastQuotePriceTextView;

    @ViewById
    TextView lastQuoteStartDateTextView;

    // professional info
    @ViewById(R.id.advertiserNameTextView)
    TextView advertiserNameTextView;

    @ViewById(R.id.clipCallTitleTextView)
    TextView clipCallTitleTextView;


    @ViewById(R.id.clipCallLoveImageView)
    ImageView clipCallLoveImageView;

    @ViewById(R.id.clipCallLoveCountTextView)
    TextView clipCallLoveCountTextView;


    @ViewById(R.id.YelpRatingImageView)
    ImageView yelpRatingImageView;

    @ViewById(R.id.yelpTotalReviewsTextView)
    TextView yelpTotalReviewsTextView;

    @ViewById(R.id.yelpLogoImageView)
    ImageView yelpLogoImageView;

    @ViewById
    ImageView quoteRequestedIconImageView;


    @ViewById(R.id.googleRatingImageView)
    ImageView googleRatingImageView;

    @ViewById(R.id.googleTotalReviewsTextView)
    TextView googleTotalReviewsTextView;

    @ViewById(R.id.googleLogoImageView)
    ImageView googleLogoImageView;

    @ViewById
    TextView unreadMessagesCountTextView;

    @ViewById
    ImageView bookedIconImageView;

    @ViewById
    Button requestQuoteButton;

    @ViewById
    Button bookNowButton;


    @ViewById
    ViewGroup advertiserItemContainer;


    @ViewById
    ImageView actionImageView;


    @ViewById
    ViewGroup quoteContainer;

    @ViewById
    ViewGroup interestedContainer;


    @ViewById
    TextView quotePriceTextView;

    @ViewById
    TextView askProForQuoteTextView;

    @ViewById
    ProgressBar requestQuoteProgressBar;


    public AdvertiserItemView(Context context) {
        super(context);
    }

    public void bind(final ProfessionalRef professional, final IProjectProfessionalListener projectProfessionalListener) {

        if(professional == null)
            return;

        Advertiser advertiser = professional.getAdvertiser();
        if(advertiser == null)
            return;

        advertiserNameTextView.setText(advertiser.getName());
        Picasso.with(getContext()).load(advertiser.getImageUrl())
                .fit()
                .transform(new CircleTransform())
                .into(advertiserImageView);

        clipCallTitleTextView.setText("ClipCall");
        clipCallLoveCountTextView.setText(advertiser.getMyLove().toString());

        yelpTotalReviewsTextView.setText(advertiser.getYelpRating() + "reviews on ");
        yelpTotalReviewsTextView.setText(advertiser.getGoogleRating() + "reviews on ");

        advertiserItemContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    advertiserImageView.setTransitionName("imageTransition");
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation((Activity) getContext(), advertiserImageView, advertiserImageView.getTransitionName());
                    bundle = options.toBundle();
                }

                projectProfessionalListener.onAdvertiserTapped(professional, bundle);

            }
        });

        actionImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                projectProfessionalListener.onAdvertiserActionTapped(professional);
            }
        });

        if(professional.getTotalUnreadMessages() > 0 && !professional.isLost()){
            unreadMessagesCountTextView.setVisibility(VISIBLE);
            unreadMessagesCountTextView.setText(""+professional.getTotalUnreadMessages() );
        }else{
            unreadMessagesCountTextView.setText("" );
            unreadMessagesCountTextView.setVisibility(GONE);
        }

        /*if(professional.getLastSentMessage() != null){
            lastMessageContainer.setVisibility(VISIBLE);
            String lastMessage = ChatMessageFactoryProvider.getInstance().getMessageDescription(professional.getLastSentMessage());
            lastMessageTextView.setText(lastMessage);
        }else{
            lastMessageContainer.setVisibility(GONE);
            lastMessageTextView.setText("");
        }*/

        resetView();

        QuoteDateFormatter formatter = new QuoteDateFormatter();

        if(professional.isLost()){
            advertiserItemContainer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.grey));
            advertiserItemContainer.setOnClickListener(null);
            requestQuoteButton.setVisibility(GONE);
            bookedIconImageView.setVisibility(GONE);
            bookNowButton.setVisibility(GONE);
            quoteRequestedIconImageView.setVisibility(GONE);
        }else{
            advertiserItemContainer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
        }

        Quote lastQuote = professional.getLastQuote();
        if(lastQuote != null){
            interestedContainer.setVisibility(GONE);
            lastQuoteContainer.setVisibility(VISIBLE);


            UsCurrencyFormatter usCurrencyFormatter = new UsCurrencyFormatter();
            String formattedValue = usCurrencyFormatter.format(lastQuote.getPrice());
            lastQuotePriceTextView.setText(formattedValue);

            String formattedStartDate = formatter.format(lastQuote);
            lastQuoteStartDateTextView.setText("Start "+formattedStartDate);
        }else{
            interestedContainer.setVisibility(VISIBLE);
            lastQuoteContainer.setVisibility(INVISIBLE);
        }

        if(professional.isQuoteRequested() && professional.getLastQuote() == null && !professional.isLost()) {
            requestQuoteButton.setVisibility(GONE);
            bookedIconImageView.setVisibility(GONE);
            bookNowButton.setVisibility(GONE);
            quoteRequestedIconImageView.setVisibility(VISIBLE);
        }

        if(professional.canRequestQuote()){

            requestQuoteButton.setVisibility(VISIBLE);
            bookedIconImageView.setVisibility(GONE);
            bookNowButton.setVisibility(GONE);
            quoteRequestedIconImageView.setVisibility(GONE);
            askProForQuoteTextView.setOnClickListener(null);
            askProForQuoteTextView.setVisibility(GONE);
            quotePriceTextView.setVisibility(GONE);
            requestQuoteProgressBar.setVisibility(GONE);
            Quote quote = professional.getLastQuote();
            /*DecimalFormat df = new DecimalFormat("#.00");
            quotePriceTextView.setText("$"+ df.format(quote.getPrice()));*/

            requestQuoteButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    projectProfessionalListener.onRequestQuoteTapped(professional);
                    askProForQuoteTextView.setVisibility(GONE);
                    requestQuoteButton.setVisibility(GONE);
                    requestQuoteProgressBar.setVisibility(VISIBLE);
                }
            });




            /*askProForQuoteTextView.setVisibility(VISIBLE);
            quotePriceTextView.setVisibility(GONE);
            requestQuoteProgressBar.setVisibility(GONE);
            askProForQuoteTextView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    projectProfessionalListener.onRequestQuoteTapped(professional);
                    askProForQuoteTextView.setVisibility(GONE);
                    requestQuoteProgressBar.setVisibility(VISIBLE);
                }
            });*/
        }else if(professional.isBooked()){
            bookedIconImageView.setVisibility(VISIBLE);
            quoteRequestedIconImageView.setVisibility(GONE);
            bookNowButton.setVisibility(GONE);
            requestQuoteButton.setVisibility(GONE);
            bookNowButton.setOnClickListener(null);
            requestQuoteButton.setOnClickListener(null);
            askProForQuoteTextView.setOnClickListener(null);
            askProForQuoteTextView.setVisibility(GONE);
            quotePriceTextView.setVisibility(GONE);
            requestQuoteProgressBar.setVisibility(GONE);
            Quote quote = professional.getLastQuote();
            DecimalFormat df = new DecimalFormat("#.00");
            quotePriceTextView.setText("$"+ df.format(quote.getPrice()));

        }else if(professional.quoteIsPending()){
            bookedIconImageView.setVisibility(GONE);
            bookNowButton.setVisibility(VISIBLE);
            quoteRequestedIconImageView.setVisibility(GONE);

            bookNowButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    bookNowButton.setVisibility(GONE);
                    projectProfessionalListener.onBookQuoteTapped(professional);
                    askProForQuoteTextView.setVisibility(GONE);
                    requestQuoteButton.setVisibility(GONE);
                    requestQuoteProgressBar.setVisibility(VISIBLE);
                }
            });
            requestQuoteButton.setVisibility(GONE);
            askProForQuoteTextView.setOnClickListener(null);
            askProForQuoteTextView.setVisibility(GONE);
            quotePriceTextView.setVisibility(GONE);
            requestQuoteProgressBar.setVisibility(GONE);
            Quote quote = professional.getLastQuote();
            DecimalFormat df = new DecimalFormat("#.00");
            quotePriceTextView.setText("$"+ df.format(quote.getPrice()));
        }
    }

    private void resetView(){
        bookedIconImageView.setVisibility(GONE);
        bookNowButton.setVisibility(GONE);
        requestQuoteButton.setVisibility(GONE);
        bookNowButton.setOnClickListener(null);
        requestQuoteButton.setOnClickListener(null);
        askProForQuoteTextView.setOnClickListener(null);
        askProForQuoteTextView.setVisibility(GONE);
        quotePriceTextView.setVisibility(GONE);
        requestQuoteProgressBar.setVisibility(GONE);
    }
}
