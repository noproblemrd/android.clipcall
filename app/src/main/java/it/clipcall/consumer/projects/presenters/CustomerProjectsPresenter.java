package it.clipcall.consumer.projects.presenters;

import android.os.Bundle;

import com.google.common.base.Strings;
import com.layer.sdk.changes.LayerChangeEvent;

import org.parceler.Parcels;
import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.common.projects.models.ProjectVideoData;
import it.clipcall.consumer.projects.controllers.ConsumerProjectsController;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.consumer.projects.views.IProjectsView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.collections.ICounter;
import it.clipcall.infrastructure.support.collections.Lists;

/**
 * Created by dorona on 04/01/2016.
 */
@PerActivity
public class CustomerProjectsPresenter extends PresenterBase<IProjectsView> {
    private final RoutingService routingService;
    private final RouteParams routeParams;
    private final ConsumerProjectsController controller;

    @Inject
    public CustomerProjectsPresenter(ConsumerProjectsController controller, RoutingService routingService, RouteParams routeParams) {
        this.controller = controller;
        this.routingService = routingService;
        this.routeParams = routeParams;
    }

    @Override
    public void initialize() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void shutDown() {
        EventBus.getDefault().unregister(this);
    }

    public void getCustomerProjects(boolean forceReload){
        List<ProjectEntityBase> projects = controller.getCustomerProjects(forceReload);
        view.setProjects(projects);
        int totalUnreadMessages = Lists.sum(projects, new ICounter<ProjectEntityBase>() {
            @Override
            public int count(ProjectEntityBase candidate) {
                return candidate.getTotalUnreadMessages();
            }
        });
        view.showTotalUnreadMessages(totalUnreadMessages);
    }

    public void projectSelected(ProjectEntity project) {
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();
        navigationContext.addParameter("projectId", project.getId());
        navigationContext.addParameter("professionalCount",project.getTotalProfessionals()+"");

        routeParams.reset();
        routeParams.setParam("project", project);
        routingService.routeTo(Routes.ConsumerProjectDetailsRoute, navigationContext);

    }

    public void createNewProjectTapped() {
        EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.CREATE_NEW_PROJECT));

        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getFragmentRoutingContext();
        routingService.routeTo(Routes.ConsumerFindProRoute, navigationContext);
    }



    @Subscriber(mode = ThreadMode.ASYNC)
    void onChatEvent(LayerChangeEvent layerChangeEvent) {
        boolean forceReload = false;
        List<ProjectEntityBase> projects = controller.getCustomerProjects(forceReload);
        int totalUnreadMessages = Lists.sum(projects, new ICounter<ProjectEntityBase>() {
            @Override
            public int count(ProjectEntityBase candidate) {
                return candidate.getTotalUnreadMessages();
            }
        });
        view.showTotalUnreadMessages(totalUnreadMessages);
        view.setProjectsAfterInvalidating(projects);
    }

    public void playVideo(ProjectEntityBase project) {
        if(Strings.isNullOrEmpty(project.getVideoUrl()))
            return;

        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        ProjectVideoData projectVideoData = new ProjectVideoData();
        projectVideoData.zipCode = project.getAddressOrZipCode();
        projectVideoData.category = project.getCategoryName();
        projectVideoData.url = project.getVideoUrl();
        projectVideoData.createDate = project.getCreatedOn();

        navigationContext.bundle = new Bundle();
        navigationContext.bundle.putParcelable("context", Parcels.wrap(projectVideoData));
        navigationContext.addParameter("projectVideoUrl",project.getVideoUrl());
        routingService.routeTo(Routes.ProjectVideoRoute, navigationContext);
    }
}