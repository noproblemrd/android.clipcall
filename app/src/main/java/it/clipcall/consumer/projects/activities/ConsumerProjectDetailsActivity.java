package it.clipcall.consumer.projects.activities;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.commit451.nativestackblur.NativeStackBlur;
import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.consumer.projects.presenters.CustomerProjectDetailsPresenter;
import it.clipcall.consumer.projects.support.IProjectProfessionalListener;
import it.clipcall.consumer.projects.support.ProjectAdvertisersArrayAdapter;
import it.clipcall.consumer.projects.views.IProjectDetailsView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.infrastructure.support.criterias.NotCriteria;
import it.clipcall.infrastructure.ui.UiDimensionConverter;

@EActivity(R.layout.consumer_activity_project_details)
public class ConsumerProjectDetailsActivity extends BaseActivity implements IProjectDetailsView, IHasToolbar, IProjectProfessionalListener {

    @ViewById
    SwipeRefreshLayout projectProfessionalsSwipeRefreshLayout;

    @ViewById
    SwipeRefreshLayout projectProfessionalsEmptySwipeRefreshLayout;

    @ViewById
    Toolbar toolbar;

    @ViewById
    ImageView blurredImageView;

    @ViewById
    ImageView circularImageView;

    @ViewById
    RecyclerView projectAdvertisersListView;

    @ViewById(R.id.playVideoImageView)
    ImageView playVideoImageView;

    @ViewById
    TextView categoryTextView;

    @ViewById
    TextView zipCodeTextView;

    @ViewById
    TextView quoteSummaryTextView;

    @ViewById
    android.support.design.widget.AppBarLayout appbar;

    @ViewById
    android.support.design.widget.CollapsingToolbarLayout collapsingToolbarLayout;

    ProjectAdvertisersArrayAdapter advertisersAdapter;

    @Inject
    CustomerProjectDetailsPresenter presenter;

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        advertisersAdapter = new ProjectAdvertisersArrayAdapter(this, this);
        projectAdvertisersListView.setLayoutManager(new LinearLayoutManager(this));
        projectAdvertisersListView.setAdapter(advertisersAdapter);
        projectProfessionalsSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadProjectProfessionals();
            }
        });
        projectProfessionalsEmptySwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadProjectProfessionals();
            }
        });

        //blurringView.setBlurredView(projectImage);
    }

    @Override
    public void onBackPressed() {
        presenter.routeToHome();
    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            BitmapDrawable drawable = new BitmapDrawable(getResources(),bitmap);
            Bitmap source = bitmap;
            Bitmap destination = NativeStackBlur.process(source, 50);
            Drawable destinationDrawable = new BitmapDrawable(getResources(),destination);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                blurredImageView.setImageDrawable(destinationDrawable);
                circularImageView.setImageDrawable(drawable);
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        presenter.bindView(this);
        presenter.initialize();
        loadProjectProfessionals();
    }



    @Background
    void loadProjectProfessionals(){
        presenter.loadProjectProfessionals();
    }

    @Background
    void advertiserActionTapped(ProfessionalRef advertiser) {
        presenter.executeAdvertiserAction(advertiser);
    }

   /* @ItemClick(R.id.projectAdvertisersListView)
    void itemClicked(int position) {
        ProfessionalRef advertiser = advertisersAdapter.getItem(position);
        advertiserTapped(advertiser, new Bundle());
    }
*/

    @Override
    public void setProjectProfessionals(List<ProfessionalRef> professionals) {


        final ICriteria<ProfessionalRef> professionalWithQuoteCriteria = new ICriteria<ProfessionalRef>(){
            @Override
            public boolean isSatisfiedBy(ProfessionalRef candidate) {
                return candidate.getLastQuote() != null;
            }
        };
        ICriteria<ProfessionalRef> professionalsWithoutQuoteCriteria = new NotCriteria<>(professionalWithQuoteCriteria);

        if(!Lists.isNullOrEmpty(professionals)){
            int totalWithQuote = Lists.where(professionals,  professionalWithQuoteCriteria).size();
            int totalWithoutQuotes = Lists.where(professionals,professionalsWithoutQuoteCriteria).size();
            String formattedText = "You got: %d quotes, %d interested";
            String quoteSummaryText = String.format(formattedText, totalWithQuote, totalWithoutQuotes);
            quoteSummaryTextView.setText(quoteSummaryText);
        }


        if(Lists.isNullOrEmpty(professionals)){
            projectProfessionalsEmptySwipeRefreshLayout.setVisibility(View.VISIBLE);
            projectProfessionalsSwipeRefreshLayout.setVisibility(View.GONE);
        }else{
            projectProfessionalsEmptySwipeRefreshLayout.setVisibility(View.GONE);
            projectProfessionalsSwipeRefreshLayout.setVisibility(View.VISIBLE);
        }
        advertisersAdapter.setItems(professionals);
    }

    @UiThread
    @Override
    public void showProjectDetails(ProjectEntityBase project) {
        ProjectEntity projectEntity = (ProjectEntity) project;
        int totalProfessionals = projectEntity.getTotalProfessionals();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenHeight  = size.y;

        int heightOfList = totalProfessionals * 100;
        heightOfList = Math.max(100, heightOfList);
        heightOfList = Math.min(300, heightOfList);

        int heightInPixels = UiDimensionConverter.dpToPx(heightOfList);
        int appBarHeight = screenHeight - heightInPixels;
        appbar.getLayoutParams().height = appBarHeight;
        appbar.setLayoutParams(appbar.getLayoutParams());

        int transparentColor = ContextCompat.getColor(this, android.R.color.transparent);
        //collapsingToolbarLayout.setCollapsedTitleTextColor(transparentColor);
        collapsingToolbarLayout.setExpandedTitleColor(transparentColor);



        circularImageView.setVisibility(View.VISIBLE);
        blurredImageView.setVisibility(View.VISIBLE);
        if(!Strings.isNullOrEmpty(project.getPreviewVideoImageUrl()))
        {
            Picasso.with(this).load(project.getPreviewVideoImageUrl()).into(target);
        }else{
            Picasso.with(this).load(R.drawable.unknown_video_large).into(target);
        }

        categoryTextView.setText(project.getCategoryName());
        zipCodeTextView.setText(project.getAddressOrZipCode());
        playVideoImageView.bringToFront();
    }

    @Override
    public void requestForQuoteWasSuccessful(ProfessionalRef professional) {
        advertisersAdapter.requestForQuoteWasSuccessful(professional);
    }


    @UiThread
    @Override
    public void showLoadingIndicator() {
        projectProfessionalsEmptySwipeRefreshLayout.setRefreshing(true);
        projectProfessionalsSwipeRefreshLayout.setRefreshing(true);
    }

    @UiThread
    @Override
    public void hideLoadingIndicator() {
        projectProfessionalsEmptySwipeRefreshLayout.setRefreshing(false);
        projectProfessionalsSwipeRefreshLayout.setRefreshing(false);
    }


    @UiThread
    @Override
    public void onAdvertiserActionExecuted(ProfessionalRef advertiser) {
        Toast.makeText(this, "calling the professional", Toast.LENGTH_LONG).show();
    }

    @UiThread
    @Override
    public void updateProjectDetails(ProjectEntity project){

    }

    @UiThread
    void advertiserTapped(ProfessionalRef advertiser, Bundle bundle){
        presenter.showAdvertiser(advertiser, bundle);
    }

    @Click(R.id.playVideoImageView)
    void playVideoTapped(){
       presenter.playVideo();
    }

    @Override
    public String getViewTitle() {
        return "PROJECT DETAILS";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unbindView();
    }
    @Override
    public void onStop() {
        super.onStop();
        presenter.shutDown();
    }


    @Override
    public void onAdvertiserTapped(ProfessionalRef professional, Bundle bundle) {
        advertiserTapped(professional, bundle);
    }

    @Override
    public void onAdvertiserActionTapped(ProfessionalRef professional) {
        advertiserActionTapped(professional);
    }

    @Override
    public void onBookQuoteTapped(ProfessionalRef professional) {
        presenter.bookQuote(professional);
    }

    @Override
    public void onRequestQuoteTapped(ProfessionalRef professional) {
        presenter.requestForQuote(professional);
    }
}
