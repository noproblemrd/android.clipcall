package it.clipcall.consumer.projects.support;

import android.content.Context;
import android.view.ViewGroup;

import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.views.CallHistoryItemView_;
import it.clipcall.consumer.projects.views.HistoryItemView;
import it.clipcall.consumer.projects.views.PaymentHistoryItemView_;
import it.clipcall.consumer.projects.views.QuoteHistoryItemView_;
import it.clipcall.consumer.projects.views.VideoChatHistoryItemView_;
import it.clipcall.infrastructure.ui.lists.RecyclerViewAdapterBase;
import it.clipcall.infrastructure.ui.lists.ViewWrapper;

/**
 * Created by dorona on 30/03/2016.
 */
public class HistoryItemAdapter extends RecyclerViewAdapterBase<HistoryItem,HistoryItemView> {

    private final boolean isProMode;
    private final Context context;
    private final IHistoryItemListener historyItemListener;

    public HistoryItemAdapter(Context context, IHistoryItemListener historyItemListener, boolean isProMode){

        this.context = context;
        this.historyItemListener = historyItemListener;
        this.isProMode = isProMode;
    }

    @Override
    protected HistoryItemView onCreateItemView(ViewGroup parent, int viewType) {
        HistoryItemView result = null;
        if(viewType == 0){
            result = CallHistoryItemView_.build(context);
        }
        else if(viewType == 1){
            result = PaymentHistoryItemView_.build(context);
        }else if(viewType == 2){
            result = VideoChatHistoryItemView_.build(context);
        }else if(viewType == 3){
            result = QuoteHistoryItemView_.build(context);
        }

        return result;
    }

    @Override
    public void onBindViewHolder(ViewWrapper<HistoryItemView> holder, int position) {
        HistoryItemView view = holder.getView();
        HistoryItem historyItem = items.get(position);
        view.bind(historyItem, historyItemListener, isProMode);
    }

    @Override
    public int getItemViewType(int position) {
        HistoryItem item = items.get(position);
        return item.getItemTypeValue();
    }

}
