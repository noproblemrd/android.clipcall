package it.clipcall.consumer.projects.models;

/**
 * Created by dorona on 13/03/2016.
 */
public class VideoHistoryItem extends VideoHistoryItemBase {

    @Override
    public boolean isProjectVideo() {
        return true;
    }
}