package it.clipcall.consumer.projects.presenters;

import android.os.Bundle;
import android.os.Parcelable;

import com.google.common.base.Strings;
import com.layer.sdk.changes.LayerChangeEvent;

import org.parceler.Parcels;
import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.common.chat.services.tasks.NavigateToChatTask;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.consumer.payment.services.tasks.RequestQuoteTask;
import it.clipcall.consumer.profile.models.CustomerProfile;
import it.clipcall.consumer.projects.controllers.ProjectAdvertiserController;
import it.clipcall.consumer.projects.models.Advertiser;
import it.clipcall.consumer.projects.models.AdvertiserData;
import it.clipcall.consumer.projects.models.ProfessionalAccount;
import it.clipcall.consumer.projects.models.ProfessionalData;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.consumer.projects.views.IProjectAdvertiserAboutView;
import it.clipcall.consumer.vidoechat.models.VideoChatContext;
import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.professional.leads.models.Constants;
import it.clipcall.professional.profile.models.Category;

/**
 * Created by dorona on 20/01/2016.
 */
@PerActivity
public class ProjectAdvertiserAboutPresenter extends PresenterBase<IProjectAdvertiserAboutView>  {


    private final ILogger logger = LoggerFactory.getLogger(ProjectAdvertiserAboutPresenter.class.getSimpleName());

    private final ProjectAdvertiserController controller;
    private final RoutingService routingService;
    private final RouteParams routeParams;
    private final EventAggregator eventAggregator;
    private final RequestQuoteTask requestQuoteTask;

    private final NavigateToChatTask navigateToChatTask;

    private String projectId;
    private String advertiserId;



    private ProfessionalData professionalData;

    @Inject
    public ProjectAdvertiserAboutPresenter(ProjectAdvertiserController controller, RoutingService routingService, RouteParams routeParams, EventAggregator eventAggregator, RequestQuoteTask requestQuoteTask, NavigateToChatTask navigateToChatTask) {
        this.controller = controller;
        this.routingService = routingService;
        this.routeParams = routeParams;
        this.eventAggregator = eventAggregator;
        this.requestQuoteTask = requestQuoteTask;
        this.navigateToChatTask = navigateToChatTask;
    }


    public void setProfessionalData(ProfessionalData professionalData){
        this.professionalData = professionalData;

        Advertiser advertiser = professionalData.getAdvertiser();
        AdvertiserData advertiserData = professionalData.getData();
        if(advertiserData == null)
            return;


        view.setAddress(advertiserData.getFullAddress());
        view.setBusinessDescription(advertiserData.getBusinessDescription());

        view.setMyLoveCount(advertiser.getMyLove());

        List<Category> categories = professionalData.getData().getCategories();
        StringBuilder builder = new StringBuilder();
        for (Category category : categories)
        {
            builder.append(category.getCategoryName() + " // ");
        }
        builder.setLength(builder.length() - 4);
        view.setCategories(builder.toString());
        view.showProfessionalData();
        ProfessionalRef professional = controller.getProfessional(projectId, advertiserId);
        if(professional == null)
            return;

        view.showTotalUnreadMessages(professional.getTotalUnreadMessages());
        view.showRequestQuote(professional.canRequestQuote());

    }

    @Override
    public void initialize() {
        //view.setChatActionEnabled(false);
        EventBus.getDefault().register(this);



    }

    @Override
    public void shutDown() {
        EventBus.getDefault().unregister(this);
    }

    public void likeAdvertiser() {

        boolean success = this.controller.likeAdvertiser(projectId,advertiserId);
        if(!success)
            return;


        Advertiser advertiser = this.controller.getAdvertiser(projectId,advertiserId);
        if(advertiser == null)
            return;

        view.setMyLoveCount(advertiser.getMyLove());

        EventBus.getDefault().post(advertiser);
    }

    public void callAdvertiser() {
        eventAggregator.publish(new ApplicationEvent(ConsumerEventNames.ProjectTapCall));
        boolean success = this.controller.callAdvertiser(projectId, advertiserId);

        if(success){
            view.notifyCallingProfessional();
        }
        view.enableActions(true);
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public void setAdvertiserId(String advertiserId) {
        this.advertiserId = advertiserId;
    }

    public void startRemoteHouseCall() {
        eventAggregator.publish(new ApplicationEvent(ConsumerEventNames.ProjectTapVideoChat));
        logger.debug("startRemoteHouseCall");
        view.enableActions(false);
        VideoSessionData videoSessionData = this.controller.createVideoSession(projectId, advertiserId);
        if(videoSessionData == null){
            logger.debug("startRemoteHouseCall failed. videoSession is null");
            view.showCannotStartVideoChat();
            view.enableActions(true);
            return;
        }

        if(professionalData == null){
            logger.debug("startRemoteHouseCall failed. professionalData is null");
            view.showCannotStartVideoChat();
            view.enableActions(true);
            return;
        }
        startRemoteHouseCallCore(videoSessionData);
        view.enableActions(true);
    }

    @RunOnUiThread
    private void startRemoteHouseCallCore(VideoSessionData videoSessionData) {
        connectToVideoSessionInBackground(videoSessionData);
        initializePublisher();
        navigateToVideoChat(videoSessionData);
    }

    private void connectToVideoSessionInBackground(VideoSessionData videoSessionData){
        logger.debug("connectToVideoSessionInBackground with session id %s",videoSessionData.getSessionId());
        controller.connectToVideoChatInBackground(videoSessionData);
    }

    private void initializePublisher(){
        String consumerNameOrPhoneNumber = controller.getCustomerNameOrPhoneNumber();
        logger.debug("setVideoChatContext with publisherName %s",consumerNameOrPhoneNumber);
        controller.initializePublisher(consumerNameOrPhoneNumber);
    }

    private void navigateToVideoChat(VideoSessionData videoSession){
        logger.debug("navigateToVideoChat with session id %s",videoSession.getSessionId());
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.bundle = new Bundle();

        VideoChatContext videoChatContext = new VideoChatContext();
        videoChatContext.videoSessionData = videoSession;
        videoChatContext.isInitiatingCall = true;
        videoChatContext.profileImageUrl = professionalData.getAdvertiser().getImageUrl();
        videoChatContext.profileName = professionalData.getAdvertiser().getName();

        navigationContext.bundle.putParcelable("videoChatContext",  Parcels.wrap(videoChatContext));
        routingService.routeTo(Routes.ConsumerVideoChatCalling, navigationContext);
        view.enableActions(true);
    }

    public void payThePro() {
        eventAggregator.publish(new ApplicationEvent(ConsumerEventNames.PayProTapped));
        RoutingContext routingContext = view.getRoutingContext();
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = routingContext;
        Advertiser advertiser = professionalData.getAdvertiser();
        ProfessionalAccount professionalAccount = new ProfessionalAccount(advertiser.getLeadAccountId(),advertiserId,advertiser.getName());
        professionalAccount.setProjectId(projectId);
        routeParams.reset();
        routeParams.setParam("projectProfessional", professionalAccount);
        routingService.routeTo(Routes.PayTheProRoute, navigationContext);
    }

    public void navigateToChat() {
        eventAggregator.publish(new ApplicationEvent(ConsumerEventNames.ChatTapped));
        view.setChatActionEnabled(false);
        NavigationContext navigationContext = new NavigationContext( view.getRoutingContext());
        Advertiser advertiser = professionalData.getAdvertiser();
        String professionalName = advertiser.getName();
        String professionalId = advertiser.getCustomerId();
        String professionalProfileImageUrl = advertiser.getImageUrl();


        navigationContext.bundle = new Bundle();

        CustomerProfile consumerProfile = controller.getConsumerProfile();

        ProjectEntity project = professionalData.getProject();
        String customerNameOrPhoneNumber = consumerProfile.getName();
        if(Strings.isNullOrEmpty(customerNameOrPhoneNumber)){
            customerNameOrPhoneNumber = project.getPrivate()  ? controller.getConsumerPhoneNumber() : Constants.AnonymousUser;
        }
        String customerProfileImageUrl = consumerProfile.getImageUrl();
        String customerId = controller.getCustomerId();



        Parcelable sender = Parcels.wrap(new ChatParticipant(customerId, customerProfileImageUrl, customerNameOrPhoneNumber, false));
        Parcelable receiver =  Parcels.wrap(new ChatParticipant(professionalId, professionalProfileImageUrl, professionalName, true));


        navigationContext.bundle.putParcelable("sender",sender);
        navigationContext.bundle.putParcelable("receiver",receiver);
        navigationContext.bundle.putString("conversationId",advertiser.getLeadAccountId());
        navigationContext.addParameter("advertiserId", advertiserId);
        navigationContext.addParameter("projectId", projectId);
        navigationContext.bundle.putBoolean("canSendQuote",false);
        routingService.routeTo(Routes.ConsumerChatRoute, navigationContext);
        view.setChatActionEnabled(true);
    }


    @Subscriber(mode = ThreadMode.ASYNC)
    void onChatEvent(LayerChangeEvent layerChangeEvent) {
        ProfessionalRef professional = controller.getProfessional(projectId, advertiserId);
        if(professional == null)
            return;

        view.showTotalUnreadMessages(professional.getTotalUnreadMessages());
    }

    public void loadUnreadMessages() {
        ProfessionalRef professional = controller.getProfessional(projectId, advertiserId);
        if(professional == null)
            return;

        view.showTotalUnreadMessages(professional.getTotalUnreadMessages());
    }

    public void requestQuote() {
        view.showRequestQuoteInProgress(true);



        final ProfessionalRef professional = controller.getProfessional(projectId, advertiserId);
        if(professional == null)
        {
            view.showRequestQuoteInProgress(false);
            return;
        }

        eventAggregator.publish(new ApplicationEvent(ConsumerEventNames.ProjectProAskQuote, professional));

        boolean success  = requestQuoteTask.execute(professionalData.getProject(), professional);
        view.showRequestQuoteInProgress(false);
        view.showQuoteRequestWasSuccessful(success);
        if(success){
            navigateToChatTask.execute(professionalData.getProject(),professional,view.getRoutingContext());
        }
    }
}
