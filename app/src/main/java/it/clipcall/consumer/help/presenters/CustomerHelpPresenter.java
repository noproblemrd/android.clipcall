package it.clipcall.consumer.help.presenters;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.help.views.ICustomerHelpView;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;

public class CustomerHelpPresenter extends PresenterBase<ICustomerHelpView> {

    private final RoutingService routingService;

    @Inject
    public CustomerHelpPresenter(RoutingService routingService) {
        this.routingService = routingService;
    }


    public void navigateToTutorial(){
        NavigationContext context = new NavigationContext();
        context.routingContext = view.getRoutingContext();
        routingService.routeTo(Routes.ConsumerTutorialRoute,context);
    }

    public void navigateToFaq(){
        NavigationContext context = new NavigationContext();
        context.routingContext = view.getRoutingContext();
        context.addParameter("url","https://storage.googleapis.com/clipcallterms/ClipCall_FAQ.pdf");
        context.addParameter("title","FAQs");

        routingService.routeTo(Routes.ConsumerFaqRoute,context);
    }

    public void navigateToContactUs() {

    }
}
