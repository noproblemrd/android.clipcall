package it.clipcall.consumer.help.fragments;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.help.presenters.CustomerHelpPresenter;
import it.clipcall.consumer.help.views.ICustomerHelpView;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.routing.models.RoutingContext;

@EFragment(R.layout.consumer_help_fragment)
public class ConsumerHelpFragment extends BaseFragment implements ICustomerHelpView {


    private boolean isNavigating;

    private String[] actions = new String[]{
            "Tutorial",
            "FAQs",
            "Contact us"
    };

    @Inject
    CustomerHelpPresenter presenter;

    @ViewById
    ListView helpListView;


    @AfterViews
    void afterAboutViewLoaded(){
        presenter.bindView(this);
        helpListView.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.about_nav_item, actions));
        isNavigating = false;

        helpListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                navigateTo(actions[position]);
            }
        });
    }

    void navigateTo(String routeName){
        if(isNavigating)
            return;

        isNavigating = true;

        if(routeName.equals("Tutorial"))
        {
            presenter.navigateToTutorial();
            isNavigating = false;
            return;
        }

        if(routeName.equals("FAQs")){
            presenter.navigateToFaq();
            isNavigating = false;
            return;
        }

        if(routeName.equals("Contact us")){
            final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

/* Fill it with Data */
            emailIntent.setType("plain/text");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"support@clipcall.it"});
//            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject");
//            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Text");

/* Send it off to the Activity-Chooser */
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            isNavigating = false;
            return;

            //presenter.navigateToContactUs();

        }
    }


    @Override
    protected String getTitle() {
        return "Help";
    }

    @Override
    public RoutingContext getRoutingContext() {
        return new RoutingContext(getActivity());
    }

    @Override
    protected int getMenuItemId() {
        return R.id.nav_help_consumer;
    }
}
