package it.clipcall.consumer.help.fragments;


import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import it.clipcall.R;
import it.clipcall.infrastructure.fragments.BaseFragment;

@EFragment(R.layout.consumer_tutorial_item_fragment)
public class ConsumerTutorialItemFragment extends BaseFragment {

    @FragmentArg
    int tutorialImageResourceId;
    @FragmentArg
    int tutorialTextResourceId;

    @FragmentArg
    int backgroundColorResourceId;

    @ViewById
    TextView tutorialTextView;
    @ViewById
    ImageView tutorialImageView;
    @ViewById
    ViewGroup containerView;

    @AfterViews
    void afterViews(){
        tutorialTextView.setText(getResources().getString(tutorialTextResourceId));
        containerView.setBackgroundResource(backgroundColorResourceId);
    }
}