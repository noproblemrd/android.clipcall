package it.clipcall.consumer.help.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.ToxicBakery.viewpager.transforms.CubeOutTransformer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.R;
import it.clipcall.consumer.help.fragments.ConsumerTutorialItemFragment_;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;

@EActivity(R.layout.consumer_tutorial_activity)
public class ConsumerTutorialActivity extends BaseActivity implements IHasToolbar{

    @ViewById
    Toolbar toolbar;

    @ViewById
    ViewPager viewPager;

    private PagerAdapter pagerAdapter;

    @AfterViews
    void afterViews(){
        pagerAdapter = new TutorialPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(pagerAdapter.getCount() - 1);
        //viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        viewPager.setPageTransformer(true, new CubeOutTransformer());
    }

    @Override
    public String getViewTitle() {
        return "TUTORIAL";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the backButtonTapped step.
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }
    }

    private class TutorialPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> tutorialItems = new ArrayList<>();

        public TutorialPagerAdapter(FragmentManager fm) {
            super(fm);
            //tutorialItems.add(ConsumerTutorialItemFragment_.builder().tutorialImageResourceId(R.drawable.app_loading_screen1).backgroundColorResourceId(R.color.consumer_tutorial_page_1).tutorialTextResourceId(R.string.consumer_tutorial_1_text).build());
            //tutorialItems.add(ConsumerTutorialItemFragment_.builder().tutorialImageResourceId(R.drawable.app_loading_screen4).backgroundColorResourceId(R.color.consumer_tutorial_page_2).tutorialTextResourceId(R.string.consumer_tutorial_2_text).build());
            //tutorialItems.add(ConsumerTutorialItemFragment_.builder().tutorialImageResourceId(R.drawable.app_loading_screen5).backgroundColorResourceId(R.color.consumer_tutorial_page_3).tutorialTextResourceId(R.string.consumer_tutorial_3_text).build());
            Log.d("Foo","tutorial page adapter created with total of "+ tutorialItems.size() + " items");
        }

        @Override
        public Fragment getItem(int position) {
            Log.d("Foo", "getting tutorial item at position: "+ position);
            return tutorialItems.get(position);
        }

        @Override
        public int getCount() {
            return tutorialItems.size();
        }
    }

}
