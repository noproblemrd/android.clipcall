package it.clipcall.common.controllers;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.consumer.vidoechat.models.VideoInvitationData;
import it.clipcall.consumer.vidoechat.services.VideoChatManager;

/**
 * Created by dorona on 28/12/2015.
 */
@Singleton
public class SplashScreenController {

    private final VideoChatManager videoChatManager;
    private final AuthenticationManager authenticationManager;


    @Inject
    public SplashScreenController(VideoChatManager videoChatManager, AuthenticationManager authenticationManager){

        this.videoChatManager = videoChatManager;
        this.authenticationManager = authenticationManager;
    }


    public VideoInvitationData getInvitationData(String invitationId) throws IOException {
        VideoInvitationData invitationData = this.videoChatManager.getVideoInvitationData(invitationId);
        return invitationData;
    }

    public String getPhoneNumber() {
        return authenticationManager.getPhoneNumber();
    }
}
