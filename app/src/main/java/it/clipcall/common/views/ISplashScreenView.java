package it.clipcall.common.views;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 28/12/2015.
 */
public interface ISplashScreenView extends IView {

    void resolveInitialRoute();
}
