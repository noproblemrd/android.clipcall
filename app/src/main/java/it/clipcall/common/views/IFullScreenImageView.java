package it.clipcall.common.views;

import android.graphics.Bitmap;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 28/12/2015.
 */
public interface IFullScreenImageView extends IView {

    void showImage(Bitmap image);
}
