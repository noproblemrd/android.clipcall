package it.clipcall.common.models.entities;

/**
 * Created by micro on 11/13/2015.
 */
public class BusinessDescription {

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
