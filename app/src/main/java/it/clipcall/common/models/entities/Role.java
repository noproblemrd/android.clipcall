package it.clipcall.common.models.entities;

import java.io.Serializable;

import it.clipcall.common.models.Profile;

/**
 * Created by omega on 2/20/2016.
 */
public abstract class Role implements Serializable {

    public abstract Profile getProfile();

    public abstract void setProfile(Profile profile);

    public abstract String getIdentifier();

    public abstract void setIdentifier(String identifier);

}
