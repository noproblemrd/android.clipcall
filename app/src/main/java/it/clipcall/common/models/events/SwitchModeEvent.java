package it.clipcall.common.models.events;

/**
 * Created by dorona on 03/07/2016.
 */
public class SwitchModeEvent {

    private final boolean isConsumerMode;
    private final String userId;

    public SwitchModeEvent(boolean isConsumerMode, String userId){

        this.isConsumerMode = isConsumerMode;
        this.userId = userId;
    }

    public boolean getIsConsumerMode() {
        return isConsumerMode;
    }

    public String getUserId() {
        return userId;
    }
}
