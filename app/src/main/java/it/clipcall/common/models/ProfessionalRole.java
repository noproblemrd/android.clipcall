package it.clipcall.common.models;

import it.clipcall.common.models.entities.Role;

/**
 * Created by omega on 2/20/2016.
 */
public class ProfessionalRole extends Role {

    private String advertiserId;



    @Override
    public Profile getProfile() {
        return null;
    }

    @Override
    public void setProfile(Profile profile) {

    }

    @Override
    public String getIdentifier() {
        return advertiserId;
    }

    @Override
    public void setIdentifier(String identifier) {
        this.advertiserId = identifier;
    }
}
