package it.clipcall.common.models;

/**
 * Created by dorona on 10/01/2016.
 */
public class ApplicationEvent {

    private final String name;

    public Object getData() {
        return data;
    }

    public String getName() {
        return name;
    }

    private final Object data;

    public ApplicationEvent(String name, Object data){
        this.name = name;
        this.data = data;
    }

    public ApplicationEvent(String name){
        this(name,null);
    }




}
