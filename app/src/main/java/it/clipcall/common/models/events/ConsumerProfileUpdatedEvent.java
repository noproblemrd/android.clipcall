package it.clipcall.common.models.events;

import it.clipcall.consumer.profile.models.CustomerProfile;

/**
 * Created by dorona on 03/07/2016.
 */
public class ConsumerProfileUpdatedEvent {

    private final CustomerProfile customerProfile;

    public ConsumerProfileUpdatedEvent(CustomerProfile customerProfile){

        this.customerProfile = customerProfile;
    }

    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }
}
