package it.clipcall.common.models.entities;

/**
 * Created by micro on 11/14/2015.
 */
public class AuthenticationData {
    private String token;
    private String customerId;
    private String supplierId;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }
}
