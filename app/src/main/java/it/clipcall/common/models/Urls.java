package it.clipcall.common.models;

/**
 * Created by dorona on 15/06/2016.
 */
public class Urls {

    public static final String ConsumerMissingProfileImageUrl = "https://mobileclipcall2.s3.amazonaws.com/media/0/consumer_profile_logo_placeholder_blue.png";
}
