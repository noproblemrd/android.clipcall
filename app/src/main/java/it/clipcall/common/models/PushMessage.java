package it.clipcall.common.models;

/**
 * Created by dorona on 26/04/2016.
 */
public class PushMessage {
    String title;
    String message;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
