package it.clipcall.common.models;

/**
 * Created by dorona on 08/03/2016.
 */
public class Device {

    public String uid;
    public String deviceOs;
    public String deviceName;
    public String osVersion;
    public String npAppVersion;
    public boolean isDebug;

}
