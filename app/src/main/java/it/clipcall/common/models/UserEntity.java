package it.clipcall.common.models;


import com.google.common.base.Strings;

import java.io.Serializable;

import it.clipcall.infrastructure.responses.AdvertiserData;

public class UserEntity implements Serializable {


    private String mode;
    private String customerId;
    private String token;
    private String advertiserId;
    private String phoneNumber;
    private boolean isRegistered;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAdvertiserId() {
        return advertiserId;
    }

    public String getUserId(){
        if (isInProfessionalMode())
            return getAdvertiserId();

        return getCustomerId();
    }

    public boolean isAuthenticated(){
        return  getToken() != null && getCustomerId() != null;
    }


    public void setAdvertiserId(String advertiserId) {
        this.advertiserId = advertiserId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(boolean registered) {
        isRegistered = registered;
    }

    public void switchToProfessionalMode(AdvertiserData advertiserData) {
        setAdvertiserId(advertiserData.getSupplierId());
        setToken(advertiserData.getToken());
        setMode(ApplicationModes.Professional);
    }

    public boolean isInProfessionalMode(){
        return ApplicationModes.Professional.equals(getMode() );
    }


    public void switchToConsumerMode(){
        setMode(ApplicationModes.Consumer);
    }


    public AdvertiserData getAdvertiserData() {
        String consumerId = getCustomerId();
        if (Strings.isNullOrEmpty(consumerId))
            return null;

        String advertiserId = getAdvertiserId();
        if(Strings.isNullOrEmpty(advertiserId))
            return null;

        String token = getToken();
        if(Strings.isNullOrEmpty(token))
            return null;

        AdvertiserData result = new AdvertiserData();
        result.setToken(token);
        result.setSupplierId(advertiserId);
        return result;
    }
}

