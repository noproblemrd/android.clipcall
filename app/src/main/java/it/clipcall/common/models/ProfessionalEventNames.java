package it.clipcall.common.models;

/**
 * Created by dorona on 24/04/2016.
 */
public class ProfessionalEventNames {

    //TODO move to application's common events
    //phone validation
    public static final String EnterPhone = "Validation-EnterPhone";
    public static final String ValidatePhoneTapped = "Validation-VALIDATE";
    public static final String ValidatePhoneTextMe = "Validation-TextMe";
    public static final String ValidatePhoneCallMe = "Validation-CallMe";

    //professional registration
    public static final String RegistrationEnterBusinessName = "AdvReg1-EnterName";
    public static final String RegistrationAddCategory = "AdvReg1-AddCat";
    public static final String RegistrationRemoveCategory = "AdvReg1-RemoveCat";
    public static final String RegistrationSetWebsite = "AdvReg1-SetWebsite";
    public static final String RegistrationSetEmail = "AdvReg1-SetEmail";
    public static final String RegistrationNext1Tapped = "AdvReg1-NEXT";
    public static final String RegistrationSelectProfileImage = "AdvReg2-UploadPic";
    public static final String RegistrationEnterBusinessDescription = "AdvReg2-AddDesc";
    public static final String RegistrationEnterLicenseNumber = "AdvReg2-EnterLicenseNumber";
    public static final String RegistrationNext2Tapped = "AdvReg2-NEXT";
    public static final String RegistrationEnterBusinessAddress = "AdvReg3-EditAdd";
    public static final String RegistrationSelectCoverArea = "AdvReg3-EditArea";
    public static final String RegistrationCompleted = "AdvReg3-LendOn Leads";
    public static final String NewProjectTakeProject= "GetLeadIWant";
    public static final String NewProjectAgreeToTerms = "GetLeadAgreeTerms";
    public static final String NewProjectPlayVideo = "GetLeadPlayVideo";
    public static final String NewProjectDisagreeToTerms = "GetLeadDisAgreeTerms";
    public static final String NewProjectCallCustomer = "GetLeadICall";
    public static final String NewProjectTextCustomer = "GetLeadIText";
    public static final String NewProjectBackTapped = "GetLeadIClose";





    public static final String GetLeadQuote = "GetLeadQuote";
    public static final String GetLeadTextMe = "GetLeadTextMe";
    public static final String GetLeadQuotePrice   = "GetLeadQuotePrice  ";
    public static final String GetLeadQuoteDate  = "GetLeadQuoteDate ";
    public static final String GetLeadQuoteNote  = "GetLeadQuoteNote ";
    public static final String GetLeadQuoteSend  = "GetLeadQuoteSend ";
    public static final String ProPushBooking  = "ProPushBooking ";

}
