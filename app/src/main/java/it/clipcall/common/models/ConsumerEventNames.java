package it.clipcall.common.models;

public class ConsumerEventNames{
    //Find pro
    public static final String ChooseCategory = "FindPro-ChooseCat";
    public static final String ChooseZipCode = "FindPro-EnterZip";
    public static final String LandingOnFindProPage = "Find a pro - Landing on \"Find a pro\" page";
    public static final String ClipCallItTapped = "FindPro-CLIPCALLIT";
    public static final String FIND_PRO_START_VIDEO_RECORDING = "FindPro-STARTVID";
    public static final String FIND_PRO_PAUSE_VIDEO_RECORDING = "FindPro-PAUSEVID";
    public static final String FIND_PRO_CONTINUE_VIDEO_RECORDING = "FindPro-CONTINUEVID";
    public static final String FIND_PRO_CLOSE_VIDEO_RECORDING = "FindPRO-CLOSEVID";
    public static final String FIND_PRO_SENDING_VIDEO = "FindPRO-SENDVID";
    public static final String FIND_PRO_LAND_ON_PROJECTS_SUCCESS = "FindPro-LandOn-Projects - SUCCESS";
    public static final String FIND_PRO_LAND_ON_PROJECTS_FAILURE = "FindPro-LandOn-Projects - FAILURE";

    //video invitations
    public static final String VID_CHAT_CONS_TAP_START = "VidChat-Cons-TapStart";
    public static final String VID_CHAT_CONS_TAP_START_REDIRECT2_VALID = "VidChat-Cons-TapStart-Redirect2Valid";
    public static final String INVITATION_SHOWED = "InvitationShowed";
    public static final String INVITATION_EXPIRED = "InvitationExpired";

    public static final String ClipCallItInvitation = "FindPro-CLIPCALLIT-INV";
    public static final String CREATE_NEW_PROJECT = "Projects-AddNew";
    public static final String ApplicationInstalled = "APP_INSTALLED";
    public static final String MenuBillingBonus = "Menu-Billing-Bonus";
    public static final String MenuBillingPay = "Menu-Billing-Pay";
    public static final String ProjectTapCall = "Project-SPro-TapCall";
    public static final String ProjectTapVideoChat = "Project-SPro-TapVideoChat";
    public static final String PayProTapped = "Project-SPro-TapPay";
    public static final String ChatTapped = "Project-SPro-TapChat";
    public static final String ClaimBonus = "Claim-Bonus";



    public static final String UserPushQuote = "UserPushQuote";
    public static final String UserQuoteLandOn = "UserQuoteLandOn";
    public static final String UserQuoteBookNow = "UserQuoteBookNow";
    public static final String UserQuoteChat = "UserQuoteChat";
    public static final String ProjectBookQuote = "Project-Book";
    public static final String ProjectAskQuote = "Project-AskQuote";
    public static final String ProjectProAskQuote = "Project-SPro-AskQuote";
}