package it.clipcall.common.videochat.models;


import org.parceler.Parcel;

@Parcel
public class VideoChatData {

    public String callingToName;
    public String callingToProfileImageUrl;
    public String sessionId;
    public String token;
    public String apiKey;

}
