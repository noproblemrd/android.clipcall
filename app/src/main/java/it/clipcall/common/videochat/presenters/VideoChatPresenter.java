package it.clipcall.common.videochat.presenters;


import javax.inject.Inject;

import it.clipcall.consumer.vidoechat.controllers.ConsumerVideoChatController;
import it.clipcall.consumer.vidoechat.views.IConsumerStartVideoChatView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;

/**
 * Created by dorona on 28/12/2015.
 */
@PerActivity
public class VideoChatPresenter extends PresenterBase<IConsumerStartVideoChatView> {
    private final ConsumerVideoChatController controller;

    @Inject
    public VideoChatPresenter(ConsumerVideoChatController controller){
        this.controller = controller;
    }
}
