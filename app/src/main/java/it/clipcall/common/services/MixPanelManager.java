package it.clipcall.common.services;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.environment.Environment;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.support.json.JsonBuilder;

/**
 * Created by dorona on 10/01/2016.
 */
@Singleton
public class MixPanelManager {
    private final MixpanelAPI mixpanelAPI;
    private final JsonBuilder jsonBuilder;

    private static final ILogger logger = LoggerFactory.getLogger(MixPanelManager.class.getSimpleName());

    @Inject
    public MixPanelManager(MixpanelAPI mixpanelAPI, JsonBuilder jsonBuilder) {
        this.mixpanelAPI = mixpanelAPI;
        this.jsonBuilder = jsonBuilder;
    }

    public void initialize(){
        EventBus.getDefault().register(this);
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onEventRecieved(ApplicationEvent event) {
        logger.debug("mixPanel:onEventRecieved event - "+event.getName());
        Map<String,Object> props = jsonBuilder.buildMap(event.getData());
        if (Environment.AnalyticsEnabled) {
            mixpanelAPI.trackMap(event.getName(), props);
            //mixpanelAPI.flush();
        }
    }
}
