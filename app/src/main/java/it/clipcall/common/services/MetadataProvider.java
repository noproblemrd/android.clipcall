package it.clipcall.common.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.consumer.projects.services.ICustomerProjectsService;
import it.clipcall.infrastructure.support.JsonLoader;
import retrofit2.Response;

/**
 * Created by dorona on 01/12/2015.
 */
@Singleton
public class MetadataProvider {

    private final JsonLoader loader;
    private final ICustomerProjectsService customerProjectsService;
    private final List<Category> categories = new ArrayList<>();

    @Inject
    public MetadataProvider(JsonLoader loader, ICustomerProjectsService customerProjectsService){
        this.loader = loader;
        this.customerProjectsService = customerProjectsService;
    }

//    public List<Category> getAvailableCategories(){
//        String categoriesJson = loader.loadJSONFromAsset("categories.json");
//        List<Category> leads = new ArrayList<Category>();
//        if(categoriesJson != null){
//            Type listType = new TypeToken<ArrayList<Category>>(){}.getType();
//            List<Category> jsonLeads = new GsonBuilder().create().fromJson(categoriesJson, listType);
//            leads.addAll(jsonLeads);
//        }
//        return leads;
//    }

    public String[] getToolTips(){
        String[] items =  new String[]{
                "Ensure no complaints have been filled.",
                "It appears you are connected to the Internet, but you might want to try to reconnect to the internet",
                "Plumbers should have insurance",
                "Check online with your state",
                "He should have at least 5-10 years",
        };

        return items;
    }

    public List<Category> getAvailableCategories(){

        if(categories.size()>0)
            return categories;

        try {
            Response<List<Category>> tipsResponse = customerProjectsService.getAvailableCategories().execute();
            if(tipsResponse == null)
                return new ArrayList<>();

            List<Category> availableCategories = tipsResponse.body();
            if(availableCategories == null || availableCategories.size() == 0)
                return new ArrayList<>();

            categories.addAll(availableCategories);
            return availableCategories;

        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    public Category getCategoryById(final String categoryId){
        Category category = Lists.firstOrDefault(this.categories, new ICriteria<Category>() {
            @Override
            public boolean isSatisfiedBy(Category candidate) {
                return candidate.getCategoryId().equals(categoryId);
            };
        });
        return category;
    }
}
