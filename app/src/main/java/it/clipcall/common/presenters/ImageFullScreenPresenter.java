package it.clipcall.common.presenters;

import android.graphics.Bitmap;

import javax.inject.Inject;

import it.clipcall.common.views.IFullScreenImageView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.services.RoutingService;

@PerActivity
public class ImageFullScreenPresenter extends PresenterBase<IFullScreenImageView> {

    private final RoutingService routingService;

    private final RouteParams routeParams;
    @Inject
    public ImageFullScreenPresenter(RoutingService routingService, RouteParams routeParams){
        this.routingService = routingService;

        this.routeParams = routeParams;
    }

    @Override
    public void initialize() {
        Bitmap image = routeParams.getParam("image");
        view.showImage(image);
    }


}
