package it.clipcall.common.presenters;

import javax.inject.Inject;

import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.views.IView;

@PerActivity
public class DefaultRoutingPresenter extends PresenterBase<IView> {

    private final RoutingService routingService;
    private final RouteParams routeParams;
    @Inject
    public DefaultRoutingPresenter(RoutingService routingService, RouteParams routeParams){
        this.routingService = routingService;

        this.routeParams = routeParams;
    }

    public void routeToHome(){
        routingService.routeToHome(view.getRoutingContext());
    }
}
