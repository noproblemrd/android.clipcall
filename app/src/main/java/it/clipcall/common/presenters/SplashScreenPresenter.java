package it.clipcall.common.presenters;

import android.net.Uri;

import javax.inject.Inject;

import it.clipcall.common.controllers.SplashScreenController;
import it.clipcall.common.invitations.services.BranchIoManager;
import it.clipcall.common.views.ISplashScreenView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.services.IBranchIoInitListener;

@PerActivity
public class SplashScreenPresenter extends PresenterBase<ISplashScreenView> {

    private final RoutingService routingService;
    private final SplashScreenController controller;
    private final BranchIoManager branchIoManager;

    @Inject
    public SplashScreenPresenter(RoutingService routingService,BranchIoManager branchIoManager, SplashScreenController controller){
        this.routingService = routingService;
        this.controller = controller;
        this.branchIoManager = branchIoManager;
    }

    public void initializeSession(Uri uri){

        //EventBus.getDefault().post(new Event("Cons-App-Start",branchIoManager.getInvitationData()));
        branchIoManager.initSession(uri, view, new IBranchIoInitListener() {
            @Override
            public void onComplete() {
                view.resolveInitialRoute();
            }

            @Override
            public void onError() {
                view.resolveInitialRoute();
            }
        });
    }

    public void resolveInitialRoute(){
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();
        routingService.routeTo((Route)null, navigationContext);
    }

    public String getPhoneNumber() {
        return controller.getPhoneNumber();

    }
}
