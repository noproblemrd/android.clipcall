package it.clipcall.common.webview;

import android.util.Log;
import android.webkit.JavascriptInterface;

import it.clipcall.consumer.about.presenters.StripeWebViewPresenter;

/**
 * Created by dorona on 28/07/2016.
 */

public class ClipCallJavaScriptInterface {

    private StripeWebViewPresenter presenter;

    public ClipCallJavaScriptInterface(StripeWebViewPresenter presenter) {
        this.presenter = presenter;
    }

    @JavascriptInterface
    public void routeToHome(String input){
        Log.d("ClipCall","JavascriptInterface:routeToHome - " + input);
        presenter.onRedirect(input);
    }
}
