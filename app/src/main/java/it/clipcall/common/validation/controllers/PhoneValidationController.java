package it.clipcall.common.validation.controllers;

import org.simple.eventbus.EventBus;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.RandomNumberGenerator;
import it.clipcall.infrastructure.gcm.services.GcmTokenProvider;
import it.clipcall.infrastructure.requests.LoginRequest;
import it.clipcall.infrastructure.requests.PhoneValidationRequest;
import it.clipcall.infrastructure.services.ProfileManager;
import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.professional.validation.services.LoginEvent;

/**
 * Created by micro on 11/14/2015.
 */
@Singleton
public class PhoneValidationController {
    private final AuthenticationManager authenticationManager;
    private final ProfileManager profileManager;
    private final RandomNumberGenerator randomNumberGenerator;
    private final GcmTokenProvider gcmTokenProvider;

    private final PinCode generatedPinCode = new PinCode();
    private final PinCode enteredPinCode = new PinCode();
    private String validationReason;


    @Inject
    public PhoneValidationController(AuthenticationManager authenticationManager, ProfileManager profileManager, RandomNumberGenerator randomNumberGenerator, GcmTokenProvider gcmTokenProvider){

        this.authenticationManager = authenticationManager;
        this.profileManager = profileManager;
        this.randomNumberGenerator = randomNumberGenerator;
        this.gcmTokenProvider = gcmTokenProvider;
    }


    public boolean validatePhoneNumber(boolean viaCall){
        String pinCode = this.randomNumberGenerator.generate(4);
        generatedPinCode.fromString(pinCode);
        String phoneNumber = this.profileManager.getPhoneNumber();
        PhoneValidationRequest request = new PhoneValidationRequest();
        request.setPhone(phoneNumber);
        request.setCode(pinCode);
        request.setCall(viaCall);
        boolean validated = authenticationManager.validatePhoneNumber(request);
        if(!validated){
            generatedPinCode.reset();
            enteredPinCode.reset();
            return false;
        }

        return true;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.profileManager.setPhoneNumber(phoneNumber);
    }

    public String getPhoneNumber() {
        return this.profileManager.getPhoneNumber();
    }

    public void setDigitAtIndex(Integer digit, int index) {
        enteredPinCode.setDigitAtIndex(digit, index);
        if(!enteredPinCode.isFull())
            return;

    }

    public boolean isPinCodeFilled(){
        return this.enteredPinCode.isFull();
    }

    public boolean isCorrectPinCode() {

       /* try{
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        return this.generatedPinCode.equals(this.enteredPinCode);
    }

    public boolean login() {
        LoginRequest request = new LoginRequest();
        request.setPhoneNumber(this.profileManager.getPhoneNumber());
        request.setDeviceName(android.os.Build.DEVICE);
        request.setUid(gcmTokenProvider.getGcmToken());
        request.setDeviceOs("ANDROID");
        request.setNpAppVersion(null);
        request.setOsVersion(System.getProperty("os.version"));
        boolean loginSuccess = this.authenticationManager.login(request);
        if(!loginSuccess)
            return false;

        if(!authenticationManager.isInProfessionalMode()){

            EventBus.getDefault().post(new LoginEvent(authenticationManager.getUserId()));
            return true;
        }

        boolean success =  authenticationManager.switchToProfessionalMode();
        EventBus.getDefault().post(new LoginEvent(authenticationManager.getUserId()));
        return success;
    }

    public boolean isCustomerAuthenticated(){
        return this.authenticationManager.isCustomerAuthenticated();
    }

    public void resetPinCode() {
        this.enteredPinCode.reset();
    }

    public boolean switchToAdvertiser() {
        boolean switchedToAdvertiser = this.authenticationManager.switchToProfessionalMode();
        return switchedToAdvertiser;
    }

    public boolean isSupplierAuthenticated() {
        return this.authenticationManager.isProfessionalAuthenticated();
    }

    public void setValidationReason(String validationReason) {
        this.validationReason = validationReason;
    }

    public String getValidationReason() {
        return validationReason;
    }

    public boolean isValidVerificationCode(String smsMessage) {

        String actualMessage = smsMessage.replace("\n","");
        String expectedMessage = String.format("Your verification code is: %s", generatedPinCode.toString());
        return actualMessage.equals(expectedMessage);

    }

    public boolean isInProfessionalMode() {
        return authenticationManager.isInProfessionalMode();

    }

    private class PinCode{

        private final Integer[] values = new Integer[4];


        public void fromString(String pinCode){
            if(pinCode.length() != 4)
                return;

            values[0] = Integer.valueOf(pinCode.charAt(0)+"");
            values[1] = Integer.valueOf(pinCode.charAt(1)+"");
            values[2] = Integer.valueOf(pinCode.charAt(2)+"");
            values[3] = Integer.valueOf(pinCode.charAt(3)+"");

        }


        public boolean isFull(){
            for(int i=0; i<values.length; ++i)
                if(values[i] == null)
                    return false;

            return true;
        }

        public void reset(){
            for(int i=0; i<values.length; ++i)
                values[i] = null;
        }

        @Override
        public boolean equals(Object o) {
            if(!(o instanceof  PinCode))
                return false;

            PinCode other = (PinCode)o;

            for(int i=0; i<values.length; ++ i){
                if(!this.values[i].equals(other.values[i]))
                    return false;
            }

            return true;
        }

        public void setDigitAtIndex(Integer digit, int index) {
            if(index < 0 || index >= values.length)
                return;

            values[index] = digit;
        }

        @Override
        public String toString() {
            return String.format("%d%d%d%d",values[0],values[1],values[2],values[3]);
        }

    }
}
