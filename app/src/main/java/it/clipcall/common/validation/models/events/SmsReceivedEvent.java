package it.clipcall.common.validation.models.events;

/**
 * Created by dorona on 04/02/2016.
 */
public class SmsReceivedEvent {

    public SmsReceivedEvent(String message) {
        this.message = message;
    }

    private String message;

    public String getMessage() {
        return message;
    }
}
