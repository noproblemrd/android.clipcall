package it.clipcall.common.validation.presenters;

import com.google.common.base.Strings;

import org.simple.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ProfessionalEventNames;
import it.clipcall.common.validation.controllers.PhoneValidationController;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.pages.validation.IPhoneValidationView;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteNames;
import it.clipcall.infrastructure.routing.models.RoutingRulesContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.services.RoutingService;


/**
 * Created by micro on 11/14/2015.
 */
@PerActivity
public class PhoneValidationPresenter extends PresenterBase<IPhoneValidationView> {

    private final PhoneValidationController controller;
    private final RoutingService routingService;
    private final RoutingRulesContext routingRulesContext;
    private int timeToWait = 60;
    private Timer timer;


    @Inject
    public PhoneValidationPresenter(PhoneValidationController controller, RoutingService routingService, RoutingRulesContext routingRulesContext){
        this.controller = controller;
        this.routingService = routingService;
        this.routingRulesContext = routingRulesContext;
    }

    public void setPhoneNumber(String phoneNumber) {
        Map<String,String> property = new HashMap<>();
        property.put("phoneNumber", phoneNumber);
        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.EnterPhone, property));
        this.controller.setPhoneNumber(phoneNumber);
    }

    public void validatePhoneNumber(boolean isCall) {
        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.ValidatePhoneTapped));
        view.showValidatingPhone();

        String phoneNumber = this.controller.getPhoneNumber();
        if(Strings.isNullOrEmpty(phoneNumber)){
            view.showRequiredPhoneNumber();
            return;
        }

        if(phoneNumber.length() != 10){
            view.showInvalidPhoneNumber();
            return;
        }

        boolean validateSuccess = this.controller.validatePhoneNumber(isCall);
        if(!validateSuccess){
            view.showValidationError();
            return;
        }



        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                removeTimer();
                view.showValidationSuccess();
                NavigationContext navigationContext = new NavigationContext();
                navigationContext.routingContext = view.getRoutingContext();
                routingService.routeTo(RouteNames.ConsumerValidation2Route, navigationContext);

            }
        }, 5000, 5000);

//        view.showValidationSuccess();
//        NavigationContext navigationContext = new NavigationContext();
//        navigationContext.routingContext = view.getRoutingContext();
//        routingService.routeTo(RouteNames.ConsumerValidation2Route, navigationContext);
    }

    public void setValidationReason(String validationReason) {
        this.controller.setValidationReason(validationReason);
    }

    public void navigateToPrivacyPolicy() {
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();
        navigationContext.addParameter("url","https://storage.googleapis.com/clipcallterms/ClipCall_PrivacyPolicy.pdf");
        navigationContext.addParameter("title","PRIVACY POLICY");
        routingService.routeTo(RouteNames.ConsumerPrivacyPolicyRoute, navigationContext);
    }

    public void navigateToEula() {
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();
        navigationContext.addParameter("url","https://storage.googleapis.com/clipcallterms/ClipCall_EULA.pdf");
        navigationContext.addParameter("title","EULA");
        routingService.routeTo(RouteNames.ConsumerEulaRoute, navigationContext);
    }

    public void smsReceived(String smsMessage) {


        if(timer == null)
            return;

        removeTimer();

        if(!controller.isValidVerificationCode(smsMessage)){
            view.showValidationError();
            return;
        }

        boolean loginSuccess = this.controller.login();
        if(!loginSuccess){
            this.view.showValidationError();
            //this.view.showFailureMessage("Login failed","Please try again later.");
            return;
        }

        routingRulesContext.isPostPhoneValidation  = true;
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();
        routingService.routeTo((Route) null, navigationContext);
    }

    private void removeTimer() {
        if(timer != null)
        {
            timer.cancel();
            timer = null;
        }
    }
}
