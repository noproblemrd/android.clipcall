package it.clipcall.common.validation.presenters;

import android.util.Log;

import org.simple.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ProfessionalEventNames;
import it.clipcall.common.validation.controllers.PhoneValidationController;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.pages.validation.IPinCodeVerificationView;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingRulesContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.services.RoutingService;

@PerActivity
public class PinCodeVerificationPresenter extends PresenterBase<IPinCodeVerificationView> {

    private final PhoneValidationController controller;
    private final RoutingService routingService;
    private final RoutingRulesContext routingRulesContext;
    private  Timer timer;
    private long timePassed;

    private int timeToWait = 60;

    @Inject
    public PinCodeVerificationPresenter(PhoneValidationController controller, RoutingService routingService, RoutingRulesContext routingRulesContext){
        this.controller = controller;
        this.routingService = routingService;
        this.routingRulesContext = routingRulesContext;
    }

    public void resendTextOrCall(boolean isCall) {

        String eventName = isCall ? ProfessionalEventNames.ValidatePhoneCallMe : ProfessionalEventNames.ValidatePhoneTextMe;
        EventBus.getDefault().post(new ApplicationEvent(eventName));
        if(timer != null)
            return;
        view.onResendingCode();

        timePassed = timeToWait;

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                Log.d("Foo", "time passed " + timePassed);
                timePassed--;
                view.showTimePassed(timePassed);
                if (timePassed <= 0) {
                    timePassed = timeToWait;
                    view.onCanResendCodeAgain();
                    removeTimer();
                }
            }
        }, 0, 1000);


        boolean success = controller.validatePhoneNumber(isCall);

    }

    public void setDigitAt(CharSequence digit, int index) {
        if(digit == null || digit.length() != 1)
            return;

        Integer digitValue = Integer.valueOf(digit.toString());
        this.controller.setDigitAtIndex(digitValue, index);

        this.view.focusNextDigit(index);


        if(!this.controller.isPinCodeFilled())
            return;

        view.showValidatingPinCode();
        if(!this.controller.isCorrectPinCode())
        {
            this.controller.resetPinCode();
            this.view.invalidatePinCode();
            return;
        }

        boolean loginSuccess = this.controller.login();
        if(!loginSuccess){
            this.view.invalidatePinCode();
            return;
        }

        if(view != null){
            view.validatedCode();
        }

        routingRulesContext.isPostPhoneValidation  = true;
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();
        routingService.routeTo((Route) null, navigationContext);
    }

    @Override
    public void shutDown() {
        super.shutDown();
        removeTimer();
    }

    private void removeTimer() {
        if(timer != null)
        {
            timer.cancel();
            timer = null;
        }
    }
}
