package it.clipcall.common.validation.activities;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.validation.presenters.PhoneValidationPresenter;
import it.clipcall.common.validation.models.events.SmsReceivedEvent;
import it.clipcall.common.validation.models.events.ValidationCancelledEvent;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.pages.validation.IPhoneValidationView;

@EActivity(R.layout.phone_validation_activity)
public class PhoneValidationActivity extends BaseActivity implements IPhoneValidationView, IHasToolbar {
    @Extra("validationReason")
    String validationReason;

    @ViewById
    TextView phoneNumberTextView;

    @ViewById
    Toolbar toolbar;


    @ViewById
    Button validationButton;

    @ViewById
    TextView phoneValidationMessageTextView;

    @ViewById
    ProgressBar validatingProgressBar;

    @Inject
    PhoneValidationPresenter presenter;

    @AfterViews
    protected void onCreateCore(){
        presenter.bindView(this);
        this.presenter.setValidationReason(this.validationReason);
        if(validationReason != null){
            phoneValidationMessageTextView.setText(validationReason);
            phoneNumberTextView.requestFocus();
        }
    }

    @TextChange(R.id.phoneNumberTextView)
    void onPhoneNumberChanged(CharSequence text, TextView hello, int before, int start, int count) {
        validationButton.setEnabled(true);
        this.presenter.setPhoneNumber(text.toString());
    }

    @Background
    @Click(R.id.validationButton)
    void onValidationTapped(){
        this.presenter.validatePhoneNumber(false);
    }

    @UiThread
    @Override
    public void showValidatingPhone() {
        validatingProgressBar.setVisibility(View.VISIBLE);
        validationButton.setVisibility(View.INVISIBLE);
        validationButton.setEnabled(false);

    }

    @UiThread
    @Override
    public void showValidationError() {
        validatingProgressBar.setVisibility(View.INVISIBLE);
        validationButton.setVisibility(View.VISIBLE);
        validationButton.setEnabled(true);
        //TODO show error
    }

    @UiThread
    @Override
    public void showValidationSuccess() {
        validatingProgressBar.setVisibility(View.INVISIBLE);
        validationButton.setVisibility(View.VISIBLE);
        validationButton.setEnabled(true);
    }

    @UiThread
    @Override
    public void showRequiredPhoneNumber() {
        phoneNumberTextView.setError("Phone number is required");
        validatingProgressBar.setVisibility(View.INVISIBLE);
        validationButton.setVisibility(View.VISIBLE);
        validationButton.setEnabled(true);
    }

    @UiThread
    @Override
    public void showInvalidPhoneNumber() {
        phoneNumberTextView.setError("Phone number must have 10 digits");
        validatingProgressBar.setVisibility(View.INVISIBLE);
        validationButton.setVisibility(View.VISIBLE);
        validationButton.setEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        validatingProgressBar.setVisibility(View.INVISIBLE);
        validationButton.setVisibility(View.VISIBLE);
        validationButton.setEnabled(true);
    }

    @Override
    public String getViewTitle() {
        return "PHONE VALIDATION";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        EventBus.getDefault().post(new ValidationCancelledEvent());
        return false;
    }

    @Click(R.id.privacyPolicyTextView)
    void onPrivacyPolicyTapped(){
        presenter.navigateToPrivacyPolicy();
    }

    @Click(R.id.eulaTextView)
    void onEulaTapped(){
        presenter.navigateToEula();
    }


    @Override
    public void onBackPressed() {

        EventBus.getDefault().post(new ValidationCancelledEvent());
        super.onBackPressed();
    }

    @Subscriber
    void onValidationSmsReceived(SmsReceivedEvent event){
        smsRecieved(event.getMessage());
    }

    @Background
    void smsRecieved(String message){
        presenter.smsReceived(message);
    }
}
