package it.clipcall.common.validation.activities;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.validation.presenters.PinCodeVerificationPresenter;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.pages.validation.IPinCodeVerificationView;

@EActivity(R.layout.activity_phone_validatoin2)
public class PhoneValidation2Activity extends BaseActivity implements IPinCodeVerificationView, IHasToolbar {

    @ViewById(R.id.firstDigitEditText)
    EditText firstDigitEditText;

    @ViewById(R.id.secondDigitEditText)
    EditText secondDigitEditText;

    @ViewById(R.id.thirdDigitEditText)
    EditText thirdDigitEditText;

    @ViewById(R.id.forthDigitEditText)
    EditText forthDigitEditText;


    @ViewById
    TextView timeLeftToResendTextView;

    @ViewById
    ProgressBar pinCodeProgressBar;

    @ViewById
    Toolbar toolbar;

    @ViewById
    LinearLayout pinCodesContainer;

    @ViewById
    ViewGroup codeSentTextContainer;

    @ViewById
    Button callMeButton;

    @ViewById
    Button textMeButton;

    @ViewById
    ViewGroup resendCodeTextContainer;

    @Inject
    PinCodeVerificationPresenter presenter;

    @AfterViews
    void afterPhoneValidation2ActivityViewLoaded(){
        presenter.bindView(this);
        pinCodeProgressBar.setVisibility(View.INVISIBLE);
        firstDigitEditText.requestFocus();
    }

    @Background
    @TextChange(R.id.firstDigitEditText)
    void onFirstDigitEntered(CharSequence text, TextView hello, int before, int start, int count){
        presenter.setDigitAt(text, 0);

    }

    @Background
    @TextChange(R.id.secondDigitEditText)
    void onSecondDigitEntered(CharSequence text, TextView hello, int before, int start, int count){
        presenter.setDigitAt(text, 1);
    }

    @Background
    @TextChange(R.id.thirdDigitEditText)
    void onThirdDigitEntered(CharSequence text, TextView hello, int before, int start, int count){
        presenter.setDigitAt(text, 2);
    }

    @Background
    @TextChange(R.id.forthDigitEditText)
    void onForthDigitEntered(CharSequence text, TextView hello, int before, int start, int count){
        presenter.setDigitAt(text, 3);
    }

    @UiThread
    @Override
    public void invalidatePinCode() {
        firstDigitEditText.requestFocus();
        firstDigitEditText.setText(null);
        secondDigitEditText.setText(null);
        thirdDigitEditText.setText(null);
        forthDigitEditText.setText(null);
        pinCodeProgressBar.setVisibility(View.INVISIBLE);
        YoYo.with(Techniques.Shake)
                .duration(700)
                .playOn(pinCodesContainer);

    }



    @UiThread
    @Override
    public void focusNextDigit(final int currentDigitIndex) {
        if(currentDigitIndex == 0)
            PhoneValidation2Activity.this.secondDigitEditText.requestFocus();
        else if(currentDigitIndex == 1)
            PhoneValidation2Activity.this.thirdDigitEditText.requestFocus();
        else if(currentDigitIndex == 2)
            PhoneValidation2Activity.this.forthDigitEditText.requestFocus();
        else if(currentDigitIndex == 3)
            PhoneValidation2Activity.this.firstDigitEditText.requestFocus();
    }

    @UiThread
    @Override
    public void validatedCode() {
        pinCodeProgressBar.setVisibility(View.INVISIBLE);
    }

    @UiThread
    @Override
    public void showValidatingPinCode() {
        pinCodeProgressBar.setVisibility(View.VISIBLE);
    }

    @UiThread
    @Override
    public void showTimePassed(long timePassed) {
        timeLeftToResendTextView.setText(timePassed + "");
    }

    @UiThread
    @Override
    public void onResendingCode() {
        callMeButton.setEnabled(false);
        textMeButton.setEnabled(false);
        codeSentTextContainer.setVisibility(View.VISIBLE);
        resendCodeTextContainer.setVisibility(View.GONE);
    }
    @UiThread
    @Override
    public void onCanResendCodeAgain(){
        codeSentTextContainer.setVisibility(View.GONE);
        resendCodeTextContainer.setVisibility(View.VISIBLE);
        callMeButton.setEnabled(true);
        textMeButton.setEnabled(true);
    }

    @Background
    @Click(R.id.callMeButton)
    void callMe(){
        boolean isCall = true;
        presenter.resendTextOrCall(isCall);
    }

    @Background
    @Click(R.id.textMeButton)
    void textMe(){
        boolean isCall = false;
        presenter.resendTextOrCall(isCall);
    }

    @Override
    public String getViewTitle() {
        return "CODE VERIFICATION";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.shutDown();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.bindView(this);
    }
}
