package it.clipcall.common.validation.support;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import org.simple.eventbus.EventBus;

import it.clipcall.common.validation.models.events.SmsReceivedEvent;

public class PhoneValidationSmsBroadcastReceiver extends BroadcastReceiver {
    public PhoneValidationSmsBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {


        Bundle bundle = intent.getExtras();
        SmsMessage[] msgs = null;
        String message = "";
        if(bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            msgs = new SmsMessage[pdus.length];

            for(int i=0; i<msgs.length;i++) {
                msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                message = msgs[i].getMessageBody();
                //Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(new SmsReceivedEvent(message));
            }

        }
    }
}
