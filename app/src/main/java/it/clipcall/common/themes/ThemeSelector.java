package it.clipcall.common.themes;


import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.professional.validation.services.AuthenticationManager;

@PerActivity
public class ThemeSelector {


    private final AuthenticationManager authenticationManager;

    @Inject
    public ThemeSelector(AuthenticationManager authenticationManager){

        this.authenticationManager = authenticationManager;
    }


    public int selectTheme(){
        if(authenticationManager.isInProfessionalMode())
            return R.style.Professional;


        return R.style.ConsumerBaseTheme;
    }

}
