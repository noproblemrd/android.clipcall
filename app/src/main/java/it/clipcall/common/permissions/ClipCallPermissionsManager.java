package it.clipcall.common.permissions;

import android.Manifest;

import javax.inject.Inject;

import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.ui.permissions.IPermissionsHandler;
import it.clipcall.infrastructure.ui.permissions.PermissionContext;
import it.clipcall.infrastructure.ui.permissions.PermissionsService;

@PerActivity
public class ClipCallPermissionsManager {

    private final PermissionsService permissionsService;

    @Inject
    public ClipCallPermissionsManager(PermissionsService permissionsService){

        this.permissionsService = permissionsService;
    }

    public void requestPaymentCardScanPermissions(IPermissionsHandler permissionsHandler){
        PermissionContext permissionContext = new PermissionContext("Permissions required", "We need your permissions to scan your credit card", Manifest.permission.CAMERA);
        permissionsService.requestPermissions(permissionContext, permissionsHandler);
    }


    public void requestCameraProfileImagePermission(IPermissionsHandler permissionsHandler){
        PermissionContext permissionContext = new PermissionContext("Permissions required", "We need your permissions to take a profile image", Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissionsService.requestPermissions(permissionContext, permissionsHandler);
    }

    public void requestVideoChatPermissions(IPermissionsHandler permissionsHandler){
        PermissionContext permissionContext = new PermissionContext("Permissions required","We need your permissions to start a video chat", Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissionsService.requestPermissions(permissionContext, permissionsHandler);
    }

    public void requestGalleryProfileImagePermissions(IPermissionsHandler permissionsHandler){
        PermissionContext permissionContext = new PermissionContext("Permission required","We need your permissions to select a profile image",Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissionsService.requestPermissions(permissionContext, permissionsHandler);
    }

    public void requestChatAudioPermissions(IPermissionsHandler permissionsHandler){
        PermissionContext permissionContext = new PermissionContext("Permissions required","We need your permissions to start a video chat", Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissionsService.requestPermissions(permissionContext, permissionsHandler);
    }
}
