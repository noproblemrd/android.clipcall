package it.clipcall.common.invitations.models;

/**
 * Created by dorona on 31/01/2016.
 */
public enum eInvitationType {

    VideoChat(1),
    Video(2),
    Unknown(-1);

    private final int value;
    eInvitationType(int value){
        this.value = value;
    }

    public static eInvitationType fromKey(int code) {
        for (eInvitationType typе : eInvitationType.values()) {
            if (typе.getValue() == code) {
                return typе;
            }
        }
        return eInvitationType.Unknown;
    }

    public int getValue() {
        return value;
    }
}
