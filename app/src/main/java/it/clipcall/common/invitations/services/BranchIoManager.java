package it.clipcall.common.invitations.services;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.common.base.Strings;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;
import it.clipcall.common.invitations.models.eInvitationType;
import it.clipcall.consumer.vidoechat.models.CustomerVideoInvitationData;
import it.clipcall.infrastructure.services.IBranchIoInitListener;
import it.clipcall.infrastructure.views.IView;

@Singleton
public class BranchIoManager implements IInvitationProvider{

    private JSONObject parameters = null;

    private String invitationId = null;


    private final static String InvitationTypeKey =  "invitationType";


    @Inject
    public BranchIoManager() {
    }

    public void initSession(Uri data, IView view, final IBranchIoInitListener listener){

      /*  if(!Environment.AnalyticsEnabled){
            listener.onComplete();
            return;
        }*/
        Activity activity = (Activity)view;
        Branch branch = Branch.getInstance();
        branch.initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    // params are the deep linked params associated with the link that the user clicked before showing up
                    Log.i("BranchConfigTest", "deep link data: " + referringParams.toString());
                    parameters = referringParams;
                    listener.onComplete();
                }else{

                    listener.onError();
                }
            }
        },data , activity);
    }

    public boolean hasInvitation(eInvitationType invitationType){
        if(parameters == null)
            return false;

        try {
            boolean clickedBranchLink = (boolean) parameters.get("+clicked_branch_link");
            if(!clickedBranchLink)
                return false;
            invitationId = (String)parameters.get("invitationID");


            if(!parameters.has("invitationType"))
                return false;

            eInvitationType type = getInvitationType(parameters);
            return type.equals(invitationType);


        } catch (JSONException e) {
            return false;
        }
    }

    public JSONObject getInvitationData(){
        return parameters;
    }

    public String getInvitationId() {
        return this.invitationId;
    }


    public void clearInvitation() {
        parameters = null;
        invitationId = null;
    }


    public void generateLink(Context context, CustomerVideoInvitationData invitationData, Branch.BranchLinkCreateListener listener){
        BranchUniversalObject branchUniversalObject = new BranchUniversalObject()
                .setCanonicalIdentifier("invitation/"+invitationId)
                .setTitle("invitationTypeTitle "+invitationData.getInvitationType())
                .setContentDescription("invitationTypeDescription "+invitationData.getInvitationType())
                .setContentImageUrl("http://www.clipcall.it/wp-content/uploads/2015/08/logo-blue240.png")
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .addContentMetadata("invitationID", invitationData.getInvitationId())
                .addContentMetadata("invitationType", Integer.toString(invitationData.getInvitationType().getValue()));

        LinkProperties linkProperties = new LinkProperties()
                .setChannel("sms")
                .setFeature("sharing")
                .addControlParameter("$desktop_url", "http://www.clipcall.it")
                .addControlParameter("$ios_url", "https://itunes.apple.com/us/app/clipcall-hire-local-professionals/id1014454741?mt=8")
                .addControlParameter("$android_url", "https://play.google.com/store/apps/details?id=it.clipcall");

        branchUniversalObject.generateShortUrl(context, linkProperties, listener);
    }


    private eInvitationType getInvitationType(JSONObject parameters){
        if(!parameters.has(InvitationTypeKey))
            return eInvitationType.Unknown;
        try {
            String invitationTypeString = null;
            invitationTypeString = parameters.getString(InvitationTypeKey);
            int invitationTypeCode = -1;
            if(!Strings.isNullOrEmpty(invitationTypeString)){
                invitationTypeCode = Integer.parseInt(invitationTypeString);
                eInvitationType type = eInvitationType.fromKey(invitationTypeCode);
                return type;
            }

            invitationTypeCode = parameters.getInt(InvitationTypeKey);
            return eInvitationType.fromKey(invitationTypeCode);
        }
        catch (JSONException e) {
            e.printStackTrace();
            return eInvitationType.Unknown;
        }
    }

}
