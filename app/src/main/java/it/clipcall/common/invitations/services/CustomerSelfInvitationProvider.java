package it.clipcall.common.invitations.services;

import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class CustomerSelfInvitationProvider implements IInvitationProvider {


    private String invitationId;
    private JSONObject invitationData;

    @Inject
    public CustomerSelfInvitationProvider() {
    }

    public void setInvitationId(String invitationId){
        this.invitationId = invitationId;
        invitationData = new JSONObject();
    }

    @Override
    public String getInvitationId() {
        return invitationId;
    }

    @Override
    public JSONObject getInvitationData() {
        return invitationData;
    }

    @Override
    public void clearInvitation() {
        this.invitationId = null;
        this.invitationData = null;
    }
}
