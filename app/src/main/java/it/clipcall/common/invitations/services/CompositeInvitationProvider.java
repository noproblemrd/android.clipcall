package it.clipcall.common.invitations.services;

import com.google.common.base.Strings;

import org.json.JSONObject;

/**
 * Created by dorona on 17/02/2016.
 */
public class CompositeInvitationProvider implements IInvitationProvider {


    private final IInvitationProvider[] invitationProviders;

    public CompositeInvitationProvider(IInvitationProvider... invitationProviders) {
        this.invitationProviders = invitationProviders;
    }

    @Override
    public String getInvitationId() {
        for (IInvitationProvider invitationProvider : invitationProviders) {
            String invitationId = invitationProvider.getInvitationId();
            if(!Strings.isNullOrEmpty(invitationId))
                return invitationId;
        }

        return null;
    }

    @Override
    public JSONObject getInvitationData() {
        for (IInvitationProvider invitationProvider : invitationProviders) {
            JSONObject invitationData= invitationProvider.getInvitationData();
            if(invitationData != null)
                return invitationData;
        }

        return null;
    }

    @Override
    public void clearInvitation() {
        for (IInvitationProvider invitationProvider : invitationProviders) {
            invitationProvider.clearInvitation();
        }
    }
}
