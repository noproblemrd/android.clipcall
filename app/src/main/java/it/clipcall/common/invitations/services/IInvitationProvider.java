package it.clipcall.common.invitations.services;

import org.json.JSONObject;

/**
 * Created by dorona on 17/02/2016.
 */
public interface IInvitationProvider {

    String getInvitationId();

    JSONObject getInvitationData();

    void clearInvitation();
}
