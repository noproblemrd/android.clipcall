package it.clipcall.common.projects.controllers;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.projects.services.ProjectsManager;

/**
 * Created by omega on 2/19/2016.
 */
public class ProjectsController {

    private final AuthenticationManager authenticationManager;
    private final ProjectsManager projectsManager;

    @Inject
    public ProjectsController(AuthenticationManager authenticationManager, ProjectsManager projectsManager) {
        this.authenticationManager = authenticationManager;
        this.projectsManager = projectsManager;
    }

    public List<? extends ProjectEntityBase>  getProjects(boolean forceReload){
        String customerId = authenticationManager.getCustomerId();
        List<? extends ProjectEntityBase> projects = projectsManager.getAll(customerId, forceReload);
        return projects;
    }
}
