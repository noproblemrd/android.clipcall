package it.clipcall.common.projects.models;

import java.io.Serializable;

/**
 * Created by dorona on 07/07/2016.
 */
public class ProjectStatus implements Serializable{
    String status;
    eProjectStatusColor color;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public eProjectStatusColor getColor() {
        return color;
    }

    public void setColor(eProjectStatusColor color) {
        this.color = color;
    }
}
