package it.clipcall.common.projects.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import com.google.common.base.Strings;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.parceler.Parcels;

import java.util.Date;

import it.clipcall.R;
import it.clipcall.common.projects.models.ProjectVideoData;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.support.dates.DateFormatter;

@EActivity(R.layout.project_video_activity)
public class ProjectVideoActivity extends BaseActivity implements MediaPlayer.OnPreparedListener, DialogInterface.OnCancelListener, IHasToolbar {

    @Extra
    Parcelable context;

    private int position = 0;

    private ProgressDialog progressDialog;

    private MediaController mediaControls;

    @ViewById
    VideoView projectVideoView;

    @ViewById
    Toolbar toolbar;

    ProjectVideoData projectVideoData;

    @AfterViews
    void afterViews(){

        projectVideoData = Parcels.unwrap(context);
        mediaControls = new MediaController(this);
        progressDialog = new ProgressDialog(this);
        String title = projectVideoData.category + " in " + projectVideoData.zipCode;
        progressDialog.setTitle(title);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(true);
        progressDialog.setOnCancelListener(this);
        progressDialog.show();



        try {

            if(!Strings.isNullOrEmpty(projectVideoData.url))
            {
                projectVideoView.setMediaController(mediaControls);
                projectVideoView.setVideoURI(Uri.parse(projectVideoData.url));
                projectVideoView.requestFocus();
                projectVideoView.setOnPreparedListener(this);

            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        progressDialog.dismiss();
        projectVideoView.seekTo(position);
        if (position == 0) {
            projectVideoView.start();
        } else {
            projectVideoView.pause();
        }

        String title = projectVideoData.category + " in " + projectVideoData.zipCode;
        Date subtitle = projectVideoData.createDate;
        toolbar.setTitle(title);
        toolbar.setSubtitle(DateFormatter.toLocalDate(subtitle));
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        //we use onSaveInstanceState in order to store the video playback position for orientation change
        savedInstanceState.putInt("Position", projectVideoView.getCurrentPosition());
        savedInstanceState.putParcelable("context",context);
        projectVideoView.pause();
    }

    @Override

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //we use onRestoreInstanceState in order to play the video playback from the stored position
        position = savedInstanceState.getInt("Position");

        Parcelable context = savedInstanceState.getParcelable("context");
        projectVideoData = Parcels.unwrap(context);
        projectVideoView.seekTo(position);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        finish();
    }

    @Override
    public String getViewTitle() {
        return "";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }
}
