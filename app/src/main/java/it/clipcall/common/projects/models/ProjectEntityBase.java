package it.clipcall.common.projects.models;

import com.google.common.base.Strings;

import java.io.Serializable;
import java.util.Date;

import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.professional.leads.models.Quote;

/**
 * Created by dorona on 28/02/2016.
 */
public abstract class ProjectEntityBase implements Serializable {

    int totalUnreadMessages;
    Date createdOn;
    String zipCode;
    String videoUrl;
    String previewVideoImageUrl;
    int videoDuration;
    boolean isPrivate;
    String fullAddress;
    Date lastUnreadMessageDate;
    Date lastUpdate;

    ProjectStatus status;

    public abstract String getId();

    public abstract String getCategoryName();

    public abstract String getProfileUrl();

    public abstract String getName();

    public int getTotalUnreadMessages() {
        return totalUnreadMessages;
    }

    public Date getLastUpdateDate(){
        if(lastUnreadMessageDate == null)
            return lastUpdate;

        if(lastUpdate.compareTo(lastUnreadMessageDate) > 0)
            return lastUpdate;

        return lastUnreadMessageDate;
    }

    public boolean isExclusive(){
        return isPrivate;
    }

    public abstract void updateUnreadMessageCount(ChatManager chatManager);

    public void setTotalUnreadMessages(int totalUnreadMessages) {
        this.totalUnreadMessages = totalUnreadMessages;
    }

    public void setLastUnreadMessageDate(Date lastUnreadMessageDate){
        this.lastUnreadMessageDate = lastUnreadMessageDate;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getPreviewVideoImageUrl() {
        return previewVideoImageUrl;
    }

    public void setPreviewVideoImageUrl(String previewVideoImageUrl) {
        this.previewVideoImageUrl = previewVideoImageUrl;
    }

    public int getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(int videoDuration) {
        this.videoDuration = videoDuration;
    }

    public boolean getPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }
    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getAddressOrZipCode(){
        String fullAddress = getFullAddress();
        return Strings.isNullOrEmpty(fullAddress) ? getZipCode() : fullAddress;
    }

    public abstract  boolean isNotHired();

    public abstract void updateLastUnreadMessage(ChatManager chatManager, String participantId);

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("address: "+getAddressOrZipCode());
        builder.append("category: "+ getCategoryName());
        return builder.toString();
    }

    public Date getLastUnreadMessageDate() {

        return lastUnreadMessageDate;
    }

    public abstract Quote getLastQuote();

    public abstract Quote getLeadAccountLastQuote(String leadAccountId);

    public ProjectStatus getStatus() {
        return status;
    }

    public void setStatus(ProjectStatus status) {
        this.status = status;
    }

    public abstract boolean isBooked() ;

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
