package it.clipcall.common.projects.support;

import android.os.Bundle;

import it.clipcall.common.projects.models.ProjectEntityBase;

/**
 * Created by omega on 2/19/2016.
 */
public interface IProjectsListener {

    void onProjectDetailsTapped(ProjectEntityBase project, Bundle bundle);

    void onProjectPlayVideoTapped(ProjectEntityBase project);

    void onProjectActionTapped(ProjectEntityBase project);

}
