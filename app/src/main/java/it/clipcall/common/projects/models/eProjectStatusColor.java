package it.clipcall.common.projects.models;

import it.clipcall.R;

/**
 * Created by dorona on 13/07/2016.
 */
public enum eProjectStatusColor {

    Green(1, R.color.green),
    Grey (2, R.color.medium_dark_grey),
    Orange(3,R.color.orange ),
    Blue(4, R.color.blue),
    Purple(5,R.color.purple);


    private final int key;
    private final int value;

    eProjectStatusColor(int key, int value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return this.key;
    }

    public static eProjectStatusColor fromKey(int key) {
        for(eProjectStatusColor type : eProjectStatusColor.values()) {
            if(type.getKey() == key) {
                return type;
            }
        }
        return eProjectStatusColor.Green;
    }

    public int getValue() {
        return value;
    }
}
