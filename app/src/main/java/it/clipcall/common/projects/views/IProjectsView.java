package it.clipcall.common.projects.views;

import java.util.List;

import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 04/01/2016.
 */
public interface IProjectsView extends IView {

    void setProjects(List<? extends ProjectEntityBase> projects);

    void showLoadingIndicator();

    void hideLoadingIndicator();

    void onProjectActionExecuted(ProjectEntityBase project);

    void showNotAuthenticated();

    RoutingContext getFragmentRoutingContext();
}
