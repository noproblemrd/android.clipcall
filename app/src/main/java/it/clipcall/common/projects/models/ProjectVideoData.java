package it.clipcall.common.projects.models;


import org.parceler.Parcel;

import java.util.Date;

@Parcel
public class ProjectVideoData {
    public String url;
    public String category;
    public String zipCode;
    public Date createDate;
}
