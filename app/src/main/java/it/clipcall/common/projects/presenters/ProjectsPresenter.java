package it.clipcall.common.projects.presenters;

import org.simple.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.projects.controllers.ProjectsController;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.common.projects.views.IProjectsView;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;

/**
 * Created by dorona on 04/01/2016.
 */
public class ProjectsPresenter extends PresenterBase<IProjectsView>{

    private final RoutingService routingService;
    private final ProjectsController controller;

    @Inject
    public ProjectsPresenter(ProjectsController controller, RoutingService routingService) {
        this.controller = controller;
        this.routingService = routingService;
    }

    public void getProjects(boolean forceReload){
        view.showLoadingIndicator();
        List<? extends ProjectEntityBase> projects = controller.getProjects(forceReload);
        view.hideLoadingIndicator();
        view.setProjects(projects);

    }

    public void projectSelected(ProjectEntity project) {
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();
        navigationContext.addParameter("projectId", project.getId());
        routingService.routeTo(Routes.ConsumerProjectDetailsRoute, navigationContext);

    }

    public void executeProjectAction(ProjectEntity project) {
        view.onProjectActionExecuted(project);
    }

    public void createNewProjectTapped() {
        EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.CREATE_NEW_PROJECT));

        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getRoutingContext();
        routingService.routeTo(Routes.ConsumerFindProRoute, navigationContext);
    }
}
