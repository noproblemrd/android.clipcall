package it.clipcall.common.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.appsee.Appsee;
import com.appsflyer.AppsFlyerLib;
import com.google.common.base.Strings;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.environment.Environment;
import it.clipcall.common.presenters.SplashScreenPresenter;
import it.clipcall.common.views.ISplashScreenView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.ui.UiDimensionConverter;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends BaseActivity implements ISplashScreenView{

    private boolean animationInProgress;

    @ViewById
    ImageView logoImageView;

    @ViewById
    ImageView logoNameImageView;

    @Inject
    SplashScreenPresenter presenter;

    private Timer timer;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Environment.AnalyticsEnabled){
            Appsee.start("ebf33a99a1454ebfa73a0ebd947e08fd");
            String phoneNumber = presenter.getPhoneNumber();
            if(!Strings.isNullOrEmpty(phoneNumber)){
                Appsee.setUserId(phoneNumber);
            }
            Map<String, Object> props = new HashMap<>();
            Appsee.addEvent("ApplicationStarted", props);
        }
    }

    @AfterViews
    public void onViewLoaded(){
        this.presenter.bindView(this);

        animationInProgress = true;
        logoImageView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        int height = UiDimensionConverter.dpToPx(200);
        ObjectAnimator translationYAnimator = ObjectAnimator.ofFloat(logoImageView, "translationY", height, 0);
        ObjectAnimator rotateAnimator = ObjectAnimator.ofFloat(logoImageView ,"rotation", 0f, 360f*4);
        rotateAnimator.setInterpolator(new DecelerateInterpolator());
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(
                translationYAnimator,
                rotateAnimator
        );
        //animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.setDuration(2000);
        animatorSet.addListener(new AnimatorListenerAdapter(){
            @Override public void onAnimationEnd(Animator animation) {

                logoImageView.setLayerType(View.LAYER_TYPE_NONE, null);

                AnimatorSet animatorSet2 = new AnimatorSet();
                animatorSet2.playTogether(
                        ObjectAnimator.ofFloat(logoNameImageView,"alpha",0,1)
                );
                animatorSet2.setInterpolator(new AccelerateInterpolator());
                animatorSet2.setDuration(1000);
                animatorSet2.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        animationInProgress = false;
                    }
                });
                animatorSet2.start();
            }
        });



        animatorSet.start();


      /*  ViewAnimator
                .animate(logoImageView)
                .dp()
                .translationY(200, 0)
                //.alpha(0, 1)
                .rotation(360)
                .descelerate()
                .duration(2000)
                .thenAnimate(logoNameImageView)
                .alpha(0,1)
                .duration(1000)
                .onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        animationInProgress = false;
                    }
                })
                .start();*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        Uri data = this.getIntent().getData();
        initSession(data);
    }

    @Background
    void initSession(Uri data){
        if(Environment.AnalyticsEnabled){
            presenter.initializeSession(data);
            AppsFlyerLib.sendTracking(getApplicationContext());
            return;
        }


        resolveInitialRoute();
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    @Background
    public void resolveInitialRoute(){
        if(!animationInProgress)
        {
            presenter.resolveInitialRoute();
            finish();
            return;
        }


        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(animationInProgress)
                    return;

                if(timer != null){
                    timer.cancel();
                    timer = null;
                }

                resolveInitialRoute();

            }
        }, 0, 500);
    }

}
