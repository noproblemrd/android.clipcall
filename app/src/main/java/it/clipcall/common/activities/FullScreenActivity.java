package it.clipcall.common.activities;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.common.base.Strings;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.presenters.ImageFullScreenPresenter;
import it.clipcall.common.views.IFullScreenImageView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;

@EActivity(R.layout.image_full_screen_activity)
public class FullScreenActivity extends BaseActivity implements IHasToolbar, IFullScreenImageView {

    @ViewById
    ImageView imageView;

    @ViewById
    ViewGroup rootCoordinatorLayout;

    @ViewById
    Toolbar toolbar;

    @Inject
    ImageFullScreenPresenter presenter;

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        presenter.initialize();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.download_image_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.downloadImageMenuItem) {
            Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "ClipCall_Chat_" + timeStamp + "_";
            String insertedFile = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, imageFileName , imageFileName);
            String message = Strings.isNullOrEmpty(insertedFile) ?  "Failed saving photo" : "Photo saved to gallery";
            Snackbar.make(rootCoordinatorLayout,message, Snackbar.LENGTH_LONG).show();
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showImage(Bitmap image) {
        imageView.setImageBitmap(image);
    }

    @Override
    public String getViewTitle() {
        return "";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }
}
