package it.clipcall.common.thirdParty;


import android.content.Intent;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;

import org.json.JSONObject;

import javax.inject.Inject;

import it.clipcall.consumer.profile.models.FacebookProfile;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;

@PerActivity
public class FacebookService {


    private final ILogger logger = LoggerFactory.getLogger(FacebookService.class.getSimpleName());
    private final Gson gson;
    private CallbackManager callbackManager;
    private ProfileTracker profileTracker;
    private AccessTokenTracker accessTokenTracker;

    @Inject
    public FacebookService(Gson gson){
        this.gson = gson;
        callbackManager = CallbackManager.Factory.create();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                if (oldProfile != null) {


                }
                if (currentProfile != null) {

                }
            }
        };


    }

    public void getProfile(final AccessToken accessToken, final IFacebookProfileCallback facebookProfileCallback){
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {

            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                logger.debug("getProfile -> %s", response.toString());

                FacebookProfile facebookProfile = gson.fromJson(object.toString(), FacebookProfile.class);
                facebookProfile.facebookAccessToken = accessToken.getToken();
                facebookProfileCallback.onSuccess(facebookProfile);
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
        request.setParameters(parameters);
        request.executeAsync();
    }

    public void start(){
        accessTokenTracker.startTracking();
        profileTracker.startTracking();
    }

    public void stop(){
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    public void setFacebookActivityResult(int requestCode, int resultCode, final Intent data){
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void registerFacebookLoginButton(LoginButton facebookLoginButton, FacebookCallback<LoginResult> facebookCallback) {
        facebookLoginButton.registerCallback(callbackManager, facebookCallback);
    }
}
