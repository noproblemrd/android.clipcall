package it.clipcall.common.thirdParty;

import it.clipcall.consumer.profile.models.FacebookProfile;

/**
 * Created by dorona on 20/06/2016.
 */
public interface IFacebookProfileCallback {

    void onSuccess(FacebookProfile facebookProfile);
}
