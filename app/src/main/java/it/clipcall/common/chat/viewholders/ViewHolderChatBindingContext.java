package it.clipcall.common.chat.viewholders;

import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.GroupedMessages;
import it.clipcall.common.chat.support.IChatMessageListener;
import it.clipcall.common.chat.support.factories.MessageContext;

/**
 * Created by dorona on 01/08/2016.
 */

public class ViewHolderChatBindingContext {
    public GroupedMessages section;
    public ChatMessage message;
    public IChatMessageListener chatMessageListener;
    public MessageContext messageContext;
}
