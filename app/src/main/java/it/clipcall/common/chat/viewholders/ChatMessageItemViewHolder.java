package it.clipcall.common.chat.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class ChatMessageItemViewHolder extends RecyclerView.ViewHolder {


    protected final Context context;

    public ChatMessageItemViewHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
    }

    public abstract void bind(ViewHolderChatBindingContext bindingContext);
}