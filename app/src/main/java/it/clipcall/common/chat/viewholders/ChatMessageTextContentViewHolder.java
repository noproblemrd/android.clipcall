package it.clipcall.common.chat.viewholders;

import android.view.View;

import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.CompositeTextChatMessage;
import it.clipcall.common.chat.models.GroupedMessages;
import it.clipcall.common.chat.support.IChatMessageListener;

public class ChatMessageTextContentViewHolder extends ChatMessageContentViewHolder{

    public ChatMessageTextContentViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bind(ViewHolderChatBindingContext bindingContext) {
        super.bind(bindingContext);
        GroupedMessages section = bindingContext.section;
        ChatMessage message = bindingContext.message;
        IChatMessageListener chatMessageListener = bindingContext.chatMessageListener;

        pictureImageView.setVisibility(View.GONE);
        messageTextView.setVisibility(View.VISIBLE);
        audioChatMessageContainer.setVisibility(View.GONE);
        quoteContentContainer.setVisibility(View.GONE);

        CompositeTextChatMessage imageMessage = (CompositeTextChatMessage) message;
        messageTextView.setText(imageMessage.getText());

    }
}