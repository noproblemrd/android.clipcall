package it.clipcall.common.chat.models;

import it.clipcall.infrastructure.support.criterias.ICriteria;

/**
 * Created by dorona on 16/08/2016.
 */

public class ChatMessageContainsPhoneNumberValidationCriteria implements ICriteria<String> {

    @Override
    public boolean isSatisfiedBy(String candidate) {
        return android.util.Patterns.PHONE.matcher(candidate).matches() && candidate.length() >= 8;
    }
}
