package it.clipcall.common.chat.models;

import android.util.Patterns;

import it.clipcall.infrastructure.support.criterias.ICriteria;

/**
 * Created by dorona on 16/08/2016.
 */

public class ChatMessageContainsWebUrlValidationCriteria implements ICriteria<String> {

    @Override
    public boolean isSatisfiedBy(String candidate) {
        return Patterns.WEB_URL.matcher(candidate).matches();
    }
}
