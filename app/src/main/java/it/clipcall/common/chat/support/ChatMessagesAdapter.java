package it.clipcall.common.chat.support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.R;
import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.GroupedMessages;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.UnknownChatMessage;
import it.clipcall.common.chat.support.factories.ChatMessageFactoryProvider;
import it.clipcall.common.chat.support.factories.IMessageFactory;
import it.clipcall.common.chat.support.factories.MessageContext;
import it.clipcall.common.chat.viewholders.ChatMessageContentViewHolder;
import it.clipcall.common.chat.viewholders.ChatMessageHeaderViewHolder;
import it.clipcall.common.chat.viewholders.ChatMessageItemViewHolder;
import it.clipcall.common.chat.viewholders.ViewHolderChatBindingContext;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;

public class ChatMessagesAdapter extends SectionedRecyclerViewAdapter<ChatMessageItemViewHolder> {

    private final Context context;
    private final IChatMessageListener chatMessageListener;
    private MessageContext messageContext;


    public ChatMessagesAdapter(Context context, IChatMessageListener chatMessageListener){
        this.context = context;
        this.chatMessageListener = chatMessageListener;
    }

    private final List<GroupedMessages> groupedMessages = new ArrayList<>();

    @Override
    public int getSectionCount() {
        return groupedMessages.size();
    }

    @Override
    public int getItemCount(int section) {
        return groupedMessages.get(section).size();
    }

    @Override
    public void onBindHeaderViewHolder(ChatMessageItemViewHolder viewHolder, int section) {
        GroupedMessages groupedMessages = this.groupedMessages.get(section);
        ViewHolderChatBindingContext bindingContext = new ViewHolderChatBindingContext();
        bindingContext.chatMessageListener = chatMessageListener;
        bindingContext.section = groupedMessages;
        bindingContext.message = null;
        bindingContext.messageContext = messageContext;
        viewHolder.bind(bindingContext);

    }

    @Override
    public int getItemViewType(int section, int relativePosition, int absolutePosition) {

        int itemViewType = super.getItemViewType(section, relativePosition, absolutePosition);
        if(itemViewType == VIEW_TYPE_HEADER)
            return itemViewType;

        //item view type
        GroupedMessages groupedMessages = this.groupedMessages.get(section);
        ChatMessage message = groupedMessages.getMessageAt(relativePosition);
        return message.getType();
    }

    @Override
    public void onBindViewHolder(ChatMessageItemViewHolder viewHolder, int section, int relativePosition, int absolutePosition) {
        GroupedMessages groupedMessages = this.groupedMessages.get(section);
        ChatMessage message = groupedMessages.getMessageAt(relativePosition);
        ViewHolderChatBindingContext bindingContext = new ViewHolderChatBindingContext();
        bindingContext.chatMessageListener = chatMessageListener;
        bindingContext.section = groupedMessages;
        bindingContext.message = message;
        bindingContext.messageContext = messageContext;
        viewHolder.bind(bindingContext);
    }

    @Override
    public ChatMessageItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(VIEW_TYPE_HEADER == viewType){
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_group_item, parent, false);
            return new ChatMessageHeaderViewHolder(v);
        }

        ChatMessageFactoryProvider messageFactoryProvider = ChatMessageFactoryProvider.getInstance();
        IMessageFactory factory = messageFactoryProvider.getFactory(viewType);


        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.consumer_chat_messages_item, parent, false);

        ChatMessageContentViewHolder contentViewHolder = factory.createViewHolder(v);
        return contentViewHolder;
    }

    public void addMessage(MessageContext messageContext, IMessage message) {
        this.messageContext = messageContext;
        this.addMessageCore(message);
        this.notifyDataSetChanged();
    }

    public void setMessages(MessageContext messageContext, List<IMessage> messages) {
        this.groupedMessages.clear();
        this.messageContext = messageContext;
        for (IMessage message: messages) {
            addMessageCore(message);
        }

        this.notifyDataSetChanged();
    }

    private void addMessageCore(IMessage message){
        final LocalDate key = new LocalDate(message.getSentAt());
        GroupedMessages existingGroup = Lists.firstOrDefault(this.groupedMessages, new ICriteria<GroupedMessages>() {
            @Override
            public boolean isSatisfiedBy(GroupedMessages candidate) {
                return candidate.getKey().equals(key);
            }
        });

        if(existingGroup == null) {
            existingGroup = new GroupedMessages(key);
            this.groupedMessages.add(existingGroup);
        }

        ChatMessageFactoryProvider messageFactoryProvider = ChatMessageFactoryProvider.getInstance();
        IMessageFactory factory = messageFactoryProvider.getFactory(message);
        ChatMessage chatMessage = factory.createChatMessage(messageContext, message);
        if(!(chatMessage instanceof UnknownChatMessage))
            existingGroup.addMessage(chatMessage);
    }


    public int getTotalMessages() {
        return getItemCount();
    }
}
