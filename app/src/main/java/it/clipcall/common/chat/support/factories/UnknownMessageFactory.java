package it.clipcall.common.chat.support.factories;

import android.view.View;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.models.TextChatMessage;
import it.clipcall.common.chat.support.tasks.IChatMessageTask;
import it.clipcall.common.chat.support.tasks.NoOpChatMessageTask;
import it.clipcall.common.chat.viewholders.ChatMessageContentViewHolder;
import it.clipcall.common.chat.viewholders.ChatMessageTextContentViewHolder;


/**
 * Created by dorona on 02/03/2016.
 */
@Singleton
public class UnknownMessageFactory extends MessageFactoryBase {

    @Inject
    public UnknownMessageFactory(){

    }
    @Override
    public boolean canCreateChatMessage(IMessage message) {
       return true;
    }


    @Override
    public ChatMessage createChatMessageCore(IMessage message) {
        ChatMessage chatMessage = new TextChatMessage("Unknown message");
        return chatMessage;
    }

    @Override
    public MessageDescription createMessageDescription(Object data) {
        return null;
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.Text;
    }

    @Override
    public ChatMessageContentViewHolder createViewHolder(View v) {
        return new ChatMessageTextContentViewHolder(v);
    }

    @Override
    public IChatMessageTask createChatMessageTask() {
        return new NoOpChatMessageTask();
    }

    @Override
    public String getMessageDescription(IMessage message) {
        return "Unknown message";
    }
}
