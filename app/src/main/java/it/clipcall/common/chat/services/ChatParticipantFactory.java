package it.clipcall.common.chat.services;

import com.google.common.base.Strings;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.consumer.profile.controllers.CustomerProfileController;
import it.clipcall.consumer.profile.models.CustomerProfile;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.professional.leads.models.Constants;
import it.clipcall.professional.leads.models.ProfessionalProjectDetails;

@Singleton
public class ChatParticipantFactory {

    private final CustomerProfileController customerProfileController;

    @Inject
    public ChatParticipantFactory(CustomerProfileController customerProfileController){

        this.customerProfileController = customerProfileController;
    }

    public ChatParticipant buildProfessionalChatParticipant(ProfessionalRef professional){
        ChatParticipant result = new ChatParticipant(professional.getCustomerId(), professional.getSupplierIcon(), professional.getSupplierName(), true);
        return result;
    }

    public ChatParticipant buildConsumerChatParticipant(ProjectEntity project){
        CustomerProfile consumerProfile = customerProfileController.getCustomerProfile();
        String customerNameOrPhoneNumber = consumerProfile.getName();
        if(Strings.isNullOrEmpty(customerNameOrPhoneNumber)){
            customerNameOrPhoneNumber = project.getPrivate()  ? customerProfileController.getCustomerPhoneNumber() : Constants.AnonymousUser;
        }
        String customerProfileImageUrl = consumerProfile.getImageUrl();
        String customerId = customerProfileController.getCustomerId();
        ChatParticipant result = new ChatParticipant(customerId, customerProfileImageUrl, customerNameOrPhoneNumber, false);
        return result;
    }

    public ChatParticipant buildConsumerChatParticipant(ProfessionalProjectDetails professionalProjectDetails){
        String customerId = professionalProjectDetails.getCustomerId();
        String customerNameOrPhoneNumber = professionalProjectDetails.getCustomerDisplayName();
        String customerProfileImageUrl = professionalProjectDetails.getCustomerProfileImage();
        ChatParticipant result = new ChatParticipant(customerId, customerProfileImageUrl, customerNameOrPhoneNumber, false);
        return result;
    }

}
