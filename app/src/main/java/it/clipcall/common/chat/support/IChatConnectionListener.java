package it.clipcall.common.chat.support;

/**
 * Created by dorona on 01/03/2016.
 */
public interface IChatConnectionListener {
    void onConnected();
}
