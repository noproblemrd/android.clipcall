package it.clipcall.common.chat.models;

import com.layer.sdk.messaging.Conversation;

/**
 * Created by omega on 3/4/2016.
 */
public class ConsversationContext {
    public ChatParticipant sender;
    public ChatParticipant receiver;
    public Conversation conversation;
}
