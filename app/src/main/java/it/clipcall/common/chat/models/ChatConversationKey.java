package it.clipcall.common.chat.models;

/**
 * Created by dorona on 03/04/2016.
 */
public class ChatConversationKey {


    private String receiverId;
    private String conversationId;

    public ChatConversationKey(String receiverId, String conversationId){
        this.receiverId = receiverId;
        this.conversationId = conversationId;
    }

    @Override
    public boolean equals(Object o) {
        if( !(o instanceof  ChatConversationKey))
            return false;

        ChatConversationKey key = (ChatConversationKey)o;
        return this.receiverId.equals(key.receiverId) && this.conversationId.equals(key.conversationId);

    }

    @Override
    public int hashCode() {
        return this.receiverId.hashCode() ^ this.conversationId.hashCode();
    }
}
