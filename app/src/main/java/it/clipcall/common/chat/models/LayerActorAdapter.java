package it.clipcall.common.chat.models;

import com.layer.sdk.messaging.Actor;

/**
 * Created by omega on 4/8/2016.
 */
public class LayerActorAdapter implements IActor {
    private final Actor actor;

    public LayerActorAdapter(Actor actor){

        this.actor = actor;
    }

    @Override
    public String getUserId() {
        return actor.getUserId();
    }

    @Override
    public String getName() {
        return actor.getName();
    }
}
