package it.clipcall.common.chat.models;

import android.net.Uri;

import java.nio.charset.Charset;
import java.util.Date;

/**
 * Created by omega on 4/8/2016.
 */
public class DummyMessage implements  IMessage{


    private final String message;
    private final Date sendAt;
    private final String sendBy;

    public DummyMessage(String message, Date sendAt, String sendBy){
        this.message = message;

        this.sendAt = sendAt;
        this.sendBy = sendBy;
    }
    @Override
    public Date getSentAt() {
        return sendAt;
    }

    @Override
    public String getMimeType() {
        return MessageType.Text.getMimeType();
    }

    @Override
    public byte[][] getRawData() {
        byte[][] data =  new byte[1][];
        data[0]  = message.getBytes(Charset.forName("UTF-8"));
        return data;
    }

    @Override
    public Date getReceivedAt() {
        return new Date();
    }

    @Override
    public IActor getSender() {
        return new IActor() {
            @Override
            public String getUserId() {
                return "1234";
            }

            @Override
            public String getName() {
                return "foo";
            }
        };
    }


    @Override
    public ChatPushMessage getMessageMetadata() {
        return null;
    }

    @Override
    public boolean isPartOfConversation(String userId) {
        return false;
    }

    @Override
    public Uri getConversationId() {
        return null;
    }

    @Override
    public boolean isDelivered(String participant) {
        return true;
    }

    @Override
    public boolean isRead(String participant) {
        return true;
    }

    @Override
    public void download(IMessageDownloaderListener listener) {

    }


    @Override
    public void markAsRead() {

    }

    @Override
    public String getSenderName() {
        return null;
    }
}
