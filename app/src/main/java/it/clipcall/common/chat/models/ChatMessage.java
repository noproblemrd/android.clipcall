package it.clipcall.common.chat.models;

import java.util.Date;

/**
 * Created by dorona on 25/02/2016.
 */
public abstract class ChatMessage {

    public String sender;
    public String senderImageUrl;
    public String status;
    public boolean sentByMe;
    public Date sentAt;
    public Date receivedAt;

    public abstract int getType();
}
