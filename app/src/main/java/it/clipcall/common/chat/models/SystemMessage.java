package it.clipcall.common.chat.models;

/**
 * Created by dorona on 30/05/2016.
 */
public class SystemMessage {

    public String message;
    public String mimeType;
    public String lead;
}
