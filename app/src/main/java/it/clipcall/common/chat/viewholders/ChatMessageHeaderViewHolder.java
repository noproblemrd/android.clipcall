package it.clipcall.common.chat.viewholders;

import android.view.View;
import android.widget.TextView;

import it.clipcall.R;
import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.GroupedMessages;
import it.clipcall.common.chat.support.IChatMessageListener;

public class ChatMessageHeaderViewHolder extends ChatMessageItemViewHolder {

    TextView headerTextView;
    public ChatMessageHeaderViewHolder(View itemView) {
        super(itemView);
        headerTextView = (TextView) itemView.findViewById(R.id.headerTextView);
        // Setup view holder.
        // You'd want some views to be optional, e.g. for header vs. normal.
    }

    @Override
    public void bind(ViewHolderChatBindingContext bindingContext) {
        GroupedMessages section = bindingContext.section;
        ChatMessage message = bindingContext.message;
        IChatMessageListener chatMessageListener = bindingContext.chatMessageListener;
        String title = section.getTitle();
        headerTextView.setText(title);
    }
}
