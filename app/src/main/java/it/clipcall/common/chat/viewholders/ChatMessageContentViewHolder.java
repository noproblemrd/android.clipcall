package it.clipcall.common.chat.viewholders;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import it.clipcall.R;
import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.GroupedMessages;
import it.clipcall.common.chat.support.IChatMessageListener;
import it.clipcall.infrastructure.support.buttons.AnimButton;
import it.clipcall.infrastructure.ui.images.CircleTransform;

public abstract class ChatMessageContentViewHolder extends ChatMessageItemViewHolder{

    public TextView senderTextView;

    public TextView messageDateTextView;

    public ImageView senderAvatarImage;

    public ViewGroup contentContainer;





    ImageView pictureImageView;
    TextView messageTextView;
    ViewGroup audioChatMessageContainer;
    AnimButton playAudioAnimButton;

    ViewGroup quoteContentContainer;
    TextView quoteNumberTextView;

    public ChatMessageContentViewHolder(View itemView) {
        super(itemView);
        this.senderTextView = (TextView)itemView.findViewById(R.id.senderTextView);

        this.messageDateTextView = (TextView)itemView.findViewById(R.id.messageDateTextView);

        this.senderAvatarImage = (ImageView)itemView.findViewById(R.id.senderAvatarImage);
        this.contentContainer = (ViewGroup)itemView.findViewById(R.id.contentContainer);


        this.pictureImageView = (ImageView)itemView.findViewById(R.id.pictureImageView);
        this.messageTextView = (TextView)itemView.findViewById(R.id.messageTextView);
        this.audioChatMessageContainer = (ViewGroup)itemView.findViewById(R.id.audioChatMessageContainer);
        this.playAudioAnimButton = (AnimButton)contentContainer.findViewById(R.id.playAudioAnimButton);

        this.quoteContentContainer = (ViewGroup)itemView.findViewById(R.id.quoteContentContainer);
        this.quoteNumberTextView = (TextView)this.quoteContentContainer.findViewById(R.id.quoteNumberTextView);
    }

    @Override
    public void bind(ViewHolderChatBindingContext bindingContext) {
        GroupedMessages section = bindingContext.section;
        ChatMessage message = bindingContext.message;
        IChatMessageListener chatMessageListener = bindingContext.chatMessageListener;
        
        if(Strings.isNullOrEmpty(message.senderImageUrl))
        {
            Picasso.with(itemView.getContext()).load(R.drawable.common_profile_missing)
                    .fit()
                    .transform(new CircleTransform())
                    .into(senderAvatarImage);
        }
        else{
            Picasso.with(itemView.getContext()).load(message.senderImageUrl)
                    .fit()
                    .transform(new CircleTransform())
                    .into(senderAvatarImage);
        }

        String senderText = message.sentByMe ? "Me" : message.sender;
        senderTextView.setText(senderText);
        String pattern ="hh:mm aa";
        Date date = message.sentAt != null ? message.sentAt : message.receivedAt;
        DateFormat formatter = new SimpleDateFormat(pattern);

        String formattedDate = formatter.format(date);
        messageDateTextView.setText(formattedDate);
    }
}