package it.clipcall.common.chat.services;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.common.base.Strings;
import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.ClipCallApplication;
import it.clipcall.common.activities.SplashActivity_;
import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.ChatPushMessage;
import it.clipcall.common.chat.models.IChatAware;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.models.PendingSystemChatMessage;
import it.clipcall.common.chat.models.SystemChatMessage;
import it.clipcall.common.chat.models.SystemMessage;
import it.clipcall.common.chat.support.factories.SystemMessageFactory;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.projects.controllers.ConsumerProjectsController;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.notifications.NotificationContext;
import it.clipcall.infrastructure.notifications.SimpleNotifier;
import it.clipcall.infrastructure.repositories.Repository2;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class SystemChatMessageHandler implements  IChatMessageHandler {

    int notificationId = 20;
    private final ILogger logger = LoggerFactory.getLogger(SystemChatMessageHandler.class.getSimpleName());

    @Inject
    ConsumerProjectsController consumerProjectsController;

    @Inject
    AuthenticationManager authenticationManager;

    @Inject
    Repository2 repository;

    @Inject
    ClipCallApplication application;

    @Inject
    SimpleNotifier notifier;

    @Inject
    RouteParams routeParams;

    @Inject
    RoutingService routingService;

    SystemMessageFactory systemMessageFactory;

    @Inject
    public SystemChatMessageHandler(Gson gson){
        systemMessageFactory = new SystemMessageFactory(gson);
    }

    @Override
    public boolean canHandleMessage(IMessage message) {
        return message.getMimeType().equals(MessageType.SystemProInterested.getMimeType());
    }

    @Override
    public void handleMessage(final IMessage message) {
        logger.debug("handleMessage - handling chat message of type %s", message.getMimeType());
        final ChatMessage chatMessage = systemMessageFactory.createChatMessageCore(message);
        if(!(chatMessage instanceof SystemChatMessage))
        {
            logger.debug("handleMessage - systemMessageFactory failed to create SystemChat message");
            return;
        }


        final SystemChatMessage systemChatMessage = (SystemChatMessage) chatMessage;
        final SystemMessage systemMessage = systemChatMessage.getSystemMessage();
        final ChatPushMessage messageMetadata = message.getMessageMetadata();
        final Activity currentActivity = application.getCurrentActivity();
        if(currentActivity != null){
            logger.debug("handleChatMessage: handling push notification within the app");
            handlePushWithinTheApp(application, currentActivity, messageMetadata, systemMessage);
            return;
        }




        final Intent intent = new Intent(application.getApplicationContext(), SplashActivity_.class);



        AsyncTask.THREAD_POOL_EXECUTOR.execute(new Runnable() {
                                                   @Override
                                                   public void run() {
                                                       authenticationManager.switchToConsumerMode();
                                                       List<ProjectEntityBase> customerProjects = consumerProjectsController.getCustomerProjects(true);
                                                       ProjectEntityBase project = Lists.firstOrDefault(customerProjects, new ICriteria<ProjectEntityBase>() {
                                                           @Override
                                                           public boolean isSatisfiedBy(ProjectEntityBase candidate) {
                                                               return candidate.getId().equals(systemMessage.lead);
                                                           }
                                                       });

                                                       if(project == null)
                                                           return;

                                                       PendingSystemChatMessage pendingMessage = repository.getSingle(PendingSystemChatMessage.class);
                                                       if(pendingMessage == null){
                                                           pendingMessage = new PendingSystemChatMessage();
                                                       }
                                                       pendingMessage.setProjectEntity((ProjectEntity)project);
                                                       repository.update(pendingMessage);

                                                       final NotificationContext notificationContext = new NotificationContext();
                                                       notificationContext.message = messageMetadata.data;
                                                       notificationContext.smallPictureImageUrl = Strings.isNullOrEmpty(messageMetadata.senderImageUrl) ? "https://storage.googleapis.com/clipcallmobile/consumer_profile_logo_placeholder3.png" : messageMetadata.senderImageUrl;
                                                       notificationContext.bigPictureImageUrl = Strings.isNullOrEmpty(messageMetadata.senderImageUrl) ? "https://storage.googleapis.com/clipcallmobile/consumer_profile_logo_placeholder3.png" : messageMetadata.senderImageUrl;
                                                       notificationContext.title = messageMetadata.senderName + "";
                                                       notificationContext.contentTitle = messageMetadata.data;
                                                       notificationContext.bigContentTitle = messageMetadata.data;

                                                       notificationContext.notificationId = notificationId;
                                                       notificationContext.notificationTag = message.getConversationId().toString();
                                                       authenticationManager.switchToConsumerMode();


                                                       notificationContext.intent = intent;
                                                       logger.debug("handleChatMessage: about to create notification message");
                                                       notifier.notify(notificationContext);
                                                       return;
                                                   }
                                               });






    }

    private void handlePushWithinTheApp(final ClipCallApplication application, final Activity currentActivity, final ChatPushMessage  pushMessage, final SystemMessage systemMessage) {
        if(currentActivity instanceof IChatAware)
            return;

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                String message = pushMessage.data;
                application.showMessage(message, new Runnable() {
                    @Override
                    public void run() {


                        AsyncTask.THREAD_POOL_EXECUTOR.execute(new Runnable() {
                            @Override
                            public void run() {
                                Log.d("LayerRemotePush", "action: "+ pushMessage.data);
                                if(authenticationManager.getCustomerId().equals(pushMessage.receiverId) && !pushMessage.receiverIsPro){
                                    //message sent to consumer
                                    authenticationManager.switchToConsumerMode();
                                    List<ProjectEntityBase> customerProjects = consumerProjectsController.getCustomerProjects(true);
                                    ProjectEntityBase project = Lists.firstOrDefault(customerProjects, new ICriteria<ProjectEntityBase>() {
                                        @Override
                                        public boolean isSatisfiedBy(ProjectEntityBase candidate) {
                                            return candidate.getId().equals(systemMessage.lead);
                                        }
                                    });

                                    if(project == null)
                                        return;

                                    NavigationContext navigationContext = new NavigationContext();
                                    navigationContext.routingContext = new RoutingContext(currentActivity);


                                    routeParams.reset();
                                    routeParams.setParam("project", project);
                                    routingService.routeTo(Routes.ConsumerProjectDetailsRoute, navigationContext);


                                }
                            }
                        });


                    }
                }, true);
            }
        });
    }
}
