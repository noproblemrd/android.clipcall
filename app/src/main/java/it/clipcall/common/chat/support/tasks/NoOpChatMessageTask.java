package it.clipcall.common.chat.support.tasks;

import it.clipcall.common.chat.models.ChatMessage;

/**
 * Created by dorona on 28/06/2016.
 */
public class NoOpChatMessageTask implements IChatMessageTask {
    @Override
    public void execute(ChatMessage chatMessage, IChatMessageContext messageContext) {

    }
}
