package it.clipcall.common.chat.models;

import it.clipcall.professional.leads.models.NewQuoteData;

/**
 * Created by dorona on 25/02/2016.
 */
public class QuoteChatMessage extends ChatMessage {

    private final NewQuoteData quoteData;

    public QuoteChatMessage(NewQuoteData quoteData){
        this.quoteData = quoteData;
    }

    public NewQuoteData getQuoteData(){
        return quoteData;
    }


    @Override
    public int getType() {
        return MessageType.SendQuote.getValue();
    }

    @Override
    public String toString() {
        return quoteData.quote.getDescription();
    }
}

