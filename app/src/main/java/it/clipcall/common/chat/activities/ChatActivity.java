package it.clipcall.common.chat.activities;

import android.Manifest;
import android.app.Activity;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.common.base.Strings;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.parceler.Parcels;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.common.chat.models.IChatAware;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.models.eTypeMode;
import it.clipcall.common.chat.presenters.ChatPresenter;
import it.clipcall.common.chat.support.ChatMessagesAdapter;
import it.clipcall.common.chat.support.IChatMessageListener;
import it.clipcall.common.chat.support.ImageTappedListener;
import it.clipcall.common.chat.support.factories.MessageContext;
import it.clipcall.common.chat.views.IChatView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.TransparentActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.commands.EmptyCallBack;
import it.clipcall.infrastructure.commands.PickImageCommand;
import it.clipcall.infrastructure.commands.RequestPermissionsCommandDecorator;
import it.clipcall.infrastructure.support.animations.CircularRevealerProvider;
import it.clipcall.infrastructure.support.animations.ICircularRevealerAnimator;
import it.clipcall.infrastructure.support.buttons.AnimButton;
import it.clipcall.infrastructure.ui.dialogs.SnackbarContext;
import it.clipcall.infrastructure.ui.dialogs.SnackbarService;

@EActivity(R.layout.consumer_chat_activity2)
public class ChatActivity extends BaseActivity implements IHasToolbar, IChatView, IChatAware,ImageTappedListener, View.OnLayoutChangeListener, IChatMessageListener {

    private boolean isAttachmentMenuVisible = false;
    private ChatMessagesAdapter adapter;

    @Extra
    Parcelable sender;

    @Extra
    Parcelable receiver;

    @Extra
    boolean canSendQuote;

    @Extra
    String conversationId;


    @Extra
    boolean isFromNotification;

    @ViewById
    Toolbar toolbar;

    @ViewById
    RecyclerView chatMessageListView;

    @ViewById
    EditText chatMessageEditText;

    @ViewById
    ViewGroup revealFrameContainer;

    @ViewById
    Button sendImageButton;

    @ViewById
    AnimButton sendButton;

    @ViewById
    ProgressBar loadingMessagesProgressBar;

    @Inject
    ChatPresenter presenter;


    @ViewById
    ViewGroup recordingContainer;

    @ViewById
    ViewGroup mainholder;


    @ViewById
    TextView recordingTimeTextView;


    @ViewById
    View recordingIndicator;

    @Inject
    SnackbarService snackbarService;

    @AfterViews
    void afterViews(){
        revealFrameContainer.setVisibility(View.INVISIBLE);
        adapter = new ChatMessagesAdapter(this, this);
        chatMessageListView.setAdapter(adapter);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        //linearLayoutManager.setReverseLayout(true);
        chatMessageListView.setLayoutManager(linearLayoutManager);
        chatMessageListView.addOnLayoutChangeListener(this);
        recordingContainer.setVisibility(View.GONE);
        chatMessageEditText.setVisibility(View.VISIBLE);
        presenter.bindView(this);
        ChatParticipant senderParticipant = Parcels.unwrap(sender);
        ChatParticipant receiverParticipant = Parcels.unwrap(receiver);
        presenter.setSender(senderParticipant);
        presenter.setReceiver(receiverParticipant);
        presenter.setConversationId(conversationId);

        presenter.initialize();

    }

    @Override
    public String getViewTitle() {
        return "CHAT";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (canSendQuote) {
            inflater.inflate(R.menu.chat_pro_menu_items, menu);
        }else{
            inflater.inflate(R.menu.chat_menu_items, menu);
        }


        return super.onCreateOptionsMenu(menu);
    }

    @Click(R.id.attachmentGalleryImageButton)
    void onAttachmentPhotoTapped(){
        toggleChatAttachment();
        final Activity self = this;
        PickImageCommand command = new PickImageCommand(self, TransparentActivity.Gallery);
        RequestPermissionsCommandDecorator decorator = new RequestPermissionsCommandDecorator(command,self,"Gallery", "Gallery", Manifest.permission.WRITE_EXTERNAL_STORAGE);
        decorator.execute(new EmptyCallBack<Bitmap>() {
            @Override
            public void onSuccess(Bitmap result) {
                Log.d("CHAT", result.toString());
                uploadImage(result);
                return;
            }
        });
    }

    @Click(R.id.attachmentCameraImageButton)
    void onAttachmentCameraTapped(){
        toggleChatAttachment();
        final Activity self = this;
        PickImageCommand command = new PickImageCommand(self, TransparentActivity.Camera);
        RequestPermissionsCommandDecorator decorator = new RequestPermissionsCommandDecorator(command,self,"Camera", "Camera", Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
        decorator.execute(new EmptyCallBack<Bitmap>() {
            @Override
            public void onSuccess(Bitmap result) {
                Log.d("CHAT", result.toString());
                uploadImage(result);
                return;
            }
        });
    }

    @UiThread
    @Override
    public void onMessageReceived(MessageContext messageContext, IMessage message) {
        adapter.addMessage(messageContext, message);
        chatMessageListView.scrollToPosition(adapter.getTotalMessages()-1);
    }

    @UiThread
    @Override
    public void onMessageSent(MessageContext messageContext, IMessage message) {
        adapter.addMessage(messageContext, message);
        chatMessageListView.scrollToPosition(adapter.getTotalMessages()-1);

    }

    @Override
    public void showMessages(MessageContext context, List<IMessage> messages) {
        adapter.setMessages(context, messages);
        chatMessageListView.scrollToPosition(adapter.getTotalMessages()-1);
    }

    @Override
    public void showHeader(ChatParticipant sender, ChatParticipant receiver) {
        toolbar.setTitle(receiver.nameOrPhoneNumber);
    }

    @UiThread
    @Override
    public void setTypingMode(eTypeMode typeMode) {

    }

    @UiThread
    @Override
    public void clearCurrentMessage() {
        chatMessageEditText.setText("");
    }

    @UiThread
    @Override
    public void onRecordingStarted() {
        sendButton.goToState(AnimButton.THIRD_STATE);
        recordingContainer.setVisibility(View.VISIBLE);
        chatMessageEditText.setVisibility(View.GONE);
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.flash_animaiton);
        recordingIndicator.startAnimation(myFadeInAnimation);
    }

    @UiThread
    @Override
    public void onRecordingEnded() {
        sendButton.goToState(AnimButton.FIRST_STATE);
        recordingContainer.setVisibility(View.GONE);
        chatMessageEditText.setVisibility(View.VISIBLE);
    }

    @UiThread
    @Override
    public void showRecordingTime(int totalTime) {

        int minutes = (int) (totalTime / (60));
        int seconds = (int) (totalTime % 60);
        String str = String.format("%d:%02d", minutes, seconds);
        recordingTimeTextView.setText(str);
    }

    @UiThread
    @Override
    public void onRecordingCanceled() {
        sendButton.goToState(AnimButton.FIRST_STATE);
        recordingContainer.setVisibility(View.GONE);
        chatMessageEditText.setVisibility(View.VISIBLE);
    }

    @Override
    public void showFailedSendingMessage() {
        Snackbar.make(mainholder, "Oops...Failed sending message.", Snackbar.LENGTH_LONG).show();
    }

    @UiThread
    @Override
    public void showGeneralError() {
        Snackbar.make(mainholder, "Oops...Something went wrong. Please try again later", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showLoadingConversation(boolean isLoading) {
        int visibility = isLoading ? View.VISIBLE : View.GONE;
        loadingMessagesProgressBar.setVisibility(visibility);
    }

    @Override
    public void showReceivedMessage(String sender) {
        String messge = "You've recieved a message";
        if(sender != null){
            messge += " from " + sender;
        }
        Snackbar.make(mainholder, messge, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showMessageContainsNonPermittedText() {
        String message = getString(R.string.terms_of_service_violation);
        SnackbarContext context = new SnackbarContext(mainholder,message,null, Snackbar.LENGTH_LONG,5);
        snackbarService.show(context);
    }


    @Override
    protected void onResume() {
        super.onResume();
        presenter.restart();
    }

    @Click(R.id.sendButton)
    void sendMessageTapped(){
        presenter.applyAction();
    }

    @Click(R.id.cancelRecordingButton)
    void cancelRecordingTapped(){
        presenter.cancelRecording();
    }

    @TextChange(R.id.chatMessageEditText)
    void chatTextMessageChanged(CharSequence text, TextView hello, int before, int start, int count){
        if(text == null)
            return;

        if(Strings.isNullOrEmpty(text.toString())){
            sendButton.goToState(AnimButton.FIRST_STATE);
           presenter.setCurrentMessage("");
            return;
        }

        sendButton.goToState(AnimButton.SECOND_STATE);
        presenter.setCurrentMessage(text.toString());
    }

    void uploadImage(Bitmap bitmap){
        presenter.sendMessage(bitmap, MessageType.Image.getMimeType());
    }

    @Background
    void uploadAudioRecording(String fileName){
        presenter.sendMessage(fileName, MessageType.Audio.getMimeType());
    }

    MediaRecorder recorder;


    private String fileName;

    @Click(R.id.startRecordingButton)
    void startRecording()
    {
        try {
            SimpleDateFormat timeStampFormat = new SimpleDateFormat(
                    "yyyy-MM-dd-HH.mm.ss");
            fileName = "audio_" + timeStampFormat.format(new Date()) + ".mp4";
            recorder = new MediaRecorder();
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            recorder.setOutputFile("/sdcard/"+fileName);
            recorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        recorder.start();
    }


    @Click(R.id.stopRecordingButton)
    void stopRecording() {
        recorder.stop();
        recorder.release();
    }

    @Click(R.id.playRecordingButton)
    void playRecording(){
        try{

           uploadAudioRecording(fileName);
            MediaPlayer myPlayer = new MediaPlayer();
            myPlayer.setDataSource("/sdcard/"+fileName);
            myPlayer.prepare();
            myPlayer.start();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.shutDown();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.attachmentMenuItem:
                toggleChatAttachment();
                return true;
            case R.id.sendQuoteMenuItem:
                presenter.sendQuote();
                return true;


        }
        return super.onOptionsItemSelected(item);
    }

    private void toggleChatAttachment(){
        CircularRevealerProvider provider = new CircularRevealerProvider();
        ICircularRevealerAnimator circularRevealer = provider.provideCircularRevealer();
        if(isAttachmentMenuVisible){
            circularRevealer.unreveal(revealFrameContainer);
            isAttachmentMenuVisible = false;
        }
        else{
            circularRevealer.reveal(revealFrameContainer);
            isAttachmentMenuVisible = true;

        }
    }

    @Override
    public void onBackPressed() {
        if(isFromNotification)
        {
            presenter.routeToHome();
            finish();
            return;
        }

        super.onBackPressed();
    }

    @Override
    public void onLayoutChange(View v, int left, int top, int right, final int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        if ( bottom < oldBottom) {
            chatMessageListView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    chatMessageListView.smoothScrollToPosition(bottom);
                }
            }, 100);
        }
    }

    @Override
    public void onImageTapped(Bitmap bitmap, Bundle uiBundle) {
        presenter.showImage(bitmap, uiBundle);
    }

    @Override
    public void onMessageTapped(ChatMessage chatMessage) {
        presenter.onMessageTapped(chatMessage);
    }
}
