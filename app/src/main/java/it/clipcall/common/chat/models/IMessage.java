package it.clipcall.common.chat.models;

import android.net.Uri;

import java.util.Date;

/**
 * Created by omega on 4/8/2016.
 */
public interface IMessage {
    Date getSentAt();
    String getMimeType();
    byte[][] getRawData();

    Date getReceivedAt();

    IActor getSender();

    ChatPushMessage getMessageMetadata();

    boolean isPartOfConversation(String userId);

    Uri getConversationId();

    boolean isDelivered(String participant);
    boolean isRead(String participant);

    void download(IMessageDownloaderListener listener);

    void markAsRead();

    String getSenderName();
}
