package it.clipcall.common.chat.viewholders;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;

import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.GroupedMessages;
import it.clipcall.common.chat.models.ImageChatMessage;
import it.clipcall.common.chat.support.IChatMessageListener;
import it.clipcall.common.chat.support.ImageTappedListener;

public class ChatMessageImageContentViewHolder extends ChatMessageContentViewHolder{

    public ChatMessageImageContentViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bind(ViewHolderChatBindingContext bindingContext) {
        super.bind(bindingContext);
        GroupedMessages section = bindingContext.section;
        ChatMessage message = bindingContext.message;
        IChatMessageListener chatMessageListener = bindingContext.chatMessageListener;


        pictureImageView.setVisibility(View.VISIBLE);
        messageTextView.setVisibility(View.GONE);
        audioChatMessageContainer.setVisibility(View.GONE);
        quoteContentContainer.setVisibility(View.GONE);

        final ImageChatMessage imageMessage = (ImageChatMessage) message;
        pictureImageView.setImageBitmap(imageMessage.image);
        pictureImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context instanceof ImageTappedListener){
                    ImageTappedListener imageTappedListener = (ImageTappedListener) context;
                    Bundle bundle = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ActivityOptionsCompat options = ActivityOptionsCompat.
                                makeSceneTransitionAnimation((Activity)context, pictureImageView, "image_transition");
                        bundle = options.toBundle();
                    }

                    imageTappedListener.onImageTapped(imageMessage.image, bundle);
                }
            }
        });
    }
}