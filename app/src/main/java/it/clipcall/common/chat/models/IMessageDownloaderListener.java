package it.clipcall.common.chat.models;

/**
 * Created by dorona on 10/05/2016.
 */
public interface IMessageDownloaderListener {

    void onSuccess(IMessage message);
    void onFailure();

}
