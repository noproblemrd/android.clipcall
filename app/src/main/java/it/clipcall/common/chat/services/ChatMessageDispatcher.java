package it.clipcall.common.chat.services;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.common.chat.models.ChatPushMessage;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;


public class ChatMessageDispatcher {


    private final ILogger logger = LoggerFactory.getLogger(ChatMessageDispatcher.class.getSimpleName());
    private final List<IChatMessageHandler> messageHandlers = new ArrayList<>();

    public void addHandler(IChatMessageHandler handler){
         messageHandlers.add(handler);
    }

    public ChatMessageDispatcher(){

    }

    public void dispatchMessage(IMessage message){
        if(message == null){
            logger.error("dispatchMessage: message cannot be null.");
            return;
        }

        ChatPushMessage messageMetadata = message.getMessageMetadata();
        if(messageMetadata == null){
            logger.error("dispatchMessage: cannot extract message metadata");
            return;
        }

        List<IChatMessageHandler> handlers = new ArrayList<>(messageHandlers);

        for (IChatMessageHandler handler : handlers){
            if(handler.canHandleMessage(message)){
                handler.handleMessage(message);
                return;
            }
        }

        logger.error("dispatchMessage: could not find any handlers to handle the message of type %s", message.getMimeType());
    }
}
