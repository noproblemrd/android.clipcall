package it.clipcall.common.chat.models;

import com.layer.sdk.messaging.Conversation;

import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.support.criterias.ICriteria;

/**
 * Created by dorona on 31/05/2016.
 */
public class NotifyChatCreatedCriteria implements ICriteria<Conversation> {

    private final ILogger logger  = LoggerFactory.getLogger(NotifyChatCreatedCriteria.class.getSimpleName());

    private final String lastMessageType;

    public NotifyChatCreatedCriteria(String lastMessageType){

        this.lastMessageType = lastMessageType;
    }

    @Override
    public boolean isSatisfiedBy(Conversation candidate) {
        logger.debug("notify chat created criteria evaluated");
        if(lastMessageType.equals(MessageType.SystemProInterested.getMimeType()))
        {
            logger.debug("notify chat created criteria is not satisfied");
            return false;
        }

        Integer totalMessageCount = candidate.getTotalMessageCount();
        if(totalMessageCount == null)
            return true;

        logger.debug("totalMessageCount is "+ totalMessageCount == null ? "null" : totalMessageCount.toString());

        return totalMessageCount != null && (totalMessageCount == 1 || totalMessageCount == 2);
    }
}
