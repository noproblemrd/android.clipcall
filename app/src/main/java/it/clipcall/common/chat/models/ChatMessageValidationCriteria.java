package it.clipcall.common.chat.models;

import com.google.common.base.Strings;

import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.infrastructure.support.criterias.OrCriteria;

/**
 * Created by dorona on 16/08/2016.
 */

public class  ChatMessageValidationCriteria implements ICriteria<String> {

    private final ICriteria<String> criteria;

    public ChatMessageValidationCriteria(){
        ChatMessageBlockedKeywordsValidationCriteria blockedKeywordsCriteria = new ChatMessageBlockedKeywordsValidationCriteria();
        ChatMessageContainsPhoneNumberValidationCriteria containsPhoneNumberCriteria = new ChatMessageContainsPhoneNumberValidationCriteria();
        ChatMessageContainsEmailValidationCriteria containsEmailCriteria = new ChatMessageContainsEmailValidationCriteria();
        ChatMessageContainsWebUrlValidationCriteria containsWebUrlCriteria = new ChatMessageContainsWebUrlValidationCriteria();
        this.criteria  = new OrCriteria<>(new OrCriteria(containsEmailCriteria,containsWebUrlCriteria), new OrCriteria(blockedKeywordsCriteria, containsPhoneNumberCriteria));
    }

    public boolean isSatisfiedBy(String candidate){
        if(Strings.isNullOrEmpty(candidate))
            return false;

        boolean isSatisfiedByOriginalText =  this.criteria.isSatisfiedBy(candidate);
        if(isSatisfiedByOriginalText)
            return true;

        String modifiedText = candidate.replace("o", "0").replace("O","0");
        boolean isSatisfiedByModifiedText =  this.criteria.isSatisfiedBy(modifiedText);
        if(isSatisfiedByModifiedText)
            return true;

        return false;

    }
}
