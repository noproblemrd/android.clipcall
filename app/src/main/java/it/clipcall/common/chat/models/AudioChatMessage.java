package it.clipcall.common.chat.models;

/**
 * Created by dorona on 25/02/2016.
 */
public class AudioChatMessage extends ChatMessage {

    public byte[] audioData;
    public AudioChatMessage(byte[] data) {
        this.audioData = data;
    }

    @Override
    public int getType() {
        return MessageType.Audio.getValue();
    }
}

