package it.clipcall.common.chat.models;

import org.joda.time.LocalTime;

import it.clipcall.infrastructure.support.criterias.ICriteria;

/**
 * Created by dorona on 10/04/2016.
 */
public class SentBySamePersonOnSameMinuteCriteria implements ICriteria<ChatMessage> {


    private final TextChatMessage textChatMessage;

    public SentBySamePersonOnSameMinuteCriteria(TextChatMessage textChatMessage){

        this.textChatMessage = textChatMessage;

    }

    @Override
    public boolean isSatisfiedBy(ChatMessage candidate) {
        if(!(candidate instanceof  CompositeTextChatMessage))
            return false;


        if(!textChatMessage.sender.equals(candidate.sender))
            return false;

        LocalTime messageTime = new LocalTime(textChatMessage.sentAt == null ? textChatMessage.receivedAt : textChatMessage.sentAt);
        LocalTime candidateTime = new LocalTime(candidate.sentAt == null ? candidate.receivedAt : candidate.sentAt);

        return messageTime.getHourOfDay() == candidateTime.getHourOfDay() && messageTime.getMinuteOfHour() == candidateTime.getMinuteOfHour();

    }
}
