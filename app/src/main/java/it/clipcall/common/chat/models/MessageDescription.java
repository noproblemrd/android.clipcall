package it.clipcall.common.chat.models;

/**
 * Created by omega on 3/4/2016.
 */
public class MessageDescription {

    public MessageDescription(String messageType, byte[] messageData){
        this.messageType = messageType;
        this.messageData = messageData;
    }

    public String messageType;

    public byte[] messageData;

    public String pushData;

    public String pushTypeText;
}
