package it.clipcall.common.chat.models;

/**
 * Created by dorona on 01/08/2016.
 */

public class ClipCallBotChatMessage extends ChatMessage {

    private final ClipCallBotMessage botMessage;

    public ClipCallBotChatMessage(ClipCallBotMessage botMessage){
        this.botMessage = botMessage;
    }

    public ClipCallBotMessage getSystemMessage(){
        return botMessage;
    }

    @Override
    public int getType() {
        return MessageType.ClipCallBot.getValue();
    }

    @Override
    public String toString() {
        return String.format("pro : %s, customer: %s", botMessage.pro, botMessage.customer);
    }
}