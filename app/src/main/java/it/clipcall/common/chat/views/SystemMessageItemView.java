package it.clipcall.common.chat.views;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import it.clipcall.R;
import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.SystemChatMessage;

public class SystemMessageItemView extends ChatMessageItemView {

    TextView messageTextView;

    public SystemMessageItemView(Context context) {
        super(context);
        init();
    }

    @Override
    public void bindExtra(ChatMessage message) {
        if(!(message instanceof SystemChatMessage)){
            Log.e("Chat","invalid chat message associated with text chat message item view");
            return;
        }
        SystemChatMessage textChatMessage = (SystemChatMessage) message;
        messageTextView.setText(textChatMessage.text);
    }

    private void init(){
        inflate(getContext(), R.layout.text_chat_message_content, contentContainer);
        this.messageTextView = (TextView)contentContainer.findViewById(R.id.messageTextView);
    }


}
