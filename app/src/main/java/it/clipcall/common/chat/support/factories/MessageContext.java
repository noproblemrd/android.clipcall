package it.clipcall.common.chat.support.factories;

import com.layer.sdk.messaging.Conversation;

import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.common.chat.models.IConversation;

/**
 * Created by dorona on 02/03/2016.
 */
public class MessageContext {
    //public Conversation conversation;
    public String authenticatedUserId;
    public ChatParticipant me;
    public ChatParticipant otherSide;
    public IConversation conversation;
}
