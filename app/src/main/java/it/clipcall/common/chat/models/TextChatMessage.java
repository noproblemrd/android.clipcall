package it.clipcall.common.chat.models;

/**
 * Created by dorona on 25/02/2016.
 */
public class TextChatMessage extends ChatMessage {

    public String text;

    public TextChatMessage(String text){
        this.text = text;
    }

    @Override
    public int getType() {
        return MessageType.Text.getValue();
    }

    @Override
    public String toString() {
        return text;
    }
}

