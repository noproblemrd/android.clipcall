package it.clipcall.common.chat.support.factories;

import android.view.View;

import com.google.common.base.Strings;

import java.io.UnsupportedEncodingException;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.models.TextChatMessage;
import it.clipcall.common.chat.support.tasks.IChatMessageTask;
import it.clipcall.common.chat.support.tasks.NoOpChatMessageTask;
import it.clipcall.common.chat.viewholders.ChatMessageContentViewHolder;
import it.clipcall.common.chat.viewholders.ChatMessageTextContentViewHolder;


@Singleton
public class TextMessageFactory extends MessageFactoryBase {

    @Inject
    public TextMessageFactory(){

    }

    @Override
    public ChatMessage createChatMessageCore(IMessage message) {
        String text = "";

        byte[][] rawData = message.getRawData();

        try {
            text += new String(rawData[0], "UTF-8") + "\n";
        } catch (UnsupportedEncodingException e) {

        }

        if(Strings.isNullOrEmpty(text))
            return null;

        ChatMessage chatMessage = new TextChatMessage(text);
        return chatMessage;
    }

    @Override
    public MessageDescription createMessageDescription(Object data) {
        if(!(data instanceof String))
            return null;

        String text = (String) data;
        byte[] textData;
        try {
            textData = text.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }

        MessageDescription messageDescription = new MessageDescription(getMessageType().getMimeType(), textData);
        messageDescription.pushData = text;
        messageDescription.pushTypeText = "text message";
        return  messageDescription;
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.Text;
    }

    @Override
    public ChatMessageContentViewHolder createViewHolder(View v) {
        return new ChatMessageTextContentViewHolder(v);
    }

    @Override
    public IChatMessageTask createChatMessageTask() {
        return new NoOpChatMessageTask();
    }

    @Override
    public String getMessageDescription(IMessage message) {
        TextChatMessage textChatMessage = (TextChatMessage) createChatMessageCore(message);
        return textChatMessage.text;
    }
}
