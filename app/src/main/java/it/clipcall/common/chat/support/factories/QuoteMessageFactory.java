package it.clipcall.common.chat.support.factories;

import android.view.View;

import com.google.common.base.Strings;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.models.QuoteChatMessage;
import it.clipcall.common.chat.support.tasks.IChatMessageTask;
import it.clipcall.common.chat.support.tasks.QuoteChatMessageTask;
import it.clipcall.common.chat.viewholders.ChatMessageContentViewHolder;
import it.clipcall.common.chat.viewholders.QuoteChatMessageContentViewHolder;
import it.clipcall.professional.leads.models.NewQuoteData;


@Singleton
public class QuoteMessageFactory extends MessageFactoryBase {

    private final Gson gson;
    @Inject
    Provider<QuoteChatMessageTask>  quoteChatMessageTaskProvider;

    @Inject
    public QuoteMessageFactory(Gson gson){
        this.gson = gson;
    }

    @Override
    public MessageDescription createMessageDescription(Object data) {
        if(!(data instanceof NewQuoteData))
            return null;

        NewQuoteData quote = (NewQuoteData) data;
        String json = gson.toJson(quote);

        byte[] textData;
        try {
            textData = json.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }

        MessageDescription messageDescription = new MessageDescription(getMessageType().getMimeType(), textData);
        messageDescription.pushData = "";
        messageDescription.pushTypeText = "text message";
        return  messageDescription;
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.SendQuote;
    }

    @Override
    public ChatMessageContentViewHolder createViewHolder(View v) {
        return new QuoteChatMessageContentViewHolder(v);
    }

    @Override
    public IChatMessageTask createChatMessageTask() {
        return quoteChatMessageTaskProvider.get();
    }

    @Override
    public String getMessageDescription(IMessage message) {
        return "Sent you a quote";
    }


    @Override
    public ChatMessage createChatMessageCore(IMessage message) {
        String text = "";

        byte[][] rawData = message.getRawData();

        try {
            text = new String(rawData[0], "UTF-8");
        } catch (UnsupportedEncodingException e) {

        }

        if(Strings.isNullOrEmpty(text))
            return null;



        NewQuoteData quoteData = gson.fromJson(text, NewQuoteData.class);
        ChatMessage chatMessage = new QuoteChatMessage(quoteData);
        return chatMessage;
    }

}
