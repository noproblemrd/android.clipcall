package it.clipcall.common.chat.support.tasks;

import it.clipcall.infrastructure.routing.models.NavigationContext;

/**
 * Created by dorona on 28/06/2016.
 */
public class ChatMessageNavigationContext implements IChatMessageContext {

    private final NavigationContext navigationContext;

    public ChatMessageNavigationContext(NavigationContext navigationContext){

        this.navigationContext = navigationContext;
    }

    public NavigationContext getNavigationContext() {
        return navigationContext;
    }
}
