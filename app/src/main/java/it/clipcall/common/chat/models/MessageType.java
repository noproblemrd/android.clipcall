package it.clipcall.common.chat.models;

/**
 * Created by omega on 3/4/2016.
 */
public class MessageType {


    private final int value;
    private final String mimeType;

    private MessageType(int value, String mimeType){
        this.value = value;
        this.mimeType = mimeType;
    }


    public static final MessageType Image = new MessageType(5,"image/png");
    public static final MessageType Audio = new MessageType(6,"audio/mp4");
    public static final MessageType Video = new MessageType(7,"video/mp4");
    public static final MessageType Text = new MessageType(8,"text/plain");
    public static final MessageType SystemProInterested = new MessageType(9,"system-message/pro-interested");
    public static final MessageType VideoUrl = new MessageType(10,"video/url");
    public static final MessageType AudioUrl = new MessageType(11,"phoneCall/url");
    public static final MessageType Payment = new MessageType(12,"payment/info");
    public static final MessageType SendQuote = new MessageType(13,"quote/send-quote");
    public static final MessageType RequestQuote = new MessageType(14,"quote/request-quote");
    public static final MessageType BookQuote = new MessageType(15,"quote/book-quote");
    public static final MessageType ClipCallBot = new MessageType(16,"system-message/clipcall-bot");

    public String getMimeType(){
        return this.mimeType;
    }

    public int getValue(){
        return this.value;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof MessageType))
            return false;

        MessageType other = (MessageType) o;
        return this.value == other.value;
    }

    @Override
    public int hashCode() {
        return value;
    }
}
