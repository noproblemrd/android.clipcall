package it.clipcall.common.chat.viewholders;

import android.Manifest;
import android.view.View;

import it.clipcall.common.chat.models.AudioChatMessage;
import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.GroupedMessages;
import it.clipcall.common.chat.support.IChatMessageListener;
import it.clipcall.infrastructure.commands.EmptyCallBack;
import it.clipcall.infrastructure.commands.PlayMediaCommand;
import it.clipcall.infrastructure.commands.RequestPermissionsCommand;
import it.clipcall.infrastructure.support.buttons.AnimButton;

public class ChatMessageAudioContentViewHolder extends ChatMessageContentViewHolder implements View.OnClickListener{

    private PlayMediaCommand playMediaCommand;
    private RequestPermissionsCommand requestPermissionsCommand;

    public ChatMessageAudioContentViewHolder(View itemView) {
        super(itemView);
        requestPermissionsCommand = new RequestPermissionsCommand(context,"Record audio","Record audio", Manifest.permission.RECORD_AUDIO,Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    @Override
    public void bind(ViewHolderChatBindingContext bindingContext) {
        super.bind(bindingContext);
        GroupedMessages section = bindingContext.section;
        ChatMessage message = bindingContext.message;
        IChatMessageListener chatMessageListener = bindingContext.chatMessageListener;
        pictureImageView.setVisibility(View.GONE);
        messageTextView.setVisibility(View.GONE);
        quoteContentContainer.setVisibility(View.GONE);



        audioChatMessageContainer.setVisibility(View.VISIBLE);
        AudioChatMessage audioChatMessage = (AudioChatMessage) message;
        playMediaCommand = new PlayMediaCommand(context, audioChatMessage.audioData);
        playAudioAnimButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        requestPermissionsCommand.execute(new EmptyCallBack<Void>(){
            @Override
            public void onSuccess(Void result) {
                playMediaCommand.execute();
                if(playMediaCommand.isPlaying())
                    playAudioAnimButton.goToState(AnimButton.SECOND_STATE);
                else
                    playAudioAnimButton.goToState(AnimButton.FIRST_STATE);
            }
        });
    }
}