package it.clipcall.common.chat.models;

/**
 * Created by omega on 3/7/2016.
 */
public enum eTypeMode {
    ReadyToRecord,
    Typing,
    Recording
}
