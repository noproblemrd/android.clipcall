package it.clipcall.common.chat.views;

import java.util.List;

import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.eTypeMode;
import it.clipcall.common.chat.support.factories.MessageContext;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 05/01/2016.
 */
public interface IChatView extends IView {


    void onMessageReceived(MessageContext messageContext, IMessage message);

    void onMessageSent(MessageContext context, IMessage message);

    void showMessages(MessageContext context, List<IMessage> messages);

    void showHeader(ChatParticipant sender, ChatParticipant receiver);

    void setTypingMode(eTypeMode typeMode);

    void clearCurrentMessage();

    void onRecordingStarted();

    void onRecordingEnded();

    void showRecordingTime(int totalTime);

    void onRecordingCanceled();

    void showFailedSendingMessage();

    void showGeneralError();

    void showLoadingConversation(boolean isLoading);

    void showReceivedMessage(String sender);

    void showMessageContainsNonPermittedText();
}
