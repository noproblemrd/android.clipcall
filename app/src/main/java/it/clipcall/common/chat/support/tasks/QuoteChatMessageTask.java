package it.clipcall.common.chat.support.tasks;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.QuoteChatMessage;
import it.clipcall.common.payment.domainModel.services.QuoteManager;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.professional.leads.models.NewQuoteData;
import it.clipcall.professional.leads.models.QuoteDetails;
import it.clipcall.professional.validation.services.AuthenticationManager;


@Singleton
public class QuoteChatMessageTask implements IChatMessageTask {

    private final RoutingService routingService;
    private final RouteParams routeParams;
    private final AuthenticationManager authenticationManager;
    private final QuoteManager quoteManager;

    @Inject
    public QuoteChatMessageTask(RoutingService routingService, RouteParams routeParams, QuoteManager quoteManager, AuthenticationManager authenticationManager){

        this.routingService = routingService;
        this.routeParams = routeParams;
        this.quoteManager = quoteManager;
        this.authenticationManager = authenticationManager;
    }



    @Override
    public void execute(ChatMessage chatMessage, IChatMessageContext messageContext) {
        QuoteChatMessage quoteChatMessage = (QuoteChatMessage) chatMessage;
        NewQuoteData quoteData = quoteChatMessage.getQuoteData();
        QuoteDetails quoteDetails = quoteManager.getQuoteDetails(quoteData.leadAccountId, quoteData.quote.getId());
        if(quoteDetails != null){
            routeParams.setParam("quote", quoteDetails.getQuote());
            routeParams.setParam("leadAccountId",quoteData.leadAccountId);
            ChatMessageNavigationContext navigationContext = (ChatMessageNavigationContext) messageContext;
            routingService.routeTo(Routes.ConsumerQuoteDetailsRoute,navigationContext.getNavigationContext());
        }
    }
}
