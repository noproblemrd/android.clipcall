package it.clipcall.common.chat.viewholders;

import android.view.View;

import com.squareup.picasso.Picasso;

import it.clipcall.R;
import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.ClipCallBotChatMessage;
import it.clipcall.common.chat.models.ClipCallBotMessage;
import it.clipcall.common.chat.models.GroupedMessages;
import it.clipcall.common.chat.support.IChatMessageListener;
import it.clipcall.common.chat.support.factories.MessageContext;
import it.clipcall.infrastructure.ui.images.CircleTransform;

public class ClipCallBotChatMessageContentViewHolder extends ChatMessageContentViewHolder{

    public ClipCallBotChatMessageContentViewHolder(View itemView) {
        super(itemView);
    }

    public void bind(ViewHolderChatBindingContext bindingContext) {
        super.bind(bindingContext);
        GroupedMessages section = bindingContext.section;
        ChatMessage message = bindingContext.message;
        IChatMessageListener chatMessageListener = bindingContext.chatMessageListener;
        pictureImageView.setVisibility(View.GONE);
        messageTextView.setVisibility(View.VISIBLE);
        audioChatMessageContainer.setVisibility(View.GONE);
        quoteContentContainer.setVisibility(View.GONE);
        ClipCallBotChatMessage botChatMessage = (ClipCallBotChatMessage) message;

        MessageContext messageContext = bindingContext.messageContext;
        ClipCallBotMessage botMessage = botChatMessage.getSystemMessage();
        String messageToDisplay = botMessage.customer;
        if(messageContext.me.isPro){
            messageToDisplay = botMessage.pro;
        }

        messageTextView.setText(messageToDisplay);

        Picasso.with(itemView.getContext()).load(R.drawable.clipcall_logo)
                .fit()
                .transform(new CircleTransform())
                .into(senderAvatarImage);

        senderTextView.setText("ClipCall Bot");

    }
}