package it.clipcall.common.chat.views;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import it.clipcall.R;
import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.TextChatMessage;

public class TextChatMessageItemView extends ChatMessageItemView {

    TextView messageTextView;

    public TextChatMessageItemView(Context context) {
        super(context);
        init();
    }

    @Override
    public void bindExtra(ChatMessage message) {
        if(!(message instanceof TextChatMessage)){
            Log.e("Chat","invalid chat message associated with text chat message item view");
            return;
        }
        TextChatMessage textChatMessage = (TextChatMessage) message;
        messageTextView.setText(textChatMessage.text);
    }

    private void init(){
        inflate(getContext(), R.layout.text_chat_message_content, contentContainer);
        this.messageTextView = (TextView)contentContainer.findViewById(R.id.messageTextView);
    }


}
