package it.clipcall.common.chat.viewholders;

import android.view.View;

import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.GroupedMessages;
import it.clipcall.common.chat.models.QuoteChatMessage;
import it.clipcall.common.chat.support.IChatMessageListener;
import it.clipcall.professional.leads.models.NewQuoteData;

public class QuoteChatMessageContentViewHolder extends ChatMessageContentViewHolder{

    public QuoteChatMessageContentViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bind(ViewHolderChatBindingContext bindingContext) {
        super.bind(bindingContext);
        GroupedMessages section = bindingContext.section;
        final ChatMessage message = bindingContext.message;
        final IChatMessageListener chatMessageListener = bindingContext.chatMessageListener;

        messageTextView.setVisibility(View.GONE);
        audioChatMessageContainer.setVisibility(View.GONE);

        final  QuoteChatMessage quoteChatMessage = (QuoteChatMessage) message;
        quoteContentContainer.setVisibility(View.VISIBLE);
        quoteContentContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chatMessageListener.onMessageTapped(message);
            }
        });


        NewQuoteData quoteData = quoteChatMessage.getQuoteData();

        String quoteText = "Quote "+ String.format("%02d", quoteData.quote.getNumber());
        quoteNumberTextView.setText(quoteText);

    }
}