package it.clipcall.common.chat.presenters;

import android.Manifest;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.common.base.Strings;
import com.layer.sdk.changes.LayerChange;
import com.layer.sdk.changes.LayerChangeEvent;
import com.layer.sdk.exceptions.LayerObjectException;
import com.layer.sdk.messaging.Message;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.common.chat.controllers.ChatController;
import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.ChatMessageBelongsToThisConversationCriteria;
import it.clipcall.common.chat.models.ChatMessageValidationCriteria;
import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.common.chat.models.IConversation;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.IMessageDownloaderListener;
import it.clipcall.common.chat.models.LayerMessage;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.models.eTypeMode;
import it.clipcall.common.chat.support.factories.ChatMessageFactoryProvider;
import it.clipcall.common.chat.support.factories.IMessageFactory;
import it.clipcall.common.chat.support.factories.MessageContext;
import it.clipcall.common.chat.support.tasks.ChatMessageNavigationContext;
import it.clipcall.common.chat.support.tasks.IChatMessageContext;
import it.clipcall.common.chat.support.tasks.IChatMessageTask;
import it.clipcall.common.chat.views.IChatView;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.audio.IRecordingListener;
import it.clipcall.infrastructure.audio.SimpleMediaRecorder;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.ui.permissions.IPermissionsHandler;
import it.clipcall.infrastructure.ui.permissions.PermissionContext;
import it.clipcall.infrastructure.ui.permissions.PermissionsService;
import it.clipcall.professional.payment.services.tasks.NavigateToCreateQuoteRouteTask;

/**
 * Created by dorona on 04/01/2016.
 */
@PerActivity
public class ChatPresenter extends PresenterBase<IChatView> implements  IRecordingListener {

    private static final String TAG = "CHAT";
    private final ChatController controller;
    private final RoutingService routingService;


    private final RouteParams routeParams;
    private final PermissionsService permissionsService;
    private final SimpleMediaRecorder mediaRecorder;

    private final NavigateToCreateQuoteRouteTask navigateToCreateQuoteRouteTask;

    private String currentMessage = "";

    private String conversationId;
    private ChatParticipant sender;
    private ChatParticipant receiver;

    private eTypeMode chatMode;

    @Inject
    public ChatPresenter(ChatController chatController, RoutingService routingService, RouteParams routeParams, PermissionsService permissionsService, SimpleMediaRecorder mediaRecorder, NavigateToCreateQuoteRouteTask task) {
        this.routeParams = routeParams;
        this.permissionsService = permissionsService;
        this.mediaRecorder = mediaRecorder;
        this.controller = chatController;
        this.routingService = routingService;
        this.navigateToCreateQuoteRouteTask = task;
    }

    @Override
    public void initialize() {
        view.showHeader(sender, receiver);
        chatMode = eTypeMode.ReadyToRecord;
        EventBus.getDefault().register(this);
    }

    public void setCurrentMessage(String currentMessage) {
        this.currentMessage = currentMessage;
        if(Strings.isNullOrEmpty(currentMessage)){
            chatMode = eTypeMode.ReadyToRecord;
        }else{
            chatMode = eTypeMode.Typing;
        }
    }


    public void sendMessage(Object content, String messageType){
        ChatMessageFactoryProvider messageFactoryProvider = ChatMessageFactoryProvider.getInstance();
        IMessageFactory messageFactory = messageFactoryProvider.getFactory(messageType);
        MessageDescription messageDescription = messageFactory.createMessageDescription(content);

        IMessage message = controller.sendMessage(sender, receiver, conversationId, messageDescription);
        IConversation conversation = controller.getConversationWithParticipant(receiver, conversationId);
        if(message == null){
            view.showFailedSendingMessage();
            return;
        }
        MessageContext messagesContext = getConsumerMessageContext(conversation);
        view.onMessageSent(messagesContext, message);
    }

    private void loadConversationWithParticipant(){
        view.showLoadingConversation(true);
        int messageLimit = 100;
        IConversation conversation = controller.getOrCreateAndGetConversationWithParticipant(sender, receiver, conversationId);
        if(conversation == null)
        {
            view.showLoadingConversation(false);
            return;
        }
        List<IMessage> messages = controller.getConversationRecentMessages(conversation, messageLimit);
        MessageContext messageContext = getConsumerMessageContext(conversation);
        view.showLoadingConversation(false);
        view.showMessages(messageContext, messages);
    }

    @NonNull
    private MessageContext getConsumerMessageContext(IConversation conversation) {
        MessageContext messagesContext = new MessageContext();
        messagesContext.conversation = conversation;
        messagesContext.authenticatedUserId = controller.getAuthenticatedUserId();
        messagesContext.me = sender;
        messagesContext.otherSide = receiver;
        return messagesContext;
    }


    private void handleChange(LayerChange layerChange){
        switch (layerChange.getObjectType()) {
            case CONVERSATION:
                handleConversationChange(layerChange);
                break;

            case MESSAGE:
                handleMessageChange(layerChange);
                break;
        }
    }

    private void handleConversationChange(LayerChange layerChange){

    }

    private void handleMessageChange(LayerChange layerChange) {
        switch (layerChange.getChangeType()) {
            case INSERT:
                Message message = (Message)layerChange.getObject();

                if(message.getSender().getUserId().equals(controller.getAuthenticatedUserId()))
                    return;

                final IConversation conversation = controller.getConversationWithParticipant(receiver,  conversationId);
                if(conversation == null)
                    return;

                Message.RecipientStatus recipientStatus = message.getRecipientStatus(controller.getAuthenticatedUserId());
                if(recipientStatus == Message.RecipientStatus.READ)
                    return;

                final LayerMessage layerMessage = new LayerMessage(message);

                ChatMessageBelongsToThisConversationCriteria criteria = new ChatMessageBelongsToThisConversationCriteria(sender,receiver, conversationId);
                if(!criteria.isSatisfiedBy(layerMessage)){

                    view.showReceivedMessage(layerMessage.getSenderName());
                    //@TODO implement when this is not
                    return;

                }




                layerMessage.markAsRead();
                layerMessage.download(new IMessageDownloaderListener() {
                    @Override
                    public void onSuccess(IMessage message) {
                        MessageContext messageContext = getConsumerMessageContext(conversation);
                        if(view != null){
                            view.onMessageReceived(messageContext, layerMessage);
                        }
                    }

                    @Override
                    public void onFailure() {

                    }
                });


                break;
        }
    }


    public void restart() {
        if(view != null){
            view.showHeader(sender, receiver);
        }
        loadConversationWithParticipant();
    }

    @Override
    public void shutDown() {
        EventBus.getDefault().unregister(this);
    }

    @RunOnUiThread
    public void setSender(ChatParticipant sender) {
        this.sender = sender;
    }

    @RunOnUiThread
    public void setReceiver(ChatParticipant receiver) {
        this.receiver = receiver;
    }

    public void pickImageFromGallery(final int requestCode) {

        PermissionContext permissionContext = new PermissionContext("Permission required","We need your permissions to select a profile image",Manifest.permission.WRITE_EXTERNAL_STORAGE);

        permissionsService.requestPermissions(permissionContext, new IPermissionsHandler() {
            @Override
            public void handleAllPermissionsGranted() {
                routeToGallery(requestCode);
            }

            @Override
            public void handlePermissionDenied() {

            }
        });
    }


    public void pickImageFromCamera(final int requestCode){

        PermissionContext permissionContext = new PermissionContext("Permission required","We need your permissions to take a profile picture",Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
        permissionsService.requestPermissions(permissionContext, new IPermissionsHandler() {
            @Override
            public void handleAllPermissionsGranted() {
                routeToCamera(requestCode);
            }

            @Override
            public void handlePermissionDenied() {

            }
        });
    }


    private void routeToCamera(final int requestCode){
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.addParameter("requestCode",requestCode+"");
        routingService.routeTo(Routes.CommonPickImageFromCameraRoute,navigationContext);
    }

    private void routeToGallery(final int requestCode){
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.addParameter("requestCode",requestCode+"");
        routingService.routeTo(Routes.CommonPickImageFromGalleryRoute,navigationContext);
    }

    public void applyAction() {
        if(chatMode == eTypeMode.Typing){

            ChatMessageValidationCriteria criteria = new ChatMessageValidationCriteria();
            if(criteria.isSatisfiedBy(currentMessage)){
                view.showMessageContainsNonPermittedText();
                return;
            }
            this.sendMessage(currentMessage, MessageType.Text.getMimeType());
            this.setCurrentMessage("");
            view.clearCurrentMessage();
            return;
        }

        if(chatMode == eTypeMode.ReadyToRecord){
            startRecording();
            return;
        }

        if(chatMode == eTypeMode.Recording){

            stopRecording();
        }
    }



    private void startRecording() {
        mediaRecorder.startRecording(this);
        chatMode = eTypeMode.Recording;
        view.onRecordingStarted();
    }

    private void stopRecording() {
        mediaRecorder.stopRecording();
        String fileUrl = mediaRecorder.getLastRecordingUri();
        chatMode = eTypeMode.ReadyToRecord;
        view.onRecordingEnded();
        sendMessage(fileUrl,MessageType.Audio.getMimeType());
    }

    @Override
    public void onTimePassed(int totalTime) {
        view.showRecordingTime(totalTime);
    }

    public void cancelRecording() {
        mediaRecorder.cancelRecording();
        view.onRecordingCanceled();
    }

    @Subscriber
    void onObjectError(LayerObjectException e) {
        if(view == null)
            return;

        view.showGeneralError();
    }



    @Subscriber(mode = ThreadMode.ASYNC)
    void onChatEvent(LayerChangeEvent layerChangeEvent) {
        Log.d(TAG, "onEventMainThread: "+ layerChangeEvent.toString());

        List<LayerChange> changes = layerChangeEvent.getChanges();
        for (LayerChange change : changes){
            handleChange(change);

        }
    }

    @RunOnUiThread
    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public void sendLogs(Activity activity) {
       controller.sendLogs(activity);
    }

    @RunOnUiThread
    public void routeToHome() {
        routingService.routeToHome(view.getRoutingContext());
    }

    public void showImage(Bitmap bitmap, Bundle uiBundle) {
        routeParams.reset();
        routeParams.setParam("image", bitmap);
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.uiBundle = uiBundle;
        routingService.routeTo(Routes.ImageFullScreenActivity, navigationContext);
    }

    public void onMessageTapped(ChatMessage chatMessage) {
        ChatMessageFactoryProvider messageFactoryProvider = ChatMessageFactoryProvider.getInstance();
        IMessageFactory messageFactory = messageFactoryProvider.getFactory(chatMessage.getType());
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        IChatMessageContext messageContext = new ChatMessageNavigationContext(navigationContext);
        IChatMessageTask task = messageFactory.createChatMessageTask();
        task.execute(chatMessage, messageContext);
    }

    public boolean isSenderPro() {
        return sender != null && sender.isPro;
    }

    public void sendQuote() {
        navigateToCreateQuoteRouteTask.execute(conversationId, view.getRoutingContext());
    }
}
