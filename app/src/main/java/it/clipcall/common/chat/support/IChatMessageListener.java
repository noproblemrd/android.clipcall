package it.clipcall.common.chat.support;

import it.clipcall.common.chat.models.ChatMessage;

/**
 * Created by dorona on 27/06/2016.
 */
public interface IChatMessageListener {

    void onMessageTapped(ChatMessage chatMessage);
}
