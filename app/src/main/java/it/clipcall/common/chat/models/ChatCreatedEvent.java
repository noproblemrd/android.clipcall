package it.clipcall.common.chat.models;

/**
 * Created by dorona on 31/05/2016.
 */
public class ChatCreatedEvent {

    private final String leadAccountId;

    public ChatCreatedEvent(String leadAccountId){

        this.leadAccountId = leadAccountId;
    }

    public String getLeadAccountId() {
        return leadAccountId;
    }
}
