package it.clipcall.common.chat.support.factories;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;

/**
 * Created by dorona on 02/03/2016.
 */
public class ChatMessageFactoryProvider {

    private static final String TAG = "ChatMessageFactoryProvider";
    private final static ChatMessageFactoryProvider instance = new ChatMessageFactoryProvider();

    private final List<IMessageFactory> factories = new ArrayList<>();

    private ChatMessageFactoryProvider(){

    }

    public static ChatMessageFactoryProvider getInstance(){return instance;}

    public IMessageFactory getFactory(String messageType){
        for(IMessageFactory factory : factories){
            if(factory.supportsMessageType(messageType))
                return factory;
        }

        return new UnknownMessageFactory();
    }


    public IMessageFactory getFactory(IMessage message){
        for(IMessageFactory factory : factories){
            if(factory.canCreateChatMessage(message))
            {
                return factory;
            }
        }

        return new UnknownMessageFactory();
    }



    public ChatMessageFactoryProvider addMessageFactory(IMessageFactory factory){
        factories.add(factory);
        return this;
    }

    public List<IMessageFactory> getFactories(){return factories;}

    public IMessageFactory getFactory(int viewType) {
        for(IMessageFactory factory : factories){
            if(factory.getMessageType().getValue() == viewType)
                return factory;
        }

        return new UnknownMessageFactory();
    }

    public String getMessageDescription(IMessage message) {
        IMessageFactory factory = getFactory(message);
        String messageDescription = factory.getMessageDescription(message);
        return messageDescription;
    }

    public MessageDescription createMessageDescription(MessageType messageType, Object messageData){
        IMessageFactory messageFactory = getFactory(messageType.getMimeType());
        MessageDescription messageDescription = messageFactory.createMessageDescription(messageData);
        return messageDescription;
    }
}
