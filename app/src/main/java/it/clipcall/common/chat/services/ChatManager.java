package it.clipcall.common.chat.services;


import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.layer.sdk.LayerClient;
import com.layer.sdk.changes.LayerChangeEvent;
import com.layer.sdk.exceptions.LayerException;
import com.layer.sdk.exceptions.LayerObjectException;
import com.layer.sdk.listeners.LayerAuthenticationListener;
import com.layer.sdk.listeners.LayerChangeEventListener;
import com.layer.sdk.listeners.LayerConnectionListener;
import com.layer.sdk.listeners.LayerObjectExceptionListener;
import com.layer.sdk.messaging.Actor;
import com.layer.sdk.messaging.Conversation;
import com.layer.sdk.messaging.ConversationOptions;
import com.layer.sdk.messaging.Message;
import com.layer.sdk.messaging.MessageOptions;
import com.layer.sdk.messaging.MessagePart;
import com.layer.sdk.messaging.Metadata;

import com.layer.sdk.query.CompoundPredicate;
import com.layer.sdk.query.Predicate;
import com.layer.sdk.query.Query;
import com.layer.sdk.query.SortDescriptor;

import org.simple.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatConversationKey;
import it.clipcall.common.chat.models.ChatCreatedEvent;
import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.common.chat.models.ChatPushMessage;
import it.clipcall.common.chat.models.ChatToken;
import it.clipcall.common.chat.models.IConversation;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.LayerConversation;
import it.clipcall.common.chat.models.LayerMessage;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.models.NotifyChatCreatedCriteria;
import it.clipcall.common.chat.support.IChatConnectionListener;
import it.clipcall.consumer.projects.models.createChatTokenRequest;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.support.collections.IAction;
import it.clipcall.infrastructure.support.collections.IFunction;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.network.RetrofitCallProcessor;
import retrofit2.Call;

@Singleton
public class ChatManager implements LayerAuthenticationListener,  LayerConnectionListener, LayerChangeEventListener, LayerObjectExceptionListener {

    private final ILogger logger = LoggerFactory.getLogger(ChatManager.class.getSimpleName());
    final int conversationMaxRetries = 7;
    final int messageMaxRetries = 7;
    boolean disabled = false;

    private final LayerClient layerClient;
    private final Gson gson;
    private final IChatService2 chatService2;
    private String userId;
    private IChatConnectionListener chatConnectionListener;

    private final Map<ChatConversationKey, Conversation> conversations = new HashMap<>();

    @Inject
    public ChatManager(LayerClient layerClient,Gson gson, IChatService2 chatService2) {
        this.layerClient = layerClient;
        this.gson = gson;
        this.chatService2 = chatService2;
        this.layerClient.registerAuthenticationListener(this);
        this.layerClient.registerConnectionListener(this);
        this.layerClient.registerEventListener(this);
        this.layerClient.registerObjectExceptionListener(this);



    }

    public void setGcmRegistrationId(String userId, String gcmRegistrationId){
        this.layerClient.setGcmRegistrationId(userId, gcmRegistrationId);
    }

    public void connect(String userId, String gcmRegistrationId,IChatConnectionListener chatConnectionListener){

        if(disabled)
            return;

        if(isConnected(userId))
        {
            chatConnectionListener.onConnected();
            return;
        }

        this.userId = userId;
        this.layerClient.setGcmRegistrationId(userId, gcmRegistrationId);
        this.chatConnectionListener  = chatConnectionListener;

        if (!layerClient.isConnected()) {

            logger.debug("connect: trying to connect to chat with userId %s",userId);
            //If Layer is not connected, make sure we connect in order to send/receive messages.
            // MyConnectionListener.java handles the callbacks associated with Connection, and
            // will start the Authentication process once the connection is established
            layerClient.connect();
            return;

        }

        if (!layerClient.isAuthenticated()) {

            logger.debug("connect: connected to chat. Trying to authenticate with userId %s",userId);
            //If the client is already connected, try to authenticate a user on this device.
            // MyAuthenticationListener.java handles the callbacks associated with Authentication
            // and will start the Conversation View once the user is authenticated
            layerClient.authenticate();
            return;
        }

        if(!layerClient.getAuthenticatedUserId().equals(userId)){
            logger.error("connect: connected to chat. Invalid state. Already authenticated with user %s while trying to authenticate with user %s",layerClient.getAuthenticatedUserId(), userId);
            layerClient.deauthenticate();
            return;
        }

        logger.debug("connect: connected and authenticated with user %s", userId);
        notifyConnected();
    }

    public IConversation getConversationWithParticipant(ChatParticipant sender, ChatParticipant receiver, String conversationId){
        if(!isConnected(userId))
            return null;

        try{
            Conversation conversation = getConversationWithParticipantCore(receiver.id, conversationId);
            if(conversation == null)
                return null;


            return new LayerConversation(conversation);
      }
      catch (Exception e){
          logger.error("getConversationWithParticipant: "+e.getMessage(), e);
          return null;
      }
    }

    public IConversation createConversation(ChatParticipant sender, ChatParticipant receiver, String conversationId){
       try{
           String uniqueKey = buildConversationKey(sender, receiver, conversationId);

           Metadata metadata = Metadata.newInstance();
           metadata.put( "sConversationKey", uniqueKey);
           metadata.put( "sLeadAcountID", conversationId);
           metadata.put("version", "Android Chat 1.0.0 2.0.0");

           Conversation newConversation = layerClient.newConversation(new ConversationOptions().distinct(true), Arrays.asList(receiver.id, conversationId));
           newConversation.putMetadata(metadata, true);
           this.conversations.put(new ChatConversationKey(receiver.id,conversationId), newConversation);
           return new LayerConversation(newConversation);
       }
       catch (Exception e){
           logger.error("createConversation: "+e.getMessage(), e);
           return null;
       }
    }

    private String buildConversationKey(ChatParticipant sender, ChatParticipant receiver,String  accountId) {

        String formattedKey = "%s|%s|%s";
        if(sender.isPro){
            return String.format(formattedKey,accountId,receiver.id, sender.id);
        }

        return String.format(formattedKey,accountId,sender.id,receiver.id);
    }

    public List<IMessage> getMessages(IConversation conversation, int limit) {

        if(!isConnected(userId))
            return Lists.empty();

        LayerConversation layerConversation = (LayerConversation)conversation;

        Conversation rawConversation = layerConversation.getConversation();

        if (rawConversation.getHistoricSyncStatus() == Conversation.HistoricSyncStatus.MORE_AVAILABLE) {
            rawConversation.syncAllHistoricMessages();
        }

        Query query = Query.builder(Message.class)
                .predicate(new Predicate(Message.Property.CONVERSATION, Predicate.Operator.EQUAL_TO, rawConversation))
                .sortDescriptor(new SortDescriptor(Message.Property.SENT_AT, SortDescriptor.Order.ASCENDING))
                .limit(limit)
                .build();

        List<Message> recentMessages = layerClient.executeQuery(query, Query.ResultType.OBJECTS);

        Lists.forEach(recentMessages, new IAction<Message>() {
            @Override
            public void invoke(Message input) {
                Actor sender = input.getSender();
                if(!Strings.isNullOrEmpty(sender.getUserId()) && sender.getUserId().equals(userId))
                    return;

                Message.RecipientStatus recipientStatus = input.getRecipientStatus(userId);
                if(recipientStatus == Message.RecipientStatus.READ)
                    return;

                input.markAsRead();
            }
        });

        List<IMessage> result = Lists.map(recentMessages, new IFunction<Message, IMessage>() {
            @Override
            public IMessage apply(Message input) {
                return new LayerMessage(input);
            }
        });

        return result;
    }



    public String getAuthenticatedUserId() {
        return layerClient.getAuthenticatedUserId();
    }


    @Override
    public void onAuthenticated(LayerClient layerClient, String s) {
        logger.debug("onAuthenticated: user is authenticated " + s);
        notifyConnected();
    }

    @Override
    public void onDeauthenticated(LayerClient layerClient) {
        logger.debug("onDeauthenticated: user is deauthenticated");
        layerClient.authenticate();
        conversations.clear();
    }

    @Override
    public void onAuthenticationChallenge(final LayerClient layerClient, final String nonce) {
        logger.debug("onAuthenticationChallenge: user received challenge request with nonce " + nonce);
        conversations.clear();
        AsyncTask.execute(new Runnable() {
                              @Override
                              public void run() {


                                  createChatTokenRequest request = new createChatTokenRequest();
                                  request.nonce = nonce;

                                  Call<ChatToken> createTokenCall = chatService2.createChatToken(userId,request);
                                  ChatToken chatToken = RetrofitCallProcessor.processCall(createTokenCall);
                                  if(chatToken == null  || chatToken.token == null){
                                      logger.debug("onAuthenticationChallenge: failed generating token for nonce" + nonce);
                                      return;
                                  }
                                  logger.debug("onAuthenticationChallenge: generated token " + chatToken.token);
                                  layerClient.answerAuthenticationChallenge(chatToken.token);
                              }
                          });
    }

    @Override
    public void onAuthenticationError(LayerClient layerClient, LayerException e) {
        logger.debug("onAuthenticationError: message "+ e.getMessage(), e);
    }

    @Override
    public void onConnectionConnected(LayerClient layerClient) {
        logger.debug("onConnectionConnected");
        if(!layerClient.isAuthenticated()){
            logger.debug("onConnectionConnected: trying to authenticate user");
            layerClient.authenticate();
            return;
        }

        if(!layerClient.getAuthenticatedUserId().equals(userId)){
            logger.debug("onConnectionConnected: layerClient.getAuthenticatedUserId() != userId. deauthenticating client");
            layerClient.deauthenticate();
        }

        logger.debug("onConnectionConnected: user is connected and authenticated");
        notifyConnected();
    }

    @Override
    public void onConnectionDisconnected(LayerClient layerClient) {
        logger.debug("onConnectionDisconnected: user is disconnected");
    }

    @Override
    public void onConnectionError(LayerClient layerClient, LayerException e) {
        logger.debug("onConnectionError: received error:  "+ e.getMessage(), e);
    }







    public IConversation getConversationWithParticipant(String participantId, String accountId){
        logger.debug("getConversationWithParticipant:authenticatedUserId: %s, otherParticipantId: %s, leadAccountId: %s", accountId, participantId, accountId);
        Conversation conversation = getConversationWithParticipantCore(participantId, accountId);
        if(conversation == null)
            return null;

        return new LayerConversation(conversation);
    }

    public IMessage sendMessage(ChatParticipant sender, ChatParticipant receiver, String accountId, MessageDescription messageDescription) {

        logger.debug("sendMessage - senderId: %s, receiverId: %s, leadAccountId: %s, messageType: %s", sender.id, receiver.id, accountId, messageDescription.messageType);

        ChatConversationKey key = new ChatConversationKey(receiver.id, accountId);

        Conversation conversation = getConversationWithParticipantCore(receiver.id, accountId);
        if(conversation == null){
            logger.error("sendMessage - conversation with receiverId %s and accountId %s was not found.", receiver.id, accountId);
            return null;
        }

        MessageOptions options = new MessageOptions();

        ChatPushMessage chatPushMessage = new ChatPushMessage();
        chatPushMessage.senderId = sender.id;
        chatPushMessage.senderName = sender.nameOrPhoneNumber;
        chatPushMessage.senderImageUrl = sender.imageUrl;
        chatPushMessage.senderIsPro = sender.isPro;
        chatPushMessage.receiverId = receiver.id;
        chatPushMessage.receiverName = receiver.nameOrPhoneNumber;
        chatPushMessage.receiverImageUrl = receiver.imageUrl;
        chatPushMessage.receiverIsPro = receiver.isPro;
        chatPushMessage.accountId = accountId;
        chatPushMessage.data = messageDescription.pushData;

        Map<String, String> properties = new HashMap<>();
        properties.put("senderId",sender.id);
        properties.put("senderName",sender.nameOrPhoneNumber);
        properties.put("senderImageUrl",sender.imageUrl);
        properties.put("senderIsPro",sender.isPro+"");
        properties.put("receiverId",receiver.id);
        properties.put("receiverName",receiver.nameOrPhoneNumber);
        properties.put("receiverImageUrl",receiver.imageUrl);
        properties.put("receiverIsPro",receiver.isPro+"");
        properties.put("accountId",accountId);
        properties.put("data",messageDescription.pushData);

        String pushNotificationMessage = "";
        if(messageDescription.messageType == MessageType.Text.getMimeType()){
            pushNotificationMessage = sender.nameOrPhoneNumber + " sent you: "+ messageDescription.pushData;
        }else if(messageDescription.messageType == MessageType.SystemProInterested.getMimeType()){
            pushNotificationMessage = messageDescription.pushData;
        }
        else {
            pushNotificationMessage = sender.nameOrPhoneNumber + " sent you "+ messageDescription.pushTypeText;
        }

        logger.debug("sendMessage-  pushNotificationMessage: "+pushNotificationMessage);
       /* PushNotificationPayload payload = new PushNotificationPayload.Builder()
                .text(pushNotificationMessage)
                .data(properties)
                .apnsData(properties)
                .build();
        options.defaultPushNotificationPayload(payload);*/
        logger.debug("sendMessage-  pushNotificationMessage: "+pushNotificationMessage);
        options.pushNotificationMessage(pushNotificationMessage);


        Message message = createMessageCore(chatPushMessage, conversation, messageDescription,options);
        if(message == null)
        {
            logger.error("Failed creating message of type %s", messageDescription.messageType);
            return null;
        }

        NotifyChatCreatedCriteria criteria = new NotifyChatCreatedCriteria(messageDescription.messageType);
        if(criteria.isSatisfiedBy(conversation)){
            EventBus.getDefault().post(new ChatCreatedEvent(accountId));
            logger.debug("notifying chat created with message type %s", messageDescription.messageType);

        }else{
            logger.debug("not notifying chat created with message type %s", messageDescription.messageType);
        }

        return new LayerMessage(message);
    }

    public boolean isConnected(String userId) {

        if(Strings.isNullOrEmpty(userId))
            return false;

        if(!layerClient.isConnected())
            return false;


        if(!layerClient.isAuthenticated())
            return false;

        if(Strings.isNullOrEmpty(layerClient.getAuthenticatedUserId()))
            return false;


        if(!layerClient.getAuthenticatedUserId().equals(userId))
            return false;

        return true;
    }

    public void sendLogs(Activity activity) {
        LayerClient.sendLogs(layerClient,activity);
    }



    private Conversation getConversationWithParticipantCore(String participantId, String conversationId) {

        if(!isConnected(userId))
            return null;
        ChatConversationKey key = new ChatConversationKey(participantId,conversationId);
        if(this.conversations.containsKey(key))
            return conversations.get(key);

        List<Conversation> conversations = layerClient.getConversationsWithParticipants(participantId, conversationId);
        if(conversations != null && conversations.size() > 0){
            this.conversations.put(key, conversations.get(0));
            return this.conversations.get(key);
        }

        return null;
    }

    private void notifyConnected(){
        logger.debug("notifyConnected: ");
        if(chatConnectionListener != null){
            this.chatConnectionListener.onConnected();
        }
    }

    private Message createMessageCore(ChatPushMessage pushMessage, Conversation conversation, MessageDescription messageDescription, MessageOptions options ){
        try{
            List<MessagePart> messageParts = new ArrayList<>();
            MessagePart contentMessagePart = layerClient.newMessagePart(messageDescription.messageType, messageDescription.messageData);
            messageParts.add(contentMessagePart);
            String json = gson.toJson(pushMessage);

            logger.debug("createMessageCore-  messageMetadata: "+json);

            MessagePart messageMetadata = layerClient.newMessagePart(messageDescription.messageType, json.getBytes("UTF-8"));
            messageParts.add(messageMetadata);
            Message message = layerClient.newMessage(options, messageParts);
            conversation.send(message);
            return message;
        }
        catch (Exception e){
            logger.error("createMessage: Failed creating message with error: "+ e.getMessage(), e);
            return null;
        }
    }

    @Override
    public void onChangeEvent(LayerChangeEvent layerChangeEvent) {
        EventBus.getDefault().post(layerChangeEvent);
    }

    @Override
    public void onObjectError(LayerClient layerClient, LayerObjectException e) {
        EventBus.getDefault().post(e);
    }

    public IMessage getMessageById(Uri conversationIdentifier, Uri messageIdentifier) {

        logger.debug("get message by id with conversation id: %s and message id: %s", conversationIdentifier, messageIdentifier);
        Query query = Query.builder(Conversation.class)
                .predicate(new Predicate(Conversation.Property.ID, Predicate.Operator.EQUAL_TO, conversationIdentifier))
                .build();


        int currentConversationRetry = 1;
        List<Conversation> conversations = null;
        while(currentConversationRetry <= conversationMaxRetries) {
            logger.debug("getMessageById - getting conversation retry number %s", currentConversationRetry+"");
             conversations = layerClient.executeQuery(query, Query.ResultType.OBJECTS);
            if(!Lists.isNullOrEmpty(conversations))
                break;

            try{
                Thread.sleep(1000);
                currentConversationRetry++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        if(Lists.isNullOrEmpty(conversations))
        {
            logger.error("getMessageById - Could not found conversation with id %s", conversationIdentifier);
            return null;
        }

        Conversation conversation =  conversations.get(0);
        logger.debug("getMessageById - conversation found. Trying to find message with id %s", messageIdentifier);

        Query messageQuery = Query.builder(Message.class)
                .predicate(new CompoundPredicate(CompoundPredicate.Type.AND,
                        new Predicate(Message.Property.CONVERSATION, Predicate.Operator.EQUAL_TO, conversation),
                        new Predicate(Message.Property.ID, Predicate.Operator.EQUAL_TO, messageIdentifier)))
                .sortDescriptor(new SortDescriptor(Message.Property.SENT_AT, SortDescriptor.Order.DESCENDING))
                .build();


        int currentMessageRetry = 1;
        while(currentMessageRetry <= messageMaxRetries){
            logger.debug("getMessageById - retry number %s", currentMessageRetry+"");

            List<Message> messages = layerClient.executeQuery(messageQuery, Query.ResultType.OBJECTS);
            if(!Lists.isNullOrEmpty(messages)) {
                Message message =  messages.get(0);
                logger.debug("getMessageById - received message successfully after %s retries", currentMessageRetry+"");
                return new LayerMessage(message);

            }

            try{
                Thread.sleep(1000);
                currentMessageRetry++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public IMessage getLastSentMessage(IConversation  conversation,String  senderId) {
        LayerConversation layerConversation = (LayerConversation)conversation;

        Query query = Query.builder(Message.class)
                .predicate(new CompoundPredicate(CompoundPredicate.Type.AND,
                        new Predicate(Message.Property.CONVERSATION, Predicate.Operator.EQUAL_TO, layerConversation.getConversation()),
                        new Predicate(Message.Property.SENDER_USER_ID, Predicate.Operator.EQUAL_TO, senderId)))
                .sortDescriptor(new SortDescriptor(Message.Property.SENT_AT, SortDescriptor.Order.DESCENDING))
                .limit(1)
                .build();

        List<Message> results = layerClient.executeQuery(query, Query.ResultType.OBJECTS);
        if(Lists.isNullOrEmpty(results))
            return null;

        return new LayerMessage(results.get(0));
    }
}
