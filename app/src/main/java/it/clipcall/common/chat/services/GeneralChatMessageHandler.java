package it.clipcall.common.chat.services;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.common.base.Strings;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.ClipCallApplication;
import it.clipcall.common.chat.activities.ChatActivity_;
import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.common.chat.models.ChatPushMessage;
import it.clipcall.common.chat.models.IChatAware;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ProfessionalEventNames;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.notifications.NotificationContext;
import it.clipcall.infrastructure.notifications.SimpleNotifier;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class GeneralChatMessageHandler implements  IChatMessageHandler {

    private final ILogger logger = LoggerFactory.getLogger(GeneralChatMessageHandler.class.getSimpleName());

    private final List<MessageType> handledMessages = new ArrayList<>();

    private final int notificationId =  10;

    @Inject
    AuthenticationManager authenticationManager;

    @Inject
    EventAggregator eventAggregator;

    @Inject
    ClipCallApplication application;

    @Inject
    SimpleNotifier notifier;

    @Inject
    public GeneralChatMessageHandler(){
        handledMessages.add(MessageType.Image);
        handledMessages.add(MessageType.Audio);
        handledMessages.add(MessageType.Video);
        handledMessages.add(MessageType.Text);
        handledMessages.add(MessageType.VideoUrl);
        handledMessages.add(MessageType.AudioUrl);
        handledMessages.add(MessageType.Payment);
        handledMessages.add(MessageType.SendQuote);
        handledMessages.add(MessageType.RequestQuote);
        handledMessages.add(MessageType.BookQuote);
    }

    @Override
    public boolean canHandleMessage(final IMessage message) {

        boolean canHandlerMessage = Lists.any(handledMessages, new ICriteria<MessageType>() {
            @Override
            public boolean isSatisfiedBy(MessageType candidate) {
                return message.getMimeType().equals(candidate.getMimeType());
            }
        });

        return canHandlerMessage;

        //return !message.getMimeType().equals(MessageType.SystemProInterested.getMimeType());
    }

    @Override
    public void handleMessage(IMessage message) {

        ChatPushMessage messageMetadata = message.getMessageMetadata();
        logger.debug("handleChatMessage: about to create notification message");



        if(MessageType.BookQuote.getMimeType().equals(message.getMimeType())){
            eventAggregator.publish(new ApplicationEvent(ProfessionalEventNames.ProPushBooking));
        }

        boolean receiverIsPro = isReceiverPro(messageMetadata);
        ChatParticipant receiverParticipant  = new ChatParticipant(messageMetadata.senderId, messageMetadata.senderImageUrl, messageMetadata.senderName, receiverIsPro);
        ChatParticipant senderParticipant = new ChatParticipant(messageMetadata.receiverId, messageMetadata.receiverImageUrl, messageMetadata.receiverName, !receiverIsPro);
        final Intent resultIntent = new Intent(application.getApplicationContext(), ChatActivity_.class);
        resultIntent.putExtra("sender", Parcels.wrap(senderParticipant));
        resultIntent.putExtra("receiver",Parcels.wrap(receiverParticipant));
        resultIntent.putExtra("conversationId",messageMetadata.accountId);
        resultIntent.putExtra("isFromNotification",true);


        logger.debug("handleChatMessage: created intent");
        Activity currentActivity = application.getCurrentActivity();
        if(currentActivity != null){
            logger.debug("handleChatMessage: handling push notification within the app");
            handlePushWithinTheApp(application, currentActivity, messageMetadata, resultIntent);
            return;
        }

        final NotificationContext notificationContext = new NotificationContext();
        notificationContext.message = messageMetadata.data;
        notificationContext.smallPictureImageUrl = Strings.isNullOrEmpty(messageMetadata.senderImageUrl) ? "https://storage.googleapis.com/clipcallmobile/consumer_profile_logo_placeholder3.png" : messageMetadata.senderImageUrl;
        notificationContext.bigPictureImageUrl = Strings.isNullOrEmpty(messageMetadata.senderImageUrl) ? "https://storage.googleapis.com/clipcallmobile/consumer_profile_logo_placeholder3.png" : messageMetadata.senderImageUrl;
        notificationContext.title = messageMetadata.senderName + "";
        notificationContext.contentTitle = "New message from "+ messageMetadata.senderName;
        notificationContext.bigContentTitle ="New message from "+ messageMetadata.senderName;
        notificationContext.notificationId = notificationId;
        notificationContext.notificationTag = message.getConversationId().toString();
        notificationContext.intent = resultIntent;

        if(!receiverIsPro){
            //message sent to consumer
            authenticationManager.switchToConsumerMode();
        }
        else{
            //message sent to professional
            authenticationManager.switchToProfessionalMode();
        }
        logger.debug("handleChatMessage: starting local notification");
        notifier.notify(notificationContext);
        return;
    }

    private boolean isReceiverPro(ChatPushMessage messageMetadata) {
        String supplierId = authenticationManager.getSupplierId();
        if(Strings.isNullOrEmpty(supplierId))
            return false;

        return messageMetadata.receiverIsPro;
    }

    private void handlePushWithinTheApp(final ClipCallApplication application, final Activity currentActivity, final ChatPushMessage  pushMessage, final Intent intent) {
        if(currentActivity instanceof IChatAware)
            return;

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(Strings.isNullOrEmpty(authenticationManager.getSupplierId())){
                    authenticationManager.switchToConsumerMode();
                }
                else if(!pushMessage.receiverIsPro){
                    //message sent to consumer
                    authenticationManager.switchToConsumerMode();
                }
                else{
                    //message sent to professional
                    authenticationManager.switchToProfessionalMode();
                }
                String message = pushMessage.data;
                if(!Strings.isNullOrEmpty(pushMessage.senderName)){
                    message = pushMessage.senderName + " sent you a message";
                }
                application.showMessage(message, new Runnable() {
                    @Override
                    public void run() {
                        Log.d("LayerRemotePush", "action: "+ pushMessage.data);
                        currentActivity.startActivity(intent);
                    }
                }, true);
            }
        });
    }
}
