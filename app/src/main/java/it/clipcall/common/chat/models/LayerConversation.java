package it.clipcall.common.chat.models;

import com.google.common.base.Strings;
import com.layer.sdk.messaging.Actor;
import com.layer.sdk.messaging.Conversation;
import com.layer.sdk.messaging.Message;

import java.util.List;

public class LayerConversation implements IConversation {


    private final Conversation conversation;

    public LayerConversation(Conversation conversation){
        this.conversation = conversation;
    }

    @Override
    public List<String> getParticipants() {
        return conversation.getParticipants();
    }

    @Override
    public int getTotalUnreadMessageCount() {
        Integer totalUnreadMessages =  conversation.getTotalUnreadMessageCount();
        if(totalUnreadMessages == null)
            return 0;

        return totalUnreadMessages.intValue();
    }

    @Override
    public IMessage getLastUnreadMessage(String senderId, String recievedId) {
        Message lastMessage = conversation.getLastMessage();
        if(lastMessage == null)
            return null;

        Actor sender = lastMessage.getSender();
        if(sender == null)
            return null;

        if(Strings.isNullOrEmpty(sender.getUserId()))
            return null;

        if(!sender.getUserId().equals(senderId))
            return null;

        if(lastMessage.getRecipientStatus(recievedId) == Message.RecipientStatus.READ)
            return null;

        return new LayerMessage(lastMessage);

    }

    public Conversation getConversation(){
        return conversation;
    }



}
