package it.clipcall.common.chat.models;

/**
 * Created by dorona on 03/04/2016.
 */
public class UnknownChatMessage extends ChatMessage {
    @Override
    public int getType() {
        return MessageType.Text.getValue();
    }
}
