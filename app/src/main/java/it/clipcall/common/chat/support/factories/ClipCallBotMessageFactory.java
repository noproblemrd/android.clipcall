package it.clipcall.common.chat.support.factories;

import android.view.View;

import com.google.common.base.Strings;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.ClipCallBotChatMessage;
import it.clipcall.common.chat.models.ClipCallBotMessage;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.support.tasks.IChatMessageTask;
import it.clipcall.common.chat.support.tasks.NoOpChatMessageTask;
import it.clipcall.common.chat.viewholders.ChatMessageContentViewHolder;
import it.clipcall.common.chat.viewholders.ClipCallBotChatMessageContentViewHolder;


/**
 * Created by dorona on 02/03/2016.
 */
@Singleton
public class ClipCallBotMessageFactory extends MessageFactoryBase {

    private final Gson gson;

    @Inject
    public ClipCallBotMessageFactory(Gson gson){
        this.gson = gson;
    }

    @Override
    public MessageDescription createMessageDescription(Object data) {
        return null;
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.ClipCallBot;
    }

    @Override
    public ChatMessageContentViewHolder createViewHolder(View v) {
        return new ClipCallBotChatMessageContentViewHolder(v);
    }

    @Override
    public IChatMessageTask createChatMessageTask() {
        return new NoOpChatMessageTask();
    }

    @Override
    public String getMessageDescription(IMessage message) {
        ClipCallBotChatMessage systemChatMessage = (ClipCallBotChatMessage)createChatMessageCore(message);
        return systemChatMessage.toString();
    }


    @Override
    public ChatMessage createChatMessageCore(IMessage message) {
        String text = "";

        byte[][] rawData = message.getRawData();

        try {
            text = new String(rawData[0], "UTF-8");
        } catch (UnsupportedEncodingException e) {

        }

        if(Strings.isNullOrEmpty(text))
            return null;


        ClipCallBotMessage botMessage = gson.fromJson(text,ClipCallBotMessage.class);
        ChatMessage chatMessage = new ClipCallBotChatMessage(botMessage);
        return chatMessage;
    }

}
