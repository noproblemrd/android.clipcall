package it.clipcall.common.chat.models;

import android.graphics.Bitmap;

/**
 * Created by dorona on 25/02/2016.
 */
public class ImageChatMessage extends ChatMessage {

    public Bitmap image;
    public ImageChatMessage(Bitmap image) {
        this.image = image;
    }

    @Override
    public int getType() {
        return MessageType.Image.getValue();
    }
}

