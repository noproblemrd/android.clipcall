package it.clipcall.common.chat.models;

import com.google.common.base.Joiner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dorona on 25/02/2016.
 */
public class CompositeTextChatMessage extends ChatMessage {


    private final List<TextChatMessage> textMessages = new ArrayList<>();

    public void addTextChatMessage(TextChatMessage message){
        textMessages.add(message);
    }

    public String getText(){
        String message = Joiner.on("").join(textMessages);
        return message;
    }

    @Override
    public int getType() {
        return MessageType.Text.getValue();
    }
}

