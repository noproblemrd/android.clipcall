package it.clipcall.common.chat.services;


import it.clipcall.common.chat.models.ChatToken;
import it.clipcall.consumer.projects.models.createChatTokenRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface IChatService2 {

    @POST("customers/{customerId}/chat/tokens")
    Call<ChatToken> createChatToken(@Path("customerId") String customerId,@Body createChatTokenRequest request);
}
