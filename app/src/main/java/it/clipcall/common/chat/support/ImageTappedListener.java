package it.clipcall.common.chat.support;

import android.graphics.Bitmap;
import android.os.Bundle;

/**
 * Created by dorona on 02/06/2016.
 */
public interface ImageTappedListener {

    void onImageTapped(Bitmap bitmap, Bundle uiBundle);
}
