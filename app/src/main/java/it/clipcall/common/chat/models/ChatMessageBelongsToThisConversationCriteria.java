package it.clipcall.common.chat.models;

import it.clipcall.infrastructure.support.criterias.ICriteria;

/**
 * Created by dorona on 31/05/2016.
 */
public class ChatMessageBelongsToThisConversationCriteria implements ICriteria<LayerMessage> {

    private final ChatParticipant sender;
    private final ChatParticipant receiver;
    private final String leadAccountId;

    public ChatMessageBelongsToThisConversationCriteria(ChatParticipant sender, ChatParticipant receiver, String leadAccountId){

        this.sender = sender;
        this.receiver = receiver;
        this.leadAccountId = leadAccountId;
    }

    @Override
    public boolean isSatisfiedBy(LayerMessage candidate) {
        if(!receiver.id.equals(candidate.getSender().getUserId()))
            return false;

        if(!candidate.isPartOfConversation(sender.id))
            return false;

        if(!candidate.isPartOfConversation(receiver.id))
            return false;

        if(!candidate.isPartOfConversation(leadAccountId))
            return false;

        return true;
    }
}
