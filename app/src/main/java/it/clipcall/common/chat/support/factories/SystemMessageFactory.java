package it.clipcall.common.chat.support.factories;

import android.view.View;

import com.google.common.base.Strings;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.models.SystemChatMessage;
import it.clipcall.common.chat.models.SystemMessage;
import it.clipcall.common.chat.support.tasks.IChatMessageTask;
import it.clipcall.common.chat.support.tasks.NoOpChatMessageTask;
import it.clipcall.common.chat.viewholders.ChatMessageContentViewHolder;
import it.clipcall.common.chat.viewholders.SystemChatMessageContentViewHolder;
import it.clipcall.professional.leads.models.NewProjectChatData;


/**
 * Created by dorona on 02/03/2016.
 */
@Singleton
public class SystemMessageFactory extends MessageFactoryBase {

    private final Gson gson;

    @Inject
    public SystemMessageFactory(Gson gson){
        this.gson = gson;
    }

    @Override
    public MessageDescription createMessageDescription(Object data) {
        if(!(data instanceof NewProjectChatData))
            return null;

        NewProjectChatData project = (NewProjectChatData) data;

        String pushMessage = String.format("%s wants to help you with your %s project",project.professionalName,project.category);
        String message = String.format("I want to help you with your %s project",project.category);


        SystemMessage systemMessage = new SystemMessage();
        systemMessage.message = message;
        systemMessage.mimeType = MessageType.SystemProInterested.getMimeType();
        systemMessage.lead = project.leadId;

        String json = gson.toJson(systemMessage);

        byte[] textData;
        try {
            textData = json.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }

        MessageDescription messageDescription = new MessageDescription(getMessageType().getMimeType(), textData);
        messageDescription.pushData = pushMessage;
        messageDescription.pushTypeText = "text message";
        return  messageDescription;
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.SystemProInterested;
    }

    @Override
    public ChatMessageContentViewHolder createViewHolder(View v) {
        return new SystemChatMessageContentViewHolder(v);
    }

    @Override
    public IChatMessageTask createChatMessageTask() {
        return new NoOpChatMessageTask();
    }

    @Override
    public String getMessageDescription(IMessage message) {
        SystemChatMessage systemChatMessage = (SystemChatMessage)createChatMessageCore(message);
        return systemChatMessage.text;
    }


    @Override
    public ChatMessage createChatMessageCore(IMessage message) {
        String text = "";

        byte[][] rawData = message.getRawData();

        try {
            text = new String(rawData[0], "UTF-8");
        } catch (UnsupportedEncodingException e) {

        }

        if(Strings.isNullOrEmpty(text))
            return null;


        SystemMessage systemMessage = gson.fromJson(text,SystemMessage.class);
        ChatMessage chatMessage = new SystemChatMessage(systemMessage);
        return chatMessage;
    }

}
