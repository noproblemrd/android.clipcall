package it.clipcall.common.chat.views;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import it.clipcall.R;
import it.clipcall.common.chat.models.AudioChatMessage;
import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.infrastructure.support.buttons.AnimButton;

public class AudioRecordingChatMessageItemView extends ChatMessageItemView implements  MediaPlayer.OnCompletionListener {

    private AnimButton playAudioAnimButton;

    private MediaPlayer mediaPlayer;

    private boolean isPlaying = false;

    private AudioChatMessage audioChatMessage;

    public AudioRecordingChatMessageItemView(Context context) {
        super(context);
        init();
    }

    @Override
    public void bindExtra(ChatMessage message) {
        if(!(message instanceof  AudioChatMessage)){
            Log.e("Chat","invalid chat message associated with Audio chat message item view");
            return;
        }
        audioChatMessage = (AudioChatMessage) message;
        prepareAudio();
    }

    private void init(){
        inflate(getContext(), R.layout.audio_chat_message_content, contentContainer);
        this.playAudioAnimButton = (AnimButton)contentContainer.findViewById(R.id.playAudioAnimButton);



        this.playAudioAnimButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleAudioPlay();
            }
        });
    }

    private void prepareAudio() {

        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnCompletionListener(this);
            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
            String fileName = "audio_" + timeStampFormat.format(new Date());
            File tempMp3  = File.createTempFile(fileName, ".mp4", getContext().getCacheDir());
            tempMp3.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(tempMp3);
            fos.write(audioChatMessage.audioData);
            fos.close();
            FileInputStream   fis = new FileInputStream(tempMp3);
            mediaPlayer.setDataSource(fis.getFD());
            mediaPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
            mediaPlayer = null;
        }
    }



    private void toggleAudioPlay() {
        if(mediaPlayer == null)
            return;

        if(isPlaying)
            pause();
        else
            play();



    }

    private void play(){
        try{
            mediaPlayer.start();
            playAudioAnimButton.goToState(AnimButton.SECOND_STATE);
            isPlaying = true;
        }
        catch(Exception e){
            mediaPlayer = null;
        }
    }

    private void pause(){
        try{
            mediaPlayer.stop();
            playAudioAnimButton.goToState(AnimButton.FIRST_STATE);
            isPlaying = false;
        }
        catch(Exception e){
            mediaPlayer = null;
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mp.seekTo(0);
    }
}
