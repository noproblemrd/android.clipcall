package it.clipcall.common.chat.support.factories;

import android.view.View;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.AudioChatMessage;
import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.support.tasks.IChatMessageTask;
import it.clipcall.common.chat.support.tasks.NoOpChatMessageTask;
import it.clipcall.common.chat.viewholders.ChatMessageAudioContentViewHolder;
import it.clipcall.common.chat.viewholders.ChatMessageContentViewHolder;


@Singleton
public class AudioMessageFactory extends MessageFactoryBase {


      @Inject
      public AudioMessageFactory(){

      }

      @Override
      public ChatMessage createChatMessageCore(IMessage message) {
        byte[][] data = message.getRawData();
        if(data == null)
            return null;

        byte[] audioData = data[0];
        ChatMessage chatMessage = new AudioChatMessage(audioData);
        return chatMessage;
    }

    @Override
    public MessageDescription createMessageDescription(Object data) {

        if(!(data instanceof String))
            return null;

        String fileName = (String) data;

         File file = new File(fileName);
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
            MessageDescription messageDescription = new MessageDescription(getMessageType().getMimeType(),bytes);
            messageDescription.pushData = "";
            messageDescription.pushTypeText = "audio recording";

            return messageDescription;

        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public MessageType getMessageType() {
        return MessageType.Audio;
    }

    @Override
    public ChatMessageContentViewHolder createViewHolder(View v) {
        return new ChatMessageAudioContentViewHolder(v);
    }

    @Override
    public IChatMessageTask createChatMessageTask() {
        return new NoOpChatMessageTask();
    }

    @Override
    public String getMessageDescription(IMessage message) {
        return "Sent you a recorded audio";
    }
}
