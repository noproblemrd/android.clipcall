package it.clipcall.common.chat.models;

/**
 * Created by omega on 4/8/2016.
 */
public interface IActor {
    String getUserId();
    String getName();
}
