package it.clipcall.common.chat.models;

/**
 * Created by dorona on 25/02/2016.
 */
public class SystemChatMessage extends ChatMessage {

    private final SystemMessage systemMessage;
    public String text;

    public SystemChatMessage(SystemMessage systemMessage){
        this.systemMessage = systemMessage;
        this.text = systemMessage.message;
    }

    public SystemMessage getSystemMessage(){
        return systemMessage;
    }

    @Override
    public int getType() {
        return MessageType.SystemProInterested.getValue();
    }

    @Override
    public String toString() {
        return systemMessage.message;
    }
}

