package it.clipcall.common.chat.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.common.base.Strings;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;

import it.clipcall.R;
import it.clipcall.infrastructure.support.buttons.AnimButton;

@EViewGroup(R.layout.chat_input_area)
public class ChatInputAreaView extends RelativeLayout {


    @ViewById
    AnimButton sendButton;


    public ChatInputAreaView(Context context) {
        this(context, null);
    }

    public ChatInputAreaView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChatInputAreaView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @TextChange(R.id.chatMessageEditText)
    void creditCardTextChange(CharSequence text, TextView hello, int before, int start, int count){
        if(Strings.isNullOrEmpty(text.toString())){
            sendButton.goToState(AnimButton.FIRST_STATE);
            return;
        }

        sendButton.goToState(AnimButton.SECOND_STATE);
    }
}
