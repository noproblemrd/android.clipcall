package it.clipcall.common.chat.models;

import java.util.List;

/**
 * Created by omega on 4/8/2016.
 */
public interface IConversation {
    List<String> getParticipants();

    int getTotalUnreadMessageCount();

    IMessage getLastUnreadMessage(String senderId, String receiverId);
}
