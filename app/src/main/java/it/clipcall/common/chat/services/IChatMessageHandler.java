package it.clipcall.common.chat.services;

import it.clipcall.common.chat.models.IMessage;

/**
 * Created by dorona on 01/06/2016.
 */
public interface IChatMessageHandler {

    boolean canHandleMessage(IMessage message);

    void handleMessage(IMessage message);
}
