package it.clipcall.common.chat.services;

/**
 * Created by dorona on 01/03/2016.
 */
public interface IIdentityTokenGenerator {
    String generateIdentityToken(String userId, String nonce);
}
