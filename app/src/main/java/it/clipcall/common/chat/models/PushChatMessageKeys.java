package it.clipcall.common.chat.models;

/**
 * Created by dorona on 03/04/2016.
 */
public class PushChatMessageKeys {

    public static final String senderId = "_sender_id_";
    public static final String senderName = "_sender_name_";
    public static final String senderImageUrl = "_sender_image_url_";
    public static final String receiverId= "_receiver_id_";
    public static final String receiverName = "_receiver_name_";
    public static final String receiverImageUrl = "_receiver_image_url_";
    public static final String accountId = "_accound_id_";

    public static final String seperator = "$$$";

}
