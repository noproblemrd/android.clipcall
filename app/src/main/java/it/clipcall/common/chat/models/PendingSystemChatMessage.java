package it.clipcall.common.chat.models;

import java.io.Serializable;

import it.clipcall.consumer.projects.models.ProjectEntity;

/**
 * Created by dorona on 01/06/2016.
 */
public class PendingSystemChatMessage implements Serializable {
    private ProjectEntity projectEntity;

    public ProjectEntity getProjectEntity() {
        return projectEntity;
    }

    public void setProjectEntity(ProjectEntity projectEntity) {
        this.projectEntity = projectEntity;
    }
}
