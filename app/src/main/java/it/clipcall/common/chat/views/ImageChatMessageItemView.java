package it.clipcall.common.chat.views;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import it.clipcall.R;
import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.ImageChatMessage;
import it.clipcall.common.chat.support.ImageTappedListener;

public class ImageChatMessageItemView extends ChatMessageItemView {

    ImageView pictureImageView;

    public ImageChatMessageItemView(Context context) {
        super(context);
        init();
    }

    @Override
    public void bindExtra(ChatMessage message) {
        if(!(message instanceof ImageChatMessage)){
            Log.e("Chat","invalid chat message associated with image chat message item view");
            return;
        }
        final ImageChatMessage imageChatMessage = (ImageChatMessage) message;
        pictureImageView.setImageBitmap(imageChatMessage.image);

    }

   private void init(){
        inflate(getContext(), R.layout.image_chat_message_content, contentContainer);
        this.pictureImageView = (ImageView)contentContainer.findViewById(R.id.pictureImageView);
    }
}
