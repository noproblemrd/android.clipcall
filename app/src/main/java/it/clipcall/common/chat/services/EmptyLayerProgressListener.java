package it.clipcall.common.chat.services;

import com.layer.sdk.listeners.LayerProgressListener;
import com.layer.sdk.messaging.MessagePart;

/**
 * Created by dorona on 10/05/2016.
 */
public class EmptyLayerProgressListener implements LayerProgressListener {
    @Override
    public void onProgressStart(MessagePart messagePart, Operation operation) {

    }

    @Override
    public void onProgressUpdate(MessagePart messagePart, Operation operation, long l) {

    }

    @Override
    public void onProgressComplete(MessagePart messagePart, Operation operation) {

    }

    @Override
    public void onProgressError(MessagePart messagePart, Operation operation, Throwable throwable) {

    }
}
