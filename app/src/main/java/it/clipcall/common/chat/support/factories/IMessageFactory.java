package it.clipcall.common.chat.support.factories;

import android.view.View;

import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.support.tasks.IChatMessageTask;
import it.clipcall.common.chat.viewholders.ChatMessageContentViewHolder;

/**
 * Created by dorona on 02/03/2016.
 */
public interface IMessageFactory {

    boolean canCreateChatMessage(IMessage message);
    ChatMessage createChatMessage(MessageContext context, IMessage message);
    MessageDescription createMessageDescription(Object data);
    boolean supportsMessageType(String messageType);
    MessageType getMessageType();
    ChatMessageContentViewHolder createViewHolder(View v);

    IChatMessageTask createChatMessageTask();

    String getMessageDescription(IMessage message);
}
