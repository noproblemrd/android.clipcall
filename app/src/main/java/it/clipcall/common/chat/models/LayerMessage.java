package it.clipcall.common.chat.models;

import android.net.Uri;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.layer.sdk.messaging.Message;
import com.layer.sdk.messaging.MessagePart;

import java.util.Date;
import java.util.List;

import it.clipcall.common.chat.services.EmptyLayerProgressListener;
import it.clipcall.infrastructure.support.BooleanSerializer;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;

/**
 * Created by omega on 4/8/2016.
 */
public class LayerMessage implements IMessage {

    private static final Gson gson;
    private final Message message;

    private ChatPushMessage messageMetadata;

    static{
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(boolean.class, new BooleanSerializer());
        gsonBuilder.registerTypeAdapter(Boolean.class, new BooleanSerializer());
        gson = gsonBuilder.create();
    }


    public LayerMessage(Message message){
        this.message = message;
    }

    private boolean initMessageMetadata(){

        if(messageMetadata != null)
            return true;

        List<MessagePart> messageParts = message.getMessageParts();
        if(Lists.isNullOrEmpty(messageParts))
            return false;

        if(messageParts.size() < 2)
            return false;

        MessagePart messagePart = messageParts.get(1);
        byte[] data = messagePart.getData();
        if(data == null)
            return false;

        try {
            String messageMetaData = new String(data, "UTF-8");
            messageMetadata = gson.fromJson(messageMetaData,ChatPushMessage.class);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }


        return true;
    }

    public Message getRawMessage(){

        return message;
    }

    @Override
    public Date getSentAt() {
        return message.getSentAt();
    }

    @Override
    public String getMimeType() {
        List<MessagePart> parts = message.getMessageParts();
        if(Lists.isNullOrEmpty(parts))
            return "";

        MessagePart messagePart = parts.get(0);
        return messagePart.getMimeType();
    }

    @Override
    public byte[][] getRawData() {
        List<MessagePart> parts = message.getMessageParts();
        if(Lists.isNullOrEmpty(parts))
            return null;

        byte data[][] = new byte[parts.size()][];
        for(int i=0; i<parts.size(); ++i){
            byte[] partData = parts.get(i).getData();
            data[i] = partData;
        }

        return data;
    }

    @Override
    public Date getReceivedAt() {
        return message.getReceivedAt();
    }

    @Override
    public IActor getSender() {
        return new LayerActorAdapter(message.getSender());
    }

    @Override
    public ChatPushMessage getMessageMetadata() {
        if(initMessageMetadata())
            return messageMetadata;

        return null;
    }

    @Override
    public boolean isPartOfConversation(final String userId) {
        List<String> participants =  message.getConversation().getParticipants();
        return Lists.any(participants, new ICriteria<String>() {
            @Override
            public boolean isSatisfiedBy(String candidate) {
                return candidate.equals(userId);
            }
        });
    }


    @Override
    public Uri getConversationId() {
        return message.getConversation().getId();
    }


    @Override
    public boolean isDelivered(String participant) {
        return message.getRecipientStatus(participant) == Message.RecipientStatus.DELIVERED;
    }


    @Override
    public boolean isRead(String participant) {
        return message.getRecipientStatus(participant) == Message.RecipientStatus.READ;
    }

    @Override
    public void download(final IMessageDownloaderListener listener) {

        List<MessagePart> messageParts = message.getMessageParts();
        if(Lists.isNullOrEmpty(messageParts))
        {
            listener.onFailure();
            return;
        }

        MessagePart messagePart = messageParts.get(0);
        if(messagePart.isContentReady()){
            listener.onSuccess(this);
            return;
        }


        final IMessage self = this;
        messagePart.download(new EmptyLayerProgressListener() {

            @Override
            public void onProgressComplete(MessagePart messagePart, Operation operation) {
                listener.onSuccess(self);
                return;
            }

            @Override
            public void onProgressError(MessagePart messagePart, Operation operation, Throwable throwable) {
                listener.onFailure();
            }
        });
    }

    @Override
    public void markAsRead() {
        message.markAsRead();
    }

    @Override
    public String getSenderName() {
        if(initMessageMetadata())
            return messageMetadata.senderName;

        return "";
    }
}
