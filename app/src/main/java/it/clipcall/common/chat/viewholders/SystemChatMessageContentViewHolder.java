package it.clipcall.common.chat.viewholders;

import android.view.View;

import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.GroupedMessages;
import it.clipcall.common.chat.models.SystemChatMessage;
import it.clipcall.common.chat.support.IChatMessageListener;

public class SystemChatMessageContentViewHolder extends ChatMessageContentViewHolder{

    public SystemChatMessageContentViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bind(ViewHolderChatBindingContext bindingContext) {
        super.bind(bindingContext);
        GroupedMessages section = bindingContext.section;
        final ChatMessage message = bindingContext.message;
        final IChatMessageListener chatMessageListener = bindingContext.chatMessageListener;
        pictureImageView.setVisibility(View.GONE);
        messageTextView.setVisibility(View.VISIBLE);
        audioChatMessageContainer.setVisibility(View.GONE);
        quoteContentContainer.setVisibility(View.GONE);

        SystemChatMessage imageMessage = (SystemChatMessage) message;
        messageTextView.setText(imageMessage.toString());

    }
}