package it.clipcall.common.chat.models;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.infrastructure.support.collections.Lists;

/**
 * Created by omega on 4/8/2016.
 */
public class GroupedMessages {

    private final String formattedDate;

    private LocalDate date;

    public GroupedMessages(LocalDate date){
        this.date = date;
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy/MM/dd");
        formattedDate = date.toString(fmt);

    }
    private final List<ChatMessage> messages = new ArrayList<>();


    public int size(){
        return messages.size();
    }

    public void addMessage(ChatMessage message){
        if (!(message instanceof  TextChatMessage)){
            messages.add(message);
            return;
        }


        boolean sameSenderAsLastMessage = isLastMessageSentBySameParticipant(message);
        TextChatMessage textMessage = (TextChatMessage) message;

        SentBySamePersonOnSameMinuteCriteria criteria = new SentBySamePersonOnSameMinuteCriteria(textMessage);
        ChatMessage chatMessage = Lists.firstOrDefault(messages, criteria);
        if( !sameSenderAsLastMessage || chatMessage == null || !(chatMessage instanceof CompositeTextChatMessage)){
            chatMessage = new CompositeTextChatMessage();
            chatMessage.sender = message.sender;
            chatMessage.receivedAt = message.receivedAt;
            chatMessage.senderImageUrl = message.senderImageUrl;
            chatMessage.sentAt = message.sentAt;
            chatMessage.sentByMe = message.sentByMe;
            chatMessage.status = message.status;

            messages.add(chatMessage);
        }

        CompositeTextChatMessage compositeMessage = (CompositeTextChatMessage)chatMessage;
        compositeMessage.addTextChatMessage(textMessage);
    }

    private boolean isLastMessageSentBySameParticipant(ChatMessage chatMessage){
        if(Lists.isNullOrEmpty(messages))
            return true;

        ChatMessage lastMessage = messages.get(messages.size()-1);
        return lastMessage.sender.equals(chatMessage.sender);
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof  GroupedMessages))
            return false;

        GroupedMessages other = (GroupedMessages) o;
        return this.formattedDate.equals(other.formattedDate);
    }

    @Override
    public int hashCode() {
        return formattedDate.hashCode();
    }

    public ChatMessage getMessageAt(int index){
        return this.messages.get(index);
    }

    public LocalDate getKey() {
       return date;
    }

    public String getTitle(){
        LocalDate today = new LocalDate();
        if(today.equals(date))
            return "Today";


        LocalDate yesterday = today.minusDays(1);
        if(yesterday.equals(date))
            return "Yestarday";

        return formattedDate;
    }
}
