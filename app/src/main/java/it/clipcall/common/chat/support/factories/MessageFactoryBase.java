package it.clipcall.common.chat.support.factories;

import android.util.Log;

import com.google.common.base.Strings;
import com.layer.sdk.messaging.Message;

import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.common.chat.models.IConversation;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.UnknownChatMessage;

/**
 * Created by dorona on 02/03/2016.
 */
public abstract class MessageFactoryBase implements  IMessageFactory {
    private static final String TAG = "MessageFactoryBase";

    @Override
    public boolean canCreateChatMessage(IMessage message) {
        Log.d(TAG, "canCreateChatMessage: message type:" + message.getMimeType());
        boolean isImageMessage = isMimeType(message, getMessageType().getMimeType());
        return isImageMessage;
    }

    protected boolean isMimeType(IMessage message, String mimeType){
        return mimeType.equalsIgnoreCase(message.getMimeType());
    }

    @Override
    public ChatMessage createChatMessage(MessageContext context, IMessage message) {
        ChatMessage chatMessage = createChatMessageCore(message);
        if(chatMessage == null)
            chatMessage = new UnknownChatMessage();

        chatMessage.status = getMessageStatus(context, message).toString();
        chatMessage.sentByMe = isMessageSentByMe(context, message);
        chatMessage.sentAt = message.getSentAt();
        chatMessage.receivedAt = message.getReceivedAt();

        ChatParticipant participantData = chatMessage.sentByMe ? context.me : context.otherSide;
        chatMessage.sender = participantData.nameOrPhoneNumber;
        chatMessage.senderImageUrl = participantData.imageUrl;

        return chatMessage;

    }





    public  abstract  ChatMessage createChatMessageCore(IMessage message);

    private Message.RecipientStatus getMessageStatus(MessageContext context, IMessage msg) {
        IConversation conversation = context.conversation;

        if (msg == null || msg.getSender() == null || msg.getSender().getUserId() == null)
            return Message.RecipientStatus.PENDING;

        //If we didn't send the message, we already know the status - we have read it
        if (!msg.getSender().getUserId().equalsIgnoreCase(context.authenticatedUserId))
            return Message.RecipientStatus.READ;

        //Assume the message has been sent
        Message.RecipientStatus status = Message.RecipientStatus.SENT;

        //Go through each user to check the status, in this case we check each user and
        // prioritize so
        // that we return the highest status: Sent -> Delivered -> Read
        for (int i = 0; i < conversation.getParticipants().size(); i++) {

            //Don't check the status of the current user
            String participant = conversation.getParticipants().get(i);
            if (participant.equalsIgnoreCase(context.authenticatedUserId))
                continue;

            if (status == Message.RecipientStatus.SENT) {


                if(msg.isDelivered(participant))
                    return Message.RecipientStatus.DELIVERED;
                if(msg.isRead(participant))
                    return Message.RecipientStatus.READ;


            } else if (status == Message.RecipientStatus.DELIVERED) {
                if (msg.isRead(participant))
                    return Message.RecipientStatus.READ;
            }
        }

        return status;
    }

    private boolean isMessageSentByMe(MessageContext context, IMessage message) {
        String senderTxt = "";
        if(message.getSender() == null)
            return false;

        if(Strings.isNullOrEmpty(message.getSender().getUserId()))
            return false;

        String userId = message.getSender().getUserId();
        boolean sentByMe = userId.equals(context.authenticatedUserId);
        return sentByMe;
    }


    @Override
    public boolean supportsMessageType(String messageType) {
        return getMessageType().getMimeType().equals(messageType);
    }
}
