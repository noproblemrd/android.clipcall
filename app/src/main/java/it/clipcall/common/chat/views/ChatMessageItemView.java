package it.clipcall.common.chat.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import it.clipcall.R;
import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.infrastructure.ui.images.CircleTransform;


public abstract class ChatMessageItemView extends RelativeLayout {

    TextView senderTextView;

    TextView messageDateTextView;
    ImageView senderAvatarImage;

    ViewGroup contentContainer;

    public ChatMessageItemView(Context context) {
        this(context, null);
    }

    public ChatMessageItemView(Context context, AttributeSet attributeSet){
        this(context, attributeSet, 0);
    }

    public ChatMessageItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.consumer_chat_messages_item, this);
        this.senderTextView = (TextView)findViewById(R.id.senderTextView);

        this.messageDateTextView = (TextView)findViewById(R.id.messageDateTextView);
        this.senderAvatarImage = (ImageView)findViewById(R.id.senderAvatarImage);
        this.contentContainer = (ViewGroup)findViewById(R.id.contentContainer);
    }

    public void bind(ChatMessage message){
        senderTextView.setText(message.sender);
        String pattern ="dd MMM yyyy, hh:mm aa";
        Date date = message.sentAt;
        if(date == null){
            date = message.receivedAt;
        }
        DateFormat formatter = new SimpleDateFormat(pattern);
        String formattedDate = formatter.format(date);
        messageDateTextView.setText(formattedDate);

        if(Strings.isNullOrEmpty(message.senderImageUrl))
        {
            Picasso.with(getContext()).load(R.drawable.common_profile_missing)
                    .fit()
                    .transform(new CircleTransform())
                    .into(senderAvatarImage);
        }
        else{
            Picasso.with(getContext()).load(message.senderImageUrl)
                    .fit()
                    .transform(new CircleTransform())
                    .into(senderAvatarImage);
        }

        bindExtra(message);
    }

    public abstract void bindExtra(ChatMessage message);
}
