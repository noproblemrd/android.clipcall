package it.clipcall.common.chat.models;

import java.util.List;

/**
 * Created by dorona on 04/04/2016.
 */
public class ChatPushMessage {
    public String senderId;
    public String senderName;
    public String senderImageUrl;

    public String receiverId;
    public String receiverName;
    public String receiverImageUrl;

    public String accountId;

    public String data;
    public Boolean senderIsPro;
    public Boolean receiverIsPro;

    public List<BotMessageRecipient> receiverUsers;
}
