package it.clipcall.common.chat.services;

import it.clipcall.common.chat.controllers.ChatController;

/**
 * Created by omega on 3/5/2016.
 */
public class ChatControllerProvider {

    private final ChatController chatManager;

    public ChatControllerProvider(ChatController chatManager){
        this.chatManager = chatManager;
    }

    public ChatController provideChatController(){
        return chatManager;
    }
}
