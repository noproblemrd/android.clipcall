package it.clipcall.common.chat.services.tasks;

import android.os.Bundle;
import android.os.Parcelable;

import org.parceler.Parcels;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.services.ChatParticipantFactory;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.tasks.ITask3;

@Singleton
public class NavigateToChatTask implements ITask3<ProjectEntity,ProfessionalRef,RoutingContext, Void> {


    private final RoutingService routingService;
    private final ChatParticipantFactory chatParticipantFactory;

    @Inject
    public NavigateToChatTask(RoutingService routingService, ChatParticipantFactory chatParticipantFactory){
        this.routingService = routingService;
        this.chatParticipantFactory = chatParticipantFactory;
    }

    @Override
    public Void execute(final ProjectEntity project, ProfessionalRef professional, RoutingContext routingContext) {
        Parcelable sender = Parcels.wrap(chatParticipantFactory.buildConsumerChatParticipant(project));
        Parcelable receiver =  Parcels.wrap(chatParticipantFactory.buildProfessionalChatParticipant(professional));
        NavigationContext navigationContext = new NavigationContext(routingContext);
        navigationContext.bundle = new Bundle();
        navigationContext.bundle.putParcelable("sender",sender);
        navigationContext.bundle.putBoolean("canSendQuote",false);
        navigationContext.bundle.putParcelable("receiver",receiver);
        navigationContext.bundle.putString("conversationId",professional.getLeadAccountId());
        navigationContext.addParameter("advertiserId", professional.getSupplierId());
        navigationContext.addParameter("projectId", project.getId());
        routingService.routeTo(Routes.ConsumerChatRoute, navigationContext);
        return null;
    }
}
