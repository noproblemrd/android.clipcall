package it.clipcall.common.chat.models;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;

/**
 * Created by dorona on 16/08/2016.
 */

public class ChatMessageBlockedKeywordsValidationCriteria implements ICriteria<String> {

    private final List<String> invalidKeywords = new ArrayList<>();


    public ChatMessageBlockedKeywordsValidationCriteria(){
        invalidKeywords.add("my number i");
        invalidKeywords.add("my phone n");
        invalidKeywords.add("skype");
        invalidKeywords.add("Twitter");
        invalidKeywords.add("Facebook");
        invalidKeywords.add("LinkedIn");
        invalidKeywords.add("Google+");
        invalidKeywords.add("WhatsApp");
        invalidKeywords.add("Snapchat");
        invalidKeywords.add("Vine");
        invalidKeywords.add("www.");
        invalidKeywords.add(".com");
    }

    @Override
    public boolean isSatisfiedBy(final String candidate) {
        final String loweredCandidate = candidate.toLowerCase();
        boolean isInvalid = Lists.any(invalidKeywords, new ICriteria<String>() {
            @Override
            public boolean isSatisfiedBy(String invalidKeyword) {
                return loweredCandidate.contains(invalidKeyword.toLowerCase());
            }
        });

        return isInvalid;
    }
}
