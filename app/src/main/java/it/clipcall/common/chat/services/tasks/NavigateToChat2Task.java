package it.clipcall.common.chat.services.tasks;

import android.os.Bundle;
import android.os.Parcelable;

import org.parceler.Parcels;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.tasks.ITask3;
import it.clipcall.professional.leads.cotrollers.ProfessionalProjectsController;
import it.clipcall.professional.leads.models.LeadEntity;
import it.clipcall.professional.leads.models.ProfessionalProjectDetails;
import it.clipcall.professional.leads.models.criterias.ProfessionalCanSendQuoteCriteria;
import it.clipcall.professional.profile.models.ProfessionalProfile;

@Singleton
public class NavigateToChat2Task implements ITask3<LeadEntity, ProfessionalProjectDetails,RoutingContext,Void> {


    private final ProfessionalProjectsController controller;
    private final RoutingService routingService;

    @Inject
    public NavigateToChat2Task(ProfessionalProjectsController controller, RoutingService routingService) {
        this.controller = controller;
        this.routingService = routingService;
    }

    @Override
    public Void execute(LeadEntity project, ProfessionalProjectDetails projectDetails, RoutingContext routingContext) {
        NavigationContext navigationContext = new NavigationContext( routingContext);
        navigationContext.bundle = new Bundle();

        String customerId = projectDetails.getCustomerId();
        String customerNameOrPhoneNumber = projectDetails.getCustomerDisplayName();
        String customerProfileImageUrl = projectDetails.getCustomerProfileImage();
        Parcelable receiver = Parcels.wrap(new ChatParticipant(customerId, customerProfileImageUrl, customerNameOrPhoneNumber, false));


        ProfessionalProfile professionalProfile = controller.getProfessionalProfile();
        String professionalName = professionalProfile.getBusinessName();
        String professionalProfileImageUrl = professionalProfile.getProfileImageUrl();
        String professionalId = controller.getProConsumerId();
        Parcelable sender = Parcels.wrap(new ChatParticipant(professionalId, professionalProfileImageUrl, professionalName, true));

        navigationContext.bundle.putString("conversationId",projectDetails.getLeadAccountId());
        navigationContext.bundle.putParcelable("sender",sender);
        navigationContext.bundle.putParcelable("receiver",receiver);

        ProfessionalCanSendQuoteCriteria criteria = new ProfessionalCanSendQuoteCriteria();
        boolean canSendQuote = criteria.isSatisfiedBy(project);
        navigationContext.bundle.putBoolean("canSendQuote",canSendQuote);
        routingService.routeTo(Routes.ProfessionalChatRoute, navigationContext);
        return null;
    }
}
