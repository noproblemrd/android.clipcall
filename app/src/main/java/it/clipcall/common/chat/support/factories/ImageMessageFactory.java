package it.clipcall.common.chat.support.factories;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;

import java.io.ByteArrayOutputStream;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatMessage;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.ImageChatMessage;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.support.tasks.IChatMessageTask;
import it.clipcall.common.chat.support.tasks.NoOpChatMessageTask;
import it.clipcall.common.chat.viewholders.ChatMessageContentViewHolder;
import it.clipcall.common.chat.viewholders.ChatMessageImageContentViewHolder;


@Singleton
public class ImageMessageFactory extends MessageFactoryBase {

    private static final String TAG = "ImageMessageFactory";

    @Inject
    public ImageMessageFactory(){

    }

    @Override
    public ChatMessage createChatMessageCore(IMessage message) {
        Log.d(TAG, "createChatMessageCore: creating image massage of type: "+ message.getMimeType());
        byte[][] data = message.getRawData();
        if(data == null)
        {
            Log.d(TAG, "createChatMessageCore: failed getting image data");
            return null;
        }

        byte[] imageData = data[0];
        if(imageData == null)
        {
            Log.d(TAG, "createChatMessageCore: failed getting image data. data[0] is null");
            return null;
        }

        Log.d(TAG, "createChatMessageCore: creating bitmap image of length: "+imageData.length);
        Bitmap bitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
        Log.d(TAG, "createChatMessageCore: created bitmap image successfully");
        ChatMessage chatMessage = new ImageChatMessage(bitmap);
        Log.d(TAG, "createChatMessageCore: created image chat message");
        return chatMessage;
    }

    @Override
    public MessageDescription createMessageDescription(Object data) {
        if(!(data instanceof  Bitmap))
            return null;

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Bitmap bitmap = (Bitmap) data;
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] imageData = stream.toByteArray();
        MessageDescription messageDescription = new MessageDescription(getMessageType().getMimeType(),imageData);
        messageDescription.pushData = "";
        messageDescription.pushTypeText = "an image";
        return messageDescription;
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.Image;
    }

    @Override
    public ChatMessageContentViewHolder createViewHolder(View v) {
        return new ChatMessageImageContentViewHolder(v);
    }

    @Override
    public IChatMessageTask createChatMessageTask() {
        return new NoOpChatMessageTask();
    }

    @Override
    public String getMessageDescription(IMessage message) {
        return "Sent you an image";
    }
}
