package it.clipcall.common.chat.models;

import org.parceler.Parcel;

/**
 * Created by dorona on 03/03/2016.
 */
@Parcel
public class ChatParticipant {

    public ChatParticipant(){

    }

    public ChatParticipant(String id, String imageUrl, String nameOrPhoneNumber, boolean isProfessional){
        this.imageUrl = imageUrl;
        this.nameOrPhoneNumber = nameOrPhoneNumber;
        this.id = id;
        this.isPro = isProfessional;

    }

    public String id;
    public String imageUrl;
    public String nameOrPhoneNumber;
    public boolean isPro;

    @Override
    public String toString() {
        return nameOrPhoneNumber;
    }
}
