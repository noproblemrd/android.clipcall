package it.clipcall.common.chat.controllers;

import android.app.Activity;

import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.common.chat.models.IConversation;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.services.ChatManager;

/**
 * Created by dorona on 20/01/2016.
 */
@Singleton
public class ChatController {

    private final ChatManager chatManager;


    @Inject
    public ChatController(ChatManager chatManager) {
        this.chatManager = chatManager;
    }

    public IConversation getConversationWithParticipant(ChatParticipant receiver, String conversationId) {
        IConversation conversation = chatManager.getConversationWithParticipant(receiver.id, conversationId);
        return conversation;
    }

    public IConversation getOrCreateAndGetConversationWithParticipant(ChatParticipant sender, ChatParticipant receiver, String conversationId) {
        IConversation conversation = chatManager.getConversationWithParticipant(sender, receiver, conversationId);
        if(conversation == null){
            conversation = chatManager.createConversation(sender,receiver,conversationId);
        }
        return conversation;
    }



    public List<IMessage> getConversationRecentMessages(IConversation conversation, int limit){
        if(conversation == null)
            return new ArrayList<>();

        List<IMessage> recentMessages = chatManager.getMessages(conversation, limit);
        return recentMessages;
    }

    public IMessage sendMessage(ChatParticipant sender, ChatParticipant receiver, String conversationId, MessageDescription messageDescription){
        if(Strings.isNullOrEmpty(receiver.id))
            return null;

        if(messageDescription == null)
            return null;

        if(Strings.isNullOrEmpty(messageDescription.messageType))
            return null;

        if(messageDescription.messageData == null)
            return null;


        IMessage message = chatManager.sendMessage(sender, receiver, conversationId, messageDescription);
        return message;

    }

    public String getAuthenticatedUserId() {
        return chatManager.getAuthenticatedUserId();
    }

    public void sendLogs(Activity activity) {
        chatManager.sendLogs(activity);
    }
}