package it.clipcall.common.payment.persistenceModel;

import it.clipcall.professional.leads.models.QuoteDetails;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by dorona on 06/07/2016.
 */
public interface IQuoteService {
    @GET("mode/userId/leads/{leadAccountId}/quotes/{quoteId}")
    Call<QuoteDetails> getQuoteDetails(@Path("leadAccountId") String leadAccountId, @Path("quoteId") String quoteId);
}
