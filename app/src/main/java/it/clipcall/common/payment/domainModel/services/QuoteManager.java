package it.clipcall.common.payment.domainModel.services;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.payment.persistenceModel.IQuoteService;
import it.clipcall.infrastructure.support.network.RetrofitCallProcessor;
import it.clipcall.professional.leads.models.QuoteDetails;
import retrofit2.Call;

@Singleton
public class QuoteManager {

    private final IQuoteService quoteService;

    @Inject
    public QuoteManager(IQuoteService quoteService){
        this.quoteService = quoteService;
    }


    public QuoteDetails getQuoteDetails(String leadAccountId, String quoteId) {
        Call<QuoteDetails> bonusClaimedCall = quoteService.getQuoteDetails(leadAccountId,quoteId);
        QuoteDetails quoteDetails = RetrofitCallProcessor.processCall(bonusClaimedCall);
        return quoteDetails;
    }

}
