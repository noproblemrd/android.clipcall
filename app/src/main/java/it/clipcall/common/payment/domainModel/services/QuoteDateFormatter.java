package it.clipcall.common.payment.domainModel.services;


import org.joda.time.LocalDate;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.support.collections.IFormatter;
import it.clipcall.infrastructure.support.dates.DateFormatter;
import it.clipcall.professional.leads.models.Quote;

@Singleton
public class QuoteDateFormatter implements IFormatter<Quote,String> {

    @Inject
    public QuoteDateFormatter(){

    }

    @Override
    public String format(Quote input) {
        String date = "Today";
        LocalDate localDate = LocalDate.fromDateFields(input.getStartJobAt());
        if(!localDate.isEqual(LocalDate.now()))
            date = DateFormatter.format(localDate, "d MMM");

        return date;
    }
}
