package it.clipcall.infrastructure.video;

/**
 * Created by dorona on 14/12/2015.
 */
public interface ISessionDataRetrievedListener {
    void onSessionConnectionDataReady(String apiKey, String sessionId, String token);
    void onWebServiceCoordinatorError(Exception error);
}
