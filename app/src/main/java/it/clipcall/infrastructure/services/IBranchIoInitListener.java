package it.clipcall.infrastructure.services;

/**
 * Created by dorona on 29/12/2015.
 */
public interface IBranchIoInitListener {

     void onComplete();
     void onError();
}
