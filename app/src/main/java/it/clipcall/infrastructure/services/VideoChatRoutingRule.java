package it.clipcall.infrastructure.services;

import android.content.Context;
import android.content.Intent;

import it.clipcall.common.invitations.services.BranchIoManager;
import it.clipcall.consumer.vidoechat.activities.ConsumerStartVideoChatActivity_;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.RouteNames;

/**
 * Created by dorona on 28/12/2015.
 */
public class VideoChatRoutingRule implements IRoutingRule {



    private final BranchIoManager branchIoManager;

    public VideoChatRoutingRule(BranchIoManager branchIoManager){
        this.branchIoManager = branchIoManager;
    }

    @Override
    public void given() {

    }

    @Override
    public boolean when(String sourceRouteName, String destinationRouteName) {
        if(!sourceRouteName.equals(RouteNames.SplashScreenRoute))
            return false;

        boolean hasPendingInvitation = false;
        return hasPendingInvitation;
    }

    @Override
    public void then(String destinationRouteName, RoutingContext routingContext) {
        if(! (routingContext instanceof RoutingContext))
            return;

        RoutingContext activityRoutingContext = (RoutingContext)routingContext;
        final Context context = activityRoutingContext.getContext();
        Intent intent = new Intent(context, ConsumerStartVideoChatActivity_.class);
        context.startActivity(intent);
    }

    @Override
    public int getPriority() {
        return 1000000;
    }
}
