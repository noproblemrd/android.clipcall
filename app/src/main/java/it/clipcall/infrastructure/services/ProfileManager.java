package it.clipcall.infrastructure.services;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ProfileManager {

    private String phoneNumber;

    @Inject
    public ProfileManager() {
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
