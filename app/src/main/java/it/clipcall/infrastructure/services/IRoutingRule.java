package it.clipcall.infrastructure.services;

import it.clipcall.infrastructure.routing.models.RoutingContext;

/**
 * Created by dorona on 28/12/2015.
 */
public interface IRoutingRule {


    void given();

    boolean when(String sourceRouteName, String destinationRouteName);

    void then(String destinationRouteName, RoutingContext routingContext);

    int getPriority();
}
