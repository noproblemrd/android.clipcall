package it.clipcall.infrastructure;

/**
 * Created by dorona on 09/11/2015.
 */
public class StatusResponse {
    public String status;

    public StatusResponse(){

    }

    public StatusResponse(String status){
        this.status = status;
    }
}
