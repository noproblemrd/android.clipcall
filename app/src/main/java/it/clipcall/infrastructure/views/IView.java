package it.clipcall.infrastructure.views;

import it.clipcall.infrastructure.routing.models.RoutingContext;

/**
 * Created by dorona on 12/11/2015.
 */
public interface IView {
//    void showProgressDialog(String title, String message);
//
//    void hideProgressDialog();
//
//    void navigateTo(String viewName);
//
//    void showFailureMessage(String title, String message);

    RoutingContext getRoutingContext();
}
