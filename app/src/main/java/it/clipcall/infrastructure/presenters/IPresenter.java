package it.clipcall.infrastructure.presenters;


import it.clipcall.infrastructure.views.IView;

public interface IPresenter{


    void initialize();

    void shutDown();
}
