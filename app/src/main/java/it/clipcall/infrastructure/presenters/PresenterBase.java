package it.clipcall.infrastructure.presenters;


import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.views.IView;


public class PresenterBase<T extends IView> implements IPresenter {

    protected T view;

    @RunOnUiThread
    public final void bindView(T view) {
        this.view = view;
    }


    public final void unbindView(){
        this.view = null;
    }

    @Override
    public void initialize() {

    }

    @Override
    public void shutDown() {

        view = null;
    }
}
