package it.clipcall.infrastructure.interceptors;

import java.io.IOException;

import it.clipcall.common.models.UserEntity;
import it.clipcall.professional.validation.services.AuthenticationManager;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by dorona on 15/11/2015.
 */
public class TokenInterceptor implements Interceptor {


    private final UserEntity user;

    public TokenInterceptor(UserEntity user){

        this.user = user;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        if(user.isAuthenticated()){
            Request newRequest = chain.request().newBuilder().addHeader("npToken", user.getToken()).build();
            return chain.proceed(newRequest);
        }
        return chain.proceed(chain.request());

    }
}
