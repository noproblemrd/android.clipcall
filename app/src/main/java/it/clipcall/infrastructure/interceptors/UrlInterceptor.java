package it.clipcall.infrastructure.interceptors;

import java.io.IOException;
import java.util.List;

import it.clipcall.common.models.UserEntity;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.strings.EqualsIgnoreCaseCriteria;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class UrlInterceptor implements Interceptor {

    private final UserEntity user;

    public UrlInterceptor(UserEntity user){
        this.user = user;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {

        if(!this.user.isAuthenticated())
            return chain.proceed(chain.request());

        //build the modified url
        HttpUrl.Builder urlBuilder = chain.request().url().newBuilder();
        HttpUrl url = chain.request().url();
        List<String> pathSegments = url.pathSegments();

        int modeIndex = Lists.indexOf(pathSegments, new EqualsIgnoreCaseCriteria("mode"));
        if(modeIndex >= 0){
            urlBuilder.setPathSegment(modeIndex, user.getMode());
        }

        int userIdIndex = Lists.indexOf(pathSegments, new EqualsIgnoreCaseCriteria("userId"));
        if(userIdIndex >= 0){
            urlBuilder.setPathSegment(userIdIndex, user.getUserId());
        }

        HttpUrl modifiedUrl = urlBuilder.build();
        Request.Builder requestBuilder = chain.request().newBuilder();
        requestBuilder.url(modifiedUrl);

        Request newRequest = requestBuilder.build();
        return chain.proceed(newRequest);
    }
}