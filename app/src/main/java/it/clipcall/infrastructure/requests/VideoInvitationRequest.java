package it.clipcall.infrastructure.requests;

import it.clipcall.common.invitations.models.eInvitationType;

/**
 * Created by dorona on 16/02/2016.
 */
public class VideoInvitationRequest {
    private int invitationType;

    private String supplierId;

    public int getInvitationType() {
        return invitationType;
    }

    public void setInvitationType(eInvitationType invitationType) {
        this.invitationType = invitationType.getValue();
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }
}
