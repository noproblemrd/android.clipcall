package it.clipcall.infrastructure.requests;

/**
 * Created by micro on 11/13/2015.
 */
public class PhoneValidationRequest {

    private String phone;

    private String code;

    private boolean call;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isCall() {
        return call;
    }

    public void setCall(boolean call) {
        this.call = call;
    }
}
