package it.clipcall.infrastructure.requests;

public class LoginRequest {

    private String phoneNumber;
    private String deviceName;
    private String deviceOs;
    private Object npAppVersion;
    private String osVersion;
    private String uid;

    /**
     *
     * @return
     * The phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     *
     * @param phoneNumber
     * The phoneNumber
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     *
     * @return
     * The deviceName
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     *
     * @param deviceName
     * The deviceName
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    /**
     *
     * @return
     * The deviceOs
     */
    public String getDeviceOs() {
        return deviceOs;
    }

    /**
     *
     * @param deviceOs
     * The deviceOs
     */
    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    /**
     *
     * @return
     * The npAppVersion
     */
    public Object getNpAppVersion() {
        return npAppVersion;
    }

    /**
     *
     * @param npAppVersion
     * The npAppVersion
     */
    public void setNpAppVersion(Object npAppVersion) {
        this.npAppVersion = npAppVersion;
    }

    /**
     *
     * @return
     * The osVersion
     */
    public String getOsVersion() {
        return osVersion;
    }

    /**
     *
     * @param osVersion
     * The osVersion
     */
    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getUid(){
        return this.uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}