package it.clipcall.infrastructure.routing.models;

import android.app.Activity;

/**
 * Created by dorona on 21/12/2015.
 */
public class RoutingContext {


    private final Activity context;

    public RoutingContext(Activity context){

        this.context = context;
    }

    public Activity getContext() {
        return context;
    }
}
