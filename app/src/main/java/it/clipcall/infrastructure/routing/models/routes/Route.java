package it.clipcall.infrastructure.routing.models.routes;


import it.clipcall.infrastructure.routing.models.NavigationContext;

public abstract class Route {

    public Route(String name , Route parent){
        this(name);
        this.parent = parent;
    }
    public Route(String name) {
        this.name = name;
    }

    public String name;

    private Route parent;


    public abstract void activate(NavigationContext navigationContext);

    @Override
    public String toString() {
        return name;
    }


    public Route getParent() {
        return parent;
    }

}
