package it.clipcall.infrastructure.routing.models.routes;

import android.app.Activity;
import android.content.Intent;

import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;

public class SmsInvitationRoute extends Route {
    public SmsInvitationRoute(String name) {
        super(name);
    }

    @Override
    public void activate(NavigationContext navigationContext) {
        RoutingContext routingContext = (RoutingContext)navigationContext.routingContext;
        String messageBody = navigationContext.getParameter("messageBody");
        final Activity context = routingContext.getContext();
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        sendIntent.putExtra("sms_body", messageBody);
        sendIntent.setType("vnd.android-dir/mms-sms");
        context.startActivity(sendIntent);
    }
}
