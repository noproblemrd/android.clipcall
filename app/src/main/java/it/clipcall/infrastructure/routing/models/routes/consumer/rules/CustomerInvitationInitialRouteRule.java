package it.clipcall.infrastructure.routing.models.routes.consumer.rules;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.invitations.models.eInvitationType;
import it.clipcall.common.invitations.services.BranchIoManager;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RouteActivationService;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class CustomerInvitationInitialRouteRule extends InitialRouteRule {

    private final BranchIoManager branchIoManager;
    private final AuthenticationManager authenticationManager;

    @Inject
    public CustomerInvitationInitialRouteRule(BranchIoManager branchIoManager, AuthenticationManager authenticationManager) {
        this.branchIoManager = branchIoManager;
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected boolean shouldBeAppliedCore(Route route) {
        if(route != null)
            return false;


        boolean hasInvitation =  branchIoManager.hasInvitation(eInvitationType.Video);
        return hasInvitation;
    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {

        authenticationManager.switchToConsumerMode();
        String invitationId = branchIoManager.getInvitationId();
        navigationContext.addParameter("invitationId",invitationId);
        //branchIoManager.clearInvitation();
        routeActivationService.activate(Routes.ConsumerStartVideoProjectRoute,navigationContext);
    }
}