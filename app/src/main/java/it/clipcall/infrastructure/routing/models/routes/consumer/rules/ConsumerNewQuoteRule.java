package it.clipcall.infrastructure.routing.models.routes.consumer.rules;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.payment.models.QuoteProjectData;
import it.clipcall.infrastructure.repositories.Repository2;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RouteActivationService;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by micro on 1/26/2016.
 */
@Singleton
public class ConsumerNewQuoteRule extends InitialRouteRule {

    private final Repository2 repository;
    private final AuthenticationManager authenticationManager;
    private final RouteParams routeParams;


    private QuoteProjectData currentProjectQuote;

    @Inject
    public ConsumerNewQuoteRule(Repository2 repository, AuthenticationManager authenticationManager, RouteParams routeParams) {
        this.repository = repository;
        this.authenticationManager = authenticationManager;
        this.routeParams = routeParams;
    }


    @Override
    protected boolean shouldBeAppliedCore(Route route) {

        QuoteProjectData quoteProjectData = repository.getSingle(QuoteProjectData.class);
        if(quoteProjectData == null)
            return false;

        repository.delete(quoteProjectData);
        currentProjectQuote = quoteProjectData;

       return true;

    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {
        routeParams.setParam("project",currentProjectQuote.project);
        routeParams.setParam("routeBackToHome", true);
        routeParams.setParam("quote",currentProjectQuote.quote);
        routeParams.setParam("leadAccountId",currentProjectQuote.leadAccountId);
        routeActivationService.activate(Routes.ConsumerQuoteDetailsRoute, navigationContext);
    }
}