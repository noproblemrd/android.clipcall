package it.clipcall.infrastructure.routing.models;

import android.app.Activity;
import android.support.v4.app.Fragment;

/**
 * Created by dorona on 15/03/2016.
 */
public class FragmentDecoratorRoutingContext extends RoutingContext {

    private final Fragment fragment;

    public FragmentDecoratorRoutingContext(Activity activity, Fragment fragment){
        super(activity);
        this.fragment = fragment;
    }

    public Fragment getFragment() {
        return fragment;
    }
}
