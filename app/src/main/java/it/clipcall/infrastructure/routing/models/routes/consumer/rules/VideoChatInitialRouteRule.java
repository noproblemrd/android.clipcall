package it.clipcall.infrastructure.routing.models.routes.consumer.rules;

import org.simple.eventbus.EventBus;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.invitations.models.eInvitationType;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RouteActivationService;
import it.clipcall.common.invitations.services.BranchIoManager;

@Singleton
public class VideoChatInitialRouteRule extends InitialRouteRule {

    private final BranchIoManager branchIoManager;

    @Inject
    public VideoChatInitialRouteRule(BranchIoManager branchIoManager) {
        this.branchIoManager = branchIoManager;
    }

    @Override
    protected boolean shouldBeAppliedCore(Route route) {
        if(route != null)
            return false;

        boolean hasInvitation =  branchIoManager.hasInvitation(eInvitationType.VideoChat);
        return hasInvitation;
    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {
        routeActivationService.activate(Routes.ConsumerStartVideoChatRoute, navigationContext);
        EventBus.getDefault().post(new ApplicationEvent("DeepLinkClick",branchIoManager.getInvitationData()));
    }
}