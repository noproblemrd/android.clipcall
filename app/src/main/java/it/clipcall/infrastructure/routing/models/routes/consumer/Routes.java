package it.clipcall.infrastructure.routing.models.routes.consumer;

import it.clipcall.R;
import it.clipcall.common.activities.FullScreenActivity_;
import it.clipcall.common.chat.activities.ChatActivity_;
import it.clipcall.common.projects.activities.ProjectVideoActivity_;
import it.clipcall.common.validation.activities.PhoneValidation2Activity_;
import it.clipcall.common.validation.activities.PhoneValidationActivity_;
import it.clipcall.consumer.about.activities.PdfActivity_;
import it.clipcall.consumer.about.activities.WebViewActivity_;
import it.clipcall.consumer.about.activities.WebViewDialogActivity_;
import it.clipcall.consumer.about.fragments.ConsumerAboutFragment_;
import it.clipcall.consumer.findprofessional.activities.CameraPreviewActivity_;
import it.clipcall.consumer.findprofessional.activities.SubmitProjectToAdvertisersActivity_;
import it.clipcall.consumer.findprofessional.fragments.ConsumerFindProfessionalFragment_;
import it.clipcall.consumer.help.activities.ConsumerTutorialActivity_;
import it.clipcall.consumer.help.fragments.ConsumerHelpFragment_;
import it.clipcall.consumer.mainMenu.activities.ConsumerMainMenuActivity_;
import it.clipcall.consumer.payment.activities.AndroidPayActivity_;
import it.clipcall.consumer.payment.activities.ConsumerClaimDiscountActivity_;
import it.clipcall.consumer.payment.activities.ConsumerPayYourProActivity_;
import it.clipcall.consumer.payment.activities.ConsumerProfessionalPaymentProjectsActivity_;
import it.clipcall.consumer.payment.activities.CreditCardDetailsActivity_;
import it.clipcall.consumer.payment.activities.CustomerCreditCardDetailsActivity_;
import it.clipcall.consumer.payment.activities.PayTheProActivity_;
import it.clipcall.consumer.payment.activities.PaymentDetailsActivity_;
import it.clipcall.consumer.payment.activities.PaymentInvoiceActivity_;
import it.clipcall.consumer.payment.fragments.ConsumerBonusCashFragment_;
import it.clipcall.consumer.payment.viewModel.activities.ConsumerQuoteDetailsActivity_;
import it.clipcall.consumer.payment.viewModel.activities.QuoteBookedActivity_;
import it.clipcall.consumer.profile.fragments.ConsumerProfileFragment_;
import it.clipcall.consumer.projects.activities.ConsumerProjectDetailsActivity_;
import it.clipcall.consumer.projects.activities.ProjectAdvertiserActivity_;
import it.clipcall.consumer.projects.fragments.ConsumerProjectsFragment_;
import it.clipcall.consumer.settings.fragments.ConsumerSettingsFragment_;
import it.clipcall.consumer.vidoechat.activities.ConsumerIncomingVideoCallActivity_;
import it.clipcall.consumer.vidoechat.activities.ConsumerStartVideoChatActivity_;
import it.clipcall.consumer.vidoechat.activities.ConsumerVideoChatCallingActivity_;
import it.clipcall.infrastructure.routing.models.RouteNames;
import it.clipcall.infrastructure.routing.models.routes.ActivityForResultRoute;
import it.clipcall.infrastructure.routing.models.routes.ActivityRoute;
import it.clipcall.infrastructure.routing.models.routes.FragmentRoute;
import it.clipcall.infrastructure.routing.models.routes.PickCroppedImageFromGalleryRoute;
import it.clipcall.infrastructure.routing.models.routes.PickImageFromCameraRoute;
import it.clipcall.infrastructure.routing.models.routes.PickImageFromGalleryRoute;
import it.clipcall.infrastructure.routing.models.routes.RateUsRoute;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.SendInvitationRoute;
import it.clipcall.infrastructure.routing.models.routes.ShareWithFriendsRoute;
import it.clipcall.infrastructure.routing.models.routes.StartVideoActivityRoute;
import it.clipcall.professional.invitecustomer.fragments.ProfessionalInviteCustomerFragment_;
import it.clipcall.professional.leads.activities.ProfessionalNewProjectActivity_;
import it.clipcall.professional.leads.activities.ProfessionalProjectDetailsActivity_;
import it.clipcall.professional.leads.fragments.ProfessionalProjectsFragment_;
import it.clipcall.professional.mainMenu.MainMenuActivity_;
import it.clipcall.professional.payment.activities.ProfessionalPaymentSettingsActivity_;
import it.clipcall.professional.payment.activities.StripeWebViewActivity_;
import it.clipcall.professional.payment.fragments.ProfessionalBonusCashFragment_;
import it.clipcall.professional.payment.fragments.ProfessionalPaymentSettingsFragment_;
import it.clipcall.professional.payment.viewModel.activities.ProfessionalCreateQuoteActivity_;
import it.clipcall.professional.profile.activities.ProfessionalEditProfileActivity_;
import it.clipcall.professional.profile.activities.ProfessionalServiceCategoriesActivity_;
import it.clipcall.professional.profile.fragments.ProfessionalProfileFragment_;
import it.clipcall.professional.registration.activities.AdvertiserRegistrationActivity_;
import it.clipcall.professional.registration.activities.ProfessionalTermsOfServiceAgreementActivity_;
import it.clipcall.professional.remoteservicecall.fragments.ProfessionalRemoteServiceCallFragment_;
import it.clipcall.professional.settings.fragments.ProfessionalSettingsFragment_;

/**
 * Created by dorona on 03/01/2016.
 */
public class Routes {


    public static final Route ConsumerMainMenuActivityRoute = new ActivityRoute(RouteNames.ConsumerMainMenuActivity, ConsumerMainMenuActivity_.class);
    public static final Route ConsumerFindProRoute = new FragmentRoute(Integer.valueOf(R.id.nav_find_pro_consumer).toString(), ConsumerFindProfessionalFragment_.class, ConsumerMainMenuActivityRoute);
    public static final Route ConsumerProjectsRoute = new FragmentRoute(Integer.valueOf(R.id.nav_my_projects_consumer).toString(), ConsumerProjectsFragment_.class, ConsumerMainMenuActivityRoute);

    public static final Route consumerProfileRoute = new FragmentRoute(Integer.valueOf(R.id.nav_user_profile_consumer).toString(), ConsumerProfileFragment_.class, ConsumerMainMenuActivityRoute);
    public static final Route ConsumerBonusCashRoute = new FragmentRoute(Integer.valueOf(R.id.bonusCashMenuItem).toString(), ConsumerBonusCashFragment_.class, ConsumerMainMenuActivityRoute);
    public static final Route ConsumerPayYourProRoute = new ActivityRoute("CONSUMER_PAY_YOUR_PRO_ROUTE", ConsumerPayYourProActivity_.class);



    public static final Route ConsumerSettingsRoute = new FragmentRoute(Integer.valueOf(R.id.nav_settings_consumer).toString(), ConsumerSettingsFragment_.class, ConsumerMainMenuActivityRoute);
    public static final Route ConsumerAboutRoute = new FragmentRoute(Integer.valueOf(R.id.nav_about_consumer).toString(), ConsumerAboutFragment_.class, ConsumerMainMenuActivityRoute);
    public static final Route ConsumerHelpRoute = new FragmentRoute(Integer.valueOf(R.id.nav_help_consumer).toString(), ConsumerHelpFragment_.class, ConsumerMainMenuActivityRoute);
    // public static final Route nav_switch_to_advertiser = new ActivityRoute(Integer.valueOf(R.id.nav_switch_to_advertiser).toString(),ConsumerMainMenuActivity_.class);
    public static final Route ConsumerValidationRoute = new ActivityRoute(RouteNames.ConsumerValidationRoute, PhoneValidationActivity_.class);
    public static final Route ConsumerValidation2Route = new ActivityRoute(RouteNames.ConsumerValidation2Route, PhoneValidation2Activity_.class);
    public static final Route ConsumerStartVideoChatRoute = new ActivityRoute(RouteNames.ConsumerStartVideoChatRoute, ConsumerStartVideoChatActivity_.class);
    public static final Route ConsumerVideoChatCalling = new ActivityRoute(RouteNames.ConsumerVideoChatCalling, ConsumerVideoChatCallingActivity_.class);
    public static final Route ConsumerStartVideoChat2Route = new ActivityRoute(RouteNames.ConsumerStartVideoChat2Route, ConsumerStartVideoChatActivity_.class);
    public static final Route ConsumerProjectDetailsRoute = new ActivityRoute(RouteNames.ConsumerProjectDetailsRoute, ConsumerProjectDetailsActivity_.class);
    public static final Route ConsumerStartVideoProjectRoute = new ActivityRoute(RouteNames.ConsumerStartVideoProjectRoute, CameraPreviewActivity_.class);
    public static final Route ConsumerAdvertiserRoute = new ActivityRoute(RouteNames.ConsumerAdvertiserRoute, ProjectAdvertiserActivity_.class);
    public static final Route ConsumerTutorialRoute = new ActivityRoute(RouteNames.ConsumerTutorialRoute, ConsumerTutorialActivity_.class);
    public static final Route ConsumerRecordVideoRoute = new FragmentRoute(RouteNames.ConsumerRecordVideoRoute, ConsumerProfileFragment_.class, ConsumerMainMenuActivityRoute);
    public static final Route ConsumerSubmitProjectToAdvertiserRoute = new ActivityRoute(RouteNames.ConsumerSubmitProjectToAdvertisersRoute, SubmitProjectToAdvertisersActivity_.class);
    public static final Route PayTheProRoute = new ActivityRoute(RouteNames.PayTheProRoute, PayTheProActivity_.class);
    public static final Route ConsumerAddCreditCardRoute = new ActivityRoute(RouteNames.ConsumerAddCreditCardRoute, CustomerCreditCardDetailsActivity_.class);
    public static final Route ConsumerCreditCardDetailsdRoute = new ActivityRoute("CONSUMER_CREDIT_CARD_DETAILS_ROUTE", CreditCardDetailsActivity_.class);
    public static final Route ConsumerQuoteBookedRoute = new ActivityRoute("CONSUMER_QUOTE_BOOKED_ROUTE", QuoteBookedActivity_.class);
    public static final Route ConsumerPaymentDetailsRoute = new ActivityRoute("CONSUMER_PAYMENT_DETAILS_ROUTE", PaymentDetailsActivity_.class);
    public static final Route ConsumerPaymentInvoiceRoute = new ActivityRoute(RouteNames.ConsumerPaymentInvoiceRoute, PaymentInvoiceActivity_.class);
    public static final Route ConsumerChatRoute = new ActivityRoute(RouteNames.ConsumerChatRoute, ChatActivity_.class);
    public static final Route ConsumerEulaRoute = new ActivityRoute(RouteNames.ConsumerEulaRoute, PdfActivity_.class);
    public static final Route ConsumerPrivacyPolicyRoute = new ActivityRoute(RouteNames.ConsumerPrivacyPolicyRoute, PdfActivity_.class);
    public static final Route ConsumerFaqRoute = new ActivityRoute(RouteNames.ConsumerFaqRoute, PdfActivity_.class);
    public static final Route ConsumerRateUsRoute = new RateUsRoute(R.id.nav_rate_us_consumer + "");
    public static final Route ConsumerShareWithFriends = new ShareWithFriendsRoute(R.id.nav_share_with_friends_consumer + "");
    public static final Route ConsumerTermsOfServiceRoute = new ActivityRoute(RouteNames.ConsumerEulaRoute, PdfActivity_.class);
    public static final Route ConsumerBuyerProtectionRoute = new ActivityRoute(RouteNames.ConsumerEulaRoute, PdfActivity_.class);
    public static final Route ConsumerAndroidPayActivity = new ActivityRoute(RouteNames.ConsumerAndroidPayRoute, AndroidPayActivity_.class);
    public static final Route ConsumerAcceptVideoCallRoute = new StartVideoActivityRoute(RouteNames.ConsumerAcceptVideoCallRoute, ConsumerIncomingVideoCallActivity_.class);
    public static final Route ConsumerProfessionalPaymentProjectsRoute = new ActivityRoute("CONSUMER_PROFESSIONAL_PAYMENT_PROJECTS_ROUTE", ConsumerProfessionalPaymentProjectsActivity_.class);
    public static final Route ConsumerClaimDiscountRoute = new ActivityRoute("CONSUMER_CLAIM_DISCOUNT_ROUTE", ConsumerClaimDiscountActivity_.class);

    public static final Route ConsumerQuoteDetailsRoute = new ActivityRoute("CONSUMER_QUOTE_DETAILS_ROUTE", ConsumerQuoteDetailsActivity_.class);

    public static final Route TermsAndConditionsApplyRoute = new ActivityRoute("TERMS_AND_CONDITIONS_APPLY_ROUTE", WebViewActivity_.class);








    //professional routes
    public static final Route AdvertiserMainMenuRoute = new ActivityRoute(RouteNames.AdvertiserMainMenuRoute, MainMenuActivity_.class);
    public static final Route ProfessionalChatRoute = new ActivityRoute(RouteNames.ProfessionalChatRoute, ChatActivity_.class);
    public static final Route ProfessionalProjectsRoute = new FragmentRoute(Integer.valueOf(R.id.professionalProjectsMenuItem).toString(), ProfessionalProjectsFragment_.class, AdvertiserMainMenuRoute);
    public static final Route ProfessionalPaymentSettingsRoute = new FragmentRoute(Integer.valueOf(R.id.professionalPaymentSettingsMenuItem).toString(), ProfessionalPaymentSettingsFragment_.class, AdvertiserMainMenuRoute);
    public static final Route ProfessionalFullPaymentSettingsRoute = new ActivityRoute("PROFESSIONAL_FULL_PAYMENT_SETTINGS_ROUTES", ProfessionalPaymentSettingsActivity_.class);

    public static final Route ProfessionalNewProjectRoute = new ActivityRoute(RouteNames.ProfessionalNewProjectRoute, ProfessionalNewProjectActivity_.class);
    public static final Route ProfessionalSelectServiceCategoriesRoute = new ActivityRoute(RouteNames.ProfessionalSelectServiceCategoriesRoute, ProfessionalServiceCategoriesActivity_.class);




    public static final Route ProfessionalProjectDetailsRoute = new ActivityRoute(RouteNames.ProfessionalProjectDetailsRoute, ProfessionalProjectDetailsActivity_.class);
    public static final Route ProfessionalRemoteServiceCallRoute = new FragmentRoute(Integer.valueOf(R.id.professional_remote_service_call_menu_item).toString(), ProfessionalRemoteServiceCallFragment_.class, AdvertiserMainMenuRoute);
    public static final Route ProfessionalInviteCustomerRoute = new FragmentRoute(Integer.valueOf(R.id.professional_invite_customer_menu_item).toString(), ProfessionalInviteCustomerFragment_.class, AdvertiserMainMenuRoute);


    public static final Route ProfessionalBonusCashRoute = new FragmentRoute(Integer.valueOf(R.id.professionalBonusCashMenuItem).toString(), ProfessionalBonusCashFragment_.class, AdvertiserMainMenuRoute);

    public static final Route nav_settings = new FragmentRoute(Integer.valueOf(R.id.nav_settings).toString(), ProfessionalSettingsFragment_.class, AdvertiserMainMenuRoute);
    public static final Route nav_business_profile = new FragmentRoute(Integer.valueOf(R.id.nav_business_profile).toString(), ProfessionalProfileFragment_.class, AdvertiserMainMenuRoute);
    //public static final Route nav_help = new FragmentRoute(Integer.valueOf(R.id.nav_help).toString(), ProfessionalProjectsFragment_.class, AdvertiserMainMenuRoute);
    //public static final Route nav_about = new FragmentRoute(Integer.valueOf(R.id.nav_about).toString(), ProfessionalProjectsFragment_.class, AdvertiserMainMenuRoute);
    public static final Route ProfessionalRegistrationRoute = new ActivityRoute(RouteNames.ProfessionalRegistrationRoute, AdvertiserRegistrationActivity_.class);
    public static final Route ProfessionalServiceCallInvitationBySmsRoute = new SendInvitationRoute(RouteNames.ProfessionalServiceCallInvitationBySmsRoute);
    public static final Route ProfessionalShareVideoRoute = new SendInvitationRoute(RouteNames.ProfessionalShareVideoRoute);
    public static final Route ProfessionalServiceCallInvitationRoute = new SendInvitationRoute(RouteNames.ProfessionalServiceCallInvitationRout + "");
    public static final Route ProfessionalEditProfileRoute = new ActivityRoute(RouteNames.ProfessionalEditProfileRoute, ProfessionalEditProfileActivity_.class);


    public static final Route ProfessoinalTermsOfServiceAgreementRoute = new ActivityRoute(Integer.valueOf(R.id.termsOfServiceMenuItem).toString(), ProfessionalTermsOfServiceAgreementActivity_.class);
    public static final Route ProfessionalTermsOfServiceAgreement2Route = new ActivityRoute("PROFESSIONAL_TERMS_OF_SERVICE_AGREEMENT_2_ROUTE", WebViewDialogActivity_.class);
    public static final Route ProfessionalRegistrationTermsOfServiceAgreement2Route = new ActivityForResultRoute("PROFESSIONAL_REGISTRATION_TERMS_OF_SERVICE_AGREEMENT_2_ROUTE", WebViewDialogActivity_.class);
    public static final Route ProfessionalCreateQuoteRoute = new ActivityRoute("PROFESSIONAL_CREATE_QUOTE_ROUTE", ProfessionalCreateQuoteActivity_.class);





    public static final Route ProfessionalAcceptVideoCallRoute = new StartVideoActivityRoute(RouteNames.ProfessionalAcceptVideoChatCall, ConsumerIncomingVideoCallActivity_.class);
    public static Route ProfessionalVideoChatRoute = new ActivityRoute(RouteNames.ProfessionalVideoChatRoute, ConsumerVideoChatCallingActivity_.class);
    //common
    public static final Route CommonPickImageFromGalleryRoute = new PickImageFromGalleryRoute(RouteNames.PickImageFromGalleryRoute);
    public static final Route CommonPickCroppedImageFromGalleryRoute = new PickCroppedImageFromGalleryRoute(RouteNames.PickCroppedImageFromGalleryRoute);
    public static final Route CommonPickImageFromCameraRoute = new PickImageFromCameraRoute(RouteNames.PickImageFromCameraRoute);
    public static final Route ProjectVideoRoute = new ActivityRoute(RouteNames.ProjectVideoRoute, ProjectVideoActivity_.class);

    public static final Route ImageFullScreenActivity = new ActivityRoute("FULL_SCREEN_ACTIVITY", FullScreenActivity_.class);


    public static final Route StripeLoginRoute = new ActivityRoute("STRIPE_LOGIN_ROUTE", StripeWebViewActivity_.class);
}
