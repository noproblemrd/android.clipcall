package it.clipcall.infrastructure.routing.services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.IRouteRule;

public class RoutingService2 {

    private final RouteResolver routeResolver;
    private final List<IRouteRule> routeRules;
    private RouteActivationService routeActivationService;

    @Singleton
    @Inject
    public RoutingService2(RouteResolver routeResolver, List<IRouteRule> routeRules, RouteActivationService routeActivationService ){
        this.routeResolver = routeResolver;
        this.routeRules = routeRules;
        this.routeActivationService = routeActivationService;
    }

    public void routeTo(Route route, NavigationContext navigationContext){
        for(IRouteRule rule: routeRules){
            if(rule.shouldBeApplied(route, routeActivationService))
            {
                rule.apply(routeActivationService, navigationContext, route);
                return;
            }
        }
    }

    public void routeToHome(RoutingContext routingContext){
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = routingContext;
        navigationContext.isRoot = true;
        routeActivationService.setPendingRoute(Routes.ConsumerProjectsRoute);
        routeTo(Routes.ConsumerMainMenuActivityRoute, navigationContext);
    }

    public void routeTo(String routeName, NavigationContext navigationContext){
        Route route = this.routeResolver.resolveRoute(routeName);
        this.routeTo(route, navigationContext);
    }

    public boolean hasPendingSubRoute() {
        Route pendingRoute = routeActivationService.getPendingRoute();
        if(pendingRoute == null)
            return false;

        Route parent = pendingRoute.getParent();
        if(parent == null)
            return false;

        Route currentRoute = routeActivationService.getCurrentRoute();
        if(currentRoute == null)
            return false;


        if(currentRoute.equals(Routes.ConsumerValidationRoute))
            return false;

        if(currentRoute.equals(Routes.ConsumerValidation2Route))
            return false;

        return true;
    }

    public void clearPendingRoute() {
        routeActivationService.setPendingRoute(null);
    }
}