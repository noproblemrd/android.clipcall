package it.clipcall.infrastructure.routing.models.routes;

import android.app.Activity;
import android.content.Intent;

import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import me.kentin.yeti.Yeti;

public class ShareWithFriendsRoute extends Route {
    public ShareWithFriendsRoute(String name) {
        super(name);
    }

    @Override
    public void activate(NavigationContext navigationContext) {
        RoutingContext routingContext = (RoutingContext)navigationContext.routingContext;
        final Activity context = routingContext.getContext();
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "This is the text BEFORE you change it bro. (if you want huh)");

        Yeti yeti = Yeti.with(context);
        Intent intent = yeti.share(shareIntent);
        String requestCode = navigationContext.getParameter("requestCode");
        context.startActivityForResult(intent, Integer.valueOf(requestCode));
    }
}
