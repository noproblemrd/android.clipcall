package it.clipcall.infrastructure.routing.models.routes.consumer.rules;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RouteActivationService;

@Singleton
public class VideoChatPhoneValidationRequiredRouteActionRule implements IRouteRule {

    private final AuthenticationManager authenticationManager;


    @Inject
    public VideoChatPhoneValidationRequiredRouteActionRule(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public boolean shouldBeApplied(Route route, RouteActivationService routeActivator) {
        if(route == null)
            return false;

        if(!route.equals(Routes.ConsumerStartVideoChat2Route))
            return false;


        return true;

    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {
        routeActivationService.setPendingRoute(Routes.ConsumerStartVideoChat2Route);
        routeActivationService.activate(Routes.ConsumerValidationRoute, navigationContext);
    }
}
