package it.clipcall.infrastructure.routing.models.routes.consumer.rules;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.services.RouteActivationService;

/**
 * Created by dorona on 03/01/2016.
 */
@Singleton
public class DefaultActionRouteRule implements IRouteRule {


    @Inject
    public DefaultActionRouteRule() {
    }

    @Override
    public boolean shouldBeApplied(Route route, RouteActivationService routeActivator) {
        return true;
    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {
        routeActivationService.activate(route,navigationContext);
    }

}
