package it.clipcall.infrastructure.routing.models.routes.consumer.rules;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.services.RouteActivationService;

@Singleton
public class RequiresPhoneValidationRule implements IRouteRule {

    private final AuthenticationManager authenticationManager;

    private final List<Route> routesRequiringValidation = new ArrayList<>();

    @Inject
    public RequiresPhoneValidationRule(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        this.routesRequiringValidation.add(Routes.consumerProfileRoute);
        this.routesRequiringValidation.add(Routes.ConsumerSettingsRoute);
        //this.routesRequiringValidation.add(Routes.ConsumerPayYourProRoute);
        //this.routesRequiringValidation.add(Routes.ConsumerProjectsRoute);
    }

    @Override
    public boolean shouldBeApplied(Route route, RouteActivationService routeActivator) {
        if(route == null)
            return false;

       if(authenticationManager.isCustomerAuthenticated())
           return false;

        for(Route routeRequiringValidation : routesRequiringValidation)
        {
            if(routeRequiringValidation.equals(route))
                return  true;
        }

        return false;
    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {
        routeActivationService.setPendingRoute(route);
        routeActivationService.activate(Routes.ConsumerValidationRoute, navigationContext);
    }
}