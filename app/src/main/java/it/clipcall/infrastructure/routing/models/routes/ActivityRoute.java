package it.clipcall.infrastructure.routing.models.routes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import java.util.Map;

import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;

/**
 * Created by dorona on 03/01/2016.
 */
public class ActivityRoute extends Route {
    private Class<? extends BaseActivity> activityClass;
    protected final Handler handler = new Handler();

    public ActivityRoute(String name,  Class<? extends BaseActivity> activityClass) {
        super(name);
        this.activityClass = activityClass;
    }

    protected  final Intent createIntent(final NavigationContext navigationContext){
        RoutingContext routingContext = (RoutingContext)navigationContext.routingContext;
        final Context context = routingContext.getContext();
        Bundle bundle = navigationContext.bundle != null ? navigationContext.bundle : new Bundle();
        Map<String, String> parameters = navigationContext.getParameters();
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            bundle.putString(key, value);
        }
        Intent intent = new Intent(context, activityClass);
        intent.putExtras(bundle);
        if(navigationContext.isRoot){
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK  | Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        if(navigationContext.navigateBack){
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }

        return intent;
    }

    @Override
    public void activate(final NavigationContext navigationContext) {
        if(!(navigationContext.routingContext instanceof RoutingContext))
            return;

        RoutingContext routingContext = (RoutingContext)navigationContext.routingContext;
        final Activity context = routingContext.getContext();
        Intent intent = createIntent(navigationContext);
        if(navigationContext.uiBundle != null){
            context.startActivity(intent, navigationContext.uiBundle);
        }else{
            context.startActivity(intent);
        }

        if(navigationContext.isRoot){
           if(!context.isFinishing())
               context.finish();
        }
    }

    @Override
    public String toString() {
        return activityClass.getCanonicalName();
    }
}
