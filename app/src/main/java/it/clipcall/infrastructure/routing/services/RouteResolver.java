package it.clipcall.infrastructure.routing.services;

import java.util.List;

import it.clipcall.infrastructure.routing.models.routes.Route;

/**
 * Created by micro on 2/9/2016.
 */
public class RouteResolver {

    private final List<Route> routes;

    public RouteResolver(List<Route> routes) {
        this.routes = routes;
    }

    Route resolveRoute(String routeName){
        for(Route route: routes){
            if(route.name.equals(routeName))
                return route;
        }
        return null;
    }
}
