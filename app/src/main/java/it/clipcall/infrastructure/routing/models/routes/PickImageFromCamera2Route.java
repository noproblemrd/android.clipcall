package it.clipcall.infrastructure.routing.models.routes;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import it.clipcall.infrastructure.routing.models.FragmentDecoratorRoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;

public class PickImageFromCamera2Route extends Route {
    public PickImageFromCamera2Route(String name) {
        super(name);
    }


    @Override
    public void activate(NavigationContext navigationContext) {
        RoutingContext routingContext = navigationContext.routingContext;

        if(!(routingContext instanceof FragmentDecoratorRoutingContext))
            return;

        FragmentDecoratorRoutingContext fragmentRoutingContext = (FragmentDecoratorRoutingContext) routingContext;

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String requestCode = navigationContext.getParameter("requestCode");
        int finalRequestCode = Integer.valueOf(requestCode);
        fragmentRoutingContext.getFragment().startActivityForResult(intent, finalRequestCode);
    }
}
