package it.clipcall.infrastructure.routing.models.routes.consumer.rules;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.PendingSystemChatMessage;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.infrastructure.repositories.Repository2;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RouteActivationService;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by micro on 1/26/2016.
 */
@Singleton
public class ConsumerPendingSystemChatMessageRule extends InitialRouteRule {

    private final Repository2 repository;
    private final AuthenticationManager authenticationManager;
    private final RouteParams routeParams;


    private ProjectEntity currentProject;

    @Inject
    public ConsumerPendingSystemChatMessageRule(Repository2 repository, AuthenticationManager authenticationManager, RouteParams routeParams) {
        this.repository = repository;
        this.authenticationManager = authenticationManager;
        this.routeParams = routeParams;
    }


    @Override
    protected boolean shouldBeAppliedCore(Route route) {

        PendingSystemChatMessage pendingSystemChatMessage = repository.getSingle(PendingSystemChatMessage.class);
        if(pendingSystemChatMessage == null)
            return false;

        ProjectEntity pendingProject = pendingSystemChatMessage.getProjectEntity();
        if(pendingProject == null)
            return false;

        pendingSystemChatMessage.setProjectEntity(null);
        repository.update(pendingSystemChatMessage);

        currentProject = pendingProject;

       return true;

    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {
        routeParams.reset();
        routeParams.setParam("project", currentProject);
        routeActivationService.activate(Routes.ConsumerProjectDetailsRoute, navigationContext);
    }
}