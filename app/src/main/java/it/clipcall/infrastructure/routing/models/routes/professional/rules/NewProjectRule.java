package it.clipcall.infrastructure.routing.models.routes.professional.rules;

import android.os.Bundle;
import android.os.Parcelable;

import org.parceler.Parcels;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.repositories.Repository2;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.InitialRouteRule;
import it.clipcall.infrastructure.routing.services.RouteActivationService;
import it.clipcall.professional.leads.models.NewProject;
import it.clipcall.professional.leads.models.NewProjectCollection;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by micro on 1/26/2016.
 */
@Singleton
public class NewProjectRule extends InitialRouteRule {

    private final Repository2 repository;
    private final AuthenticationManager authenticationManager;


    private NewProject currentNewProject;

    @Inject
    public NewProjectRule(Repository2 repository, AuthenticationManager authenticationManager) {
        this.repository = repository;
        this.authenticationManager = authenticationManager;
    }


    @Override
    protected boolean shouldBeAppliedCore(Route route) {

        if(!authenticationManager.isProfessionalRegistered())
            return false;

        NewProjectCollection collection = repository.getSingle(NewProjectCollection.class);
        if(collection == null)
            return false;

        NewProject removedProject = collection.removeFirst();
        if(removedProject == null)
            return false;

        collection.clearAll();
        repository.update(collection);

        currentNewProject = removedProject;

       return true;

    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {

        Parcelable context =  Parcels.wrap(currentNewProject);
        Bundle bundle =  new Bundle();
        bundle.putParcelable("context", context);
        navigationContext.bundle = bundle;
        currentNewProject = null;
        routeActivationService.activate(Routes.ProfessionalNewProjectRoute, navigationContext);
    }
}