package it.clipcall.infrastructure.routing.models.routes.professional.rules;

import android.os.Bundle;
import android.os.Parcelable;

import org.parceler.Parcels;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.repositories.Repository2;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.InitialRouteRule;
import it.clipcall.infrastructure.routing.services.RouteActivationService;
import it.clipcall.professional.payment.models.NewPaymentCollection;
import it.clipcall.professional.payment.models.PaymentReceivedMessage;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by micro on 1/26/2016.
 */
@Singleton
public class NewPaymentRule extends InitialRouteRule {

    private final Repository2 repository;
    private final AuthenticationManager authenticationManager;


    private PaymentReceivedMessage currentNewPayment;

    @Inject
    public NewPaymentRule(Repository2 repository, AuthenticationManager authenticationManager) {
        this.repository = repository;
        this.authenticationManager = authenticationManager;
    }


    @Override
    protected boolean shouldBeAppliedCore(Route route) {

        if(!authenticationManager.isProfessionalRegistered())
            return false;

        NewPaymentCollection collection = repository.getSingle(NewPaymentCollection.class);
        if(collection == null)
            return false;

        PaymentReceivedMessage removedProject = collection.removeFirst();
        if(removedProject == null)
            return false;

        repository.update(collection);
        currentNewPayment = removedProject;

       return true;

    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {
        Parcelable context =  Parcels.wrap(currentNewPayment);
        Bundle bundle =  new Bundle();
        bundle.putParcelable("context", context);
        navigationContext.bundle = bundle;
        routeActivationService.activate(Routes.ProfessionalNewProjectRoute, navigationContext);
    }
}