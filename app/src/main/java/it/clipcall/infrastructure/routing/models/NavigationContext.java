package it.clipcall.infrastructure.routing.models;

import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dorona on 28/12/2015.
 */
public class NavigationContext {

    private  final Map<String,String> parameters = new HashMap<>();
    public Bundle bundle;

    public Bundle uiBundle;

    public NavigationContext(){

    }

    public NavigationContext(RoutingContext routingContext){
        this.routingContext = routingContext;
    }


    public void addParameter(String parameterName, String parameterValue){
        this.parameters.put(parameterName,parameterValue);
    }

    public RoutingContext routingContext;

    public String destinationRouteName;

    public boolean navigateBack = false;

    public boolean isRoot;

    public Map<String,String> getParameters(){return parameters;}

    public String getParameter(String parameterName) {
        if(parameters.containsKey(parameterName))
            return parameters.get(parameterName);

        return null;
    }
}
