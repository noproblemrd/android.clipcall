package it.clipcall.infrastructure.routing.models.routes;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;

/**
 * Created by dorona on 17/01/2016.
 */
public class ExternalUrlRoute extends Route {

    private final String url;

    public ExternalUrlRoute(String name, String url) {
        super(name);
        this.url = url;
    }

    @Override
    public void activate(NavigationContext navigationContext) {
        if(!(navigationContext.routingContext instanceof RoutingContext))
            return;

        RoutingContext routingContext = (RoutingContext)navigationContext.routingContext;
        final Activity context = routingContext.getContext();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(browserIntent);
    }
}
