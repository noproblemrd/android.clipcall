package it.clipcall.infrastructure.routing.models.routes.consumer.rules;

import android.support.v4.app.FragmentManager;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.R;
import it.clipcall.infrastructure.routing.models.RoutingRulesContext;
import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.routing.models.FragmentRoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.services.RouteActivationService;

/**
 * Created by dorona on 03/01/2016.
 */
@Singleton
public class PostPhoneValidationRule  implements IRouteRule {

    private final AuthenticationManager authenticationManager;
    private final RoutingRulesContext routingRulesContext;

    @Inject
    public PostPhoneValidationRule(AuthenticationManager authenticationManager, RoutingRulesContext routingRulesContext) {
        this.authenticationManager = authenticationManager;
        this.routingRulesContext = routingRulesContext;
    }

    @Override
    public boolean shouldBeApplied(Route route, RouteActivationService routeActivator) {

        if(route != null)
            return false;

        return routingRulesContext.isPostPhoneValidation;

   /*
        return routingRulesContext.isPostPhoneValidation;

        return routeActivator.getPendingRoute() != null;*/

    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {
        routingRulesContext.isPostPhoneValidation = false;
        Route pendingRoute = routeActivationService.getPendingRoute();
        Route currentRoute = routeActivationService.getCurrentRoute();
        Route parentRoute = pendingRoute.getParent();
        if(parentRoute != null && !parentRoute.equals(currentRoute)){
            routeActivationService.setPendingRoute(pendingRoute);
            routeActivationService.activate(parentRoute, navigationContext);
        }else{
            BaseActivity baseActivity = (BaseActivity)navigationContext.routingContext.getContext();
            FragmentManager fragmentManager = baseActivity.getSupportFragmentManager();
            NavigationContext newNavigationContext = new NavigationContext();
            newNavigationContext.routingContext = new FragmentRoutingContext(fragmentManager, R.id.menu_fragment_container, baseActivity);
            routeActivationService.setPendingRoute(null);
            routeActivationService.activate(pendingRoute, newNavigationContext);

        }

    }
}
