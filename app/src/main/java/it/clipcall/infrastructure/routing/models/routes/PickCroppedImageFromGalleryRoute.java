package it.clipcall.infrastructure.routing.models.routes;

import android.app.Activity;
import android.content.Intent;
import android.provider.MediaStore;

import it.clipcall.infrastructure.routing.models.FragmentDecoratorRoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;

public class PickCroppedImageFromGalleryRoute extends Route {
    public PickCroppedImageFromGalleryRoute(String name) {
        super(name);
    }

    @Override
    public void activate(NavigationContext navigationContext) {
        RoutingContext routingContext = navigationContext.routingContext;
        final Activity context = routingContext.getContext();
        Intent pickImageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        String url = navigationContext.getParameter("url");
        pickImageIntent.setType("image/*");
        pickImageIntent.putExtra("crop", "true");
        pickImageIntent.putExtra("outputX", 200);
        pickImageIntent.putExtra("outputY", 200);
        pickImageIntent.putExtra("aspectX", 1);
        pickImageIntent.putExtra("aspectY", 1);
        pickImageIntent.putExtra("scale", true);
        pickImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, url);


        String requestCode = navigationContext.getParameter("requestCode");
        int requestCodeValue = Integer.valueOf(requestCode);

        if(routingContext instanceof FragmentDecoratorRoutingContext){

            FragmentDecoratorRoutingContext fragmentRoutingContext = (FragmentDecoratorRoutingContext) routingContext;
            fragmentRoutingContext.getFragment().startActivityForResult(Intent.createChooser(pickImageIntent, "Select File"), requestCodeValue);
            return;
        }

        context.startActivityForResult(Intent.createChooser(pickImageIntent, "Select File"), requestCodeValue);

    }
}
