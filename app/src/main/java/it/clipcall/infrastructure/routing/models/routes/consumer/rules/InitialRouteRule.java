package it.clipcall.infrastructure.routing.models.routes.consumer.rules;

import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.services.RouteActivationService;

/**
 * Created by dorona on 03/01/2016.
 */
public abstract class InitialRouteRule implements IRouteRule {
    @Override
    public boolean shouldBeApplied(Route route, RouteActivationService routeActivator) {
        if(route != null)
            return false;

//        if(routeActivator.getPendingRoute() != null)
//            return false;

        return shouldBeAppliedCore(route);
    }

    protected abstract boolean shouldBeAppliedCore(Route route);


    public abstract void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route);

    @Override
    public String toString() {
        return this.getClass().getCanonicalName();
    }
}
