package it.clipcall.infrastructure.routing.services;

import javax.inject.Inject;

import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;

/**
 * Created by dorona on 03/01/2016.
 */
public class RouteActivationService {

    private Route currentRoute;
    private Route pendingRoute;


    @Inject
    public RouteActivationService() {
    }

    public Route getPendingRoute() {
        return pendingRoute;
    }

    public void setPendingRoute(Route pendingRoute) {
        this.pendingRoute = pendingRoute;
    }

    public Route getCurrentRoute() {
        return currentRoute;
    }

    public void activate(Route route, NavigationContext navigationContext){
        currentRoute = route;
        route.activate(navigationContext);
    }
}
