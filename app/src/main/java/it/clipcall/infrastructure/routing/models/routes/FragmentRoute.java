package it.clipcall.infrastructure.routing.models.routes;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import it.clipcall.R;
import it.clipcall.infrastructure.routing.models.FragmentRoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;

/**
 * Created by dorona on 03/01/2016.
 */
public class FragmentRoute extends Route {
    private Class<? extends Fragment> fragmentClass;
    public FragmentRoute(String name, Class<? extends Fragment> fragmentClass, Route parent) {
        super(name, parent);
        this.fragmentClass = fragmentClass;
    }

    @Override
    public void activate(NavigationContext navigationContext) {

        RoutingContext routingContext = navigationContext.routingContext;
        if(!(routingContext instanceof FragmentRoutingContext))
            return;

        FragmentRoutingContext fragmentRoutingContext = (FragmentRoutingContext)routingContext;
        FragmentManager fragmentManager = fragmentRoutingContext.getFragmentManager();
        int containerViewId = fragmentRoutingContext.getContainerViewId();

        Fragment fragment = null;
        try {

            Method builderMethod = fragmentClass.getDeclaredMethod("builder");
            if(builderMethod == null){
                fragment = fragmentClass.newInstance();
            }else{
                Object builder = builderMethod.invoke(null,null);
                Class<?> builderClass = builder.getClass();
                Method buildMethod = builderClass.getDeclaredMethod("build");
                fragment = (Fragment)buildMethod.invoke(builder,null);
            }


        } catch (InstantiationException e) {
            e.printStackTrace();

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        try{
            fragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.activity_fade_in, R.anim.activity_fade_out)
                    .replace(containerViewId, fragment).commit();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return fragmentClass.getCanonicalName();
    }
}
