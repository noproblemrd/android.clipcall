package it.clipcall.infrastructure.routing.models.routes;

import android.app.Activity;
import android.content.Intent;

import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;

public class SendInvitationRoute extends Route {
    public SendInvitationRoute(String name) {
        super(name);
    }

    @Override
    public void activate(NavigationContext navigationContext) {
        RoutingContext routingContext = (RoutingContext)navigationContext.routingContext;
        final Activity context = routingContext.getContext();
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String message = navigationContext.getParameter("message");
        intent.putExtra(Intent.EXTRA_TEXT,message);
        context.startActivity(intent);
    }
}
