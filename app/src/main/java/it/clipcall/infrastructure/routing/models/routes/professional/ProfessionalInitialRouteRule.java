package it.clipcall.infrastructure.routing.models.routes.professional;

import android.support.v4.app.FragmentManager;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.routing.models.FragmentRoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.InitialRouteRule;
import it.clipcall.infrastructure.routing.services.RouteActivationService;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by omega on 3/12/2016.
 */
public class ProfessionalInitialRouteRule extends InitialRouteRule {


    private final AuthenticationManager authenticationManager;

    @Inject
    public ProfessionalInitialRouteRule(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected boolean shouldBeAppliedCore(Route route) {
        if(route != null)
            return false;


        if(!authenticationManager.isInProfessionalMode())
            return false;

        return authenticationManager.isProfessionalRegistered();
    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {
        Route pendingRoute = routeActivationService.getPendingRoute();
        if(pendingRoute == null) {
            routeActivationService.setPendingRoute(Routes.ProfessionalProjectsRoute);
            routeActivationService.activate(Routes.AdvertiserMainMenuRoute, navigationContext);
            return;
        }


        BaseActivity baseActivity = (BaseActivity)navigationContext.routingContext.getContext();
        FragmentManager fragmentManager = baseActivity.getSupportFragmentManager();
        NavigationContext newNavigationContext = new NavigationContext();
        newNavigationContext.routingContext = new FragmentRoutingContext(fragmentManager, R.id.menu_fragment_container, baseActivity);
        routeActivationService.setPendingRoute(null);
        routeActivationService.activate(pendingRoute, newNavigationContext);

    }


}

