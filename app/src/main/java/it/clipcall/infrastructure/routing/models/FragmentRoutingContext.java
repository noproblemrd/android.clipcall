package it.clipcall.infrastructure.routing.models;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentManager;

/**
 * Created by dorona on 21/12/2015.
 */
public class FragmentRoutingContext extends  RoutingContext{

    private final FragmentManager fragmentManager;
    private final int containerViewId;

    public FragmentRoutingContext(FragmentManager fragmentManager, @IdRes int containerViewId, Activity context){

        super(context);
        this.fragmentManager = fragmentManager;
        this.containerViewId = containerViewId;
    }

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public int getContainerViewId() {
        return containerViewId;
    }

}
