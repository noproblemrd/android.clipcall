package it.clipcall.infrastructure.routing.models;

import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class RouteParams {

    private final Map<String,Object> params = new HashMap<>();
    private Bundle uiParams;

    @Inject
    public RouteParams(){

    }

    public <T> T getParam(String key){
        if(params.containsKey(key))
            return(T)params.get(key);

        return null;
    }

    public void reset(){
        this.params.clear();
    }

    public Bundle getUiParams() {
        return uiParams;
    }

    public void setUiParams(Bundle uiParams) {
        this.uiParams = uiParams;
    }

    public <T> void setParam(String key, T value) {
        this.params.put(key,value);
    }
}
