package it.clipcall.infrastructure.routing.models.routes;

import android.app.Activity;
import android.content.Intent;

import it.clipcall.infrastructure.routing.models.FragmentDecoratorRoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;

public class PickImageFromGalleryRoute extends Route {
    public PickImageFromGalleryRoute(String name) {
        super(name);
    }

    @Override
    public void activate(NavigationContext navigationContext) {
        RoutingContext routingContext = navigationContext.routingContext;
        final Activity context = routingContext.getContext();
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        String requestCode = navigationContext.getParameter("requestCode");
        int requestCodeValue = Integer.valueOf(requestCode);

        if(routingContext instanceof FragmentDecoratorRoutingContext){

            FragmentDecoratorRoutingContext fragmentRoutingContext = (FragmentDecoratorRoutingContext) routingContext;
            fragmentRoutingContext.getFragment().startActivityForResult(Intent.createChooser(intent, "Select File"), requestCodeValue);
            return;
        }

        context.startActivityForResult(Intent.createChooser(intent, "Select File"), requestCodeValue);

    }
}
