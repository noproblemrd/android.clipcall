package it.clipcall.infrastructure.routing.models.routes;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;

import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;

/**
 * Created by dorona on 19/01/2016.
 */
public class RateUsRoute extends Route {
    public RateUsRoute(String name) {
        super(name);
    }

    @Override
    public void activate(NavigationContext navigationContext) {
        RoutingContext routingContext = (RoutingContext)navigationContext.routingContext;
        final Activity context = routingContext.getContext();

        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }
}
