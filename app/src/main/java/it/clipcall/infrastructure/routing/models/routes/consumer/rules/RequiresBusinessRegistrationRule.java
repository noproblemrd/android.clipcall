package it.clipcall.infrastructure.routing.models.routes.consumer.rules;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RouteActivationService;

@Singleton
public class RequiresBusinessRegistrationRule implements IRouteRule {

    private final AuthenticationManager authenticationManager;

    private final List<Route> routesRequiringValidation = new ArrayList<>();

    @Inject
    public RequiresBusinessRegistrationRule(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        this.routesRequiringValidation.add(Routes.ProfessionalProjectsRoute);
    }

    @Override
    public boolean shouldBeApplied(Route route, RouteActivationService routeActivator) {
        if(route == null)
            return false;

       if(authenticationManager.isProfessionalRegistered())
           return false;

        for(Route routeRequiringValidation : routesRequiringValidation)
        {
            if(routeRequiringValidation.equals(route))
                return  true;
        }

        return false;
    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {

        if(authenticationManager.isProfessionalRegistered()){
            routeActivationService.activate(route, navigationContext);
            return;
        }

        if(authenticationManager.isCustomerAuthenticated()){
            routeActivationService.activate(Routes.ProfessionalRegistrationRoute, navigationContext);
            return;
        }

        routeActivationService.setPendingRoute(Routes.ProfessionalRegistrationRoute);
        routeActivationService.activate(Routes.ConsumerValidationRoute, navigationContext);
    }
}