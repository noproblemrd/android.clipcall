package it.clipcall.infrastructure.routing.models;

/**
 * Created by dorona on 21/12/2015.
 */
public class RouteNames {

    public static final String SplashScreenRoute = "SPLASH_SCREEN_ROUTE";
    public static final String ConsumerStartVideoChat2Route = "CONSUMER_START_VIDEO_CALL_2_ROUTE";
    public static final String ConsumerStartVideoChatRoute = "CONSUMER_START_VIDEO_CALL_ROUTE";
    public static final String ConsumerChatRoute = "CONSUMER_CHAT_ROUTE";
    public static final String AdvertiserRoute = "ADVERTISER_ROUTE";
    public static final String ConsumerVideoChatCalling = "CONSUMER_VIDEO_CHAT_CALLING";
    public static final String ConsumerValidationRoute = "CONSUMER_VALIDATION";
    public static final String ConsumerValidation2Route = "CONSUMER_VALIDATION_2";

    public static final String ConsumerProjectDetailsRoute = "CONSUMER_PROJECT_DETAILS_ROUTE";

    public static final String ConsumerMainMenuActivity = "CONSUMER_MAIN_MENU_ROUTE";
    public static final String ConsumerAcceptVideoCallRoute = "CONSUMER_ACCEPT_VIDEO_CHAT_CALL";
    public static final String ConsumerStartVideoProjectRoute = "CONSUMER_START_VIDEO_PROJECT_ROUTE";
    public static final String switchToAdvertiserRoute = "SWITCH_TO_ADVERTISER_ROUTE";
    public static final String ConsumerEulaRoute = "CONSUMER_EULA_ROUTE";
    public static final String ConsumerPrivacyPolicyRoute = "CONSUMER_PRIVACY_POLICY_ROUTE";
    public static final String ConsumerFaqRoute = "CONSUMER_FAQ_ROUTE";
    public static final String ConsumerAdvertiserRoute = "CONSUMER_ADVERTISER_ROUTE";
    public static final String ConsumerTutorialRoute = "CONSUMER_TUTORIAL_ROUTE";
    public static final String ConsumerRecordVideoRoute = "CONSUMER_RECORD_VIDEO_ROUTE";
    public static final String ConsumerSubmitProjectToAdvertisersRoute = "CONSUMER_SUBMIT_PROJECT_TO_ADVERTISERS_ROUTE";
    public static final String AdvertiserMainMenuRoute = "ADVERTISER_MAIN_MENU_ROUTE";
    public static final String ProfessionalRegistrationRoute = "PROFESSIONAL_REGISTRATION_ROUTE";
    public static final String PayTheProRoute = "PAY_THE_PRO_ROUTE";
    public static final String ConsumerAddCreditCardRoute = "CONSUMER_ADD_CREDIT_CARD_ROUTE";
    public static final String ConsumerAndroidPayRoute = "CONSUMER_ANDROID_PAY_ROUTE";
    public static final String ConsumerPaymentInvoiceRoute = "CONSUMER_PAYMENT_INVOICE_ROUTE";
    public static final String ProfessionalServiceCallInvitationBySmsRoute = "PROFESSIONAL_SERVICE_CALL_INVITATION_BY_SMS_ROUTE";
    public static final String ProfessionalShareVideoRoute = "PROFESSIONAL_SHARE_VIDEO_ROUTE";
    public static final String ProfessionalProjectDetailsRoute = "PROFESSIONAL_PROJECT_DETAILS_ROUTE";
    public static final String ProfessionalEditProfileRoute = "PROFESSIONAL_EDIT_PROFILE_ROUTE";
    public static final String ProfessionalChatRoute = "PROFESSIONAL_CHAT_ROUTE";



    public static final String ProfessionalAcceptVideoChatCall = "PROFESSIONAL_ACCEPT_VIDEO_CHAT_CALL";

    public static final String ProfessionalVideoChatRoute = "PROFESSIONAL_VIDEO_CHAT_CALL";

    //common
    public static final String PickImageFromGalleryRoute = "PICK_IMAGE_FROM_GALLERY_ROUTE";
    public static final String PickCroppedImageFromGalleryRoute = "PICK_CROPPED_IMAGE_FROM_GALLERY_ROUTE";
    public static final String PickImageFromCameraRoute = "PICK_IMAGE_FROM_CAMERA_ROUTE";
    public static final String StartVideoChat2Route = "START_VIDEO_CALL_2_ROUTE";

    public static final String ProfessionalServiceCallInvitationRout = "PROFESSIONAL_REMOTE_SERVICE_CALL_INVITATION";
    public static final String ProfessionalNewProjectRoute = "PROFESSIONAL_NEW_PROJECT_ROUTE";
    public static final String ProfessionalSelectServiceCategoriesRoute = "PROFESSIONAL_SELECT_SERVICE_CATEGORIES";

    public static final String ProjectVideoRoute = "PROJECT_VIDEO_ROUTE";
}
