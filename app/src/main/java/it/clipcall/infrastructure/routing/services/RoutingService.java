package it.clipcall.infrastructure.routing.services;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.IRouteRule;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class RoutingService {
    private final ILogger logger = LoggerFactory.getLogger(RoutingService.class.getSimpleName());

    private final List<Route> routes = new ArrayList<>();
    private final List<IRouteRule> routeRules = new ArrayList<>();
    private RouteActivationService routeActivationService;
    private final AuthenticationManager authenticationManager;


    @Inject
    public RoutingService(RouteActivationService routeActivationService, AuthenticationManager authenticationManager){
        this.routeActivationService = routeActivationService;
        this.authenticationManager = authenticationManager;
    }

    public void addRoute(Route route){
        routes.add(route);
    }

    public void addRoutingRule(IRouteRule rule){
        routeRules.add(rule);
    }

    Route getRouteByName(String routeName){
        for(Route route: routes){
            if(route.name.equals(routeName))
                return route;
        }
        return null;
    }

    @RunOnUiThread
    public void routeTo(Route route, NavigationContext navigationContext){
        for(IRouteRule rule: routeRules){
            if(rule.shouldBeApplied(route, routeActivationService))
            {
                String routeName = route == null ? "empty" : route.name;
                logger.debug("activating route rule %s with route %s ",rule.getClass().getSimpleName(), routeName );
                rule.apply(routeActivationService, navigationContext, route);
                return;
            }
        }
    }

    public void routeToHome(RoutingContext routingContext){
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = routingContext;
        navigationContext.isRoot = true;
        boolean routeToProfessionalHome = authenticationManager.isInProfessionalMode() && authenticationManager.isProfessionalRegistered();
        if(routeToProfessionalHome){
            routeActivationService.setPendingRoute(Routes.ProfessionalProjectsRoute);
            routeTo(Routes.AdvertiserMainMenuRoute, navigationContext);
            return;
        }


        routeActivationService.setPendingRoute(Routes.ConsumerProjectsRoute);
        routeTo(Routes.ConsumerMainMenuActivityRoute, navigationContext);
    }

    public void routeTo(String routeName, NavigationContext navigationContext){
        Route route = this.getRouteByName(routeName);
        this.routeTo(route, navigationContext);
    }

    public boolean hasPendingSubRoute() {
        Route pendingRoute = routeActivationService.getPendingRoute();
        if(pendingRoute == null)
            return false;

        Route parent = pendingRoute.getParent();
        if(parent == null)
            return false;

        Route currentRoute = routeActivationService.getCurrentRoute();
        if(currentRoute == null)
            return false;


        if(currentRoute.equals(Routes.ConsumerValidationRoute))
            return false;

        if(currentRoute.equals(Routes.ConsumerValidation2Route))
            return false;

        return true;
    }

    public void clearPendingRoute() {
        routeActivationService.setPendingRoute(null);
    }
}