package it.clipcall.infrastructure.routing.models.routes.consumer.rules;

import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.services.RouteActivationService;

/**
 * Created by dorona on 03/01/2016.
 */
public interface IRouteRule {

    boolean shouldBeApplied(Route route, RouteActivationService routeActivator);
    void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route);
}
