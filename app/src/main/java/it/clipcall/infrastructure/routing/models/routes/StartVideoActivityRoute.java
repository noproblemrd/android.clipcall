package it.clipcall.infrastructure.routing.models.routes;

import android.app.Activity;
import android.content.Intent;

import it.clipcall.R;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;

/**
 * Created by dorona on 04/01/2016.
 */
public class StartVideoActivityRoute extends ActivityRoute {
    public StartVideoActivityRoute(String name, Class<? extends BaseActivity> activityClass) {
        super(name, activityClass);
    }

    @Override
    public void activate(final NavigationContext navigationContext) {
        if(!(navigationContext.routingContext instanceof RoutingContext))
            return;

        RoutingContext routingContext = (RoutingContext)navigationContext.routingContext;
        final Activity context = routingContext.getContext();
        Intent intent = createIntent(navigationContext);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.activity_slide_up,R.anim.activity_no_change);
        if(navigationContext.isRoot){
            context.finish();
        }
    }
}
