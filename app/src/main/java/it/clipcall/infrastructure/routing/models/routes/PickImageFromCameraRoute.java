package it.clipcall.infrastructure.routing.models.routes;

import android.app.Activity;
import android.content.Intent;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import it.clipcall.infrastructure.routing.models.FragmentDecoratorRoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;

public class PickImageFromCameraRoute extends Route {
    public PickImageFromCameraRoute(String name) {
        super(name);
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        return image;
    }

    @Override
    public void activate(NavigationContext navigationContext) {
        RoutingContext routingContext = (RoutingContext)navigationContext.routingContext;
        final Activity context = routingContext.getContext();
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) == null)
            return;


        String requestCode = navigationContext.getParameter("requestCode");
        int finalRequestCode = Integer.valueOf(requestCode);

        if(routingContext instanceof FragmentDecoratorRoutingContext){
            ((FragmentDecoratorRoutingContext)routingContext).getFragment().startActivityForResult(takePictureIntent, finalRequestCode);
            return;
        }

        try {
            File photoFile = createImageFile();
            if (photoFile != null) {
               // takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(photoFile));
            }

            context.startActivityForResult(takePictureIntent, finalRequestCode);


        } catch (IOException ex) {

            context.startActivityForResult(takePictureIntent, finalRequestCode);
        }
        // Continue only if the File was successfully created






    }
}
