package it.clipcall.infrastructure.routing.models.routes.consumer.rules;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.services.RouteActivationService;

/**
 * Created by micro on 1/26/2016.
 */
@Singleton
public class SubRouteActivationRule implements IRouteRule {

    private final AuthenticationManager authenticationManager;

    @Inject
    public SubRouteActivationRule(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public boolean shouldBeApplied(Route route, RouteActivationService routeActivator) {

        Route parentRoute = route.getParent();
        if(parentRoute == null)
           return false;

        Route currentRoute = routeActivator.getCurrentRoute();
        if(parentRoute.equals(currentRoute))
            return false;

        return true;

    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {

        Route parent = route.getParent();
        routeActivationService.setPendingRoute(route);
        routeActivationService.activate(parent, navigationContext);
    }
}