package it.clipcall.infrastructure.routing.models.routes.consumer.rules;

import android.support.v4.app.FragmentManager;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.R;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.routing.models.FragmentRoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RouteActivationService;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class ConsumerInitialRouteRule extends InitialRouteRule {


    private final AuthenticationManager authenticationManager;

    @Inject
    public ConsumerInitialRouteRule(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected boolean shouldBeAppliedCore(Route route) {
        if(route != null)
            return false;


        if(!authenticationManager.isInProfessionalMode())
            return true;



        if(!authenticationManager.isProfessionalRegistered())
            return true;

 /*       if(authenticationManager.isInProfessionalMode())
            return false;*/

        return false;
    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {
        Route pendingRoute = routeActivationService.getPendingRoute();
        if(pendingRoute == null) {
            routeActivationService.setPendingRoute(Routes.ConsumerFindProRoute);
            routeActivationService.activate(Routes.ConsumerMainMenuActivityRoute, navigationContext);
            return;
        }


        BaseActivity baseActivity = (BaseActivity)navigationContext.routingContext.getContext();
        FragmentManager fragmentManager = baseActivity.getSupportFragmentManager();
        NavigationContext newNavigationContext = new NavigationContext();
        newNavigationContext.routingContext = new FragmentRoutingContext(fragmentManager, R.id.menu_fragment_container, baseActivity);
        routeActivationService.setPendingRoute(null);
        routeActivationService.activate(pendingRoute, newNavigationContext);

    }


}
