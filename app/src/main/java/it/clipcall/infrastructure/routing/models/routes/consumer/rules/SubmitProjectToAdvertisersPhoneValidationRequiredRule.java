package it.clipcall.infrastructure.routing.models.routes.consumer.rules;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RouteActivationService;

/**
 * Created by micro on 1/26/2016.
 */
@Singleton
public class SubmitProjectToAdvertisersPhoneValidationRequiredRule implements IRouteRule {

    private final AuthenticationManager authenticationManager;



    @Inject
    public SubmitProjectToAdvertisersPhoneValidationRequiredRule(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public boolean shouldBeApplied(Route route, RouteActivationService routeActivator) {
        if (route == null)
            return false;

        if (!route.equals(Routes.ConsumerSubmitProjectToAdvertiserRoute))
            return false;

        if(authenticationManager.isCustomerAuthenticated())
            return false;

        return true;

    }

    @Override
    public void apply(RouteActivationService routeActivationService, NavigationContext navigationContext, Route route) {
        routeActivationService.setPendingRoute(Routes.ConsumerSubmitProjectToAdvertiserRoute);
        routeActivationService.activate(Routes.ConsumerValidationRoute, navigationContext);
    }
}