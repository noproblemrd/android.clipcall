package it.clipcall.infrastructure.maps.entities;

/**
 * Created by micro on 1/27/2016.
 */
public class AddressRef {

    private String zipCode;

    public AddressRef(String zipCode, String formattedAddress) {
        this.zipCode = zipCode;
        this.formattedAddress = formattedAddress;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    private String formattedAddress;
}
