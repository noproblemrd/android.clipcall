package it.clipcall.infrastructure.maps.services;


import it.clipcall.infrastructure.maps.entities.GeocodingResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by micro on 1/27/2016.
 */
public interface GoogleMapsApiService {



    @GET("geocode/json")
    Call<GeocodingResponse> getByPostalCode(@Query("components")String components, @Query("key")String key);

}
