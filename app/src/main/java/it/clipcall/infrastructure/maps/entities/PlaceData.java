package it.clipcall.infrastructure.maps.entities;

import org.parceler.Parcel;

@Parcel
public class PlaceData {

    public String address;
    public LatitudeLongitude latLng;
}
