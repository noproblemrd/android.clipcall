package it.clipcall.infrastructure.maps.services;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.findprofessional.models.AddressEntity;
import it.clipcall.infrastructure.maps.entities.AddressComponent;
import it.clipcall.infrastructure.maps.entities.AddressRef;
import it.clipcall.infrastructure.maps.entities.GeocodingResponse;
import it.clipcall.infrastructure.maps.entities.Result;
import retrofit2.Response;

/**
 * Created by micro on 1/27/2016.
 */
public class AddressService {

    private GoogleMapsApiService googleMapsApiService;

    @Inject
    @Singleton
    public AddressService(GoogleMapsApiService googleMapsApiService) {
        this.googleMapsApiService = googleMapsApiService;
    }


    public List<AddressRef> findAddressByPostalCode(String postalCode){
        try {
            List<AddressRef> addresses = new ArrayList<>();
            String components = "country:US|postal_code:" + postalCode;
            String key = "AIzaSyDdFXI1oUV6ik19hEFFmcRRi87F2oEZvPo";
            Response<GeocodingResponse> response = this.googleMapsApiService.getByPostalCode(components, key).execute();
            if(response == null)
                return addresses;


            GeocodingResponse geocoding = response.body();
            if(geocoding == null)
                return addresses;

            String status = geocoding.getStatus();
            if(Strings.isNullOrEmpty(status))
                return addresses;

            if(!"OK".equals(status.toUpperCase()))
                return addresses;

            List<Result> results = geocoding.getResults();
            if(results == null || results.size() == 0)
                return addresses;


            Result result = results.get(0);
            String formattedAddress = result.getFormattedAddress();
            List<AddressComponent> addressComponents = result.getAddressComponents();
            String zipCode = null;
            for(AddressComponent component: addressComponents){
                if(component.getTypes().contains("postal_code")){
                    zipCode = component.getLongName();
                }
            }

            if(Strings.isNullOrEmpty(zipCode))
                return null;

            AddressRef address = new AddressRef(zipCode,formattedAddress);
            addresses.add(address);
            return addresses;
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}
