package it.clipcall.infrastructure.maps.entities;

import org.parceler.Parcel;

@Parcel
public class LatitudeLongitude {

    public double latitude;
    public double longitude;

    public LatitudeLongitude(){

    }

    public LatitudeLongitude(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
