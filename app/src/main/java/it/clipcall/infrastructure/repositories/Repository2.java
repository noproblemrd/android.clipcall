package it.clipcall.infrastructure.repositories;

import android.content.Context;
import android.util.Log;

import com.github.pwittchen.prefser.library.Prefser;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class Repository2 {

    private static final String TAG = "Repository";
    private Prefser prefser;

    @Inject
    public Repository2(Context context){
        try {
            prefser = new Prefser(context);
        } catch (Exception e) {
            prefser  = null;
            Log.e(TAG, "Repository: initialization failed with error" + e.getMessage(), e);
        }
    }

    public <T extends Serializable> T getSingle(Class<T> clazz){
        try {
            String key = getKey(clazz);
            Log.d(TAG, "getSingle: getting entity with key: "+ key);
            T existingEntity = prefser.get(key, clazz, null);
            Log.d(TAG, "getSingle: retrieved successfully");
            return existingEntity;
        } catch (Exception e) {
            Log.e(TAG, "getSingle: failed getting entity with error: " +e.getMessage(), e);
            return null;
        }
    }

    public <T extends Serializable> void  add(T entity){
        try {
            String key = getKey(entity.getClass());
            Log.d(TAG, "add: adding entity with key: "+ key);
            prefser.put(key,entity);
            Log.d(TAG, "add: entity with key: "+ key + " added successfully");
        } catch (Exception e) {
            Log.e(TAG, "add: failed adding entity with error: " +e.getMessage(), e);
        }
    }

    public <T extends Serializable> void  delete(T entity){

        try {
            String key = getKey(entity.getClass());
            Log.d(TAG, "delete: deleting entity with key: "+ key);
            prefser.remove(key);
            Log.d(TAG, "delete: entity with key: "+ key+ " deleted successfully");
        } catch (Exception e) {
            Log.e(TAG, "add: failed deleting entity with error: " +e.getMessage(), e);
        }
    }

    public <T extends Serializable> void update(T entity){
        String key = getKey(entity.getClass());
        try {
            Log.d(TAG, "Updating entity with key: "+ key);
            prefser.put(key,entity);
        } catch (Exception e) {
            Log.e("Error", "Failed updating entity : "+ entity.getClass().getCanonicalName() + " with error: "+ e.getMessage(), e);
        }
    }

    private <T> String getKey(Class<T> clazz){
        return clazz.getName();
    }
}
