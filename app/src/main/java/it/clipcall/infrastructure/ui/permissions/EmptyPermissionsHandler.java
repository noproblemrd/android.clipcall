package it.clipcall.infrastructure.ui.permissions;

/**
 * Created by omega on 3/17/2016.
 */
public class EmptyPermissionsHandler implements IPermissionsHandler {
    @Override
    public void handleAllPermissionsGranted() {

    }

    @Override
    public void handlePermissionDenied() {

    }
}
