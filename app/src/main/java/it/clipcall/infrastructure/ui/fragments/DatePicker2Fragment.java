package it.clipcall.infrastructure.ui.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.widget.DatePicker;

import org.joda.time.LocalDate;

import java.util.Calendar;

/**
 * Created by dorona on 23/06/2016.
 */
public  class DatePicker2Fragment extends DialogFragment implements DatePickerDialog.OnDateSetListener
{


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR)-18;
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog dialog =  new DatePickerDialog(getActivity(), this, year, month, day);
        dialog.getDatePicker().setMaxDate(LocalDate.now().minusYears(18).toDate().getTime());
        return dialog;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {

        Fragment parentFragment = getParentFragment();

        if(getActivity() instanceof DatePickerDialog.OnDateSetListener){
            ((DatePickerDialog.OnDateSetListener)getActivity()).onDateSet(view, year, month, day);
        }else if(parentFragment instanceof DatePickerDialog.OnDateSetListener){
            ((DatePickerDialog.OnDateSetListener)parentFragment).onDateSet(view, year, month, day);
        }
    }
}