package it.clipcall.infrastructure.ui.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.lang.reflect.Field;
import java.util.Calendar;

/**
 * Created by dorona on 23/06/2016.
 */
public  class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener
{


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        Calendar calendar = Calendar.getInstance();
        int year    = calendar.get(Calendar.YEAR);
        int month   = calendar.get(Calendar.MONTH);
        int day     = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(getContext(), this, year, month, day);
        Field mDatePickerField;
        try {
            mDatePickerField = dialog.getClass().getDeclaredField("mDatePicker");
            mDatePickerField.setAccessible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        return dialog;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        if(getActivity() instanceof DatePickerDialog.OnDateSetListener){
            ((DatePickerDialog.OnDateSetListener)getActivity()).onDateSet(view, year, month, day);
        }
    }
}