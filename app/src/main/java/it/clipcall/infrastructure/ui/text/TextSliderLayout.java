package it.clipcall.infrastructure.ui.text;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.google.android.gms.vision.Frame;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import it.clipcall.R;

@EViewGroup(R.layout.text_slider_layout)
public class TextSliderLayout extends FrameLayout {


    private int currentIndex = -1;
    private List<String> textItems;
    private SliderListener sliderListener;


    public void setSliderListener(SliderListener sliderListener){

        this.sliderListener = sliderListener;
    }

    public void setTextItems(List<String> textItems){
        this.textItems = textItems;
    }

    private Timer timer;

    @ViewById
    TextSwitcher textSwitcher;

    public TextSliderLayout(Context context) {
        this(context, null);
    }

    public TextSliderLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TextSliderLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @AfterViews
    void afterViews(){
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView switcherTextView = new TextView(getContext());
                Typeface custom_font = Typeface.createFromAsset(getContext().getAssets(), "fonts/ProximaNova-Regular.otf");
                switcherTextView.setTextSize(14);
                //int color = ContextCompat.getColor(getContext(), R.color.tooltip_slider_background_color);
                switcherTextView.setBackgroundResource(R.drawable.text_slider_background);
                //switcherTextView.setBackgroundColor(color);
                switcherTextView.setTypeface(custom_font);
                switcherTextView.setTextColor(Color.WHITE);
                switcherTextView.setPadding(40,40,40,40);
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.CENTER;
                switcherTextView.setLayoutParams(layoutParams);
                switcherTextView.setShadowLayer(6, 6, 6, Color.BLACK);
                return switcherTextView;
            }
        });

        Animation animationOut = AnimationUtils.loadAnimation(getContext(), R.anim.tooltip_exit);
        Animation animationIn = AnimationUtils.loadAnimation(getContext(), R.anim.tooltip_enter);

        textSwitcher.setOutAnimation(animationOut);
        textSwitcher.setInAnimation(animationIn);
    }


    public void start(){

        if(textItems == null || textItems.size() == 0)
            return;

        if(timer != null)
            return;


        timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (currentIndex >= textItems.size()) {
                    timer.cancel();
                    timer = null;
                    if(sliderListener != null){
                        sliderListener.onFinished();
                    }
                    return;
                }

                updateCurrentText();


            }
        }, 10, 10000);
    }


    @UiThread
    void updateCurrentText(){
        ++currentIndex;

        if(currentIndex >= textItems.size())
            return;

        textSwitcher.setText(textItems.get(currentIndex));
    }


    public interface  SliderListener{
        void onFinished();
    }

}
