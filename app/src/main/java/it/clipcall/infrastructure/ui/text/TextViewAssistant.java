package it.clipcall.infrastructure.ui.text;

import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.widget.TextView;

/**
 * Created by dorona on 21/01/2016.
 */
public class TextViewAssistant {
    private static final String TAG = TextViewAssistant.class.getSimpleName();

    private static class URLSpanNoUnderline extends URLSpan {
        public URLSpanNoUnderline(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    // http://stackoverflow.com/questions/4096851/remove-underline-from-links-in-textview-android
    public static void stripUnderlines(TextView textView) {
        Spannable s = (Spannable)textView.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            URLSpanNoUnderline noUnderlineSpan = new URLSpanNoUnderline(span.getURL());
            s.setSpan(noUnderlineSpan, start, end, 0);
        }
        textView.setText(s);
    }
}
