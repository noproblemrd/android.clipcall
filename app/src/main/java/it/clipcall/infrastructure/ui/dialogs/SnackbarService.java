package it.clipcall.infrastructure.ui.dialogs;


import android.app.Activity;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import it.clipcall.infrastructure.di.scopes.PerActivity;

@PerActivity
public class SnackbarService {
    private final Handler handler;
    private final String defaultActionText = "OK";
    private final int defaultSnackbarDuration  = Snackbar.LENGTH_INDEFINITE;

    @Inject
    public SnackbarService(Activity activity){
        handler = new Handler(activity.getMainLooper());
    }

    public void show(final SnackbarContext snackbarContext){

        handler.post(new Runnable() {
            @Override
            public void run() {
                final Snackbar snackbar = Snackbar.make(snackbarContext.getView(), snackbarContext.getMessage(), Snackbar.LENGTH_LONG);
                snackbar.setDuration(5000);
                if(snackbarContext.getOnClickListener() != null){
                    snackbar.setAction(defaultActionText, snackbarContext.getOnClickListener());
                }

                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                //increase max lines of text in snackbar. default is 2.
                textView.setMaxLines(snackbarContext.getNumberOfRows());
                snackbar.show();
            }
        });
    }

    public void show(final View view, final String message, final int duration){
        this.show(view, message, duration, null);
    }


    public void show(final View view, final String message, final int duration, final View.OnClickListener listener){
        handler.post(new Runnable() {
            @Override
            public void run() {
                final Snackbar snackbar = Snackbar.make(view, message, duration);
                if(listener != null){
                    snackbar.setAction(defaultActionText, listener);
                }

                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                //increase max lines of text in snackbar. default is 2.
                textView.setMaxLines(3);
                snackbar.show();
            }
        });
    }
}
