package it.clipcall.infrastructure.ui.permissions;

/**
 * Created by omega on 3/6/2016.
 */
public interface IPermissionsHandler {
    void handleAllPermissionsGranted();
    void handlePermissionDenied();
}
