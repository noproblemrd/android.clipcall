package it.clipcall.infrastructure.ui.permissions;


import android.app.Activity;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.DialogOnAnyDeniedMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;

@PerActivity
public class PermissionsService {

    private final ILogger logger = LoggerFactory.getLogger(PermissionsService.class.getSimpleName());

    private final WeakReference<Activity> activity;
    @Inject
    public PermissionsService(Activity activity){
        this.activity = new WeakReference<Activity>(activity);
    }

    public void requestPermissions(PermissionContext permissionContext, IPermissionsHandler permissionsHandler){
        requestPermissions(permissionsHandler, permissionContext.getTitle(), permissionContext.getMessage(), permissionContext.getPermissions());
    }

    public void requestPermissions(IPermissionsHandler permissionsHandler, String title, String message,String...permissions){

        Activity activity = this.activity.get();
        if(activity == null)
            return;

        MultiplePermissionsListener dialogMultiplePermissionsListener =
                DialogOnAnyDeniedMultiplePermissionsListener.Builder
                        .withContext(activity)
                        .withTitle(title)
                        .withMessage(message)
                        .withButtonText(android.R.string.ok)
                        .withIcon(R.drawable.clipcall_logo)
                        .build();


        DefaultPermissionListener defaultPermissionListener = new DefaultPermissionListener(permissionsHandler);
        CompositeMultiplePermissionsListener compositeMultiplePermissionsListener = new CompositeMultiplePermissionsListener( dialogMultiplePermissionsListener, defaultPermissionListener);

        try{
            Dexter.checkPermissions(compositeMultiplePermissionsListener, permissions);
        }
        catch (Exception e){
            logger.error("Failed requesting persmissions with error "+ e.getMessage(), e);
            permissionsHandler.handlePermissionDenied();
        }

    }
}
