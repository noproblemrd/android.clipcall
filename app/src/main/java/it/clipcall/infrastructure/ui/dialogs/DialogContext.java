package it.clipcall.infrastructure.ui.dialogs;

/**
 * Created by dorona on 26/05/2016.
 */
public class DialogContext {

    private int layoutResourceId;
    private final String title;
    private final String message;
    private final String positiveActionText;
    private final String negetiveActionText;

    public DialogContext(String title, String message, String positiveActionText, String negetiveActionText){

        this.title = title;
        this.message = message;
        this.positiveActionText = positiveActionText;
        this.negetiveActionText = negetiveActionText;
    }

    public DialogContext(String title, String message){
        this(title, message, "OK", "CANCAEL");
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public String getPositiveActionText() {
        return positiveActionText;
    }

    public String getNegetiveActionText() {
        return negetiveActionText;
    }

    public int getLayoutResourceId() {
        return layoutResourceId;
    }

    public void setLayoutResourceId(int layoutResourceId) {
        this.layoutResourceId = layoutResourceId;
    }
}
