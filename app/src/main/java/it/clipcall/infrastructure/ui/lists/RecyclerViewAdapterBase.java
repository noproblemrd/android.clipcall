package it.clipcall.infrastructure.ui.lists;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;

/**
 * Created by dorona on 30/03/2016.
 */
public abstract class RecyclerViewAdapterBase<T, V extends View> extends RecyclerView.Adapter<ViewWrapper<V>> {

    protected final List<T> items = new ArrayList<T>();

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public final ViewWrapper<V> onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewWrapper<V>(onCreateItemView(parent, viewType));
    }


    public T getItemByCriteria(ICriteria<T> criteria){
        T result = Lists.firstOrDefault(items, criteria);
        return result;
    }

    public int getItemIndexByCriteria(ICriteria<T> criteria){
        int index = Lists.firstIndexOrDefault(items, criteria);
        return index;
    }

    public void setItems(List<T> items){
        this.items.clear();
        if(Lists.isNullOrEmpty(items))
            return;

        this.items.addAll(items);
        this.notifyDataSetChanged();
    }

    public T getItem(int position){
        return this.items.get(position);
    }

    protected abstract V onCreateItemView(ViewGroup parent, int viewType);

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<T> getItems() {
        return items;
    }

    public boolean remove(T entity){
        boolean result =  this.items.remove(entity);
        this.notifyDataSetChanged();
        return result;
    }


    public boolean add(T entity){
        boolean result =  this.items.add(entity);
        this.notifyDataSetChanged();
        return result;
    };

    // additional methods to manipulate the items
}