package it.clipcall.infrastructure.ui.dialogs;


import android.app.Activity;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.View;

import javax.inject.Inject;

import it.clipcall.infrastructure.commands.ICommand;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import me.drakeet.materialdialog.MaterialDialog;

@PerActivity
public class DialogService {

    private final MaterialDialog dialog;
    private final Handler handler;

    private boolean isPositiveAction = false;


    @Inject
    public DialogService(Activity activity){
        dialog = new MaterialDialog(activity);
        handler = new Handler(activity.getMainLooper());
    }


    public void show(DialogContext dialogContext){
        this.show(dialogContext, null, null);
    }

    public void show(final DialogContext dialogContext, final ICommand positiveCommand, final ICommand negetiveCommand){

        handler.post(new Runnable() {
            @Override
            public void run() {
                dialog
                        .setTitle(dialogContext.getTitle())
                        .setMessage(dialogContext.getMessage())
                        .setPositiveButton(dialogContext.getPositiveActionText(), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                isPositiveAction = true;
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton(dialogContext.getNegetiveActionText(), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                isPositiveAction = false;
                                dialog.dismiss();


                            }
                        });

                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if(isPositiveAction && positiveCommand != null){
                            positiveCommand.execute();
                        }else if(!isPositiveAction && negetiveCommand != null){
                            negetiveCommand.execute();
                        }
                    }
                });
                dialog.show();
            }
        });
    }

    public void show(final int layoutResourceId){


        handler.post(new Runnable() {

            @Override
            public void run() {
                dialog.setContentView(layoutResourceId);
                dialog.show();

            }
        });
    }

}
