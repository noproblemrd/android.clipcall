package it.clipcall.infrastructure.ui.behavior;

import android.view.View;
import android.view.View.OnLongClickListener;

/**
 * Created by micro on 2/6/2016.
 */
public class StartDraggingOnLongClickBehavior implements OnLongClickListener{

    private final Object localState;

    public StartDraggingOnLongClickBehavior(Object localState) {
        this.localState = localState;
    }

    @Override
    public boolean onLongClick(View v) {
        View.DragShadowBuilder shadowBuilder =
                new View.DragShadowBuilder(v);
// Start a drag, and pass the View's audioData along as
// the local state
        v.startDrag(null, shadowBuilder, localState, 0);
        return true;
    }
}
