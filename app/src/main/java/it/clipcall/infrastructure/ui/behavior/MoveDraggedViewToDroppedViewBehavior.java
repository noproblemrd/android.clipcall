package it.clipcall.infrastructure.ui.behavior;

import android.view.DragEvent;
import android.view.View;

/**
 * Created by micro on 2/6/2016.
 */
public class MoveDraggedViewToDroppedViewBehavior implements View.OnDragListener {

    private boolean isDropped;

    @Override
    public boolean onDrag(View v, DragEvent event) {
        switch (event.getAction()){
            case DragEvent.ACTION_DROP:

                float x = event.getX();
                float y = event.getY();
                v.setX(x);
                v.setY(y);
                break;

            default:
                return false;

        }

        return true;
    }
}
