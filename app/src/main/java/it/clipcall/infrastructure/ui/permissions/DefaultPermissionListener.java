package it.clipcall.infrastructure.ui.permissions;

import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

/**
 * Created by omega on 3/6/2016.
 */
public class DefaultPermissionListener implements MultiplePermissionsListener {


    private final IPermissionsHandler handler;

    public DefaultPermissionListener(IPermissionsHandler handler){

        this.handler = handler;
    }

    @Override
    public void onPermissionsChecked(MultiplePermissionsReport report) {
        if(report.areAllPermissionsGranted())
        {
            handler.handleAllPermissionsGranted();
            return;
        }

        handler.handlePermissionDenied();
    }

    @Override
    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
        token.continuePermissionRequest();
    }
}
