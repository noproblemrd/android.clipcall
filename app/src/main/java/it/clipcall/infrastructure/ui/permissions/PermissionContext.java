package it.clipcall.infrastructure.ui.permissions;

/**
 * Created by omega on 3/16/2016.
 */
public class PermissionContext {

    private final String title;
    private final String message;
    private final String[] permissions;

    public PermissionContext(String title, String message, String...permissions){

        this.title = title;
        this.message = message;
        this.permissions = permissions;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public String[] getPermissions() {
        return permissions;
    }
}
