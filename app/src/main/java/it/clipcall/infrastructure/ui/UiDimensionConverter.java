package it.clipcall.infrastructure.ui;

import android.content.res.Resources;

/**
 * Created by dorona on 07/04/2016.
 */
public class UiDimensionConverter {

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px)
    {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
}
