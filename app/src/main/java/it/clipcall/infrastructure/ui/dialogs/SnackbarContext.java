package it.clipcall.infrastructure.ui.dialogs;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by dorona on 12/07/2016.
 */
public class SnackbarContext {

    private final View view;
    private final String message;
    private final View.OnClickListener onClickListener;
    @Snackbar.Duration
    private final int duration;
    private final int numberOfRows;

    public SnackbarContext(View view, String message, View.OnClickListener onClickListener, @Snackbar.Duration int duration, int numberOfRows){
        this.view = view;
        this.message = message;
        this.onClickListener = onClickListener;
        this.duration = duration;
        this.numberOfRows = numberOfRows;
    }


    public View.OnClickListener getOnClickListener() {
        return onClickListener;
    }


    public @Snackbar.Duration int getDuration() {
        return duration;
    }

    public int getNumberOfRows() {
        return numberOfRows;
    }

    public String getMessage() {
        return message;
    }

    public View getView() {
        return view;
    }
}
