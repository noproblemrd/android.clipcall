package it.clipcall.infrastructure.ui.images.services;

import android.app.ProgressDialog;
import android.content.Context;


public class ProgressBarService {


    private ProgressDialog progressBar;

    public ProgressBarService(Context context){

        progressBar = new ProgressDialog(context);
    }

    public void hide(){
        progressBar.hide();
    }

    public void show(){
        progressBar.setIndeterminate(true);
        progressBar.show();
    }


}
