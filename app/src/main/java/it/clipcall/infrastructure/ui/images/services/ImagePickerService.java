package it.clipcall.infrastructure.ui.images.services;


import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;

import javax.inject.Inject;

import it.clipcall.infrastructure.activities.TransparentActivity;
import it.clipcall.infrastructure.commands.ICallBack;
import it.clipcall.infrastructure.commands.PickImageCommand;
import it.clipcall.infrastructure.commands.RequestPermissionsCommandDecorator;
import it.clipcall.infrastructure.di.scopes.PerActivity;

@PerActivity
public class ImagePickerService {

    final CharSequence[] items = { "Take Photo", "Choose from Library", "Cancel" };
    private Activity activity;

    @Inject
    public ImagePickerService(Activity activity){
        this.activity = activity;
    }


    public void pickImage(final ICallBack<Bitmap> callBack){

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    PickImageCommand command = new PickImageCommand(activity, TransparentActivity.Camera);
                    RequestPermissionsCommandDecorator decorator = new RequestPermissionsCommandDecorator(command,activity,"Camera", "Camera", Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
                    decorator.execute(callBack);
                    return;

                }
                if (items[item].equals("Choose from Library")) {
                    PickImageCommand command = new PickImageCommand(activity, TransparentActivity.Gallery);
                    RequestPermissionsCommandDecorator decorator = new RequestPermissionsCommandDecorator(command,activity,"Gallery", "Gallery", Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    decorator.execute(callBack);
                    return;
                }
                if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                    callBack.onFailure();
                }
            }
        });
        builder.show();
    }
}
