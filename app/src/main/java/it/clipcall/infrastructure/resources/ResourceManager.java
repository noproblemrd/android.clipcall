package it.clipcall.infrastructure.resources;


import android.app.Activity;

import javax.inject.Inject;

import it.clipcall.infrastructure.di.scopes.PerActivity;

@PerActivity
public class ResourceManager {


    private final Activity activity;

    @Inject
    public ResourceManager(Activity activity){

        this.activity = activity;
    }

    public String getString(int resourceId){
        return activity.getString(resourceId);
    }

    public String getString(int resourceId,Object... formatArgs){
        return activity.getString(resourceId,formatArgs);
    }
}
