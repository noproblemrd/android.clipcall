package it.clipcall.infrastructure.thirdparty;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import org.androidannotations.annotations.EReceiver;
import org.androidannotations.annotations.SystemService;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.ClipCallApplication;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.common.chat.services.ChatMessageDispatcher;
import it.clipcall.common.chat.support.IChatConnectionListener;
import it.clipcall.infrastructure.gcm.services.GcmTokenProvider;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.professional.validation.services.AuthenticationManager;


@Singleton
@EReceiver
public class LayerPushReceiver extends BroadcastReceiver {


    private final ILogger logger = LoggerFactory.getLogger(LayerPushReceiver.class.getSimpleName());

    @SystemService
    NotificationManager notificationManager;


    private int NotificaitonId = 20;

    @Inject
    public LayerPushReceiver(){

    }

    @Inject ChatMessageDispatcher dispatcher;

    @Inject
    AuthenticationManager authenticationManager;



    @Inject
    ChatManager chatManager;

    @Inject
    GcmTokenProvider gcmTokenProvider;

    private void handleChatMessage(Context context, String messageText, Uri conversationId, Uri messageId){
        try {
            final ClipCallApplication application = (ClipCallApplication)context.getApplicationContext();
            IMessage message = chatManager.getMessageById(conversationId, messageId);
            dispatcher.dispatchMessage(message);
        } catch (Exception e) {
            logger.error("handleChatMessage failed with error %s", e.getMessage());
        }
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        try{
            logger.debug("onReceive: layer push message - start");
            final ClipCallApplication application = (ClipCallApplication)context.getApplicationContext();
            application.getComponent().inject(this);

            if(intent.getAction() == Intent.ACTION_BOOT_COMPLETED)
                return;

            Bundle extras = intent.getExtras();
            if(!extras.containsKey("layer-push-message"))
                return;

            if (!extras.containsKey("layer-conversation-id"))
                return;

            if(!extras.containsKey("layer-message-id"))
                return;


            final Uri conversationId = extras.getParcelable("layer-conversation-id");
            final Uri  messageId = extras.getParcelable("layer-message-id");
            final String messageText = extras.getString("layer-push-message");
            String gcmRegistrationId = gcmTokenProvider.getGcmToken();
            String userId = authenticationManager.getCustomerId();


            logger.debug("onReceive: handling message with id - "+ messageId);
            logger.debug("onReceive: gcm registration id - "+ gcmRegistrationId);
            logger.debug("onReceive: user id - "+ userId);

            chatManager.connect(userId, gcmRegistrationId, new IChatConnectionListener() {


                @Override
                public void onConnected() {
                    logger.debug("onConnected: user is connected to chat");
                    handleChatMessage(context, messageText, conversationId, messageId);
                }
            });

            logger.debug("onReceive: layer push message - end");
        }
        catch (Exception e){
            logger.error("onReceive: "+e.getMessage(), e);
        }

    }


}
