package it.clipcall.infrastructure.thirdparty;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;

import org.simple.eventbus.EventBus;

import java.util.Map;

import it.clipcall.common.models.ConsumerEventNames;
import it.clipcall.common.models.ApplicationEvent;

public class AppsFlyerBroadcastReceiver extends BroadcastReceiver {

    public AppsFlyerBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        AppsFlyerLib.registerConversionListener(context.getApplicationContext(), new AppsFlyerConversionListener() {

            @Override
            public void onAppOpenAttribution(Map<String, String> arg0) {


            }

            @Override
            public void onAttributionFailure(String errorMessage) {
                //Added this to avoid compilation failure
            }

            @Override
            public void onInstallConversionDataLoaded(Map<String, String> conversionData) {
                //EventBus.getDefault().post(new MixPanelEvent(ConsumerEventNames.ApplicationInstalled, conversionData));
               // Bundle extras = intent.getExtras();
                EventBus.getDefault().post(new ApplicationEvent(ConsumerEventNames.ApplicationInstalled, conversionData));

            }

            @Override
            public void onInstallConversionFailure(String arg0) {


            }

        });


    }
}
