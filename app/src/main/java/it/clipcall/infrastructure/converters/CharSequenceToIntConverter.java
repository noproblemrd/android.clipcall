package it.clipcall.infrastructure.converters;

import com.google.common.base.Strings;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by dorona on 02/08/2016.
 */
@Singleton
public class CharSequenceToIntConverter implements IConverter<CharSequence, Integer> {

    @Inject
    public CharSequenceToIntConverter(){

    }

    @Override
    public Integer convert(CharSequence charSequence) {
        if(charSequence == null)
            return null;
        String value = charSequence + "";
        if(Strings.isNullOrEmpty(value))
            return null;
       try{
           Integer result = Integer.parseInt(value);
           return result;
       }catch (Exception e){
           return null;
       }
    }
}
