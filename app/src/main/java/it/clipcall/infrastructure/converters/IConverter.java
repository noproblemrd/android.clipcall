package it.clipcall.infrastructure.converters;

/**
 * Created by dorona on 13/07/2016.
 */
public interface IConverter<TFrom,TTo> {

    TTo convert(TFrom from);
}
