package it.clipcall.infrastructure.converters;

import java.text.NumberFormat;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.support.collections.IFormatter;

@Singleton
public class UsCurrencyFormatter implements IFormatter<Double,String> {

    private final static  Locale locale = new Locale("en", "US");

    @Inject
    public UsCurrencyFormatter(){

    }

    @Override
    public String format(Double input) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
        String formattedValue = currencyFormatter.format(input);
        return formattedValue;
    }
}
