package it.clipcall.infrastructure.activities.ui;

import android.support.v7.widget.Toolbar;


public interface IHasToolbar {
    String getViewTitle();
    Toolbar getToolbar();
    boolean onBackTapped();
}
