package it.clipcall.infrastructure.activities.lifecycle;

import android.app.Activity;
import android.support.v4.app.FragmentManager;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.validation.activities.PhoneValidation2Activity;
import it.clipcall.common.validation.activities.PhoneValidationActivity;
import it.clipcall.common.validation.models.events.ValidationCancelledEvent;
import it.clipcall.consumer.about.activities.PdfActivity;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.services.RoutingService;

@Singleton
public class RoutingActivityLifecycleCallbacks extends ActivityLifecycleCallbacksBase  {

    private final RoutingService routingService;


    @Inject
    public RoutingActivityLifecycleCallbacks(RoutingService routingService) {
        this.routingService = routingService;
        EventBus.getDefault().register(this);
    }

    @Override
    public void onActivityStarted(Activity activity) {
        super.onActivityStarted(activity);


        if(activity instanceof PhoneValidationActivity)
            return;

        if(activity instanceof PhoneValidation2Activity)
            return;

        if(activity instanceof PdfActivity)
            return;

        if(!(activity instanceof  BaseActivity))
            return;



        if(!routingService.hasPendingSubRoute())
            return;

        BaseActivity baseActivity = (BaseActivity)activity;
        FragmentManager fragmentManager = baseActivity.getSupportFragmentManager();
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = new RoutingContext(baseActivity);
        routingService.routeTo((Route) null, navigationContext);
    }


    @Subscriber
    void onValidationCancelled(ValidationCancelledEvent event){
        routingService.clearPendingRoute();
    }
}
