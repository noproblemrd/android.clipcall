package it.clipcall.infrastructure.activities.lifecycle;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.R;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.annotations.OverrideDefaultStyle;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class ThemingActivityLifecycleCallbacks extends ActivityLifecycleCallbacksBase{
    private final String tag = "Themes";
    private final AuthenticationManager authenticationManager;


    @Inject
    public ThemingActivityLifecycleCallbacks(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        if(!(activity instanceof BaseActivity))
            return;


        List<Annotation> annotations = Arrays.asList(activity.getClass().getSuperclass().getAnnotations());
        boolean isIgnoreDefaultStyle = Lists.any(annotations, new ICriteria<Annotation>() {
            @Override
            public boolean isSatisfiedBy(Annotation candidate) {
                return candidate instanceof OverrideDefaultStyle;
            }
        });
        if(isIgnoreDefaultStyle)
            return;


        if(authenticationManager.isInProfessionalMode()){
            activity.setTheme(R.style.Professional);
        }
        else{
            activity.setTheme(R.style.ConsumerBaseTheme);
        }

        String activityName = activity.getLocalClassName();
        Log.d(tag,"onActivityCreated:"+activityName);
    }
}
