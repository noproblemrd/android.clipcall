package it.clipcall.infrastructure.activities.lifecycle;

import android.app.Activity;
import android.os.Bundle;

import org.simple.eventbus.EventBus;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class EventBusActivityLifecycleCallbacks extends ActivityLifecycleCallbacksBase {


    @Inject
    public EventBusActivityLifecycleCallbacks() {
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        EventBus.getDefault().register(activity);
    }

    @Override
    public void onActivityStarted(Activity activity) {
        EventBus.getDefault().register(activity);
    }

    @Override
    public void onActivityResumed(Activity activity) {
        EventBus.getDefault().register(activity);
    }

    @Override
    public void onActivityPaused(Activity activity) {
        EventBus.getDefault().unregister(activity);
    }

    @Override
    public void onActivityStopped(Activity activity) {
        EventBus.getDefault().unregister(activity);
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        EventBus.getDefault().unregister(activity);
    }
}
