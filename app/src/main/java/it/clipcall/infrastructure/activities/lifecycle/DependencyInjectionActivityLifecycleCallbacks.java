package it.clipcall.infrastructure.activities.lifecycle;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.di.components.ActivityComponent;
import it.clipcall.infrastructure.di.components.ApplicationComponent;
import it.clipcall.infrastructure.di.components.DaggerActivityComponent;
import it.clipcall.infrastructure.di.modules.ActivityModule;
import it.clipcall.infrastructure.di.support.InjectMethodsCache;

@Singleton
public class DependencyInjectionActivityLifecycleCallbacks extends ActivityLifecycleCallbacksBase  {

    private final ApplicationComponent applicationComponent;

    @Inject
    public DependencyInjectionActivityLifecycleCallbacks(ApplicationComponent applicationComponent) {
        this.applicationComponent = applicationComponent;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if(!(activity instanceof BaseActivity))
            return;

        BaseActivity applicationActivity = (BaseActivity) activity;
        ActivityComponent activityComponent = DaggerActivityComponent.builder().applicationComponent(applicationComponent)
                .activityModule(new ActivityModule(applicationActivity)).build();

        applicationActivity.setActivityComponent(activityComponent);

        Map<Class, Method> cache = InjectMethodsCache.getActivityComponentsMethodsCache();
        Method method = cache.get(activity.getClass().getSuperclass());
        if(method == null)
            return;

        try {
            method.invoke(activityComponent, activity);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
