package it.clipcall.infrastructure.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.EActivity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import it.clipcall.infrastructure.di.components.ActivityComponent;
import it.clipcall.infrastructure.di.support.InjectMethodsCache;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.views.IView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

@EActivity
public abstract class BaseActivity extends AppCompatActivity implements IView {
    private ActivityComponent activityComponent;

    @Override
    public RoutingContext getRoutingContext() {
        return new RoutingContext(this);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void setActivityComponent(ActivityComponent activityComponent) {
        this.activityComponent = activityComponent;
    }


    public void onFragmentAttached(BaseFragment fragment){
        Map<Class, Method> cache = InjectMethodsCache.getActivityComponentsMethodsCache();
        Method method = cache.get(fragment.getClass().getSuperclass());
        if(method == null)
            return;

        try {
            method.invoke(activityComponent, fragment);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public ActivityComponent getActivityComponent() {
        return activityComponent;
    }


}
