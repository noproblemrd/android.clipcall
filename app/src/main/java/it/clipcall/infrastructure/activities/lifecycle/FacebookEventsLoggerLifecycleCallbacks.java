package it.clipcall.infrastructure.activities.lifecycle;

import android.app.Activity;
import android.os.Bundle;

import com.facebook.appevents.AppEventsLogger;

import org.json.JSONObject;
import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.environment.Environment;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.support.json.JsonBuilder;

@Singleton
public class FacebookEventsLoggerLifecycleCallbacks extends ActivityLifecycleCallbacksBase{
    private final String tag = "FacebookEvents";
    private final JsonBuilder jsonBuilder;
    AppEventsLogger logger;


    @Inject
    public FacebookEventsLoggerLifecycleCallbacks(JsonBuilder jsonBuilder) {
        this.jsonBuilder = jsonBuilder;
        EventBus.getDefault().register(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        if(!Environment.AnalyticsEnabled){
            return;
        }
        if(activity instanceof BaseActivity)
            logger  = AppEventsLogger.newLogger(activity);
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onApplicationEvent(ApplicationEvent applicationEvent){
        if(logger == null)
            return;
        try {
            JSONObject jsonObject = jsonBuilder.buildJsonObject(applicationEvent.getData());
            Bundle bundle = new Bundle();
            bundle.putString("data",jsonObject.toString());
            logger.logEvent(applicationEvent.getName(), bundle);
        }
        catch (Exception e){

        }
    }
}
