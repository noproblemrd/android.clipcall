package it.clipcall.infrastructure.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.os.ResultReceiver;
import android.view.WindowManager;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.parceler.Parcels;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import it.clipcall.infrastructure.maps.entities.LatitudeLongitude;
import it.clipcall.infrastructure.maps.entities.PlaceData;
import it.clipcall.infrastructure.support.images.BitmapAssistant;
import it.clipcall.professional.profile.activities.ProfessionalCoverAreaActivity_;
import it.clipcall.professional.profile.activities.ProfessionalServiceCategoriesActivity_;

/**
 * Created by omega on 4/7/2016.
 */
public class TransparentActivity extends Activity {


    public static final int PickImageAction = 1;
    public static final int PickAddressAction = 2;
    public static final int PickCoverAreaAction = 3;
    public static final int PickCategoriesAction = 4;

    public static final int Camera = 555;
    public static final int Gallery = 666;
    public static final int GoogleAddress = 777;
    public static final int CoverArea = 888;
    public static final int Categories = 999;

    ResultReceiver resultReceiver;
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        invokeAction(getIntent());
    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        invokeAction(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode != RESULT_OK || data == null)
        {
            resultReceiver.send(resultCode,Bundle.EMPTY);
            finish();
            return;
        }

        if (requestCode == Gallery) {
            Bitmap bitmap = createImage(data);
            Bundle bundle = new Bundle();
            bundle.putParcelable("image",bitmap);
            resultReceiver.send(resultCode,bundle);
            finish();
            return;
        }

        if(requestCode == Camera){
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            Bundle bundle = new Bundle();
            bundle.putParcelable("image",imageBitmap);
            resultReceiver.send(resultCode,bundle);
            finish();
            return;
        }

        if(requestCode == GoogleAddress){
            Place place = PlaceAutocomplete.getPlace(this, data);
            PlaceData placeData = new PlaceData();
            placeData.address = place.getAddress()+"";
            placeData.latLng = new LatitudeLongitude(place.getLatLng().latitude, place.getLatLng().longitude);
            Bundle bundle = new Bundle();
            bundle.putParcelable("place", Parcels.wrap(placeData));
            resultReceiver.send(resultCode,bundle);
            finish();
            return;
        }


        if(requestCode == CoverArea){
            Bundle extras = data.getExtras();
            Parcelable parcelable = extras.getParcelable("area");
            if(parcelable == null)
            {
                resultReceiver.send(RESULT_CANCELED,null);
                return;
            }

            Bundle bundle = new Bundle();
            bundle.putParcelable("area", parcelable);
            resultReceiver.send(resultCode,bundle);
            finish();
            return;
        }

        if(requestCode == Categories){
            Bundle extras = data.getExtras();
            Parcelable parcelable = extras.getParcelable("categories");
            if(parcelable == null)
            {
                resultReceiver.send(RESULT_CANCELED,null);
                return;
            }

            Bundle bundle = new Bundle();
            bundle.putParcelable("categories", parcelable);
            resultReceiver.send(resultCode,bundle);
            finish();
            return;
        }

    }


    private Bitmap createImage(Intent data) {
        Uri pickedImage = data.getData();
        //Bitmap image = BitmapAssistant.createImage(getContentResolver(), pickedImage);
        Bitmap image = BitmapAssistant.createScaledImage2(getContentResolver(), pickedImage);
        return image;
    }

    private File createImageFile() {
        // Create an image file name
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

            return image;
        }
        catch (Exception exception){
            exception.printStackTrace();
            return null;
        }
    }

    private void invokeAction(Intent intent){
        Bundle bundle = intent.getExtras();
        int action = bundle.getInt("action");
        if(action == TransparentActivity.PickImageAction){
            pickImage(intent);
            return;
        }

        if(action == TransparentActivity.PickAddressAction){
            pickAddress(intent);
            return;
        }

        if(action == TransparentActivity.PickCoverAreaAction){
            pickCoverArea(intent);
            return;
        }

        if(action == TransparentActivity.PickCategoriesAction){
            pickCategories(intent);
            return;
        }
    }

    private void pickCategories(Intent incomingIntent) {
        Bundle bundle = incomingIntent.getExtras();
        resultReceiver = bundle.getParcelable("receiver");
        Parcelable categories = bundle.getParcelable("categories");
        Intent intent = new Intent(this, ProfessionalServiceCategoriesActivity_.class);
        intent.putExtra("categories", categories);
        startActivityForResult(intent, Categories);
    }

    private void pickCoverArea(Intent incomingIntent) {
        Bundle bundle = incomingIntent.getExtras();
        resultReceiver = bundle.getParcelable("receiver");
        Parcelable coverArea = bundle.getParcelable("coverArea");
        Intent intent = new Intent(this, ProfessionalCoverAreaActivity_.class);
        intent.putExtra("coverArea", coverArea);
        startActivityForResult(intent, CoverArea);
    }

    private void pickAddress(Intent incomingIntent) {
        Bundle bundle = incomingIntent.getExtras();
        resultReceiver = bundle.getParcelable("receiver");
        LatLngBounds usaBounds = new LatLngBounds(new LatLng( 32.6393,-117.004304), new LatLng(44.901184 ,-67.32254));
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).build();
        Intent intent =
                null;
        try {
            intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(typeFilter)
                    .setBoundsBias(usaBounds)
                    .build(this);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
            finish();
        }
        startActivityForResult(intent, GoogleAddress);
    }

    private void pickImage(Intent incomingIntent){
        Bundle bundle = incomingIntent.getExtras();
        resultReceiver = bundle.getParcelable("receiver");
        int source = bundle.getInt("source");
        Intent intent = null;
        if(source == Gallery)
        {
            intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");

        }
        else if(source == Camera) {
            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photoFile = createImageFile();
            if (photoFile != null) {
                // takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(photoFile));
            }
        }

        if(intent == null || intent.resolveActivity(getPackageManager()) == null){
            resultReceiver.send(Camera, Bundle.EMPTY);
            finish();
            return;
        }

        startActivityForResult(Intent.createChooser(intent, "Select File"), source);
    }


}