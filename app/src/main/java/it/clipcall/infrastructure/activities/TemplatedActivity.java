package it.clipcall.infrastructure.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.EActivity;

import it.clipcall.R;
import it.clipcall.infrastructure.views.IView;


@EActivity
public abstract class TemplatedActivity extends BaseActivity implements IView {

    AppBarLayout appBarLayout;
    ViewGroup contentContainer;
    protected Toolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.default_template_activity_layout);
        contentContainer = (ViewGroup)findViewById(R.id.contentContainer);
        appBarLayout = (AppBarLayout)findViewById(R.id.appBarLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        final AppCompatActivity compatActivity = this;
        this.setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar supportActionBar = this.getSupportActionBar();
        supportActionBar.setDisplayHomeAsUpEnabled(true);
        supportActionBar.setHomeButtonEnabled(true);
        supportActionBar.setDisplayShowHomeEnabled(true);

 /*       int color = activity.getResources().getColor(R.color.consumer_primary);
        toolbar.setBackgroundColor(color);*/

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compatActivity.finish();
            }
        });


        appBarLayout.setLayoutParams(new CoordinatorLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getDefaultAppBarLayoutHeight()));
        int resourceId = getContentLayoutResourceId();
        getLayoutInflater().inflate(resourceId,contentContainer);
    }

    public abstract int getContentLayoutResourceId();

    public int getDefaultAppBarLayoutHeight(){
        TypedValue tv = new TypedValue();
        int height = 100;
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            height = tv.data;
        }

        return TypedValue.complexToDimensionPixelSize(height, getResources().getDisplayMetrics());
    }
}
