package it.clipcall.infrastructure.activities.lifecycle;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class LoggerActivityLifecycleCallbacks extends ActivityLifecycleCallbacksBase{
    private final String tag = "Activities";



    @Inject
    public LoggerActivityLifecycleCallbacks() {
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        String activityName = activity.getLocalClassName();
        Log.d(tag,"onActivityCreated:"+activityName);
    }

    @Override
    public void onActivityStarted(Activity activity) {
        String activityName = activity.getLocalClassName();
        Log.d(tag,"onActivityStarted:"+activityName);
    }

    @Override
    public void onActivityResumed(Activity activity) {
        String activityName = activity.getLocalClassName();
        Log.d(tag,"onActivityResumed:"+activityName);
    }

    @Override
    public void onActivityPaused(Activity activity) {
        String activityName = activity.getLocalClassName();
        Log.d(tag,"onActivityPaused:"+activityName);
    }

    @Override
    public void onActivityStopped(Activity activity) {
        String activityName = activity.getLocalClassName();
        Log.d(tag,"onActivityStopped:"+activityName);
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        String activityName = activity.getLocalClassName();
        Log.d(tag,"onActivitySaveInstanceState:"+activityName);
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        String activityName = activity.getLocalClassName();
        Log.d(tag,"onActivityDestroyed:"+activityName);
    }
}
