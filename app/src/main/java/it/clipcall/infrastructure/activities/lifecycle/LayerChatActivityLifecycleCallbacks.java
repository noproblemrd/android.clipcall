package it.clipcall.infrastructure.activities.lifecycle;

import android.app.Activity;
import android.util.Log;

import com.google.common.base.Strings;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.common.chat.support.IChatConnectionListener;
import it.clipcall.infrastructure.gcm.services.GcmTokenProvider;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class LayerChatActivityLifecycleCallbacks extends ActivityLifecycleCallbacksBase implements IChatConnectionListener{
    private static final String TAG = "LayerChatCallbacks";
    private final AuthenticationManager authenticationManager;
    private final ChatManager chatManager;
    private final GcmTokenProvider gcmTokenProvider;


    @Inject
    public LayerChatActivityLifecycleCallbacks(AuthenticationManager authenticationManager, ChatManager chatManager, GcmTokenProvider gcmTokenProvider) {
        this.authenticationManager = authenticationManager;
        this.chatManager = chatManager;
        this.gcmTokenProvider = gcmTokenProvider;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        if(authenticationManager.isInProfessionalMode() && !authenticationManager.isProfessionalRegistered())
        {
            Log.d(TAG, "onActivityResumed: user is in pro mode, but not registered yet. no need to connect to client");
            return;
        }

        if(authenticationManager.inInConsumerMode() && !authenticationManager.isCustomerAuthenticated())
        {
            Log.d(TAG, "onActivityResumed: user is connected to chat client");
            return;
        }

        if(chatManager.isConnected(authenticationManager.getUserId()))
        {
            Log.d(TAG, "onActivityResumed: user is connected to chat client");
            return;
        }


        String gcmRegistrationId = gcmTokenProvider.getGcmToken();
        if(Strings.isNullOrEmpty(gcmRegistrationId))
            return;

        String userId = authenticationManager.getCustomerId();
        if(Strings.isNullOrEmpty(userId))
            return;

        chatManager.connect(userId,gcmRegistrationId, this);
    }


    @Override
    public void onConnected() {
        Log.d(TAG, "onConnected: user is connected to chat");
    }
}
