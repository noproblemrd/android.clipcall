package it.clipcall.infrastructure.activities.lifecycle;

import android.app.Activity;
import android.util.Log;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.ClipCallApplication;
import it.clipcall.infrastructure.activities.BaseActivity;

@Singleton
public class IsActivityInForegroundLifecycleCallbacks extends ActivityLifecycleCallbacksBase{

    private final String tag = "IsActivityInForeground";
    private final ClipCallApplication application;
    private int visibleActivitiesCount = 0;

    @Inject
    public IsActivityInForegroundLifecycleCallbacks(ClipCallApplication application) {
        this.application = application;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        if(activity instanceof BaseActivity){
            visibleActivitiesCount++;
        }

        updateIsInForeground(activity);

        Log.d(tag, "visibleActivitiesCount: " + visibleActivitiesCount);
    }

    @Override
    public void onActivityPaused(Activity activity) {
        if(activity instanceof BaseActivity){
            visibleActivitiesCount--;
        }
        updateIsInForeground(activity);
        Log.d(tag, "visibleActivitiesCount: " + visibleActivitiesCount);
    }

    void updateIsInForeground(Activity currentActivity){
        if(visibleActivitiesCount > 0){
            application.setCurrentActivity(currentActivity);
        }else{
            application.setCurrentActivity(null);
        }
    }
}
