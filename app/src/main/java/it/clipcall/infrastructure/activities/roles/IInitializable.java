package it.clipcall.infrastructure.activities.roles;

/**
 * Created by micro on 1/27/2016.
 */
public interface IInitializable {

    void initialize();
}
