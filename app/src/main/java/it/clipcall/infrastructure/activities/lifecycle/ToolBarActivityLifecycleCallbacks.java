package it.clipcall.infrastructure.activities.lifecycle;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.common.base.Strings;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.activities.ui.IHasToolbar;

@Singleton
public class ToolBarActivityLifecycleCallbacks extends ActivityLifecycleCallbacksBase {

    @Inject
    public ToolBarActivityLifecycleCallbacks() {
    }

    @Override
    public void onActivityStarted(Activity activity) {
        super.onActivityStarted(activity);
        if(!(activity instanceof AppCompatActivity))
            return;

        if(!(activity instanceof IHasToolbar))
            return;

        final IHasToolbar viewWithToolBar = (IHasToolbar)activity;
        final AppCompatActivity compatActivity = (AppCompatActivity)activity;

        Toolbar toolbar = viewWithToolBar.getToolbar();
        compatActivity.setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar supportActionBar = compatActivity.getSupportActionBar();
        supportActionBar.setDisplayHomeAsUpEnabled(true);
        supportActionBar.setHomeButtonEnabled(true);
        supportActionBar.setDisplayShowHomeEnabled(true);

 /*       int color = activity.getResources().getColor(R.color.consumer_primary);
        toolbar.setBackgroundColor(color);*/

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean handledBackTapped = viewWithToolBar.onBackTapped();
                if(!handledBackTapped ){
                    compatActivity.finish();
                }

            }
        });



        String title = viewWithToolBar.getViewTitle();

        if(!Strings.isNullOrEmpty(title))
            Log.d("foo", title);
            supportActionBar.setTitle(title);

    }
}