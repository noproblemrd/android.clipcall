package it.clipcall.infrastructure.bootstrappingExtensions;


public interface IBootstrappingExtension {

    void initialize();
}
