package it.clipcall.infrastructure.bootstrappingExtensions;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class CompositeBootstrappingExtension implements IBootstrappingExtension {


    private static final String TAG = "Bootstrapping";
    private final List<IBootstrappingExtension> bootstrappingExtensions = new ArrayList<>();


    public void addBootstrappingExtension(IBootstrappingExtension extension){
        bootstrappingExtensions.add(extension);
    }

    @Override
    public void initialize() {
        for(IBootstrappingExtension bootstrappingExtension: bootstrappingExtensions)
        {
            Log.d(TAG, "initializing " + bootstrappingExtension.toString());
            bootstrappingExtension.initialize();
        }
    }
}
