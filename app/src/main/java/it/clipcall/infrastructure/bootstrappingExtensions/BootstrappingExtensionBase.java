package it.clipcall.infrastructure.bootstrappingExtensions;

import android.app.Application;


public abstract class BootstrappingExtensionBase implements IBootstrappingExtension {

    protected final Application application;

    protected BootstrappingExtensionBase(Application application){

        this.application = application;
    }

    @Override
    public abstract void initialize() ;

    public abstract String toString();
}
