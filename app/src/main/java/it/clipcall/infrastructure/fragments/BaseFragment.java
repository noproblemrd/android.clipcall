package it.clipcall.infrastructure.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EFragment;

import it.clipcall.ClipCallApplication;
import it.clipcall.consumer.mainMenu.support.IBadgeListener;
import it.clipcall.consumer.mainMenu.support.ICollapsingToolBarListener;
import it.clipcall.consumer.mainMenu.support.ITitleListener;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.di.components.ActivityComponent;


@EFragment
public abstract class BaseFragment extends Fragment {

    @App
    protected ClipCallApplication application;

    protected String getTitle(){
        return "";
    }

    protected int getMenuItemId(){return 0;}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof ITitleListener)
        {
            ITitleListener listener = (ITitleListener) context;
            listener.onTitleChanged(getTitle());
            listener.onItemActivated(getMenuItemId());
        }

        if(context instanceof IBadgeListener){
            IBadgeListener badgeListener = (IBadgeListener) context;
            badgeListener.showBadge(0);
        }

        if(context instanceof ICollapsingToolBarListener){
            ICollapsingToolBarListener collapsingToolBarListener = (ICollapsingToolBarListener) context;
            collapsingToolBarListener.showImage((String)null);
            collapsingToolBarListener.setAction(null);
        }

        if(context instanceof BaseActivity)
        {
            BaseActivity activity = (BaseActivity) context;
            activity.onFragmentAttached(this);
        }
    }

    public ActivityComponent getComponent(){
        return ((BaseActivity)getActivity()).getActivityComponent();
    }
}
