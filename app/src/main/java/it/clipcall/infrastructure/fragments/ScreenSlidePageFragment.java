package it.clipcall.infrastructure.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import it.clipcall.R;

public class ScreenSlidePageFragment extends Fragment{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View swipeView = inflater.inflate(R.layout.fragement_screen_slide_page, container, false);
        ImageView imageView = (ImageView) swipeView.findViewById(R.id.imageView);
        Bundle bundle = getArguments();
        int imageResourceId = bundle.getInt("imageResourceId");
        imageView.setImageResource(imageResourceId);
        return swipeView;
    }


    public static Fragment newInstance(int imageResourceId) {
        Fragment swipeFragment = new ScreenSlidePageFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("imageResourceId", imageResourceId);
        swipeFragment.setArguments(bundle);
        return swipeFragment;
    }
}
