package it.clipcall.infrastructure.viewModel.support.recycleviews;

/**
 * Created by omega on 4/25/2016.
 */
public interface IBindable<T> {

    void bind(T entity, int position, ItemSelectedListener itemSelectedListener);
}
