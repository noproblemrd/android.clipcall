package it.clipcall.infrastructure.viewModel.support.recycleviews;

/**
 * Created by omega on 4/29/2016.
 */
public interface ItemsLoader<T> {

    void loadItems();
}
