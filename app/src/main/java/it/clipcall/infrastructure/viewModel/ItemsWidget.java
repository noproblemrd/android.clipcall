package it.clipcall.infrastructure.viewModel;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;

import com.kennyc.view.MultiStateView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import it.clipcall.R;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.recyclerviews.ActionInvokerListener;
import it.clipcall.infrastructure.viewModel.support.recycleviews.ItemSelectedListener;
import it.clipcall.infrastructure.viewModel.support.recycleviews.ItemsLoader;
import it.clipcall.infrastructure.viewModel.support.recycleviews.RecyclerViewAdapterBase;
import jp.wasabeef.recyclerview.animators.BaseItemAnimator;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;

@EViewGroup(R.layout.items_widget_layout)
public class ItemsWidget<T> extends FrameLayout {

    @ViewById
    MultiStateView multiStateView;

    @ViewById
    RecyclerView itemsRecyclerView;

    @ViewById
    SwipeRefreshLayout itemsSwipeRefreshLayout;

    SwipeRefreshLayout emptyRefreshLayout;

    RecyclerViewAdapterBase adapterBase;
    private ItemsLoader<T> itemsLoader;

    @AfterViews
    void afterViews(){
        itemsRecyclerView.setHasFixedSize(true);
        itemsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterBase = new RecyclerViewAdapterBase();
        itemsRecyclerView.setAdapter(adapterBase);
        BaseItemAnimator animator = new FadeInAnimator();
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        itemsRecyclerView.setItemAnimator(animator);

        View emptyViewContainer = multiStateView.getView(MultiStateView.VIEW_STATE_EMPTY);
        emptyRefreshLayout = (SwipeRefreshLayout) emptyViewContainer.findViewById(R.id.emptyRefreshLayout);
        emptyRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                itemsLoader.loadItems();
            }
        });

        itemsSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                itemsLoader.loadItems();
            }
        });

    }

    public void setEmptyStateView(int layoutResourceId){
        multiStateView.setViewForState(layoutResourceId, MultiStateView.VIEW_STATE_EMPTY, false);
        View emptyViewContainer = multiStateView.getView(MultiStateView.VIEW_STATE_EMPTY);
        emptyRefreshLayout = (SwipeRefreshLayout) emptyViewContainer.findViewById(R.id.emptyRefreshLayout);
        emptyRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                itemsLoader.loadItems();
            }
        });
    }

    public void setLoadingStateView(int layoutResourceId){
        multiStateView.setViewForState(layoutResourceId, MultiStateView.VIEW_STATE_LOADING, false);
    }

    public void setErrorStateView(int layoutResourceId){
        multiStateView.setViewForState(layoutResourceId, MultiStateView.VIEW_STATE_ERROR, false);
    }

    public ItemsWidget(Context context) {
        this(context,null);
    }

    public ItemsWidget(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ItemsWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setItems(List<T> items) {

        boolean isRefreshing = false;
        itemsSwipeRefreshLayout.setRefreshing(isRefreshing);
        emptyRefreshLayout.setRefreshing(isRefreshing);

        if(Lists.isNullOrEmpty(items))
        {
            multiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
            return;
        }
        multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        boolean anyMoved = adapterBase.animateTo(items);
        if(anyMoved){
            itemsRecyclerView.getLayoutManager().scrollToPosition(0);
        }

        //adapterBase.setItems(items);
    }

    public void invalidateItems(){
        adapterBase.invalidate();
    }

    public void setItemSelectedListener(ItemSelectedListener<T> itemSelectedListener){
        adapterBase.setItemSelectedListener(itemSelectedListener);
    }

    public void setActionInvokerListener(ActionInvokerListener<T> actionInvokerListener){
        adapterBase.setActionInvokerListener(actionInvokerListener);
    }

    public void setItemsLoader(ItemsLoader<T> itemsLoader){
        this.itemsLoader = itemsLoader;
    }

    public void setViewBuilder(RecyclerViewAdapterBase.IViewBuilder<T> viewBuilder){
        adapterBase.setViewBuilder(viewBuilder);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ItemsWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
