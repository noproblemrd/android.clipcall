package it.clipcall.infrastructure.viewModel.support.recycleviews;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.infrastructure.support.recyclerviews.ActionInvokerListener;

/**
 * Created by omega on 4/25/2016.
 */
public class RecyclerViewAdapterBase<T, V extends View & IBindable<T> & IActionBindable<T>> extends RecyclerView.Adapter<ViewWrapper<V>> {

    private IViewBuilder viewBuilder;
    private ItemSelectedListener itemSelectedListener;
    private ActionInvokerListener<T> actionInvokerListener;
    protected final List<T> items = new ArrayList<T>();

    public RecyclerViewAdapterBase(IViewBuilder<T> viewBuilder, ItemSelectedListener itemSelectedListener){
        this.viewBuilder = viewBuilder;
        this.itemSelectedListener = itemSelectedListener;
    }

    public RecyclerViewAdapterBase(ItemSelectedListener itemSelectedListener){
        this.itemSelectedListener = itemSelectedListener;
    }

    public RecyclerViewAdapterBase(){

    }

    public void setItemSelectedListener(ItemSelectedListener<T> itemSelectedListener){
        this.itemSelectedListener = itemSelectedListener;
    }
    public void setViewBuilder(IViewBuilder<T> viewBuilder){
        this.viewBuilder = viewBuilder;
    }

    public void setItems(List<T> items){
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void invalidate(){
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public final ViewWrapper<V> onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewWrapper<V>(onCreateItemView(parent, viewType));
    }

    protected V onCreateItemView(ViewGroup parent, int viewType){
        final V view = (V) viewBuilder.build(viewType);
        return view;
    }



    public T removeItem(int position) {
        final T item = items.remove(position);
        notifyItemRemoved(position);
        return item;
    }

    public boolean animateTo(List<T> items) {
        applyAndAnimateRemovals(items);
        applyAndAnimateAdditions(items);
        boolean anyMoved = applyAndAnimateMovedItems(items);
        return anyMoved;
    }

    private boolean applyAndAnimateRemovals(List<T> newItems) {
        boolean anyRemoved = false;
        for (int i = items.size() - 1; i >= 0; i--) {
            final T item = items.get(i);
            if (!newItems.contains(item)) {
                removeItem(i);
                anyRemoved = true;
            }
        }
        return anyRemoved;
    }

    private boolean applyAndAnimateAdditions(List<T> newItems) {
        boolean anyAdded = false;
        for (int i = 0, count = newItems.size(); i < count; i++) {
            final T item = newItems.get(i);
            if (!items.contains(item)) {
                addItem(i, item);
                anyAdded = true;
            }
        }
        return anyAdded;
    }

    private boolean applyAndAnimateMovedItems(List<T> newItems) {
        boolean anyMoved = false;
        for (int toPosition = newItems.size() - 1; toPosition >= 0; toPosition--) {
            final T item = newItems.get(toPosition);
            final int fromPosition = items.indexOf(item);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
                anyMoved = true;
            }
        }
        return anyMoved;
    }

    public void addItem(int position, T item) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final T item = items.remove(fromPosition);
        items.add(toPosition, item);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<V> viewHolder, int position) {
        final  V view = viewHolder.getView();
        final T item = items.get(position);
        view.bind(item, position,itemSelectedListener);
        if(actionInvokerListener != null){
            view.bindAction(item,position, actionInvokerListener);
        }
    }

    public void setActionInvokerListener(ActionInvokerListener<T> actionInvokerListener) {
        this.actionInvokerListener = actionInvokerListener;
    }

    // additional methods to manipulate the items

    public interface IViewBuilder<T> {
        <V extends View & IBindable<T>> V build(int viewType);
    }

    public interface IItemsProvider<T>
    {
        List<T> getItems();
    }
}