package it.clipcall.infrastructure.viewModel.support.recycleviews;

import android.os.Bundle;

/**
 * Created by omega on 4/29/2016.
 */
public interface ItemSelectedListener<T> {

    void onItemSelected(T item);
    void onItemSelected(T item, Bundle uiData);
    void onItemSelected(T item, Bundle uiData, Bundle data);
}
