package it.clipcall.infrastructure.viewModel.support.recycleviews;


import it.clipcall.infrastructure.support.recyclerviews.ActionInvokerListener;

public interface IActionBindable<T> {

    void bindAction(T entity, int position, ActionInvokerListener listener);
}
