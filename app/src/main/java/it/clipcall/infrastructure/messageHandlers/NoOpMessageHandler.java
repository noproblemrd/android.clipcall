package it.clipcall.infrastructure.messageHandlers;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by dorona on 07/03/2016.
 */

@Singleton
public class NoOpMessageHandler implements IMessageHandler {

    private static final String TAG = "MessageHandler";

    @Inject
    public NoOpMessageHandler(){

    }

    @Override
    public boolean canHandlerMessage(String message) {
        return true;
    }

    @Override
    public void handleForegroundMessage(String from, Bundle data, Activity currentActivity) {
        Log.d(TAG, "handleForegroundMessage: from: "+ from + ", bundle: "+ data.toString());
    }

    @Override
    public void handleBackgroundMessage(String from, Bundle data, Application application) {
        Log.d(TAG, "handleBackgroundMessage: from: "+ from + ", bundle: "+ data.toString());
    }
}
