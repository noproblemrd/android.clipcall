package it.clipcall.infrastructure.messageHandlers;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/**
 * Created by dorona on 07/03/2016.
 */
public interface IMessageHandler {

    boolean canHandlerMessage(String message);
    void handleForegroundMessage(String from, Bundle data, Activity activeActivity);
    void handleBackgroundMessage(String from, Bundle data, Application application);
}
