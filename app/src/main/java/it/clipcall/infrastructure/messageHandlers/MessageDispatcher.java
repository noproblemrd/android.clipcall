package it.clipcall.infrastructure.messageHandlers;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import com.google.common.base.Strings;

import java.util.List;

import javax.inject.Singleton;


@Singleton
public class MessageDispatcher {

    private static final String TAG = "GCM-Messages";
    private final NoOpMessageHandler noOpMessageHandler = new NoOpMessageHandler();
    private final Application application;
    private final List<IMessageHandler> messageHandlers;

    public MessageDispatcher(Application application, List<IMessageHandler> messageHandlers){
        this.application = application;
        this.messageHandlers = messageHandlers;
    }


    public void handleMessage(String from, Bundle data, Activity currentActivity){
        Log.d(TAG, String.format("handleMessage - from %s , data : %s",from,data));
        String message = data.getString("service");
        if(Strings.isNullOrEmpty(message))
            return;



        for (IMessageHandler messageHandler : messageHandlers){
            if(messageHandler.canHandlerMessage(message))
            {
                if(currentActivity != null){
                    messageHandler.handleForegroundMessage(from, data, currentActivity);
                    return;
                }else{
                    messageHandler.handleBackgroundMessage(from, data, application);
                    return;
                }

            }
        }
    }







}
