package it.clipcall.infrastructure.features;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;

@Singleton
public class ApplicationFeaturesService {


    private final List<ApplicationFeature> applicationFeatures = new ArrayList<>();


    @Inject
    public ApplicationFeaturesService(){

    }

    public void addFeature(ApplicationFeature feature){
        applicationFeatures.add(feature);
    }

    public boolean isFeatureEnabled(final ApplicationFeature feature){
        return Lists.any(applicationFeatures, new ICriteria<ApplicationFeature>() {
            @Override
            public boolean isSatisfiedBy(ApplicationFeature candidate) {
                return candidate.getFeatureName().equals(feature.getFeatureName()) && candidate.isFeatureEnabled();
            }
        });
    }
}
