package it.clipcall.infrastructure.features;

/**
 * Created by dorona on 12/07/2016.
 */
public class ApplicationFeature {

    String featureName;
    boolean featureEnabled;

    public ApplicationFeature(String featureName, boolean featureEnabled){

        this.featureName = featureName;
        this.featureEnabled = featureEnabled;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public boolean isFeatureEnabled() {
        return featureEnabled;
    }

    public void setFeatureEnabled(boolean featureEnabled) {
        this.featureEnabled = featureEnabled;
    }
}
