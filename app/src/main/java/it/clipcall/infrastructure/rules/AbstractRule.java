package it.clipcall.infrastructure.rules;

public abstract class AbstractRule extends AbstractComponent {
    private AbstractComponent positiveOutcomeStep;
    private AbstractComponent negativeOutcomeStep;

    public void execute(Object arg) throws Exception {
        boolean outcome = makeDecision(arg);
        if (outcome)
            positiveOutcomeStep.execute(arg);
        else
            negativeOutcomeStep.execute(arg);
    }

    protected abstract boolean makeDecision(Object arg) throws Exception;

    public void setPositiveOutcomeStep(AbstractComponent positiveOutcomeStep) {
        this.positiveOutcomeStep = positiveOutcomeStep;
    }

    public void setNegativeOutcomeStep(AbstractComponent negativeOutcomeStep) {
        this.negativeOutcomeStep = negativeOutcomeStep;
    }
}