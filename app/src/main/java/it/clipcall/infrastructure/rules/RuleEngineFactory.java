package it.clipcall.infrastructure.rules;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.common.validation.activities.PhoneValidationActivity_;
import it.clipcall.infrastructure.routing.models.RouteNames;
import it.clipcall.infrastructure.rules.navigation.NavigationAction;
import it.clipcall.infrastructure.rules.navigation.RedirectToPhoneValidationRule;

import it.clipcall.common.invitations.services.BranchIoManager;

/**
 * Created by dorona on 29/12/2015.
 */
public class RuleEngineFactory {

    private final BranchIoManager branchIoManager;
    private final AuthenticationManager authenticationManager;

    public RuleEngineFactory(BranchIoManager branchIoManager, AuthenticationManager authenticationManager){

        this.branchIoManager = branchIoManager;
        this.authenticationManager = authenticationManager;
    }

    public RuleEngine getRuleEngine(){

        RuleEngine ruleEngine = new RuleEngine();


        String[] routesRequiringPhoneValidation = new String[]{RouteNames.ConsumerValidationRoute};
        RedirectToPhoneValidationRule redirectToPhoneValidationRule = new RedirectToPhoneValidationRule(authenticationManager,routesRequiringPhoneValidation);
        redirectToPhoneValidationRule.setPositiveOutcomeStep(new NavigationAction(PhoneValidationActivity_.class));
        redirectToPhoneValidationRule.setNegativeOutcomeStep(new NavigationAction(PhoneValidationActivity_.class));


//        StartVideoChatRule startVideoChatRule = new StartVideoChatRule(branchIoManager);
//
//
//        ConsumerModeRule consumerModeRule = new ConsumerModeRule();
//        consumerModeRule.setPositiveOutcomeStep(new NavigationAction(ConsumerMainMenuActivity_.class));
//        consumerModeRule.setNegativeOutcomeStep(new NavigationAction(MainMenuActivity_.class));
//
//        startVideoChatRule.setPositiveOutcomeStep(new NavigationAction(ConsumerStartVideoChatActivity_.class));
//        startVideoChatRule.setNegativeOutcomeStep(consumerModeRule);
//
//        ruleEngine.setFirstStep(startVideoChatRule);

        return ruleEngine;
    }
}
