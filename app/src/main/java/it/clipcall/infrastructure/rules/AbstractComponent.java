package it.clipcall.infrastructure.rules;

/**
 * Created by dorona on 29/12/2015.
 */
public abstract class AbstractComponent {
    public abstract void execute(Object arg) throws Exception;
}
