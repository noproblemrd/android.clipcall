package it.clipcall.infrastructure.rules.navigation;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.rules.AbstractAction;


public class NavigationAction extends AbstractAction{

    private final Class<? extends BaseActivity> activityClass;
    private final Handler handler = new Handler();

    public NavigationAction(Class<? extends BaseActivity> activityClass) {
        this.activityClass = activityClass;
    }


    @Override
    protected void doExecute(Object arg) throws Exception {
        NavigationContext navigationContext = (NavigationContext)arg;
        RoutingContext routingContext = (RoutingContext)navigationContext.routingContext;
        final Context context = routingContext.getContext();

        handler.post(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(context, activityClass);
                context.startActivity(intent);
            }
        });

    }
}
