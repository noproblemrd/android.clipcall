package it.clipcall.infrastructure.rules.navigation;

import it.clipcall.common.invitations.models.eInvitationType;
import it.clipcall.infrastructure.rules.AbstractRule;
import it.clipcall.common.invitations.services.BranchIoManager;

/**
 * Created by dorona on 29/12/2015.
 */
//public class StartVideoChatRule extends AbstractRule {
//
//
//    private final BranchIoManager branchIoManager;
//
//    public StartVideoChatRule(BranchIoManager branchIoManager){
//        this.branchIoManager = branchIoManager;
//    }
//
//    @Override
//    protected boolean makeDecision(Object arg) throws Exception {
//        return branchIoManager.hasInvitation(eInvitationType.Unknown);
//    }
//}
