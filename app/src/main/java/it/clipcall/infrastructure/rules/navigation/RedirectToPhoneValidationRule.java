package it.clipcall.infrastructure.rules.navigation;

import java.util.Arrays;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.rules.AbstractRule;

public class RedirectToPhoneValidationRule extends AbstractRule {


    private AuthenticationManager authenticationManager;
    private final String[] routeNames;

    public RedirectToPhoneValidationRule(AuthenticationManager authenticationManager,String...routeNames){
        this.authenticationManager = authenticationManager;
        this.routeNames = routeNames;
    }



    @Override
    protected boolean makeDecision(Object arg) throws Exception {
        if(!(arg instanceof NavigationContext))
            return false;

        if(authenticationManager.isCustomerAuthenticated())
            return false;

        NavigationContext navigationContext = (NavigationContext)arg;
        String destinationRouteName = navigationContext.destinationRouteName;
        boolean shouldBeValidated = Arrays.asList(routeNames).contains(destinationRouteName);
        if(!shouldBeValidated)
            return false;

        return true;

    }
}
