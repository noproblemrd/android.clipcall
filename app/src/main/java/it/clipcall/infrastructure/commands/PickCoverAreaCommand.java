package it.clipcall.infrastructure.commands;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.os.ResultReceiver;

import org.parceler.Parcels;

import it.clipcall.infrastructure.activities.TransparentActivity;
import it.clipcall.professional.profile.models.Area;
import it.clipcall.professional.profile.models.CoverArea;

/**
 * Created by omega on 4/8/2016.
 */
public class PickCoverAreaCommand implements IAsyncCommand<Area> {
    private final Activity context;
    private final CoverArea coverArea;

    public PickCoverAreaCommand(Activity context, CoverArea coverArea){
        this.context = context;
        this.coverArea = coverArea;
    }

    @Override
    public void execute(ICallBack<Area> callBack) {

        Intent intent = new Intent(context, TransparentActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("coverArea",Parcels.wrap(coverArea));
        intent.putExtra("receiver",new PickCoverAreaResultReceiver(null,callBack));
        intent.putExtra("action", TransparentActivity.PickCoverAreaAction);
        context.startActivity(intent);
    }

    @SuppressLint("ParcelCreator")
    private class PickCoverAreaResultReceiver extends ResultReceiver{

        private final ICallBack<Area> callBack;

        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler
         */
        public PickCoverAreaResultReceiver(Handler handler, ICallBack<Area> callBack) {
            super(handler);

            this.callBack = callBack;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            Parcelable area = resultData.getParcelable("area");
            if(area == null){
                callBack.onFailure();
                return;
            }
            Area selectedArea = Parcels.unwrap(area);
            if(selectedArea == null){
                callBack.onFailure();
                return;
            }

            callBack.onSuccess(selectedArea);
        }
    }


}
