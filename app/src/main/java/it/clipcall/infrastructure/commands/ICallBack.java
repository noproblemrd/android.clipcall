package it.clipcall.infrastructure.commands;

/**
 * Created by omega on 4/8/2016.
 */
public interface ICallBack<T> {
    void onSuccess(T result);

    void onFailure();
}
