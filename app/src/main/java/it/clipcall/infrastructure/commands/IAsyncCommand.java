package it.clipcall.infrastructure.commands;

/**
 * Created by omega on 4/8/2016.
 */
public interface IAsyncCommand<T> {

    void execute(ICallBack<T> callBack);
}
