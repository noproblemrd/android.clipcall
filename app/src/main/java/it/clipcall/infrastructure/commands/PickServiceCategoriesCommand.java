package it.clipcall.infrastructure.commands;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.os.ResultReceiver;

import org.parceler.Parcels;

import java.util.List;

import it.clipcall.infrastructure.activities.TransparentActivity;
import it.clipcall.professional.profile.models.Category;

/**
 * Created by omega on 4/8/2016.
 */
public class PickServiceCategoriesCommand implements IAsyncCommand<List<Category>> {
    private final Activity context;
    private final List<Category> categories;

    public PickServiceCategoriesCommand(Activity context, List<Category> categories){
        this.context = context;
        this.categories = categories;
    }

    @Override
    public void execute(ICallBack<List<Category>> callBack) {

        Intent intent = new Intent(context, TransparentActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("categories",Parcels.wrap(categories));
        intent.putExtra("receiver",new PickServiceCategoriesResultReceiver(null,callBack));
        intent.putExtra("action", TransparentActivity.PickCategoriesAction);
        context.startActivity(intent);
    }

    @SuppressLint("ParcelCreator")
    private class PickServiceCategoriesResultReceiver extends ResultReceiver{

        private final ICallBack<List<Category>> callBack;

        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler
         */
        public PickServiceCategoriesResultReceiver(Handler handler, ICallBack<List<Category>> callBack) {
            super(handler);

            this.callBack = callBack;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            Parcelable categories = resultData.getParcelable("categories");
            if(categories == null){
                callBack.onFailure();
                return;
            }
            List<Category>  selectedCategories = Parcels.unwrap(categories);
            if(selectedCategories == null){
                callBack.onFailure();
                return;
            }

            callBack.onSuccess(selectedCategories);
        }
    }


}
