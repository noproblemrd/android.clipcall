package it.clipcall.infrastructure.commands;

/**
 * Created by omega on 4/8/2016.
 */
public abstract class AsyncCommandDecorator<T,U> implements IAsyncCommand<U> {

    protected final IAsyncCommand<U> decoratedCommand;

    public AsyncCommandDecorator(IAsyncCommand<U> decoratedCommand){

        this.decoratedCommand = decoratedCommand;
    }



    public abstract void execute(ICallBack<U> callBack);
}
