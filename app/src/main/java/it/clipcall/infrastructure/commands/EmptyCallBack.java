package it.clipcall.infrastructure.commands;

/**
 * Created by omega on 4/8/2016.
 */
public class EmptyCallBack<T> implements ICallBack<T> {
    @Override
    public void onSuccess(T result) {

    }

    @Override
    public void onFailure() {

    }
}
