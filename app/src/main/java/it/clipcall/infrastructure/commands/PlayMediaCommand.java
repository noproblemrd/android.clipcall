package it.clipcall.infrastructure.commands;

import android.content.Context;
import android.media.MediaPlayer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PlayMediaCommand implements ICommand, MediaPlayer.OnCompletionListener {

    private MediaPlayer mediaPlayer;

    public PlayMediaCommand(Context context, byte[] audioData){
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnCompletionListener(this);
            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
            String fileName = "audio_" + timeStampFormat.format(new Date());
            File tempMp3  = File.createTempFile(fileName, ".mp4", context.getCacheDir());
            tempMp3.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(tempMp3);
            fos.write(audioData);
            fos.close();
            FileInputStream fis = new FileInputStream(tempMp3);
            mediaPlayer.setDataSource(fis.getFD());
            mediaPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
            mediaPlayer = null;
        }
    }

    @Override
    public void execute() {
        toggleAudioPlay();
    }

    public boolean isPlaying(){
        return mediaPlayer != null && mediaPlayer.isPlaying();
    }

    private void toggleAudioPlay() {
        if(mediaPlayer == null)
            return;

        if(mediaPlayer.isPlaying())
            pause();
        else
            play();
    }

    private void play(){
        try{
            mediaPlayer.start();

        }
        catch(Exception e){
            mediaPlayer = null;
        }
    }

    private void pause(){
        try{
            mediaPlayer.pause();
        }
        catch(Exception e){
            mediaPlayer = null;
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mp.seekTo(0);
    }
}
