package it.clipcall.infrastructure.commands;

import android.content.Context;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.DialogOnAnyDeniedMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import it.clipcall.R;
import it.clipcall.infrastructure.ui.permissions.DefaultPermissionListener;
import it.clipcall.infrastructure.ui.permissions.EmptyPermissionsHandler;

/**
 * Created by omega on 4/8/2016.
 */
public class RequestPermissionsCommand implements IAsyncCommand<Void> {

    private final Context context;
    private final String title;
    private final String message;
    private final String[] permissions;

    public RequestPermissionsCommand(Context context, String title, String message, String ... permissions){
        this.context = context;
        this.title = title;
        this.message = message;
        this.permissions = permissions;
    }
    @Override
    public void execute(final ICallBack<Void> callBack) {


        MultiplePermissionsListener dialogMultiplePermissionsListener =
                DialogOnAnyDeniedMultiplePermissionsListener.Builder
                        .withContext(context)
                        .withTitle(title)
                        .withMessage(message)
                        .withButtonText(android.R.string.ok)
                        .withIcon(R.drawable.clipcall_logo)
                        .build();

        final EmptyPermissionsHandler handler = new EmptyPermissionsHandler(){

            @Override
            public void handleAllPermissionsGranted() {
                callBack.onSuccess(null);
            }

            @Override
            public void handlePermissionDenied() {
                callBack.onFailure();
            }
        };

        DefaultPermissionListener defaultPermissionListener = new DefaultPermissionListener(handler);
        CompositeMultiplePermissionsListener compositeMultiplePermissionsListener = new CompositeMultiplePermissionsListener( dialogMultiplePermissionsListener, defaultPermissionListener);

        try{
            Dexter.checkPermissions(compositeMultiplePermissionsListener, permissions);
        }
        catch (Exception e) {
            callBack.onFailure();
        }
    }


}
