package it.clipcall.infrastructure.commands;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.os.ResultReceiver;

import org.parceler.Parcels;

import it.clipcall.infrastructure.activities.TransparentActivity;
import it.clipcall.infrastructure.maps.entities.PlaceData;

/**
 * Created by omega on 4/8/2016.
 */
public class PickGoogleAddressCommand implements IAsyncCommand<PlaceData> {
    private final Activity context;

    public PickGoogleAddressCommand(Activity context){
       this.context = context;
    }

    @Override
    public void execute(ICallBack<PlaceData> callBack) {

        Intent intent = new Intent(context, TransparentActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("receiver",new PickGoogleAddress(null,callBack));
        intent.putExtra("action", TransparentActivity.PickAddressAction);
        context.startActivity(intent);
    }

    @SuppressLint("ParcelCreator")
    private class PickGoogleAddress extends ResultReceiver{

        private final ICallBack<PlaceData> callBack;

        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler
         */
        public PickGoogleAddress(Handler handler, ICallBack<PlaceData> callBack) {
            super(handler);

            this.callBack = callBack;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            Parcelable place = resultData.getParcelable("place");
            if(place == null){
                callBack.onFailure();
                return;
            }
            PlaceData placeData = Parcels.unwrap(place);
            if(placeData == null){
                callBack.onFailure();
                return;
            }

            callBack.onSuccess(placeData);
        }
    }


}
