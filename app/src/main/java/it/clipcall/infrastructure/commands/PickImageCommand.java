package it.clipcall.infrastructure.commands;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.os.ResultReceiver;

import it.clipcall.infrastructure.activities.TransparentActivity;

/**
 * Created by omega on 4/8/2016.
 */
public class PickImageCommand implements IAsyncCommand<Bitmap> {
    private final Activity context;
    private final int source;

    public PickImageCommand(Activity context, int source){
       this.context = context;
        this.source = source;
    }

    @Override
    public void execute(ICallBack<Bitmap> callBack) {
        Intent intent = new Intent(context, TransparentActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("receiver",new PickImageResultReceiver(null,callBack));
        intent.putExtra("source", source);
        intent.putExtra("action", TransparentActivity.PickImageAction);
        context.startActivity(intent);
    }

    @SuppressLint("ParcelCreator")
    private class PickImageResultReceiver extends ResultReceiver{

        private final ICallBack<Bitmap> callBack;

        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler
         */
        public PickImageResultReceiver(Handler handler, ICallBack<Bitmap> callBack) {
            super(handler);

            this.callBack = callBack;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            Parcelable image = resultData.getParcelable("image");
            if(image == null){
                callBack.onFailure();
                return;
            }

            callBack.onSuccess((Bitmap)image);
        }
    }


}
