package it.clipcall.infrastructure.commands;

import android.media.AudioManager;
import android.media.MediaPlayer;

/**
 * Created by dorona on 11/04/2016.
 */
public class PlayMediaFromUrlCommand implements ICommand {

    private MediaPlayer mediaPlayer;
    public PlayMediaFromUrlCommand(String url){
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
            mediaPlayer = null;
        }
    }

    public void setOnCompletionListener( MediaPlayer.OnCompletionListener onCompletionListener){
        if(mediaPlayer != null){
            mediaPlayer.setOnCompletionListener(onCompletionListener);
        }
    }

    @Override
    public void execute() {
        toggleAudioPlay();
    }

    public boolean isPlaying(){
        return mediaPlayer != null && mediaPlayer.isPlaying();
    }

    private void toggleAudioPlay() {
        if(mediaPlayer == null)
            return;

        if(mediaPlayer.isPlaying())
            pause();
        else
            play();
    }

    private void play(){
        try{
            mediaPlayer.start();

        }
        catch(Exception e){
            mediaPlayer = null;
        }
    }

    public void reset(){
        if(mediaPlayer != null){
            mediaPlayer.seekTo(0);
        }
    }

    private void pause(){
        try{
            mediaPlayer.pause();
        }
        catch(Exception e){
            mediaPlayer = null;
        }
    }

    public void shutDown() {
        if(mediaPlayer != null)
            mediaPlayer = null;
    }
}
