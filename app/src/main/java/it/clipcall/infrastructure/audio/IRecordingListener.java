package it.clipcall.infrastructure.audio;

/**
 * Created by dorona on 07/03/2016.
 */
public interface IRecordingListener {

    void onTimePassed(int totalTime);
}
