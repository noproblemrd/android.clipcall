package it.clipcall.infrastructure.audio;

import android.media.MediaRecorder;
import android.os.Environment;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import it.clipcall.infrastructure.di.scopes.PerActivity;


@PerActivity
public class SimpleMediaRecorder {

    private MediaRecorder recorder;
    private String fileName;

    private Timer timer;

    private int totalTimeRecorded;


    @Inject
    public SimpleMediaRecorder(){

    }

    public boolean startRecording(final IRecordingListener recordingListener)
    {
        try {

            if(recorder != null)
                return false;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat(
                    "yyyy-MM-dd-HH.mm.ss");
            fileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/audio_" + timeStampFormat.format(new Date()) + ".mp4";
            recorder = new MediaRecorder();
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            recorder.setOutputFile(fileName);
            recorder.prepare();
            recorder.start();

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    totalTimeRecorded++;
                    recordingListener.onTimePassed(totalTimeRecorded);
                }
            }, 0, 1000);

            return true;
        } catch (Exception e) {
            recorder = null;
            e.printStackTrace();
            releaseTimer();
            return false;
        }

    }

    public boolean stopRecording(){
       try{
           totalTimeRecorded = 0;
           recorder.stop();
           recorder.release();
           recorder = null;
           releaseTimer();

           return true;
       }
       catch (Exception e){
           recorder = null;
           releaseTimer();
           return false;
       }
    }

    public String getLastRecordingUri(){
        return fileName;
    }


    private void releaseTimer(){
        if(timer != null){
            timer.cancel();
            timer = null;
        }
    }

    public void cancelRecording() {
        this.stopRecording();

        fileName = "";
    }
}
