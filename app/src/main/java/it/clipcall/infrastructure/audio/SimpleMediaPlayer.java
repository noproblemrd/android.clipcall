package it.clipcall.infrastructure.audio;


import android.app.Activity;
import android.media.MediaPlayer;

import javax.inject.Inject;

import it.clipcall.infrastructure.di.scopes.PerActivity;

@PerActivity
public class SimpleMediaPlayer {

    private final Activity activity;
    private boolean isLooping;

    private MediaPlayer mediaPlayer;

    @Inject
    public SimpleMediaPlayer(Activity activity){

        this.activity = activity;
    }

    public void setIsLooping(boolean isLooping){
        this.isLooping = isLooping;
    }

    public void setAudioTrack(int resourceId){
        mediaPlayer = MediaPlayer.create(activity, resourceId);
        mediaPlayer.setLooping(isLooping);
    }

    public void play(){
        if(mediaPlayer == null)
            return;

        mediaPlayer.start();
    }

    public void pause(){
        if(mediaPlayer == null)
            return;

        mediaPlayer.pause();
    }

    public void stop(){
        if(mediaPlayer == null)
            return;

        mediaPlayer.stop();
        mediaPlayer = null;
    }
}
