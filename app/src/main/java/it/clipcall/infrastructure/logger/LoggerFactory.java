package it.clipcall.infrastructure.logger;

/**
 * Created by dorona on 18/05/2016.
 */
public class LoggerFactory {

    public static ILogger getLogger(String name){
        return new SimpleLogger2(name);
    }
}
