package it.clipcall.infrastructure.logger;

import org.slf4j.Logger;

/**
 * Created by dorona on 18/05/2016.
 */
public class SimpleLogger3 implements ILogger {

    private final Logger logger;

    public SimpleLogger3(Class clazz){
        logger = org.slf4j.LoggerFactory.getLogger(clazz.getSimpleName());
    }

    @Override
    public void debug(String message) {
        logger.debug(message);
    }

    @Override
    public void debug(String format, Object... params) {
        logger.debug(format, params);
    }

    @Override
    public void warning(String format, Object... params) {

    }

    @Override
    public void error(String message, Throwable error) {
        logger.error(message, error);
    }

    @Override
    public void error(String message) {
        logger.error(message);
    }

    @Override
    public void error(String format, Object... params) {

    }
}
