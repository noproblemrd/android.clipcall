package it.clipcall.infrastructure.logger;

import android.util.Log;

/**
 * Created by dorona on 18/05/2016.
 */
public class SimpleLogger2 implements ILogger {


    private final String className;

    public SimpleLogger2(String className){

        this.className = className;
    }

    private String ClipCallTag = "ClipCall";

    @Override
    public void debug(String message) {
        Log.d(ClipCallTag,buildFinalMessage(message));
    }

    @Override
    public void debug(String format, Object... params) {
        String message = String.format(format,params);
        Log.d(ClipCallTag,buildFinalMessage(message));
    }

    @Override
    public void warning(String format, Object... params) {
        String message = String.format(format,params);
        Log.w(ClipCallTag,buildFinalMessage(message));
    }

    @Override
    public void error(String message, Throwable error) {
        Log.e(ClipCallTag,buildFinalMessage(message),error);
    }

    @Override
    public void error(String message) {
        Log.e(ClipCallTag,buildFinalMessage(message));
    }

    @Override
    public void error(String format, Object... params) {
        String message = String.format(format,params);
        Log.e(ClipCallTag,buildFinalMessage(message));
    }

    private String buildFinalMessage(String message){
        String finalMessage = String.format("%s - %s",className,message);
        return finalMessage;
    }
}
