package it.clipcall.infrastructure.logger;

import android.util.Log;

/**
 * Created by dorona on 18/05/2016.
 */
public class SimpleLogger implements ILogger {


    private String ClipCallTag = "ClipCall";

    @Override
    public void debug(String message) {
        Log.d(ClipCallTag,message);
    }

    @Override
    public void debug(String format, Object... params) {
        String message = String.format(format,params);
        Log.d(ClipCallTag,message);
    }

    @Override
    public void warning(String format, Object... params) {
        String message = String.format(format,params);
        Log.w(ClipCallTag,message);
    }

    @Override
    public void error(String message, Throwable error) {
        Log.e(ClipCallTag,message,error);
    }

    @Override
    public void error(String message) {
        Log.e(ClipCallTag,message);
    }

    @Override
    public void error(String format, Object... params) {
        String message = String.format(format,params);
        Log.e(ClipCallTag, message);
    }
}
