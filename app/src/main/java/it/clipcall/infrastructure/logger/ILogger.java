package it.clipcall.infrastructure.logger;

/**
 * Created by dorona on 18/05/2016.
 */
public interface ILogger {

    void debug(String message);
    void debug(String format, Object...params);
    void warning(String format, Object...params);
    void error(String message);
    void error(String format, Object...params);
    void error(String message, Throwable error);
}
