package it.clipcall.infrastructure.gcm.services;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.common.base.Strings;

import org.simple.eventbus.EventBus;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.models.GcmToken;

@Singleton
public class GcmTokenProvider {

    private final String senderId = "348674619996";

    private static final String TAG = "GCM";

    private String gcmToken;

    @Inject
    public GcmTokenProvider(){

    }

    public void initialize(Context applicationContext){
        InstanceID instanceID = InstanceID.getInstance(applicationContext);
        try {
            this.gcmToken = instanceID.getToken(senderId, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            if(!Strings.isNullOrEmpty(gcmToken)){
                EventBus.getDefault().post(new GcmToken(gcmToken));
            }

            Log.d(TAG, "run: gcm token: "+ gcmToken);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "failed generating token with exception: "+ e.getMessage());
        }

    }

    public String getGcmToken() {
        return gcmToken;
    }
}

