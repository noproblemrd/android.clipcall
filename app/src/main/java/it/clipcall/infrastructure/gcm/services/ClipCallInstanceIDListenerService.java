package it.clipcall.infrastructure.gcm.services;

import android.os.AsyncTask;

import com.google.android.gms.iid.InstanceIDListenerService;

import javax.inject.Inject;

import it.clipcall.ClipCallApplication;
import it.clipcall.consumer.profile.controllers.CustomerProfileController;


public class ClipCallInstanceIDListenerService extends InstanceIDListenerService {
    @Inject
    CustomerProfileController profileController;

    @Inject
    GcmTokenProvider gcmTokenProvider;

    @Override
    public void onCreate() {
        super.onCreate();
        ClipCallApplication application = (ClipCallApplication)getApplication();
        application.getComponent().inject(this);
    }

    @Override
    public void onTokenRefresh() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                gcmTokenProvider.initialize(getApplicationContext());
                //
            }
        });
    }
}
