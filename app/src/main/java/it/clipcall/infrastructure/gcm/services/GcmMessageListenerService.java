package it.clipcall.infrastructure.gcm.services;

import android.os.Bundle;

import com.google.android.gms.gcm.GcmListenerService;

import org.androidannotations.annotations.EService;

import javax.inject.Inject;

import it.clipcall.ClipCallApplication;
import it.clipcall.infrastructure.messageHandlers.MessageDispatcher;

@EService
public class GcmMessageListenerService extends GcmListenerService {

    @Inject
    MessageDispatcher messageDispatcher;

    @Override
    public void onCreate() {
        super.onCreate();
        ClipCallApplication application = (ClipCallApplication)getApplication();
        application.getComponent().inject(this);
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        ClipCallApplication application = (ClipCallApplication)getApplication();
        if(application == null)
            return;

        messageDispatcher.handleMessage(from, data,application.getCurrentActivity());
    }
}