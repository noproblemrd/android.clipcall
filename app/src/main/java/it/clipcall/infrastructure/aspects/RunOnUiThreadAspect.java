package it.clipcall.infrastructure.aspects;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import it.clipcall.infrastructure.presenters.IPresenter;

@Aspect
public class RunOnUiThreadAspect {

    private final Handler handler = new Handler(Looper.getMainLooper());

    @Pointcut("(call(public void it.clipcall.infrastructure.views.IView+.*(..)) || call(public void it.clipcall.infrastructure.routing.services.RoutingService.*(..)) || call(public void it.clipcall.infrastructure.ui.images.services.ImagePickerService.*(..)))  && this(presenter)")
    public void viewOperations(IPresenter presenter){}


    @Around("viewOperations(presenter)")
    public void viewOperations(final ProceedingJoinPoint thisJoinPoint, final IPresenter presenter){

        final Runnable action = new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d("UIThread", thisJoinPoint.toShortString());
                    thisJoinPoint.proceed(new Object[]{presenter});
                } catch (Throwable throwable) {
                    Log.e("UIThread",throwable.getMessage(), throwable);
                }
            }
        };

        runOnUiThread(action);


    }

    private final void runOnUiThread(Runnable action) {
        if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
            handler.post(action);
        } else {
            action.run();
        }
    }
}
