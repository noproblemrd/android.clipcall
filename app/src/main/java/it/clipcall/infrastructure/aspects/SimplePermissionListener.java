package it.clipcall.infrastructure.aspects;

import android.util.Log;

import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

/**
 * Created by Omega on 5/24/2016.
 */

public class SimplePermissionListener implements MultiplePermissionsListener {




    @Override
    public void onPermissionsChecked(MultiplePermissionsReport report) {
        if(report.areAllPermissionsGranted()){
            Log.d("permissions", "permissions granted");
        }else{
            Log.d("permissions", "permissions denied");



        }
    }

    @Override
    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
        Log.d("permissions", "permission rationale should be shown");
    }
}
