package it.clipcall.infrastructure.aspects;

import android.util.Log;

import com.google.common.base.Joiner;
import com.google.gson.Gson;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import java.util.Arrays;
import java.util.List;

import it.clipcall.common.environment.Environment;
import it.clipcall.infrastructure.support.collections.IFunction;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.professional.validation.services.AuthenticationManager;


public aspect AuthenticationTracingAspect extends TracingAspect{

    private final Gson gson = new Gson();

    pointcut traced(): execution(public * AuthenticationManager.*(..));



    protected String describe(Object object){
      return gson.toJson(object);
    }


}