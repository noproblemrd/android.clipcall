package it.clipcall.infrastructure.aspects;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import it.clipcall.infrastructure.aspects.SecureMethod;
import com.karumi.dexter.*;
import com.karumi.dexter.listener.*;
import com.karumi.dexter.listener.multi.*;
import it.clipcall.infrastructure.aspects.SimplePermissionListener;
import android.util.Log;
import android.app.Activity;
import java.util.List;

public aspect PermissionAspect{

   pointcut permissions(Activity activity)
     : execution(* *.*(..)) && target(activity);

   void around(final Activity activity, final SecureMethod securedMethod) : permissions(activity) && @annotation(securedMethod){


         MultiplePermissionsListener dialogMultiplePermissionsListener =
             DialogOnAnyDeniedMultiplePermissionsListener.Builder
                 .withContext(activity)
                 .withTitle(securedMethod.title())
                 .withMessage(securedMethod.message())
                 .withButtonText(android.R.string.ok)
                 .build();


         SimplePermissionListener simplePermissionListener = new SimplePermissionListener(){
             @Override
                 public void onPermissionsChecked(MultiplePermissionsReport report) {
                     if(report.areAllPermissionsGranted()){
                         Log.d("permissions", "permissions22 granted");
                         proceed(activity, securedMethod);
                     }else{
                         Log.d("permissions", "permissions22 denied");
                     }
                 }

                 @Override
                 public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions,PermissionToken token){
                    token.continuePermissionRequest();
                 }


         };
         CompositeMultiplePermissionsListener compositeListener = new CompositeMultiplePermissionsListener(dialogMultiplePermissionsListener, simplePermissionListener);

         Log.d("permissions", "about 2 request permissions for: "+java.util.Arrays.toString(securedMethod.permissions()));
         Dexter.checkPermissions(compositeListener, securedMethod.permissions());


   }
}