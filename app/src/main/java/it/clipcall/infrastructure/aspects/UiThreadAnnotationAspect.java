package it.clipcall.infrastructure.aspects;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class UiThreadAnnotationAspect {
    private final Handler handler = new Handler(Looper.getMainLooper());

    @Pointcut("execution(@it.clipcall.infrastructure.aspects.RunOnUiThread * *.*(..))")
    public void viewOperations(){}


    @Around("viewOperations()")
    public void viewOperations(final ProceedingJoinPoint thisJoinPoint){

        final Runnable action = new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d("UIThreadPresenter", thisJoinPoint.toShortString());
                    thisJoinPoint.proceed();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        };

        runOnUiThread(action);
    }

    private final void runOnUiThread(Runnable action) {
        if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
            handler.post(action);
        } else {
            action.run();
        }
    }

}


