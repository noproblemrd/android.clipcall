package it.clipcall.infrastructure.aspects;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SecureMethod {
    public String[] permissions();
    public String title() default  "Permissions888";

    public String message() default "This is a message for the permissions requested";
}