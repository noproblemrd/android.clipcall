package it.clipcall.infrastructure.aspects;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import it.clipcall.infrastructure.aspects.SecureMethod;

import it.clipcall.infrastructure.aspects.SimplePermissionListener;
import android.util.Log;
import android.app.Activity;
import java.util.List;
import it.clipcall.infrastructure.views.IView;
import it.clipcall.infrastructure.presenters.IPresenter;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

public aspect RunOnBackgroundThreadAspect{

   pointcut backgroundOperation()
     : execution(public void IPresenter+.*(..)) && !@annotation(it.clipcall.infrastructure.aspects.RunOnUiThread) ;

   void around() : backgroundOperation(){


    AsyncTask.THREAD_POOL_EXECUTOR.execute(new Runnable() {
               @Override
               public void run() {
                            try{
                                Log.d("ClipCall", String.format("Executing Background Task %s", thisJoinPoint.getSignature().toShortString()));
                                proceed();
                            }
                            catch(Exception e){
                                Log.e("ClipCall", String.format("Executing Background Task %s failed with error %s", thisJoinPoint.getSignature().toShortString(), e.getMessage()), e);
                            }

               }
           });
   }
}