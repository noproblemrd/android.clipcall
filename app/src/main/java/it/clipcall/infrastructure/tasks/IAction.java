package it.clipcall.infrastructure.tasks;

/**
 * Created by micro on 11/13/2015.
 */
public interface IAction<TInput> {
    void execute(TInput input);
    boolean isSuccess();
    void setNextSuccessAction(IAction<TInput> next);
    void setNextFailureAction(IAction<TInput> next);
}
