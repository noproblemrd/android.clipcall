package it.clipcall.infrastructure.tasks;

/**
 * Created by micro on 11/13/2015.
 */
public interface ICommand {

    void execute();
}

