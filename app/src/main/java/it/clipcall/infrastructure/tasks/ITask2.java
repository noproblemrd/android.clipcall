package it.clipcall.infrastructure.tasks;

/**
 * Created by dorona on 30/06/2016.
 */
public interface ITask2<TFirstInput, TSecondInput, TOutput> {
    TOutput execute(TFirstInput firstInput, TSecondInput secondInput);
}
