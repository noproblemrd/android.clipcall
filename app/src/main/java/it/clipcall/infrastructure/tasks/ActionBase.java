package it.clipcall.infrastructure.tasks;

/**
 * Created by micro on 11/14/2015.
 */
public abstract class ActionBase<TInput> implements IAction<TInput> {

    protected IAction<TInput> nextSuccessAction;
    private IAction<TInput> nextFailureAction;

    @Override
    public void setNextSuccessAction(IAction<TInput> nextSuccessAction) {
        this.nextSuccessAction = nextSuccessAction;
    }

    @Override
    public void setNextFailureAction(IAction<TInput> nextFailureAction) {
        this.nextFailureAction = nextFailureAction;
    }


    @Override
    public void execute(TInput input) {
        this.executeCore(input);

        if(!this.isSuccess() && this.nextFailureAction != null)
        {
            this.nextFailureAction.execute(input);
            return;
        }

        if(this.isSuccess() && this.nextSuccessAction != null){
            this.nextSuccessAction.execute(input);
        }
    }


    protected abstract void executeCore(TInput input);
}
