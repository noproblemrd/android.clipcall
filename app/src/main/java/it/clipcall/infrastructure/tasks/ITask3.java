package it.clipcall.infrastructure.tasks;

/**
 * Created by dorona on 30/06/2016.
 */
public interface ITask3<TFirstInput, TSecondInput,TThirdInput, TOutput> {
    TOutput execute(TFirstInput firstInput, TSecondInput secondInput, TThirdInput thirdInput);
}
