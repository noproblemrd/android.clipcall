package it.clipcall.infrastructure.tasks;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by micro on 11/14/2015.
 */
public class CompositeAction<TInput> implements IAction<TInput>{


    private final List<IAction<TInput>> actions = new ArrayList<IAction<TInput>>();
    public void addAction(IAction<TInput> action){
        actions.add(action);
    }


    @Override
    public void execute(TInput input) {
        for (IAction<TInput> action : actions){
            action.execute(input);
        }
    }

    @Override
    public boolean isSuccess() {
        boolean isSuccess = true;
        for (IAction<TInput> action : actions){
            isSuccess = isSuccess && action.isSuccess();
        }
        return isSuccess;
    }

    @Override
    public void setNextSuccessAction(IAction<TInput> next) {

    }

    @Override
    public void setNextFailureAction(IAction<TInput> next) {

    }
}
