package it.clipcall.infrastructure.tasks;

/**
 * Created by micro on 11/14/2015.
 */
public class DecoratedAction<TInput> implements IAction<TInput> {


    private final IAction<TInput> action;

    public DecoratedAction(IAction<TInput> action){

        this.action = action;
    }


    @Override
    public void execute(TInput tInput) {

    }

    @Override
    public boolean isSuccess() {
        return false;
    }

    @Override
    public void setNextSuccessAction(IAction<TInput> next) {

    }

    @Override
    public void setNextFailureAction(IAction<TInput> next) {

    }
}
