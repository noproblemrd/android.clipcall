package it.clipcall.infrastructure.responses;

/**
 * Created by dorona on 14/12/2015.
 */
public class RecordingResponse {

    private long createdAt;

    /// The duration of the archive, in milliseconds.
    private long duration;

    /// The archive ID.
    private String id;

    /// The name of the archive.
    private String name;

    /// The OpenTok API key associated with the archive.
    private int partnerId;

    /// The session ID of the OpenTok session associated with this archive.
    private String sessionId;

    private boolean hasVideo;

    /// Whether the arch; track (true) or not (false).
    private boolean hasAudio;

    private String url;

    public long getCreatedAt() {
        return createdAt;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(int partnerId) {
        this.partnerId = partnerId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public boolean isHasVideo() {
        return hasVideo;
    }

    public void setHasVideo(boolean hasVideo) {
        this.hasVideo = hasVideo;
    }

    public boolean isHasAudio() {
        return hasAudio;
    }

    public void setHasAudio(boolean hasAudio) {
        this.hasAudio = hasAudio;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }
}
