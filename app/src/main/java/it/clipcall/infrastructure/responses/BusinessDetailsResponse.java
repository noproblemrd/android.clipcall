package it.clipcall.infrastructure.responses;

public class BusinessDetailsResponse {

    private Object companyName;
    private Object contactName;
    private Object businessAddress;
    private Double longitude;
    private Double latitude;
    private String mobileNumber;

    /**
     *
     * @return
     * The companyName
     */
    public Object getCompanyName() {
        return companyName;
    }

    /**
     *
     * @param companyName
     * The companyName
     */
    public void setCompanyName(Object companyName) {
        this.companyName = companyName;
    }

    /**
     *
     * @return
     * The contactName
     */
    public Object getContactName() {
        return contactName;
    }

    /**
     *
     * @param contactName
     * The contactName
     */
    public void setContactName(Object contactName) {
        this.contactName = contactName;
    }

    /**
     *
     * @return
     * The businessAddress
     */
    public Object getBusinessAddress() {
        return businessAddress;
    }

    /**
     *
     * @param businessAddress
     * The businessAddress
     */
    public void setBusinessAddress(Object businessAddress) {
        this.businessAddress = businessAddress;
    }

    /**
     *
     * @return
     * The longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     * The longitude
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     * The latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     * The latitude
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     * The mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     *
     * @param mobileNumber
     * The mobileNumber
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

}