package it.clipcall.infrastructure.responses;

public class AdvertiserData {

    private String status;
    private String supplierId;
    private String step;
    private String token;

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The supplierId
     */
    public String getSupplierId() {
        return supplierId;
    }

    /**
     *
     * @param supplierId
     * The supplierId
     */
    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    /**
     *
     * @return
     * The step
     */
    public String getStep() {
        return step;
    }

    /**
     *
     * @param step
     * The step
     */
    public void setStep(String step) {
        this.step = step;
    }

    /**
     *
     * @return
     * The token
     */
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     * The token
     */
    public void setToken(String token) {
        this.token = token;
    }

}