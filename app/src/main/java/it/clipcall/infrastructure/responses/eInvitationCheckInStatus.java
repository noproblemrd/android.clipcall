package it.clipcall.infrastructure.responses;

public enum eInvitationCheckInStatus
{
    success(0),
    notAvailable(1),
    failed(2),
    failedToFindSupplier(3);


    private final int key;

    eInvitationCheckInStatus(int key) {
        this.key = key;
    }

    public int getKey() {
        return this.key;
    }

    public static eInvitationCheckInStatus fromKey(int key) {
        for(eInvitationCheckInStatus type : eInvitationCheckInStatus.values()) {
            if(type.getKey() == key) {
                return type;
            }
        }
        return null;
    }
}