package it.clipcall.infrastructure.responses;

/**
 * Created by dorona on 14/12/2015.
 */
public class StartVideoChatConversationResponse {

    private int apiKey;
    private String sessionId;
    private String token;

    public int getApiKey() {
        return apiKey;
    }

    public void setApiKey(int apiKey) {
        this.apiKey = apiKey;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
