package it.clipcall.infrastructure.responses;

/**
 * Created by dorona on 28/12/2015.
 */
public class ValidateInvitationResponse {

    private String supplierName ;
    private String text;
    private String supplierImage;
    private eInvitationCheckInStatus checkInStatus ;

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSupplierImage() {
        return supplierImage;
    }

    public void setSupplierImage(String supplierImage) {
        this.supplierImage = supplierImage;
    }

    public eInvitationCheckInStatus getCheckInStatus() {
        return checkInStatus;
    }

    public void setCheckInStatus(eInvitationCheckInStatus checkInStatus) {
        this.checkInStatus = checkInStatus;
    }
}
