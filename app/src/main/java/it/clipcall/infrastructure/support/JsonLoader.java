package it.clipcall.infrastructure.support;

import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by dorona on 01/12/2015.
 */
public class JsonLoader {


    private final AssetManager assetManager;

    @Inject
    @Singleton
    public JsonLoader(AssetManager assetManager){
        this.assetManager = assetManager;
    }


    public String loadJSONFromAsset(String fileName) {
        String json = null;
        try {
            InputStream is = assetManager.open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
