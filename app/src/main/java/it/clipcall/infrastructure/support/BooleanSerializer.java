package it.clipcall.infrastructure.support;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by dorona on 23/02/2016.
 */
public class BooleanSerializer implements JsonDeserializer<Boolean> {
    @Override
    public Boolean deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        ParseBoolResult parseStringResult = parseString(json);
        if(parseStringResult.success)
            return parseStringResult.value;

        ParseBoolResult parseBoolResult = parseBool(json);
        if(parseBoolResult.success)
            return parseBoolResult.value;

        ParseBoolResult parseIntResult = parseInt(json);
        if(parseIntResult.success)
            return parseIntResult.value;

        return false;
    }

    private ParseBoolResult parseInt(JsonElement json){
        try{
            int intValue = json.getAsInt();
            boolean value = intValue == 1 ? true : false;
            return new ParseBoolResult(true,value);
        }
        catch (Exception e){
            return  new ParseBoolResult(false,false);
        }
    }


    private ParseBoolResult parseBool(JsonElement json){
        try{
            boolean value = json.getAsBoolean();
            return new ParseBoolResult(true,value);
        }
        catch (Exception e){
            return  new ParseBoolResult(false,false);
        }
    }

    private ParseBoolResult parseString(JsonElement json){
        try{
            String stringValue = json.getAsString();
            if("1".equals(stringValue))
                return  new ParseBoolResult(true,true);

            if("0".equals(stringValue))
                return  new ParseBoolResult(true,false);

            return new ParseBoolResult(false,false);
        }
        catch (Exception e){
            return  new ParseBoolResult(false,false);
        }
    }

    private class ParseBoolResult{

        public ParseBoolResult(boolean success, boolean value){
            this.success = success;
            this.value = value;
        }

        public boolean success;
        public boolean value;
    }
}
