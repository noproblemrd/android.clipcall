package it.clipcall.infrastructure.support.collections;

/**
 * Created by omega on 4/8/2016.
 */
public interface IFunction<T,U> {

    U apply(T input);

}
