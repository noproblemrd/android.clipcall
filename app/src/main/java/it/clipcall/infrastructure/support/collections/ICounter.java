package it.clipcall.infrastructure.support.collections;

/**
 * Created by dorona on 04/04/2016.
 */
public interface ICounter<T> {

    int count(T candidate);
}
