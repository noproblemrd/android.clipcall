package it.clipcall.infrastructure.support.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by omega on 4/8/2016.
 */
public class Group<TKey,TValue> {

    private final Map<TKey,List<TValue>> map = new HashMap<>();


    public void put(TKey key, TValue value){

        if(!map.containsKey(key))
            map.put(key, new ArrayList<TValue>());

        List<TValue> values = map.get(key);
        values.add(value);
    }

    public List<TValue> getValues(TKey key){
        if(map.containsKey(key))
            return map.get(key);

        return Lists.empty();
    }
}
