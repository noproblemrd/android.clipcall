package it.clipcall.infrastructure.support;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;

import it.clipcall.consumer.projects.models.CallHistoryItem;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.models.PaymentHistoryItem;
import it.clipcall.consumer.projects.models.QuoteHistoryItem;
import it.clipcall.consumer.projects.models.VideoChatHistoryItem;
import it.clipcall.consumer.projects.models.VideoHistoryItem;
import it.clipcall.professional.leads.models.eQuoteStatus;
import it.clipcall.professional.leads.models.eQuoteStatusReason;

/**
 * Created by dorona on 23/02/2016.
 */
public class HistoryItemDeserializer implements JsonDeserializer<HistoryItem> {
    @Override
    public HistoryItem deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson g = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .registerTypeAdapter(eQuoteStatus.class,new eQuoteStatusDeserializer())
                .registerTypeAdapter(eQuoteStatusReason.class,new eQuoteStatusReasonDeserializer())
                .create();
        HistoryItem result = null;
        String itemType = ((JsonObject) json).get("itemType").getAsString();
        if(itemType.equals("Payment")){
           result =  g.fromJson(json.toString(), PaymentHistoryItem.class);
        }else if(itemType.equals("CallRecording")){
            result =  g.fromJson(json.toString(), CallHistoryItem.class);
        }
        else if(itemType.equals("VideoChat")){
            result =  g.fromJson(json.toString(), VideoChatHistoryItem.class);
        }else if(itemType.equals("Video")){
            result =  g.fromJson(json.toString(), VideoHistoryItem.class);
        }else if(itemType.equals("Quote")){
            result = g.fromJson(json.toString(), QuoteHistoryItem.class);
        }
        return result;
    }
}
