package it.clipcall.infrastructure.support.fragments;

/**
 * Created by dorona on 13/03/2016.
 */
public interface IHandlesBackPressed {

    boolean handleBackPressed();

}
