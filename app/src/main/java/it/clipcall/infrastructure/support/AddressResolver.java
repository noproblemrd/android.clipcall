package it.clipcall.infrastructure.support;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.maps.entities.LatitudeLongitude;
import it.clipcall.infrastructure.support.collections.Lists;

@PerActivity
public class AddressResolver {


    private final Geocoder geocoder;

    @Inject
    public AddressResolver(Activity activity){
        geocoder = new Geocoder(activity);
    }

    public Address resolveAddress(LatitudeLongitude latitudeLongitude){
        try {
            List<android.location.Address> addresses = geocoder.getFromLocation(latitudeLongitude.latitude, latitudeLongitude.longitude,1);
            if(Lists.isNullOrEmpty(addresses))
                return null;

            return addresses.get(0);
        } catch (Exception e) {
            return null;
        }
    }
}
