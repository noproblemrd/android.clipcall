package it.clipcall.infrastructure.support;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import it.clipcall.professional.leads.models.eQuoteStatus;

/**
 * Created by dorona on 29/12/2015.
 */
public class eQuoteStatusDeserializer implements JsonDeserializer<eQuoteStatus> {

    @Override
    public eQuoteStatus deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
        String key = element.getAsString();
        return eQuoteStatus.valueOf(key);
    }
}
