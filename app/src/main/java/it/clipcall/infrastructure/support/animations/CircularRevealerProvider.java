package it.clipcall.infrastructure.support.animations;

import android.os.Build;

/**
 * Created by omega on 3/4/2016.
 */
public class CircularRevealerProvider {



    public ICircularRevealerAnimator provideCircularRevealer(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            return new CircularRevealerAnimator();
        }

        return new PreLollipopCircularRevealerAnimator();
    }
}
