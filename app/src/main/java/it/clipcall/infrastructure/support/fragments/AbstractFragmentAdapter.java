package it.clipcall.infrastructure.support.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 29/02/2016.
 */
public abstract class AbstractFragmentAdapter<T extends Fragment> extends FragmentPagerAdapter {
    private final List<T> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public AbstractFragmentAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public T getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(T fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    public abstract void setData(Object data);

    public abstract <T extends IView> void setParentView(T view);
}
