package it.clipcall.infrastructure.support.animations;

import android.view.View;

/**
 * Created by omega on 3/4/2016.
 */
public interface ICircularRevealerAnimator {

    void reveal(View view);

    void unreveal(View view);
}
