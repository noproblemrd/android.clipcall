package it.clipcall.infrastructure.support;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import it.clipcall.common.invitations.models.eInvitationType;

/**
 * Created by dorona on 29/12/2015.
 */
public class InvitationTypeDeserializer implements JsonDeserializer<eInvitationType> {

    @Override
    public eInvitationType deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
        int key = element.getAsInt();
        return eInvitationType.fromKey(key);
    }
}
