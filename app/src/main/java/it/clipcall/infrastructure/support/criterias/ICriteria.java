package it.clipcall.infrastructure.support.criterias;

/**
 * Created by omega on 3/11/2016.
 */
public interface ICriteria<T> {

    boolean isSatisfiedBy(T candidate);
}
