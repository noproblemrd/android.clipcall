package it.clipcall.infrastructure.support.recyclerviews;

import android.os.Bundle;

import it.clipcall.infrastructure.viewModel.support.recycleviews.ItemSelectedListener;

/**
 * Created by Omega on 5/23/2016.
 */
public class EmptyItemSelectedListener<T> implements ItemSelectedListener<T> {
    @Override
    public void onItemSelected(T item) {

    }

    @Override
    public void onItemSelected(T item, Bundle uiData) {

    }

    @Override
    public void onItemSelected(T item, Bundle uiData, Bundle data) {

    }
}
