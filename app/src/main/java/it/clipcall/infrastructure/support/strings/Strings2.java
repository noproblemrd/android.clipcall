package it.clipcall.infrastructure.support.strings;

import com.google.common.base.Strings;

/**
 * Created by dorona on 04/05/2016.
 */
public class Strings2 {

    public static boolean isNullOrWhiteSpace(String value){
        if(Strings.isNullOrEmpty(value))
            return true;


        return value.trim().isEmpty();
    }
}
