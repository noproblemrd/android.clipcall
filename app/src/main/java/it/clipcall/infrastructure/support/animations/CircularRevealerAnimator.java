package it.clipcall.infrastructure.support.animations;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.view.View;
import android.view.ViewAnimationUtils;

/**
 * Created by omega on 3/4/2016.
 */
public class CircularRevealerAnimator implements ICircularRevealerAnimator {

    public void reveal(final View view){

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            return;

        // previously invisible view
        // get the center for the clipping circle
        int cx = view.getMeasuredWidth() / 2;
        int cy = view.getMeasuredHeight() / 2;

        // get the final radius for the clipping circle
        int finalRadius = Math.max(view.getWidth(), view.getHeight()) / 2;

        // create the animator for this view (the start radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);

/*        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.setDuration(400);*/

        // make the view visible and start the animation
        view.setVisibility(View.VISIBLE);
        anim.start();
    }

    public void unreveal(final View view){
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            return;

        // get the center for the clipping circle
        int cx = view.getMeasuredWidth() / 2;
        int cy = view.getMeasuredHeight() / 2;

        // get the initial radius for the clipping circle
        int initialRadius = view.getWidth() / 2;

        // create the animation (the final radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius, 0);
 /*       anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.setDuration(400);*/

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setVisibility(View.INVISIBLE);
            }
        });

        // start the animation
        anim.start();
    }

}
