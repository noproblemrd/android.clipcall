package it.clipcall.infrastructure.support.network;

import android.util.Log;

import com.google.common.base.Strings;

import it.clipcall.infrastructure.StatusResponse;
import retrofit2.Call;
import retrofit2.Response;


public class RetrofitCallProcessor {


    public static <T> T processCall(Call<T> call){
        try {
            Response<T> response = call.execute();
            if(response == null)
                return null;
            T body = response.body();
            return body;
        } catch (Exception e) {

            Log.e("ClipCall", e.getMessage(),e);
            return null;
        }
    }

    public static boolean processStatusCall(Call<StatusResponse> statusCall){
        StatusResponse statusResponse = processCall(statusCall);
        if(statusResponse == null )
            return false;

        if(Strings.isNullOrEmpty(statusResponse.status))
            return false;

        boolean success = "OK".equals(statusResponse.status);
        return success;
    }
}
