package it.clipcall.infrastructure.support.collections;

/**
 * Created by dorona on 05/04/2016.
 */
public interface IAction<T> {

    void invoke(T input);
}
