package it.clipcall.infrastructure.support.animations;



import android.os.Build;
import android.view.View;

import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;

/**
 * Created by omega on 3/4/2016.
 */
public class PreLollipopCircularRevealerAnimator implements ICircularRevealerAnimator {
    public void reveal(final View view){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            return;

        // previously invisible view
        // get the center for the clipping circle
        int cx = view.getMeasuredWidth() / 2;
        int cy = view.getMeasuredHeight() / 2;

        // get the final radius for the clipping circle
        int finalRadius = Math.max(view.getWidth(), view.getHeight()) / 2;

        // create the animator for this view (the start radius is zero)

        SupportAnimator animator = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);

/*        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setDuration(400);*/

        // make the view visible and start the animation
        view.setVisibility(View.VISIBLE);
        animator.start();
    }

    public void unreveal(final View view){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            return;

        // get the center for the clipping circle
        int cx = view.getMeasuredWidth() / 2;
        int cy = view.getMeasuredHeight() / 2;

        // get the initial radius for the clipping circle
        int initialRadius = view.getWidth() / 2;

        // create the animation (the final radius is zero)
        SupportAnimator animator  =
                ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius, 0);
 /*       animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setDuration(400);*/

        // make the view invisible when the animation is done
        animator.addListener(new SupportAnimator.AnimatorListener() {
            @Override
            public void onAnimationStart() {

            }

            @Override
            public void onAnimationEnd() {
                view.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationCancel() {

            }

            @Override
            public void onAnimationRepeat() {

            }
        });

        // start the animation
        animator.start();
    }
}
