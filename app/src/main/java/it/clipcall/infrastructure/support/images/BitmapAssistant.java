package it.clipcall.infrastructure.support.images;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by dorona on 18/01/2016.
 */
public class BitmapAssistant {

    public static File convertToFile(Bitmap bitmap){

        try {
            if(bitmap == null)
                return null;

            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");
            destination.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapData = bos.toByteArray();
            FileOutputStream fos = new FileOutputStream(destination);
            fos.write(bitmapData);
            fos.flush();
            fos.close();
            return destination;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap createImage(ContentResolver contentResolver, Uri imageUri){
       try{
           String[] filePath = { MediaStore.Images.Media.DATA };
           Cursor cursor = contentResolver.query(imageUri, filePath, null, null, null);
           cursor.moveToFirst();
           String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

           BitmapFactory.Options options = new BitmapFactory.Options();
           options.inPreferredConfig = Bitmap.Config.ARGB_8888;
           Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
           cursor.close();
           return bitmap;
       }
       catch (Exception e){
           return null;
       }
    }

    public static Bitmap createScaledImage2(ContentResolver contentResolver, Uri imageUri){
        try{

            float maxImageSize = 500;
            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor cursor = contentResolver.query(imageUri, filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap originalImage = BitmapFactory.decodeFile(imagePath, options);
            cursor.close();


            float ratio = Math.min(
                    (float) maxImageSize / originalImage.getWidth(),
                    (float) maxImageSize / originalImage.getHeight());

            if(ratio > 1){
                return originalImage;
            }

            int width = Math.round((float) ratio * originalImage.getWidth());
            int height = Math.round((float) ratio * originalImage.getHeight());

            Bitmap newBitmap = Bitmap.createScaledBitmap(originalImage, width,
                    height, true);
            return newBitmap;

        }
        catch (Exception e){
            return null;
        }
    }


    public static Bitmap createScaledImage(Context context, Uri imageUri){
        try{
            String[] projection = {MediaStore.MediaColumns.DATA};
            CursorLoader cursorLoader = new CursorLoader(context, imageUri, projection, null, null,
                    null);
            Cursor cursor = cursorLoader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();

            String selectedImagePath = cursor.getString(column_index);

            Bitmap bm;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(selectedImagePath, options);
            final int REQUIRED_SIZE = 200;
            int scale = 1;
            while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                    && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            options.inSampleSize = scale;
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeFile(selectedImagePath, options);
            return bm;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
