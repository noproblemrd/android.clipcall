package it.clipcall.infrastructure.support;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;

import it.clipcall.infrastructure.support.dates.DateFormatter;

/**
 * Created by dorona on 23/02/2016.
 */
public class DateDeserializer implements JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonElement jsonElement, Type typeOF,
                            JsonDeserializationContext context) throws JsonParseException {
        String dateAsString = jsonElement.getAsString();
        Date date = DateFormatter.toDate(dateAsString);
        if(date == null)
            throw new JsonParseException("Unparseable date: \"" + jsonElement.getAsString());
        return date;
    }
}