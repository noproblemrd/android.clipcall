package it.clipcall.infrastructure.support.validation;

import java.util.regex.Matcher;

import it.clipcall.infrastructure.support.strings.Strings2;

/**
 * Created by dorona on 10/05/2016.
 */
public class Validator {

    public static boolean isValidEmail(String email){
        if(Strings2.isNullOrWhiteSpace(email))
            return false;

        Matcher matcher = android.util.Patterns.EMAIL_ADDRESS.matcher(email);
        return matcher.matches();
    }
}
