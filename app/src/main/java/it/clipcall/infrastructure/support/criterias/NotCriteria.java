package it.clipcall.infrastructure.support.criterias;

/**
 * Created by dorona on 05/07/2016.
 */
public class NotCriteria<T> implements ICriteria<T> {


    private final ICriteria<T> criteria;

    public NotCriteria(ICriteria<T> criteria){

        this.criteria = criteria;
    }

    @Override
    public boolean isSatisfiedBy(T candidate) {
        return ! criteria.isSatisfiedBy(candidate);
    }
}
