package it.clipcall.infrastructure.support.criterias.strings;

import com.google.common.base.Strings;

import it.clipcall.infrastructure.support.criterias.ICriteria;

/**
 * Created by omega on 3/26/2016.
 */
public class EqualsIgnoreCaseCriteria implements ICriteria<String> {

    private final String value;

    public EqualsIgnoreCaseCriteria(String value){
        this.value = value;
    }

    @Override
    public boolean isSatisfiedBy(String candidate) {
        if(Strings.isNullOrEmpty(candidate))
            return false;

        if(Strings.isNullOrEmpty(value))
            return false;

        return candidate.toUpperCase().equals(value.toUpperCase());
    }
}
