package it.clipcall.infrastructure.support.ui;

import android.view.MotionEvent;
import android.view.View;


public class RippleForegroundListener implements View.OnTouchListener {
    View  parentView;

    public RippleForegroundListener(View  parentView) {
        this.parentView = parentView;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // Convert to card view coordinates. Assumes the host view is
        // a direct child and the card view is not scrollable.
        float x = event.getX() + v.getLeft();
        float y = event.getY() + v.getTop();

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            // Simulate motion on the card view.
            parentView.drawableHotspotChanged(x, y);
        }

        // Simulate pressed state on the card view.
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                parentView.setPressed(true);
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                parentView.setPressed(false);
                break;
        }

        // Pass all events through to the host view.
        return false;
    }
}