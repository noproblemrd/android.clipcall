package it.clipcall.infrastructure.support.json;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class JsonBuilder {

    private final  Gson gson;
    @Inject
    public JsonBuilder(Gson gson){

        this.gson = gson;
    }

    public JSONObject buildJsonObject(Object object){
        if(object == null)
            return new JSONObject();

        if(object instanceof JSONObject){
            return (JSONObject)object;
        }
        try {

            String jsonString = gson.toJson(object);
            JSONObject request = new JSONObject(jsonString);
            return request;
        } catch (JSONException e) {
            return new JSONObject();
        }
    }

    public Map<String,Object> buildMap(Object object){
        if(object == null)
            return new HashMap<>();

        try {
            Type stringStringMap = new TypeToken<Map<String, Object>>(){}.getType();
            String jsonString = gson.toJson(object);
            Map<String,Object> map = gson.fromJson(jsonString, stringStringMap);
            return map;

        } catch (Exception e) {
            return new HashMap<>();
        }
    }
}
