package it.clipcall.infrastructure.support.recyclerviews;

public interface ActionInvokerListener<T> {
    void onInvokeAction(T item);
}
