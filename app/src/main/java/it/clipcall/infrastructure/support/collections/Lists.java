package it.clipcall.infrastructure.support.collections;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.infrastructure.support.criterias.ICriteria;

/**
 * Created by omega on 3/11/2016.
 */
public class Lists {


    public static <T> List<T> empty(){
        return new ArrayList<>();
    }

    public static <T> boolean isNullOrEmpty(List<T> items){
        return items == null || items.size() == 0;
    }


    public static <T, U> List<U> map(List<T> items, IFunction<T,U> func){
        if(Lists.isNullOrEmpty(items))
            return Lists.empty();

        List<U> result = new ArrayList<>();
        for(T item: items){
            U output = func.apply(item);
            result.add(output);
        }
        return result;
    }

    public static <T> T firstOrDefault(List<T> items, ICriteria<T> criteria){
        if(isNullOrEmpty(items))
            return null;

        if(criteria == null)
            return items.get(0);

        for(T item: items){
            if(criteria.isSatisfiedBy(item))
                return item;
        }

        return null;
    }

    public static <TKey, TValue> Group<TKey,TValue> groupBy(List<TValue> values){
        return null;
    }




    public static <T> boolean any(List<T> items,  ICriteria<T> criteria){
        if(isNullOrEmpty(items))
            return false;

        if(criteria == null)
            return true;

        for(T item: items){
            if(criteria.isSatisfiedBy(item))
                return true;
        }

        return false;
    }

    public static <T> int indexOf(List<T> items, ICriteria<T> criteria){
        if(isNullOrEmpty(items))
            return -1;

        if(criteria == null)
            return -1;

        for(int i = 0; i<items.size(); ++i){
            T candidate = items.get(i);
            if(criteria.isSatisfiedBy(candidate))
                return i;
        }

        return -1;
    }

    public static <T> boolean all(List<T> items,  ICriteria<T> criteria){
        if(isNullOrEmpty(items))
            return false;

        if(criteria == null)
            return true;

        for(T item: items){
            if(!criteria.isSatisfiedBy(item))
                return false;
        }

        return true;
    }

    public static <T> List<T> where(List<T> items, ICriteria<T> criteria){
        if(isNullOrEmpty(items))
            return new ArrayList<>();

        if(criteria == null)
            return new ArrayList<>(items);


        List<T> result = new ArrayList<>();

        for(T item: items){
            if(criteria.isSatisfiedBy(item))
                result.add(item);
        }

        return result;
    }

    public static <T> int sum(List<T> items, ICounter<T> counter){
        if(isNullOrEmpty(items))
            return 0;

        int total = 0;
        for(T item: items){
            int value = counter.count(item);
            total+= value;
        }
        return total;
    }

    public static <T> void forEach(List<T> items, IAction<T> action){
        for(T item: items){
            action.invoke(item);
        }
    }

    public static <T> int firstIndexOrDefault(List<T> items, ICriteria<T> criteria) {
        for(int i=0; i<items.size(); ++i){
            if(criteria.isSatisfiedBy(items.get(i)))
                return i;
        }
        return -1;
    }
}
