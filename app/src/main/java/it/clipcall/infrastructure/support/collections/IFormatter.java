package it.clipcall.infrastructure.support.collections;

/**
 * Created by dorona on 10/07/2016.
 */
public interface IFormatter<T,U>{

    U format(T input);
}
