package it.clipcall.infrastructure.support;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import it.clipcall.common.projects.models.eProjectStatusColor;

/**
 * Created by dorona on 29/12/2015.
 */
public class ProjectStatusColorDeserializer implements JsonDeserializer<eProjectStatusColor> {

    @Override
    public eProjectStatusColor deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
        int key = element.getAsInt();
        return eProjectStatusColor.fromKey(key);
    }
}
