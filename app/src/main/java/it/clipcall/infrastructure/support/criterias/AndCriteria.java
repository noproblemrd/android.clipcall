package it.clipcall.infrastructure.support.criterias;

/**
 * Created by dorona on 05/07/2016.
 */
public class AndCriteria<T> implements ICriteria<T> {


    private final ICriteria<T> firstCriteria;
    private final ICriteria<T> secondCriteria;

    public AndCriteria(ICriteria<T> firstCriteria, ICriteria<T> secondCriteria){

        this.firstCriteria = firstCriteria;
        this.secondCriteria = secondCriteria;
    }

    @Override
    public boolean isSatisfiedBy(T candidate) {
        return firstCriteria.isSatisfiedBy(candidate) && secondCriteria.isSatisfiedBy(candidate);
    }
}
