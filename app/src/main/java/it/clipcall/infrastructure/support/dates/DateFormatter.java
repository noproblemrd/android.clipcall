package it.clipcall.infrastructure.support.dates;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by dorona on 16/06/2016.
 */
public class DateFormatter {

    private static final String defaultDateFormat = "yyyy-MM-dd HH:mm:ss";


    private static final String defaultDateShortFormat = "yyyy-MM-dd";

    private static final TimeZone utcZone = TimeZone.getTimeZone("UTC");
    private static final String[] DATE_FORMATS = new String[] {
            "MMM dd, yyyy HH:mm:ss",
            "MMM dd, yyyy",
            "yyyy-MM-dd HH:mm:ss",
            "yyyy-MM-dd HH:mm:ss Z",

    };

    public static String toLocalDate(Date utcDate){
        String result = toLocalDate(utcDate, defaultDateFormat);
        return result;
    }


    public static String format(LocalDate dateTime){
        String formattedDate = format(dateTime, defaultDateShortFormat);
        return formattedDate;
    }

    public static String format(LocalDate dateTime, String format){
        DateTimeFormatter dateTimerFormatter = DateTimeFormat.forPattern(format);
        String formattedDate = dateTimerFormatter.print(dateTime);
        return formattedDate;
    }

    public static String toLocalDate(Date utcDate, String dateFormat){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        simpleDateFormat.setTimeZone(TimeZone.getDefault());
        String formattedDate = simpleDateFormat.format(utcDate);
        return formattedDate;
    }

    public static Date toDate(String date){
        for (String format : DATE_FORMATS) {
            try {
                SimpleDateFormat simpleDateFormat =  new SimpleDateFormat(format, Locale.US);
                simpleDateFormat.setTimeZone(utcZone);
                Date result = simpleDateFormat.parse(date);
                return result;
            } catch (Exception e) {
            }
        }
        return null;
    }
}
