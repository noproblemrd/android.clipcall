package it.clipcall.infrastructure.support.collections;

import com.google.common.base.Predicate;

import java.util.Collection;

/**
 * Created by dorona on 09/03/2016.
 */
public class Collections3 {

    public static  <T> T  getFirst(Collection<T> items, Predicate<T> predicate){

        if(isNullOrEmpty(items))
            return null;

        for(T item : items)
            if(predicate.apply(item))
                return item;

        return null;

    }


    public static <T>  boolean isNullOrEmpty(Collection<T>  items){
        return items == null || items.size() == 0;
    }

}
