package it.clipcall.infrastructure.support;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import it.clipcall.infrastructure.responses.eInvitationCheckInStatus;

/**
 * Created by dorona on 29/12/2015.
 */
public class InvitationCheckInStatusDeserializer implements JsonDeserializer<eInvitationCheckInStatus> {

    @Override
    public eInvitationCheckInStatus deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
        int key = element.getAsInt();
        return eInvitationCheckInStatus.fromKey(key);
    }
}
