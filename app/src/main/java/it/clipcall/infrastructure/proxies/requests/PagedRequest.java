package it.clipcall.infrastructure.proxies.requests;

/**
 * Created by micro on 2/5/2016.
 */
public class PagedRequest {

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    private int pageNumber;
    private int pageSize;
}
