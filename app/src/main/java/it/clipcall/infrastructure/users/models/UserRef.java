package it.clipcall.infrastructure.users.models;

import java.io.Serializable;

/**
 * Created by dorona on 28/01/2016.
 */
public class UserRef implements Serializable {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
