package it.clipcall.infrastructure;

import java.util.Random;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by dorona on 09/11/2015.
 */
public class RandomNumberGenerator {

    @Inject
    @Singleton
    public RandomNumberGenerator() {
    }

    public String generate(int length){
        final Random rn = new Random();
        final StringBuilder builder = new StringBuilder();
        for(int i=0; i<length; ++i){
            builder.append(rn.nextInt(10));
        }
        return builder.toString();
    }
}
