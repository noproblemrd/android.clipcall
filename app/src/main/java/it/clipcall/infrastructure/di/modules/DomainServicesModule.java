package it.clipcall.infrastructure.di.modules;

import android.app.Application;

import com.layer.sdk.LayerClient;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.services.BotChatMessageHandler;
import it.clipcall.common.chat.services.ChatMessageDispatcher;
import it.clipcall.common.chat.services.GeneralChatMessageHandler;
import it.clipcall.common.chat.services.SystemChatMessageHandler;
import it.clipcall.common.invitations.services.BranchIoManager;
import it.clipcall.common.invitations.services.CompositeInvitationProvider;
import it.clipcall.common.invitations.services.CustomerSelfInvitationProvider;
import it.clipcall.common.invitations.services.IInvitationProvider;
import it.clipcall.common.models.ApplicationModes;
import it.clipcall.common.models.UserEntity;
import it.clipcall.consumer.payment.services.messageHandlers.ConsumerAcceptQuoteMessageHandler;
import it.clipcall.consumer.vidoechat.services.ConsumerAcceptVideoCallMessageHandler;
import it.clipcall.infrastructure.messageHandlers.IMessageHandler;
import it.clipcall.infrastructure.messageHandlers.MessageDispatcher;
import it.clipcall.infrastructure.messageHandlers.NoOpMessageHandler;
import it.clipcall.infrastructure.repositories.Repository2;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.ConsumerInitialRouteRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.CustomerInvitationInitialRouteRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.DefaultActionRouteRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.IRouteRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.PostPhoneValidationRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.RequiresPhoneValidationRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.SubmitProjectToAdvertisersPhoneValidationRequiredRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.VideoChatInitialRouteRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.VideoChatPhoneValidationRequiredRouteActionRule;
import it.clipcall.infrastructure.routing.services.RouteActivationService;
import it.clipcall.infrastructure.routing.services.RouteResolver;
import it.clipcall.infrastructure.routing.services.RoutingService2;
import it.clipcall.professional.leads.services.NewProjectMessageHandler;
import it.clipcall.professional.payment.services.PaymentReceivedMessageHandler;
import it.clipcall.professional.remoteservicecall.services.AcceptVideoCallMessageHandler;

@Module
public class DomainServicesModule {

    @Provides
    @Singleton
    MixpanelAPI provideMixPanelApi(Application application){
        String projectToken = "f13165eb7b019b597ae3b6b36b5ee532";
        MixpanelAPI mixpanelApi = MixpanelAPI.getInstance(application, projectToken);
        return mixpanelApi;
    }

    @Provides
    @Singleton
    UserEntity provideUserEntity(Repository2 repository ){
        UserEntity user = repository.getSingle(UserEntity.class);
        if(user == null){
            user = new UserEntity();
            user.setMode(ApplicationModes.Consumer);
            repository.update(user);
        }

        return user;

    }


    @Provides
    @Named("consumerRoutingRules")
    @Singleton
    List<IRouteRule> provideConsumerRoutingRules(RequiresPhoneValidationRule requiresPhoneValidationRule,
                                                  VideoChatInitialRouteRule videoChatInitialRouteRule,
                                                  PostPhoneValidationRule postPhoneValidationRule,
                                                  ConsumerInitialRouteRule consumerInitialRouteRule,
                                                         VideoChatPhoneValidationRequiredRouteActionRule videoChatPhoneValidationRequiredRouteActionRule,
                                                  SubmitProjectToAdvertisersPhoneValidationRequiredRule submitProjectToAdvertisersPhoneValidationRequiredRule,
                                                  DefaultActionRouteRule defaultActionRouteRule,
                                                 CustomerInvitationInitialRouteRule customerInvitationInitialRouteRule
                                                  ){

        List<IRouteRule> rules = new ArrayList<>();
        rules.add(requiresPhoneValidationRule);
        rules.add(customerInvitationInitialRouteRule);
        rules.add(videoChatInitialRouteRule);
        rules.add(postPhoneValidationRule);
        rules.add(consumerInitialRouteRule);

        rules.add(videoChatPhoneValidationRequiredRouteActionRule);
        rules.add(submitProjectToAdvertisersPhoneValidationRequiredRule);

        rules.add(defaultActionRouteRule);

        return rules;
    }

    @Provides
    @Named("professionalRoutingService")
    @Singleton
    RoutingService2 provideProfessionalRoutingService2(@Named("professionalRoutingRules") List<IRouteRule> routingRules,
                                           @Named("professionalRouteResolver") RouteResolver routeResolver,
                                           RouteActivationService routeActivationService){
        return new RoutingService2(routeResolver, routingRules, routeActivationService);
    }


    @Provides
    @Named("consumerRoutingService")
    @Singleton
    RoutingService2 provideConsumerRoutingService2(@Named("consumerRoutingRules") List<IRouteRule> routingRules,
                                           @Named("consumerRouteResolver") RouteResolver routeResolver,
                                           RouteActivationService routeActivationService){
        return new RoutingService2(routeResolver, routingRules, routeActivationService);
    }


    @Provides
    @Named("professionalRoutingRules")
    @Singleton
    List<IRouteRule> provideProfessionalRoutingRules(){
        List<IRouteRule> rules = new ArrayList<>();
        return rules;
    }


    @Provides
    @Named("consumerRouteResolver")
    @Singleton
    RouteResolver provideConsumerRouteResolver(@Named("consumerRoutes") List<Route> routes){
        return new RouteResolver(routes);
    }

    @Provides
    @Named("professionalRouteResolver")
    @Singleton
    RouteResolver provideProfessionalRouteResolver(@Named("professionalRoutes") List<Route> routes){
        return new RouteResolver(routes);
    }



    @Provides
    @Named("consumerRoutes")
    @Singleton
    List<Route> provideConsumerRoutes(){
        List<Route> routes = new ArrayList<>();
        Field[] declaredFields = Routes.class.getDeclaredFields();
        List<Field> staticFields = new ArrayList<Field>();
        for (Field field : declaredFields) {
            if (java.lang.reflect.Modifier.isStatic(field.getModifiers())) {
                staticFields.add(field);
                try {
                    Object route = field.get(null);
                    if(route instanceof Route){
                        routes.add((Route) route);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return routes;
    }

    @Provides
    @Named("professionalRoutes")
    @Singleton
    List<Route> provideProfessionalRoutes(){
        List<Route> routes = new ArrayList<>();
        return routes;
    }


    public static final String LAYER_APP_ID = "layer:///apps/staging/960ede38-dadc-11e5-9816-1097211b7cce";
    public static final String GCM_PROJECT_NUMBER = "348674619996";

    @Provides
    @Singleton
    IInvitationProvider provideInvitationProvider(BranchIoManager branchIoManager, CustomerSelfInvitationProvider customerSelfInvitationProvider){
        CompositeInvitationProvider compositeInvitationProvider = new CompositeInvitationProvider(branchIoManager, customerSelfInvitationProvider);
        return compositeInvitationProvider;
    }

    @Singleton
    @Provides
    LayerClient providProfessionaleLayerClient(Application application){
        LayerClient.setLoggingEnabled(application.getApplicationContext(), true);

        // Initializes a LayerClient object with the Google Project Number
        LayerClient.Options options = new LayerClient.Options();

        //Sets the GCM sender id allowing for push notifications
        //options.googleCloudMessagingSenderId(GCM_PROJECT_NUMBER);
        options.broadcastPushInForeground(true);

        //By default, only unread messages are synced after a user is authenticated, but you
        // can change that behavior to all messages or just the last message in a conversation
       // options.historicSyncPolicy(LayerClient.Options.HistoricSyncPolicy.ALL_MESSAGES);
        //options.historicSyncPolicy(LayerClient.Options.HistoricSyncPolicy.FROM_LAST_MESSAGE);
        LayerClient layerClient = LayerClient.newInstance(application.getBaseContext(), LAYER_APP_ID, options);
        layerClient.setAutoDownloadSizeThreshold(1024 * 100 * 50);
        layerClient.setAutoDownloadMimeTypes(Arrays.asList(MessageType.Image.getMimeType(), MessageType.Audio.getMimeType()));


        return layerClient;
    }





    @Singleton
    @Provides
    MessageDispatcher provideMessageDispatcher(Application application,
                                               NoOpMessageHandler noOpMessageHandler,
                                               AcceptVideoCallMessageHandler acceptVideoCallMessageHandler,
                                               ConsumerAcceptVideoCallMessageHandler consumerAcceptVideoCallMessageHandler,
                                               NewProjectMessageHandler newProjectMessageHandler,
                                               PaymentReceivedMessageHandler paymentReceivedMessageHandler,
                                               ConsumerAcceptQuoteMessageHandler acceptQuoteMessageHandler){


        List<IMessageHandler> messageHandlers = new ArrayList<>();
        messageHandlers.add(acceptVideoCallMessageHandler);
        messageHandlers.add(consumerAcceptVideoCallMessageHandler);
        messageHandlers.add(newProjectMessageHandler);
        messageHandlers.add(paymentReceivedMessageHandler);
        messageHandlers.add(acceptQuoteMessageHandler);

        //last message handler
        messageHandlers.add(noOpMessageHandler);

        MessageDispatcher messageDispatcher = new MessageDispatcher(application, messageHandlers);
        return messageDispatcher;

    }


    @Singleton
    @Provides
    ChatMessageDispatcher provideChatMessageDispatcher(Application application,
                                                       GeneralChatMessageHandler generalChatMessageHandler,
                                                       BotChatMessageHandler botChatMessageHandler,
                                                       SystemChatMessageHandler systemChatMessageHandler){
        ChatMessageDispatcher dispatcher = new ChatMessageDispatcher();
        dispatcher.addHandler(generalChatMessageHandler);
        dispatcher.addHandler(botChatMessageHandler);
        dispatcher.addHandler(systemChatMessageHandler);
        return dispatcher;
    }

}