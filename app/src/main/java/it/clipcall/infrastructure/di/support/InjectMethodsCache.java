package it.clipcall.infrastructure.di.support;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import it.clipcall.infrastructure.di.components.ActivityComponent;

/**
 * Created by omega on 3/12/2016.
 */
public class InjectMethodsCache {

    private final static Map<Class, Method> activityComponentCache = buildActivityComponentCache();

    static Map<Class, Method> buildActivityComponentCache() {
        Map<Class, Method> cache = new HashMap<>();
        for (Method m : ActivityComponent.class.getDeclaredMethods()) {
            Class[] types = m.getParameterTypes();
            if (types.length == 1) {
                cache.put(types[0], m);
            }
        }
        return cache;
    }

    public static Map<Class, Method>  getActivityComponentsMethodsCache(){
        return activityComponentCache;
    }
}
