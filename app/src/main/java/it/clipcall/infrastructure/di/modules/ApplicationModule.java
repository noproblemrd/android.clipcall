package it.clipcall.infrastructure.di.modules;

import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.preference.PreferenceManager;

import org.modelmapper.ModelMapper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import it.clipcall.ClipCallApplication;
import it.clipcall.bootstrappingExtensions.ApplicationCrashBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.ApplicationFeaturesBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.ApplicationLifecycleCallbacksBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.AppsFlyerBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.BonusClaimBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.BranchIoBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.CategoryTooltipsBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.ChatBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.CreateApplicationShortcutBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.DeviceBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.EmozeBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.FacebookBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.FontsBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.GcmBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.IconsBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.IntercomBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.LoggerBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.MixPanelBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.ObjectMapperBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.PermissionsBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.RepositoryBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.RoutingBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.UberTestersBootstrappingExtension;
import it.clipcall.bootstrappingExtensions.UserIdentificationBootstrappingExtension;
import it.clipcall.common.chat.support.factories.AudioMessageFactory;
import it.clipcall.common.chat.support.factories.BookQuoteMessageFactory;
import it.clipcall.common.chat.support.factories.ChatMessageFactoryProvider;
import it.clipcall.common.chat.support.factories.ClipCallBotMessageFactory;
import it.clipcall.common.chat.support.factories.ImageMessageFactory;
import it.clipcall.common.chat.support.factories.QuoteMessageFactory;
import it.clipcall.common.chat.support.factories.RequestQuoteMessageFactory;
import it.clipcall.common.chat.support.factories.SystemMessageFactory;
import it.clipcall.common.chat.support.factories.TextMessageFactory;
import it.clipcall.common.chat.support.factories.UnknownMessageFactory;
import it.clipcall.consumer.payment.controllers.PaymentCoordinator;
import it.clipcall.consumer.profile.services.CustomerProfileManager;
import it.clipcall.infrastructure.bootstrappingExtensions.CompositeBootstrappingExtension;
import it.clipcall.infrastructure.bootstrappingExtensions.IBootstrappingExtension;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.SimpleLogger;
import it.clipcall.professional.profile.services.AnonymousDiscountClaimer;
import it.clipcall.professional.profile.services.AuthenticatedDiscountClaimer;
import it.clipcall.professional.profile.services.IDiscountClaimer;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Module
public class ApplicationModule {

    private ClipCallApplication application;

    public ApplicationModule(ClipCallApplication app) {
        application = app;
    }

    @Provides
    @Singleton
    Application provideApplication(){
        return this.application;
    }

    @Provides
    @Singleton
    ClipCallApplication provideClipCallApplication(){
        return this.application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return application.getApplicationContext();
    }


    @Provides
    @Singleton
    NotificationManager provideNotificationManager(){
        NotificationManager notificationManager = (NotificationManager) application.getSystemService(Context.NOTIFICATION_SERVICE);
        return notificationManager;
    }


    @Provides
    @Singleton
    SharedPreferences provideSharedPrefs() {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    @Singleton
    AssetManager provideAssetManager(Context context){
        return context.getAssets();
    }

    @Provides
    @Singleton
    ModelMapper provideModelMapper(){
        return new ModelMapper();
    }

    @Provides
    @Singleton
    CompositeBootstrappingExtension provideBootstrappingExtensions(ApplicationFeaturesBootstrappingExtension applicationFeaturesBootstrappingExtension,
                                                                   DeviceBootstrappingExtension deviceBootstrappingExtension,
                                                                   UserIdentificationBootstrappingExtension userIdentificationBootstrappingExtension,
                                                                   ObjectMapperBootstrappingExtension objectMapperBootstrappingExtension,
                                                                   RepositoryBootstrappingExtension repositoryBootstrappingExtension,
                                                                   RoutingBootstrappingExtension routingBootstrappingExtension,
                                                                   FontsBootstrappingExtension fontsBootstrappingExtension,
                                                                   BranchIoBootstrappingExtension branchIoBootstrappingExtension,
                                                                   AppsFlyerBootstrappingExtension appsFlyerBootstrappingExtension,
                                                                   GcmBootstrappingExtension gcmBootstrappingExtension,
                                                                   IconsBootstrappingExtension iconsBootstrappingExtension,
                                                                   CategoryTooltipsBootstrappingExtension categoryTooltipsBootstrappingExtension,
                                                                   MixPanelBootstrappingExtension mixPanelBootstrappingExtension,
                                                                   PermissionsBootstrappingExtension permissionsBootstrappingExtension,
                                                                   ApplicationLifecycleCallbacksBootstrappingExtension applicationLifecyleCallbacksBootstrappingExtension,
                                                                   ChatBootstrappingExtension chatBootstrappingExtension,
                                                                   ApplicationCrashBootstrappingExtension applicationCrashBootstrappingExtension,
                                                                   UberTestersBootstrappingExtension uberTestersBootstrappingExtension,
                                                                   FacebookBootstrappingExtension facebookBootstrappingExtension,
                                                                   IntercomBootstrappingExtension intercomBootstrappingExtension,
                                                                   CreateApplicationShortcutBootstrappingExtension createApplicationShortcutBootstrappingExtension,
                                                                   EmozeBootstrappingExtension emozeBootstrappingExtension,
                                                                   LoggerBootstrappingExtension loggerBootstrappingExtension,
                                                                   BonusClaimBootstrappingExtension bonusClaimBootstrappingExtension)
    {
        CompositeBootstrappingExtension bootstrappingExtension = new CompositeBootstrappingExtension();
        bootstrappingExtension.addBootstrappingExtension(applicationFeaturesBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(loggerBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(chatBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(deviceBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(createApplicationShortcutBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(userIdentificationBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(objectMapperBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(repositoryBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(routingBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(fontsBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(branchIoBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(appsFlyerBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(gcmBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(iconsBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(categoryTooltipsBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(mixPanelBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(permissionsBootstrappingExtension);

        bootstrappingExtension.addBootstrappingExtension(applicationLifecyleCallbacksBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(applicationCrashBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(uberTestersBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(intercomBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(facebookBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(emozeBootstrappingExtension);
        bootstrappingExtension.addBootstrappingExtension(bonusClaimBootstrappingExtension);



        return bootstrappingExtension;
    }


    @Provides
    @Singleton
    IBootstrappingExtension provideRoutingBootstrappingExtension(CompositeBootstrappingExtension compositeBootstrappingExtension){
        return compositeBootstrappingExtension;
    }

    @Provides
    @Singleton
    ILogger provideLogger(){
        return new SimpleLogger();
    }

    @Provides
    IDiscountClaimer provideDiscountClaimer(AuthenticationManager authenticationManager, PaymentCoordinator paymentCoordinator, CustomerProfileManager profileManager){
        if(authenticationManager.isCustomerAuthenticated()){
            return new AuthenticatedDiscountClaimer(paymentCoordinator, authenticationManager, profileManager);
        }

        return new AnonymousDiscountClaimer(profileManager);
    }

    @Provides
    @Singleton
    ChatMessageFactoryProvider provideChatMessageFactoryProvider(AudioMessageFactory audioMessageFactory,
                         ImageMessageFactory imageMessageFactory,
                         QuoteMessageFactory quoteMessageFactory,
                         RequestQuoteMessageFactory requestQuoteMessageFactory,
                         BookQuoteMessageFactory bookQuoteMessageFactory,
                         TextMessageFactory textMessageFactory,
                         SystemMessageFactory systemMessageFactory,
                         ClipCallBotMessageFactory clipCallBotMessageFactory,
                         UnknownMessageFactory unknownMessageFactory){
        ChatMessageFactoryProvider factoryProvider = ChatMessageFactoryProvider.getInstance();
        factoryProvider.addMessageFactory(audioMessageFactory)
                .addMessageFactory(imageMessageFactory)
                .addMessageFactory(quoteMessageFactory)
                .addMessageFactory(clipCallBotMessageFactory)
                .addMessageFactory(requestQuoteMessageFactory)
                .addMessageFactory(bookQuoteMessageFactory)
                .addMessageFactory(textMessageFactory)
                .addMessageFactory(systemMessageFactory)
                .addMessageFactory(unknownMessageFactory);

        return factoryProvider;
    }
}