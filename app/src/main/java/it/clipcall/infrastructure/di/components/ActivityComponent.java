package it.clipcall.infrastructure.di.components;

import dagger.Component;
import it.clipcall.common.activities.FullScreenActivity;
import it.clipcall.common.activities.SplashActivity;
import it.clipcall.common.chat.activities.ChatActivity;
import it.clipcall.common.validation.activities.PhoneValidation2Activity;
import it.clipcall.common.validation.activities.PhoneValidationActivity;
import it.clipcall.consumer.about.activities.WebViewActivity;
import it.clipcall.consumer.about.activities.WebViewDialogActivity;
import it.clipcall.consumer.about.fragments.ConsumerAboutFragment;
import it.clipcall.consumer.findprofessional.activities.CameraPreviewActivity;
import it.clipcall.consumer.findprofessional.activities.SubmitProjectToAdvertisersActivity;
import it.clipcall.consumer.findprofessional.fragments.ConsumerFindProfessionalFragment;
import it.clipcall.consumer.findprofessional.fragments.SendingVideoRecordingFragment;
import it.clipcall.consumer.findprofessional.fragments.VideoPreviewFragment;
import it.clipcall.consumer.findprofessional.fragments.VideoRecordingFragment;
import it.clipcall.consumer.findprofessional.fragments.VideoRecordingPausedFragment;
import it.clipcall.consumer.help.fragments.ConsumerHelpFragment;
import it.clipcall.consumer.help.fragments.ConsumerTutorialItemFragment;
import it.clipcall.consumer.mainMenu.activities.ConsumerMainMenuActivity;
import it.clipcall.consumer.payment.activities.AndroidPayActivity;
import it.clipcall.consumer.payment.activities.ConsumerClaimDiscountActivity;
import it.clipcall.consumer.payment.activities.ConsumerPayYourProActivity;
import it.clipcall.consumer.payment.activities.ConsumerProfessionalPaymentProjectsActivity;
import it.clipcall.consumer.payment.activities.CreditCardDetailsActivity;
import it.clipcall.consumer.payment.activities.CustomerCreditCardDetailsActivity;
import it.clipcall.consumer.payment.activities.PayTheProActivity;
import it.clipcall.consumer.payment.activities.PaymentDetailsActivity;
import it.clipcall.consumer.payment.activities.PaymentInvoiceActivity;
import it.clipcall.consumer.payment.fragments.ConsumerBonusCashFragment;
import it.clipcall.consumer.payment.presenters.CustomerAddCreditCardPresenter;
import it.clipcall.consumer.payment.viewModel.activities.ConsumerQuoteDetailsActivity;
import it.clipcall.consumer.payment.viewModel.activities.QuoteBookedActivity;
import it.clipcall.consumer.profile.fragments.ConsumerProfileFragment;
import it.clipcall.consumer.projects.activities.ConsumerProjectDetailsActivity;
import it.clipcall.consumer.projects.activities.ProjectAdvertiserActivity;
import it.clipcall.consumer.projects.fragments.ConsumerProjectsFragment;
import it.clipcall.consumer.projects.fragments.ProjectAdvertiserAboutFragment;
import it.clipcall.consumer.projects.fragments.ProjectAdvertiserHistoryFragment;
import it.clipcall.consumer.settings.fragments.ConsumerSettingsFragment;
import it.clipcall.consumer.vidoechat.activities.ConsumerIncomingVideoCallActivity;
import it.clipcall.consumer.vidoechat.activities.ConsumerStartVideoChatActivity;
import it.clipcall.consumer.vidoechat.activities.ConsumerVideoChatCallingActivity;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.di.modules.ActivityModule;
import it.clipcall.infrastructure.di.modules.NetworkModule;
import it.clipcall.infrastructure.di.modules.PresentersModule;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.professional.invitecustomer.fragments.ProfessionalInviteCustomerFragment;
import it.clipcall.professional.leads.activities.ProfessionalNewProjectActivity;
import it.clipcall.professional.leads.activities.ProfessionalProjectDetailsActivity;
import it.clipcall.professional.leads.fragments.ProfessionalProjectDetailsAboutFragment;
import it.clipcall.professional.leads.fragments.ProfessionalProjectDetailsHistoryFragment;
import it.clipcall.professional.leads.fragments.ProfessionalProjectsFragment;
import it.clipcall.professional.mainMenu.MainMenuActivity;
import it.clipcall.professional.mainMenu.support.NavigationView;
import it.clipcall.professional.payment.activities.ProfessionalPaymentSettingsActivity;
import it.clipcall.professional.payment.activities.StripeWebViewActivity;
import it.clipcall.professional.payment.fragments.ProfessionalBonusCashFragment;
import it.clipcall.professional.payment.fragments.ProfessionalPaymentSettingsFragment;
import it.clipcall.professional.payment.viewModel.activities.ProfessionalCreateQuoteActivity;
import it.clipcall.professional.profile.activities.ProfessionalEditProfileActivity;
import it.clipcall.professional.profile.activities.ProfessionalServiceCategoriesActivity;
import it.clipcall.professional.profile.fragments.AdvertiserProfileAboutFragment;
import it.clipcall.professional.profile.fragments.AdvertiserProfileDetailsFragment;
import it.clipcall.professional.profile.fragments.AdvertiserProfileServiceAreaFragment;
import it.clipcall.professional.profile.fragments.ProfessionalProfileFragment;
import it.clipcall.professional.registration.activities.AdvertiserRegistrationActivity;
import it.clipcall.professional.registration.activities.ProfessionalTermsOfServiceAgreementActivity;
import it.clipcall.professional.remoteservicecall.fragments.ProfessionalRemoteServiceCallFragment;
import it.clipcall.professional.settings.fragments.ProfessionalSettingsFragment;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, PresentersModule.class, NetworkModule.class})
public interface ActivityComponent {

    void inject(BaseActivity baseActivity);
    void inject(PhoneValidationActivity activity);
    void inject(StripeWebViewActivity activity);
    void inject(CreditCardDetailsActivity activity);
    void inject(ConsumerPayYourProActivity activity);
    void inject(ConsumerQuoteDetailsActivity activity);
    void inject(PhoneValidation2Activity activity);
    void inject(ConsumerProjectDetailsActivity consumerProjectDetailsActivity);
    void inject(ConsumerMainMenuActivity consumerMainMenuActivity);
    void inject(MainMenuActivity mainMenuActivity);
    void inject(SplashActivity splashActivity);
    void inject(ConsumerStartVideoChatActivity consumerStartVideoChatActivity);
    void inject(ConsumerVideoChatCallingActivity consumerVideoChatCallingActivity);
    void inject(CameraPreviewActivity cameraPreviewActivity);
    void inject(ProfessionalProjectDetailsActivity professionalProjectDetailsActivity);
    void inject(ProfessionalEditProfileActivity  professionalEditProfileActivity);
    void inject(ProjectAdvertiserActivity projectAdvertiserActivity);
    void inject(SubmitProjectToAdvertisersActivity submitProjectToAdvertisersActivity);
    void inject(PayTheProActivity payTheProActivity);
    void inject(CustomerCreditCardDetailsActivity customerAddCreditCardActivity);
    void inject(AndroidPayActivity androidPayActivity);
    void inject(AdvertiserRegistrationActivity advertiserRegistrationActivity);
    void inject(PaymentInvoiceActivity paymentInvoiceActivity);
    void inject(ConsumerIncomingVideoCallActivity consumerIncomingVideoCallActivity);
    void inject(ProfessionalNewProjectActivity newProjectActivity);
    void inject(ProfessionalPaymentSettingsActivity newProjectActivity);

    void inject(QuoteBookedActivity quoteBookedActivity);

    void inject(ChatActivity chatActivity2);
    void inject(ProfessionalServiceCategoriesActivity professionalServiceCategoriesActivity);
    void inject(ProfessionalCreateQuoteActivity activity);
    void inject(ConsumerProfessionalPaymentProjectsActivity activity);
    void inject(WebViewActivity activity);
    void inject(FullScreenActivity activity);
    void inject(ConsumerClaimDiscountActivity activity);
    void inject(ProfessionalTermsOfServiceAgreementActivity activity);
    void inject(WebViewDialogActivity activity);
    void inject(PaymentDetailsActivity activity);



    void inject(ConsumerBonusCashFragment bonusCashFragment);

    void inject(ConsumerProjectsFragment consumerProjectsFragment);
    void inject(ConsumerProfileFragment consumerProfileFragment);
    void inject(ConsumerFindProfessionalFragment consumerFindProfessionalFragment);
    void inject(ConsumerSettingsFragment consumerSettingsFragment);
    void inject(VideoPreviewFragment videoPreviewFragment);
    void inject(ConsumerAboutFragment consumerAboutFragment);
    void inject(ConsumerHelpFragment consumerHelpFragment);
    void inject(NavigationView navigationView);
    void inject(ProjectAdvertiserHistoryFragment projectAdvertiserHistoryFragment);
    void inject(ProjectAdvertiserAboutFragment projectAdvertiserAboutFragment);
    void inject(ConsumerTutorialItemFragment consumerTutorialItemFragment);
    void inject(SendingVideoRecordingFragment sendingVideoRecordingFragment);
    void inject(VideoRecordingFragment videoRecordingFragment);
    void inject(VideoRecordingPausedFragment videoRecordingPausedFragment);


    void inject(ProfessionalBonusCashFragment bonusCashFragment);
    void inject(ProfessionalPaymentSettingsFragment bonusCashFragment);
    void inject(ProfessionalProjectsFragment professionalProjectsFragment);
    void inject(ProfessionalProfileFragment professionalProfileFragment);
    void inject(AdvertiserProfileServiceAreaFragment advertiserProfileServiceAreaFragment);
    void inject(AdvertiserProfileDetailsFragment advertiserProfileDetailsFragment);
    void inject(AdvertiserProfileAboutFragment advertiserProfileAboutFragment);
    void inject(ProfessionalSettingsFragment professionalSettingsFragment);
    void inject(ProfessionalRemoteServiceCallFragment professionalRemoteServiceCallFragment);
    void inject(ProfessionalInviteCustomerFragment professionalInviteCustomerFragment);
    void inject(ProfessionalProjectDetailsHistoryFragment professionalProjectDetailsHistoryFragment);
    void inject(ProfessionalProjectDetailsAboutFragment professionalProjectDetailsAboutFragment);


    CustomerAddCreditCardPresenter getCustomerAddCreditCardPresenter();
}
