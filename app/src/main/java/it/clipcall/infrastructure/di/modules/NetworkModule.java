package it.clipcall.infrastructure.di.modules;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import it.clipcall.common.chat.services.IChatService2;
import it.clipcall.common.environment.Environment;
import it.clipcall.common.invitations.models.eInvitationType;
import it.clipcall.common.models.UserEntity;
import it.clipcall.common.payment.persistenceModel.IQuoteService;
import it.clipcall.common.projects.models.eProjectStatusColor;
import it.clipcall.consumer.chat.services.IChatService;
import it.clipcall.consumer.payment.proxies.IPaymentService;
import it.clipcall.consumer.profile.services.ICustomerProfileService;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.services.ICustomerProjectsService;
import it.clipcall.consumer.vidoechat.services.IConsumerVideoChatService;
import it.clipcall.infrastructure.interceptors.TokenInterceptor;
import it.clipcall.infrastructure.interceptors.UrlInterceptor;
import it.clipcall.infrastructure.maps.services.GoogleMapsApiService;
import it.clipcall.infrastructure.responses.eInvitationCheckInStatus;
import it.clipcall.infrastructure.support.DateDeserializer;
import it.clipcall.infrastructure.support.HistoryItemDeserializer;
import it.clipcall.infrastructure.support.InvitationCheckInStatusDeserializer;
import it.clipcall.infrastructure.support.InvitationTypeDeserializer;
import it.clipcall.infrastructure.support.ProjectStatusColorDeserializer;
import it.clipcall.infrastructure.support.eQuoteStatusDeserializer;
import it.clipcall.infrastructure.support.eQuoteStatusReasonDeserializer;
import it.clipcall.professional.leads.models.eQuoteStatus;
import it.clipcall.professional.leads.models.eQuoteStatusReason;
import it.clipcall.professional.leads.services.IProfessionalProjectsService;
import it.clipcall.professional.payment.services.IProfessionalPaymentService;
import it.clipcall.professional.profile.services.IProfessionalProfileService;
import it.clipcall.professional.remoteservicecall.services.IProfessionalVideoChatService;
import it.clipcall.professional.validation.services.IClipCallAuthenticationService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {


    @Singleton
    @Provides
    OkHttpClient provideOkHttpClient(UserEntity userEntity){
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(120,TimeUnit.SECONDS);
        builder.connectTimeout(120, TimeUnit.SECONDS);
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override public void log(String message) {
                if(Environment.IsDebug){
                    Log.d("http", message);
                }

            }
        });
        builder.addInterceptor(new TokenInterceptor(userEntity));
        builder.addInterceptor(new UrlInterceptor(userEntity));
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(loggingInterceptor);
        return builder.build();
    }

    @Provides
    Gson provideGson(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(eInvitationCheckInStatus.class,new InvitationCheckInStatusDeserializer());
        gsonBuilder.registerTypeAdapter(eInvitationType.class,new InvitationTypeDeserializer());
        gsonBuilder.registerTypeAdapter(HistoryItem.class,new HistoryItemDeserializer());
        gsonBuilder.registerTypeAdapter(eProjectStatusColor.class,new ProjectStatusColorDeserializer());
        gsonBuilder.registerTypeAdapter(eQuoteStatus.class,new eQuoteStatusDeserializer());
        gsonBuilder.registerTypeAdapter(eQuoteStatusReason.class,new eQuoteStatusReasonDeserializer());
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        Gson gson = gsonBuilder.create();
        return gson;
    }

    @Provides
    Retrofit provideRetrofit(OkHttpClient httpClient, Gson gson){

        //gsonBuilder.registerTypeAdapter(Date.class,new DateDeserializer());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Environment.BaseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient)
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    ICustomerProjectsService provideCustomerProjectsService(Retrofit retrofit) {
        ICustomerProjectsService service = retrofit.create(ICustomerProjectsService.class);
        return service;
    }

    @Provides
    @Singleton
    IQuoteService provideQuoteService(Retrofit retrofit){
        IQuoteService service = retrofit.create(IQuoteService.class);
        return service;
    }

    @Provides
    @Singleton
    IPaymentService provideCustomerPaymentService(Retrofit retrofit) {
        IPaymentService service = retrofit.create(  IPaymentService.class);
        return service;
    }

    @Provides
    @Singleton
    IProfessionalPaymentService provideProfessionalPaymentService(Retrofit retrofit) {
        IProfessionalPaymentService service = retrofit.create(  IProfessionalPaymentService.class);
        return service;
    }

    @Provides
    @Singleton
    ICustomerProfileService provideCustomerProfileService(Retrofit retrofit) {
        ICustomerProfileService service = retrofit.create(ICustomerProfileService.class);
        return service;
    }

    @Provides
    @Singleton
    IConsumerVideoChatService provideOpenTokService(Retrofit retrofit) {
        IConsumerVideoChatService service = retrofit.create(IConsumerVideoChatService.class);
        return service;
    }

    @Provides
    @Singleton
    IClipCallAuthenticationService provideAuthenticationService(Retrofit retrofit){
        IClipCallAuthenticationService service = retrofit.create(IClipCallAuthenticationService.class);
        return service;
    }

    @Provides
    @Singleton
    GoogleMapsApiService provideGoogleMapsApiService(OkHttpClient httpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/maps/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();
        GoogleMapsApiService service = retrofit.create(GoogleMapsApiService.class);
        return service;
    }

    @Provides
    @Singleton
    IChatService provideConsumerChatService(Retrofit retrofit){
        IChatService service = retrofit.create(IChatService.class);
        return service;
    }


    @Provides
    @Singleton
    IProfessionalVideoChatService provideProfessionalVideoChatService(Retrofit retrofit){
        IProfessionalVideoChatService service = retrofit.create(IProfessionalVideoChatService.class);
        return service;
    }

    @Provides
    @Singleton
    IProfessionalProjectsService provideProfessionalProjectsService(Retrofit retrofit){
        IProfessionalProjectsService service = retrofit.create(IProfessionalProjectsService.class);
        return service;
    }

    @Provides
    @Singleton
    IChatService2 provideChatService(Retrofit retrofit){
        IChatService2 service = retrofit.create(IChatService2.class);
        return service;
    }


    @Provides
    @Singleton
    IProfessionalProfileService provideProfessionalProfileService(Retrofit retrofit){
        IProfessionalProfileService service = retrofit.create(IProfessionalProfileService.class);
        return service;
    }

}