package it.clipcall.infrastructure.di.modules;

import android.app.Activity;

import dagger.Module;
import dagger.Provides;
import it.clipcall.infrastructure.activities.BaseActivity;

@Module
public class ActivityModule {

    private BaseActivity activity;

    public ActivityModule(BaseActivity activity) {
        this.activity = activity;
    }

    @Provides
    Activity provideActivity(){
        return this.activity;
    }


    @Provides
    BaseActivity provideApplicationActivity(){
        return this.activity;
    }


/*    @Provides
    ChatPresenter provideChatPresenter(ChatControllerProvider chatControllerProvider, RoutingService routingService, PermissionsService permissionsService){
        return new ChatPresenter(chatControllerProvider,routingService, permissionsService);
    }*/

}