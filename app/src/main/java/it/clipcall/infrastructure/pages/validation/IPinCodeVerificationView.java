package it.clipcall.infrastructure.pages.validation;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by micro on 11/14/2015.
 */
public interface IPinCodeVerificationView extends IView {

    void invalidatePinCode();

    void focusNextDigit(int currentDigitIndex);

    void validatedCode();

    void showValidatingPinCode();

    void showTimePassed(long timePassed);

    void onResendingCode();

    void onCanResendCodeAgain();
}
