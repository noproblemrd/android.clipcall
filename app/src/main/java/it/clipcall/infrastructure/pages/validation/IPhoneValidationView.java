package it.clipcall.infrastructure.pages.validation;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by micro on 11/14/2015.
 */
public interface IPhoneValidationView extends IView {


    void showValidatingPhone();

    void showValidationError();

    void showValidationSuccess();

    void showRequiredPhoneNumber();

    void showInvalidPhoneNumber();
}
