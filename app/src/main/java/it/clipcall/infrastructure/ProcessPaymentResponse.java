package it.clipcall.infrastructure;

import it.clipcall.consumer.payment.models.PaymentDetails;

/**
 * Created by dorona on 09/11/2015.
 */
public class ProcessPaymentResponse {
    public String status;
    public String paymentId;
    public PaymentDetails paymentDetails;
}
