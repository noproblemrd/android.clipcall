package it.clipcall.infrastructure.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.R;
import it.clipcall.infrastructure.ui.images.CircleTransform;

@Singleton
public class SimpleNotifier implements INotificationReady {


    private static final String TAG = "SimpleNotifier";
    private int NOTIFICATION_ID = 0;

    @Inject
    NotificationManager notificationManager;

    @Inject
    Context applicationContext;

    @Inject
    public SimpleNotifier(){

    }

    public void notify(final NotificationContext notificationContext){
        final INotificationReady notificationReady = this;
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                NotificationImageTarget target = new NotificationImageTarget(notificationReady,notificationContext);
                if(Strings.isNullOrEmpty(notificationContext.bigPictureImageUrl)){
                    onNotificationReady(notificationContext,null);
                    return;
                }
                Picasso.with(applicationContext).load(notificationContext.bigPictureImageUrl).into(target);
            }
        });


    }



    @Override
    public void onNotificationReady(final NotificationContext notificationContext, Bitmap bigPicture) {
        try{
            NOTIFICATION_ID++;
            NotificationCompat.BigPictureStyle notificationStyle = new
                    NotificationCompat.BigPictureStyle();
            notificationStyle.setBigContentTitle(notificationContext.bigContentTitle);
            notificationStyle.setSummaryText(notificationContext.summaryText);
            if(bigPicture != null){
                notificationStyle.bigPicture(bigPicture);
            }
            PendingIntent pendingIntent = PendingIntent.getActivity(applicationContext, 0, notificationContext.intent, PendingIntent.FLAG_UPDATE_CURRENT);
            final RemoteViews remoteViews = new RemoteViews(applicationContext.getPackageName(),
                    R.layout.widget);
            //remoteViews.setImageViewBitmap(R.id.image,smallPicture);

            //remoteViews.setImageViewBitmap(R.id.rightImageView, smallPicture);
            remoteViews.setTextViewText(R.id.title, notificationContext.title);
            remoteViews.setTextViewText(R.id.text, notificationContext.message);
            //remoteViews.setInt(R.id.layout, "setBackgroundColor", android.R.color.darker_gray);

            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(applicationContext)
                    .setVibrate(new long[]{500,1000})
                    .setSound(uri)
                    .setSmallIcon(R.drawable.clipcall_logo)
                    .setAutoCancel(true)
                    .setNumber(1)
                    .setPriority(Notification.PRIORITY_MAX)
                    //.setLargeIcon(smallPicture)
                    .setOnlyAlertOnce(true)
                    .setContentTitle(notificationContext.contentTitle)
                    .setContentText(notificationContext.message)
                    .setContentIntent(pendingIntent)
                    .setContent(remoteViews)
                    .setAutoCancel(true)

                    .setFullScreenIntent(pendingIntent, true)
                    //.addAction(R.drawable.ic_launcher, "Add", pendingIntent)
                    //.addAction(R.drawable.ic_launcher, "Remove", pendingIntent)
                    .setStyle(notificationStyle);

            final Notification notification = mBuilder.build();
            Picasso
                    .with(applicationContext)
                    .load(notificationContext.smallPictureImageUrl)
                    .transform(new CircleTransform())
                    .into(remoteViews, R.id.image,notificationContext.notificationId , notification);

            Log.e(TAG, String.format("notifying with tag %s and id %d:", notificationContext.notificationTag, notificationContext.notificationId));

            //notificationManager.notify(NOTIFICATION_ID, notification);
        }
        catch (Exception e){
            Log.e(TAG, "onNotificationReady: error: "+ e.getMessage(), e);
        }
    }
}
