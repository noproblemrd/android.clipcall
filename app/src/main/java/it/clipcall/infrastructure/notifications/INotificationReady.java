package it.clipcall.infrastructure.notifications;

import android.graphics.Bitmap;

/**
 * Created by dorona on 04/04/2016.
 */
public interface INotificationReady {

    void onNotificationReady(NotificationContext notificationContext, Bitmap largeImage);
}
