package it.clipcall.infrastructure.notifications;

import android.content.Intent;

/**
 * Created by dorona on 31/03/2016.
 */
public class NotificationContext {
    public String title;
    public String message;
    public String bigContentTitle;

    public String notificationTag;
    public int notificationId;
    public String summaryText;
    public String bigPictureImageUrl;
    public String smallPictureImageUrl;

    public String contentTitle;
    public Intent intent;
}
