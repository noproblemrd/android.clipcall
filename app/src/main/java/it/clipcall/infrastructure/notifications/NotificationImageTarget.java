package it.clipcall.infrastructure.notifications;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.squareup.picasso.Picasso;

/**
 * Created by dorona on 04/04/2016.
 */
public class NotificationImageTarget implements com.squareup.picasso.Target {

    private final INotificationReady notificationReady;
    private final NotificationContext context;

    public NotificationImageTarget(INotificationReady notificationReady, NotificationContext context){
        this.notificationReady = notificationReady;
        this.context = context;
    }

    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
        notificationReady.onNotificationReady(context, bitmap);
    }

    @Override
    public void onBitmapFailed(Drawable errorDrawable) {
        notificationReady.onNotificationReady(context, null);
    }

    @Override
    public void onPrepareLoad(Drawable placeHolderDrawable) {

    }


}
