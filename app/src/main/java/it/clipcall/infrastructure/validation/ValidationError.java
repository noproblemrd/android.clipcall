package it.clipcall.infrastructure.validation;

/**
 * Created by dorona on 25/07/2016.
 */

public class ValidationError {
    int resourceId;
    int viewId;

    public ValidationError(int viewId, int resourceId){
        this.viewId = viewId;
        this.resourceId = resourceId;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public int getViewId() {
        return viewId;
    }

    public void setViewId(int viewId) {
        this.viewId = viewId;
    }
}
