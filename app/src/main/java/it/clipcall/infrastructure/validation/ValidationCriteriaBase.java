package it.clipcall.infrastructure.validation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dorona on 30/06/2016.
 */
public abstract class ValidationCriteriaBase<T> implements IValidationCriteria<T> {

    private final List<String> validationErrors = new ArrayList<>();

    protected void addValidationError(String errorMessage){
        validationErrors.add(errorMessage);
    }

    @Override
    public List<String> getValidationErrors() {
        return validationErrors;
    }

    @Override
    public abstract boolean isSatisfiedBy(T candidate);
}
