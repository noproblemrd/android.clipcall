package it.clipcall.infrastructure.validation;

import java.util.List;

import it.clipcall.infrastructure.support.criterias.ICriteria;

/**
 * Created by dorona on 30/06/2016.
 */
public interface IValidationCriteria<T> extends ICriteria<T> {

    List<String> getValidationErrors();
}
