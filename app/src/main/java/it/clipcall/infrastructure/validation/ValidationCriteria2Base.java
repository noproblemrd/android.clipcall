package it.clipcall.infrastructure.validation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dorona on 30/06/2016.
 */
public abstract class ValidationCriteria2Base<T> implements IValidation2Criteria<T> {

    private final List<ValidationError> validationErrors = new ArrayList<>();

    protected void addValidationError(ValidationError errorMessage){
        validationErrors.add(errorMessage);
    }

    @Override
    public List<ValidationError> getValidationErrors() {
        return validationErrors;
    }

    @Override
    public abstract boolean isSatisfiedBy(T candidate);
}
