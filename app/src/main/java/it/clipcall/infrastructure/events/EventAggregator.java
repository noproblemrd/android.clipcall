package it.clipcall.infrastructure.events;

import org.simple.eventbus.EventBus;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class EventAggregator {
    private final EventBus eventBus;

    @Inject
    public EventAggregator(){
        eventBus = EventBus.getDefault();
    }

    public void publish(Object event, String tag){
        eventBus.post(event, tag);
    }

    public void publish(Object event){
        eventBus.post(event);
    }

    public void subscribe(Object subscriber){
        eventBus.register(subscriber);
    }

    public void unSubscribe(Object subscriber) {
        eventBus.unregister(subscriber);
    }
}
