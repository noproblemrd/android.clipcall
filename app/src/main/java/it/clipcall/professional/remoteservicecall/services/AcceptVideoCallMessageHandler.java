package it.clipcall.professional.remoteservicecall.services;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;

import org.parceler.Parcels;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.vidoechat.activities.ConsumerIncomingVideoCallActivity_;
import it.clipcall.consumer.vidoechat.models.RemoteServiceCallData;
import it.clipcall.infrastructure.messageHandlers.IMessageHandler;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.professional.validation.services.AuthenticationManager;


@Singleton
public class AcceptVideoCallMessageHandler implements IMessageHandler {


    private final RoutingService routingService;
    private final AuthenticationManager authenticationManager;

    @Inject
    public AcceptVideoCallMessageHandler(RoutingService routingService, AuthenticationManager authenticationManager){

        this.routingService = routingService;
        this.authenticationManager = authenticationManager;
    }


    @Override
    public boolean canHandlerMessage(String message) {
        boolean canHandle =  "VIDEO_CHAT_JOIN_SUPPLIER".equals(message.toUpperCase());
        return canHandle;
    }

    @Override
    public void handleForegroundMessage(String from, Bundle data, Activity currentActivity) {


        if(!authenticationManager.isInProfessionalMode()){
            authenticationManager.switchToProfessionalMode();
        }

        Parcelable remoteServiceCallContext = getParcelable(data);

        NavigationContext navigationContext = new NavigationContext(new RoutingContext(currentActivity));
        navigationContext.bundle = new Bundle();
        navigationContext.bundle.putParcelable("remoteServiceCallContext", remoteServiceCallContext);
        routingService.routeTo(Routes.ProfessionalAcceptVideoCallRoute,navigationContext);
    }

    private Parcelable getParcelable(Bundle data) {
        String customerPhone = data.getString("customerPhone");
        String profileImage = data.getString("customerImage");
        String invitationId = data.getString("invitationId");
        String apiKey = data.getString("apiKey");
        String sessionId = data.getString("sessionId");
        String token = data.getString("token");
        String message = data.getString("message");
        String title = data.getString("title");

        RemoteServiceCallData remoteHouseCallData = new RemoteServiceCallData();
        remoteHouseCallData.phone = customerPhone;
        remoteHouseCallData.profileImage = profileImage;
        remoteHouseCallData.invitationId = invitationId;
        remoteHouseCallData.apiKey = apiKey;
        remoteHouseCallData.sessionId = sessionId;
        remoteHouseCallData.token = token;
        remoteHouseCallData.message = message;
        remoteHouseCallData.title = title;
        remoteHouseCallData.professionalId = authenticationManager.getSupplierId();

        return Parcels.wrap(remoteHouseCallData);
    }

    @Override
    public void handleBackgroundMessage(String from, Bundle data, Application application) {
        Intent intent = new Intent(application.getApplicationContext(), ConsumerIncomingVideoCallActivity_.class);
        Parcelable parcelable = getParcelable(data);
        Bundle bundle =  new Bundle();
        bundle.putParcelable("remoteServiceCallContext", parcelable);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(bundle);

        application.startActivity(intent);
    }
}
