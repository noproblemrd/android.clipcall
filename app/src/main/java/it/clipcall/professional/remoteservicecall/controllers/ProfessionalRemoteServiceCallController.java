package it.clipcall.professional.remoteservicecall.controllers;

import android.content.Context;

import com.google.common.base.Strings;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.branch.referral.Branch;
import it.clipcall.professional.remoteservicecall.services.ProfessionalVideoChatManager;
import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.common.invitations.services.BranchIoManager;
import it.clipcall.consumer.vidoechat.models.CustomerVideoInvitationData;

/**
 * Created by dorona on 18/01/2016.
 */
@Singleton
public class ProfessionalRemoteServiceCallController {

    private final AuthenticationManager authenticationManager;
    private final ProfessionalVideoChatManager videoChatManager;
    private final BranchIoManager branchIoManager;

    @Inject
    public ProfessionalRemoteServiceCallController(AuthenticationManager authenticationManager, ProfessionalVideoChatManager videoChatManager, BranchIoManager branchIoManager) {
        this.authenticationManager = authenticationManager;
        this.videoChatManager = videoChatManager;
        this.branchIoManager = branchIoManager;
    }

    public CustomerVideoInvitationData createVideoChatInvitation() {
        String supplierId = authenticationManager.getSupplierId();
        if(Strings.isNullOrEmpty(supplierId))
            return null;

        CustomerVideoInvitationData videoChatInvitation = videoChatManager.createVideoChatInvitation(supplierId);
        return videoChatInvitation;
    }
    public void generateLink(Context context, CustomerVideoInvitationData invitationData, Branch.BranchLinkCreateListener listener){
        branchIoManager.generateLink(context, invitationData, listener);
    }
}
