package it.clipcall.professional.remoteservicecall.presenters;

import android.content.Context;

import javax.inject.Inject;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import it.clipcall.professional.remoteservicecall.controllers.ProfessionalRemoteServiceCallController;
import it.clipcall.professional.remoteservicecall.views.IProfessionalRemoteServiceCallView;
import it.clipcall.consumer.vidoechat.models.CustomerVideoInvitationData;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;

@PerActivity
public class ProfessionalRemoteServiceCallPresenter extends PresenterBase<IProfessionalRemoteServiceCallView> implements Branch.BranchLinkCreateListener {


    private final ProfessionalRemoteServiceCallController controller;
    private final RoutingService routingService;

    private CustomerVideoInvitationData currentInvitationData;

    @Inject
    public ProfessionalRemoteServiceCallPresenter(ProfessionalRemoteServiceCallController controller, RoutingService routingService) {
        this.controller = controller;
        this.routingService = routingService;
    }

    @Override
    public void initialize() {
        super.initialize();
    }

    public void inviteCustomer(Context context) {
        view.showCreateInvitationProcessing();
        currentInvitationData = controller.createVideoChatInvitation();
        if(currentInvitationData == null){
            view.createInvitationProcessingCompleted();
            return;
        }

        controller.generateLink(context, currentInvitationData, this);
    }

    @Override
    public void onLinkCreate(String url, BranchError error) {
        if(error != null || currentInvitationData == null){
            view.createInvitationProcessingCompleted();
            return;
        }
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        String message = currentInvitationData.getMessage();
        String finalMessage = message.replace("{0}", url);
        navigationContext.addParameter("message",finalMessage);
        routingService.routeTo(Routes.ProfessionalServiceCallInvitationRoute, navigationContext);

    }
}
