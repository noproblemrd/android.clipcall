package it.clipcall.professional.remoteservicecall.views;

import it.clipcall.consumer.vidoechat.models.RemoteServiceCallData;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 17/01/2016.
 */
public interface IProfessionalIncomingVideoCallView extends IView {


    void showRemoteServiceCall(RemoteServiceCallData remoteServiceCallData);

    void showAcceptingCallInProgress();

}
