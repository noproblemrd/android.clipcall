package it.clipcall.professional.remoteservicecall.presenters;


import android.Manifest;
import android.os.AsyncTask;
import android.os.Bundle;

import org.parceler.Parcels;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.vidoechat.models.RemoteServiceCallData;
import it.clipcall.consumer.vidoechat.models.VideoChatContext;
import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.infrastructure.audio.SimpleMediaPlayer;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.ui.permissions.IPermissionsHandler;
import it.clipcall.infrastructure.ui.permissions.PermissionContext;
import it.clipcall.infrastructure.ui.permissions.PermissionsService;
import it.clipcall.professional.remoteservicecall.controllers.ProfessionalVideoChatController;
import it.clipcall.professional.remoteservicecall.views.IProfessionalIncomingVideoCallView;


@PerActivity
public class ProfessionalIncomingVideoCallPresenter extends PresenterBase<IProfessionalIncomingVideoCallView> {

    private final SimpleMediaPlayer simpleMediaPlayer;
    private final PermissionsService permissionsService;
    private final RoutingService routingService;
    private final ProfessionalVideoChatController controller;
    private final EventAggregator eventAggregator;

    RemoteServiceCallData remoteServiceCallData;

    @Inject
    public ProfessionalIncomingVideoCallPresenter(SimpleMediaPlayer simpleMediaPlayer, PermissionsService permissionsService, RoutingService routingService, ProfessionalVideoChatController controller, EventAggregator eventAggregator){
        this.simpleMediaPlayer = simpleMediaPlayer;
        this.permissionsService = permissionsService;
        this.routingService = routingService;
        this.controller = controller;
        this.eventAggregator = eventAggregator;
    }

    @Override
    public void initialize() {
        simpleMediaPlayer.setAudioTrack(R.raw.video_chat_incoming_call_ring);
        simpleMediaPlayer.setIsLooping(true);
        simpleMediaPlayer.play();
        view.showRemoteServiceCall(remoteServiceCallData);
    }

    public void connectToVideoSessionInBackground(){
        VideoSessionData videoSessionData = new VideoSessionData(Integer.parseInt(remoteServiceCallData.apiKey),remoteServiceCallData.token,  remoteServiceCallData.sessionId);
        controller.connectToVideoChatInBackground(videoSessionData);
    }

    public void initializePublisher(){
        controller.initializePublisher(remoteServiceCallData.phone);
    }

    @Override
    public void shutDown() {
        simpleMediaPlayer.stop();
    }

    public void setRemoteServiceCallData(RemoteServiceCallData remoteServiceCallData){
        this.remoteServiceCallData = remoteServiceCallData;
    }

    public void acceptCall() {

        view.showAcceptingCallInProgress();
        simpleMediaPlayer.stop();
        PermissionContext permissionContext = new PermissionContext("Permissions needed", "We need your permissions to start a video chat", Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO);
        permissionsService.requestPermissions(permissionContext, new IPermissionsHandler() {
            @Override
            public void handleAllPermissionsGranted() {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        acceptCallCore();
                    }
                });
            }

            @Override
            public void handlePermissionDenied() {

            }
        });
    }


    private void acceptCallCore(){
        boolean callAnswered = controller.answerVideoChatCall(remoteServiceCallData.invitationId);
        if(!callAnswered)
            return;

        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.bundle = new Bundle();
        VideoSessionData videoSessionData = new VideoSessionData(Integer.parseInt(remoteServiceCallData.apiKey),remoteServiceCallData.token,  remoteServiceCallData.sessionId);


        VideoChatContext videoChatContext = new VideoChatContext();
        videoChatContext.isInitiatingCall = false;
        videoChatContext.videoSessionData = videoSessionData;
        videoChatContext.profileName = remoteServiceCallData.phone;
        videoChatContext.profileImageUrl = remoteServiceCallData.profileImage;
        videoChatContext.invitationId = remoteServiceCallData.invitationId;

        navigationContext.bundle.putParcelable("videoChatContext", Parcels.wrap(videoChatContext));
        routingService.routeTo(Routes.ProfessionalVideoChatRoute, navigationContext);
    }

    public void denyCall() {
        routingService.routeToHome(view.getRoutingContext());

    }
}
