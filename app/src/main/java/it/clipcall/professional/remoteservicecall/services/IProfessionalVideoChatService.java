package it.clipcall.professional.remoteservicecall.services;

import it.clipcall.consumer.vidoechat.models.CustomerVideoInvitationData;
import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.infrastructure.requests.VideoInvitationRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by dorona on 14/12/2015.
 */
public interface IProfessionalVideoChatService {

    @POST("suppliers/{supplierId}/videochat/create")
    Call<CustomerVideoInvitationData> createVideoInvitationData(@Path("supplierId") String supplierId, @Body VideoInvitationRequest videoInvitationRequest);


    @POST("suppliers/{supplierId}/videochat/{invitationId}/answer")
    Call<StatusResponse> answerVideoChatCall(@Path("supplierId") String  supplierId, @Path("invitationId") String  invitationId);

    @POST("suppliers/{supplierId}/videochat/createvideochat/{leadAccountId}")
    Call<VideoSessionData> createVideoChatSession(@Path("supplierId") String supplierId, @Path("leadAccountId") String leadAccountId);

}
