package it.clipcall.professional.remoteservicecall.controllers;

import com.google.common.base.Strings;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.invitations.services.BranchIoManager;
import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.consumer.vidoechat.services.VideoChatCoordinator;
import it.clipcall.professional.remoteservicecall.services.ProfessionalVideoChatManager;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by dorona on 18/01/2016.
 */
@Singleton
public class ProfessionalVideoChatController {

    private final AuthenticationManager authenticationManager;
    private final ProfessionalVideoChatManager videoChatManager;
    private final BranchIoManager branchIoManager;
    private final VideoChatCoordinator videoChatCoordinator;

    @Inject
    public ProfessionalVideoChatController(AuthenticationManager authenticationManager, ProfessionalVideoChatManager videoChatManager, BranchIoManager branchIoManager, VideoChatCoordinator videoChatCoordinator) {
        this.authenticationManager = authenticationManager;
        this.videoChatManager = videoChatManager;
        this.branchIoManager = branchIoManager;
        this.videoChatCoordinator = videoChatCoordinator;
    }

    public boolean answerVideoChatCall(String invitationId) {
        String supplierId = authenticationManager.getSupplierId();
        if(Strings.isNullOrEmpty(supplierId))
            return false;



        boolean accepted = videoChatManager.answerVideoChatCall(supplierId, invitationId);
        return accepted;

    }

    public void connectToVideoChatInBackground(VideoSessionData videoSessionData) {
        videoChatCoordinator.connectToSession(videoSessionData);
    }

    public void initializePublisher(String publisherName) {
        videoChatCoordinator.initializePublisher(publisherName);
    }
}
