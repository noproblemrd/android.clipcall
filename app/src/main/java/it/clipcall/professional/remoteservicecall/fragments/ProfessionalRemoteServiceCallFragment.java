package it.clipcall.professional.remoteservicecall.fragments;

import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.professional.remoteservicecall.presenters.ProfessionalRemoteServiceCallPresenter;
import it.clipcall.professional.remoteservicecall.views.IProfessionalRemoteServiceCallView;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.routing.models.RoutingContext;

@EFragment(R.layout.professional_remote_service_call_fragment)
public class ProfessionalRemoteServiceCallFragment extends BaseFragment implements IProfessionalRemoteServiceCallView {

    @Inject
    ProfessionalRemoteServiceCallPresenter presenter;

    @ViewById
    Button inviteCustomerButton;

    @ViewById
    ProgressBar loadingSpinnerProgressBar;

    @AfterViews
    void afterAboutViewLoaded(){
        presenter.bindView(this);
        initialize();
    }

    @Background
    void initialize(){
        presenter.initialize();
    }

    @Override
    public RoutingContext getRoutingContext() {
        return new RoutingContext(getActivity());
    }

    @Override
    protected String getTitle() {
        return "REMOTE SERVICE CALL";
    }

    @Override
    protected int getMenuItemId() {
        return R.id.professional_remote_service_call_menu_item;
    }


    @Override
    public void onResume() {
        super.onResume();
        createInvitationProcessingCompleted();

    }

    @Background
    @Click(R.id.inviteCustomerButton)
    void inviteCustomerTapped(){
        presenter.inviteCustomer(getActivity());
    }

    @UiThread
    @Override
    public void showCreateInvitationProcessing() {
        loadingSpinnerProgressBar.setVisibility(View.VISIBLE);
        inviteCustomerButton.setVisibility(View.INVISIBLE);
    }

    @UiThread
    @Override
    public void createInvitationProcessingCompleted() {
        loadingSpinnerProgressBar.setVisibility(View.INVISIBLE);
        inviteCustomerButton.setVisibility(View.VISIBLE);
    }
}
