package it.clipcall.professional.remoteservicecall.services;

import com.google.common.base.Strings;
import com.opentok.android.Session;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.invitations.models.eInvitationType;
import it.clipcall.consumer.vidoechat.models.CustomerVideoInvitationData;
import it.clipcall.consumer.vidoechat.models.VideoInvitationData;
import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.infrastructure.requests.VideoInvitationRequest;
import it.clipcall.infrastructure.support.network.RetrofitCallProcessor;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by dorona on 14/12/2015.
 */
@Singleton
public class ProfessionalVideoChatManager {

    private final IProfessionalVideoChatService videoChatService;

    private VideoInvitationData invitationData;
    private VideoSessionData videoSessionData;
    private Session mSession;


    @Inject
    public ProfessionalVideoChatManager(IProfessionalVideoChatService openTokService){
        this.videoChatService = openTokService;
    }

    public CustomerVideoInvitationData createVideoChatInvitation(String advertiserId) {
        CustomerVideoInvitationData invitationData = createInvitation(advertiserId, eInvitationType.VideoChat);
        return invitationData;
    }


    public CustomerVideoInvitationData createVideoInvitation(String advertiserId) {
        CustomerVideoInvitationData invitationData = createInvitation(advertiserId, eInvitationType.Video);
        return invitationData;
    }


    private CustomerVideoInvitationData createInvitation(String advertiserId, eInvitationType invitationType) {
        if(Strings.isNullOrEmpty(advertiserId))
            return null;


        VideoInvitationRequest request = new VideoInvitationRequest();
        request.setSupplierId(advertiserId);
        request.setInvitationType(invitationType);
        try {
            Response<CustomerVideoInvitationData> response = videoChatService.createVideoInvitationData(advertiserId, request).execute();
            if(response == null)
                return null;

            CustomerVideoInvitationData videoInvitationData = response.body();
            if(videoInvitationData == null)
                return null;

            return videoInvitationData;
        } catch (IOException e) {

            return null;
        }
    }

    public boolean answerVideoChatCall(String supplierId, String invitationId) {
        try {
            Response<StatusResponse> response = videoChatService.answerVideoChatCall(supplierId, invitationId).execute();
            if(response == null)
                return false;

            StatusResponse statusData = response.body();
            if(statusData == null)
                return false;

            return "OK".equals(statusData.status);

        } catch (Exception e) {
            return false;
        }
    }

    public VideoSessionData createVideoChatSession(String supplierId, String leadAccountId) {
        Call<VideoSessionData> videoChatCall = videoChatService.createVideoChatSession(supplierId, leadAccountId);
        VideoSessionData videoSessionData = RetrofitCallProcessor.processCall(videoChatCall);
        return videoSessionData;
    }
}
