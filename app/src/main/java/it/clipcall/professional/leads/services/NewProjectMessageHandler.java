package it.clipcall.professional.leads.services;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.activities.SplashActivity_;
import it.clipcall.infrastructure.messageHandlers.IMessageHandler;
import it.clipcall.infrastructure.notifications.NotificationContext;
import it.clipcall.infrastructure.notifications.SimpleNotifier;
import it.clipcall.infrastructure.repositories.Repository2;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.infrastructure.support.dates.DateFormatter;
import it.clipcall.professional.leads.models.NewProject;
import it.clipcall.professional.leads.models.NewProjectCollection;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class NewProjectMessageHandler implements IMessageHandler {

    private final RoutingService routingService;
    private final AuthenticationManager authenticationManager;
    private final SimpleNotifier simpleNotifier;
    private final Repository2 repository;


    private final List<String> supportedMessages = new ArrayList<>();

    private final int NotificaitonId = 10;

    @Inject
    public NewProjectMessageHandler(RoutingService routingService, AuthenticationManager authenticationManager, SimpleNotifier simpleNotifier, Repository2 repository){
        this.routingService = routingService;
        this.authenticationManager = authenticationManager;
        this.simpleNotifier = simpleNotifier;
        this.repository = repository;

        supportedMessages.add("PRIVATE_VIDEO_REQUEST");
        supportedMessages.add("BID_REQUEST");
        supportedMessages.add("EXCLUSIVE_VIDEO_REQUEST");


    }

    @Override
    public boolean canHandlerMessage(final String message) {

        boolean canHandle = Lists.any(supportedMessages, new ICriteria<String>() {
            @Override
            public boolean isSatisfiedBy(String candidate) {
                return candidate.equals(message.toUpperCase());
            }
        });
        return canHandle;
    }

    @Override
    public void handleForegroundMessage(String from, Bundle data, Activity activeActivity) {

        if(!authenticationManager.isInProfessionalMode()){
            authenticationManager.switchToProfessionalMode();
        }

        NewProject project = getNewProject(data);
        Parcelable context =  Parcels.wrap(project);

        NavigationContext navigationContext = new NavigationContext(new RoutingContext(activeActivity));
        navigationContext.bundle = new Bundle();
        navigationContext.bundle.putParcelable("context", context);
        routingService.routeTo(Routes.ProfessionalNewProjectRoute,navigationContext);

    }

    @Override
    public void handleBackgroundMessage(String from, Bundle data, Application application) {
        if(!authenticationManager.isInProfessionalMode()){
            authenticationManager.switchToProfessionalMode();
        }

        NewProject project = getNewProject(data);

        NotificationContext notificationContext = new NotificationContext();
        notificationContext.title = data.getString("title");
        notificationContext.message = data.getString("message");
        notificationContext.bigContentTitle = data.getString("title");
        notificationContext.bigPictureImageUrl = project.getVideoPicUrl();
        notificationContext.contentTitle = data.getString("title");
        notificationContext.smallPictureImageUrl = project.getVideoPicUrl();
        notificationContext.summaryText = data.getString("message");
        notificationContext.notificationId = NotificaitonId;
        notificationContext.notificationTag = project.getLeadAccountId();

        Intent intent = new Intent(application.getApplicationContext(), SplashActivity_.class);

        NewProjectCollection newProjects = repository.getSingle(NewProjectCollection.class);
        if(newProjects == null){
            newProjects = new NewProjectCollection();
            newProjects.setProjects(new ArrayList<NewProject>());
        }

        newProjects.addProject(project);
        repository.update(newProjects);

        notificationContext.intent = intent;
        simpleNotifier.notify(notificationContext);
    }

    private NewProject getNewProject(Bundle data) {
        String leadAccountId = data.getString("leadAccountId");
        String leadId = data.getString("leadId");
        String categoryName = data.getString("category");
        String zipCode = data.getString("zipCode");
        String createDateString = data.getString("timeStampUtc");
        String address = data.getString("address");
        String customerName = data.getString("customerName");
        String customerPhoneNumber = data.getString("phoneNumber");
        String videoUrl = data.getString("videoUrl");
        String videoPicUrl = data.getString("videoPicUrl");
        int videoDuration = Integer.parseInt(data.getString("videoDuration"));
        boolean isFreeBid = Boolean.parseBoolean(data.getString("isFreeBid"));
        boolean isPrivateVideo = Boolean.parseBoolean(data.getString("true"));


        Date createDate = DateFormatter.toDate(createDateString);
        NewProject lead = new NewProject();
        lead.setLeadId(leadId);
        lead.setLeadAccountId(leadAccountId);
        lead.setCategoryName(categoryName);
        lead.setZipCode(zipCode);
        lead.setCreateDate(createDate);
        lead.setAddress(address);
        lead.setCustomerName(customerName);
        lead.setCustomerPhoneNum(customerPhoneNumber);
        lead.setVideoUrl(videoUrl);
        lead.setVideoPicUrl(videoPicUrl);
        lead.setVideoDuration(videoDuration);
        lead.setFreeBid(isFreeBid);
        lead.setPrivateVideo(isPrivateVideo);

        return lead;
    }
}