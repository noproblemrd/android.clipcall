package it.clipcall.professional.leads.fragments;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionsMenu;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.common.projects.support.IProjectsListener;
import it.clipcall.consumer.mainMenu.support.IBadgeListener;
import it.clipcall.consumer.projects.views.IProjectsView;
import it.clipcall.consumer.vidoechat.support.GoneViewAnimatinoListener;
import it.clipcall.consumer.vidoechat.support.ShowViewAnimatinoListener;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.routing.models.FragmentRoutingContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.support.fragments.IHandlesBackPressed;
import it.clipcall.infrastructure.support.recyclerviews.ActionInvokerListener;
import it.clipcall.infrastructure.viewModel.ItemsWidget;
import it.clipcall.infrastructure.viewModel.support.recycleviews.ItemSelectedListener;
import it.clipcall.infrastructure.viewModel.support.recycleviews.ItemsLoader;
import it.clipcall.infrastructure.viewModel.support.recycleviews.RecyclerViewAdapterBase;
import it.clipcall.professional.leads.models.LeadEntity;
import it.clipcall.professional.leads.presenters.ProfessionalProjectsPresenter;
import it.clipcall.professional.leads.views.ProfessionalProjectsItemView;
import it.clipcall.professional.leads.views.ProfessionalProjectsItemView_;

@EFragment(R.layout.professional_projects_fragment)
public class ProfessionalProjectsFragment extends BaseFragment implements IProjectsView, IProjectsListener, FloatingActionsMenu.OnFloatingActionsMenuUpdateListener, IHandlesBackPressed, ItemsLoader<ProjectEntityBase>,ActionInvokerListener<ProjectEntityBase>,RecyclerViewAdapterBase.IViewBuilder<ProjectEntityBase>,ItemSelectedListener<ProjectEntityBase> {
    @Inject
    ProfessionalProjectsPresenter presenter;

    @ViewById
    ItemsWidget<ProjectEntityBase> projectsItemsWidget;

    @ViewById
    FloatingActionsMenu projectActionsfloatingActionMenu;

    @ViewById
    ViewGroup dimBackgroundContainer;

    SearchView searchView;

    MenuItem searchMenuItem;

    @Background
    void getProfessionalProjects(){
        presenter.getProfessionalProjects(true);
    }


    @Background
    void filterProfessionalProjects(String filter){
        presenter.filterProfessionalProjects(filter);
    }

    private void closeSearchMenu(){
        searchView.onActionViewCollapsed();
        MenuItemCompat.collapseActionView(searchMenuItem);
    }

    @AfterViews
    public void afterProjectFragmentViewsLoaded(){
        presenter.bindView(this);
        projectsItemsWidget.setItemSelectedListener(this);
        projectsItemsWidget.setItemsLoader(this);
        projectsItemsWidget.setActionInvokerListener(this);
        projectsItemsWidget.setViewBuilder(this);
        projectActionsfloatingActionMenu.setOnFloatingActionsMenuUpdateListener(this);
    }

    @Override
    public RoutingContext getRoutingContext() {
        Activity activity = getActivity();
        if(!(activity instanceof BaseActivity))
            return null;
        RoutingContext routingContext = ((BaseActivity) activity).getRoutingContext();
        return routingContext;
    }

    @Override
    public void setProjects(List<ProjectEntityBase> projects) {
        projectsItemsWidget.setItems(projects);
    }

    @Override
    public void setProjectsAfterInvalidating(final List<ProjectEntityBase> projects) {
        projectsItemsWidget.invalidateItems();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                projectsItemsWidget.setItems(projects);
            }
        }, 200);
    }

    @Override
    public void setFilteredProjects(List<ProjectEntityBase> projects){
        projectsItemsWidget.setItems(projects);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setHasOptionsMenu(true);
    }

    @UiThread
    @Override
    public void onProjectActionExecuted(ProjectEntityBase project) {
        Toast.makeText(getActivity(), "calling the customer", Toast.LENGTH_LONG).show();
    }

    @Override
    public RoutingContext getFragmentRoutingContext() {
        RoutingContext routingContext = new FragmentRoutingContext(getFragmentManager(), R.id.menu_fragment_container, getActivity());
        return routingContext;
    }

    @UiThread
    @Override
    public void showTotalUnreadMessages(int totalUnreadMessages) {
        if(getContext() instanceof IBadgeListener){
            IBadgeListener listener = (IBadgeListener) getContext();
            listener.showBadge(totalUnreadMessages);
        }
    }

    @Override
    protected String getTitle() {
        return "My Projects";
    }

    @Override
    protected int getMenuItemId() {
        return R.id.professionalProjectsMenuItem;
    }

    @Override
    public void onProjectDetailsTapped(ProjectEntityBase project, Bundle uiBundle) {
        presenter.projectSelected((LeadEntity)project,uiBundle);
    }

    @Override
    public void onProjectPlayVideoTapped(ProjectEntityBase project) {
        presenter.showVideo((LeadEntity) project);
    }

    @Override
    public void onProjectActionTapped(ProjectEntityBase project) {
        presenter.executeProjectAction((LeadEntity)project);
    }

    @Click(R.id.remoteHouseCallButton)
    void remoteHouseCallTapped(){
        presenter.navigateToRemoteHouseCall();
    }

    @Click(R.id.inviteCustomerButton)
    void inviteCustomerTapped(){
        presenter.navigateToInviteCustomer();
    }

    @Override
    public void onMenuExpanded() {

        Animation fadeIn = new AlphaAnimation(0, 0.6f);
        fadeIn.setFillAfter(true);
        fadeIn.setFillEnabled(true);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(1000);
        fadeIn.setAnimationListener(new ShowViewAnimatinoListener(dimBackgroundContainer));
        dimBackgroundContainer.startAnimation(fadeIn);
        dimBackgroundContainer.setClickable(true);
        dimBackgroundContainer.setFocusable(true);
    }

    @Override
    public void onMenuCollapsed() {

        Animation fadeIn = new AlphaAnimation(0.6f, 0);
        fadeIn.setFillAfter(true);
        fadeIn.setFillEnabled(true);
        fadeIn.setInterpolator(new AccelerateInterpolator()); //add this
        fadeIn.setDuration(1000);
        fadeIn.setAnimationListener(new GoneViewAnimatinoListener(dimBackgroundContainer));

        dimBackgroundContainer.startAnimation(fadeIn);
        dimBackgroundContainer.setClickable(false);
        dimBackgroundContainer.setFocusable(false);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(projectActionsfloatingActionMenu == null)
            return;

        if(projectActionsfloatingActionMenu.isExpanded()){
            projectActionsfloatingActionMenu.collapse();
        }
    }

    @Override
    public boolean handleBackPressed() {
        if(projectActionsfloatingActionMenu.isExpanded()){
            projectActionsfloatingActionMenu.collapse();
            return true;
        }

        if (!searchView.isIconified()) {
            closeSearchMenu();
            return true;
        }

        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.bindView(this);
        presenter.initialize();
        getProfessionalProjects();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.shutDown();
    }

    int lastBadgeVisibility;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.professional_projects_menu, menu);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.searchProjectsMenuItem);
        searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener()
        {
            public boolean onQueryTextChange(String newText)
            {
                filterProfessionalProjects(newText);
                return true;
            }

            public boolean onQueryTextSubmit(String query)
            {
                return true;
            }
        };

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                IBadgeListener listener = (IBadgeListener) getContext();
                listener.setBadgeVisibility(lastBadgeVisibility);
                return false;
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IBadgeListener listener = (IBadgeListener) getContext();
                lastBadgeVisibility = listener.getBadgeVisibility();
                listener.setBadgeVisibility(View.GONE);
            }
        });

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setIconifiedByDefault(true);
        searchView.setOnQueryTextListener(queryTextListener);
        searchView.setVisibility(View.VISIBLE);


        super.onCreateOptionsMenu(menu, inflater);


    }

    @Override
    public void loadItems() {
        closeSearchMenu();
        getProfessionalProjects();
    }

    @Override
    public void onInvokeAction(ProjectEntityBase item) {
        presenter.playVideo(item);
    }

    @Override
    public ProfessionalProjectsItemView  build(int viewType) {
        return ProfessionalProjectsItemView_.build(ProfessionalProjectsFragment.this.getActivity());
    }

    @Override
    public void onItemSelected(ProjectEntityBase item) {

    }

    @Override
    public void onItemSelected(ProjectEntityBase item, Bundle uiData) {
        presenter.projectSelected((LeadEntity)item,uiData);
        closeSearchMenu();
    }

    @Override
    public void onItemSelected(ProjectEntityBase item, Bundle uiData, Bundle data) {

    }
}