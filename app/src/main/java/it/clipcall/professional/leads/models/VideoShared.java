package it.clipcall.professional.leads.models;

/**
 * Created by dorona on 21/04/2016.
 */
public class VideoShared {
    private String message;
    private boolean success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
