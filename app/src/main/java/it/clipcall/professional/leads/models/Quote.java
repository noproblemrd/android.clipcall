package it.clipcall.professional.leads.models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by dorona on 26/06/2016.
 */
public class Quote implements Serializable {

    String id;

    Date createdOn;

    Date startJobAt;

    String description;

    double price;

    int number;

    eQuoteStatus status;

    eQuoteStatusReason statusReason;

    public Date getStartJobAt() {
        return startJobAt;
    }

    public void setStartJobAt(Date startJobAt) {
        this.startJobAt = startJobAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNumber(){
        return this.number;
    }

    public void setNumber(int number) {
        this.number = number;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public eQuoteStatus getStatus() {
        return status;
    }

    public void setStatus(eQuoteStatus status) {
        this.status = status;
    }

    public eQuoteStatusReason getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(eQuoteStatusReason statusReason) {
        this.statusReason = statusReason;
    }

    public boolean isActive() {
        return status.equals(eQuoteStatus.Open);
    }

    public boolean isBooked() {
        return status.equals(eQuoteStatus.Closed) && statusReason.equals(eQuoteStatusReason.Booked);
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public boolean isLost() {
        return status.equals(eQuoteStatus.Closed) && statusReason.equals(eQuoteStatusReason.Lost);
    }

    public boolean isPending() {
        return status.equals(eQuoteStatus.Open) && statusReason.equals(eQuoteStatusReason.NewQuote);
    }

    public void markAsLost() {
        setStatus(eQuoteStatus.Closed);
        setStatusReason(eQuoteStatusReason.Lost);
    }
}
