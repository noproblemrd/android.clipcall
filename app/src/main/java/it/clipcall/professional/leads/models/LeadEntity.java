package it.clipcall.professional.leads.models;

import org.parceler.Parcel;

import it.clipcall.common.chat.models.IConversation;
import it.clipcall.common.chat.models.IMessage;
import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.common.projects.models.ProjectEntityBase;

@Parcel
public class LeadEntity extends ProjectEntityBase {

    String categoryName;

    String leadId;
    String leadAccountId;
    String customerProfileImage;
    String wonTitle;
    String customerId;
    boolean isAnonymous;

    ProfessionalProjectDetails projectDetails;

    Quote lastQuote;


    @Override
    public String getId() {
        return getLeadAccountId();
    }

    public String getCategoryName() {
        return categoryName;
    }

    @Override
    public String getProfileUrl() {
        return getCustomerProfileImage();
    }

    @Override
    public String getName() {
        if(projectDetails == null)
            return "";

        return projectDetails.getCustomerDisplayName();
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getLeadAccountId() {
        return leadAccountId;
    }

    public void setLeadAccountId(String leadAccountId) {
        this.leadAccountId = leadAccountId;
    }

    public String getCustomerProfileImage() {
        return customerProfileImage;
    }

    public void setCustomerProfileImage(String customerProfileImage) {
        this.customerProfileImage = customerProfileImage;
    }

    public String getWonTitle() {
        return wonTitle;
    }

    public void setWonTitle(String wonTitle) {
        this.wonTitle = wonTitle;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @Override
    public void updateUnreadMessageCount(ChatManager chatManager) {
        IConversation conversation  = chatManager.getConversationWithParticipant(getCustomerId(), getLeadAccountId());
        if(conversation == null)
            return;

        Integer totalUnreadMessages = conversation.getTotalUnreadMessageCount();
        if(totalUnreadMessages == null)
            return;

         setTotalUnreadMessages(totalUnreadMessages);
    }

    @Override
    public void updateLastUnreadMessage(ChatManager chatManager, String receiverId) {
        if(isNotHired()){
            setLastUnreadMessageDate(null);
            return;
        }

        IConversation conversation  = chatManager.getConversationWithParticipant(getCustomerId(), getLeadAccountId());
        if(conversation == null)
            return;

        IMessage lastUnreadMessage = conversation.getLastUnreadMessage(getCustomerId(), receiverId);
        if(lastUnreadMessage != null)
        setLastUnreadMessageDate(lastUnreadMessage.getSentAt());
    }


    public void setProjectDetails(ProfessionalProjectDetails details) {
        this.projectDetails = details;
    }

    @Override
    public boolean isNotHired(){
        return "NOT HIRED".equals(getStatus().getStatus().toUpperCase());
    }

    public ProfessionalProjectDetails getProjectDetails() {
        return projectDetails;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public boolean getIsAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(boolean isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    public Quote getLastQuote() {
        return lastQuote;
    }

    @Override
    public Quote getLeadAccountLastQuote(String leadAccountId) {
        if(!getLeadAccountId().equals(leadAccountId))
            return null;

        return getLastQuote();
    }

    @Override
    public boolean isBooked() {
        return getStatus().getStatus().toUpperCase().equals("ALREADY BOOKED");
    }

    public void setLastQuote(Quote lastQuote) {
        this.lastQuote = lastQuote;
    }

    public boolean isMissed() {
        return "MISSED".equals(getStatus().getStatus().toUpperCase());
    }
}