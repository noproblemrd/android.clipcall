package it.clipcall.professional.leads.models;

/**
 * Created by dorona on 03/07/2016.
 */
public enum  eQuoteStatus {
    Open("Open"),
    Closed("Closed");

    private final String status;

    eQuoteStatus(String status) {
        this.status = status;
    }

}
