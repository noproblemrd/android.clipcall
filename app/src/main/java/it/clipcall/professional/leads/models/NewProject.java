package it.clipcall.professional.leads.models;


import org.parceler.Parcel;

import java.io.Serializable;
import java.util.Date;

@Parcel
public class NewProject implements Serializable {
    String leadId;
    String leadAccountId;
    String categoryName;
    String zipCode;
    Date createDate;
    String address;
    String customerName;
    String customerPhoneNum;
    String videoUrl;
    String videoPicUrl;
    int videoDuration;
    boolean isFreeBid;
    boolean isPrivateVideo;


    public NewProject(){

    }

    public String getLeadAccountId() {
        return leadAccountId;
    }

    public void setLeadAccountId(String leadAccountId) {
        this.leadAccountId = leadAccountId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhoneNum() {
        return customerPhoneNum;
    }

    public void setCustomerPhoneNum(String customerPhoneNum) {
        this.customerPhoneNum = customerPhoneNum;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoPicUrl() {
        return videoPicUrl;
    }

    public void setVideoPicUrl(String videoPicUrl) {
        this.videoPicUrl = videoPicUrl;
    }

    public int getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(int videoDuration) {
        this.videoDuration = videoDuration;
    }

    public boolean isFreeBid() {
        return isFreeBid;
    }

    public void setFreeBid(boolean freeBid) {
        isFreeBid = freeBid;
    }

    public boolean isPrivateVideo() {
        return isPrivateVideo;
    }

    public void setPrivateVideo(boolean privateVideo) {
        isPrivateVideo = privateVideo;
    }

    @Override
    public boolean equals(Object o) {
        return this.getLeadAccountId().equals(((NewProject)o).getLeadAccountId());
    }

    @Override
    public int hashCode() {
        return getLeadAccountId().hashCode();
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }
}
