package it.clipcall.professional.leads.cotrollers;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.common.chat.models.IConversation;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.common.chat.support.factories.ChatMessageFactoryProvider;
import it.clipcall.professional.leads.models.NewProject;
import it.clipcall.professional.leads.models.NewProjectChatData;
import it.clipcall.professional.leads.models.ProfessionalProjectDetails;
import it.clipcall.professional.leads.services.ProfessionalProjectsManager;
import it.clipcall.professional.payment.models.AccountMetadata;
import it.clipcall.professional.payment.models.PaymentSettings;
import it.clipcall.professional.payment.services.ProfessionalPaymentManager;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.services.ProfessionalManager;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class ProfessionalNewProjectController {


    private final ProfessionalManager professionalManager;
    private final ProfessionalProjectsManager professionalProjectsManager;
    private final AuthenticationManager authenticationManager;
    private final ProfessionalPaymentManager professionalPaymentManager;
    private final ChatManager chatManager;

    @Inject
    public ProfessionalNewProjectController(ProfessionalManager professionalManager, ProfessionalProjectsManager professionalProjectsManager, AuthenticationManager authenticationManager, ProfessionalPaymentManager professionalPaymentManager, ChatManager chatManager){
        this.professionalManager = professionalManager;
        this.professionalProjectsManager = professionalProjectsManager;
        this.authenticationManager = authenticationManager;
        this.professionalPaymentManager = professionalPaymentManager;
        this.chatManager = chatManager;
    }

    public boolean takeProject(NewProject project) {
        if(project == null)
            return false;

        if(!authenticationManager.isProfessionalAuthenticated())
            return false;

        String supplierId = authenticationManager.getSupplierId();
        boolean projectTaken = professionalProjectsManager.takeProject(supplierId, project);
        return projectTaken;

    }

    public void loadPolicyConfirmationStatus() {
        String userId = authenticationManager.getSupplierId();
        professionalProjectsManager.loadPolicyConfirmationStatus(userId);
    }

    public boolean isPolicyConfirmed(NewProject project){
        if(project.isPrivateVideo())
            return true;
        return professionalProjectsManager.isPolicyConfirmed();
    }

    public boolean confirmPolicy(){
        String userId = authenticationManager.getSupplierId();
        boolean confirmed = professionalProjectsManager.confirmPolicy(userId);
        return confirmed;
    }

    public void sendEstimationPriceText(NewProject project, String priceEstimationText) {
        sendChatMessageCore(project,priceEstimationText,MessageType.Text);
    }

    public void sendProjectAcceptedMessage(NewProject project){
        String userId = authenticationManager.getSupplierId();
        ProfessionalProfile professionalProfile = professionalManager.getProfessionalProfile(userId);
        String professionalName = professionalProfile.getBusinessName();

        NewProjectChatData chatData = new NewProjectChatData();
        chatData.category = project.getCategoryName();
        chatData.professionalName = professionalName;
        chatData.leadId = project.getLeadId();
        sendChatMessageCore(project, chatData, MessageType.SystemProInterested);
    }

    private void sendChatMessageCore(NewProject project, Object messageData, MessageType messageType){
        String userId = authenticationManager.getSupplierId();
        ProfessionalProjectDetails projectDetails = professionalProjectsManager.getProjectDetails2(userId, project.getLeadAccountId());
        if(projectDetails == null){
            return;
        }

        project.setCustomerName(projectDetails.getCustomerName());
        project.setCustomerPhoneNum(projectDetails.getPhoneNumber());

        String customerId = projectDetails.getCustomerId();
        String customerNameOrPhoneNumber = projectDetails.getCustomerDisplayName();
        String customerProfileImageUrl = projectDetails.getCustomerProfileImage();
        ChatParticipant receiver = new ChatParticipant(customerId, customerProfileImageUrl, customerNameOrPhoneNumber, false);


        ProfessionalProfile professionalProfile = professionalManager.getProfessionalProfile(userId);
        String professionalName = professionalProfile.getBusinessName();
        String professionalProfileImageUrl = professionalProfile.getProfileImageUrl();
        String proCustomerId = authenticationManager.getCustomerId();
        ChatParticipant sender = new ChatParticipant(proCustomerId, professionalProfileImageUrl, professionalName, true);

        IConversation conversationWithParticipant = chatManager.getConversationWithParticipant(sender, receiver, project.getLeadAccountId());
        if(conversationWithParticipant == null){
            conversationWithParticipant = chatManager.createConversation(sender,receiver, project.getLeadAccountId());
        }

        if(conversationWithParticipant == null){

            return;
        }

        MessageDescription messageDescription = ChatMessageFactoryProvider.getInstance().createMessageDescription(messageType, messageData);
        chatManager.sendMessage(sender,receiver,project.getLeadAccountId(), messageDescription);
    }

    public boolean callCustomer(String leadAccountId) {
        String userId = authenticationManager.getSupplierId();
        boolean success = professionalProjectsManager.callCustomer(userId, leadAccountId);
        return success;
    }

    public double calculateReducedPrice(double price) {
        String userId = authenticationManager.getSupplierId();
        double fee = professionalProjectsManager.getProjectFee(userId);
        return (1 - fee) * price;
    }

    public double getFeePercentage() {
        String userId = authenticationManager.getSupplierId();
        return professionalProjectsManager.getProjectFee(userId) * 100;
    }

    public PaymentSettings getPaymentDetails() {
        String supplierId = authenticationManager.getSupplierId();
        PaymentSettings paymentSettings = professionalPaymentManager.getPaymentSettings(supplierId);
        return paymentSettings;
    }

    public PaymentSettings getPaymentSettings() {
        String supplierId = authenticationManager.getSupplierId();
        PaymentSettings paymentSettings = professionalPaymentManager.getPaymentSettings(supplierId);
        return paymentSettings;
    }

    public AccountMetadata getPaymentAccountMetadata() {
        String supplierId = authenticationManager.getSupplierId();
        AccountMetadata accountMetadata =  professionalPaymentManager.getPaymentAccountMetadata(supplierId);
        return accountMetadata;
    }
}
