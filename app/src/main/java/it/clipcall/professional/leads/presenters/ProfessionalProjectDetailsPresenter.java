package it.clipcall.professional.leads.presenters;

import javax.inject.Inject;

import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.professional.leads.cotrollers.ProfessionalProjectsController;
import it.clipcall.professional.leads.models.LeadEntity;
import it.clipcall.professional.leads.views.IProfessionalProjectDetailsView;

@PerActivity
public class ProfessionalProjectDetailsPresenter extends PresenterBase<IProfessionalProjectDetailsView>{


    private final ILogger logger = LoggerFactory.getLogger(ProfessionalProjectDetailsPresenter.class.getSimpleName());

    private final RoutingService routingService;
    private final RouteParams routeParams;
    private final ProfessionalProjectsController controller;

    private ProjectEntityBase project;

    @Inject
    public ProfessionalProjectDetailsPresenter(RoutingService routingService, RouteParams routeParams, ProfessionalProjectsController controller) {
        this.routingService = routingService;
        this.routeParams = routeParams;
        this.controller = controller;

        this.project = routeParams.getParam("project");

        logger.debug("project details: %s", project);

    }

    @Override
    public void initialize() {
        getProjectDetails();
    }

    public void getProjectDetails(){
        ProjectEntityBase projectDetails = controller.getProjectDetails(project.getId());
        if(projectDetails != null){
            LeadEntity leadEntity = (LeadEntity) projectDetails;
            view.showProjectDetails(leadEntity);
        }
    }

    public ProjectEntityBase getProject() {
        return project;
    }
}
