package it.clipcall.professional.leads.services;


import java.util.List;

import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.professional.leads.models.LeadEntity;
import it.clipcall.professional.leads.models.PolicyConfirmationStatus;
import it.clipcall.professional.leads.models.ProfessionalProjectDetails;
import it.clipcall.professional.leads.models.ProfessionalSettings;
import it.clipcall.professional.leads.models.ProjectBid;
import it.clipcall.professional.leads.models.Quote;
import it.clipcall.professional.leads.models.QuoteDetails;
import it.clipcall.professional.leads.models.ShareVideoRequest;
import it.clipcall.professional.leads.models.VideoShared;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface IProfessionalProjectsService {

    @GET("v2/suppliers/{supplierId}/leads?pageNumber=0&pageSize=100&supplierLeadStatus=Any")
    Call<List<LeadEntity>> getProjects(@Path("supplierId") String supplierIds);


    @GET("v2/suppliers/{supplierId}/leads/{leadAccountId}")
    Call<ProfessionalProjectDetails> getProjectDetails(@Path("supplierId") String supplierId, @Path("leadAccountId") String leadAccountId);

    @POST("suppliers/{supplierId}/connectandrecord/{leadAccountId}")
    Call<StatusResponse> callCustomer(@Path("supplierId") String supplierId, @Path("leadAccountId") String leadAccountId);

    @POST("suppliers/{supplierId}/chat/{leadAccountId}/chatcreated")
    Call<StatusResponse> notifyChatCreated(@Path("supplierId") String supplierId, @Path("leadAccountId") String leadAccountId);

    @POST("v2/suppliers/{supplierId}/bids/{leadAccountId}")
    Call<StatusResponse> bidProject(@Path("supplierId") String supplierId, @Path("leadAccountId") String leadAccountId, @Body ProjectBid bid);

    @GET("v4/suppliers/{supplierId}/leads/{leadAccountId}/history")
    Call<List<HistoryItem>>  getHistoryItems(@Path("supplierId")String supplierId, @Path("leadAccountId") String leadAccountId);

    @POST("v2/suppliers/{supplierId}/leads/share")
    Call<VideoShared> shareVideo(@Path("supplierId") String supplierId,@Body ShareVideoRequest videoShare);

    @GET("suppliers/{supplierId}/confirmpolicy")
    Call<PolicyConfirmationStatus> getPolicyConfirmationStatus(@Path("supplierId") String supplierId);


    @GET("suppliers/{supplierId}/settings/fee")
    Call<ProfessionalSettings> getProSettings(@Path("supplierId") String supplierId);

    @POST("suppliers/{supplierId}/confirmpolicy")
    Call<StatusResponse> confirmPolicy(@Path("supplierId") String supplierId);

    @POST("suppliers/{supplierId}/leads/{leadAccountId}/quotes")
    Call<QuoteDetails> createQuote(@Path("supplierId")String supplierId, @Path("leadAccountId") String leadAccountId, @Body Quote quote);
}
