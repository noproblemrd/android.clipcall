package it.clipcall.professional.leads.models;

/**
 * Created by dorona on 03/07/2016.
 */
public enum  eQuoteStatusReason {
    NewQuote("NewQuote"),
    OverriddenByNewQuote("OverriddenByNewQuote"),
    Booked("Booked"),
    Lost("Lost");

    private final String statusReason;

    eQuoteStatusReason(String statusReason) {

        this.statusReason = statusReason;
    }

}
