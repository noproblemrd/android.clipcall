package it.clipcall.professional.leads.views;

import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.leads.models.LeadEntity;

/**
 * Created by dorona on 05/01/2016.
 */
public interface IProfessionalProjectDetailsView extends IView {

    void showProjectDetails(LeadEntity project);

    void notifyCallingCustomer();
}
