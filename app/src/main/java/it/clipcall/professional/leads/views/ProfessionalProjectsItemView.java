package it.clipcall.professional.leads.views;


import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import it.clipcall.R;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.common.projects.models.ProjectStatus;
import it.clipcall.common.projects.models.eProjectStatusColor;
import it.clipcall.infrastructure.support.dates.DateFormatter;
import it.clipcall.infrastructure.support.recyclerviews.ActionInvokerListener;
import it.clipcall.infrastructure.support.ui.RippleForegroundListener;
import it.clipcall.infrastructure.viewModel.support.recycleviews.IActionBindable;
import it.clipcall.infrastructure.viewModel.support.recycleviews.IBindable;
import it.clipcall.infrastructure.viewModel.support.recycleviews.ItemSelectedListener;

@EViewGroup(R.layout.professional_projects_item)
public class ProfessionalProjectsItemView extends LinearLayout implements IBindable<ProjectEntityBase>,IActionBindable<ProjectEntityBase> {


    @ViewById
    ViewGroup projectIconContainer;

    @ViewById
    ViewGroup projectDataContainer;

    @ViewById
    TextView categoryTextView;

    @ViewById
    TextView zipCodeTextView;

    @ViewById
    TextView createDateTextView;

    @ViewById
    TextView statusTextView;


    @ViewById
    ImageView previewVideoImageView;

    @ViewById
    ImageView profileLogoImageView;



    @ViewById
    ViewGroup unreadMessagesContainer;

    @ViewById
    TextView totalUnreadMessagesTextView;

    @ViewById
    CardView projectCardView;

    @ViewById
    ViewGroup projectContainer;




    public ProfessionalProjectsItemView(Context context) {
        this(context,null);
    }

    public ProfessionalProjectsItemView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ProfessionalProjectsItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void bind(final ProjectEntityBase entity, int position, final ItemSelectedListener itemSelectedListener) {
        createDateTextView.setText(DateFormatter.toLocalDate(entity.getCreatedOn()));
        categoryTextView.setText(entity.getCategoryName());
        zipCodeTextView.setText(entity.getAddressOrZipCode());
        String videoPreviewUrl = entity.getPreviewVideoImageUrl();
        if(!Strings.isNullOrEmpty(videoPreviewUrl)){
            Picasso.with(getContext()).load(videoPreviewUrl)
                    .into(previewVideoImageView);
        }
        else{
            Picasso.with(getContext()).load(R.drawable.unknown_video)
                    .into(previewVideoImageView);
        }

        if(!Strings.isNullOrEmpty(entity.getProfileUrl())){
            Picasso.with(getContext()).load(entity.getProfileUrl())
                    .into(profileLogoImageView);
        }else{
            Picasso.with(getContext()).load(R.drawable.profile_missing_rectangle2)
                    .into(profileLogoImageView);
        }
        ProjectStatus status = entity.getStatus();
        statusTextView.setText(status.getStatus());

        eProjectStatusColor color = status.getColor();
        if(color != null){
            statusTextView.setTextColor(ContextCompat.getColor(getContext(), color.getValue()));
        }else{
            String status1 = status.getStatus();
        }





        final String transitionName = String.format("imageTransition%s", entity.getId());
        projectDataContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectProject(transitionName, itemSelectedListener, entity);
            }
        });



        projectDataContainer.setOnTouchListener(new RippleForegroundListener(projectContainer));

        int backgroundColor = ContextCompat.getColor(getContext(), android.R.color.white);


        if(entity.isNotHired()){
            projectDataContainer.setOnClickListener(null);
            projectDataContainer.setOnTouchListener(null);
            backgroundColor = ContextCompat.getColor(getContext(), R.color.grey);

        }
        projectDataContainer.setBackgroundColor(backgroundColor);
        projectIconContainer.setBackgroundColor(backgroundColor);



        if(entity.getTotalUnreadMessages() > 0){
            unreadMessagesContainer.setVisibility(VISIBLE);
            totalUnreadMessagesTextView.setText(entity.getTotalUnreadMessages()+"");
        }else{
            unreadMessagesContainer.setVisibility(INVISIBLE);
            totalUnreadMessagesTextView.setText("");
        }
    }

    private void selectProject(String transitionName1, ItemSelectedListener itemSelectedListener, ProjectEntityBase project) {
        Bundle uiBundle = new Bundle();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Activity activity = (Activity) getContext();
            String transitionName = activity.getString(R.string.image_transition_name);
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(activity,profileLogoImageView, transitionName);
            uiBundle = options.toBundle();
        }

        itemSelectedListener.onItemSelected(project, uiBundle);
    }


    @Override
    public void bindAction(final ProjectEntityBase entity, int position, final ActionInvokerListener listener) {

        if(!entity.isNotHired()){
            projectIconContainer.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onInvokeAction(entity);
                }
            });
        }else{
            projectIconContainer.setOnClickListener(null);
        }

    }
}
