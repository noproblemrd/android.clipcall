package it.clipcall.professional.leads.models;

import org.parceler.Parcel;

/**
 * Created by dorona on 29/02/2016.
 */
@Parcel
public class ProjectCategory {
    Double maxPrice;
    Double mediumPrice;
    Double minPrice;
    String name;

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Double getMediumPrice() {
        return mediumPrice;
    }

    public void setMediumPrice(Double mediumPrice) {
        this.mediumPrice = mediumPrice;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
