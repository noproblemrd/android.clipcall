package it.clipcall.professional.leads.support;

import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.projects.models.ProjectEntity;

/**
 * Created by dorona on 02/12/2015.
 */
public class SupplierStatusConverter {


    public String convert(ProjectEntityBase input){

        if(input instanceof ProjectEntity){

            ProjectEntity project = (ProjectEntity) input;
            String status = project.getSupplierAvailabilityStatus();
            final String upperStatus = status.toUpperCase();
            if(project.getPrivate()){
                return upperStatus.equals("LOCATING_SUPPLIER") ? "Waiting for "+project.getSupplierName() : project.getSupplierName();
            }



            if(upperStatus.equals("AVAILABLE_SUPPLIERS"))
                return "CONNECTED";

            if(upperStatus.equals("NO_AVAILABLE_SUPPLIERS"))
                return "NO AVAILABLE PROS";


            if(upperStatus.equals("LOCATING_SUPPLIER"))
                return "LOCATING PROS...";

            return "Unknown status";

        }

        return "";


    }
}
