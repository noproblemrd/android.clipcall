package it.clipcall.professional.leads.presenters;

import android.os.Bundle;

import com.google.common.base.Strings;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.common.projects.models.ProjectVideoData;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.models.QuoteHistoryItem;
import it.clipcall.consumer.projects.models.VideoHistoryItemBase;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.professional.leads.cotrollers.ProfessionalProjectsController;
import it.clipcall.professional.leads.models.ProfessionalProjectDetails;
import it.clipcall.professional.leads.models.Quote;
import it.clipcall.professional.leads.views.IHistoryView;

@PerActivity
public class ProfessionalProjectDetailsHistoryPresenter extends PresenterBase<IHistoryView> {

    private final ProfessionalProjectsController controller;
    private final RoutingService routingService;
    private ProfessionalProjectDetails projectDetails;
    private final RouteParams routeParams;
    private ProjectEntityBase project;

    @Inject
    public ProfessionalProjectDetailsHistoryPresenter(ProfessionalProjectsController controller, RoutingService routingService, RouteParams routeParams) {
        this.controller = controller;
        this.routingService = routingService;
        this.routeParams = routeParams;
        this.project = routeParams.getParam("project");
    }


    @RunOnUiThread
    public void setProjectDetails(ProfessionalProjectDetails projectDetails) {
        this.projectDetails = projectDetails;
    }

    public void loadProjectHistoryItems() {
        List<HistoryItem> historyItems =  controller.getProjectHistoryItems(projectDetails);

        view.showHistoryItems(historyItems);
    }

    public void showVideoChat(VideoHistoryItemBase videoChatHistoryItem) {
        if(Strings.isNullOrEmpty(videoChatHistoryItem.getUrl()))
            return;

        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        ProjectVideoData projectVideoData = new ProjectVideoData();
        projectVideoData.zipCode = projectDetails.getRegion();
        projectVideoData.category = projectDetails.getCategoryName();
        projectVideoData.url = videoChatHistoryItem.getUrl();
        projectVideoData.createDate = projectDetails.getCreatedOn();

        navigationContext.bundle = new Bundle();
        navigationContext.bundle.putParcelable("context", Parcels.wrap(projectVideoData));
        routingService.routeTo(Routes.ProjectVideoRoute, navigationContext);
    }

    public void shareVideo(VideoHistoryItemBase videoChatHistoryItem) {
        view.showProcessingRequest(false);
        String message = controller.shareVideo(projectDetails.getLeadAccountId(),videoChatHistoryItem);
        if (Strings.isNullOrEmpty(message)) {
            //TODO show error

            view.showProcessingRequest(true);
            return;
        }

        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.addParameter("message", message);
        routingService.routeTo(Routes.ProfessionalShareVideoRoute, navigationContext);
        view.showProcessingRequest(true);
    }

    public void showQuote(QuoteHistoryItem quoteHistoryItem) {

        Quote quote = new Quote();
        quote.setDescription(quoteHistoryItem.getDescription());
        quote.setCreatedOn(quoteHistoryItem.getDate());
        quote.setStartJobAt(quoteHistoryItem.getStartJobAt());
        quote.setPrice(quoteHistoryItem.getPrice());
        quote.setNumber(quoteHistoryItem.getNumber());
        quote.setStatus(quoteHistoryItem.getStatus());
        quote.setStatusReason(quoteHistoryItem.getStatusReason());
        routeParams.setParam("quote",quote);

        routeParams.setParam("project",project);
        routeParams.setParam("quote",quote);
        routeParams.setParam("leadAccountId",projectDetails.getLeadAccountId());

        NavigationContext navContext = new NavigationContext(view.getRoutingContext());
        routingService.routeTo(Routes.ConsumerQuoteDetailsRoute,navContext);
    }
}
