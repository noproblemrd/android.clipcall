package it.clipcall.professional.leads.models.criterias;

import com.google.common.base.Strings;

import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.professional.leads.models.LeadEntity;

/**
 * Created by dorona on 22/05/2016.
 */

public class ProjectFilterCriteria implements ICriteria<ProjectEntityBase> {

    private final String filter;

    public ProjectFilterCriteria(String filter){

        this.filter = filter;
    }

    @Override
    public boolean isSatisfiedBy(ProjectEntityBase candidate) {
        if( !(candidate instanceof LeadEntity))
            return false;

        String loweredFilter = filter.toLowerCase();
        LeadEntity project = (LeadEntity) candidate;
        String loweredCategory = project.getCategoryName().toLowerCase();
        if(loweredCategory.contains(loweredFilter))
            return true;

        String loweredAddressOrZipCode = project.getAddressOrZipCode().toLowerCase();
        if(loweredAddressOrZipCode.contains(loweredFilter))
            return true;

        String loweredCreateDate = project.getCreatedOn().toString().toLowerCase();
        if(loweredCreateDate.contains(loweredFilter))
            return true;

        String wonTitle = project.getWonTitle();
        if(!Strings.isNullOrEmpty(wonTitle) && wonTitle.toLowerCase().contains(loweredFilter))
            return true;


        String status = project.getStatus().getStatus();
        if(!Strings.isNullOrEmpty(status) && status.toLowerCase().contains(loweredFilter))
            return true;

        return false;
    }
}
