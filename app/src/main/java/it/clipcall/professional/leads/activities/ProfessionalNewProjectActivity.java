package it.clipcall.professional.leads.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.commit451.nativestackblur.NativeStackBlur;
import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.joda.time.LocalDate;
import org.parceler.Parcels;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.payment.domainModel.services.QuoteDateFormatter;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.commands.ICommand;
import it.clipcall.infrastructure.resources.ResourceManager;
import it.clipcall.infrastructure.ui.dialogs.SnackbarContext;
import it.clipcall.infrastructure.ui.dialogs.SnackbarService;
import it.clipcall.infrastructure.ui.fragments.DatePickerFragment;
import it.clipcall.professional.leads.models.NewProject;
import it.clipcall.professional.leads.models.Quote;
import it.clipcall.professional.leads.presenters.ProfessionalNewProjectPresenter;
import it.clipcall.professional.leads.views.INewProjectView;

@EActivity(R.layout.professional_new_project_activity)
public class ProfessionalNewProjectActivity extends BaseActivity implements IHasToolbar, INewProjectView, MediaPlayer.OnPreparedListener, DialogInterface.OnCancelListener, DatePickerDialog.OnDateSetListener{

    private ProgressDialog progressDialog;
    private MediaController mediaControls;
    private int position = 0;

    @Extra
    Parcelable context;

    @ViewById
    android.support.design.widget.FloatingActionButton sendTextFab;

    @Inject
    ProfessionalNewProjectPresenter presenter;

    @Inject
    ResourceManager resourceManager;

    @Inject
    QuoteDateFormatter quoteDateFormatter;

    @Inject
    SnackbarService snackbarService;

    @ViewById
    Toolbar toolbar;

    @ViewById
    ImageView projectImage;

    @ViewById
    ImageView projectBlurryImage;

    @ViewById
    ImageView playVideoImageView;

    @ViewById
    TextView incomingCallTextView;

    @ViewById
    TextView reducedPriceTextView;

    @ViewById
    Button  takeProjectButton;

    @ViewById
    Button  callActionButton;

    @ViewById
    Button  textActionButton;

    @ViewById
    TextView priceEstimationTextView;

    @ViewById
    EditText quotePriceEditText;

    @ViewById
    ProgressBar takeProjectProgressBar;

    @ViewById
    VideoView projectVideoView;

    NewProject project;

    @ViewById
    EditText priceEstimationEditText;

    @ViewById
    ViewGroup textInputContainer;

    @ViewById
    ViewGroup rootCoordinatorLayout;

    @ViewById
    Button responseLaterButton;

    @ViewById
    Button quoteButton;

    @ViewById
    TextView quoteTextView;

    @ViewById
    ViewGroup quoteContainer;

    @ViewById
    EditText pickDateEditText;

    @ViewById
    EditText quoteEditText;

    @ViewById
    ProgressBar sendQuoteProgressBar;

    @ViewById
     Button sendQuoteButton;


    @Override
    public String getViewTitle() {
        return "New Project";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
      /*  if(projectVideoView.getVisibility() == View.VISIBLE){
            projectVideoView.stopPlayback();
            videoEnded();
            return true;
        }

        presenter.routeToHome();*/
        presenter.onBackTapped();
        return true;
  //      return false;
    }

    @Override
    public void onBackPressed() {
        onBackTapped();
    }

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        project = Parcels.unwrap(context);
        presenter.setProject(project);
        presenter.initialize();
    }

    @Click(R.id.sendTextFab)
    void onSubmitTextTapped(){
        presenter.sendTextMessage();
    }

    @UiThread
    @Override
    public void showProject(NewProject project) {
        String text;
        if(project.isPrivateVideo()){
            text = "Direct ClipCall\nfrom invited customer";
            takeProjectButton.setText("CONNECT NOW");
        }else{
            String formattedText = "Incoming ClipCall\nfor a %s\nin the %s area";
            text = String.format(formattedText,project.getCategoryName(),project.getZipCode());
        }

        if(!Strings.isNullOrEmpty(project.getVideoPicUrl()))
        {
            Picasso.with(this).load(project.getVideoPicUrl()).into(target);
        }else{
            Picasso.with(this).load(R.drawable.unknown_video_large).into(target);
        }


        incomingCallTextView.setText(text);
        if(!Strings.isNullOrEmpty(project.getVideoUrl()))
            projectVideoView.setVideoPath(project.getVideoUrl());

        projectVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videoEnded();
            }
        });

    }

    @Click(R.id.callActionButton)
    void onCallTapped(){
        presenter.callCustomer();
    }

    private void videoEnded(){
        projectVideoView.setVisibility(View.GONE);
        playVideoImageView.setVisibility(View.VISIBLE);
        toolbar.setTitle(getViewTitle());
        toolbar.setSubtitle("");
        presenter.setVideoIsPlaying(false);
    }



    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            BitmapDrawable drawable = new BitmapDrawable(getResources(),bitmap);
            Bitmap source = bitmap;
            Bitmap destination = NativeStackBlur.process(source, 50);
            Drawable destinationDrawable = new BitmapDrawable(getResources(),destination);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                projectBlurryImage.setImageDrawable(destinationDrawable);
                projectImage.setImageDrawable(drawable);
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };


    @Override
    public void showTakeProjectInProgress(boolean inProgress) {
       if(inProgress){
           takeProjectProgressBar.setVisibility(View.VISIBLE);
           playVideoImageView.setVisibility(View.INVISIBLE);
           takeProjectButton.setVisibility(View.INVISIBLE);
           toolbar.setTitle("");
           toolbar.setSubtitle("");
       }else{
           takeProjectProgressBar.setVisibility(View.GONE);
           takeProjectButton.setVisibility(View.VISIBLE);
       }
    }


    @UiThread
    @Override
    public void showActionOptions() {
        hideContent();
    }

    @Override
    public void hideContent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(rootCoordinatorLayout);
        }

        projectImage.setVisibility(View.GONE);
        incomingCallTextView.setVisibility(View.GONE);
        playVideoImageView.setVisibility(View.GONE);
        takeProjectButton.setVisibility(View.GONE);
        priceEstimationEditText.clearFocus();
        textInputContainer.setVisibility(View.GONE);
        quoteContainer.setVisibility(View.GONE);


        //responseLaterButton.setVisibility(View.VISIBLE);
        callActionButton.setVisibility(View.VISIBLE);
        textActionButton.setVisibility(View.VISIBLE);
        priceEstimationTextView.setVisibility(View.VISIBLE);
        quoteTextView.setVisibility(View.VISIBLE);
        quoteButton.setVisibility(View.VISIBLE);

    }

    @Click(R.id.responseLaterButton)
    void onResponseLaterTapped(){
        //presenter.cancelTextTapped();
    }


    @Click(R.id.cancelTextButton)
    void onCancelTextTapped(){
        presenter.cancelTextTapped();
    }


    @Click(R.id.pickDateEditText)
    void onPickDateTapped(){
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void showTextInput() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(rootCoordinatorLayout);
        }
        callActionButton.setVisibility(View.GONE);
        textActionButton.setVisibility(View.GONE);
        quoteButton.setVisibility(View.GONE);
        priceEstimationTextView.setVisibility(View.GONE);
        quoteTextView.setVisibility(View.GONE);
        textInputContainer.setVisibility(View.VISIBLE);
        priceEstimationEditText.requestFocus();
        //

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(priceEstimationEditText, InputMethodManager.SHOW_IMPLICIT);
        }
    }



    @Override
    public void showAcceptingPolicy() {

    }

    @Override
    public void showMessage(String message, final ICommand command) {
        snackbarService.show(rootCoordinatorLayout,message, Snackbar.LENGTH_INDEFINITE,new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                command.execute();
            }
        });
    }

    @UiThread
    public void showMessage(String message){
        Snackbar.make(rootCoordinatorLayout,message,Snackbar.LENGTH_LONG).setCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                presenter.routeToHome();
            }
        }).show();
    }


    @Override
    public void showEstimationTextRequeired() {
        priceEstimationEditText.setError("Price estimation text is required.");
    }

    @Override
    public void showSendingTextMessage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(rootCoordinatorLayout);
        }
        textInputContainer.setVisibility(View.GONE);

    }

    @UiThread
    @Override
    public void stopVideo() {
        videoEnded();
    }

    @Override
    public void showQuoteInput() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(rootCoordinatorLayout);
        }
        callActionButton.setVisibility(View.GONE);
        textActionButton.setVisibility(View.GONE);
        priceEstimationTextView.setVisibility(View.GONE);
        textInputContainer.setVisibility(View.INVISIBLE);
        quoteTextView.setVisibility(View.INVISIBLE);
        quoteButton.setVisibility(View.INVISIBLE);
        quoteContainer.setVisibility(View.VISIBLE);

        /*InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(priceEstimationEditText, InputMethodManager.SHOW_IMPLICIT);
        }*/
    }

    @Override
    public void showPriceRequired() {
        quotePriceEditText.setError("Quote price is required");
    }

    @Override
    public void showStartDateRequired() {
        pickDateEditText.setError("You must select a start date");
        Snackbar.make(rootCoordinatorLayout,"You must select a start date",Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showStartDateMustBeGreateThanOrEqualToToday() {
        String message = resourceManager.getString(R.string.past_date_validation);
        pickDateEditText.setError(message);
        snackbarService.show(rootCoordinatorLayout,message, Snackbar.LENGTH_LONG);
    }

    @Override
    public void showQuoteDescriptionIsRequired() {
        quoteEditText.setError("Quote description is required");
    }

    @Override
    public void showQuoteSubmissionFaliure() {
        String message = resourceManager.getString(R.string.quote_sending_failed);
        Snackbar.make(rootCoordinatorLayout,message,Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.routeToHome();
            }
        }).show();
    }

    @Override
    public void sendQuoteInProgress(boolean isInProgress) {
        if(isInProgress){
            sendQuoteProgressBar.setVisibility(View.VISIBLE);
            sendQuoteButton.setVisibility(View.INVISIBLE);
        }else{
            sendQuoteProgressBar.setVisibility(View.INVISIBLE);
            sendQuoteButton.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showStartDate(Quote quote) {
        pickDateEditText.setText(quoteDateFormatter.format(quote));
    }

    @Override
    public void showQuoteSent(Boolean success) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(rootCoordinatorLayout);
        }
        quoteContainer.setVisibility(View.GONE);
    }

    @Override
    public void showReducedPrice(double reducedPrice, double feePercentage) {
        if(reducedPrice > 0){
            NumberFormat formatter = NumberFormat.getCurrencyInstance();
            NumberFormat percentageFormat = new DecimalFormat("##");
            String formattedPercentage = percentageFormat.format(feePercentage);
            String reducedPriceText = formatter.format(reducedPrice);
            String reducedPriceTextFormat = resourceManager.getString(R.string.payment_after_clipcall_fee);
            reducedPriceTextView.setVisibility(View.VISIBLE);
            reducedPriceTextView.setText(String.format(reducedPriceTextFormat,formattedPercentage, reducedPriceText));
        }else{
            reducedPriceTextView.setVisibility(View.INVISIBLE);
        }


    }

    @Override
    public void showQuoteMinValue() {
        quotePriceEditText.setError("Price must be at least 50 cents");
    }

    @Override
    public void showMessageContainsNonPermittedText() {
        String message = getString(R.string.terms_of_service_violation);
        SnackbarContext context = new SnackbarContext(rootCoordinatorLayout,message,null, Snackbar.LENGTH_LONG,5);
        snackbarService.show(context);
    }


    @Click(R.id.textActionButton)
    void textConsumerTapped(){
        presenter.textConsumerTapped();
    }


    @Click(R.id.quoteButton)
    void quoteNowTapped(){
        presenter.quoteNowTapped();
    }

    @Click(R.id.sendQuoteButton)
    void sendQuoteTapped(){
        presenter.sendQuote();
    }

    @TextChange(R.id.priceEstimationEditText)
    void priceEstimatinTextChanged(CharSequence text, TextView hello, int before, int start, int count){
        if(text == null)
            return;

        presenter.setPriceEstimationText(text + "");
    }

    @TextChange(R.id.quoteEditText)
    void qouteTextChanged(CharSequence text, TextView hello, int before, int start, int count){
        if(text == null)
            return;

        presenter.setQuoteText(text + "");
    }


    @TextChange(R.id.quotePriceEditText)
    void quotePriceChanged(CharSequence text, TextView hello, int before, int start, int count){
        if(text == null)
            return;

        presenter.setQuotePrice(text + "");
    }


    //@Background
    @Click(R.id. takeProjectButton)
    void takeProjectTapped(){
        presenter.takeProject();
    }

    @Click(R.id.playVideoImageView)
    void playVideoTapped(){


        playVideoImageView.setVisibility(View.INVISIBLE);
        projectVideoView.setVisibility(View.VISIBLE);


        if(mediaControls == null)
            mediaControls = new MediaController(this);
        if(progressDialog == null)
            progressDialog = new ProgressDialog(this);


        String title = project.getCategoryName() + " in " + project.getZipCode();
        progressDialog.setTitle(title);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(true);
        progressDialog.setOnCancelListener(this);
        progressDialog.show();


        try {
            String url = project.getVideoUrl();
            projectVideoView.setMediaController(mediaControls);
            projectVideoView.setVideoURI(Uri.parse(url));
            projectVideoView.requestFocus();
            projectVideoView.setOnPreparedListener(this);
            projectVideoView.start();

        } catch (Exception e) {
            progressDialog.hide();
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        projectVideoView.start();
        presenter.setVideoIsPlaying(true);
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        //we use onSaveInstanceState in order to store the video playback position for orientation change
        savedInstanceState.putInt("Position", projectVideoView.getCurrentPosition());
        savedInstanceState.putParcelable("context",context);
        projectVideoView.pause();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //we use onRestoreInstanceState in order to play the video playback from the stored position
        position = savedInstanceState.getInt("Position");

        Parcelable context = savedInstanceState.getParcelable("context");
        project = Parcels.unwrap(context);
        projectVideoView.seekTo(position);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        videoEnded();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        progressDialog.dismiss();
        projectVideoView.seekTo(position);
        if (position == 0) {
            projectVideoView.start();
        } else {
            projectVideoView.pause();
        }

        String title = project.getCategoryName() + " in " + project.getZipCode();
        String subtitle = project.getCreateDate().toString();
        toolbar.setTitle(title);
        toolbar.setSubtitle(subtitle);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode, data);
        presenter.handleActivityResult(requestCode, resultCode,data);

    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        LocalDate startDate = new LocalDate(year,monthOfYear+1, dayOfMonth);
        presenter.setQuoteStartDateDate(startDate);
    }
}
