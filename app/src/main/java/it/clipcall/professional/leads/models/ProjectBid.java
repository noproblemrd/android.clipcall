package it.clipcall.professional.leads.models;

/**
 * Created by dorona on 16/03/2016.
 */
public class ProjectBid {

    public ProjectBid(String status, double price){

        this.status = status;
        this.price = price;
    }
    public double price;
    public String status;
}
