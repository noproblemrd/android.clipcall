package it.clipcall.professional.leads.models.requests;

import it.clipcall.infrastructure.proxies.requests.PagedRequest;

/**
 * Created by micro on 2/5/2016.
 */
public class ProjectsRequest extends PagedRequest {

    public String getSupplierLeadStatus() {
        return supplierLeadStatus;
    }

    public void setSupplierLeadStatus(String supplierLeadStatus) {
        this.supplierLeadStatus = supplierLeadStatus;
    }

    public String supplierLeadStatus;
}
