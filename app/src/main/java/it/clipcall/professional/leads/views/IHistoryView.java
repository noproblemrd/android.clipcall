package it.clipcall.professional.leads.views;

import java.util.List;

import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by micro on 1/20/2016.
 */
public interface IHistoryView extends IView {

    void showHistoryItems(List<HistoryItem> historyItems);

    void showProcessingRequest(boolean isCompleted);
}
