package it.clipcall.professional.leads.proxies.responses;

import java.util.List;

import it.clipcall.professional.leads.models.LeadEntity;

/**
 * Created by dorona on 28/02/2016.
 */
public class GetProjectsResponse {
    private List<LeadEntity> leads;

    public List<LeadEntity> getLeads() {
        return leads;
    }

    public void setLeads(List<LeadEntity> leads) {
        this.leads = leads;
    }
}
