package it.clipcall.professional.leads.models.criterias;

import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.professional.leads.models.LeadEntity;

/**
 * Created by dorona on 07/07/2016.
 */
public class ProfessionalCanSendQuoteCriteria implements ICriteria<LeadEntity> {
    @Override
    public boolean isSatisfiedBy(LeadEntity candidate) {

        if(candidate.isMissed() )
            return false;

        if(candidate.isBooked())
            return false;


        return true;
    }
}
