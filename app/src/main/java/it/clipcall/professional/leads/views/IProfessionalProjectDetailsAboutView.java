package it.clipcall.professional.leads.views;

import it.clipcall.common.projects.models.ProjectDetails;
import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.leads.models.LeadEntity;
import it.clipcall.professional.leads.models.ProfessionalProjectDetails;

/**
 * Created by micro on 1/20/2016.
 */
public interface IProfessionalProjectDetailsAboutView extends IView {

    void setProjectDetails(ProjectDetails projectDetails);

    void startRemoteCallFailed();

    void notifyCallingCustomer();

    void enableActions(boolean enable);

    void enableChatAction(boolean enable);

    void showErrorCreatingVideoChatSession();

    void showTotalUnreadMessages(int totalUnreadMessages);

    void showProjectDetails(LeadEntity project, ProfessionalProjectDetails projectDetails);


}
