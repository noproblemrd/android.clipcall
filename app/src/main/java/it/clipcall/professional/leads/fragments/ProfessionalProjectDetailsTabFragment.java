package it.clipcall.professional.leads.fragments;

import it.clipcall.professional.leads.views.IProfessionalProjectDetailsView;
import it.clipcall.common.projects.models.ProjectDetails;
import it.clipcall.infrastructure.fragments.BaseFragment;


public abstract class ProfessionalProjectDetailsTabFragment extends BaseFragment {

   IProfessionalProjectDetailsView parentView;

   public void setParentView(IProfessionalProjectDetailsView parentView) {
      this.parentView = parentView;
   }

   public abstract void setProjectDetails(ProjectDetails projectDetails);
}
