package it.clipcall.professional.leads.models;

/**
 * Created by dorona on 27/06/2016.
 */
public class NewQuoteData {

    public Quote quote;
    public String leadId;
    public String leadAccountId;
}

