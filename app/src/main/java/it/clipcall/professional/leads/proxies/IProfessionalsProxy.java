package it.clipcall.professional.leads.proxies;

import java.util.List;

import it.clipcall.common.models.entities.BusinessDescription;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.infrastructure.requests.LoginRequest;
import it.clipcall.infrastructure.responses.AdvertiserData;
import it.clipcall.infrastructure.responses.BusinessDetailsResponse;
import it.clipcall.infrastructure.responses.LoginResponse;
import it.clipcall.professional.profile.models.BusinessAddress;
import it.clipcall.professional.profile.models.BusinessProfileLogo;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.profile.models.CoverAreaCollection;
import it.clipcall.professional.profile.models.FullBusinessAddress;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by dorona on 09/11/2015.
 */
public interface IProfessionalsProxy {


    @POST("logon/customerlogon")
    Call<LoginResponse> login(@Body LoginRequest request);

    @POST("customers/{customerId}/switchtoadvertiser")
    Call<AdvertiserData> switchToAdvertiser(@Path("customerId") String customerId);


    @POST("suppliers/{supplierId}/coverareas/bussinessaddress")
    Call<CoverAreaCollection> updateBusinessAddress(@Path("supplierId") String supplierId, @Body BusinessAddress address);


    @POST("suppliers/{supplierId}/categories")
    Call<StatusResponse> updateCategories(@Path("supplierId") String supplierId, @Body List<Category> categories);

    @POST("suppliers/{supplierId}/businessdetails")
    Call<StatusResponse> updateBusinessDetails(@Path("supplierId") String supplierId, @Body FullBusinessAddress businessAddress);

    @POST("suppliers/{supplierId}/BusinessDescription")
    Call<StatusResponse> updateBusinessDescription(@Path("supplierId") String supplierId, @Body BusinessDescription businessDescription);

    @GET("suppliers/{supplierId}/businessdetails")
    Call<BusinessDetailsResponse> getBusinessDetails(@Path("supplierId") String supplierId);

    @GET("suppliers/{supplierId}/BusinessDescription")
    Call<BusinessDescription> getBusinessDescription(@Path("supplierId") String supplierId);

    @GET("suppliers/{supplierId}/coverareas")
    Call<CoverAreaCollection> getBusinessProfileCoverAreas(@Path("supplierId") String supplierId);

    @GET("suppliers/{supplierId}/logo")
    Call<BusinessProfileLogo> getBusinessProfileLogo(@Path("supplierId") String supplierId);

    @GET("suppliers/{supplierId}/categories")
    List<Category> getBusinessProfileCategories(@Path("supplierId") String supplierId);

}
