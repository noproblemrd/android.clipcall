package it.clipcall.professional.leads.fragments;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.kennyc.view.MultiStateView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.projects.models.ProjectDetails;
import it.clipcall.consumer.projects.models.CallHistoryItem;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.models.PaymentHistoryItem;
import it.clipcall.consumer.projects.models.QuoteHistoryItem;
import it.clipcall.consumer.projects.models.VideoHistoryItemBase;
import it.clipcall.consumer.projects.support.HistoryItemAdapter;
import it.clipcall.consumer.projects.support.IHistoryItemListener;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.professional.leads.models.ProfessionalProjectDetails;
import it.clipcall.professional.leads.presenters.ProfessionalProjectDetailsHistoryPresenter;
import it.clipcall.professional.leads.views.IHistoryView;

@EFragment(R.layout.project_advertiser_history_fragment)
public class ProfessionalProjectDetailsHistoryFragment extends ProfessionalProjectDetailsTabFragment implements IHistoryView, IHistoryItemListener {

    @FragmentArg
    String leadAccountId;

    @ViewById
    SwipeRefreshLayout swipeRefreshLayout;

    SwipeRefreshLayout emptyRefreshLayout;

    @ViewById
    RecyclerView callHistoryRecycleView;

    @Inject
    ProfessionalProjectDetailsHistoryPresenter presenter;

    HistoryItemAdapter historyItemAdapter;

    @ViewById
    com.kennyc.view.MultiStateView multiStateView;

    @AfterViews
    public void afterViews(){
        presenter.bindView(this);
        View emptyViewContainer = multiStateView.getView(MultiStateView.VIEW_STATE_EMPTY);
        emptyRefreshLayout = (SwipeRefreshLayout) emptyViewContainer.findViewById(R.id.emptyProjectsRefreshLayout);
        emptyRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadHistoryItems();
            }
        });

        historyItemAdapter = new HistoryItemAdapter(getActivity(),this, true);
        callHistoryRecycleView.setAdapter(historyItemAdapter);
        callHistoryRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadHistoryItems();
            }
        });


    }

    void loadHistoryItems(){
        presenter.loadProjectHistoryItems();
    }


    @Override
    public void setProjectDetails(ProjectDetails projectDetails) {
        ProfessionalProjectDetails proProjectDetails = (ProfessionalProjectDetails) projectDetails;
        presenter.setProjectDetails(proProjectDetails);
        loadHistoryItems();
    }

    public void showHistoryItems(List<HistoryItem> historyItems) {

        if(Lists.isNullOrEmpty(historyItems))
            multiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
        else
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);


        swipeRefreshLayout.setRefreshing(false);
        emptyRefreshLayout.setRefreshing(false);

        historyItemAdapter.setItems(historyItems);
    }


    @Override
    public void showProcessingRequest(boolean isCompleted) {
        if(isCompleted){
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        }else{
            multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        }



    }

    @Override
    public RoutingContext getRoutingContext() {
        return new RoutingContext(getActivity());
    }


    @Override
    public void onItemTapped(CallHistoryItem item) {

    }

    @Background
    @Override
    public void onShareVideoTapped(VideoHistoryItemBase videoChatHistoryItem) {
        presenter.shareVideo(videoChatHistoryItem);
    }

    @Override
    public void onItemTapped(VideoHistoryItemBase videoChatHistoryItem) {
        presenter.showVideoChat(videoChatHistoryItem);
    }

    @Override
    public void onItemTapped(PaymentHistoryItem videoChatHistoryItem) {

    }

    @Override
    public void onItemTapped(QuoteHistoryItem quoteHistoryItem) {
        presenter.showQuote(quoteHistoryItem);
    }
}
