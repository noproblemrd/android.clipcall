package it.clipcall.professional.leads.views;

import it.clipcall.infrastructure.commands.ICommand;
import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.leads.models.NewProject;
import it.clipcall.professional.leads.models.Quote;

/**
 * Created by dorona on 16/03/2016.
 */
public interface INewProjectView extends IView {
    void showProject(NewProject project);

    void showTakeProjectInProgress(boolean inProgress);

    void showActionOptions();

    void hideContent();

    void showTextInput();

    void showAcceptingPolicy();

    void showMessage(String message, ICommand command);

    void showMessage(String message);

    void showEstimationTextRequeired();

    void showSendingTextMessage();

    void stopVideo();

    void showQuoteInput();

    void showPriceRequired();

    void showStartDateRequired();

    void showStartDateMustBeGreateThanOrEqualToToday();

    void showQuoteDescriptionIsRequired();

    void showQuoteSubmissionFaliure();

    void sendQuoteInProgress(boolean isInProgress);

    void showStartDate(Quote quote);

    void showQuoteSent(Boolean success);

    void showReducedPrice(double reducedPrice, double feePercentage);

    void showQuoteMinValue();

    void showMessageContainsNonPermittedText();
}
