package it.clipcall.professional.leads.presenters;

import android.os.Bundle;
import android.os.Parcelable;

import com.google.common.base.Strings;
import com.layer.sdk.changes.LayerChangeEvent;

import org.parceler.Parcels;
import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.common.projects.models.ProjectVideoData;
import it.clipcall.consumer.projects.views.IProjectsView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.collections.ICounter;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.professional.leads.cotrollers.ProfessionalProjectsController;
import it.clipcall.professional.leads.models.LeadEntity;
import it.clipcall.professional.leads.models.NewProject;

@PerActivity
public class ProfessionalProjectsPresenter extends PresenterBase<IProjectsView>{

    private final RoutingService routingService;
    private final RouteParams routeParams;
    private final ProfessionalProjectsController controller;
    private final Logger logger = LoggerFactory.getLogger(ProfessionalProjectsPresenter.class.getSimpleName());

    @Inject
    public ProfessionalProjectsPresenter(RoutingService routingService, RouteParams routeParams, ProfessionalProjectsController controller) {
        this.routingService = routingService;
        this.routeParams = routeParams;
        this.controller = controller;
    }

    @Override
    public void initialize() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void shutDown() {
        EventBus.getDefault().unregister(this);
    }

    public void getProfessionalProjects(boolean forceReload){
        logger.debug("getProfessionalProjects forceReload is {}", forceReload);
        List<ProjectEntityBase> projects = controller.getProfessionalProjects(forceReload);
        view.setProjects(projects);
        int totalUnreadMessages = Lists.sum(projects, new ICounter<ProjectEntityBase>() {
            @Override
            public int count(ProjectEntityBase candidate) {
                return candidate.getTotalUnreadMessages();
            }
        });
        view.showTotalUnreadMessages(totalUnreadMessages);

    }

    public void projectSelected(LeadEntity project, Bundle bundle) {
        if(project.getStatus().getStatus().toUpperCase().equals("NEW")){
            routeToNewProject(project);
            return;
        }

        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        routeParams.reset();
        routeParams.setParam("project", project);
        navigationContext.uiBundle = bundle;
        routingService.routeTo(Routes.ProfessionalProjectDetailsRoute, navigationContext);
    }

    private void routeToNewProject(LeadEntity project) {
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        NewProject newProject = new NewProject();
        newProject.setLeadId(project.getLeadId());
        newProject.setAddress("");
        newProject.setCategoryName(project.getCategoryName());
        newProject.setCreateDate(project.getCreatedOn());
        newProject.setCustomerName("");
        newProject.setCustomerPhoneNum("");
        newProject.setPrivateVideo(project.getPrivate());
        newProject.setLeadAccountId(project.getLeadAccountId());
        newProject.setVideoDuration(project.getVideoDuration());
        newProject.setVideoPicUrl(project.getPreviewVideoImageUrl());
        newProject.setVideoUrl(project.getVideoUrl());
        newProject.setZipCode(project.getAddressOrZipCode());
        Parcelable context = Parcels.wrap(newProject);
        navigationContext.bundle = new Bundle();
        navigationContext.bundle.putParcelable("context",context);
        routingService.routeTo(Routes.ProfessionalNewProjectRoute,navigationContext);
    }

    public void executeProjectAction(LeadEntity project) {
        view.onProjectActionExecuted(project);
    }

    public void navigateToRemoteHouseCall() {
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getFragmentRoutingContext();
        routingService.routeTo(Routes.ProfessionalRemoteServiceCallRoute, navigationContext);
    }

    public void navigateToInviteCustomer() {
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = view.getFragmentRoutingContext();
        routingService.routeTo(Routes.ProfessionalInviteCustomerRoute, navigationContext);
    }


    @Subscriber(mode = ThreadMode.ASYNC)
    void onChatEvent(LayerChangeEvent layerChangeEvent) {
        boolean forceReload = false;
        List<ProjectEntityBase> projects = controller.getProfessionalProjects(forceReload);
        int totalUnreadMessages = Lists.sum(projects, new ICounter<ProjectEntityBase>() {
            @Override
            public int count(ProjectEntityBase candidate) {
                return candidate.getTotalUnreadMessages();
            }
        });
        view.showTotalUnreadMessages(totalUnreadMessages);
        view.setProjectsAfterInvalidating(projects);
    }

    public void showVideo(LeadEntity project) {

        if(project.getStatus().getStatus().toUpperCase().equals("NEW")){
            routeToNewProject(project);
            return;
        }

        if(Strings.isNullOrEmpty(project.getVideoUrl()))
            return;

        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        ProjectVideoData projectVideoData = new ProjectVideoData();
        projectVideoData.zipCode = project.getAddressOrZipCode();
        projectVideoData.category = project.getCategoryName();
        projectVideoData.url = project.getVideoUrl();
        projectVideoData.createDate = project.getCreatedOn();

        navigationContext.bundle = new Bundle();
        navigationContext.bundle.putParcelable("context", Parcels.wrap(projectVideoData));
        navigationContext.addParameter("projectVideoUrl",project.getVideoUrl());
        routingService.routeTo(Routes.ProjectVideoRoute, navigationContext);
    }

    public void filterProfessionalProjects(String filter) {
        List<ProjectEntityBase> projects = controller.getProjectsByFilter(filter);
        view.setFilteredProjects(projects);
    }

    public void playVideo(ProjectEntityBase project) {
        if(Strings.isNullOrEmpty(project.getVideoUrl()))
            return;

        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        ProjectVideoData projectVideoData = new ProjectVideoData();
        projectVideoData.zipCode = project.getAddressOrZipCode();
        projectVideoData.category = project.getCategoryName();
        projectVideoData.url = project.getVideoUrl();
        projectVideoData.createDate = project.getCreatedOn();

        navigationContext.bundle = new Bundle();
        navigationContext.bundle.putParcelable("context", Parcels.wrap(projectVideoData));
        navigationContext.addParameter("projectVideoUrl",project.getVideoUrl());
        routingService.routeTo(Routes.ProjectVideoRoute, navigationContext);
    }
}