package it.clipcall.professional.leads.presenters;

import android.os.Bundle;

import com.layer.sdk.changes.LayerChangeEvent;

import org.parceler.Parcels;
import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import javax.inject.Inject;

import it.clipcall.common.chat.services.tasks.NavigateToChat2Task;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.vidoechat.models.VideoChatContext;
import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.professional.leads.cotrollers.ProfessionalProjectsController;
import it.clipcall.professional.leads.models.LeadEntity;
import it.clipcall.professional.leads.models.ProfessionalProjectDetails;
import it.clipcall.professional.leads.views.IProfessionalProjectDetailsAboutView;

@PerActivity
public class ProfessionalProjectDetailsAboutPresenter extends PresenterBase<IProfessionalProjectDetailsAboutView>{

    private final ProfessionalProjectsController controller;
    private final RoutingService routingService;
    private ProfessionalProjectDetails projectDetails;
    private LeadEntity project;
    private final NavigateToChat2Task navigateToChatTask;
    private final RouteParams routeParams;


    @Inject
    public ProfessionalProjectDetailsAboutPresenter(ProfessionalProjectsController controller, RoutingService routingService, NavigateToChat2Task navigateToChatTask, RouteParams routeParams) {
        this.controller = controller;
        this.routingService = routingService;
        this.navigateToChatTask = navigateToChatTask;
        this.routeParams = routeParams;
    }

    @Override
    public void initialize() {
        //view.enableChatAction(false);
        //controller.connectToChat(this);
        EventBus.getDefault().register(this);
        this.project = routeParams.getParam("project");
    }

    @Override
    public void shutDown() {
        EventBus.getDefault().unregister(this);
    }

    public void callCustomer() {
        view.enableActions(false);
        boolean success = this.controller.callCustomer(projectDetails.getLeadAccountId());
        if(success){
           view.notifyCallingCustomer();
        }
        view.enableActions(true);
    }

    private void connectToVideoSessionInBackground(VideoSessionData videoSessionData){
        controller.connectToVideoChatInBackground(videoSessionData);
    }

    private void initializePublisher(){
        String businessName = controller.getProfessionalProfile().getBusinessName();
        controller.initializePublisher(businessName);
    }

    public void startRemoteHouseCall() {
        view.enableActions(false);
        VideoSessionData videoSessionData = this.controller.createVideoChatSession(projectDetails.getLeadAccountId());
        if(videoSessionData == null){
            view.showErrorCreatingVideoChatSession();
            view.enableActions(true);
            return;
        }


        startRemoteHouseCallCore(videoSessionData);


    }

    @RunOnUiThread
    private void startRemoteHouseCallCore(VideoSessionData videoSessionData) {
        connectToVideoSessionInBackground(videoSessionData);
        initializePublisher();
        navigateToVideoChat(videoSessionData);
    }

    private void navigateToVideoChat(VideoSessionData videoSessionData){
        RoutingContext routingContext = view.getRoutingContext();
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.bundle = new Bundle();

        VideoChatContext videoChatContext = new VideoChatContext();
        videoChatContext.videoSessionData = videoSessionData;
        videoChatContext.profileImageUrl = projectDetails.getCustomerProfileImage();
        videoChatContext.profileName = projectDetails.getCustomerDisplayName();
        videoChatContext.isInitiatingCall = true;

        navigationContext.bundle.putParcelable("videoChatContext",  Parcels.wrap(videoChatContext));
        navigationContext.routingContext = routingContext;
        routingService.routeTo(Routes.ProfessionalVideoChatRoute, navigationContext);
        view.enableActions(true);
    }

    public void navigateToChat() {
        view.enableChatAction(false);
        navigateToChatTask.execute(project,projectDetails,view.getRoutingContext());
        view.enableChatAction(true);
    }

    public void setProjectDetails(ProfessionalProjectDetails projectDetails) {
        this.projectDetails = projectDetails;
        view.showProjectDetails(project, projectDetails);
        ProjectEntityBase project = controller.getProjectDetails(projectDetails.getLeadAccountId());
        view.showTotalUnreadMessages(project.getTotalUnreadMessages());
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onChatEvent(LayerChangeEvent layerChangeEvent) {
        ProjectEntityBase project = controller.getProjectDetails(projectDetails.getLeadAccountId());
        view.showTotalUnreadMessages(project.getTotalUnreadMessages());
    }

    public void loadUnreadMessages() {
        if(projectDetails == null)
            return;

        ProjectEntityBase professional = controller.getProjectDetails(projectDetails.getLeadAccountId());
        if(professional == null)
            return;

        view.showTotalUnreadMessages(professional.getTotalUnreadMessages());
    }

    public void sendQuote() {
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        ProjectEntityBase project = controller.getProjectDetails(projectDetails.getLeadAccountId());
        routeParams.setParam("project",project);
        routeParams.setParam("leadAccountId",projectDetails.getLeadAccountId());
        routingService.routeTo(Routes.ProfessionalCreateQuoteRoute, navigationContext);
    }
}