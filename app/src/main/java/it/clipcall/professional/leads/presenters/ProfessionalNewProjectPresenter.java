package it.clipcall.professional.leads.presenters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.google.common.base.Strings;

import org.joda.time.LocalDate;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import it.clipcall.common.chat.models.ChatMessageValidationCriteria;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ProfessionalEventNames;
import it.clipcall.common.models.UrlConstants;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.commands.ICommand;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.strings.Strings2;
import it.clipcall.infrastructure.ui.dialogs.DialogContext;
import it.clipcall.infrastructure.ui.dialogs.DialogService;
import it.clipcall.professional.leads.cotrollers.ProfessionalNewProjectController;
import it.clipcall.professional.leads.models.NewProject;
import it.clipcall.professional.leads.models.Quote;
import it.clipcall.professional.leads.views.INewProjectView;
import it.clipcall.professional.payment.models.AccountMetadata;
import it.clipcall.professional.payment.services.tasks.SendQuoteTask;

@PerActivity
public class ProfessionalNewProjectPresenter  extends PresenterBase<INewProjectView>{

    private final int termsOfConditionRequestCode = 333;
    private final ProfessionalNewProjectController controller;
    private final DialogService dialogService;
    private final RoutingService routingService;
    private final EventAggregator eventAggregator;
    private final RouteParams routeParams;
    private final SendQuoteTask sendQuoteTask;

    private VideoState videoState;

    private NewProject project;
    private String priceEstimationText;

    Quote quote;


    @Inject
    public ProfessionalNewProjectPresenter(ProfessionalNewProjectController controller, DialogService dialogService, RoutingService routingService, EventAggregator eventAggregator, RouteParams routeParams, SendQuoteTask sendQuoteTask){
        this.controller = controller;
        this.dialogService = dialogService;
        this.routingService = routingService;
        this.eventAggregator = eventAggregator;
        this.routeParams = routeParams;
        this.sendQuoteTask = sendQuoteTask;
        this.videoState = new InitialVideoState();

        quote = new Quote();
    }

    public void setVideoState(VideoState videoState){
        this.videoState = videoState;
    }

    @RunOnUiThread
    public void setProject(NewProject project){
        this.project = project;
    }

    @Override
    public void initialize() {
        if(project == null)
            return;


        view.showProject(project);
        controller.loadPolicyConfirmationStatus();
    }

    public void takeProject() {
        eventAggregator.publish(new ApplicationEvent(ProfessionalEventNames.NewProjectTakeProject));
        view.showTakeProjectInProgress(true);

        if(controller.isPolicyConfirmed(project)){
            takeProjectCore();
            return;
        }
        view.showTakeProjectInProgress(false);
        view.hideContent();


        navigateToTermsOfService();

       /* routingService.routeTo(Routes.ProfessionalTermsOfServiceAgreement2Route);
        Intent intent = new Intent(context, TermsOfServiceAgreementActivity_.class);
        context.startActivityForResult(intent,termsOfConditionRequestCode);*/
    }

    private void navigateToTermsOfService() {
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.addParameter("requestCode",termsOfConditionRequestCode+"");
        navigationContext.bundle = new Bundle();
        routeParams.setParam("url", UrlConstants.TermsOfServiceUrl);
        routeParams.setParam("title","");
        routeParams.setParam("showActions",true);
        routingService.routeTo(Routes.ProfessionalRegistrationTermsOfServiceAgreement2Route, navigationContext);
    }

    private void takeProjectCore(){
        boolean projectTaken = controller.takeProject(project);
        if(projectTaken) {

            videoState = new JobTakenVideoState();
            controller.sendProjectAcceptedMessage(project);
            view.showTakeProjectInProgress(false);
            view.showActionOptions();
            return;
        }

        final ICommand routeToHomeCommand = new ICommand() {
            @Override
            public void execute() {
                routingService.routeToHome(view.getRoutingContext());
            }
        };

        final String message = "Oops.Could not get this job! Please try again later.";
        view.showTakeProjectInProgress(false);
        view.showMessage(message, routeToHomeCommand);
        return;

    }

    public void callCustomer() {
        eventAggregator.publish(new ApplicationEvent(ProfessionalEventNames.NewProjectCallCustomer));
        boolean success = this.controller.callCustomer(project.getLeadAccountId());
        if(success){
            view.showMessage("Connecting now. Keep phone nearby");
            return;
        }

        final ICommand command = new ICommand() {
            @Override
            public void execute() {
                routingService.routeToHome(view.getRoutingContext());
            }
        };

        final String message = "Oops. Failed establishing a connection. Please try later.";
        view.showMessage(message, command);
    }

    public void routeToHome() {
        routingService.routeToHome(view.getRoutingContext());
    }

    public void routeToPaymentSettingsOrHome() {
        AccountMetadata accountMetadata = controller.getPaymentAccountMetadata();
        if(accountMetadata != null && accountMetadata.isActive())
            routingService.routeToHome(view.getRoutingContext());
        else{
            NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
            routingService.routeTo(Routes.ProfessionalFullPaymentSettingsRoute, navigationContext);
        }
    }

    public void termsOfConditionAccepted(boolean accepted) {
        if(accepted){
            eventAggregator.publish(new ApplicationEvent(ProfessionalEventNames.NewProjectAgreeToTerms));
            confirmPolicy();
            return;
        }

        eventAggregator.publish(new ApplicationEvent(ProfessionalEventNames.NewProjectDisagreeToTerms));
        final ICommand positiveCommand = new ICommand() {
            @Override
            public void execute() {
                routingService.routeToHome(view.getRoutingContext());
            }
        };

        final ICommand negativeCommand= new ICommand() {
            @Override
            public void execute() {
                navigateToTermsOfService();
            }
        };
        DialogContext dialogContext = new DialogContext("Are you sure?","Are you sure you don't want to get job leads from ClipCall?","Yes","Cancel");
        dialogService.show(dialogContext, positiveCommand, negativeCommand);
    }

    private void confirmPolicy() {
        view.showAcceptingPolicy();
        boolean confirmed = controller.confirmPolicy();
        if(!confirmed)
        {
            return;
        }

        takeProjectCore();
    }


    public void quoteNowTapped(){
        eventAggregator.publish(new ApplicationEvent(ProfessionalEventNames.GetLeadQuote));
        videoState = new QuotingVideoState();
        view.showQuoteInput();
    }

    public void textConsumerTapped() {
        eventAggregator.publish(new ApplicationEvent(ProfessionalEventNames.GetLeadTextMe));
        videoState = new TextingMessageVideoState();
        view.showTextInput();
    }

    public void setPriceEstimationText(String priceEstimationText) {
        this.priceEstimationText = priceEstimationText;
    }

    public void cancelTextTapped() {
        routingService.routeToHome(view.getRoutingContext());
    }

    public void sendTextMessage() {
        if(Strings.isNullOrEmpty(priceEstimationText)){
            view.showEstimationTextRequeired();
            return;
        }

        ChatMessageValidationCriteria criteria = new ChatMessageValidationCriteria();
        if(criteria.isSatisfiedBy(priceEstimationText)){
            view.showMessageContainsNonPermittedText();
            return;
        }

        Map<String,String> property = new HashMap<>();
        property.put("priceEstimationText", priceEstimationText);
        eventAggregator.publish(new ApplicationEvent(ProfessionalEventNames.NewProjectTextCustomer,property));
        view.showSendingTextMessage();
        controller.sendEstimationPriceText(project, priceEstimationText);
        view.showMessage("Thank you. The customer may contact you to proceed with the project.");
    }

    public void onBackTapped() {
        videoState.onBackTapped();
    }

    public void setVideoIsPlaying(boolean videoIsPlaying) {
        if(videoIsPlaying)
        {
            eventAggregator.publish(new ApplicationEvent(ProfessionalEventNames.NewProjectPlayVideo));
            videoState = new PlayingVideoState();
        }
        else
            videoState = new InitialVideoState();
    }

    public void handleActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK)
            return;

        if (requestCode != termsOfConditionRequestCode)
            return;

        if(data == null)
            return;


        Bundle extras = data.getExtras();
        boolean accepted = extras.getBoolean("accepted");
        termsOfConditionAccepted(accepted);
    }

    public void sendQuote() {
        if(quote.getPrice() <= 0){
            view.showPriceRequired();
            return;
        }

        if(quote.getPrice() < 0.5){
            view.showQuoteMinValue();
            return;
        }

        if(quote.getStartJobAt() == null)
        {
            view.showStartDateRequired();
            return;
        }
        LocalDate localDateNow = LocalDate.now();
        LocalDate localDate = LocalDate.fromDateFields(quote.getStartJobAt());
        if(localDate.isBefore(localDateNow)){
            view.showStartDateMustBeGreateThanOrEqualToToday();
            return;
        }

        ChatMessageValidationCriteria criteria = new ChatMessageValidationCriteria();
        if(!(Strings.isNullOrEmpty(quote.getDescription())) && criteria.isSatisfiedBy(quote.getDescription())){
            view.showMessageContainsNonPermittedText();
            return;
        }

       /* if(Strings2.isNullOrWhiteSpace(quote.getDescription()))
        {
            view.showQuoteDescriptionIsRequired();
            return;
        }*/

        view.sendQuoteInProgress(true);


        Boolean success = sendQuoteTask.execute(this.project.getLeadId(),this.project.getLeadAccountId(), this.quote);

        eventAggregator.publish(new ApplicationEvent(ProfessionalEventNames.GetLeadQuoteSend  ,quote));

        view.sendQuoteInProgress(false);

        view.showQuoteSent(success);

        videoState = new QuoteSentVideoState();

        final ICommand command = new ICommand() {
            @Override
            public void execute() {
                routeToPaymentSettingsOrHome();
            }
        };

        final String message = "Quote sent. The customer may contact you to proceed with the project.";
        view.showMessage(message, command);
    }

    public void setQuoteStartDateDate(LocalDate quoteStartDateDate) {
        Map<String,String> properties = new HashMap<>();
        properties.put("start date",quoteStartDateDate.toString());
        eventAggregator.publish(new ApplicationEvent(ProfessionalEventNames.GetLeadQuoteDate,properties));
        Date startDate = quoteStartDateDate.toDate();
        quote.setStartJobAt(startDate);
        view.showStartDate(quote);
    }

    public void setQuoteText(String quoteText) {
        Map<String,String> properties = new HashMap<>();
        properties.put("text",quoteText);
        eventAggregator.publish(new ApplicationEvent(ProfessionalEventNames.GetLeadQuoteNote ,properties));
        this.quote.setDescription(quoteText);
    }

    public void setQuotePrice(String quotePrice) {
        if(Strings2.isNullOrWhiteSpace(quotePrice)){
            this.quote.setPrice(0);
            view.showReducedPrice(0, 0);
            return;
        }

        try{

            double price = Double.parseDouble(quotePrice);
            if(price <= 0 ){
                view.showReducedPrice(0, 0);
                return;
            }

            Map<String,String> properties = new HashMap<>();
            properties.put("price",quotePrice);
            eventAggregator.publish(new ApplicationEvent(ProfessionalEventNames.GetLeadQuotePrice,properties));

            this.quote.setPrice(price);
            double feePercentage = controller.getFeePercentage();
            double reducedPrice = controller.calculateReducedPrice(price);
            view.showReducedPrice(reducedPrice, feePercentage);


        }
        catch (Exception e){
            view.showReducedPrice(0, 0);
        }
    }

    public abstract class VideoState {


        public abstract void onBackTapped();

    }

    public  class InitialVideoState extends VideoState{

        @Override
        public void onBackTapped() {
            eventAggregator.publish(new ApplicationEvent(ProfessionalEventNames.NewProjectBackTapped));
            routeToHome();
        }
    }

    public class PlayingVideoState extends VideoState{

        @Override
        public void onBackTapped() {
            view.stopVideo();
        }
    }

    public class JobTakenVideoState extends VideoState{

        @Override
        public void onBackTapped() {
            view.showMessage("Thank you. The customer may contact you to proceed with the project.");
        }
    }

    public class TextingMessageVideoState extends VideoState{

        @Override
        public void onBackTapped() {
            view.showActionOptions();
            videoState = new JobTakenVideoState();
        }
    }

    public class QuotingVideoState extends VideoState{

        @Override
        public void onBackTapped() {
            view.showActionOptions();
            videoState = new JobTakenVideoState();
        }
    }

    public class QuoteSentVideoState extends VideoState{

        @Override
        public void onBackTapped() {
            routeToPaymentSettingsOrHome();
        }
    }
}
