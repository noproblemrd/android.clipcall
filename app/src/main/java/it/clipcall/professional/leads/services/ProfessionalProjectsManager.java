package it.clipcall.professional.leads.services;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.models.VideoHistoryItemBase;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.infrastructure.domainModel.services.IDomainService;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.infrastructure.support.network.RetrofitCallProcessor;
import it.clipcall.professional.leads.models.LeadEntity;
import it.clipcall.professional.leads.models.NewProject;
import it.clipcall.professional.leads.models.PolicyConfirmationStatus;
import it.clipcall.professional.leads.models.ProfessionalProjectDetails;
import it.clipcall.professional.leads.models.ProfessionalSettings;
import it.clipcall.professional.leads.models.ProjectBid;
import it.clipcall.professional.leads.models.Quote;
import it.clipcall.professional.leads.models.QuoteDetails;
import it.clipcall.professional.leads.models.ShareVideoRequest;
import it.clipcall.professional.leads.models.VideoShared;
import retrofit2.Call;

import static it.clipcall.infrastructure.support.network.RetrofitCallProcessor.processCall;

@Singleton
public class ProfessionalProjectsManager implements IDomainService {

    private final IProfessionalProjectsService professionalProjectsService;
    private final List<ProjectEntityBase> projects = new ArrayList<>();

    private final static double defaultFee = 0.10;

    private PolicyConfirmationStatus status;

    public double getProjectFee(String supplierId){
        Call<ProfessionalSettings> proSettingsCall = professionalProjectsService.getProSettings(supplierId);
        ProfessionalSettings professionalSettings = processCall(proSettingsCall);
        if(professionalSettings == null)
            return defaultFee;

        double feeInPrecentage =  professionalSettings.getClipCallFee();
        double fee = feeInPrecentage / 100.0d;
        return fee;
    }

    @Inject
    public ProfessionalProjectsManager(IProfessionalProjectsService professionalProjectsService){
        this.professionalProjectsService = professionalProjectsService;
    }

    public synchronized List<ProjectEntityBase> getProjects(String supplierId, boolean forceReload){
        if(!forceReload && this.projects.size() > 0){
            return this.projects;
        }
        this.projects.clear();

        Call<List<LeadEntity>> getProjectsCall = professionalProjectsService.getProjects(supplierId);
        List<LeadEntity> projects = processCall(getProjectsCall);
        if(Lists.isNullOrEmpty(projects))
            return Lists.empty();

        this.projects.addAll(projects);
        return this.projects;
    }

    public ProjectEntityBase getProjectDetails(String supplierId, final String leadAccountId){

        ProjectEntityBase project = Lists.firstOrDefault(projects, new ICriteria<ProjectEntityBase>() {
            @Override
            public boolean isSatisfiedBy(ProjectEntityBase candidate) {
                return candidate.getId().equals(leadAccountId);
            }
        });

        if(project == null)
            return null;

        LeadEntity leadEntity = (LeadEntity)project;
        if(leadEntity.getProjectDetails() != null)
            return leadEntity;

        Call<ProfessionalProjectDetails> projectDetailsCall = professionalProjectsService.getProjectDetails(supplierId, leadAccountId);
        ProfessionalProjectDetails projectDetails = RetrofitCallProcessor.processCall(projectDetailsCall);
        if(projectDetails == null)
            return null;

        leadEntity.setProjectDetails(projectDetails);
        return leadEntity;
    }

    public ProfessionalProjectDetails getProjectDetails2(String supplierId, final String leadAccountI){
        Call<ProfessionalProjectDetails> getProjectDetailsCall = professionalProjectsService.getProjectDetails(supplierId, leadAccountI);
        ProfessionalProjectDetails professionalProjectDetails = processCall(getProjectDetailsCall);
        return professionalProjectDetails;
    }

    public boolean setChatCreated(String userId, String leadAccountId){
        Call<StatusResponse> statusResponseCall = professionalProjectsService.notifyChatCreated(userId, leadAccountId);
        boolean notified = RetrofitCallProcessor.processStatusCall(statusResponseCall);
        return notified;
    }

    public boolean callCustomer(String supplierId, String leadAccountId){
        Call<StatusResponse> callCustomerCall = professionalProjectsService.callCustomer(supplierId, leadAccountId);
        boolean success = RetrofitCallProcessor.processStatusCall(callCustomerCall);
        return success;
    }


    public boolean takeProject(String supplierId, NewProject project) {
        ProjectBid bid = new ProjectBid("ACCEPTED", 0);
        Call<StatusResponse> statusResponseCall = professionalProjectsService.bidProject(supplierId, project.getLeadAccountId(), bid);
        boolean success = RetrofitCallProcessor.processStatusCall(statusResponseCall);
        return success;
    }

    public List<HistoryItem> getProjectHistoryItems(String userId, String leadAccountId) {
        Call<List<HistoryItem>> historyItemsCall = professionalProjectsService.getHistoryItems(userId, leadAccountId);
        List<HistoryItem> historyItems = processCall(historyItemsCall);
        if(historyItems == null)
            historyItems = new ArrayList<>();
        return historyItems;
    }

    public String shareVideo(String userId, String leadAccountId, VideoHistoryItemBase videoChatHistoryItem) {

        ShareVideoRequest videoShare = new ShareVideoRequest();
        videoShare.setItemId(videoChatHistoryItem.getId());
        videoShare.setLeadAccountId(leadAccountId);
        Call<VideoShared> shareVideoCall = professionalProjectsService.shareVideo(userId, videoShare);
        VideoShared videoShared = processCall(shareVideoCall);
        if(videoShared == null)
            return null;

        if(!videoShared.isSuccess())
            return null;

        return videoShared.getMessage();
    }

    public void loadPolicyConfirmationStatus(String userId) {
        Call<PolicyConfirmationStatus> policyConfirmationStatusCall = professionalProjectsService.getPolicyConfirmationStatus(userId);
        this.status  = processCall(policyConfirmationStatusCall);
    }

    public boolean isPolicyConfirmed(){
        if(this.status != null && this.status.getIsConfirmed())
            return true;

        return false;
    }

    public boolean confirmPolicy(String userId){
        Call<StatusResponse> statusResponseCall = professionalProjectsService.confirmPolicy(userId);
        boolean success = RetrofitCallProcessor.processStatusCall(statusResponseCall);
        if(!success)
            return false;


        status = new PolicyConfirmationStatus();
        status.setIsConfirmed(true);
        return true;
    }

    public QuoteDetails createQuote(String userId, String projectId, Quote quote) {
        Call<QuoteDetails> createQuoteCall = professionalProjectsService.createQuote(userId, projectId, quote);
        QuoteDetails createdQuote = processCall(createQuoteCall);
        return createdQuote;
    }
}
