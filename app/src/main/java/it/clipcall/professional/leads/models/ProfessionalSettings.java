package it.clipcall.professional.leads.models;

/**
 * Created by dorona on 17/07/2016.
 */

public class ProfessionalSettings {

    double clipCallFee;

    public double getClipCallFee() {
        return clipCallFee;
    }

    public void setClipCallFee(double clipCallFee) {
        this.clipCallFee = clipCallFee;
    }
}
