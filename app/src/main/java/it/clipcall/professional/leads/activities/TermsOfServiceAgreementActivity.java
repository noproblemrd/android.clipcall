package it.clipcall.professional.leads.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FromHtml;
import org.androidannotations.annotations.ViewById;

import it.clipcall.R;

@EActivity(R.layout.terms_of_service_agreement_widget)
public class TermsOfServiceAgreementActivity extends AppCompatActivity{


    @ViewById
    @FromHtml(R.string.perform_professional_work)
    TextView professionalWorkTextView;

    @ViewById
    @FromHtml(R.string.tos_work_guarantee)
    TextView workGuaranteeTextView;

    @ViewById
    @FromHtml(R.string.registration_tos_payment_title)
    TextView paymentTitleTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Click(R.id.declineButton)
    void declineTermsOfConditions(){
        setAccepted(false);
    }


    @Click(R.id.acceptButton)
    void acceptTermsOfConditions(){
        setAccepted(true);
    }

    private void setAccepted(boolean accepted){
        Intent intent = this.getIntent();
        intent.putExtra("accepted", accepted);
        this.setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        setAccepted(false);
    }
}
