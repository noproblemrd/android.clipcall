package it.clipcall.professional.leads.models;

import com.google.common.base.Strings;

import org.parceler.Parcel;

import java.util.Date;

import it.clipcall.common.projects.models.ProjectDetails;
import it.clipcall.common.projects.models.ProjectStatus;

/**
 * Created by dorona on 29/02/2016.
 */
@Parcel
public class ProfessionalProjectDetails extends ProjectDetails {

    String leadId;
    String categoryName;
    String region;
    String fullAddress;
    Date createdOn;
    ProjectStatus status;
    String leadAccountId;
    String videoUrl;
    String previewVideoImageUrl;
    int videoDuration;
    boolean isPrivate;
    String customerId;
    String customerName;
    String customerProfileImage;
    String phoneNumber;
    boolean isAnonymous;
    Quote lastQuote;




    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getLeadAccountId() {
        return leadAccountId;
    }

    public void setLeadAccountId(String leadAccountId) {
        this.leadAccountId = leadAccountId;
    }

    public String getPreviewVideoImageUrl() {
        return previewVideoImageUrl;
    }

    public void setPreviewVideoImageUrl(String previewVideoImageUrl) {
        this.previewVideoImageUrl = previewVideoImageUrl;
    }

    public ProjectStatus getStatus() {
        return status;
    }

    public void setStatus(ProjectStatus status) {
        this.status = status;
    }

    public int getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(int videoDuration) {
        this.videoDuration = videoDuration;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getCustomerProfileImage() {
        return customerProfileImage;
    }

    public void setCustomerProfileImage(String customerProfileImage) {
        this.customerProfileImage = customerProfileImage;
    }

    public String getCustomerDisplayName(){

        String customerName = getCustomerName();
        if(!Strings.isNullOrEmpty(customerName))
            return customerName;

        if(isAnonymous)
            return Constants.AnonymousUser;

        return getPhoneNumber();
    }

    public boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }


    public String getCategoryName() {
        return categoryName;
    }

    public void setCategory(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public boolean getIsAnonymous(){
        return isAnonymous;
    }

    public void setIsAnonymous(boolean isAnonymous){
        this.isAnonymous = isAnonymous;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public Quote getLastQuote() {
        return lastQuote;
    }

    public void setLastQuote(Quote lastQuote) {
        this.lastQuote = lastQuote;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
