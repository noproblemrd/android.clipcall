package it.clipcall.professional.leads.activities;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.professional.leads.fragments.ProfessionalProjectDetailsAboutFragment_;
import it.clipcall.professional.leads.fragments.ProfessionalProjectDetailsHistoryFragment_;
import it.clipcall.professional.leads.models.LeadEntity;
import it.clipcall.professional.leads.models.ProfessionalProjectDetails;
import it.clipcall.professional.leads.presenters.ProfessionalProjectDetailsPresenter;
import it.clipcall.professional.leads.support.ProfessionalProjectDetailsTabsFragmentAdapter;
import it.clipcall.professional.leads.views.IProfessionalProjectDetailsView;

@EActivity(R.layout.project_advertiser_activity)
public class ProfessionalProjectDetailsActivity extends BaseActivity implements IProfessionalProjectDetailsView, IHasToolbar {

    @ViewById
    Toolbar toolbar;

    @ViewById
    TabLayout tabLayout;

    @ViewById
    ViewPager viewPager;

    @Inject
    ProfessionalProjectDetailsPresenter presenter;

    @ViewById
    ImageView advertiserProfileLogoImageView;

    @ViewById
    TextView advertiserNameTextView;

    @ViewById
    ViewGroup clipcallHeartContainer;

    ProfessionalProjectDetailsTabsFragmentAdapter adapter;

    @ViewById
    ViewGroup root;

    @ViewById
    CollapsingToolbarLayout collapsingToolbarLayout;

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        clipcallHeartContainer.setVisibility(View.GONE);
        ProjectEntityBase project = presenter.getProject();
        setupViewPager(project);
        Picasso.with(getBaseContext()).load(project.getProfileUrl())
                .placeholder(R.drawable.consumer_profile_logo_placeholder3)
                .into(advertiserProfileLogoImageView);

        int transparentColor = ContextCompat.getColor(this, android.R.color.transparent);
        collapsingToolbarLayout.setCollapsedTitleTextColor(transparentColor);
        collapsingToolbarLayout.setExpandedTitleColor(transparentColor);

        presenter.initialize();
    }

    private void setupViewPager(ProjectEntityBase project) {
        adapter = new ProfessionalProjectDetailsTabsFragmentAdapter(getSupportFragmentManager());
        adapter.addFragment(ProfessionalProjectDetailsAboutFragment_.builder().leadAccountId(project.getId()).build(), "About");
        adapter.addFragment(ProfessionalProjectDetailsHistoryFragment_.builder().leadAccountId(project.getId()).build(), "Records");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.shutDown();
    }

    @Override
    public String getViewTitle() {
        return "PROJECT DETAILS";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }

    @Override
    public void showProjectDetails(LeadEntity project) {

        ProfessionalProjectDetails projectDetails = project.getProjectDetails();
        String displayName = projectDetails.getCustomerDisplayName();
        if(!Strings.isNullOrEmpty(displayName)){
            advertiserNameTextView.setVisibility(View.VISIBLE);
            advertiserNameTextView.setText(displayName);
        }else{
            advertiserNameTextView.setVisibility(View.INVISIBLE);
        }

        if(projectDetails.getStatus().getStatus().toUpperCase().equals("MISSED")){
            advertiserNameTextView.setVisibility(View.VISIBLE);
            advertiserNameTextView.setText("Missed");
        }

        adapter.setData(projectDetails);
        adapter.setParentView(this);
    }

    @Override
    public void notifyCallingCustomer() {
        Snackbar snackbar = Snackbar
                .make(root, "Connecting now. Keep phone nearby", Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}
