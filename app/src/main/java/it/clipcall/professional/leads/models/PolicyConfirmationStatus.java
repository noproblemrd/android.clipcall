package it.clipcall.professional.leads.models;

/**
 * Created by dorona on 29/05/2016.
 */
public class PolicyConfirmationStatus {

    private boolean isConfirmed;

    public boolean getIsConfirmed() {
        return isConfirmed;
    }

    public void setIsConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }
}
