package it.clipcall.professional.leads.fragments;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.kennyc.view.MultiStateView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.simple.eventbus.Subscriber;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.projects.models.ProjectDetails;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.professional.leads.models.LeadEntity;
import it.clipcall.professional.leads.models.ProfessionalProjectDetails;
import it.clipcall.professional.leads.models.Quote;
import it.clipcall.professional.leads.presenters.ProfessionalProjectDetailsAboutPresenter;
import it.clipcall.professional.leads.views.IProfessionalProjectDetailsAboutView;

@EFragment(R.layout.professional_project_details_about_fragment)
public class ProfessionalProjectDetailsAboutFragment extends ProfessionalProjectDetailsTabFragment implements IProfessionalProjectDetailsAboutView {

    @FragmentArg
    String leadAccountId;

    @ViewById
    TextView addressTextView;

    @ViewById
    TextView categoriesTextView;

    @ViewById
    ImageView chatImageView;

    @ViewById
    ImageView callProImageView;

    @ViewById
    ImageView remoteHouseCallImageView;

    @ViewById
    ProgressBar actionsProgressBar;

    @ViewById
    ViewGroup actionsContainer;


    @ViewById
    TextView badgeTextView;

    @ViewById
    ViewGroup phoneNumberContainer;

    @ViewById
    ViewGroup meetingContainer;


    @ViewById
    TextView sendQuoteTextView;

    @ViewById
    ViewGroup quoteContainer;

    @ViewById
    com.kennyc.view.MultiStateView multiStateView;

    @Inject
    ProfessionalProjectDetailsAboutPresenter presenter;

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.shutDown();

    }

    @AfterViews
    public void afterViews(){
       this.presenter.bindView(this);
    }


    @Background
    void initialize(){
        presenter.initialize();
    }

    @Click(R.id.remoteHouseCallImageView)
    void onStartRemoteHouseCallTapped(){
        remoteHouseCallImageView.setEnabled(false);
        startRemoteHouseCall();
    }

    @Click(R.id.sendQuoteTextView)
    void onSendQuoteTapped(){
        presenter.sendQuote();
    }

    @Click(R.id.chatImageView)
    void onStartChatTapped(){
        //chatImageView.setEnabled(false);
        startChat();
    }

    @Background
    void startChat() {
        presenter.navigateToChat();

    }


    @Click(R.id.callProImageView)
    void callProTapped(){
        callProImageView.setEnabled(false);
        callAdvertiser();
    }


    @Background
    void callAdvertiser(){
        presenter.callCustomer();
    }



    @Background
    void startRemoteHouseCall() {
        presenter.startRemoteHouseCall();
    }

    @Subscriber
    void onLiked(Boolean successfully){
        if(successfully){

        }
    }

    @Subscriber
    void onCalledAdvertiser(Boolean successfully){
        enableCallAdvertiser();
    }

    @UiThread
    void enableCallAdvertiser(){
        callProImageView.setEnabled(true);
    }

    @Click(R.id.meetingTextView)
    void addMeetingToCalendarTapped(){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("content://com.android.calendar/time"));

        startActivity(i);
//        intent.setType("vnd.android.cursor.item/event");
//        intent.putExtra("title", "Some title");
//        intent.putExtra("description", "Some description");
//        intent.pu5tExtra("beginTime", eventStartInMillis);
//        intent.putExtra("endTime", eventEndInMillis);
//        startActivity(intent);
    }

    @Click(R.id.payTheProTextView)
    void onPayTheProTapped(){


    }

    @Override
    public RoutingContext getRoutingContext() {
      return new RoutingContext(getActivity());
    }


    @UiThread
    @Override
    public void startRemoteCallFailed() {
        enableActions(true);
    }

    @UiThread
    @Override
    public void notifyCallingCustomer() {
        parentView.notifyCallingCustomer();
    }

    @Click(R.id.addressTextView)
    void addressTapped(){
        Intent geoIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q="
                +addressTextView.getText().toString()));
        startActivity(geoIntent);
    }

    /*
    @Click(R.id.phoneNumberTextView)
    void onPhoneTapped(){
        Uri number = Uri.parse("tel:"+phoneNumberTextView.getText().toString());
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
        startActivity(callIntent);
    }
    */

    @UiThread
    public  void enableActions(boolean enable){
      /*  callProImageView.setEnabled(enable);
        remoteHouseCallImageView.setEnabled(enable);
        chatImageView.setEnabled(enable);*/
        if(enable){
            showActions();
        }else{
            hideActions();
        }
    }

    private void showActions(){
       actionsContainer.setVisibility(View.VISIBLE);
       actionsProgressBar.setVisibility(View.INVISIBLE);

        remoteHouseCallImageView.setEnabled(true);
        callProImageView.setEnabled(true);
        chatImageView.setEnabled(true);
    }

    private void hideActions(){
        actionsContainer.setVisibility(View.INVISIBLE);
        actionsProgressBar.setVisibility(View.VISIBLE);

        remoteHouseCallImageView.setEnabled(false);
        callProImageView.setEnabled(false);
        chatImageView.setEnabled(false);
    }

    @UiThread
    @Override
    public void enableChatAction(boolean enable) {
        chatImageView.setEnabled(enable);
    }

    @UiThread
    @Override
    public void showErrorCreatingVideoChatSession() {

    }

    @Background
    @Override
    public void setProjectDetails(ProjectDetails projectDetails) {
        ProfessionalProjectDetails proProjectDetails = (ProfessionalProjectDetails)projectDetails;
        presenter.setProjectDetails(proProjectDetails);

    }

    @Override
    public void showProjectDetails(LeadEntity project, ProfessionalProjectDetails projectDetails) {
        addressTextView.setText(projectDetails.getFullAddress());
        categoriesTextView.setText(projectDetails.getCategoryName());
        multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);


        Quote lastQuote = project.getLastQuote();
        if(lastQuote == null){
            quoteContainer.setVisibility(View.VISIBLE);
            sendQuoteTextView.setText("Send a quote");
        }else if(lastQuote.isActive()){
            quoteContainer.setVisibility(View.VISIBLE);
            sendQuoteTextView.setText("Update your quote");
        }else{

        }
        boolean projectMissed = project.isMissed();
        if(projectMissed){
            phoneNumberContainer.setVisibility(View.GONE);
            meetingContainer.setVisibility(View.GONE);
            quoteContainer.setVisibility(View.GONE);
        }

    }



    @UiThread
    @Override
    public void showTotalUnreadMessages(int totalUnreadMessages) {
        if(totalUnreadMessages <= 0){
            badgeTextView.setVisibility(View.GONE);
        }else{
            badgeTextView.setVisibility(View.VISIBLE);
            badgeTextView.setText(""+totalUnreadMessages);
            YoYo.with(Techniques.Bounce)
                    .duration(700)
                    .playOn(badgeTextView);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        enableActions(true);
        showActions();
        initialize();
        presenter.loadUnreadMessages();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.shutDown();
    }
}