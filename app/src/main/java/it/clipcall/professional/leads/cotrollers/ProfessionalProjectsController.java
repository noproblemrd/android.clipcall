package it.clipcall.professional.leads.cotrollers;

import com.google.common.base.Strings;
import com.google.common.collect.Ordering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.models.VideoHistoryItemBase;
import it.clipcall.consumer.vidoechat.models.VideoSessionData;
import it.clipcall.consumer.vidoechat.services.VideoChatCoordinator;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.professional.leads.models.ProfessionalProjectDetails;
import it.clipcall.professional.leads.models.criterias.ProjectFilterCriteria;
import it.clipcall.professional.leads.services.ProfessionalProjectsManager;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.services.ProfessionalManager;
import it.clipcall.professional.remoteservicecall.services.ProfessionalVideoChatManager;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by dorona on 18/01/2016.
 */
@Singleton
public class ProfessionalProjectsController {

    private final AuthenticationManager authenticationManager;

    private final ProfessionalManager professionalManager;
    private final ProfessionalProjectsManager professionalProjectsManager;
    private final ChatManager chatManager;
    private final ProfessionalVideoChatManager videoChatManager;
    private final VideoChatCoordinator videoChatCoordinator;

    @Inject
    public ProfessionalProjectsController(AuthenticationManager authenticationManager,
                                          ProfessionalManager professionalManager,
                                          ProfessionalProjectsManager professionalProjectsManager,
                                          ChatManager chatManager,
                                          ProfessionalVideoChatManager videoChatManager, VideoChatCoordinator videoChatCoordinator) {
        this.authenticationManager = authenticationManager;
        this.professionalManager = professionalManager;
        this.professionalProjectsManager = professionalProjectsManager;
        this.chatManager = chatManager;
        this.videoChatManager = videoChatManager;
        this.videoChatCoordinator = videoChatCoordinator;
    }


    private  List<ProjectEntityBase>  getProfessionalProjectsCore(boolean forceReload){
        if(!authenticationManager.isProfessionalAuthenticated())
            return Lists.empty();

        String supplierId = authenticationManager.getSupplierId();
        if(Strings.isNullOrEmpty(supplierId))
            return Lists.empty();

        List<ProjectEntityBase> projects = professionalProjectsManager.getProjects(supplierId, forceReload);
        List<ProjectEntityBase> projectsCopy = new ArrayList<>(projects);
        return projectsCopy;
    }

    public List<ProjectEntityBase> getProfessionalProjects(boolean forceReload){
        List<ProjectEntityBase> projects = getProfessionalProjectsCore(forceReload);
        String customerId = authenticationManager.getCustomerId();
        if(chatManager.isConnected(customerId)){
            for(ProjectEntityBase project: projects){
                project.updateUnreadMessageCount(chatManager);
                project.updateLastUnreadMessage(chatManager, customerId);
            }
        }

        Collections.sort(projects, new Ordering<ProjectEntityBase>() {
            @Override
            public int compare(ProjectEntityBase left, ProjectEntityBase right) {
                return right.getLastUpdateDate().compareTo(left.getLastUpdateDate());
            }
        });

        return projects;
    }


    public ProjectEntityBase getProjectDetails(String leadAccountId){
        if(!authenticationManager.isProfessionalAuthenticated())
            return null;

        String supplierId = authenticationManager.getSupplierId();
        if(Strings.isNullOrEmpty(supplierId))
            return null;


        ProjectEntityBase projectDetails = professionalProjectsManager.getProjectDetails(supplierId, leadAccountId);
        if(projectDetails == null)
            return null;


        String customerId = authenticationManager.getCustomerId();
        projectDetails.updateUnreadMessageCount(chatManager);
        projectDetails.updateLastUnreadMessage(chatManager, customerId);
        return projectDetails;
    }


    public boolean callCustomer(String leadAccountId){
        if(!authenticationManager.isProfessionalAuthenticated())
            return false;

        String supplierId = authenticationManager.getSupplierId();
        if(Strings.isNullOrEmpty(supplierId))
            return false;


        boolean success = professionalProjectsManager.callCustomer(supplierId, leadAccountId);
        return success;
    }

    public ProfessionalProfile getProfessionalProfile(){
        String supplierId = authenticationManager.getSupplierId();
        if(Strings.isNullOrEmpty(supplierId))
            return null;

        ProfessionalProfile profile = professionalManager.getProfessionalProfile(supplierId);
        return profile;
    }

    public String getUserId() {
        return authenticationManager.getSupplierId();
    }

    public String getProConsumerId(){
        return authenticationManager.getCustomerId();
    }

    public VideoSessionData createVideoChatSession(String leadAccountId) {
        String userId = authenticationManager.getSupplierId();
        VideoSessionData videoSession = videoChatManager.createVideoChatSession(userId, leadAccountId);
        return videoSession;
    }

    public List<HistoryItem> getProjectHistoryItems(ProfessionalProjectDetails projectDetails) {
        String userId = authenticationManager.getUserId();
        List<HistoryItem> historyItems = professionalProjectsManager.getProjectHistoryItems(userId, projectDetails.getLeadAccountId());
        return historyItems;
    }

    public String shareVideo(String leadAccountId, VideoHistoryItemBase videoChatHistoryItem) {
        String userId = authenticationManager.getUserId();
        String message = professionalProjectsManager.shareVideo(userId, leadAccountId, videoChatHistoryItem);
        return message;
    }

    public void connectToVideoChatInBackground(VideoSessionData videoSessionData) {
        videoChatCoordinator.connectToSession(videoSessionData);
    }

    public void initializePublisher(String publisherName) {
        videoChatCoordinator.initializePublisher(publisherName);
    }

    public List<ProjectEntityBase> getProjectsByFilter(String filter) {

        List<ProjectEntityBase> projects = this.getProfessionalProjectsCore(false);

        if(Strings.isNullOrEmpty(filter))
            return projects;

        List<ProjectEntityBase> filteredProjects = Lists.where(projects, new ProjectFilterCriteria(filter));
        return filteredProjects;
    }
}
