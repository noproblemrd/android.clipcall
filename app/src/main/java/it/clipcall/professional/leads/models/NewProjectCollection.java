package it.clipcall.professional.leads.models;


import org.parceler.Parcel;

import java.io.Serializable;
import java.util.List;

import it.clipcall.infrastructure.support.collections.Lists;

@Parcel
public class NewProjectCollection implements Serializable {


    List<NewProject> projects;

    public List<NewProject> getProjects() {
        return projects;
    }

    public void setProjects(List<NewProject> projects) {
        this.projects = projects;
    }

    public void addProject(NewProject project){
        this.projects.add(project);
    }

    public NewProject removeFirst(){
        if(Lists.isNullOrEmpty(projects))
            return null;

        return projects.remove(0);
    }

    public void clearAll() {
        projects.clear();
    }
}
