package it.clipcall.professional.leads.support;

import android.support.v4.app.FragmentManager;

import it.clipcall.professional.leads.fragments.ProfessionalProjectDetailsTabFragment;
import it.clipcall.professional.leads.views.IProfessionalProjectDetailsView;
import it.clipcall.common.projects.models.ProjectDetails;
import it.clipcall.infrastructure.support.fragments.AbstractFragmentAdapter;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 29/02/2016.
 */
public class ProfessionalProjectDetailsTabsFragmentAdapter extends AbstractFragmentAdapter<ProfessionalProjectDetailsTabFragment> {
    public ProfessionalProjectDetailsTabsFragmentAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public void setData(Object data) {
        ProjectDetails projectDetails = (ProjectDetails) data;
        for (int i = 0; i < getCount(); i++) {
            ProfessionalProjectDetailsTabFragment fragment =  getItem(i);
            fragment.setProjectDetails(projectDetails);
        }

    }

    @Override
    public <T extends IView> void setParentView(T view) {
        if(view instanceof IProfessionalProjectDetailsView){
            IProfessionalProjectDetailsView proProjectDetailsView = (IProfessionalProjectDetailsView) view;
            setParentView(view);

        }
    }


    public void setParentView(IProfessionalProjectDetailsView view){
        for (int i = 0; i < getCount(); i++) {
            ProfessionalProjectDetailsTabFragment fragment =  getItem(i);
            fragment.setParentView(view);
        }
    }
}
