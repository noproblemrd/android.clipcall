package it.clipcall.professional.leads.models;

/**
 * Created by dorona on 30/05/2016.
 */
public class NewProjectChatData {

    public String category;
    public String professionalName;
    public String leadId;
}
