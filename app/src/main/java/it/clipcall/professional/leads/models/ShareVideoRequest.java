package it.clipcall.professional.leads.models;

/**
 * Created by dorona on 21/04/2016.
 */
public class ShareVideoRequest {

    private String leadAccountId;
    private String itemId;

    public String getLeadAccountId() {
        return leadAccountId;
    }

    public void setLeadAccountId(String leadAccountId) {
        this.leadAccountId = leadAccountId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
}
