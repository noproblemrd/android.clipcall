package it.clipcall.professional.payment.models;

import android.location.Address;

import com.stripe.android.model.Token;

import java.util.Date;

/**
 * Created by dorona on 24/07/2016.
 */

public class PaymentSettings {

    PaymentSettingsPaymentDetails paymentDetails;
    PaymentSettingsPersonalDetails personalDetails;
    boolean isNew;


    public PaymentSettings(){
        personalDetails = new PaymentSettingsPersonalDetails();
        paymentDetails = new PaymentSettingsPaymentDetails();
    }



    public PaymentSettingsPaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(PaymentSettingsPaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public PaymentSettingsPersonalDetails getPersonalDetails() {
        return personalDetails;
    }

    public void setPersonalDetails(PaymentSettingsPersonalDetails personalDetails) {
        this.personalDetails = personalDetails;
    }

    public void setFirstName(String firstName) {
        this.personalDetails.setFirstName(firstName);
    }

    public void setLastName(String lastName) {
        this.personalDetails.setLastName(lastName);
    }

    public void setSslLastFourDigits(String ssnLastFourDigits) {
        this.personalDetails.setSsnLastFourDigits(ssnLastFourDigits);
    }

    public void setTaxId(String taxId) {
        this.personalDetails.setTaxId(taxId);
    }

    public void setDateOfBirth(Date dateOfBirth) {
        personalDetails.setDateOfBirth(dateOfBirth);
    }

    public void setAccountNumber(String accountNumber) {
        paymentDetails.setAccountNumber(accountNumber);
    }

    public void setRoutingNumber(String routingNumber) {
        paymentDetails.setRoutingNumber(routingNumber);
    }

    public void setBusinessName(String businessName) {
        personalDetails.setBusinessName(businessName);
    }

    public void setCardNumber(String cardNumber) {
        paymentDetails.setCardNumber(cardNumber);
    }

    public void setCvv(String cvv) {
        paymentDetails.setCvv(cvv);
    }

    public void setExpirationYear(int expirationYear) {
        paymentDetails.setExpirationYear(expirationYear);
    }

    public void setExpirationMonth(int expirationMonth) {
        paymentDetails.setExpirationMonth(expirationMonth);
    }

    public void setAddress(Address address, String fullAddress) {
        PaymentAddress paymentAddress = new PaymentAddress();
        paymentAddress.setFullAddress(fullAddress);
        paymentAddress.setLine1(address.getAddressLine(0));
        if(address.getMaxAddressLineIndex() >=1){
            paymentAddress.setLine2(address.getAddressLine(1));
        }
        paymentAddress.setCity(address.getLocality());
        paymentAddress.setPostalCode(address.getPostalCode());
        paymentAddress.setState(address.getAdminArea());
        paymentAddress.setLatitude(address.getLatitude());
        paymentAddress.setLongitude(address.getLongitude());
        personalDetails.setAddress(paymentAddress);
    }

    public void setIsCompany(boolean isCompany) {
        this.personalDetails.setCompany(isCompany);
    }

    public void setIsBankTransfer(boolean isBankTransfer) {
        paymentDetails.setBankTransfer(isBankTransfer);
    }

    public void setAccountHolderName(String accountHolderName) {
        paymentDetails.setAccountHolderName(accountHolderName);
    }

    public void setToken(Token token) {
        paymentDetails.setToken(token);
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
        if(this.isNew)
           return;


        paymentDetails.setReadOnlyMode();

    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }
}
