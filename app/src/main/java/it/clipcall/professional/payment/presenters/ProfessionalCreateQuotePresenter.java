package it.clipcall.professional.payment.presenters;

import com.google.common.base.Strings;

import org.joda.time.LocalDate;

import java.util.Date;

import javax.inject.Inject;

import it.clipcall.common.chat.models.ChatMessageValidationCriteria;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.infrastructure.commands.ICommand;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.strings.Strings2;
import it.clipcall.infrastructure.ui.dialogs.DialogService;
import it.clipcall.professional.leads.cotrollers.ProfessionalNewProjectController;
import it.clipcall.professional.leads.models.LeadEntity;
import it.clipcall.professional.leads.models.Quote;
import it.clipcall.professional.payment.services.tasks.SendQuoteTask;
import it.clipcall.professional.payment.viewModel.views.ICreateQuoteView;

@PerActivity
public class ProfessionalCreateQuotePresenter extends PresenterBase<ICreateQuoteView>{

    private final ProfessionalNewProjectController controller;
    private final DialogService dialogService;
    private final RoutingService routingService;
    private final EventAggregator eventAggregator;
    private final SendQuoteTask sendQuoteTask;

    ProjectEntityBase project;

    Quote quote;


    @Inject
    public ProfessionalCreateQuotePresenter(ProfessionalNewProjectController controller, DialogService dialogService, RoutingService routingService, EventAggregator eventAggregator, RouteParams routeParams, SendQuoteTask sendQuoteTask){
        this.controller = controller;
        this.dialogService = dialogService;
        this.routingService = routingService;
        this.eventAggregator = eventAggregator;
        this.sendQuoteTask = sendQuoteTask;

        String leadAccountId  = routeParams.getParam("leadAccountId");
        this.project = routeParams.getParam("project");
        this.quote = new Quote();
        Quote lastQuote = this.project.getLeadAccountLastQuote(leadAccountId);
        if(lastQuote != null){
            this.quote.setDescription(this.project.getLastQuote().getDescription());
        }
    }

    @Override
    public void initialize() {
        LocalDate now = LocalDate.now();
        setQuoteStartDateDate(now);
        view.showQuote(this.quote);
    }

    public void sendQuote() {
        if(quote.getPrice() <= 0){
            view.showPriceRequired();
            return;
        }

        if(quote.getPrice() < 0.5){
            view.showQuoteMinValue();
            return;
        }

        if(quote.getStartJobAt() == null)
        {
            view.showStartDateRequired();
            return;
        }

        ChatMessageValidationCriteria criteria = new ChatMessageValidationCriteria();
        if(!(Strings.isNullOrEmpty(quote.getDescription())) && criteria.isSatisfiedBy(quote.getDescription())){
            view.showMessageContainsNonPermittedText();
            return;
        }

        LocalDate localDateNow = LocalDate.now();

        LocalDate localDate = LocalDate.fromDateFields(quote.getStartJobAt());
        if(localDate.isBefore(localDateNow)){
            view.showStartDateMustBeGreaterThanOrEqualToToday();
            return;
        }

        LeadEntity leadEntity = (LeadEntity) project;

        view.sendQuoteInProgress(true);
        boolean quoteCreated = sendQuoteTask.execute(leadEntity.getLeadId(),leadEntity.getLeadAccountId(), quote);
        if(!quoteCreated ){
            view.sendQuoteInProgress(false);
            view.showQuoteSubmissionFaliure();
            return;
        }

        view.sendQuoteInProgress(false);

        final ICommand command = new ICommand() {
            @Override
            public void execute() {
                routingService.routeToHome(view.getRoutingContext());
            }
        };

        final String message = "Quote sent. The customer may contact you to proceed with the project.";
        view.updateQuoteSent();
        view.showMessage(message, command);
    }

    public void setQuoteStartDateDate(LocalDate quoteStartDateDate) {
        Date startDate = quoteStartDateDate.toDate();
        quote.setStartJobAt(startDate);
        view.showStartDate(quote);
    }

    public void setQuoteText(String quoteText) {
        this.quote.setDescription(quoteText);
    }


    public void setQuotePrice(String quotePrice) {
        if(Strings2.isNullOrWhiteSpace(quotePrice)){
            this.quote.setPrice(0);
            view.showReducedPrice(0, 0);
            return;
        }

        try{

            double price = Double.parseDouble(quotePrice);
            if(price <= 0 ){
                view.showReducedPrice(0, 0);
                return;
            }

            this.quote.setPrice(price);
            double feePercentage = controller.getFeePercentage();
            double reducedPrice = controller.calculateReducedPrice(price);
            view.showReducedPrice(reducedPrice, feePercentage);
        }
        catch (Exception e){
            view.showReducedPrice(0, 0);
        }
    }
}
