package it.clipcall.professional.payment.services;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.infrastructure.support.network.RetrofitCallProcessor;
import it.clipcall.professional.payment.models.AccountMetadata;
import it.clipcall.professional.payment.models.PaymentSettings;
import retrofit2.Call;

/**
 * Created by dorona on 25/07/2016.
 */

@Singleton
public class ProfessionalPaymentManager {

    private final IProfessionalPaymentService professionalPaymentService;
    @Inject
    public ProfessionalPaymentManager(IProfessionalPaymentService professionalPaymentService){

        this.professionalPaymentService = professionalPaymentService;
    }

    public boolean savePaymentSettings(String supplierId, PaymentSettings paymentSettings){
        Call<StatusResponse> statusResponseCall = professionalPaymentService.savePaymentSettings(supplierId, paymentSettings);
        boolean success = RetrofitCallProcessor.processStatusCall(statusResponseCall);
        return success;
    }

    public PaymentSettings getPaymentSettings(String supplierId){
        Call<PaymentSettings> paymentSettingsCall = professionalPaymentService.getPaymentSettings(supplierId);
        PaymentSettings paymentSettings = RetrofitCallProcessor.processCall(paymentSettingsCall);
        if(paymentSettings == null){
            paymentSettings = new PaymentSettings();
            paymentSettings.setIsNew(true);
        }
        return paymentSettings;
    }

    public AccountMetadata getPaymentAccountMetadata(String supplierId){
        Call<AccountMetadata> paymentSettingsCall = professionalPaymentService.getAccountMetadata(supplierId);
        AccountMetadata accountMetadata = RetrofitCallProcessor.processCall(paymentSettingsCall);
        return accountMetadata;
    }

    public boolean disconnectFromStandaloneAccount(String supplierId){
        Call<StatusResponse> disconnectStatusCall = professionalPaymentService.disconnectFromStandaloneAccount(supplierId);
        boolean success = RetrofitCallProcessor.processStatusCall(disconnectStatusCall);
        return success;
    }
}
