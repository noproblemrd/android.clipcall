package it.clipcall.professional.payment.models;

/**
 * Created by dorona on 24/07/2016.
 */

public class AccountMetadata {

    String accountStatus;
    String accountType;

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public boolean isActive(){
        return "ACTIVE".equals(accountStatus.toUpperCase());
    }

    public boolean isManaged(){
        return "MANAGED".equals(accountType.toUpperCase());
    }
}
