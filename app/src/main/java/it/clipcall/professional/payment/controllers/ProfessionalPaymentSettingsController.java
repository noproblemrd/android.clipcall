package it.clipcall.professional.payment.controllers;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.invitations.services.BranchIoManager;
import it.clipcall.professional.payment.models.AccountMetadata;
import it.clipcall.professional.payment.models.PaymentSettings;
import it.clipcall.professional.payment.services.ProfessionalPaymentManager;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by dorona on 18/01/2016.
 */
@Singleton
public class ProfessionalPaymentSettingsController {

    private final AuthenticationManager authenticationManager;
    private final ProfessionalPaymentManager professionalPaymentManager;
    private final BranchIoManager branchIoManager;

    @Inject
    public ProfessionalPaymentSettingsController(AuthenticationManager authenticationManager, ProfessionalPaymentManager professionalPaymentManager, BranchIoManager branchIoManager) {
        this.authenticationManager = authenticationManager;
        this.professionalPaymentManager = professionalPaymentManager;
        this.branchIoManager = branchIoManager;
    }

    public PaymentSettings getPaymentDetails() {
        String supplierId = authenticationManager.getSupplierId();
        PaymentSettings paymentSettings = professionalPaymentManager.getPaymentSettings(supplierId);
        return paymentSettings;
    }

    public boolean disconnectFromStandaloneAccount(){
        String supplierId = authenticationManager.getSupplierId();
        boolean disconnected = professionalPaymentManager.disconnectFromStandaloneAccount(supplierId);
        return disconnected;
    }

    public boolean savePaymentSettings(PaymentSettings paymentSettings) {
        String supplierId = authenticationManager.getSupplierId();
        boolean success = professionalPaymentManager.savePaymentSettings(supplierId, paymentSettings);
        return success;
    }

    public AccountMetadata getPaymentAccountMetadata() {
        String supplierId = authenticationManager.getSupplierId();
        AccountMetadata accountMetadata =  professionalPaymentManager.getPaymentAccountMetadata(supplierId);
        return accountMetadata;
    }

    public String getProfessionalId() {
        return authenticationManager.getSupplierId();
    }
}
