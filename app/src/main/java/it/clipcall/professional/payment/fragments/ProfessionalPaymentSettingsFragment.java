package it.clipcall.professional.payment.fragments;

import android.app.DatePickerDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.kennyc.view.MultiStateView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;
import org.joda.time.LocalDate;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.payment.viewModel.support.CardResourceResolver;
import it.clipcall.infrastructure.commands.EmptyCallBack;
import it.clipcall.infrastructure.commands.PickGoogleAddressCommand;
import it.clipcall.infrastructure.converters.CharSequenceToIntConverter;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.maps.entities.PlaceData;
import it.clipcall.infrastructure.resources.ResourceManager;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.support.dates.DateFormatter;
import it.clipcall.infrastructure.ui.dialogs.SnackbarService;
import it.clipcall.infrastructure.ui.fragments.DatePicker2Fragment;
import it.clipcall.infrastructure.validation.ValidationError;
import it.clipcall.professional.payment.models.AccountMetadata;
import it.clipcall.professional.payment.models.PaymentDebitCardDetails;
import it.clipcall.professional.payment.models.PaymentSettings;
import it.clipcall.professional.payment.models.PaymentSettingsPaymentDetails;
import it.clipcall.professional.payment.models.PaymentSettingsPersonalDetails;
import it.clipcall.professional.payment.presenters.ProfessionalPaymentSettingsPresenter;
import it.clipcall.professional.payment.views.IProfessionalPaymentSettingsView;


@EFragment(R.layout.professional_payment_settings_fragment)
public class ProfessionalPaymentSettingsFragment extends BaseFragment implements IProfessionalPaymentSettingsView, DatePickerDialog.OnDateSetListener {



    @Inject
    ProfessionalPaymentSettingsPresenter presenter;

    @Inject
    CharSequenceToIntConverter charSequenceToIntConverter;

    @Inject
    ResourceManager resourceManager;

    @Inject
    SnackbarService snackbarService;

    @ViewById
    ViewGroup consumerProfileContainer;

    @ViewById
    ViewGroup standaloneAccountContainer;

    @ViewById
    ViewGroup connectedAccountContainer;

    @ViewById
    ProgressBar loadingSpinnerProgressBar;

    @ViewById
    FloatingActionButton editFab;

    @ViewById
    Button inviteCustomersButton;

    @ViewById
    ViewGroup bonusCashContainerRoot;

    @ViewById
    Spinner accountTypeSpinner;

    @ViewById
    ViewGroup debitCardContainer;

    @ViewById
    ViewGroup expirationCvvContainer;;

    @ViewById
    ViewGroup bankAccountContainer;

    @ViewById
    ViewGroup taxIdContainer;

    @ViewById
    ViewGroup businessNameContainer;

    @ViewById
    ViewGroup saveButtonContainer;

    @ViewById
    ViewGroup paymentSettingsRootContainer;

    @ViewById
    ViewGroup standaloneInactiveContainer;

    @ViewById
    ViewGroup standaloneActiveContainer;

    @ViewById
    Spinner moneyTransferMethodSpinner;

    ArrayAdapter<CharSequence> moneyTransferMethodAdapter;

    ArrayAdapter<CharSequence> accountTypeAdapter;

    @ViewById
    EditText dateOfBirthEditText;

    @ViewById
    EditText businessAddressEditText;

    @ViewById
    EditText firstNameEditText;

    @ViewById
    EditText ssnEditText;

    @ViewById
    EditText lastNameEditText;

    @ViewById
    EditText taxIdEditText;

    @ViewById
    EditText accountNumberEditText;

    @ViewById
    EditText routingNumberEditText;


    @ViewById
    EditText businessNameEditText;

    @ViewById
    EditText cardNumberEditText;

    @ViewById
    ImageView creditCardImageView;

    @ViewById
    EditText cvvEditText;

    @ViewById
    EditText creditCardExpirationMonthEditText;

    @ViewById
    EditText creditCardExpirationYearEditText;

    @ViewById
    EditText accountHolderNameEditText;


    @ViewById
    ProgressBar saveSettingsProgressBar;

    @ViewById
    Button savePaymentSettingsButton;

    @ViewById
    com.kennyc.view.MultiStateView multiStateView;


    @AfterViews
    void consumerProfileViewLoaded(){
        initialize();
    }

    @Click(R.id.dateOfBirthEditText)
    void onDateOfBirthTapped(){
        DialogFragment newFragment = new DatePicker2Fragment();
        newFragment.show(getChildFragmentManager(), "datePicker");
    }


    void initialize(){
        presenter.bindView(this);


        accountTypeAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.stripe_account_types, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        accountTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        accountTypeSpinner.setAdapter(accountTypeAdapter);
        accountTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String accountType = (String) accountTypeAdapter.getItem(position);
                onAccountTypeChanged(accountType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        moneyTransferMethodAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.stripe_money_transfer_method, R.layout.money_transfer_method_spinner_layout);
// Specify the layout to use when the list of choices appears
        moneyTransferMethodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        moneyTransferMethodSpinner.setAdapter(moneyTransferMethodAdapter);

        moneyTransferMethodSpinner.setDropDownHorizontalOffset(0);

        moneyTransferMethodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String moneyTransferMethod = (String) moneyTransferMethodAdapter.getItem(position);
                onMoneyTransferMethodChanged(moneyTransferMethod);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        presenter.initialize();

    }

    private void onAccountTypeChanged(String accountType) {
        presenter.selectAccountType(accountType);


    }


    private void onMoneyTransferMethodChanged(String moneyTransferMethod) {
       presenter.setMoneyTransferMethod(moneyTransferMethod);
    }



    @Override
    protected String getTitle() {
        return "PAYMENT SETTINGS";
    }




    @Override
    public RoutingContext getRoutingContext() {
        return new RoutingContext(getActivity());
    }




    @Override
    protected int getMenuItemId() {
        return R.id.professionalPaymentSettingsMenuItem;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        LocalDate dateOfBirth = new LocalDate(year,monthOfYear+1, dayOfMonth);
        String formattedDate = DateFormatter.format(dateOfBirth);
        presenter.setDateOfBirth(dateOfBirth.toDate());
        dateOfBirthEditText.setText(formattedDate);
    }

    boolean isInSelectionMode = false;

    @Click(R.id.businessAddressEditText)
    void onBusinessAddressTapped(){
        if(isInSelectionMode)
            return;

        isInSelectionMode = true;

        selectBusinessAddress();
    }

    @Click(R.id.disconnectStripeButton)
    void onDisconnectFromStipeTapped(){
        presenter.disconnectFromStandaloneAccount();
    }

    private void selectBusinessAddress(){
        PickGoogleAddressCommand command = new PickGoogleAddressCommand(getActivity());
        command.execute(new EmptyCallBack<PlaceData>(){
            @Override
            public void onSuccess(PlaceData result) {
                isInSelectionMode = false;
                onBusinessAddressSelected(result);
            }

            @Override
            public void onFailure() {
                isInSelectionMode = false;
            }
        });
    }

    void onBusinessAddressSelected(PlaceData result) {
        presenter.setBusinessLocationAddress(result);
    }

    @Override
    public void showSelectedAddress(String address) {
        businessAddressEditText.setError(null);
        businessAddressEditText.setText(address);
    }

    @Override
    public void showPaymentSettings(PaymentSettings paymentSettings, AccountMetadata accountMetadata) {





        PaymentSettingsPersonalDetails personalDetails = paymentSettings.getPersonalDetails();
        PaymentSettingsPaymentDetails paymentDetails = paymentSettings.getPaymentDetails();
        Date dateOfBirth = paymentSettings.getPersonalDetails().getDateOfBirth();
        if(dateOfBirth != null){
            LocalDate localDate = LocalDate.fromDateFields(dateOfBirth);
            dateOfBirthEditText.setText(DateFormatter.format(localDate, "d MMM yyyy"));
        }
        businessAddressEditText.setText(personalDetails.getAddress().getFullAddress());
        firstNameEditText.setText(personalDetails.getFirstName());
        ssnEditText.setText(personalDetails.getSsnLastFourDigits());
        lastNameEditText.setText(personalDetails.getLastName());
        taxIdEditText.setText(personalDetails.getTaxId());
        accountNumberEditText.setText(paymentDetails.getBankAccount().getAccountNumber());
        routingNumberEditText.setText(paymentDetails.getBankAccount().getRoutingNumber());
        businessNameEditText.setText(personalDetails.getBusinessName());
        cardNumberEditText.setText(paymentDetails.getDebitCard().getCardNumber());
        cvvEditText.setText(paymentDetails.getDebitCard().getCvv());
        creditCardExpirationYearEditText.setText(paymentDetails.getDebitCard().getExpirationYear() > 0 ? paymentDetails.getDebitCard().getExpirationYear()+"" : "");
        creditCardExpirationMonthEditText.setText(paymentDetails.getDebitCard().getExpirationMonth() > 0 ? paymentDetails.getDebitCard().getExpirationMonth()+"" : "");
        accountHolderNameEditText.setText(paymentDetails.getBankAccount().getAccountHolderName());

        String accountType;
        if(personalDetails.isCompany()){
            accountType = accountTypeAdapter.getItem(0)+"";
        }else{
            accountType = accountTypeAdapter.getItem(1)+"";;
        }

        onAccountTypeChanged(accountType);

        String moneyTransferMethod;
        if(paymentDetails.isBankTransfer()){
            moneyTransferMethod = moneyTransferMethodAdapter.getItem(0)+"";
        }else{
            moneyTransferMethod = moneyTransferMethodAdapter.getItem(1)+"";
        }

        onMoneyTransferMethodChanged(moneyTransferMethod);


        if(!paymentSettings.isNew()){
            setReadOnlyMode(paymentSettings, accountMetadata);
        }else{
            setWriteMode(paymentSettings, accountMetadata);
        }

        if(!accountMetadata.isActive()){
            connectedAccountContainer.setVisibility(View.VISIBLE);
            standaloneAccountContainer.setVisibility(View.VISIBLE);
            standaloneInactiveContainer.setVisibility(View.VISIBLE);
            standaloneActiveContainer.setVisibility(View.GONE);

        }else{
            if(accountMetadata.isManaged()){
                connectedAccountContainer.setVisibility(View.VISIBLE);
                standaloneAccountContainer.setVisibility(View.GONE);
            }else{
                connectedAccountContainer.setVisibility(View.GONE);
                standaloneAccountContainer.setVisibility(View.VISIBLE);
                standaloneInactiveContainer.setVisibility(View.GONE);
                standaloneActiveContainer.setVisibility(View.VISIBLE);
                saveButtonContainer.setVisibility(View.GONE);
            }
        }
    }

    private void setWriteMode(PaymentSettings paymentSettings, AccountMetadata accountMetadata) {



        firstNameEditText.setEnabled(true);
        lastNameEditText.setEnabled(true);
        dateOfBirthEditText.setEnabled(true);
        dateOfBirthEditText.setClickable(true);
        dateOfBirthEditText.setTextColor(ContextCompat.getColor(getContext(), R.color.link_color));
        businessNameEditText.setEnabled(true);
        businessAddressEditText.setEnabled(true);
        businessAddressEditText.setClickable(true);
        businessAddressEditText.setTextColor(ContextCompat.getColor(getContext(), R.color.link_color));
        taxIdEditText.setEnabled(true);
        ssnEditText.setEnabled(true);

        //bank account
        PaymentSettingsPaymentDetails paymentDetails = paymentSettings.getPaymentDetails();
        if(paymentDetails.isBankTransfer()){
            debitCardContainer.setVisibility(View.GONE);
            bankAccountContainer.setVisibility(View.VISIBLE);
            accountHolderNameEditText.setVisibility(View.VISIBLE);
            routingNumberEditText.setVisibility(View.VISIBLE);
            accountNumberEditText.setVisibility(View.VISIBLE);
            accountNumberEditText.setEnabled(true);
            accountNumberEditText.setText("");
        }
        else{
            PaymentDebitCardDetails debitCard = paymentDetails.getDebitCard();
            bankAccountContainer.setVisibility(View.GONE);
            debitCardContainer.setVisibility(View.VISIBLE);
            creditCardExpirationMonthEditText.setVisibility(View.VISIBLE);
            creditCardExpirationYearEditText.setVisibility(View.VISIBLE);
            cvvEditText.setVisibility(View.VISIBLE);
            cardNumberEditText.setVisibility(View.VISIBLE);
            cardNumberEditText.setEnabled(true);
            cardNumberEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            cardNumberEditText.setText("");
            creditCardImageView.setVisibility(View.GONE);
            expirationCvvContainer.setVisibility(View.VISIBLE);

        }



        accountTypeSpinner.setEnabled(true);
        accountTypeSpinner.setClickable(true);
        moneyTransferMethodSpinner.setEnabled(true);
        moneyTransferMethodSpinner.setClickable(true);

        saveButtonContainer.setVisibility(View.VISIBLE);
        editFab.setVisibility(View.GONE);

        if(accountMetadata.isActive() && accountMetadata.isManaged()){
            connectedAccountContainer.setVisibility(View.VISIBLE);
            standaloneAccountContainer.setVisibility(View.GONE);
        }


    }

    private void setReadOnlyMode(PaymentSettings paymentSettings, AccountMetadata accountMetadata) {

        //personal details
        firstNameEditText.setEnabled(false);
        lastNameEditText.setEnabled(false);
        dateOfBirthEditText.setEnabled(false);
        dateOfBirthEditText.setClickable(false);
        dateOfBirthEditText.setTextColor(ContextCompat.getColor(getContext(), R.color.disabled_link_color));
        businessNameEditText.setEnabled(false);
        businessAddressEditText.setEnabled(false);
        businessAddressEditText.setClickable(false);
        businessAddressEditText.setTextColor(ContextCompat.getColor(getContext(), R.color.disabled_link_color));
        taxIdEditText.setEnabled(false);
        ssnEditText.setEnabled(false);

        //bank account
        PaymentSettingsPaymentDetails paymentDetails = paymentSettings.getPaymentDetails();
        if(paymentDetails.isBankTransfer()){
            debitCardContainer.setVisibility(View.GONE);
            bankAccountContainer.setVisibility(View.VISIBLE);
            accountHolderNameEditText.setVisibility(View.GONE);
            routingNumberEditText.setVisibility(View.GONE);
            accountNumberEditText.setVisibility(View.VISIBLE);
            accountNumberEditText.setEnabled(false);
            creditCardImageView.setVisibility(View.INVISIBLE);
            accountNumberEditText.setText("******"+paymentDetails.getBankAccount().getAccountNumber());

            creditCardImageView.setVisibility(View.GONE);
            expirationCvvContainer.setVisibility(View.GONE);
        }
        else{
            PaymentDebitCardDetails debitCard = paymentDetails.getDebitCard();
            CardResourceResolver resourceResolver = new CardResourceResolver();
            int resourceId = resourceResolver.apply(debitCard.getCardNumber());
            bankAccountContainer.setVisibility(View.GONE);
            debitCardContainer.setVisibility(View.VISIBLE);

            cardNumberEditText.setEnabled(false);
            cardNumberEditText.setText("******"+debitCard.getCardNumber());
            cardNumberEditText.setVisibility(View.VISIBLE);
            creditCardImageView.setVisibility(View.VISIBLE);
            creditCardImageView.setImageResource(resourceId);
            expirationCvvContainer.setVisibility(View.GONE);
        }

        accountTypeSpinner.setEnabled(false);
        accountTypeSpinner.setClickable(false);
        moneyTransferMethodSpinner.setEnabled(false);
        moneyTransferMethodSpinner.setClickable(false);

        saveButtonContainer.setVisibility(View.GONE);
        editFab.setVisibility(View.VISIBLE);

        if(accountMetadata.isActive() && accountMetadata.isManaged()){
            connectedAccountContainer.setVisibility(View.VISIBLE);
            standaloneAccountContainer.setVisibility(View.GONE);
        }

    }

    @Click(R.id.editFab)
    void onEditPaymentSettingsTapped(){
        presenter.editPaymentSettings();
    }



    @Override
    public void showValidationErrors(List<ValidationError> validationErrors) {
        boolean firstViewRecievedFocus = false;
        for(ValidationError validationError : validationErrors){
            View view = getActivity().findViewById(validationError.getViewId());
            if(!(view instanceof EditText))
                continue;
            String errorMessage = resourceManager.getString(validationError.getResourceId());
            EditText editText = (EditText)view;
            editText.setError(errorMessage);

            if(!firstViewRecievedFocus){
                editText.requestFocus();
                firstViewRecievedFocus = true;
            }
        }
    }

    @Override
    public void toggleCompanyDetails(boolean showCompany) {
        int visibility = showCompany ? View.VISIBLE : View.GONE;
        businessNameContainer.setVisibility(visibility);
        taxIdContainer.setVisibility(visibility);
        if(showCompany)
            moneyTransferMethodSpinner.setSelection(0);
        else
            moneyTransferMethodSpinner.setSelection(1);

    }

    @Override
    public void showBankTransfer(boolean showBankTransfer, PaymentSettings paymentSettings) {
        if(showBankTransfer){
            debitCardContainer.setVisibility(View.GONE);
            bankAccountContainer.setVisibility(View.VISIBLE);
            moneyTransferMethodSpinner.setSelection(0);
        }else{
            debitCardContainer.setVisibility(View.VISIBLE);
            bankAccountContainer.setVisibility(View.GONE);
            moneyTransferMethodSpinner.setSelection(1);
            if(paymentSettings.isNew()){
                expirationCvvContainer.setVisibility(View.VISIBLE);
            }else{
                expirationCvvContainer.setVisibility(View.GONE);
            }


        }
    }

    @Override
    public void showSavingSettingIsInProgress(boolean isInProgress) {
        if(isInProgress){
            saveSettingsProgressBar.setVisibility(View.VISIBLE);
            savePaymentSettingsButton.setVisibility(View.INVISIBLE);
        }else{
            saveSettingsProgressBar.setVisibility(View.GONE);
            savePaymentSettingsButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showSavedSettings(boolean saved) {
        int  messageResourceId = saved ? R.string.payment_settings_saved : R.string.payment_settings_failed_saving;
        String message = resourceManager.getString(messageResourceId);
        snackbarService.show(paymentSettingsRootContainer,message, Snackbar.LENGTH_LONG);
    }

    @Override
    public void showInitializingInProgress(boolean isInProgress) {
        if(isInProgress){
            multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        }
        else{
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        }



    }

    @Override
    public void showDisconnectingInProgress(boolean isInProgress) {
        if(isInProgress)
            multiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        else
            multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
    }

    @TextChange({R.id.firstNameEditText})
    void onFirstNameChanged(TextView tv, CharSequence text) {
       presenter.setFirstName(text+"");
    }

    @TextChange({R.id.lastNameEditText})
    void onLastNameChanged(TextView tv, CharSequence text) {
        presenter.setLastName(text+"");
    }

    @TextChange({R.id.ssnEditText})
    void onSocialSecurityNumberChanged(TextView tv, CharSequence text) {
        presenter.setSsnLastFourDigits(text+"");
    }

    @TextChange({R.id.taxIdEditText})
    void onTaxIdChanged(TextView tv, CharSequence text) {
        presenter.setTaxId(text+"");
    }

    @TextChange({R.id.accountNumberEditText})
    void onAccountNumberChanged(TextView tv, CharSequence text) {
        presenter.setAccountNumber(text+"");
    }

    @TextChange({R.id.routingNumberEditText})
    void onRoutingNumberChanged(TextView tv, CharSequence text) {
        presenter.setRoutingNumber(text+"");
    }

    @TextChange({R.id.businessNameEditText})
    void onBusinessNameChanged(TextView tv, CharSequence text) {
        presenter.setBusinessName(text+"");
    }

    @TextChange({R.id.cardNumberEditText})
    void onCardNumberChanged(TextView tv, CharSequence text) {
        presenter.setCardNumber(text+"");
    }

    @TextChange({R.id.cvvEditText})
    void onCvvChanged(TextView tv, CharSequence text) {
        presenter.setCvv(text+"");
    }


    @TextChange(R.id.accountHolderNameEditText)
    void onAccountHolderNameChanged(TextView tv, CharSequence text) {
        presenter.setAccountHolderName(text+"");
    }

    @TextChange(R.id.creditCardExpirationMonthEditText)
    public void onExpirationMonthChanged(TextView tv, CharSequence text){
        Integer value = charSequenceToIntConverter.convert(text);
        if(value == null)
            return;

        presenter.setExpirationMonth(value);
    }

    @TextChange(R.id.creditCardExpirationYearEditText)
    public void onExpirationYearChanged(TextView tv, CharSequence text){
        Integer value = charSequenceToIntConverter.convert(text);
        if(value == null)
            return;

        presenter.setExpirationYear(value);
    }

    @Click(R.id.existingAccountButton)
    void onAlreadyHaveStripeAccountTapped(){
        presenter.loginViaStripe();
    }

    @Click(R.id.savePaymentSettingsButton)
    void onSavePaymentSettingsTapped(){
        presenter.savePaymentSettings();
    }
}
