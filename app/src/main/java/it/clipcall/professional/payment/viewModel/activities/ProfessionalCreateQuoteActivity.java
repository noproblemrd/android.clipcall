package it.clipcall.professional.payment.viewModel.activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.common.base.Strings;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;
import org.joda.time.LocalDate;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.payment.domainModel.services.QuoteDateFormatter;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.commands.ICommand;
import it.clipcall.infrastructure.converters.UsCurrencyFormatter;
import it.clipcall.infrastructure.resources.ResourceManager;
import it.clipcall.infrastructure.ui.dialogs.SnackbarContext;
import it.clipcall.infrastructure.ui.dialogs.SnackbarService;
import it.clipcall.infrastructure.ui.fragments.DatePickerFragment;
import it.clipcall.professional.leads.models.Quote;
import it.clipcall.professional.payment.presenters.ProfessionalCreateQuotePresenter;
import it.clipcall.professional.payment.viewModel.views.ICreateQuoteView;

@EActivity(R.layout.professional_create_quote_activity)
public class ProfessionalCreateQuoteActivity extends BaseActivity implements ICreateQuoteView, IHasToolbar,  DialogInterface.OnCancelListener, DatePickerDialog.OnDateSetListener{

    @Inject
    ProfessionalCreateQuotePresenter presenter;

    @Inject
    ResourceManager resourceManager;

    @Inject
    QuoteDateFormatter quoteDateFormatter;

    @Inject
    UsCurrencyFormatter usCurrencyFormatter;

    @Inject
    SnackbarService snackbarService;

    @ViewById
    Toolbar toolbar;

    @ViewById
    EditText quotePriceEditText;


    @ViewById
    ViewGroup rootCoordinatorLayout;


    @ViewById
    EditText pickDateEditText;

    @ViewById
    EditText quoteEditText;

    @ViewById
    ProgressBar sendQuoteProgressBar;

    @ViewById
    Button sendQuoteButton;

    DatePickerFragment datePicker;


    @ViewById
    TextView reducedPriceTextView;

    @Override
    public String getViewTitle() {
        return "UPDATE QUOTE";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }




    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        presenter.initialize();
        datePicker = new DatePickerFragment();
    }

    @Click(R.id.pickDateEditText)
    void onPickDateTapped(){
        datePicker.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void showPriceRequired() {
        quotePriceEditText.setError("quote price is required");
    }

    @Override
    public void showStartDateRequired() {
        pickDateEditText.setError("you must select a start date");
    }

    @Override
    public void showStartDateMustBeGreaterThanOrEqualToToday() {
        String message =  resourceManager.getString(R.string.past_date_validation);
        pickDateEditText.setError(message);
        snackbarService.show(rootCoordinatorLayout,message, Snackbar.LENGTH_LONG);
    }


    @Override
    public void showQuoteDescriptionIsRequired() {
        quoteEditText.setError("Quote description is required");
    }

    @Override
    public void showQuoteSubmissionFaliure() {
        String message = resourceManager.getString(R.string.quote_sending_failed);
        Snackbar.make(rootCoordinatorLayout,message, Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //presenter.routeToHome();
            }
        }).show();
    }

    @Override
    public void sendQuoteInProgress(boolean isInProgress) {
        if(isInProgress){
            sendQuoteProgressBar.setVisibility(View.VISIBLE);
            sendQuoteButton.setVisibility(View.INVISIBLE);
        }else{
            sendQuoteProgressBar.setVisibility(View.INVISIBLE);
            sendQuoteButton.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showStartDate(Quote quote) {
        pickDateEditText.setText(quoteDateFormatter.format(quote));
    }

    @Override
    public void showQuote(Quote quote) {
        if(!Strings.isNullOrEmpty(quote.getDescription()))
            quoteEditText.setText(quote.getDescription());
    }

    @Override
    public void showQuoteMinValue() {
        quotePriceEditText.setError("price must be at least 50 cents");
    }


    @Override
    public void showMessage(String message, final ICommand command) {
        snackbarService.show(rootCoordinatorLayout,message,Snackbar.LENGTH_INDEFINITE, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Click(R.id.sendQuoteButton)
    void sendQuoteTapped(){
        presenter.sendQuote();
    }

    @TextChange(R.id.quoteEditText)
    void qouteTextChanged(CharSequence text, TextView hello, int before, int start, int count){
        if(text == null)
            return;

        presenter.setQuoteText(text + "");
    }


    @TextChange(R.id.quotePriceEditText)
    void quotePriceChanged(CharSequence text, TextView hello, int before, int start, int count){
        if(text == null)
            return;

        presenter.setQuotePrice(text + "");
    }



    @Override
    public void showReducedPrice(double reducedPrice, double feePercentage) {
        if(reducedPrice > 0){

            NumberFormat percentageFormat = new DecimalFormat("##");
            String formattedPercentage = percentageFormat.format(feePercentage);
            String reducedPriceText = usCurrencyFormatter.format(reducedPrice);
            String reducedPriceTextFormat = resourceManager.getString(R.string.payment_after_clipcall_fee);
            reducedPriceTextView.setVisibility(View.VISIBLE);
            reducedPriceTextView.setText(String.format(reducedPriceTextFormat,formattedPercentage, reducedPriceText));
        }else{
            reducedPriceTextView.setVisibility(View.INVISIBLE);
        }


    }

    @Override
    public void updateQuoteSent() {
        boolean enabled = false;
        quotePriceEditText.setEnabled(enabled);
        pickDateEditText.setEnabled(false);
        pickDateEditText.setClickable(false);
        quoteEditText.setEnabled(false);
    }

    @Override
    public void showMessageContainsNonPermittedText() {
        String message = getString(R.string.terms_of_service_violation);
        SnackbarContext context = new SnackbarContext(rootCoordinatorLayout,message,null, Snackbar.LENGTH_LONG,5);
        snackbarService.show(context);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        LocalDate startDate = new LocalDate(year,monthOfYear+1, dayOfMonth);
        presenter.setQuoteStartDateDate(startDate);
    }

    @Override
    public void onCancel(DialogInterface dialog) {

    }
}