package it.clipcall.professional.payment.services.tasks;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatParticipant;
import it.clipcall.common.chat.models.IConversation;
import it.clipcall.common.chat.models.MessageDescription;
import it.clipcall.common.chat.models.MessageType;
import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.common.chat.support.factories.ChatMessageFactoryProvider;
import it.clipcall.infrastructure.tasks.ITask3;
import it.clipcall.professional.leads.models.NewQuoteData;
import it.clipcall.professional.leads.models.ProfessionalProjectDetails;
import it.clipcall.professional.leads.models.Quote;
import it.clipcall.professional.leads.models.QuoteDetails;
import it.clipcall.professional.leads.services.ProfessionalProjectsManager;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.services.ProfessionalManager;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class SendQuoteTask implements ITask3<String , String, Quote, Boolean> {

    private final ProfessionalProjectsManager professionalProjectsManager;
    private final AuthenticationManager authenticationManager;
    private final ProfessionalManager professionalManager;
    private final ChatManager chatManager;

    @Inject
    public SendQuoteTask(ProfessionalProjectsManager professionalProjectsManager, AuthenticationManager authenticationManager, ProfessionalManager professionalManager, ChatManager chatManager){

        this.professionalProjectsManager = professionalProjectsManager;
        this.authenticationManager = authenticationManager;
        this.professionalManager = professionalManager;
        this.chatManager = chatManager;
    }

    @Override
    public Boolean execute(String leadId, String leadAccountId, Quote quote) {

        String userId = authenticationManager.getSupplierId();
        QuoteDetails createdQuote = professionalProjectsManager.createQuote(userId,leadAccountId,  quote);
        if(createdQuote ==  null)
            return Boolean.FALSE;


        NewQuoteData quoteData = new NewQuoteData();
        quoteData.quote = createdQuote.getQuote();

        quoteData.leadId = leadId;
        quoteData.leadAccountId = leadAccountId;

        ProfessionalProjectDetails projectDetails = professionalProjectsManager.getProjectDetails2(userId, leadAccountId);
        if(projectDetails == null)
            return Boolean.FALSE;


        //TODO test if these two lines are required
        //newProject.setCustomerName(projectDetails.getCustomerName());
        //newProject.setCustomerPhoneNum(projectDetails.getPhoneNumber());

        String customerId = projectDetails.getCustomerId();
        String customerNameOrPhoneNumber = projectDetails.getCustomerDisplayName();
        String customerProfileImageUrl = projectDetails.getCustomerProfileImage();
        ChatParticipant receiver = new ChatParticipant(customerId, customerProfileImageUrl, customerNameOrPhoneNumber, false);


        ProfessionalProfile professionalProfile = professionalManager.getProfessionalProfile(userId);
        String professionalName = professionalProfile.getBusinessName();
        String professionalProfileImageUrl = professionalProfile.getProfileImageUrl();
        String proCustomerId = authenticationManager.getCustomerId();
        ChatParticipant sender = new ChatParticipant(proCustomerId, professionalProfileImageUrl, professionalName, true);

        IConversation conversationWithParticipant = chatManager.getConversationWithParticipant(sender, receiver, leadAccountId);
        if(conversationWithParticipant == null){
            conversationWithParticipant = chatManager.createConversation(sender,receiver, leadAccountId);
        }

        if(conversationWithParticipant == null){

            return Boolean.FALSE;
        }

        MessageDescription messageDescription = ChatMessageFactoryProvider.getInstance().createMessageDescription(MessageType.SendQuote, quoteData);
        chatManager.sendMessage(sender,receiver,leadAccountId, messageDescription);


        return Boolean.TRUE;
    }
}
