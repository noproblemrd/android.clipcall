package it.clipcall.professional.payment.activities;

import android.support.v7.widget.Toolbar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.presenters.DefaultRoutingPresenter;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.payment.fragments.ProfessionalPaymentSettingsFragment;

@EActivity(R.layout.professional_payment_settings_activity)
public class ProfessionalPaymentSettingsActivity extends BaseActivity implements IView, IHasToolbar {

    @Inject
    DefaultRoutingPresenter presenter;

    @ViewById
    Toolbar toolbar;

    @FragmentById
    ProfessionalPaymentSettingsFragment paymentSettingsFragment;

    @AfterViews
    void  afterViews(){
        presenter.bindView(this);
        presenter.initialize();
    }

    @Override
    public String getViewTitle() {
        return "PAYMENT SETTINGS";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        presenter.routeToHome();
    }
}
