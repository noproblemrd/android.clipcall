package it.clipcall.professional.payment.models;

import com.stripe.android.model.Token;

/**
 * Created by dorona on 24/07/2016.
 */

public class PaymentSettingsPaymentDetails {

    PaymentBankAccountDetails bankAccount;
    PaymentDebitCardDetails debitCard;
    boolean isBankTransfer;

    public PaymentSettingsPaymentDetails(){
        isBankTransfer = true;
        bankAccount = new PaymentBankAccountDetails();
        debitCard = new PaymentDebitCardDetails();
    }

    public PaymentBankAccountDetails getBankAccount() {
        return bankAccount;
    }

    public void setBankAccountDetails(PaymentBankAccountDetails bankAccountDetails) {
        this.bankAccount = bankAccountDetails;
    }

    public PaymentDebitCardDetails getDebitCard() {
        return debitCard;
    }

    public void setDebitCardDetails(PaymentDebitCardDetails debitCard) {
        this.debitCard = debitCard;
    }

    public void setAccountNumber(String accountNumber) {
        bankAccount.setAccountNumber(accountNumber);
    }

    public void setRoutingNumber(String routingNumber) {
        bankAccount.setRoutingNumber(routingNumber);
    }

    public void setCardNumber(String cardNumber) {
        debitCard.setCardNumber(cardNumber);
    }

    public void setCvv(String cvv) {
        this.debitCard.setCvv(cvv);
    }
    public void setExpirationMonth(int expirationMonth) {
        this.debitCard.setExpirationMonth(expirationMonth);
    }

    public void setExpirationYear(int expirationYear) {
        this.debitCard.setExpirationYear(expirationYear);
    }

    public void setBankTransfer(boolean bankTransfer) {
        this.isBankTransfer = bankTransfer;
    }

    public boolean isBankTransfer() {
        return isBankTransfer;
    }

    public void setAccountHolderName(String accountHolderName) {
        bankAccount.setAccountHolderName(accountHolderName);
    }

    public void setToken(Token token) {
        debitCard.setToken(token);
    }

    public void setReadOnlyMode() {
        if(isBankTransfer()){
            String accountNumber = bankAccount.getAccountNumber();
            if(accountNumber.length() > 4){
                String substring = accountNumber.substring(accountNumber.length() - 4);
                bankAccount.setAccountNumber(substring);
            }

        }else{
            String cardNumber = debitCard.getCardNumber();
            if(cardNumber.length() > 4) {
                String substring = cardNumber.substring(cardNumber.length() - 4);
                debitCard.setCardNumber(substring);
            }
        }

    }
}
