package it.clipcall.professional.payment.models;


import org.parceler.Parcel;

import java.io.Serializable;
import java.util.List;

import it.clipcall.infrastructure.support.collections.Lists;

@Parcel
public class NewPaymentCollection implements Serializable {


    List<PaymentReceivedMessage> payments;

    public List<PaymentReceivedMessage> getPayments() {
        return payments;
    }

    public void setProjects(List<PaymentReceivedMessage> payments) {
        this.payments = payments;
    }

    public void addPayment(PaymentReceivedMessage project){
        this.payments.add(project);
    }

    public PaymentReceivedMessage removeFirst(){
        if(Lists.isNullOrEmpty(payments))
            return null;

        return payments.remove(0);
    }
}
