package it.clipcall.professional.payment.services.validators;

import org.joda.time.LocalDate;

import it.clipcall.infrastructure.support.strings.Strings2;
import it.clipcall.infrastructure.validation.ValidationCriteriaBase;
import it.clipcall.professional.leads.models.Quote;

/**
 * Created by dorona on 30/06/2016.
 */
public class QuoteValidationCriteria extends ValidationCriteriaBase<Quote> {
    @Override
    public boolean isSatisfiedBy(Quote candidate) {
        if(candidate.getPrice() <= 0){
            addValidationError("Price must be greater than zero");
            return false;
        }

        if(candidate.getStartJobAt() == null)
        {
            addValidationError("Start date is required");
            return false;
        }
        LocalDate localDateNow = LocalDate.now();
        LocalDate localDate = LocalDate.fromDateFields(candidate.getStartJobAt());
        if(localDate.isBefore(localDateNow)){
            addValidationError("Start date must be equal or greater than today");
            return false;
        }

        if(Strings2.isNullOrWhiteSpace(candidate.getDescription()))
        {
            addValidationError("quote description is required");
            return false;
        }

        return true;
    }
}
