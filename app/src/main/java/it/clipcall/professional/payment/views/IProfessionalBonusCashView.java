package it.clipcall.professional.payment.views;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by micro on 1/20/2016.
 */
public interface IProfessionalBonusCashView extends IView {


    void showInvitingCustomerInProgress(boolean inProgress);

    void showErrorGeneratingInvitation();
}
