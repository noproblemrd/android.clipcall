package it.clipcall.professional.payment.models;

import org.parceler.Parcel;

import it.clipcall.common.models.PushMessage;

@Parcel
public class PaymentReceivedMessage extends PushMessage {

    String leadAccountId;
    String customerName;
    String customerProfileImage;
    String customerPhone;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerProfileImage() {
        return customerProfileImage;
    }

    public void setCustomerProfileImage(String customerProfileImage) {
        this.customerProfileImage = customerProfileImage;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getLeadAccountId() {
        return leadAccountId;
    }

    public void setLeadAccountId(String leadAccountId) {
        this.leadAccountId = leadAccountId;
    }
}
