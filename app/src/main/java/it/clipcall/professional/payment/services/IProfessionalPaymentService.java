package it.clipcall.professional.payment.services;

import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.professional.payment.models.AccountMetadata;
import it.clipcall.professional.payment.models.PaymentSettings;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by dorona on 18/02/2016.
 */
public interface IProfessionalPaymentService {

    @POST("suppliers/{supplierId}/payments/settings")
    Call<StatusResponse> savePaymentSettings(@Path("supplierId") String supplierId, @Body PaymentSettings paymentSettings);

    @GET("suppliers/{supplierId}/payments/settings")
    Call<PaymentSettings> getPaymentSettings(@Path("supplierId") String supplierId);

    @GET("suppliers/{supplierId}/payments/account")
    Call<AccountMetadata> getAccountMetadata(@Path("supplierId") String supplierId);

    @POST("suppliers/{supplierId}/payments/standaloneaccount/disconnect")
    Call<StatusResponse> disconnectFromStandaloneAccount(@Path("supplierId") String supplierId);

}
