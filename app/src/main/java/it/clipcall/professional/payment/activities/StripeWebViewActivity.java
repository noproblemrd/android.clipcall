package it.clipcall.professional.payment.activities;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.webview.ClipCallJavaScriptInterface;
import it.clipcall.consumer.about.presenters.StripeWebViewPresenter;
import it.clipcall.consumer.about.views.IStripeWebView;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.ui.dialogs.SnackbarService;

@EActivity(R.layout.webview_layout)
public class StripeWebViewActivity extends BaseActivity implements IHasToolbar, IStripeWebView {

    @ViewById
    WebView webView;

    @ViewById
    ViewGroup rootContainer;

    @Inject
    SnackbarService snackbarService;

    @ViewById
    Toolbar toolbar;

    @ViewById
    ProgressBar loadingSpinnerProgressBar;


    @Inject
    StripeWebViewPresenter presenter;

    @AfterViews
    void afterViews(){
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        ClipCallJavaScriptInterface jsInterface = new ClipCallJavaScriptInterface(presenter);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(jsInterface, "clipcall");

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                loadingSpinnerProgressBar.setProgress(progress);
                if (progress == 100) {
                    loadingSpinnerProgressBar.setVisibility(View.GONE);

                } else {
                    loadingSpinnerProgressBar.setVisibility(View.VISIBLE);

                }
            }
        });

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        presenter.bindView(this);
        presenter.initialize();

        toolbar.setTitle("STRIPE ACCOUNT");
    }

    @Override
    public String getViewTitle() {
        return "STRIPE ACCOUNT";
    }


    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if(webView.canGoBack())
        {
            webView.goBack();
            return;
        }

        super.onBackPressed();
    }


    @Override
    public void showStripeLogin(String url) {
        webView.loadUrl(url);
    }

    @Override
    public void showMessage(String message) {
        snackbarService.show(rootContainer,message, Snackbar.LENGTH_LONG);
    }

    @Override
    public void showMessageAndRedirectToHome(String message) {
        snackbarService.show(rootContainer, message, Snackbar.LENGTH_INDEFINITE, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.routeToHome();
            }
        });
    }
}
