package it.clipcall.professional.payment.views;

import java.util.List;

import it.clipcall.infrastructure.validation.ValidationError;
import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.payment.models.AccountMetadata;
import it.clipcall.professional.payment.models.PaymentSettings;

/**
 * Created by micro on 1/20/2016.
 */
public interface IProfessionalPaymentSettingsView extends IView {


    void showSelectedAddress(String address);

    void showPaymentSettings(PaymentSettings paymentSettings, AccountMetadata accountMetadata);

    void showValidationErrors(List<ValidationError> validationErrors);

    void toggleCompanyDetails(boolean showCompany);

    void showBankTransfer(boolean showBankTransfer, PaymentSettings paymentSettings);

    void showSavingSettingIsInProgress(boolean isInProgress);

    void showSavedSettings(boolean saved);

    void showInitializingInProgress(boolean isInProgress);

    void showDisconnectingInProgress(boolean isInProgress);
}
