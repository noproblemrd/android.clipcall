package it.clipcall.professional.payment.viewModel.views;

import it.clipcall.infrastructure.commands.ICommand;
import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.leads.models.Quote;

/**
 * Created by dorona on 16/03/2016.
 */
public interface ICreateQuoteView extends IView {

    void showMessage(String message, final ICommand command);

    void showPriceRequired();

    void showStartDateRequired();

    void showStartDateMustBeGreaterThanOrEqualToToday();

    void showQuoteDescriptionIsRequired();

    void showQuoteSubmissionFaliure();

    void sendQuoteInProgress(boolean isInProgress);

    void showStartDate(Quote quote);

    void showQuote(Quote quote);

    void showQuoteMinValue();

    void showReducedPrice(double reducedPrice, double feePercentage);

    void updateQuoteSent();

    void showMessageContainsNonPermittedText();
}
