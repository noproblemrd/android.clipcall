package it.clipcall.professional.payment.fragments;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.professional.payment.presenters.ProfessionalBonusCashPresenter;
import it.clipcall.professional.payment.views.IProfessionalBonusCashView;


@EFragment(R.layout.professional_bonus_cash_fragment)
public class ProfessionalBonusCashFragment extends BaseFragment implements IProfessionalBonusCashView {

    @Inject
    ProfessionalBonusCashPresenter presenter;

    @ViewById
    ViewGroup consumerProfileContainer;

    @ViewById
    ProgressBar loadingSpinnerProgressBar;

    @ViewById
    Button inviteCustomersButton;

    @ViewById
    ViewGroup bonusCashContainerRoot;


    @AfterViews
    void consumerProfileViewLoaded(){
        initialize();
    }

    @Background
    void initialize(){
        presenter.bindView(this);
        presenter.initialize();
    }


    @Override
    protected String getTitle() {
        return "$50 Bonus Cash";
    }

    @Click( {R.id.inviteCustomersButton})
    void inviteCustomersTapped(){
        inviteCustomer();
    }

    @Background
    void inviteCustomer(){
        presenter.inviteCustomer();
    }

    @Override
    public RoutingContext getRoutingContext() {
        return new RoutingContext(getActivity());
    }


    @UiThread
    @Override
    public void showInvitingCustomerInProgress(boolean inProgress) {
        if(inProgress){
            loadingSpinnerProgressBar.setVisibility(View.VISIBLE);
            inviteCustomersButton.setVisibility(View.INVISIBLE);
        }else{
            loadingSpinnerProgressBar.setVisibility(View.GONE);
            inviteCustomersButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showErrorGeneratingInvitation() {
        Snackbar.make(bonusCashContainerRoot,"Oops. Failed generating invitation. Please try again later.",Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected int getMenuItemId() {
        return R.id.professionalBonusCashMenuItem;
    }
}
