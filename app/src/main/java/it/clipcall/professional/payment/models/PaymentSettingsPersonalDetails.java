package it.clipcall.professional.payment.models;

import java.util.Date;

/**
 * Created by dorona on 24/07/2016.
 */

public class PaymentSettingsPersonalDetails {

    String firstName;
    String lastName;
    Date dateOfBirth;
    String businessName;
    PaymentAddress address;
    String taxId;
    String ssnLastFourDigits;
    boolean isCompany;

    public PaymentSettingsPersonalDetails(){
        firstName = "";
        lastName = "";
        dateOfBirth = null;
        businessName = "";
        address = new PaymentAddress();
        taxId = "";
        ssnLastFourDigits = "";
        isCompany = true;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public PaymentAddress getAddress() {
        return address;
    }

    public void setAddress(PaymentAddress address) {
        this.address = address;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getSsnLastFourDigits() {
        return ssnLastFourDigits;
    }

    public void setSsnLastFourDigits(String sslLastFourDigits) {
        this.ssnLastFourDigits = sslLastFourDigits;
    }

    public boolean isCompany() {
        return isCompany;
    }

    public void setCompany(boolean company) {
        isCompany = company;
    }
}
