package it.clipcall.professional.payment.services.validators;

import it.clipcall.R;
import it.clipcall.infrastructure.support.strings.Strings2;
import it.clipcall.infrastructure.validation.ValidationCriteria2Base;
import it.clipcall.infrastructure.validation.ValidationError;
import it.clipcall.professional.payment.models.PaymentBankAccountDetails;
import it.clipcall.professional.payment.models.PaymentDebitCardDetails;
import it.clipcall.professional.payment.models.PaymentSettings;
import it.clipcall.professional.payment.models.PaymentSettingsPaymentDetails;
import it.clipcall.professional.payment.models.PaymentSettingsPersonalDetails;

/**
 * Created by dorona on 30/06/2016.
 */
public class ProfessionalPaymentSettingsValidationCriteria extends ValidationCriteria2Base<PaymentSettings> {
    @Override
    public boolean isSatisfiedBy(PaymentSettings candidate) {


        PaymentSettingsPersonalDetails personalDetails = candidate.getPersonalDetails();
        if(Strings2.isNullOrWhiteSpace(personalDetails.getFirstName())){
            addValidationError(new ValidationError(R.id.firstNameEditText,R.string.first_name_is_required));
        }
        if(Strings2.isNullOrWhiteSpace(personalDetails.getLastName())){
            addValidationError(new ValidationError(R.id.lastNameEditText,R.string.last_name_is_required));
        }
        if(personalDetails.getDateOfBirth() == null){
            addValidationError(new ValidationError(R.id.dateOfBirthEditText,R.string.date_of_birth_is_required));
        }

        if(personalDetails.isCompany() && Strings2.isNullOrWhiteSpace(personalDetails.getBusinessName())){
            addValidationError(new ValidationError(R.id.businessNameEditText,R.string.business_name_is_required));
        }
        if(personalDetails.getAddress() == null || Strings2.isNullOrWhiteSpace(personalDetails.getAddress().getLine1())){
            addValidationError(new ValidationError(R.id.businessAddressEditText,R.string.business_address_is_required));
        }
        if(personalDetails.isCompany() && Strings2.isNullOrWhiteSpace(personalDetails.getTaxId())){
            addValidationError(new ValidationError(R.id.taxIdEditText,R.string.tax_id_is_required));
        }

        if(personalDetails.isCompany() && !Strings2.isNullOrWhiteSpace(personalDetails.getTaxId()) && personalDetails.getTaxId().length() != 9){
            addValidationError(new ValidationError(R.id.taxIdEditText,R.string.tax_id_invalid_length));
        }
        if(Strings2.isNullOrWhiteSpace(personalDetails.getSsnLastFourDigits())){
            addValidationError(new ValidationError(R.id.ssnEditText,R.string.ssn_is_required));
        }
        if(!Strings2.isNullOrWhiteSpace(personalDetails.getSsnLastFourDigits()) && personalDetails.getSsnLastFourDigits().length() != 4){
            addValidationError(new ValidationError(R.id.ssnEditText,R.string.ssn_invalid_length));
        }
        PaymentSettingsPaymentDetails paymentDetails = candidate.getPaymentDetails();
        if(paymentDetails.isBankTransfer()){
            PaymentBankAccountDetails bankAccountDetails = paymentDetails.getBankAccount();
            if(Strings2.isNullOrWhiteSpace(bankAccountDetails.getAccountNumber())){
                addValidationError(new ValidationError(R.id.accountNumberEditText,R.string.account_number_is_required));
            }

            if(!Strings2.isNullOrWhiteSpace(bankAccountDetails.getAccountNumber()) && bankAccountDetails.getAccountNumber().length() < 4){
                addValidationError(new ValidationError(R.id.accountNumberEditText,R.string.account_number_invalid_length));
            }

            if(Strings2.isNullOrWhiteSpace(bankAccountDetails.getRoutingNumber())){
                addValidationError(new ValidationError(R.id.routingNumberEditText,R.string.routing_number_is_required));
            }

            if(!Strings2.isNullOrWhiteSpace(bankAccountDetails.getRoutingNumber()) && bankAccountDetails.getRoutingNumber().length() != 9){
                addValidationError(new ValidationError(R.id.routingNumberEditText,R.string.routing_number_invalid_length));
            }

            if(Strings2.isNullOrWhiteSpace(bankAccountDetails.getAccountHolderName())){
                addValidationError(new ValidationError(R.id.accountHolderNameEditText,R.string.account_holder_name_is_required));
            }
        }else{
            PaymentDebitCardDetails debitCardDetails = paymentDetails.getDebitCard();
            if(Strings2.isNullOrWhiteSpace(debitCardDetails.getCardNumber())){
                addValidationError(new ValidationError(R.id.cardNumberEditText,R.string.credit_card_number_is_required));
            }
        }
        return getValidationErrors().size() == 0;
    }
}
