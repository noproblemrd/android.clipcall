package it.clipcall.professional.payment.presenters;

import android.location.Address;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import it.clipcall.common.environment.Environment;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.maps.entities.PlaceData;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.AddressResolver;
import it.clipcall.professional.payment.controllers.ProfessionalPaymentSettingsController;
import it.clipcall.professional.payment.models.AccountMetadata;
import it.clipcall.professional.payment.models.PaymentAddress;
import it.clipcall.professional.payment.models.PaymentDebitCardDetails;
import it.clipcall.professional.payment.models.PaymentSettings;
import it.clipcall.professional.payment.models.PaymentSettingsPaymentDetails;
import it.clipcall.professional.payment.models.PaymentSettingsPersonalDetails;
import it.clipcall.professional.payment.services.validators.ProfessionalPaymentSettingsValidationCriteria;
import it.clipcall.professional.payment.views.IProfessionalPaymentSettingsView;

@PerActivity
public class ProfessionalPaymentSettingsPresenter extends PresenterBase<IProfessionalPaymentSettingsView> {


    private final RoutingService routingService;
    private final ProfessionalPaymentSettingsController controller;
    private final AddressResolver addressResolver;
    private final RouteParams routeParams;

    PaymentSettings paymentSettings;
    AccountMetadata accountMetadata;


    @Inject
    public ProfessionalPaymentSettingsPresenter(RoutingService routingService, ProfessionalPaymentSettingsController controller, AddressResolver addressResolver, RouteParams routeParams) {
        this.routingService = routingService;
        this.controller = controller;
        this.addressResolver = addressResolver;
        this.routeParams = routeParams;
    }

    @Override
    public void initialize() {
        view.showInitializingInProgress(true);
        paymentSettings = controller.getPaymentDetails();
        accountMetadata = controller.getPaymentAccountMetadata();
        view.showPaymentSettings(paymentSettings, accountMetadata);
        view.showInitializingInProgress(false);

    }


    public void setBusinessLocationAddress(PlaceData place) {
        if(paymentSettings == null)
            return;

        Map<String, String> property = new HashMap<>();
        property.put("address", place.address);
        //EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationEnterBusinessAddress, property));
        Address address = addressResolver.resolveAddress(place.latLng);
        if (address == null)
            return;
        paymentSettings.setAddress(address, place.address);
        view.showSelectedAddress(place.address);

    }

    public void setFirstName(String firstName) {
        if(paymentSettings == null)
            return;
        paymentSettings.setFirstName(firstName);
    }

    public void setLastName(String lastName) {
        if(paymentSettings == null)
            return;
        paymentSettings.setLastName(lastName);
    }

    public void setSsnLastFourDigits(String ssnLastFourDigits) {
        if(paymentSettings == null)
            return;
        this.paymentSettings.setSslLastFourDigits(ssnLastFourDigits);
    }

    public void setTaxId(String taxId) {
        if(paymentSettings == null)
            return;
        paymentSettings.setTaxId(taxId);
    }

    public void setAccountNumber(String accountNumber)
    {
        if(paymentSettings == null)
            return;
        paymentSettings.setAccountNumber(accountNumber);
    }

    public void setRoutingNumber(String routingNumber) {
        if(paymentSettings == null)
            return;
        paymentSettings.setRoutingNumber(routingNumber);
    }

    public void setBusinessName(String businessName) {
        if(paymentSettings == null)
            return;
        paymentSettings.setBusinessName(businessName);
    }

    public void setCardNumber(String cardNumber) {
        if(paymentSettings == null)
            return;
        paymentSettings.setCardNumber(cardNumber);
    }

    public void setExpirationYear(int expirationYear) {
        if(paymentSettings == null)
            return;
        paymentSettings.setExpirationYear(expirationYear);
    }

    public void setExpirationMonth(int expirationMonth) {
        if(paymentSettings == null)
            return;
        paymentSettings.setExpirationMonth(expirationMonth);
    }

    public void setCvv(String cvv) {
        if(paymentSettings == null)
            return;
        paymentSettings.setCvv(cvv);
    }

    public void loginViaStripe() {
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        routeParams.setParam("professionalId", controller.getProfessionalId());
        routingService.routeTo(Routes.StripeLoginRoute,navigationContext);
    }

    public void setDateOfBirth(Date dateOfBirth) {
        paymentSettings.setDateOfBirth(dateOfBirth);
    }

    @RunOnUiThread
    public void savePaymentSettings(){

        view.showSavingSettingIsInProgress(true);


        ProfessionalPaymentSettingsValidationCriteria criteria = new ProfessionalPaymentSettingsValidationCriteria();
        if(!criteria.isSatisfiedBy(paymentSettings)){
            view.showSavingSettingIsInProgress(false);
            view.showValidationErrors(criteria.getValidationErrors());
            return;
        }


        if(paymentSettings.getPaymentDetails().isBankTransfer()){
           savePaymentSettingsCore();
            return;
        }
        PaymentSettingsPersonalDetails personalDetails = paymentSettings.getPersonalDetails();
        PaymentAddress address = personalDetails.getAddress();

        PaymentSettingsPaymentDetails paymentDetails = paymentSettings.getPaymentDetails();
        PaymentDebitCardDetails debitCard = paymentDetails.getDebitCard();
        Card card = new Card.Builder(debitCard.getCardNumber(),debitCard.getExpirationMonth(),debitCard.getExpirationYear(),debitCard.getCvv())
                .currency("usd")
                .addressCity(address.getCity())
                .addressCountry(address.getState())
                .addressZip(address.getPostalCode())
                .addressLine1(address.getLine1())
                .addressLine2(address.getLine2())
                .addressState(address.getState())
                .build();
        Stripe stripe = null;
        try {
            stripe = new Stripe(Environment.StripPublishedKey);
            stripe.createToken(
                    card,
                    new TokenCallback() {
                        public void onSuccess(Token token) {
                            paymentSettings.setToken(token);
                            savePaymentSettingsCore();
                        }

                        public void onError(Exception error) {

                        }
                    });
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }



    }

    public void disconnectFromStandaloneAccount(){
        view.showDisconnectingInProgress(true);
        boolean disconnected = controller.disconnectFromStandaloneAccount();
        if(disconnected){
            paymentSettings.setIsNew(true);
            accountMetadata = controller.getPaymentAccountMetadata();
        }
        view.showDisconnectingInProgress(false);
        view.showPaymentSettings(paymentSettings, accountMetadata);
    }

    public void savePaymentSettingsCore(){
        boolean saved = controller.savePaymentSettings(paymentSettings);
        if(saved){
            paymentSettings.setIsNew(false);
            accountMetadata = controller.getPaymentAccountMetadata();
        }
        view.showSavingSettingIsInProgress(false);
        view.showPaymentSettings(paymentSettings, accountMetadata);
        view.showSavedSettings(saved);
    }

    public void selectAccountType(String accountType) {

        if(paymentSettings == null)
            return;

        if("COMPANY".equals(accountType.toUpperCase())){
            paymentSettings.setIsCompany(true);
            view.toggleCompanyDetails(true);


        }else if("INDIVIDUAL".equals(accountType.toUpperCase())){
            paymentSettings.setIsCompany(false);
            view.toggleCompanyDetails(false);
        }
    }

    public void setMoneyTransferMethod(String moneyTransferMethod) {
        if(paymentSettings == null)
            return;

        if("BANK TRANSFER".equals(moneyTransferMethod.toUpperCase())){
            paymentSettings.setIsBankTransfer(true);
            view.showBankTransfer(true, paymentSettings);

        }else if("DEBIT CARD".equals(moneyTransferMethod.toUpperCase())){
            paymentSettings.setIsBankTransfer(false);
            view.showBankTransfer(false, paymentSettings);
        }
    }

    public void setAccountHolderName(String accountHolderName) {
        paymentSettings.setAccountHolderName(accountHolderName);
    }

    public void editPaymentSettings() {
        paymentSettings.setIsNew(true);
        view.showPaymentSettings(paymentSettings, accountMetadata);
    }
}


