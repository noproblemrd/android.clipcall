package it.clipcall.professional.payment.services;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.google.common.base.Strings;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.ClipCallApplication;
import it.clipcall.common.activities.SplashActivity_;
import it.clipcall.infrastructure.messageHandlers.IMessageHandler;
import it.clipcall.infrastructure.notifications.NotificationContext;
import it.clipcall.infrastructure.notifications.SimpleNotifier;
import it.clipcall.infrastructure.repositories.Repository2;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.professional.payment.models.AccountMetadata;
import it.clipcall.professional.payment.models.NewPaymentCollection;
import it.clipcall.professional.payment.models.PaymentReceivedMessage;
import it.clipcall.professional.validation.services.AuthenticationManager;


@Singleton
public class PaymentReceivedMessageHandler implements IMessageHandler {


    private final RoutingService routingService;
    private final AuthenticationManager authenticationManager;

    private ProfessionalPaymentManager professionalPaymentManager;
    private final SimpleNotifier notifier;
    private final Repository2 repository;
    private int NotificaitonId = 30;

    @Inject
    public PaymentReceivedMessageHandler(RoutingService routingService, AuthenticationManager authenticationManager, ProfessionalPaymentManager professionalPaymentManager, SimpleNotifier notifier, Repository2 repository){

        this.routingService = routingService;
        this.authenticationManager = authenticationManager;
        this.professionalPaymentManager = professionalPaymentManager;
        this.notifier = notifier;
        this.repository = repository;
    }


    @Override
    public boolean canHandlerMessage(String message) {
        boolean canHandle =  "PAYMENT_ACCEPTED_BY_CUSTOMER".equals(message.toUpperCase());
        return canHandle;
    }

    @Override
    public void handleForegroundMessage(String from, Bundle data, final Activity currentActivity) {
        final PaymentReceivedMessage message = getMessage(data);
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {

                final ClipCallApplication app = (ClipCallApplication) currentActivity.getApplication();
                final Runnable action = new Runnable() {
                    @Override
                    public void run() {
                        authenticationManager.switchToProfessionalMode();
                        String supplierId = authenticationManager.getSupplierId();
                        AccountMetadata accountMetadata = professionalPaymentManager.getPaymentAccountMetadata(supplierId);
                        if(accountMetadata != null && accountMetadata.isActive())
                            routingService.routeToHome(new RoutingContext(currentActivity));
                        else{
                            NavigationContext navigationContext = new NavigationContext(new RoutingContext(currentActivity));
                            routingService.routeTo(Routes.ProfessionalFullPaymentSettingsRoute, navigationContext);
                        }
                    }
                };

                app.showMessage(message.getMessage(),action, false);
            }
        });

    }

    private PaymentReceivedMessage getMessage(Bundle data) {
        String customerPhone = data.getString("customerPhone");
        String profileImage = data.getString("customerProfileImage");
        String customerName = data.getString("customerName");
        String messageText = data.getString("message");
        String title = data.getString("title");
        String leadAccountId = data.getString("leadAccountId");
        PaymentReceivedMessage message = new PaymentReceivedMessage();
        message.setCustomerName(customerName);
        message.setCustomerPhone(customerPhone);
        message.setCustomerProfileImage(profileImage);
        message.setTitle(title);
        message.setLeadAccountId(leadAccountId);
        message.setMessage(messageText);

        return message;
    }

    @Override
    public void handleBackgroundMessage(String from, Bundle data, Application application) {

        if(!authenticationManager.isInProfessionalMode()){
            authenticationManager.switchToProfessionalMode();
        }

        PaymentReceivedMessage message = getMessage(data);

        NewPaymentCollection newPayments = repository.getSingle(NewPaymentCollection.class);
        if(newPayments == null){
            newPayments = new NewPaymentCollection();
            newPayments.setProjects(new ArrayList<PaymentReceivedMessage>());
        }

        newPayments.addPayment(message);
        repository.update(newPayments);

        final Intent resultIntent = new Intent(application.getApplicationContext(), SplashActivity_.class);


        final NotificationContext notificationContext = new NotificationContext();
        notificationContext.message = message.getMessage();
        notificationContext.smallPictureImageUrl = Strings.isNullOrEmpty(message.getCustomerProfileImage()) ? "https://storage.googleapis.com/clipcallmobile/consumer_profile_logo_placeholder3.png" : message.getCustomerProfileImage();
        notificationContext.bigPictureImageUrl = Strings.isNullOrEmpty(message.getCustomerProfileImage()) ? "https://storage.googleapis.com/clipcallmobile/consumer_profile_logo_placeholder3.png" : message.getCustomerProfileImage();
        notificationContext.title = message.getTitle();
        notificationContext.contentTitle = message.getTitle();
        notificationContext.bigContentTitle = message.getTitle();
        notificationContext.intent = resultIntent;
        notificationContext.summaryText = message.getMessage();
        notificationContext.notificationId = NotificaitonId;
        notificationContext.notificationTag = message.getLeadAccountId();

        notifier.notify(notificationContext);
    }
}
