package it.clipcall.professional.payment.presenters;

import android.app.Activity;

import javax.inject.Inject;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import it.clipcall.consumer.vidoechat.models.CustomerVideoInvitationData;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.AddressResolver;
import it.clipcall.professional.invitecustomer.controllers.ProfessionalInviteCustomerController;
import it.clipcall.professional.payment.views.IProfessionalBonusCashView;

@PerActivity
public class ProfessionalBonusCashPresenter extends PresenterBase<IProfessionalBonusCashView> implements Branch.BranchLinkCreateListener{


    private final Activity activity;
    private final RoutingService routingService;
    private final ProfessionalInviteCustomerController controller;
    private CustomerVideoInvitationData currentInvitationData;
    private final AddressResolver addressResolver;

    @Inject
    public ProfessionalBonusCashPresenter(Activity activity, RoutingService routingService, ProfessionalInviteCustomerController controller, AddressResolver addressResolver) {
        this.activity = activity;
        this.routingService = routingService;
        this.controller = controller;
        this.addressResolver = addressResolver;
    }

    @Override
    public void initialize() {


    }

    public void inviteCustomer() {
        view.showInvitingCustomerInProgress(true);

        currentInvitationData = controller.createInvitation();
        if(currentInvitationData == null){
            view.showInvitingCustomerInProgress(false);
            return;
        }

        controller.generateLink(activity, currentInvitationData, this);

    }

    @Override
    public void onLinkCreate(String url, BranchError error) {
        if(error != null || currentInvitationData == null){
            view.showInvitingCustomerInProgress(false);
            view.showErrorGeneratingInvitation();
            return;
        }
        String message = currentInvitationData.getMessage();
        String finalMessage = message.replace("{0}", url);
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.addParameter("message", finalMessage);
        routingService.routeTo(Routes.ProfessionalServiceCallInvitationBySmsRoute, navigationContext);
        view.showInvitingCustomerInProgress(false);
    }


}
