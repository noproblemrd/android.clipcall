package it.clipcall.professional.payment.services.tasks;

import com.google.common.base.Strings;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.infrastructure.tasks.ITask2;
import it.clipcall.professional.leads.models.LeadEntity;
import it.clipcall.professional.leads.services.ProfessionalProjectsManager;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class NavigateToCreateQuoteRouteTask implements ITask2<String,RoutingContext, Void> {

    private final RouteParams routeParams;
    private final RoutingService routingService;
    private final ProfessionalProjectsManager professionalProjectsManager;
    private final AuthenticationManager authenticationManager;

    @Inject
    public NavigateToCreateQuoteRouteTask(RouteParams routeParams, RoutingService routingService, ProfessionalProjectsManager professionalProjectsManager, AuthenticationManager authenticationManager){

        this.routeParams = routeParams;
        this.routingService = routingService;
        this.professionalProjectsManager = professionalProjectsManager;
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Void execute(final String leadAccountId, RoutingContext routingContext) {
        String supplierId = authenticationManager.getSupplierId();
        if(Strings.isNullOrEmpty(supplierId))
            return null;

        final List<ProjectEntityBase> projects = professionalProjectsManager.getProjects(supplierId, false);
        ProjectEntityBase project = Lists.firstOrDefault(projects, new ICriteria<ProjectEntityBase>() {
            @Override
            public boolean isSatisfiedBy(ProjectEntityBase candidate) {
                if( !(candidate instanceof LeadEntity))
                    return false;

                LeadEntity project = (LeadEntity) candidate;
                return project.getLeadAccountId().equals(leadAccountId);
            }
        });

        if(project == null)
            return null;

        NavigationContext navigationContext = new NavigationContext(routingContext);
        routeParams.setParam("project",project);
        routeParams.setParam("leadAccountId",leadAccountId);
        routingService.routeTo(Routes.ProfessionalCreateQuoteRoute, navigationContext);
        return null;
    }
}
