package it.clipcall.professional.profile.activities;

import android.graphics.Bitmap;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.commands.EmptyCallBack;
import it.clipcall.infrastructure.commands.PickCoverAreaCommand;
import it.clipcall.infrastructure.commands.PickGoogleAddressCommand;
import it.clipcall.infrastructure.commands.PickServiceCategoriesCommand;
import it.clipcall.infrastructure.maps.entities.PlaceData;
import it.clipcall.infrastructure.support.images.BitmapAssistant;
import it.clipcall.professional.profile.models.Area;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.presenters.ProfessionalEditProfilePresenter;
import it.clipcall.professional.profile.views.IProfessionalEditProfileView;

@EActivity(R.layout.professional_edit_profile_activity)
public class ProfessionalEditProfileActivity extends BaseActivity implements IHasToolbar, IProfessionalEditProfileView {

    @Inject
    ProfessionalEditProfilePresenter presenter;

    boolean isInSelectionMode = false;

    @ViewById
    Toolbar toolbar;

    @ViewById
    EditText businessNameEditText;

    @ViewById
    EditText emailEditText;

    @ViewById
    EditText websiteEditText;

    @ViewById
    EditText businessAddressEditText;

    @ViewById
    EditText businessDescriptionEditText;

    @ViewById
    EditText serviceCategoriesEditText;

    @ViewById
    EditText coverAreaEditText;

    @ViewById
    CircularImageView circularImageView;

    @ViewById
    EditText licenseNumberEditText;


    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        initialize();
    }

    @Click({R.id.circularImageView, R.id.updateProfileTextView})
    void onProfileImageTapped(){
        presenter.pickImage();
    }

    @Background
    void initialize(){
        presenter.initialize();
    }

   @Override
    public String getViewTitle() {
        return "EDIT PROFILE";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        onBackPressed();
        return true;
    }

    @Background
    @Override
    public void onBackPressed() {
        presenter.finishProfileEditing();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.bindView(this);
    }

    @UiThread
    @Override
    public void showProfile(ProfessionalProfile profile) {
        businessAddressEditText.setText(profile.getBusinessAddress());
        businessNameEditText.setText(profile.getBusinessName());
        emailEditText.setText(profile.getEmail());
        websiteEditText.setText(profile.getWebsite());
        licenseNumberEditText.setText(profile.getLicenseNumber());
        businessDescriptionEditText.setText(profile.getBusinessDescription());
        Picasso.with(this).load(profile.getProfileImageUrl()).into(circularImageView);
        Area area = profile.getCoverSelectedArea();
        showCoverArea(area);
        showSelectedCategories(profile.getCategories());
    }

    @UiThread
    @Override
    public void showSelectedAddress(String address) {
        businessAddressEditText.setText(address);
    }

    @UiThread
    @Override
    public void showCoverArea(Area selectedArea) {
        String areaText;
        if(selectedArea.getIsRange()){
            areaText = selectedArea.getId() + " miles";
        }else{
            areaText = selectedArea.getName();
        }
        coverAreaEditText.setText(areaText);
    }

    @UiThread
    @Override
    public void showSelectedCategories(List<Category> selectedCategories) {

        StringBuilder builder = new StringBuilder();
        for (Category category : selectedCategories)
        {
            builder.append(category.getCategoryName() + " // ");
        }
        builder.setLength(builder.length() - 4);
        serviceCategoriesEditText.setText(builder.toString());
    }

    @UiThread
    @Override
    public void showPickedImage(Bitmap image) {
        circularImageView.setImageBitmap(image);
        uploadProfileLogo(image);

    }

    @UiThread
    @Override
    public void goBack() {
        finish();
    }

    @Background
    void uploadProfileLogo(Bitmap image){
        File file = BitmapAssistant.convertToFile(image);
        presenter.uploadProfileLogo(file);
    }

    @Click(R.id.businessAddressEditText)
    void onBusinessAddressTapped(){
        if(isInSelectionMode)
            return;

        isInSelectionMode = true;

        selectBusinessAddress();
    }

    @Click(R.id.serviceCategoriesEditText)
    void onServiceCategoriesTapped(){
        if(isInSelectionMode)
            return;

        isInSelectionMode = true;

        selectServiceCategories();
    }

    @Click(R.id.coverAreaEditText)
    void onCoverAreaTapped(){
        if(isInSelectionMode)
            return;

        isInSelectionMode = true;

        selectCoverArea();
    }

    @TextChange(R.id.businessNameEditText)
    void businessNameChanged(CharSequence text, TextView hello, int before, int start, int count) {
        presenter.setBusinessName(text.toString());
    }

    @TextChange(R.id.emailEditText)
    void emailChanged(CharSequence text, TextView hello, int before, int start, int count) {
        presenter.setEmail(text.toString());
    }

    @TextChange(R.id.websiteEditText)
    void websiteChanged(CharSequence text, TextView hello, int before, int start, int count) {
        presenter.setWebsite(text.toString());
    }

    @TextChange(R.id.businessDescriptionEditText)
    void businessDescriptionChanged(CharSequence text, TextView hello, int before, int start, int count) {
        presenter.setBusinessDescription(text.toString());
    }

    @TextChange(R.id.licenseNumberEditText)
    void licenseNumberChanged(CharSequence text, TextView hello, int before, int start, int count) {
        presenter.setLicenseNumber(text.toString());
    }

    private void selectBusinessAddress(){
        PickGoogleAddressCommand command = new PickGoogleAddressCommand(this);
        command.execute(new EmptyCallBack<PlaceData>(){
            @Override
            public void onSuccess(PlaceData result) {
                isInSelectionMode = false;
                onBusinessAddressSelected(result);
            }

            @Override
            public void onFailure() {
                isInSelectionMode = false;
            }
        });
    }

    void selectServiceCategories(){
        PickServiceCategoriesCommand command = new PickServiceCategoriesCommand(this, presenter.getSelectedCategories());
        command.execute(new EmptyCallBack<List<Category>>(){
            @Override
            public void onSuccess(List<Category> result) {
                isInSelectionMode = false;
                onCategoriesSelected(result);
            }

            @Override
            public void onFailure() {
                isInSelectionMode = false;
            }
        });

    }

    private void selectCoverArea(){
        PickCoverAreaCommand command = new PickCoverAreaCommand(this, presenter.getCoverArea());
        command.execute(new EmptyCallBack<Area>(){
            @Override
            public void onSuccess(Area result) {
                isInSelectionMode = false;
                onCoverAreaSelected(result);
            }

            @Override
            public void onFailure() {
                isInSelectionMode = false;
            }
        });
    }

    @Background
    void onCoverAreaSelected(Area area) {
        presenter.selectArea(area);
    }

    @Background
    void onCategoriesSelected(List<Category> categories){
        presenter.selectCategories(categories);
    }

    @Background
    void onBusinessAddressSelected(PlaceData result) {
        presenter.setBusinessLocationAddress(result);
    }
}