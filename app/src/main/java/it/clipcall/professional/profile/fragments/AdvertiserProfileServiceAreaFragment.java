package it.clipcall.professional.profile.fragments;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.common.base.Joiner;
import com.google.maps.android.SphericalUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.infrastructure.commands.EmptyCallBack;
import it.clipcall.infrastructure.commands.PickGoogleAddressCommand;
import it.clipcall.infrastructure.maps.MapInsideScrollView;
import it.clipcall.infrastructure.maps.entities.PlaceData;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.support.collections.Collections3;
import it.clipcall.professional.profile.models.Area;
import it.clipcall.professional.profile.models.CoverArea;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.presenters.AdvertiserProfileServiceAreaPresenter;
import it.clipcall.professional.profile.support.adapters.CoverAreaAdapter;
import it.clipcall.professional.profile.views.CoverAreaItemView;
import it.clipcall.professional.profile.views.IAdvertiserProfileServiceAreaView;
import it.clipcall.professional.registration.views.IProfessionalRegistrationView;


@EFragment(R.layout.advertiser_profile_service_area)
public class AdvertiserProfileServiceAreaFragment extends ProfileFragment implements IAdvertiserProfileServiceAreaView, OnMapReadyCallback {

    private GoogleMap googleMap;

    private Circle coverAreaCircle;

    @ViewById
    NestedScrollView scrollViewContainer;

    CoverAreaAdapter coverAreaAdapter;

    @Inject
    AdvertiserProfileServiceAreaPresenter presenter;

    @ViewById
    Spinner coverAreaSpinner;
    @ViewById
    EditText businessAddressAutoCompleteTextView;

    @ViewById
    Switch termsOfServiceSwitch;

    MapInsideScrollView mapContainer;

    Geocoder geocoder;


    boolean isSelectingAddress = false;

    @Click(R.id.businessAddressAutoCompleteTextView)
    void pickAddressTapped() {
        if(isSelectingAddress)
            return;

        isSelectingAddress = true;

        selectBusinessAddress();
    }


    private void selectBusinessAddress() {

        PickGoogleAddressCommand command = new PickGoogleAddressCommand(getActivity());
        command.execute(new EmptyCallBack<PlaceData>(){
            @Override
            public void onSuccess(PlaceData result) {
                isSelectingAddress = false;
                onBusinessAddressSelected(result);
            }

            @Override
            public void onFailure() {
                isSelectingAddress = false;
            }
        });
    }



    @Click(R.id.termsOfServiceTextView)
    void onTermsOfServiceTapped(){
        presenter.navigateToTermsOfServicePro();
    }


    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        mapContainer = (MapInsideScrollView) getChildFragmentManager().findFragmentById(R.id.mapContainer);
        mapContainer.getMapAsync(this);
        mapContainer.setListener(new MapInsideScrollView.OnTouchListener() {
            @Override
            public void onTouch() {
                scrollViewContainer.requestDisallowInterceptTouchEvent(true);
            }
        });;

/*

        businessAddressAutoCompleteTextView.setAdapter(new AddressAutoCompleteAdapter(getContext()));
        businessAddressAutoCompleteTextView.setThreshold(1);
        businessAddressAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Address selectedAddress = (Address) businessAddressAutoCompleteTextView.getAdapter().getItem(position);
                setBusinessLocationAddress(selectedAddress);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

            }
        });

        businessAddressAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String input = s +"";
                if(input.length() < 2){
                    clearBusinessAddress();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
*/


        coverAreaAdapter = new CoverAreaAdapter(getContext());
        coverAreaSpinner.setAdapter(coverAreaAdapter);
        coverAreaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Area coverArea = coverAreaAdapter.getItem(position);
                setCoverArea(coverArea);

                View selectedView = coverAreaSpinner.getSelectedView();
                if(selectedView == null)
                    return;

                if(!(selectedView instanceof CoverAreaItemView))
                    return;

                CoverAreaItemView itemView = (CoverAreaItemView) view;
                itemView.clearError();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        geocoder = new Geocoder(getActivity());

    }

    @Background
    void clearBusinessAddress(){
        presenter.clearBusinessAddress();
    }

    @Background
    void initialize(){
        presenter.initialize();
    }

    @Override
    protected String getTitle() {
        return "Profile";
    }

    @Override
    public RoutingContext getRoutingContext() {
        return new RoutingContext(getActivity());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        initialize();
    }

    @Background
    @Override
    public void nextButtonTapped() {
        presenter.nextButtonTapped();
    }

    @Override
    public void backButtonTapped() {

    }

    @Override
    public boolean isInValidState() {
        return presenter.validate();
    }

    @UiThread
    @Override
    public void completeProfileRegistration() {
        profileStagesLisenter.onLastStage();
    }

    @UiThread
    @Override
    public void showProfile(ProfessionalProfile profile) {
        CoverArea coverArea = profile.getCoverArea();
        if(coverArea == null)
            return;
        String address = coverArea.getBusinessAddress();
        businessAddressAutoCompleteTextView.setText(address);
        showCoverAreaCore(coverArea);
    }

    @UiThread
    @Override
    public void showCoverArea(CoverArea coverArea) {
        showCoverAreaCore(coverArea);
    }


    @UiThread
    @Override
    public void showRequiredBusinessAddress(boolean requestFocus) {
        businessAddressAutoCompleteTextView.setError("Business address is required");
    }

    @UiThread
    @Override
    public void nextStageProcessingCompleted() {
        profileStagesLisenter.onNextStageProcessingCompleted();
    }

    @UiThread
    @Override
    public void showUpdatingProfileError() {

    }

    @UiThread
    @Override
    public void showRequiredCoverArea(boolean requestFocus) {
        View view = coverAreaSpinner.getSelectedView();
        if(view == null)
            return;

        if(!(view instanceof CoverAreaItemView))
            return;

        CoverAreaItemView itemView = (CoverAreaItemView) view;

        itemView.setRequiredError(requestFocus);
    }

    @UiThread
    @Override
    public void showSelectedAddress(String businessAddress) {
        businessAddressAutoCompleteTextView.setText(businessAddress);
    }

    @UiThread
    @Override
    public void showCompleteRegistrationErrors(List<String> errorMessages) {
        String message = Joiner.on("\n").join(errorMessages);
        if( getActivity() instanceof IProfessionalRegistrationView){
            IProfessionalRegistrationView registrationView = (IProfessionalRegistrationView) getActivity();
            registrationView.showErrorMessage(message);
        }
    }

    @Override
    public void showPolicyConfirmationRequired(boolean requestFocus) {
        String errorMessage = "You must agree to ClipCall's terms of service";
        termsOfServiceSwitch.setError(errorMessage);
        if(requestFocus)
        {
            if( getActivity() instanceof IProfessionalRegistrationView){
                IProfessionalRegistrationView registrationView = (IProfessionalRegistrationView) getActivity();
                registrationView.showErrorMessage(errorMessage);
            }
        }


    }

    private void showCoverAreaCore(CoverArea coverArea){
        coverAreaAdapter.clear();
        coverAreaAdapter.addAll(coverArea.getAreasInList());
        int index = coverArea.getSelectedAreaIndex();
        coverAreaSpinner.setSelection(index);
        LatLng latLng = new LatLng(coverArea.getLatitude(),coverArea.getLongitude());
        float zoomLevel = 5;
        //googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));


        Area selectedArea = coverArea.getSelectedArea();
        if(selectedArea == null){
            Area emptyArea = new Area();
            emptyArea.setId("");
            emptyArea.setSelected(false);
            emptyArea.setName("Cover area is required");
            coverAreaAdapter.add(emptyArea);
            return;
        }


        int selectedAreaIndex = coverArea.getSelectedAreaIndex();
        double radiusInMeters = 100.0;
        int strokeColor = 0xffff0000; //red outline
        int shadeColor = 0x44ff0000; //opaque red fill

        if(selectedArea.getIsRange()){
            double miles = Double.parseDouble(selectedArea.getId());
            radiusInMeters = 1609.344 * miles;
        }
        else{
            String areaAddress = selectedArea.getName();
            if(!areaAddress.contains("United States"))
                areaAddress += ", USA";
            try {
                List<Address> addresses =  geocoder.getFromLocationName(areaAddress, 1);
                if(!Collections3.isNullOrEmpty(addresses)){
                    Address areaFullAddress = addresses.get(0);
                    latLng = new LatLng(areaFullAddress.getLatitude(),areaFullAddress.getLongitude());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        CircleOptions circleOptions = new CircleOptions().center(latLng).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
        googleMap.clear();
        coverAreaCircle = googleMap.addCircle(circleOptions);

        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, getZoomLevel());
        googleMap.animateCamera(yourLocation);



    }



    public int getZoomLevel() {
        double radius = coverAreaCircle.getRadius();
        double scale = radius / 100;
        int zoomLevel = (int) (16 - Math.log(scale) / Math.log(2));
        return zoomLevel;

    }

    @Background
    public void onBusinessAddressSelected(PlaceData place) {
        presenter.setBusinessLocationAddress(place);
    }

    @Background
    public void setCoverArea(Area coverArea) {
        this.presenter.selectCoverArea(coverArea);
    }

    public LatLngBounds toBounds(LatLng center, double radius) {
        LatLng southwest = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 225);
        LatLng northeast = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 45);
        return new LatLngBounds(southwest, northeast);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.handleActivityResult(requestCode, resultCode, data);
    }
}