package it.clipcall.professional.profile.views;

import android.content.Context;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import it.clipcall.R;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.profile.support.ICategoryListener;

@EViewGroup(R.layout.professional_category_item_view)
public class ProfessionalCategoryItemView extends RelativeLayout {

    private ICategoryListener categoryListener;
    private Category category;


    @ViewById
    ImageButton deleteCategoryImageButton;

    @ViewById
    TextView categoryNameTextView;




    public ProfessionalCategoryItemView(Context context) {
        super(context);
    }

    public void bind(Category category, final ICategoryListener categoryListener){
        this.categoryListener = categoryListener;
        this.category = category;
        categoryNameTextView.setText(category.getCategoryName());
    }

    @Click(R.id.deleteCategoryImageButton)
    void onDeleteTapped(){
        if(categoryListener != null)
            categoryListener.onDeletingCategory(category);
    }

}
