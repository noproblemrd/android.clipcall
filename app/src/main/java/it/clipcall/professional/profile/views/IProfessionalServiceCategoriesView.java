package it.clipcall.professional.profile.views;

import java.util.List;

import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.profile.models.Category;

/**
 * Created by dorona on 08/05/2016.
 */
public interface IProfessionalServiceCategoriesView extends IView {

    void setAvailableCategories(List<Category> availableCategories);
    void showCategories(List<Category> categories);
}
