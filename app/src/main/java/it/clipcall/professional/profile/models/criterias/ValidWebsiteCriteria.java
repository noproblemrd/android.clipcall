package it.clipcall.professional.profile.models.criterias;

import android.util.Patterns;

import it.clipcall.infrastructure.support.criterias.ICriteria;

/**
 * Created by dorona on 02/06/2016.
 */
public class ValidWebsiteCriteria implements ICriteria<String> {
    @Override
    public boolean isSatisfiedBy(String candidate) {
        return Patterns.WEB_URL.matcher(candidate).matches();
    }
}
