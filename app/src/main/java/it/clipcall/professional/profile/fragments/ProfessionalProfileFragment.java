package it.clipcall.professional.profile.fragments;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;

import com.google.common.base.Strings;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.simple.eventbus.EventBus;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.mainMenu.support.FabContext;
import it.clipcall.consumer.mainMenu.support.ICollapsingToolBarListener;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.professional.profile.models.Area;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.models.ProfileData;
import it.clipcall.professional.profile.presenters.AdvertiserProfilePresenter;
import it.clipcall.professional.profile.support.IProfileStagesListener;
import it.clipcall.professional.profile.support.adapters.ProfessionalCategoryAdapter;
import it.clipcall.professional.profile.views.IAdvertiserProfileView;


@EFragment(R.layout.professional_profile_fragment)
public class ProfessionalProfileFragment extends BaseFragment implements IAdvertiserProfileView, IProfileStagesListener, View.OnClickListener{

    @Inject
    AdvertiserProfilePresenter presenter;


    @ViewById
    EditText businessNameEditText;

    @ViewById
    EditText emailAddressEditText;


    @ViewById
    EditText websiteEditText;

    @ViewById
    EditText businessAddressEditText;


    @ViewById
    EditText coverAreaEditText;

    @ViewById
    EditText categoriesEditText;

    @ViewById
    EditText businessDescriptionEditText;

    @ViewById
    EditText licenseNumberEditText;


    /*@ViewById
    RecyclerView proCategoriesRecyclerView;*/

    ProfessionalCategoryAdapter professionalCategoryAdapter;

    @AfterViews
    void afterViews(){
        professionalCategoryAdapter = new ProfessionalCategoryAdapter(getActivity(), null);
        /*proCategoriesRecyclerView.setAdapter(professionalCategoryAdapter);
        proCategoriesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));*/
    }



    @Background
    void initialize(){
        presenter.initialize();
    }

    @Override
    protected String getTitle() {
        return "Profile";
    }

    @Override
    public RoutingContext getRoutingContext() {
        return new RoutingContext(getActivity());
    }


    @Override
    protected int getMenuItemId() {
        return R.id.nav_business_profile;
    }




    @Override
    public void onNextStage() {
      /*  currentIndex++;
        showCurrentFragment();*/
    }

    @UiThread
    @Override
    public void onNextStageProcessingCompleted() {
        /*loadingSpinnerProgressBar.setVisibility(View.GONE);
        nextButton.setVisibility(View.VISIBLE);
        nextButton.setEnabled(true);*/
    }

    @Override
    public void onLastStage() {
        presenter.profileCompleted();
    }

    @UiThread
    @Override
    public void titleChanged(String title) {

    }

    @UiThread
    @Override
    public void showError(String message) {
        //Snackbar.make(profileContainer, message ,Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
      /*  super.setFacebookActivityResult(requestCode, resultCode, data);
        ProfileFragment profileFragment = fragments.get(currentIndex);
        profileFragment.setFacebookActivityResult(requestCode,resultCode,data);*/
    }

  /*  private ProfileFragment getCurrentProfileFragment(){
        ProfileFragment profileFragment = fragments.get(currentIndex);
        return profileFragment;
    }*/

    private boolean allOthersSectionsValid(int sectionIndex){
      /*  for(int i=0; i<fragments.size(); ++i)
        {
            if(sectionIndex == 1)
                continue;

            ProfileFragment profileFragment = fragments.get(currentIndex);
            if(!profileFragment.isInValidState())
                return false;
        }*/

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.bindView(this);
        initialize();
    }

    @UiThread
    @Override
    public void showProfile(ProfessionalProfile profile) {
        if(getActivity()  instanceof ICollapsingToolBarListener){
            ICollapsingToolBarListener collapsingToolBarListener = (ICollapsingToolBarListener) getActivity();
            collapsingToolBarListener.showImage(profile.getProfileImageUrl());
            FabContext context = new FabContext();
            context.drawableResourceId = R.drawable.ic_mode_edit_white_24dp;
            context.listener = this;
            collapsingToolBarListener.setAction(context);
        }

        businessNameEditText.setText(profile.getBusinessName());
        emailAddressEditText.setText(profile.getBusinessAddress());


        emailAddressEditText.setText(profile.getEmail());
        websiteEditText.setText(Strings.isNullOrEmpty(profile.getWebsite()) ? "N\\A" : profile.getWebsite());
        licenseNumberEditText.setText(Strings.isNullOrEmpty(profile.getLicenseNumber()) ? "N\\A"  : profile.getLicenseNumber());
        businessAddressEditText.setText(profile.getBusinessAddress());
        businessDescriptionEditText.setText(profile.getBusinessDescription());

        StringBuilder builder = new StringBuilder();
        for (Category category : profile.getCategories())
        {
            builder.append(category.getCategoryName() + " // ");
        }
        builder.setLength(builder.length() - 4);
        categoriesEditText.setText(builder.toString());


        Area selectedArea = profile.getCoverArea().getSelectedArea();
        String areaText;
        if(selectedArea.getIsRange()){
            areaText = selectedArea.getId() + " miles";
        }else{
            areaText = selectedArea.getName();
        }
        coverAreaEditText.setText(areaText);

        ProfileData profileData  = new ProfileData(profile.getProfileImageUrl(),profile.getBusinessName());
        EventBus.getDefault().post(profileData);
    }

    @Override
    public void onClick(View v) {

        presenter.editProfile();
    }
}