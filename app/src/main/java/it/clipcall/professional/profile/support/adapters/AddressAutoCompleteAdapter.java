package it.clipcall.professional.profile.support.adapters;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dorona on 01/12/2015.
 */
public class AddressAutoCompleteAdapter extends ArrayAdapter<Address> implements Filterable {

    private LayoutInflater layoutInflater;
    private Geocoder geocoder;
    private StringBuilder stringBuilder = new StringBuilder();

    public AddressAutoCompleteAdapter(Context context, int resource, Address[] objects) {
        super(context, resource, objects);
    }

    private String typedAddress = null;

    public AddressAutoCompleteAdapter(final Context context) {
        super(context, -1);
        layoutInflater = LayoutInflater.from(context);
        geocoder = new Geocoder(context);
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        final TextView tv;
        if (convertView != null) {
            tv = (TextView) convertView;
        } else {
            tv = (TextView) layoutInflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        }

        tv.setText(createFormattedAddressFromAddress(getItem(position)));
        return tv;
    }

    private String createFormattedAddressFromAddress(final Address address) {
        //return address.getPostalCode();
        ArrayList<String> addressFragments = new ArrayList<String>();
        for(int i = 0; i < address.getMaxAddressLineIndex(); i++) {
            addressFragments.add(address.getAddressLine(i));
        }
        String formattedAddress = TextUtils.join(", ",addressFragments);
        return formattedAddress;
    }

    @Override
    public Filter getFilter() {

        Filter myFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(final CharSequence constraint) {
                List<Address> addresses = new ArrayList<>();
                if (constraint != null) {

                    try {

                        String value = (String) constraint;
                        typedAddress = value;
                        if (!value.toLowerCase().contains("us"))
                            value += ", US";

                        addresses = geocoder.getFromLocationName(value, 7);

                    } catch (Exception e) {

                    }
                }

                final FilterResults filterResults = new FilterResults();
                filterResults.values = addresses;
                filterResults.count = addresses.size();
                return filterResults;
            }



            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(final CharSequence constraint, final FilterResults results) {
                clear();
                for (Address address : (List<Address>) results.values) {
                    add(address);
                }
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }

            @Override
            public CharSequence convertResultToString(final Object resultValue) {
                if(resultValue == null)
                    return "";

                Address address = (Address)resultValue;
                ArrayList<String> addressFragments = new ArrayList<String>();
                for(int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressFragments.add(address.getAddressLine(i));
                }
                String formattedAddress = TextUtils.join(", ",addressFragments);
                return formattedAddress;
            }
        };
        return myFilter;
    }
}
