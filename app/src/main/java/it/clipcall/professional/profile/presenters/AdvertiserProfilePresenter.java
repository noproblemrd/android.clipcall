package it.clipcall.professional.profile.presenters;

import javax.inject.Inject;

import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.objects.ObjectSupport;
import it.clipcall.professional.profile.controllers.ProfessionalProfileController;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.views.IAdvertiserProfileView;

public class AdvertiserProfilePresenter extends PresenterBase<IAdvertiserProfileView> {

    private final ProfessionalProfileController controller;
    private final RoutingService routingService;
    private final RouteParams routeParams;
    ProfessionalProfile profile;

    @Inject
    public AdvertiserProfilePresenter(ProfessionalProfileController controller, RoutingService routingService, RouteParams routeParams) {
        this.controller = controller;
        this.routingService = routingService;
        this.routeParams = routeParams;
    }

    @Override
    public void initialize() {
        ProfessionalProfile profile = controller.getProfessionalProfile();
        if(profile == null)
        {
            return;
        }

        this.profile  = profile;
        view.showProfile(profile);
    }


    public void profileCompleted() {
        routingService.routeToHome(view.getRoutingContext());
    }

    public boolean isProfessionalRegistered(){
        return controller.isProfessionalRegistered();
    }

    public void editProfile() {
        routeParams.reset();
        ProfessionalProfile clonedProfile  = ObjectSupport.deepClone(profile);
        routeParams.setParam("profile", clonedProfile);
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        routingService.routeTo(Routes.ProfessionalEditProfileRoute,navigationContext);
    }
}
