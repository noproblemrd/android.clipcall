package it.clipcall.professional.profile.presenters;

import android.graphics.Bitmap;

import com.google.common.base.Strings;

import org.simple.eventbus.EventBus;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ProfessionalEventNames;
import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.commands.EmptyCallBack;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.images.BitmapAssistant;
import it.clipcall.infrastructure.ui.images.services.ImagePickerService;
import it.clipcall.infrastructure.ui.permissions.PermissionsService;
import it.clipcall.professional.profile.controllers.ProfessionalProfileController;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.models.eProfessionalProfileSection;
import it.clipcall.professional.profile.views.IAdvertiserProfileAboutView;

@PerActivity
public class AdvertiserProfileAboutPresenter extends PresenterBase<IAdvertiserProfileAboutView> {

    private final ProfessionalProfileController controller;
    private final PermissionsService permissionsService;
    private final ImagePickerService imagePickerService;
    private final RoutingService routingService;


    @Inject
    public AdvertiserProfileAboutPresenter(ProfessionalProfileController controller, PermissionsService permissionsService, ImagePickerService imagePickerService, RoutingService routingService) {
        this.controller = controller;
        this.permissionsService = permissionsService;
        this.imagePickerService = imagePickerService;
        this.routingService = routingService;
    }

    @Override
    public void initialize() {
        view.showUploadingProfileLogo();
        ProfessionalProfile profile = controller.getProfessionalProfile();
        if(profile == null)
        {
            return ;
        }

        view.showProfile(profile);
    }

    public void setBusinessDescription(String businessDescription) {
        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationEnterBusinessDescription),businessDescription );
        this.controller.setBusinessDescription(businessDescription);
    }

    public void setHasLicense(boolean hasLicense) {
        controller.setHasLicenseNumber(hasLicense);
    }

    public void setLicenseNumber(String licenseNumber) {
        Map<String,String> property = new HashMap<String, String>();
        property.put("licenseNumber", licenseNumber);
        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationEnterLicenseNumber, property));
        controller.setLicenseNumber(licenseNumber);
    }

    public void nextButtonTapped() {
        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationNext2Tapped));
        boolean isValid = validate();
        if(!isValid)
        {
            view.nextStageProcessingCompleted();
            return;
        }

        boolean profileUpdated = controller.updateProfile(eProfessionalProfileSection.About);
        if(!profileUpdated){

            view.showUpdatingProfileError();
            view.nextStageProcessingCompleted();
            return;
        }

        view.goToNextStage();
        view.nextStageProcessingCompleted();
    }

    public boolean isProfessionalRegistered() {
        return controller.isProfessionalRegistered();

    }

    public boolean validate(){
        ProfessionalProfile profile = controller.getProfessionalProfile();
        if(profile == null)
            return false;

        int totalErrors = 0;
        boolean requestFocus;

        if(Strings.isNullOrEmpty(profile.getProfileImageUrl())){
            totalErrors++;
            requestFocus = totalErrors == 1;
            view.showRequiredProfileImage(requestFocus);
        }
        if(profile.hasLicenseNumber() && Strings.isNullOrEmpty(profile.getLicenseNumber())){
            totalErrors++;
            requestFocus = totalErrors == 1;
            view.showRequiredLicenseNumber(requestFocus);
        }

        if(Strings.isNullOrEmpty(profile.getBusinessDescription())){
            totalErrors++;
            requestFocus = totalErrors == 1;
            view.showRequiredBusinessDescription(requestFocus);
        }

        return totalErrors == 0;
    }

    @RunOnUiThread
    public void pickImage() {
        imagePickerService.pickImage(new EmptyCallBack<Bitmap>(){

            @Override
            public void onSuccess(Bitmap result) {
                imagePicked(result);
            }
        });
    }


    public void imagePicked(Bitmap image) {
        File file = BitmapAssistant.convertToFile(image);
        view.showUploadingProfileLogo();
        if(file == null)
        {
            view.showUploadingProfileLogoError();
            return;
        }
        String url = controller.uploadImage(file);
        if (Strings.isNullOrEmpty(url)) {
            view.showUploadingProfileLogoError();
            return;
        }

        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationSelectProfileImage),url );
        controller.setProfileImageUrl(url);
        view.profileLogoUploaded(url);
    }
}
