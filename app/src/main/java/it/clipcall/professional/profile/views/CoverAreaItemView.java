package it.clipcall.professional.profile.views;

import android.content.Context;
import android.graphics.Color;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import it.clipcall.R;
import it.clipcall.professional.profile.models.Area;

@EViewGroup(R.layout.professional_cover_area_item_view)
public class CoverAreaItemView extends RelativeLayout {

    @ViewById
    TextView coverAreaTextView;

    int originalTextColor;

    public CoverAreaItemView(Context context) {
        super(context);
    }

    public void bind(Area area){
        String areaName = area.getName();
        coverAreaTextView.setText(areaName);
    }

    public void setRequiredError(boolean requestFocus) {
        coverAreaTextView.setError("Cover area is required");
        coverAreaTextView.setTextColor(Color.RED);
        originalTextColor = coverAreaTextView.getCurrentTextColor();
        /*if(requestFocus){
            coverAreaTextView.requestFocus();
        }*/
    }

    public void clearError() {
        coverAreaTextView.setTextColor(originalTextColor);
        coverAreaTextView.setError(null);
    }
}
