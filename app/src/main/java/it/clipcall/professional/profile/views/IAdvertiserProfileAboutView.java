package it.clipcall.professional.profile.views;

import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 18/01/2016.
 */
public interface IAdvertiserProfileAboutView extends IView {

    void showRequiredBusinessDescription(boolean requestFocus);

    void showRequiredLicenseNumber(boolean requestFocus);

    void showInvalidBusinessLogo();

    void goToNextStage();

    void showProfile(ProfessionalProfile profile);

    void showRequiredProfileImage(boolean requestFocus);

    void profileLogoUploaded(String url);

    void nextStageProcessingCompleted();

    void showUploadingProfileLogo();

    void showUpdatingProfileError();

    void showUploadingProfileLogoError();

}
