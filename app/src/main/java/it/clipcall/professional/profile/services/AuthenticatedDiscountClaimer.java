package it.clipcall.professional.profile.services;

import it.clipcall.consumer.payment.controllers.PaymentCoordinator;
import it.clipcall.consumer.profile.models.FacebookProfile;
import it.clipcall.consumer.profile.services.CustomerProfileManager;
import it.clipcall.infrastructure.domainModel.services.IDomainService;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by dorona on 08/06/2016.
 */
public class AuthenticatedDiscountClaimer implements IDiscountClaimer, IDomainService {

    private final PaymentCoordinator paymentCoordinator;
    private final AuthenticationManager authenticationManager;
    private final CustomerProfileManager profileManager;

    public AuthenticatedDiscountClaimer(PaymentCoordinator paymentCoordinator, AuthenticationManager authenticationManager, CustomerProfileManager profileManager) {
        this.paymentCoordinator = paymentCoordinator;
        this.authenticationManager = authenticationManager;
        this.profileManager = profileManager;
    }

    @Override
    public boolean claimDiscount() {
        return paymentCoordinator.claimDiscount(false);
    }

    @Override
    public boolean isDiscountClaimed() {
        return paymentCoordinator.isDiscountClaimed();
    }

    public boolean updateCustomerProfile(FacebookProfile facebookProfile) {
        String customerId = authenticationManager.getCustomerId();
        boolean updated = profileManager.updateCustomerProfile(customerId, facebookProfile);
        return updated;
    }
}
