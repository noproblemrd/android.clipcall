package it.clipcall.professional.profile.models;

import android.location.Address;

import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.Collections2;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import it.clipcall.common.models.Profile;

@Parcel
public class ProfessionalProfile extends Profile{

    CoverArea coverArea;
    String businessName;
    String email;
    String website;

    List<Category> categories;
    String profileImageUrl;
    String licenseNumber;
    String businessDescription;

    public ProfessionalProfile(){
        categories = new ArrayList<>();
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public boolean addCategory(final Category category) {
        Collection<Category> filteredCategories = Collections2.filter(this.categories, new Predicate<Category>() {
            @Override
            public boolean apply(Category input) {
                return input.equals(category);
            }
        });

        if(filteredCategories != null && filteredCategories.size() > 0)
            return false;

        return this.categories.add(category);
    }

    public boolean removeCategory(Category category){
        return this.categories.remove(category);
    }

    public Category findCategory(Category category){
        Collection<Category> categories = findCategories(category);
        if(categories != null && categories.size() == 1)
            return category;

        return null;
    }

    private Collection<Category> findCategories(final Category category){
        Collection<Category> filteredCategories = Collections2.filter(this.categories, new Predicate<Category>() {
            @Override
            public boolean apply(Category input) {
                return input.equals(category);
            }
        });

        return filteredCategories;
    }

    public String getBusinessAddress() {
        return coverArea.getBusinessAddress();
    }

    public String getBusinessDescription() {
        return businessDescription;
    }

    public void setBusinessDescription(String businessDescription) {
        this.businessDescription = businessDescription;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public CoverArea getCoverArea() {
        return coverArea;
    }

    public void setCoverArea(CoverArea coverArea) {
        this.coverArea = coverArea;
    }

    public boolean hasLicenseNumber() {
        return !Strings.isNullOrEmpty(licenseNumber);
    }

    public void setAddress(Address address) {
        coverArea.setAddress(address);
    }

    public boolean setSelectedArea(Area area) {
        boolean updatedSelectedArea = coverArea.setSelectedArea(area);
        return updatedSelectedArea;
    }

    public void clearBusinessAddress() {
        if(coverArea == null)
            return;

        coverArea.setBusinessAddress("");
    }

    public Area getCoverSelectedArea() {
        return getCoverArea().getSelectedArea();
    }
}
