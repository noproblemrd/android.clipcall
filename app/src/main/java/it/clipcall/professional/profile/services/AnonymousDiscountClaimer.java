package it.clipcall.professional.profile.services;

import it.clipcall.consumer.profile.models.FacebookProfile;
import it.clipcall.consumer.profile.services.CustomerProfileManager;
import it.clipcall.infrastructure.domainModel.services.IDomainService;

/**
 * Created by dorona on 08/06/2016.
 */
public class AnonymousDiscountClaimer implements IDiscountClaimer, IDomainService {

    private final CustomerProfileManager profileManager;


    public AnonymousDiscountClaimer(CustomerProfileManager profileManager) {
        this.profileManager = profileManager;
    }

    @Override
    public boolean updateCustomerProfile(FacebookProfile facebookProfile) {
        profileManager.saveFacebookProfile(facebookProfile);
        return true;
    }

    @Override
    public boolean claimDiscount() {
        return true;
    }

    @Override
    public boolean isDiscountClaimed() {
        return profileManager.getFacebookProfile() != null;
    }
}
