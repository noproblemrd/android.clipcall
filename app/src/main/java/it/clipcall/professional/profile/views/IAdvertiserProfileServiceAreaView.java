package it.clipcall.professional.profile.views;

import java.util.List;

import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.profile.models.CoverArea;
import it.clipcall.professional.profile.models.ProfessionalProfile;

/**
 * Created by dorona on 18/01/2016.
 */
public interface IAdvertiserProfileServiceAreaView extends IView {

    void completeProfileRegistration();

    void showProfile(ProfessionalProfile profile);

    void showCoverArea(CoverArea coverArea);

    void showRequiredBusinessAddress(boolean requestFocus);

    void nextStageProcessingCompleted();

    void showUpdatingProfileError();

    void showRequiredCoverArea(boolean requestFocus);

    void showSelectedAddress(String businessAddress);

    void showCompleteRegistrationErrors(List<String> errorMessages);

    void showPolicyConfirmationRequired(boolean requestFocus);
}
