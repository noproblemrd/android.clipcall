package it.clipcall.professional.profile.presenters;

import com.google.common.base.Strings;

import org.simple.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import javax.inject.Inject;

import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ProfessionalEventNames;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.professional.profile.controllers.ProfessionalProfileController;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.models.criterias.ValidWebsiteCriteria;
import it.clipcall.professional.profile.models.eProfessionalProfileSection;
import it.clipcall.professional.profile.views.IAdvertiserProfileDetailsView;

@PerActivity
public class AdvertiserProfileDetailsPresenter extends PresenterBase<IAdvertiserProfileDetailsView> {

    private final ProfessionalProfileController controller;

    @Inject
    public AdvertiserProfileDetailsPresenter(ProfessionalProfileController controller) {
        this.controller = controller;
    }

    @Override
    public void initialize() {

        view.showLoadingProfile();

        final List<Category> availableCategories = controller.getAvailableCategories();
        view.setAvailableCategories(availableCategories);

        ProfessionalProfile professionalProfile = controller.getProfessionalProfile();
        view.showProfileLoaded();
        if(professionalProfile  == null)
            return;

        view.showProfile(professionalProfile );
    }


    public void addCategory(Category category) {
        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationAddCategory, category));
        boolean categoryAdded = controller.addCategory(category);
        if(categoryAdded)
            view.showAddedCategory(category);
    }

    public void removeCategory(Category category){
        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationRemoveCategory, category));
        boolean categoryRemoved = controller.removeCategory(category);
        if(categoryRemoved)
            view.showRemovedCategory(category);
    }

    public void setWebsite(String website) {
        Map<String,String> property = new HashMap<String, String>();
        property.put("website", website);
        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationSetWebsite, property));
        this.controller.setWebsite(website);
    }

    public void setEmail(String email) {
        Map<String,String> property = new HashMap<String, String>();
        property.put("email", email);
        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationSetEmail, property));
        controller.setEmail(email);
    }

    public void setBusinessName(String businessName) {
        Map<String,String> property = new HashMap<String, String>();
        property.put("businessName", businessName);
        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationEnterBusinessName, property));
        controller.setBusinessName(businessName);
    }

    public void nextButtonTapped() {

        boolean isValid = validate();
        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationNext1Tapped));
        if (!isValid) {
            view.nextStageProcessingCompleted();
            return;
        }

        boolean profileUpdated = controller.updateProfile(eProfessionalProfileSection.Details);
        if (!profileUpdated) {

            view.showUpdatingProfileError();
            view.nextStageProcessingCompleted();
            return;
        }


        view.goToNextStage();
        view.nextStageProcessingCompleted();
    }

    public boolean validate() {
        ProfessionalProfile professionalProfile = controller.getProfessionalProfile();
        int totalErrors = 0;
        boolean requestFocus;

        if(professionalProfile == null)
            return false;

        if(Strings.isNullOrEmpty(professionalProfile.getBusinessName()))
        {
            totalErrors++;
            requestFocus = totalErrors == 1;
            view.showRequiredBusinessName(requestFocus);
        }

        if(Strings.isNullOrEmpty(professionalProfile.getEmail())){
            totalErrors++;
            requestFocus = totalErrors == 1;
            view.showRequiredEmail(requestFocus);
        }

        if(!isValidEmail(professionalProfile.getEmail())){
            totalErrors++;
            requestFocus = totalErrors == 1;
            view.showInvalidEmail(requestFocus);
        }

        if(!Strings.isNullOrEmpty(professionalProfile.getWebsite())){
            ValidWebsiteCriteria validWebsiteCriteria = new ValidWebsiteCriteria();
            if(!validWebsiteCriteria.isSatisfiedBy(professionalProfile.getWebsite())){
                totalErrors++;
                requestFocus = totalErrors == 1;
                view.showInvalidWebsite(requestFocus);
            }
        }



        List<Category> categories = professionalProfile.getCategories();
        if(categories == null || categories.size() == 0){
            totalErrors++;
            requestFocus = totalErrors == 1;
            view.showRequiredCategories(requestFocus);
        }

        return totalErrors == 0;

    }

    private boolean isValidEmail(String email){
        if(Strings.isNullOrEmpty(email))//email is optional
            return true;

        Matcher matcher = android.util.Patterns.EMAIL_ADDRESS.matcher(email);
        return matcher.matches();
    }


}
