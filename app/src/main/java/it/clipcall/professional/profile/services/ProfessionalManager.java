package it.clipcall.professional.profile.services;


import android.location.Address;

import com.google.common.base.Strings;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.profile.models.Logo;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.infrastructure.domainModel.services.IDomainService;
import it.clipcall.infrastructure.repositories.Repository2;
import it.clipcall.infrastructure.support.collections.Collections3;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.support.network.RetrofitCallProcessor;
import it.clipcall.infrastructure.support.strings.Strings2;
import it.clipcall.infrastructure.support.validation.Validator;
import it.clipcall.professional.leads.services.IProfessionalProjectsService;
import it.clipcall.professional.profile.models.Area;
import it.clipcall.professional.profile.models.BusinessAddress;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.profile.models.CoverArea;
import it.clipcall.professional.profile.models.CoverAreaCollection;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.models.criterias.ValidWebsiteCriteria;
import it.clipcall.professional.profile.models.eProfessionalProfileSection;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;

@Singleton
public class ProfessionalManager implements IDomainService {

    private final Repository2 repository;
    private final IProfessionalProfileService professionalProfileService;
    private final IProfessionalProjectsService professionalProjectsService;

    private ProfessionalProfile profile;

    @Inject
    public ProfessionalManager(Repository2 repository, IProfessionalProfileService professionalProfileService, IProfessionalProjectsService professionalProjectsService){

        this.repository = repository;
        this.professionalProfileService = professionalProfileService;
        this.professionalProjectsService = professionalProjectsService;
    }

    public boolean addCategory(Category category){
        if(profile == null)
            return false;

        boolean added = profile.addCategory(category);
        return added;
    }

    public boolean removeCategory(Category category){
       if(profile == null)
           return false;

        boolean removed = profile.removeCategory(category);
        return removed;
    }

    public void setBusinessName(String businessName){
        if(profile == null)
            return;

        profile.setBusinessName(businessName);
    }

    public void setBusinessDescription(String businessDescription){
        if(profile == null)
            return;

        profile.setBusinessDescription(businessDescription);
    }

    public void setProfileImageUrl(String logoUrl){
        if(profile == null)
            return;

        profile.setProfileImageUrl(logoUrl);
    }

    public void setLicenseNumber(String licenseNumber){
        if(profile == null)
            return;

        profile.setLicenseNumber(licenseNumber);
    }

    public void setWebsite(String website) {
        if(this.profile == null)
            return;

        this.profile.setWebsite(website);
    }

    public void setEmail(String email) {
        if(profile == null)
            return;

        profile.setEmail(email);
    }

    public String uploadImage(String supplierId, File file) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        Call<Logo> uploadImageCall = professionalProfileService.uploadImage(supplierId, requestBody);
        Logo logo = RetrofitCallProcessor.processCall(uploadImageCall);
        if(logo == null){
            return null;
        }
        String url = logo.getUrl();
        return url;
    }

    public void saveProfile(ProfessionalProfile profile) {
        repository.update(profile);
    }

    public CoverArea updateProfileBusinessAddress(String supplierId, Address address) {
        if(profile == null)
            return null;

        profile.setAddress(address);

        BusinessAddress businessAddress = new BusinessAddress();
        CoverArea coverArea = profile.getCoverArea();
        businessAddress.setLatitude(coverArea.getLatitude());
        businessAddress.setLongitude(coverArea.getLongitude());
        businessAddress.setBusinessAddress(coverArea.getBusinessAddress());
        Call<CoverAreaCollection> updateBusinessAddressCall = professionalProfileService.updateProfileBusinessAddress(supplierId, businessAddress);
        CoverAreaCollection coverAreaCollection = RetrofitCallProcessor.processCall(updateBusinessAddressCall);
        if(coverAreaCollection == null)
            return null;

        if(Collections3.isNullOrEmpty(coverAreaCollection.getAreas()))
            return null;

        CoverArea firstCoverArea = coverAreaCollection.getAreas().get(0);
        profile.setCoverArea(firstCoverArea);
        return firstCoverArea;
    }

    public boolean updateProfile(String supplierId, eProfessionalProfileSection section) {
        Call<StatusResponse> statusResponseCall = professionalProfileService.updateProfile(supplierId, this.profile, section.toString());
        boolean success = RetrofitCallProcessor.processStatusCall(statusResponseCall);
        return success;
    }

    public ProfessionalProfile getProfessionalProfile(String supplierId){
        if(profile != null)
            return profile;

        if(Strings.isNullOrEmpty(supplierId))
            return null;

        Call<ProfessionalProfile> getProfileCall = professionalProfileService.getProfile(supplierId);
        ProfessionalProfile profile = RetrofitCallProcessor.processCall(getProfileCall);
        this.profile = profile;
        return profile;
    }

    public boolean selectArea(Area area) {
        if(profile == null)
            return false;

        return profile.setSelectedArea(area);
    }

    public void clearBusinessAddress() {
        if(profile == null)
            return;

        profile.clearBusinessAddress();
    }

    public void updateProfileLocally(ProfessionalProfile tempProfile) {
        if(profile == null)
            return;

        if(!Strings2.isNullOrWhiteSpace(tempProfile.getBusinessName()))
            profile.setBusinessName(tempProfile.getBusinessName());

        if(!Strings2.isNullOrWhiteSpace(tempProfile.getBusinessDescription()))
            profile.setBusinessDescription(tempProfile.getBusinessDescription());

        if(Validator.isValidEmail(tempProfile.getEmail()))
            profile.setEmail(tempProfile.getEmail());


        if(!Strings.isNullOrEmpty(tempProfile.getWebsite())){
            ValidWebsiteCriteria validWebsiteCriteria = new ValidWebsiteCriteria();
            if(validWebsiteCriteria.isSatisfiedBy(tempProfile.getWebsite()))
                profile.setWebsite(tempProfile.getWebsite());
        }

        if(!Lists.isNullOrEmpty(tempProfile.getCategories()))
            profile.setCategories(tempProfile.getCategories());

        profile.setLicenseNumber(tempProfile.getLicenseNumber());

    }

    public boolean confirmPolicy(String supplierId) {
        Call<StatusResponse> statusCall = professionalProjectsService.confirmPolicy(supplierId);
        boolean success = RetrofitCallProcessor.processStatusCall(statusCall);
        return success;
    }
}
;