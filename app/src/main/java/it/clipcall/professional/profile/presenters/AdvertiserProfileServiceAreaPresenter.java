package it.clipcall.professional.profile.presenters;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;

import com.google.common.base.Strings;

import org.simple.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ProfessionalEventNames;
import it.clipcall.common.models.UrlConstants;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.maps.entities.PlaceData;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.AddressResolver;
import it.clipcall.professional.profile.controllers.ProfessionalProfileController;
import it.clipcall.professional.profile.models.Area;
import it.clipcall.professional.profile.models.CoverArea;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.models.criterias.CanCompleteProfessionalRegistrationCriteria;
import it.clipcall.professional.profile.views.IAdvertiserProfileServiceAreaView;
import it.clipcall.professional.validation.services.RegistrationCompletedEvent;

@PerActivity
public class AdvertiserProfileServiceAreaPresenter extends PresenterBase<IAdvertiserProfileServiceAreaView> {

    private final RoutingService routingService;
    private final RouteParams routeParams;
    private final ProfessionalProfileController controller;
    private final AddressResolver addressResolver;


    private final int TermsOfServiceRequestCode = 100;



    @Inject
    public AdvertiserProfileServiceAreaPresenter(RoutingService routingService, RouteParams routeParams, ProfessionalProfileController controller, AddressResolver addressResolver) {
        this.routingService = routingService;
        this.routeParams = routeParams;
        this.controller = controller;
        this.addressResolver = addressResolver;
    }

    @Override
    public void initialize() {
        ProfessionalProfile profile = controller.getProfessionalProfile();
        if(profile == null)
            return;

        if(!Strings.isNullOrEmpty(profile.getBusinessAddress()) && profile.getBusinessAddress().equals("USA"))
            profile.clearBusinessAddress();
        view.showProfile(profile);
    }

    public void selectCoverArea(Area area){
        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationSelectCoverArea, area));
        boolean updatedSelectedArea = controller.selectArea(area);
        if(!updatedSelectedArea)
            return;

        ProfessionalProfile profile = controller.getProfessionalProfile();
        view.showCoverArea(profile.getCoverArea());
    }

    public void setBusinessLocationAddress(PlaceData place) {
        Map<String,String> property = new HashMap<String, String>();
        property.put("address", place.address);
        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationEnterBusinessAddress, property));
        Address address = addressResolver.resolveAddress(place.latLng);
        if(address == null)
            return;

        CoverArea coverArea = controller.updateProfileBusinessAddress(address);
        if(coverArea == null)
            return;

        view.showSelectedAddress(place.address);
        view.showCoverArea(coverArea);

    }

    public void nextButtonTapped() {
        boolean isValid = validate();
        if(!isValid){
            view.nextStageProcessingCompleted();
            return;
        }



        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.addParameter("requestCode",TermsOfServiceRequestCode+"");
        navigationContext.bundle = new Bundle();
        routeParams.setParam("url", UrlConstants.TermsOfServiceUrl);
        routeParams.setParam("title","");
        routeParams.setParam("showActions",true);
        routingService.routeTo(Routes.ProfessionalRegistrationTermsOfServiceAgreement2Route, navigationContext);
    }

    public boolean validate() {
        ProfessionalProfile profile = controller.getProfessionalProfile();
        if(profile == null)
            return false;

        CoverArea coverArea = profile.getCoverArea();
        if(coverArea == null)
            return false;

        int totalErrors = 0;
        boolean requestFocus;
        if(Strings.isNullOrEmpty(coverArea.getBusinessAddress())){
            totalErrors++;
            requestFocus = totalErrors == 1;
            view.showRequiredBusinessAddress(requestFocus);

        }
        if(coverArea.getSelectedArea() == null){
            totalErrors++;
            requestFocus = totalErrors == 1;
            view.showRequiredCoverArea(requestFocus);
        }

        CanCompleteProfessionalRegistrationCriteria criteria = new CanCompleteProfessionalRegistrationCriteria();
        if(!criteria.isSatisfiedBy(profile)){
            totalErrors++;
            view.showCompleteRegistrationErrors(criteria.getErrorMessages());
        }



        return totalErrors == 0;
    }

    public void clearBusinessAddress() {
        controller.clearBusinessAddress();
    }

    public void navigateToTermsOfServicePro() {
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        routingService.routeTo(Routes.ProfessoinalTermsOfServiceAgreementRoute, navigationContext);
    }


    public void handleActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != Activity.RESULT_OK)
            return;

        if(requestCode != TermsOfServiceRequestCode)
            return;


        boolean accepted = data.getBooleanExtra("accepted", false);
        if(!accepted)
        {
            view.nextStageProcessingCompleted();
            view.showPolicyConfirmationRequired(true);
            return;
        }

        boolean confirmed = controller.confirmPolicy();
        if(!confirmed){
            view.nextStageProcessingCompleted();
            view.showPolicyConfirmationRequired(true);
            return;
        }

        boolean registrationCompleted = controller.completeRegistration();
        if(!registrationCompleted ){
            view.showUpdatingProfileError();
            view.nextStageProcessingCompleted();
            return;
        }
        ProfessionalProfile profile = controller.getProfessionalProfile();

        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationCompleted));
        EventBus.getDefault().post(new RegistrationCompletedEvent(profile));
        view.completeProfileRegistration();
        view.nextStageProcessingCompleted();

    }
}
