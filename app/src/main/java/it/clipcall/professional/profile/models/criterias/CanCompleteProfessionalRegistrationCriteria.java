package it.clipcall.professional.profile.models.criterias;

import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import it.clipcall.infrastructure.support.criterias.ICriteria;
import it.clipcall.infrastructure.support.strings.Strings2;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.profile.models.CoverArea;
import it.clipcall.professional.profile.models.ProfessionalProfile;

/**
 * Created by dorona on 04/05/2016.
 */
public class CanCompleteProfessionalRegistrationCriteria implements ICriteria<ProfessionalProfile> {

    private final List<String> errorMessages = new ArrayList<>();


    @Override
    public boolean isSatisfiedBy(ProfessionalProfile candidate) {

        if(candidate == null)
        {
            errorMessages.add("Business profile is empty");
            return false;
        }

        CoverArea coverArea = candidate.getCoverArea();
        if(coverArea == null)
        {
            errorMessages.add("Cover area is missing");
            return false;
        }

        if(Strings2.isNullOrWhiteSpace(coverArea.getBusinessAddress())){
            errorMessages.add("Business address is required");
        }
        if(coverArea.getSelectedArea() == null){
            errorMessages.add("Cover area is required");
        }

        if(Strings2.isNullOrWhiteSpace(candidate.getBusinessName()))
        {
            errorMessages.add("Business name is required");
        }

        if(Strings2.isNullOrWhiteSpace(candidate.getEmail())){
            errorMessages.add("Email is required");
        }

        if(!isValidEmail(candidate.getEmail())){
            errorMessages.add("Invalid email address");
        }

        List<Category> categories = candidate.getCategories();
        if(categories == null || categories.size() == 0){
            errorMessages.add("You must select at least one category");
        }

        if(Strings2.isNullOrWhiteSpace(candidate.getProfileImageUrl())){
            errorMessages.add("Business profile image is requiredy");
        }
        if(candidate.hasLicenseNumber() && Strings2.isNullOrWhiteSpace(candidate.getLicenseNumber())){
            errorMessages.add("License number is requiredy");
        }

        if(Strings2.isNullOrWhiteSpace(candidate.getBusinessDescription())){
            errorMessages.add("Business description is required");
        }

        return errorMessages.size() == 0;
    }

    private boolean isValidEmail(String email){
        if(Strings.isNullOrEmpty(email))//email is optional
            return true;

        Matcher matcher = android.util.Patterns.EMAIL_ADDRESS.matcher(email);
        return matcher.matches();
    }

    public List<String> getErrorMessages(){
        return errorMessages;
    }
}
