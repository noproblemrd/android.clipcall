package it.clipcall.professional.profile.views;

import android.graphics.Bitmap;

import java.util.List;

import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.profile.models.Area;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.profile.models.ProfessionalProfile;

/**
 * Created by dorona on 08/05/2016.
 */
public interface IProfessionalEditProfileView  extends IView {
    void showProfile(ProfessionalProfile profile);

    void showSelectedAddress(String address);

    void showCoverArea(Area area);

    void showSelectedCategories(List<Category> selectedCategories);

    void showPickedImage(Bitmap image);

    void goBack();
}
