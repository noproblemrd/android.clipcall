package it.clipcall.professional.profile.support;

import android.support.v4.app.FragmentManager;

import java.util.ArrayList;
import java.util.List;

import it.clipcall.professional.profile.fragments.AdvertiserProfileAboutFragment_;
import it.clipcall.professional.profile.fragments.AdvertiserProfileDetailsFragment_;
import it.clipcall.professional.profile.fragments.AdvertiserProfileServiceAreaFragment_;
import it.clipcall.professional.profile.fragments.ProfileFragment;

/**
 * Created by micro on 1/25/2016.
 */
public class ProfilePagerAdapter {
    private final FragmentManager fragmentManager;

    private final List<ProfileFragment> fragments = new ArrayList<>();
    private int currentIndex = 0;

    public ProfilePagerAdapter(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;

        fragments.add(AdvertiserProfileDetailsFragment_.builder().build());
        fragments.add(AdvertiserProfileAboutFragment_.builder().build());
        fragments.add(AdvertiserProfileServiceAreaFragment_.builder().build());

        showCurrentFragment();
    }


    public boolean nextButtonTapped(){

       /* ProfileFragment profileFragment = fragments.get(currentIndex);
        profileFragment.nextButtonTapped();*/
        return false;

       /* if(currentIndex >= fragments.size()-1)
            return false;




        if(currentIndex != -1){
            ProfileFragment profileFragment = fragments.get(currentIndex);
            boolean isValid = profileFragment.validate();
            if(isValid == false)
                return false;

            profileFragment.saveChanges();
        }



        ++currentIndex;

        this.showCurrentFragment();
        return true;*/
    }

    private void showCurrentFragment(){
        /*FragmentTransaction transaction = fragmentManager.beginTransaction();
        Fragment fragment = fragments.get(currentIndex);
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.commit();*/
    }

    public boolean backButtonTapped(){

        /*ProfileFragment profileFragment = fragments.get(currentIndex);
        profileFragment.backButtonTapped();*/
        return false;
        /*if(currentIndex <= 0)
            return false;

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        Fragment fragment = fragments.get(--currentIndex);
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.commit();
        return true;*/

    }
}
