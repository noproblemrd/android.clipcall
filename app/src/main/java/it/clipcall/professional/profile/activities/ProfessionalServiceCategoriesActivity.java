package it.clipcall.professional.profile.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.consumer.findprofessional.support.filters.CategoryFilter;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.support.collections.Lists;

import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.profile.presenters.ProfessionalServiceCategoriesPresenter;
import it.clipcall.professional.profile.support.ICategoryListener;
import it.clipcall.professional.profile.support.adapters.ProfessionalCategoryAdapter;
import it.clipcall.professional.profile.views.IProfessionalServiceCategoriesView;

@EActivity(R.layout.professional_service_categories_activity)
public class ProfessionalServiceCategoriesActivity extends BaseActivity implements IHasToolbar, ICategoryListener, IProfessionalServiceCategoriesView {


    @ViewById
    RecyclerView proCategoriesRecyclerView;
    @ViewById
    Toolbar toolbar;

    @Extra
    Parcelable categories;

    @ViewById
    AutoCompleteTextView categoryAutoCompleteTextView;

    ProfessionalCategoryAdapter professionalCategoryAdapter;

    @Inject
    ProfessionalServiceCategoriesPresenter presenter;

    @Override
    public String getViewTitle() {
        return "SERVICE CATEGORIES";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        Intent data = new Intent();
        data.putExtra("categories", Parcels.wrap(this.presenter.getSelectedCategories()));
        setResult(RESULT_OK, data);
        finish();

        return true;
    }

    @Override
    public void onBackPressed() {
        onBackTapped();
    }

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        professionalCategoryAdapter = new ProfessionalCategoryAdapter(this, this);
        proCategoriesRecyclerView.setAdapter(professionalCategoryAdapter);
        proCategoriesRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        List<Category> selectedCategories = Parcels.unwrap(categories);
        presenter.setSelectedCategories(selectedCategories);
        presenter.initialize();
    }

    @Override
    public void onDeletingCategory(Category category) {
        presenter.deleteCategory(category);
    }

    @UiThread
    @Override
    public void showCategories(List<Category> categories) {
        if(Lists.isNullOrEmpty(categories))
            return;

        professionalCategoryAdapter.setItems(categories);
    }

    @UiThread
    @Override
    public void setAvailableCategories(final List<Category> availableCategories) {
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,new ArrayList<Category>()){
            private final Filter filter = new CategoryFilter(this, availableCategories);
            @Override
            public Filter getFilter() {
                return filter;
            }
        };

        categoryAutoCompleteTextView.setAdapter(adapter);
        categoryAutoCompleteTextView.setThreshold(1);
        categoryAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Category selectedCategory = (Category) categoryAutoCompleteTextView.getAdapter().getItem(position);
                addCategory(selectedCategory);
                categoryAutoCompleteTextView.setText("");
                InputMethodManager imm = (InputMethodManager)  getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(categoryAutoCompleteTextView.getWindowToken(), 0);
            }
        });
    }

    @Background
    void addCategory(Category category){
        presenter.addCategory(category);
    }
}
