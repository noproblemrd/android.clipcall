package it.clipcall.professional.profile.models;

/**
 * Created by dorona on 10/05/2016.
 */
public class ProfileData {

    public ProfileData(String profileImageUrl, String name){
        this.imageUrl = profileImageUrl;
        this.name = name;
    }
    public String imageUrl;
    public String name;
}
