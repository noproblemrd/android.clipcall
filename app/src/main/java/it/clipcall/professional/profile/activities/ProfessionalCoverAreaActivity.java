package it.clipcall.professional.profile.activities;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Parcelable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.parceler.Parcels;

import java.io.IOException;
import java.util.List;

import it.clipcall.R;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.support.collections.Collections3;
import it.clipcall.professional.profile.models.Area;
import it.clipcall.professional.profile.models.CoverArea;
import it.clipcall.professional.profile.support.adapters.CoverAreaAdapter;

@EActivity(R.layout.professional_cover_area_activity)
public class ProfessionalCoverAreaActivity extends BaseActivity implements OnMapReadyCallback, IHasToolbar {

    GoogleMap googleMap;

    @Extra
    Parcelable coverArea;

    CoverArea rawCoverArea;

    Circle coverAreaCircle;

    @FragmentById
    SupportMapFragment mapContainer;


    @ViewById
    Toolbar toolbar;

    Geocoder geocoder;

    CoverAreaAdapter coverAreaAdapter;

    @ViewById
    Spinner coverAreaSpinner;

    @ViewById
    TextView businessAddressTextView;

    @AfterViews
    void afterViews(){
        mapContainer.getMapAsync(this);
        coverAreaAdapter = new CoverAreaAdapter(this);
        coverAreaSpinner.setAdapter(coverAreaAdapter);
        coverAreaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Area coverArea = coverAreaAdapter.getItem(position);
                setCoverArea(coverArea);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        geocoder = new Geocoder(this);

        this.rawCoverArea = Parcels.unwrap(coverArea);


    }

    @UiThread
    void showInMap(){
        coverAreaAdapter.clear();
        coverAreaAdapter.addAll(rawCoverArea.getAreasInList());
        coverAreaAdapter.notifyDataSetChanged();
        businessAddressTextView.setText(rawCoverArea.getBusinessAddress());

        int index = rawCoverArea.getSelectedAreaIndex();
        coverAreaSpinner.setSelection(index);
        LatLng latLng = new LatLng(rawCoverArea.getLatitude(),rawCoverArea.getLongitude());
        double radiusInMeters = 100.0;
        int strokeColor = 0xffff0000; //red outline
        int shadeColor = 0x44ff0000; //opaque red fill

        Area selectedArea = rawCoverArea.getSelectedArea();
        if(selectedArea.getIsRange()){
            double miles = Double.parseDouble(selectedArea.getId());
            radiusInMeters = 1609.344 * miles;
        }
        else{
            String areaAddress = selectedArea.getName();
            if(!areaAddress.contains("United States"))
                areaAddress += ", USA";
            try {
                List<Address> addresses =  geocoder.getFromLocationName(areaAddress, 1);
                if(!Collections3.isNullOrEmpty(addresses)){
                    Address areaFullAddress = addresses.get(0);
                    latLng = new LatLng(areaFullAddress.getLatitude(),areaFullAddress.getLongitude());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        CircleOptions circleOptions = new CircleOptions().center(latLng).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
        googleMap.clear();
        coverAreaCircle = googleMap.addCircle(circleOptions);

        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, getZoomLevel());
        googleMap.animateCamera(yourLocation);
    }

    public int getZoomLevel() {
        double radius = coverAreaCircle.getRadius();
        double scale = radius / 100;
        int zoomLevel = (int) (16 - Math.log(scale) / Math.log(2));
        return zoomLevel;

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        showInMap();
        initialize();
    }

    @Background
    void initialize() {


    }

    @Background
    public void setCoverArea(Area coverArea) {
        rawCoverArea.setSelectedArea(coverArea);
        showInMap();
    }

    @Override
    public String getViewTitle() {
        return "COVER AREA";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        Intent data = new Intent();
        data.putExtra("area",Parcels.wrap(this.rawCoverArea.getSelectedArea()));
        setResult(RESULT_OK, data);
        finish();

        return true;
    }

    @Override
    public void onBackPressed() {
        onBackTapped();
    }
}
