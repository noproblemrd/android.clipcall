package it.clipcall.professional.profile.models;

import java.util.ArrayList;
import java.util.List;

public class CoverAreaCollection {

    private List<CoverArea> areas = new ArrayList<CoverArea>();

    /**
     *
     * @return
     * The areas
     */
    public List<CoverArea> getAreas() {
        return areas;
    }

    /**
     *
     * @param areas
     * The areas
     */
    public void setAreas(List<CoverArea> areas) {
        this.areas = areas;
    }

}