package it.clipcall.professional.profile.models;

/**
 * Created by dorona on 04/05/2016.
 */
public enum  eProfessionalProfileSection {

    All("All"),
    Details("Details"),
    About("About"),
    ServiceArea("ServiceArea");

    private final String name;

    eProfessionalProfileSection(String name){
        this.name = name;
    }

    public String toString() {
        return this.name;
    }
}
