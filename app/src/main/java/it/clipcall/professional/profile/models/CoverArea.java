package it.clipcall.professional.profile.models;

import android.location.Address;

import com.google.common.base.Predicate;

import org.parceler.Parcel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import it.clipcall.infrastructure.support.collections.Collections3;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.professional.profile.support.AddressAssist;

@Parcel
public class CoverArea implements Serializable {

    String businessAddress;
    double longitude;
    double latitude;
    int order;
    List<Area> areasInList = new ArrayList<Area>();


    /**
     *
     * @return
     * The businessAddress
     */
    public String getBusinessAddress() {
        return businessAddress;
    }

    /**
     *
     * @param businessAddress
     * The businessAddress
     */
    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    /**
     *
     * @return
     * The longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     * The longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     * The latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     * The latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     * The order
     */
    public int getOrder() {
        return order;
    }

    /**
     *
     * @param order
     * The order
     */
    public void setOrder(int order) {
        this.order = order;
    }

    /**
     *
     * @return
     * The areasInList
     */
    public List<Area> getAreasInList() {
        return areasInList;
    }

    /**
     *
     * @param areasInList
     * The areasInList
     */
    public void setAreasInList(List<Area> areasInList) {
        this.areasInList = areasInList;
    }


    @Override
    public String toString() {
        if(areasInList.isEmpty())
            return "";

        Area area = areasInList.get(0);
        return area.getName();
    }


    public Area getSelectedArea(){
        if(Lists.isNullOrEmpty(areasInList))
            return null;

        Area area = Collections3.getFirst(areasInList, new Predicate<Area>(){

            @Override
            public boolean apply(Area input) {
                return input.getSelected();
            }
        });

        return area;
    }

    public int getSelectedAreaIndex(){
        if(Collections3.isNullOrEmpty(areasInList))
            return -1;

        for(int i=0; i<areasInList.size(); ++i){
            Area area = areasInList.get(i);
            if(area.getSelected())
                return i;
        }

        return -1;
    }

    public void setAddress(Address address) {
        latitude = address.getLatitude();
        longitude = address.getLongitude();
        businessAddress = AddressAssist.getFormattedAddress(address);
    }

    public boolean setSelectedArea(Area selectedArea) {
        if(Collections3.isNullOrEmpty(areasInList))
            return false;
        for(int i=0; i<areasInList.size(); ++i){
            Area area = areasInList.get(i);
            if(area.equals(selectedArea))
                area.setSelected(true);
            else
                area.setSelected(false);
        }

        return true;

    }
}