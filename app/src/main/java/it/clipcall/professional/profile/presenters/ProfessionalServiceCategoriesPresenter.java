package it.clipcall.professional.profile.presenters;

import java.util.List;

import javax.inject.Inject;

import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.AddressResolver;
import it.clipcall.professional.profile.controllers.ProfessionalProfileController;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.profile.views.IProfessionalServiceCategoriesView;

@PerActivity
public class ProfessionalServiceCategoriesPresenter extends PresenterBase<IProfessionalServiceCategoriesView> {

    private final AddressResolver addressResolver;
    private final RoutingService routingService;
    private final ProfessionalProfileController controller;

    private List<Category> categories;


    @Inject
    public ProfessionalServiceCategoriesPresenter(AddressResolver addressResolver, RoutingService routingService, ProfessionalProfileController controller){
        this.addressResolver = addressResolver;
        this.routingService = routingService;
        this.controller = controller;
    }

    @Override
    public void initialize() {
         view.showCategories(categories);

        final List<Category> availableCategories = controller.getAvailableCategories();
        view.setAvailableCategories(availableCategories);
    }

    public void addCategory(Category category) {
        if(categories.contains(category))
            return;

        categories.add(category);

        view.showCategories(categories);
    }

    public void deleteCategory(Category category) {
        if(!categories.contains(category))
            return;

        categories.remove(category);
        view.showCategories(categories);

    }

    public List<Category> getSelectedCategories() {
        return this.categories;

    }

    @RunOnUiThread
    public void setSelectedCategories(List<Category> selectedCategories) {
        this.categories = selectedCategories;
    }
}
