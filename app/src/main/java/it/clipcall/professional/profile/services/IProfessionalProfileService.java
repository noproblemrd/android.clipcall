package it.clipcall.professional.profile.services;


import it.clipcall.consumer.profile.models.CustomerSettings;
import it.clipcall.consumer.profile.models.Logo;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.professional.profile.models.BusinessAddress;
import it.clipcall.professional.profile.models.CoverAreaCollection;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface IProfessionalProfileService {

    @GET("suppliers/{supplierId}/settings")
    Call<CustomerSettings> getSettings(@Path("supplierId") String supplierId);


    @POST("suppliers/{supplierId}/settings")
    Call<StatusResponse> updateSettings(@Path("supplierId") String supplierId, @Body CustomerSettings settings);


    @POST("suppliers/{supplierId}/profile")
    Call<StatusResponse> updateProfile(@Path("supplierId") String supplierId, @Body ProfessionalProfile profile, @Query("section") String section);

    @GET("suppliers/{supplierId}/profile")
    Call<ProfessionalProfile> getProfile(@Path("supplierId") String supplierId);

    @Multipart
    @POST("suppliers/{supplierId}/images")
    Call<Logo> uploadImage(@Path("supplierId")String supplierId, @Part("myfile\"; filename=\"audioData.png\" ") RequestBody file);

    @POST("suppliers/{supplierId}/coverareas/bussinessaddress")
    Call<CoverAreaCollection> updateProfileBusinessAddress(@Path("supplierId") String supplierId, @Body BusinessAddress businessAddress);
}
