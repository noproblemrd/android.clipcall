package it.clipcall.professional.profile.views;

import it.clipcall.infrastructure.views.IView;
import it.clipcall.professional.profile.models.ProfessionalProfile;

/**
 * Created by dorona on 18/01/2016.
 */
public interface IAdvertiserProfileView extends IView {

    void showProfile(ProfessionalProfile profile);

}
