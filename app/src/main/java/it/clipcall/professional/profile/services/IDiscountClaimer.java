package it.clipcall.professional.profile.services;

import it.clipcall.consumer.profile.models.FacebookProfile;

/**
 * Created by dorona on 08/06/2016.
 */
public interface IDiscountClaimer {

    boolean updateCustomerProfile(FacebookProfile facebookProfile);

    boolean claimDiscount();

    boolean isDiscountClaimed();
}
