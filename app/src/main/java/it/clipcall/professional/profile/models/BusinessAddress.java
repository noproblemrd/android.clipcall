package it.clipcall.professional.profile.models;

/**
 * Created by micro on 11/13/2015.
 */
public class BusinessAddress {

    private String businessAddress;

    private Double latitude;

    private Double longitude;

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
