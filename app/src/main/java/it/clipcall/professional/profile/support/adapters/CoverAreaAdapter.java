package it.clipcall.professional.profile.support.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import it.clipcall.R;
import it.clipcall.professional.profile.models.Area;
import it.clipcall.professional.profile.views.CoverAreaItemView;
import it.clipcall.professional.profile.views.CoverAreaItemView_;

/**
 * Created by omega on 2/26/2016.
 */
public class CoverAreaAdapter extends ArrayAdapter<Area> {
    public CoverAreaAdapter(Context context) {
        super(context, R.layout.professional_cover_area_item_view);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CoverAreaItemView itemView;
        if (convertView == null) {
            itemView = CoverAreaItemView_.build(getContext());
        } else {
            itemView = (CoverAreaItemView) convertView;
        }

        itemView.bind(getItem(position));

        return itemView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position,convertView,parent);
    }
}
