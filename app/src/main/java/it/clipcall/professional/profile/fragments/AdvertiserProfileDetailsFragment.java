package it.clipcall.professional.profile.fragments;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.common.models.Profile;
import it.clipcall.consumer.findprofessional.support.filters.CategoryFilter;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.presenters.AdvertiserProfileDetailsPresenter;
import it.clipcall.professional.profile.support.ICategoryListener;
import it.clipcall.professional.profile.support.adapters.ProfessionalCategoryAdapter;
import it.clipcall.professional.profile.views.IAdvertiserProfileDetailsView;


@EFragment(R.layout.advertiser_profile_details)
public class AdvertiserProfileDetailsFragment extends ProfileFragment implements IAdvertiserProfileDetailsView, ICategoryListener {
    @Inject
    AdvertiserProfileDetailsPresenter presenter;

    @ViewById
    EditText websiteEditText;

    @ViewById
    EditText emailEditText;

    @ViewById
    EditText businessNameEditText;

    @ViewById
    AutoCompleteTextView categoryAutoCompleteTextView;

    @ViewById
    RecyclerView proCategoriesListView;

    @ViewById
    ProgressBar businessNameProgressBar;

    @ViewById
    ProgressBar emailProgressBar;

    ProfessionalCategoryAdapter professionalCategoryAdapter;



    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        professionalCategoryAdapter = new ProfessionalCategoryAdapter(getContext(), this);
        proCategoriesListView.setAdapter(professionalCategoryAdapter);
        proCategoriesListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        presenter.initialize();
    }

    @Override
    protected String getTitle() {
        return "Profile";
    }

    @Override
    public RoutingContext getRoutingContext() {
        return null;
    }

    @Background
    @Override
    public void nextButtonTapped() {
        presenter.nextButtonTapped();
    }

    @Override
    public void backButtonTapped() {

    }

    @Override
    public boolean isInValidState() {
        return presenter.validate();
    }


    @Override
    public void setAvailableCategories(final List<Category> availableCategories) {
        ArrayAdapter adapter = new ArrayAdapter(this.getContext(), android.R.layout.simple_list_item_1,new ArrayList<Category>()){
            private final Filter filter = new CategoryFilter(this, availableCategories);
            @Override
            public Filter getFilter() {
                return filter;
            }
        };

        categoryAutoCompleteTextView.setAdapter(adapter);
        categoryAutoCompleteTextView.setThreshold(1);
        categoryAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Category selectedCategory = (Category) categoryAutoCompleteTextView.getAdapter().getItem(position);
                presenter.addCategory(selectedCategory);
                categoryAutoCompleteTextView.setText("");
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(categoryAutoCompleteTextView.getWindowToken(), 0);
            }
        });
    }


    @Override
    public void showAddedCategory(Category category) {
        professionalCategoryAdapter.add(category);
    }

    @UiThread
    @Override
    public void showRemovedCategory(Category category) {
        professionalCategoryAdapter.remove(category);
    }

    @UiThread
    @Override
    public void goToNextStage() {
        profileStagesLisenter.onNextStage();
    }

    @Override
    public void showProfile(Profile profile) {
        ProfessionalProfile professionalProfile = (ProfessionalProfile) profile;
        businessNameEditText.setText(professionalProfile.getBusinessName());
        emailEditText.setText(professionalProfile.getEmail());
        websiteEditText.setText(professionalProfile.getWebsite());
        ProfessionalCategoryAdapter adapter = (ProfessionalCategoryAdapter) proCategoriesListView.getAdapter();
        adapter.setItems(professionalProfile.getCategories());
    }

    @Override
    public void showRequiredBusinessName(boolean requestFocus) {
        businessNameEditText.setError("Business name is required");
        if(requestFocus){
            businessNameEditText.requestFocus();
        }
    }

    @Override
    public void showRequiredEmail(boolean requestFocus) {
        emailEditText.setError("Email address is required");
        if(requestFocus){
            emailEditText.requestFocus();
        }
    }

    @Override
    public void showInvalidEmail(boolean requestFocus) {
        emailEditText.setError("Email address is invalid");
        if(requestFocus){
            emailEditText.requestFocus();
        }
    }

    @Override
    public void showInvalidWebsite(boolean requestFocus) {
        websiteEditText.setError("Website is invalid");
        if(requestFocus){
            websiteEditText.requestFocus();
        }
    }

    @Override
    public void showRequiredCategories(boolean requestFocus) {
        categoryAutoCompleteTextView.setError("You must select at least one category");
        if(requestFocus){
            categoryAutoCompleteTextView.requestFocus();
        }
    }

    @TextChange(R.id.websiteEditText)
    void creditCardExpirationYearChange(CharSequence text, TextView hello, int before, int start, int count){
        if(text == null)
            return;

       presenter.setWebsite(text.toString());
    }

    @TextChange(R.id.emailEditText)
    void emailChange(CharSequence text, TextView hello, int before, int start, int count){
        if(text == null)
            return;

        presenter.setEmail(text.toString());
    }

    @TextChange(R.id.businessNameEditText)
    void businessNameChange(CharSequence text, TextView hello, int before, int start, int count){
        if(text == null)
            return;

        presenter.setBusinessName(text.toString());
    }

    @Override
    public void onDeletingCategory(Category category) {
        presenter.removeCategory(category);
    }

    @UiThread
    @Override
    public void nextStageProcessingCompleted() {
        profileStagesLisenter.onNextStageProcessingCompleted();
    }

    @UiThread
    @Override
    public void showUpdatingProfileError() {

    }

    @UiThread
    @Override
    public void showLoadingProfile() {
        if(businessNameProgressBar != null)
            businessNameProgressBar.setVisibility(View.VISIBLE);

        if(emailProgressBar != null)
            emailProgressBar.setVisibility(View.VISIBLE);
    }

    @UiThread
    @Override
    public void showProfileLoaded() {
        if(businessNameProgressBar != null)
            businessNameProgressBar.setVisibility(View.GONE);
        if(emailProgressBar != null)
            emailProgressBar.setVisibility(View.GONE);
    }
}
