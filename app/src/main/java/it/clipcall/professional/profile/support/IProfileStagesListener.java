package it.clipcall.professional.profile.support;

/**
 * Created by omega on 2/27/2016.
 */
public interface IProfileStagesListener {
    void onNextStage();

    void onNextStageProcessingCompleted();

    void onLastStage();

    void titleChanged(String title);


    void showError(String message);
}
