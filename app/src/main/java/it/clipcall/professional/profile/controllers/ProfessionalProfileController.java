package it.clipcall.professional.profile.controllers;

import android.location.Address;

import com.google.common.base.Strings;

import org.modelmapper.ModelMapper;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.services.MetadataProvider;
import it.clipcall.professional.profile.models.Area;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.profile.models.CoverArea;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.models.eProfessionalProfileSection;
import it.clipcall.professional.profile.services.ProfessionalManager;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by dorona on 18/01/2016.
 */
@Singleton
public class ProfessionalProfileController {

    private final AuthenticationManager authenticationManager;
    private final ProfessionalManager professionalManager;
    private final MetadataProvider metadataProvider;
    private final ModelMapper modelMapper;


    @Inject
    public ProfessionalProfileController(AuthenticationManager authenticationManager, ProfessionalManager professionalsManager, MetadataProvider metadataProvider, ModelMapper modelMapper) {
        this.authenticationManager = authenticationManager;
        this.professionalManager = professionalsManager;
        this.metadataProvider = metadataProvider;
        this.modelMapper = modelMapper;
    }



    public List<Category> getAvailableCategories() {
        return metadataProvider.getAvailableCategories();
    }

    public boolean addCategory(Category category) {
        return this.professionalManager.addCategory(category);
    }

    public boolean removeCategory(Category category) {
        return this.professionalManager.removeCategory(category);
    }

    public ProfessionalProfile getProfessionalProfile(){
        String supplierId = authenticationManager.getSupplierId();
        if(Strings.isNullOrEmpty(supplierId))
            return null;
        ProfessionalProfile profile = professionalManager.getProfessionalProfile(supplierId);
        return profile;
    }

    public String uploadImage(File file){
        String supplierId = authenticationManager.getSupplierId();
        String  url = professionalManager.uploadImage(supplierId, file);
        return url;
    }

    public CoverArea updateProfileBusinessAddress(Address address){
        String supplierId = authenticationManager.getSupplierId();
        CoverArea coverArea = professionalManager.updateProfileBusinessAddress(supplierId, address);
        return coverArea;
    }

    public boolean completeRegistration() {
        String supplierId = authenticationManager.getSupplierId();
        boolean updated = professionalManager.updateProfile(supplierId, eProfessionalProfileSection.All);
        if(!updated)
            return false;

        authenticationManager.registrationCompleted();
        return updated;
    }


    public void setWebsite(String website) {
        professionalManager.setWebsite(website);
    }

    public void setEmail(String email) {
        professionalManager.setEmail(email);
    }

    public void setBusinessName(String businessName) {
        professionalManager.setBusinessName(businessName);
    }

    public void setBusinessDescription(String businessDescription) {
        this.professionalManager.setBusinessDescription(businessDescription);
    }

    public void setHasLicenseNumber(boolean hasLicenseNumber) {
        if(!hasLicenseNumber){
            this.professionalManager.setLicenseNumber("");
        }

    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.professionalManager.setProfileImageUrl(profileImageUrl);
    }

    public void setLicenseNumber(String licenseNumber) {
        this.professionalManager.setLicenseNumber(licenseNumber);
    }

    public boolean selectArea(Area area) {
        return professionalManager.selectArea(area);
    }

    public boolean updateProfile(eProfessionalProfileSection section) {
        if(!authenticationManager.isProfessionalAuthenticated())
            return false;

        String supplierId = authenticationManager.getSupplierId();
        boolean profileUpdated = professionalManager.updateProfile(supplierId, section);
        return profileUpdated;

    }

    public boolean isProfessionalRegistered() {
        return authenticationManager.isProfessionalRegistered();

    }

    public void clearBusinessAddress() {
        professionalManager.clearBusinessAddress();
    }

    public void updateProfileLocally(ProfessionalProfile tempProfile) {
        professionalManager.updateProfileLocally(tempProfile);
    }

    public boolean confirmPolicy() {
        if(!authenticationManager.isProfessionalAuthenticated())
            return false;

        String supplierId = authenticationManager.getSupplierId();
        boolean success = professionalManager.confirmPolicy(supplierId);
        return success;
    }
}
