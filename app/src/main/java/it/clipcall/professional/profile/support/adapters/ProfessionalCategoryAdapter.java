package it.clipcall.professional.profile.support.adapters;

import android.content.Context;
import android.view.ViewGroup;

import it.clipcall.infrastructure.ui.lists.RecyclerViewAdapterBase;
import it.clipcall.infrastructure.ui.lists.ViewWrapper;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.profile.support.ICategoryListener;
import it.clipcall.professional.profile.views.ProfessionalCategoryItemView;
import it.clipcall.professional.profile.views.ProfessionalCategoryItemView_;

/**
 * Created by dorona on 09/03/2016.
 */
public class ProfessionalCategoryAdapter extends RecyclerViewAdapterBase<Category,ProfessionalCategoryItemView> {
    private final Context context;
    private final ICategoryListener categoryListener;

    public ProfessionalCategoryAdapter(Context context, ICategoryListener categoryListener) {
        this.context = context;
        this.categoryListener = categoryListener;
    }

    @Override
    protected ProfessionalCategoryItemView onCreateItemView(ViewGroup parent, int viewType) {
        ProfessionalCategoryItemView itemView = ProfessionalCategoryItemView_.build(context);
        return itemView;
    }

    @Override
    public void onBindViewHolder(ViewWrapper<ProfessionalCategoryItemView> holder, int position) {
        ProfessionalCategoryItemView view = holder.getView();
        Category historyItem = items.get(position);
        view.bind(historyItem, categoryListener);
    }


    /*@Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ProfessionalCategoryItemView itemView;
        if (convertView == null) {
            itemView = ProfessionalCategoryItemView_.build(getContext());
        } else {
            itemView = (ProfessionalCategoryItemView) convertView;
        }

        itemView.bind(getItem(position), categoryListener);

        return itemView;
    }*/
}
