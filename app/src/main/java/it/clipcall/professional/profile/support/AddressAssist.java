package it.clipcall.professional.profile.support;

import android.location.Address;
import android.text.TextUtils;

import java.util.ArrayList;

/**
 * Created by omega on 2/26/2016.
 */
public class AddressAssist {

    public static String getFormattedAddress(Address address){
        if(address == null)
            return "";

        ArrayList<String> addressFragments = new ArrayList<String>();
        for(int i = 0; i < address.getMaxAddressLineIndex(); i++) {
            addressFragments.add(address.getAddressLine(i));
        }
        String formattedAddress = TextUtils.join(", ",addressFragments);
        return formattedAddress;
    }
}
