package it.clipcall.professional.profile.fragments;

import it.clipcall.R;
import it.clipcall.professional.profile.support.IProfileStagesListener;
import it.clipcall.infrastructure.fragments.BaseFragment;

/**
 * Created by omega on 2/19/2016.
 */
public abstract class ProfileFragment extends BaseFragment {


    IProfileStagesListener profileStagesLisenter;

    public abstract void nextButtonTapped();

    public abstract void backButtonTapped();

    public void setProfileStagesListener(IProfileStagesListener profileStagesLisenter) {
        this.profileStagesLisenter = profileStagesLisenter;
    }

    @Override
    protected int getMenuItemId() {
        return R.id.nav_business_profile;
    }

    public abstract boolean isInValidState();
}
