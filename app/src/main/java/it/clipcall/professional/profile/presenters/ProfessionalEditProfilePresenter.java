package it.clipcall.professional.profile.presenters;

import android.graphics.Bitmap;
import android.location.Address;

import com.google.common.base.Strings;

import org.simple.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.ProfessionalEventNames;
import it.clipcall.infrastructure.commands.EmptyCallBack;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.maps.entities.PlaceData;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.AddressResolver;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.infrastructure.ui.images.services.ImagePickerService;
import it.clipcall.professional.profile.controllers.ProfessionalProfileController;
import it.clipcall.professional.profile.models.Area;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.profile.models.CoverArea;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.models.eProfessionalProfileSection;
import it.clipcall.professional.profile.views.IProfessionalEditProfileView;

@PerActivity
public class ProfessionalEditProfilePresenter extends PresenterBase<IProfessionalEditProfileView> {


    private final RouteParams routeParams;
    private final AddressResolver addressResolver;
    private final RoutingService routingService;
    private final ImagePickerService imagePickerService;
    private final ProfessionalProfileController controller;

    private ProfessionalProfile tempProfile;

    @Inject
    public ProfessionalEditProfilePresenter(RouteParams routeParams, AddressResolver addressResolver, RoutingService routingService, ImagePickerService imagePickerService, ProfessionalProfileController controller){
        this.routeParams = routeParams;
        this.addressResolver = addressResolver;
        this.routingService = routingService;
        this.imagePickerService = imagePickerService;
        this.controller = controller;
    }

    @Override
    public void initialize() {
        tempProfile = routeParams.getParam("profile");
        if(tempProfile == null)
            return;

        view.showProfile(tempProfile);
    }


    public void setBusinessLocationAddress(PlaceData place) {
        Map<String,String> property = new HashMap<>();
        property.put("address", place.address);
        EventBus.getDefault().post(new ApplicationEvent(ProfessionalEventNames.RegistrationEnterBusinessAddress, property));
        Address address = addressResolver.resolveAddress(place.latLng);
        if(address == null)
            return;

        CoverArea coverArea = controller.updateProfileBusinessAddress(address);
        if(coverArea == null)
            return;

        view.showSelectedAddress(place.address);
        view.showCoverArea(coverArea.getSelectedArea());

    }

    public void selectArea(Area area) {
        controller.selectArea(area);
        view.showCoverArea(area);
    }

    public CoverArea getCoverArea() {
        ProfessionalProfile professionalProfile = controller.getProfessionalProfile();
        return professionalProfile.getCoverArea();
    }

    public void pickServiceCategories() {
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        List<Category> categories = new ArrayList<>(this.tempProfile.getCategories());
        routeParams.reset();
        routeParams.setParam("categories",categories);
        routingService.routeTo(Routes.ProfessionalSelectServiceCategoriesRoute, navigationContext);
    }

    public void selectCategories(List<Category> selectedCategories) {

        if(Lists.isNullOrEmpty(selectedCategories))
            return;

        tempProfile.setCategories(selectedCategories);
        view.showSelectedCategories(selectedCategories);
    }

    public List<Category> getSelectedCategories() {
        return tempProfile.getCategories();
    }

    public void pickImage() {
        imagePickerService.pickImage(new EmptyCallBack<Bitmap>(){

            @Override
            public void onSuccess(Bitmap result) {
                imagePicked(result);
            }
        });
    }

    void imagePicked(final Bitmap image){
        view.showPickedImage(image);
    }

    public void uploadProfileLogo(File file) {
        if(file == null)
        {
            return;
        }
        String url = controller.uploadImage(file);
        if (Strings.isNullOrEmpty(url)) {
            return;
        }

        controller.setProfileImageUrl(url);
    }

    public void finishProfileEditing() {
        controller.updateProfileLocally(tempProfile);
        view.goBack();
        controller.updateProfile(eProfessionalProfileSection.All);
    }

    public void setBusinessName(String businessName) {
        if(this.tempProfile == null)
            return;

        tempProfile.setBusinessName(businessName);
    }
    public void setEmail(String email) {
        if(this.tempProfile == null)
            return;

        tempProfile.setEmail(email);
    }

    public void setWebsite(String website){
        if(this.tempProfile == null)
            return;

        tempProfile.setWebsite(website);
    }

    public void setLicenseNumber(String licenseNumber){
        if(this.tempProfile == null)
            return;

        tempProfile.setLicenseNumber(licenseNumber);
    }

    public void setBusinessDescription(String businessDescription){
        if(this.tempProfile == null)
            return;

        tempProfile.setBusinessDescription(businessDescription);
    }
}
