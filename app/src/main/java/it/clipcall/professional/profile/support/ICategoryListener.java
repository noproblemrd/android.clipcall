package it.clipcall.professional.profile.support;

import it.clipcall.professional.profile.models.Category;

/**
 * Created by dorona on 09/03/2016.
 */
public interface ICategoryListener {

    void onDeletingCategory(Category category);
}
