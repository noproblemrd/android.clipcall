package it.clipcall.professional.profile.models;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Created by omega on 2/26/2016.
 */
public class Business implements Serializable {
    private String name;
    private String email;
    private String website;
    private String referralCode;
    private List<Category> categories;
    private String logoUrl;
    private boolean hasLicense;
    private String licenseNumber;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public boolean isHasLicense() {
        return hasLicense;
    }

    public void setHasLicense(boolean hasLicense) {
        this.hasLicense = hasLicense;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean addCategory(final Category category) {
        Collection<Category> filteredCategories = Collections2.filter(this.categories, new Predicate<Category>() {
            @Override
            public boolean apply(Category input) {
                return input.equals(category);
            }
        });

        if(filteredCategories != null && filteredCategories.size() > 0)
            return false;

        return this.categories.add(category);
    }

    public boolean removeCategory(Category category){
        return this.categories.remove(category);
    }

    public Category findCategory(Category category){
        Collection<Category> categories = findCategories(category);
        if(categories != null && categories.size() == 1)
            return category;

        return null;
    }

    private Collection<Category> findCategories(final Category category){
        Collection<Category> filteredCategories = Collections2.filter(this.categories, new Predicate<Category>() {
            @Override
            public boolean apply(Category input) {
                return input.equals(category);
            }
        });

       return filteredCategories;
    }

}
