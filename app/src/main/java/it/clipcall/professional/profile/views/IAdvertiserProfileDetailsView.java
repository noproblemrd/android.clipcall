package it.clipcall.professional.profile.views;

import java.util.List;

import it.clipcall.common.models.Profile;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 18/01/2016.
 */
public interface IAdvertiserProfileDetailsView extends IView {
    void setAvailableCategories(final List<Category> availableCategories);

    void showAddedCategory(Category category);

    void showRemovedCategory(Category category);

    void goToNextStage();

    void showProfile(Profile profile);

    void showRequiredBusinessName(boolean requestFocus);

    void showRequiredEmail(boolean requestFocus);

    void showInvalidEmail(boolean requestFocus);

    void showRequiredCategories(boolean requestFocus);

    void nextStageProcessingCompleted();

    void showUpdatingProfileError();

    void showLoadingProfile();

    void showProfileLoaded();

    void showInvalidWebsite(boolean requestFocus);
}
