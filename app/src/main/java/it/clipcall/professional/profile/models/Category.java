package it.clipcall.professional.profile.models;

import org.parceler.Parcel;

import java.io.Serializable;

@Parcel
public class Category implements Serializable{

    String categoryId;
    String categoryName;
    String value;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.categoryName;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Category))
            return false;

        Category other = (Category)o;

        return this.categoryId.equals(other.categoryId);
    }


    @Override
    public int hashCode() {
        return this.categoryId.hashCode();
    }
}
