package it.clipcall.professional.profile.models;

import org.parceler.Parcel;

import java.io.Serializable;

@Parcel
public class Area implements Serializable {

    boolean selected;
    String name;
    String id;
    int listOrder;
    boolean isRange;

    /**
     *
     * @return
     * The selected
     */
    public boolean getSelected() {
        return selected;
    }

    /**
     *
     * @param selected
     * The selected
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The listOrder
     */
    public int getListOrder() {
        return listOrder;
    }

    /**
     *
     * @param listOrder
     * The listOrder
     */
    public void setListOrder(int listOrder) {
        this.listOrder = listOrder;
    }

    /**
     *
     * @return
     * The isRange
     */
    public Boolean getIsRange() {
        return isRange;
    }

    /**
     *
     * @param isRange
     * The isRange
     */
    public void setIsRange(Boolean isRange) {
        this.isRange = isRange;
    }

    @Override
    public boolean equals(Object o) {
        if( !(o instanceof Area))
            return false;

        Area other = (Area)o;
        return this.id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }
}