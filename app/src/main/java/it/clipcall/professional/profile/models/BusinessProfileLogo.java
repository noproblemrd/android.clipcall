package it.clipcall.professional.profile.models;

/**
 * Created by micro on 11/13/2015.
 */
public class BusinessProfileLogo {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
