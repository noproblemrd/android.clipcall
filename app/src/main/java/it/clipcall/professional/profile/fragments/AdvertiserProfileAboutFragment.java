package it.clipcall.professional.profile.fragments;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.common.base.Strings;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.DrawableRes;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.infrastructure.routing.models.FragmentDecoratorRoutingContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.presenters.AdvertiserProfileAboutPresenter;
import it.clipcall.professional.profile.views.IAdvertiserProfileAboutView;


@EFragment(R.layout.advertiser_profile_about)
public class AdvertiserProfileAboutFragment extends ProfileFragment implements IAdvertiserProfileAboutView{


    private Handler handler = new Handler(Looper.getMainLooper());
    @Inject
    AdvertiserProfileAboutPresenter presenter;

    @ViewById
    ImageButton profileImageButton;

    @ViewById
    CircularImageView profileCircularImageView;
    @ViewById
    EditText businessDescriptionEditText;

    @ViewById
    Switch haveLicenseSwitch;
    @ViewById
    EditText licenseNumberEditText;

    @ViewById
    ProgressBar uploadingProfileLogoProgressBar;

    @DrawableRes(R.drawable.profile_required_background_border)
    Drawable profileRequiredBackgroundBorder;

    @ViewById
    TextView requiredProfileImageTextView;

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        presenter.initialize();
    }

    @Click({R.id.profileImageButton, R.id.profileCircularImageView})
    void voidProfileImageTapped(){
       presenter.pickImage();
    }

    @Override
    protected String getTitle() {
        return "Profile";
    }

    @Override
    public RoutingContext getRoutingContext() {
        if(!presenter.isProfessionalRegistered()){
            return new RoutingContext(getActivity());
        }

        return new FragmentDecoratorRoutingContext(getActivity(), this);
    }

    @CheckedChange(R.id.haveLicenseSwitch)
    void checkedChangeOnHelloCheckBox(CompoundButton haveLicenseSwitch, boolean isChecked) {
        if(isChecked){
            licenseNumberEditText.setVisibility(View.VISIBLE);

        }else{
            licenseNumberEditText.setVisibility(View.GONE);
            licenseNumberEditText.setText("");
        }
        presenter.setHasLicense(isChecked);
    }

    @Override
    public void nextButtonTapped() {
        presenter.nextButtonTapped();
    }


    @Override
    public void backButtonTapped() {

    }


    @Override
    public boolean isInValidState() {
        return presenter.validate();
    }

    @TextChange(R.id.businessDescriptionEditText)
    void businessDescriptionChanged(CharSequence text, TextView hello, int before, int start, int count){
        if(text == null)
            return;

        presenter.setBusinessDescription(text.toString());
    }

    @TextChange(R.id.licenseNumberEditText)
    void licenseNumberChanged(CharSequence text, TextView hello, int before, int start, int count){
        if(text == null)
            return;

        presenter.setLicenseNumber(text.toString());
    }



    @Override
    public void showRequiredBusinessDescription(boolean requestFocus) {
        businessDescriptionEditText.setError("Business description is required");
        if(requestFocus){
            businessDescriptionEditText.requestFocus();
        }
    }

    @Override
    public void showRequiredLicenseNumber(boolean requestFocus) {
        licenseNumberEditText.setError("License number is required");
        if(requestFocus)
            licenseNumberEditText.requestFocus();
    }

    @Override
    public void showInvalidBusinessLogo() {

    }
    @UiThread
    @Override
    public void goToNextStage() {
        profileStagesLisenter.onNextStage();
    }

    @Override
    public void showProfile(final ProfessionalProfile profile) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                businessDescriptionEditText.setText(profile.getBusinessDescription());
                licenseNumberEditText.setText(profile.getLicenseNumber());
                haveLicenseSwitch.setChecked(profile.hasLicenseNumber());
                String profileImageUrl = profile.getProfileImageUrl();
                setProfileLogo(profileImageUrl);
            }
        }, 300);
    }

    private void setProfileLogo(String profileLogo){
        if(!Strings.isNullOrEmpty(profileLogo)){
            uploadingProfileLogoProgressBar.setVisibility(View.VISIBLE);
            profileCircularImageView.setVisibility(View.INVISIBLE);
            profileImageButton.setVisibility(View.INVISIBLE);

            Picasso.with(this.getContext()).load(profileLogo)
                    .into(profileCircularImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            profileImageButton.setVisibility(View.GONE);
                            uploadingProfileLogoProgressBar.setVisibility(View.GONE);
                            profileCircularImageView.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void onError() {
                            profileImageButton.setVisibility(View.VISIBLE);
                            uploadingProfileLogoProgressBar.setVisibility(View.GONE);
                            profileCircularImageView.setVisibility(View.GONE);
                        }
                    });
        }else{
            profileCircularImageView.setVisibility(View.GONE);
            profileImageButton.setVisibility(View.VISIBLE);
            uploadingProfileLogoProgressBar.setVisibility(View.GONE);
        }
    }


    @Override
    public void showRequiredProfileImage(boolean requestFocus) {
        //profileImageButton.setBackground(profileRequiredBackgroundBorder);
        requiredProfileImageTextView.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.Shake)
                .duration(700)
                .playOn(profileImageButton);
    }


    @Override
    public void profileLogoUploaded(String profileImageUrl) {
        requiredProfileImageTextView.setVisibility(View.INVISIBLE);
        setProfileLogo(profileImageUrl);
    }

    @UiThread
    @Override
    public void nextStageProcessingCompleted() {
        profileStagesLisenter.onNextStageProcessingCompleted();
    }

    @Override
    public void showUploadingProfileLogo() {
        profileCircularImageView.setVisibility(View.INVISIBLE);
        profileImageButton.setVisibility(View.INVISIBLE);
        uploadingProfileLogoProgressBar.setVisibility(View.VISIBLE);
    }


    @Override
    public void showUpdatingProfileError() {

    }

    @Override
    public void showUploadingProfileLogoError() {
        profileCircularImageView.setVisibility(View.GONE);
        profileImageButton.setVisibility(View.VISIBLE);
        //requiredProfileImageTextView.setVisibility(View.INVISIBLE);
        uploadingProfileLogoProgressBar.setVisibility(View.GONE);
        profileStagesLisenter.showError("Failed updating profile image. Please try a different image");
    }
}