package it.clipcall.professional.settings.fragments;

import android.widget.CompoundButton;
import android.widget.Switch;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.professional.settings.presenters.ProfessionalSettingsPresenter;
import it.clipcall.professional.settings.views.IProfessionalSettingsView;
import it.clipcall.infrastructure.fragments.BaseFragment;
import it.clipcall.infrastructure.routing.models.RoutingContext;

@EFragment(R.layout.professional_settings_fragmet)
public class ProfessionalSettingsFragment extends BaseFragment implements IProfessionalSettingsView {

    @Inject
    ProfessionalSettingsPresenter presenter;

    @ViewById
    Switch recordMyCallsSwitch;

    @AfterViews
    void afterAboutViewLoaded(){
        presenter.bindView(this);
        initialize();
    }

    @Background
    void initialize(){
        presenter.initialize();
    }

    @UiThread
    @Override
    public void setRecordMyCalls(boolean shouldRecordMyCalls) {
        recordMyCallsSwitch.setChecked(shouldRecordMyCalls);
    }

    @Override
    public RoutingContext getRoutingContext() {
        return null;
    }

    @CheckedChange(R.id.recordMyCallsSwitch)
    void toggleRecordMyCalls(CompoundButton recordMyCallSwitch, boolean isChecked){
        updateRecordMyCalls(isChecked);
    }

    @Background
    void updateRecordMyCalls(boolean shouldRecordMyCalls){
        presenter.updateRecordMyCalls(shouldRecordMyCalls);
    }

    @Override
    protected String getTitle() {
        return "Settings";
    }

    @Override
    protected int getMenuItemId() {
        return R.id.nav_settings;
    }
}
