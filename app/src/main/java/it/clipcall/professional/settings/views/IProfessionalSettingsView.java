package it.clipcall.professional.settings.views;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 17/01/2016.
 */
public interface IProfessionalSettingsView extends IView {

    void setRecordMyCalls(boolean shouldRecordMyCalls);
}
