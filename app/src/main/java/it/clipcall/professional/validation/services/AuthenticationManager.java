package it.clipcall.professional.validation.services;

import com.google.common.base.Strings;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.models.ApplicationModes;
import it.clipcall.common.models.UserEntity;
import it.clipcall.common.models.events.SwitchModeEvent;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.infrastructure.events.EventAggregator;
import it.clipcall.infrastructure.repositories.Repository2;
import it.clipcall.infrastructure.requests.LoginRequest;
import it.clipcall.infrastructure.requests.PhoneValidationRequest;
import it.clipcall.infrastructure.responses.AdvertiserData;
import it.clipcall.infrastructure.responses.LoginResponse;
import it.clipcall.infrastructure.support.network.RetrofitCallProcessor;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import retrofit2.Call;
import retrofit2.Response;

@Singleton
public class AuthenticationManager {

    private final IClipCallAuthenticationService authenticationService;
    private final EventAggregator eventAggregator;
    private final Repository2 repository;
    private final UserEntity user;

    @Inject
    public AuthenticationManager(IClipCallAuthenticationService authenticationService, EventAggregator eventAggregator, Repository2 repository, UserEntity user){
        this.authenticationService = authenticationService;
        this.eventAggregator = eventAggregator;
        this.repository = repository;
        this.user = user;
    }


    public boolean validatePhoneNumber(PhoneValidationRequest request) {
        Call<StatusResponse> validationCall = authenticationService.validate(request);
        boolean validated = RetrofitCallProcessor.processStatusCall(validationCall);
        return validated;
    }

    public boolean isCustomerAuthenticated(){
        return  user.getToken() != null && user.getCustomerId() != null;
    }

    public String getCustomerId(){
        return user.getCustomerId();
    }

    public String getSupplierId(){
        return user.getAdvertiserId();
    }

    public boolean isProfessionalAuthenticated(){
        return user.getToken() != null && user.getCustomerId() != null && user.getAdvertiserId() != null;
    }

    public boolean isProfessionalRegistered(){
        return  user.isRegistered();
    }

    public void registrationCompleted() {
        user.setIsRegistered(true);
        repository.update(user);
    }
    public boolean login(LoginRequest request){
        Call<LoginResponse> loginCall = this.authenticationService.login(request);
        LoginResponse loginResponse = RetrofitCallProcessor.processCall(loginCall);
        if(loginResponse == null)
            return false;

        user.setCustomerId(loginResponse.getCustomerId());
        user.setToken(loginResponse.getToken());
        user.setPhoneNumber(request.getPhoneNumber());
        repository.update(user);
        return true;
    }

    public boolean switchToProfessionalMode(){

        try{
            user.setMode(ApplicationModes.Professional);
            repository.update(user);

            String consumerId = user.getCustomerId();
            if(Strings.isNullOrEmpty(consumerId))
                return false;

            ProfessionalProfile profile = repository.getSingle(ProfessionalProfile.class);
            if(profile == null){
                profile = new ProfessionalProfile();
                repository.update(profile);
            }

            AdvertiserData advertiserData = user.getAdvertiserData();
            if(advertiserData == null){
                Response<AdvertiserData> response = this.authenticationService.switchToAdvertiser(consumerId).execute();
                if(response == null)
                    return false;
                advertiserData = response.body();
            }

            if(advertiserData == null)
                return false;

            user.switchToProfessionalMode(advertiserData);
            repository.update(user);
            eventAggregator.publish(new SwitchModeEvent(false,user.getAdvertiserId()));
            return true;

        }
        catch (IOException e){
            return false;
        }
    }

    public String getPhoneNumber() {
        return user.getPhoneNumber();
    }

    public boolean isInProfessionalMode() {
        return user.isInProfessionalMode();
    }

    public boolean inInConsumerMode() {
        return !user.isInProfessionalMode();
    }

    public boolean switchToConsumerMode() {
        user.switchToConsumerMode();
        repository.update(user);
        eventAggregator.publish(new SwitchModeEvent(true,user.getCustomerId()));
        return true;
    }

    public boolean updateToken(String token) {
         user.setToken(token);
        repository.update(user);
        return true;
    }

    public String getUserId(){
        return user.getUserId();
    }

    public String getMode() {
        return user.getMode();
    }
}
