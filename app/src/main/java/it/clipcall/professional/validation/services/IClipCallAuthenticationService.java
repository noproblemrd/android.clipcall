package it.clipcall.professional.validation.services;

import it.clipcall.common.models.entities.BusinessDescription;
import it.clipcall.infrastructure.StatusResponse;
import it.clipcall.infrastructure.requests.LoginRequest;
import it.clipcall.infrastructure.requests.PhoneValidationRequest;
import it.clipcall.infrastructure.responses.AdvertiserData;
import it.clipcall.infrastructure.responses.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by dorona on 09/11/2015.
 */
public interface IClipCallAuthenticationService {

    @POST("phonevalidation/validate")
    Call<StatusResponse> validate(@Body PhoneValidationRequest request);

    @POST("logon/customerlogon")
    Call<LoginResponse> login(@Body LoginRequest request);

    @POST("customers/{customerId}/switchtoadvertiser")
    Call<AdvertiserData> switchToAdvertiser(@Path("customerId") String  customerId);

    @GET("suppliers/{supplierId}/BusinessDescription")
    Call<BusinessDescription> getBusinessDescription(@Path("supplierId") String  supplierId);
}
