package it.clipcall.professional.validation.services;

import it.clipcall.professional.profile.models.ProfessionalProfile;

/**
 * Created by dorona on 19/04/2016.
 */
public class RegistrationCompletedEvent {
    private final ProfessionalProfile professionalProfile;

    public RegistrationCompletedEvent(ProfessionalProfile professionalProfile){
        this.professionalProfile = professionalProfile;
    }

    public ProfessionalProfile getProfile() {
        return professionalProfile;
    }
}
