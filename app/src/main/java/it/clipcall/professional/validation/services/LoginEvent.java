package it.clipcall.professional.validation.services;

/**
 * Created by dorona on 19/04/2016.
 */
public class LoginEvent {
    private String userId;

    public LoginEvent(String userId){
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
