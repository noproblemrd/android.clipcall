package it.clipcall.professional.mainMenu.support;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EView;
import org.androidannotations.annotations.UiThread;

import it.clipcall.ClipCallApplication;
import it.clipcall.R;
import it.clipcall.common.models.Urls;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.professional.mainMenu.views.INavigationView;

@EView
public class NavigationView extends android.support.design.widget.NavigationView implements INavigationView{

    @App
    ClipCallApplication application;

    ImageView logoImageView;

    TextView nameTextView;


    TextView toggleModeTextView;

    private String mode;
    private boolean isPostRegistration;

    public NavigationView(Context context) {
        this(context,null);
    }

    public NavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @AfterInject
    void afterInject(){
        if(isInEditMode())
            return;
        ((BaseActivity)getContext()).getActivityComponent().inject(this);

    }

    @AfterViews
    protected void afterViews() {

        if(isInEditMode())
            return;
        View headerView = getHeaderView(0);
        logoImageView = (ImageView) headerView.findViewById(R.id.logo_image_view);
        nameTextView = (TextView)headerView.findViewById(R.id.nameTextView);
        toggleModeTextView = (TextView)findViewById(R.id.toggleModeTextView);

         //clear old inflated items.

    }

    private void onModeChanged(){
        getMenu().clear();
        int menuResourceId;
        if("Consumer".equals(mode) ){
            String text = isPostRegistration ? "Service Pro mode" : "Service Pro? Join Now!";
            toggleModeTextView.setText(text);
            menuResourceId = R.menu.consumer_activity_main_menu_drawer;
        }else {
            toggleModeTextView.setText("Consumer");
            menuResourceId = R.menu.professional_main_menu_drawer;
        }
        inflateMenu(menuResourceId);
    }


    @Override
    public RoutingContext getRoutingContext() {
        return null;
    }

    @UiThread
    @Override
    public void setProfileImage(String url) {
        if(Strings.isNullOrEmpty(url)){
            url = Urls.ConsumerMissingProfileImageUrl;
        }
        Picasso.with(this.getContext()).load(url)
                .into(logoImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        View headerView = getHeaderView(0);
                        headerView.invalidate();
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    @UiThread
    @Override
    public void setName(String name) {
        if(Strings.isNullOrEmpty(name))
            return;

        if(nameTextView != null){
            nameTextView.setText(name);
            View headerView = getHeaderView(0);
            headerView.invalidate();
        }

    }

    public void setMode(String mode) {
        this.mode = mode;
        onModeChanged();
    }

    public void setIsPostRegistration(boolean isPostRegistration) {
        this.isPostRegistration = isPostRegistration;
    }

    public void setProfileImage(Bitmap image) {
        logoImageView.setImageBitmap(image);

      /*  Picasso.with(this.getContext()).load(file)
                .into(logoImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        View headerView = getHeaderView(0);
                        headerView.invalidate();
                    }

                    @Override
                    public void onError() {

                    }
                });*/
    }
}
