package it.clipcall.professional.mainMenu.support;

import android.support.design.widget.AppBarLayout;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

/**
 * Created by omega on 5/8/2016.
 */
public class ResizeViewOnOffsetChangedListener implements  AppBarLayout.OnOffsetChangedListener{

    private final View view;
    private static final float THRESHOLD_PERCENTAGE = 0.5F;
    private boolean isSmall;

    public ResizeViewOnOffsetChangedListener(View view){

        this.view = view;
    }
    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        float maxScroll = appBarLayout.getTotalScrollRange();
        Log.d("Foo","maxScroll: " + maxScroll);
        Log.d("Foo","verticalOffset: " + verticalOffset);
        Log.d("Foo","Math.abs(verticalOffset)/maxScroll: " + Math.abs(verticalOffset)/maxScroll);
        float progressPercentage = (float) (Math.abs(verticalOffset)/maxScroll);
        Log.d("Foo","progressPercentage: " + progressPercentage);

        if (progressPercentage >= THRESHOLD_PERCENTAGE  && !isSmall) {
            isSmall = true;
            ScaleAnimation fadeOut = new ScaleAnimation(1f,0.4f,1f,0.4f, Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF, 0.5f);
            TranslateAnimation translateAnimation = new TranslateAnimation(0,0,0,-50);
            AnimationSet set = new AnimationSet(true);
            set.addAnimation(fadeOut);
            set.addAnimation(translateAnimation);
            set.setDuration(200);
            view.startAnimation(set);
            set.setFillAfter(true);
            //start an alpha animation on your ImageView here (i.e. fade out)
        } else if(progressPercentage < THRESHOLD_PERCENTAGE && isSmall){

            isSmall = false;
            ScaleAnimation fadeOut = new ScaleAnimation(0.4f,1f,0.4f,1f, Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF,0.5f);
            fadeOut.setDuration(200);
            view.startAnimation(fadeOut);
            fadeOut.setFillAfter(true);
        }
    }

}
