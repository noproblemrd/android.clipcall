package it.clipcall.professional.mainMenu;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.design.internal.NavigationMenu;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.commit451.nativestackblur.NativeStackBlur;
import com.google.common.base.Strings;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import io.intercom.android.sdk.Intercom;
import it.clipcall.R;
import it.clipcall.consumer.mainMenu.support.FabContext;
import it.clipcall.consumer.mainMenu.support.IBadgeListener;
import it.clipcall.consumer.mainMenu.support.ICollapsingToolBarListener;
import it.clipcall.consumer.mainMenu.support.ITitleListener;
import it.clipcall.consumer.social.support.ShareWithFriendsListener;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.routing.models.FragmentRoutingContext;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.support.fragments.IHandlesBackPressed;
import it.clipcall.professional.mainMenu.presenters.ProfessionalMainMenuPresenter;
import it.clipcall.professional.mainMenu.support.ResizeViewOnOffsetChangedListener;
import it.clipcall.professional.mainMenu.views.IProfessionalMainMenuView;
import me.drakeet.materialdialog.MaterialDialog;
import me.kentin.yeti.Yeti;
import me.kentin.yeti.listener.OnShareListener;


@EActivity(R.layout.activity_main_menu)
public class MainMenuActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, ITitleListener, IProfessionalMainMenuView, IBadgeListener, ICollapsingToolBarListener {

    private static final int REQUEST_CODE_YETI = 10;
    private boolean doubleBackToExitPressedOnce = false;

    private final Handler handler = new Handler() ;

    private final  OnShareListener shareListener = new ShareWithFriendsListener(this);

    private  ResizeViewOnOffsetChangedListener resizeViewOnOffsetChangedListener;


    @ViewById
    Toolbar toolbar;

    @ViewById
    FloatingActionButton fab;

    @ViewById(R.id.rootContainer)
    DrawerLayout drawer;

    @ViewById(R.id.nav_view)
    it.clipcall.professional.mainMenu.support.NavigationView navigationView;

    @ViewById
    TextView badgeTextView;

    @ViewById
    ViewGroup badgeContainer;

    @ViewById
    CircularImageView circularImageView;

    @ViewById
    CollapsingToolbarLayout collapsingToolbarLayout;

    @ViewById
    ImageView blurredImageView;

    @ViewById
    FloatingActionButton editFab;

    @ViewById
    AppBarLayout appBarLayout;

    @Inject
    ProfessionalMainMenuPresenter presenter;

    @AfterViews
    protected void OnCreateCore(){
        presenter.bindView(this);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setMode("Professional");

        resizeViewOnOffsetChangedListener = new ResizeViewOnOffsetChangedListener(circularImageView);


        initialize();
    }

    @Background
    void initialize(){
        presenter.initialize();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.menu_fragment_container);
        if(fragment == null){
            executeDefaultBackPressedBehavior();
            return;
        }

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }

        if(!(fragment instanceof IHandlesBackPressed))
        {
            executeDefaultBackPressedBehavior();
            return;
        }

        IHandlesBackPressed handlesBackPressed = (IHandlesBackPressed) fragment;
        boolean handled = handlesBackPressed.handleBackPressed();
        if(handled)
            return;


        executeDefaultBackPressedBehavior();


    }

    private void executeDefaultBackPressedBehavior() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.rootContainer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.professional_additional_menu_items, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id == R.id.termsOfServiceMenuItem){
            presenter.routeToTermsOfService();
            return true;
        }

        selectNavigationItem(item);

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int menuItemId = item.getItemId();
        if(menuItemId == R.id.nav_rate_us_consumer){

            final MaterialDialog materialDialog = new MaterialDialog(this);
            materialDialog.setTitle("Rate us")
                    .setMessage("We'd really appreciate you taking the time to rate us and review our app in Google Play. Thanks!")
                    .setPositiveButton("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            materialDialog.dismiss();
                            NavigationContext navigationContext = new NavigationContext();
                            navigationContext.routingContext = new RoutingContext(MainMenuActivity.this);
                            presenter.routeTo(Routes.ConsumerRateUsRoute, navigationContext);
                        }
                    })
                    .setNegativeButton("CANCEL", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            materialDialog.dismiss();
                        }
                    });

            materialDialog.show();


            return true;
        }

        if(menuItemId == R.id.nav_share_with_friends_consumer){
            NavigationContext navigationContext = new NavigationContext();
            navigationContext.routingContext = new RoutingContext(MainMenuActivity.this);
            navigationContext.addParameter("requestCode",Integer.toString(REQUEST_CODE_YETI));
            presenter.routeTo(Routes.ConsumerShareWithFriends, navigationContext);
            return true;
        }

        if(menuItemId == R.id.support_menu_item){

            Intercom.client().displayConversationsList();
            return true;
        }

        selectNavigationItem(item);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.rootContainer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void selectNavigationItem(MenuItem item){
        int id = item.getItemId();
        String routeName = id + "";
        RoutingContext routingContext =  new FragmentRoutingContext(getSupportFragmentManager(),R.id.menu_fragment_container, this);
        NavigationContext navigationContext = new NavigationContext();
        navigationContext.routingContext = routingContext;
        presenter.routeTo(routeName, navigationContext);
    }

    @Background
    @Click(R.id.toggleModeTextView)
    void switchToConsumerMode(){
         presenter.switchToConsumerMode();
        //routingService.routeTo(Routes.SwitchToAdvertiserRoute, navigationContext);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_YETI) {
            Yeti yeti = Yeti.with(this);
            yeti.result(data, shareListener);
        }
    }

    @Override
    public void onItemActivated(int menuItemId) {
        if(navigationView == null)
            return;

        Menu menu = navigationView.getMenu();
        if(menu == null)
            return;

        MenuItem item = menu.findItem(menuItemId);
        if(item == null)
        {
            for(MenuItemImpl menuItem: ((NavigationMenu) menu).getNonActionItems()){
                menuItem.setChecked(false);
            }
            return;
        };

        item.setChecked(true);
        navigationView.invalidate();
    }

    @Override
    public void onTitleChanged(String newTitle) {
        ActionBar supportActionBar = getSupportActionBar();
        if(supportActionBar == null)
            return;

        supportActionBar.setTitle(newTitle.toUpperCase());
        collapsingToolbarLayout.setTitle(newTitle.toUpperCase());
    }

    @UiThread
    @Override
    public void setProfileImage(String url) {
        navigationView.setProfileImage(url);
    }

    @UiThread
    @Override
    public void setBusinessName(String name) {
        navigationView.setName(name);
    }

    @UiThread
    @Override
    public void showBadge(int value) {
        if(value <= 0){
            badgeContainer.setVisibility(View.GONE);
        }
        else{
            badgeContainer.setVisibility(View.VISIBLE);
            badgeTextView.setText(""+value);
        }
    }

    @UiThread
    @Override
    public void setBadgeVisibility(int visibility) {
        badgeContainer.setVisibility(visibility);
    }

    @Override
    public int getBadgeVisibility() {
        return badgeContainer.getVisibility();
    }
    @UiThread
    @Override
    public void showImage(String url) {

        if(Strings.isNullOrEmpty(url)){
            circularImageView.setVisibility(View.GONE);
            blurredImageView.setVisibility(View.GONE);
        }else{
            circularImageView.setVisibility(View.VISIBLE);
            blurredImageView.setVisibility(View.VISIBLE);
            Picasso.with(this).load(url).into(target);
        }

    }

    @UiThread
    @Override
    public void showImage(Bitmap bitmap) {
        if(bitmap == null){
            circularImageView.setVisibility(View.GONE);
            blurredImageView.setVisibility(View.GONE);
            return;
        }

        Drawable originalDrawable = new BitmapDrawable(getResources(),bitmap);
        Bitmap destination = NativeStackBlur.process(bitmap, 5);
        Drawable destinationDrawable = new BitmapDrawable(getResources(),destination);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            blurredImageView.setImageDrawable(destinationDrawable);
            circularImageView.setImageDrawable(originalDrawable);
        }
    }

    @Override
    public void setAction(FabContext context) {
        if(context == null)
        {
            if(editFab != null){
                editFab.setVisibility(View.GONE);
                editFab.setOnClickListener(null);
            }
           if(appBarLayout != null){
               appBarLayout.removeOnOffsetChangedListener(resizeViewOnOffsetChangedListener);
           }

            return;
        }

        editFab.setVisibility(View.VISIBLE);
        editFab.setImageDrawable(ContextCompat.getDrawable(this, context.drawableResourceId));
        editFab.setOnClickListener(context.listener);
        appBarLayout.addOnOffsetChangedListener(resizeViewOnOffsetChangedListener);
    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            BitmapDrawable drawable = new BitmapDrawable(getResources(),bitmap);
            Bitmap source = bitmap;
            Bitmap destination = NativeStackBlur.process(source, 50);
            Drawable destinationDrawable = new BitmapDrawable(getResources(),destination);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                blurredImageView.setImageDrawable(destinationDrawable);
                circularImageView.setImageDrawable(drawable);
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };


}
