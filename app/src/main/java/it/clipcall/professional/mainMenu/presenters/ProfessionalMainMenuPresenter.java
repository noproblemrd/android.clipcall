package it.clipcall.professional.mainMenu.presenters;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import javax.inject.Inject;

import it.clipcall.common.models.UrlConstants;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.professional.mainMenu.controllers.ProfessionalMainMenuController;
import it.clipcall.professional.mainMenu.views.IProfessionalMainMenuView;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.models.ProfileData;

@PerActivity
public class ProfessionalMainMenuPresenter extends PresenterBase<IProfessionalMainMenuView>{


    private final RoutingService routingService;
    private final RouteParams routeParams;
    private final ProfessionalMainMenuController controller;

    @Inject
    public ProfessionalMainMenuPresenter(RoutingService routingService, RouteParams routeParams, ProfessionalMainMenuController controller) {
        this.routingService = routingService;
        this.routeParams = routeParams;
        this.controller = controller;
        EventBus.getDefault().register(this);
    }

    @Override
    public void initialize() {
        super.initialize();
        ProfessionalProfile profile = this.controller.getProfessionalProfile();
        if(profile == null)
            return;

        view.setProfileImage(profile.getProfileImageUrl());
        view.setBusinessName(profile.getBusinessName());
    }

    public void routeTo(Route route, NavigationContext navigationContext) {
        routingService.routeTo(route, navigationContext);
    }

    public void routeTo(String routeName, NavigationContext navigationContext) {
        routingService.routeTo(routeName, navigationContext);
    }

    public void switchToConsumerMode() {
        boolean switched = controller.switchToConsumerMode();
        if(switched){
          /*  NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
            routingService.routeTo(Routes.ConsumerProjectsRoute, navigationContext);*/
            routingService.routeToHome(view.getRoutingContext());
            view.finish();
        }
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onProfileImageChanged(ProfileData profileData){
        view.setProfileImage(profileData.imageUrl);
        view.setBusinessName(profileData.name);
    }

    public void routeToTermsOfService() {
        routeParams.setParam("url", UrlConstants.TermsOfServiceUrl);
        routeParams.setParam("title","");
        routeParams.setParam("showActions",false);
        routingService.routeTo(Routes.ProfessionalTermsOfServiceAgreement2Route,new NavigationContext(view.getRoutingContext()));
    }
}
