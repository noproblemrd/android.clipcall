package it.clipcall.professional.mainMenu.controllers;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.profile.services.ProfessionalManager;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class ProfessionalMainMenuController {

    private final AuthenticationManager authenticationManager;
    private final ProfessionalManager professionalManager;
    @Inject
    public ProfessionalMainMenuController(AuthenticationManager authenticationManager, ProfessionalManager professionalManager){

        this.authenticationManager = authenticationManager;
        this.professionalManager = professionalManager;
    }
    public boolean switchToConsumerMode(){
        boolean switched = authenticationManager.switchToConsumerMode();
        return switched;
    }

    public ProfessionalProfile getProfessionalProfile(){
        if(!authenticationManager.isProfessionalAuthenticated())
            return null;

        String professionalId = authenticationManager.getSupplierId();
        ProfessionalProfile profile = professionalManager.getProfessionalProfile(professionalId);
        return profile;
    }
}
