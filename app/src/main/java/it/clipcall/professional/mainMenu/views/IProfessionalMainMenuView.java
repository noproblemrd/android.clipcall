package it.clipcall.professional.mainMenu.views;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 17/01/2016.
 */
public interface IProfessionalMainMenuView extends IView {


    void setProfileImage(String url);

    void setBusinessName(String name);

    void finish();
}
