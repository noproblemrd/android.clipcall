package it.clipcall.professional.invitecustomer.presenters;

import android.content.Context;

import javax.inject.Inject;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import it.clipcall.professional.invitecustomer.controllers.ProfessionalInviteCustomerController;
import it.clipcall.professional.invitecustomer.views.IProfessionalInviteCustomerView;
import it.clipcall.consumer.vidoechat.models.CustomerVideoInvitationData;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.models.NavigationContext;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RoutingService;

@PerActivity
public class ProfessionalInviteCustomerPresenter extends PresenterBase<IProfessionalInviteCustomerView> implements Branch.BranchLinkCreateListener{


    private final ProfessionalInviteCustomerController controller;

    private final RoutingService routingService;

    private CustomerVideoInvitationData currentInvitationData;

    @Inject
    public ProfessionalInviteCustomerPresenter(ProfessionalInviteCustomerController controller, RoutingService routingService) {
        this.controller = controller;
        this.routingService = routingService;
    }

    @Override
    public void initialize() {
        super.initialize();
    }


    public void inviteCustomer(Context context) {
        view.showCreateInvitationProcessing();
        currentInvitationData = controller.createInvitation();
        if(currentInvitationData == null){
            view.createInvitationProcessingCompleted();
            return;
        }

        controller.generateLink(context, currentInvitationData, this);
    }

    @Override
    public void onLinkCreate(String url, BranchError error) {
        if(error != null || currentInvitationData == null){
            view.createInvitationProcessingCompleted();
            return;
        }
        String message = currentInvitationData.getMessage();
        String finalMessage = message.replace("{0}", url);
        NavigationContext navigationContext = new NavigationContext(view.getRoutingContext());
        navigationContext.addParameter("message", finalMessage);
        routingService.routeTo(Routes.ProfessionalServiceCallInvitationBySmsRoute, navigationContext);
        view.createInvitationProcessingCompleted();
    }
}
