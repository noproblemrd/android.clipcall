package it.clipcall.professional.invitecustomer.views;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 17/01/2016.
 */
public interface IProfessionalInviteCustomerView extends IView {


    void showCreateInvitationProcessing();

    void createInvitationProcessingCompleted();
}
