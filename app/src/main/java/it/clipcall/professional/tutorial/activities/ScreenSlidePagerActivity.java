package it.clipcall.professional.tutorial.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import java.util.Timer;
import java.util.TimerTask;

import it.clipcall.R;
import it.clipcall.common.validation.activities.PhoneValidationActivity;
import it.clipcall.infrastructure.fragments.ScreenSlidePageFragment;
import it.clipcall.infrastructure.ViewPagerCustomDuration;
import it.clipcall.infrastructure.ZoomOutPageTransformer;


public class ScreenSlidePagerActivity extends FragmentActivity {

    private static final int NUM_PAGES = 5;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access backButtonTapped
     * and nextButtonTapped wizard steps.
     */
    private ViewPagerCustomDuration mPager;


    private Timer timer = new Timer(); // At this line a new Thread will be created

    private int page = 0;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_slide_pager);
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPagerCustomDuration) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        mPager.setScrollDurationFactor(4); // make the animation twice as slow

        mPager.setPageTransformer(true, new ZoomOutPageTransformer());

        timer.scheduleAtFixedRate(new RemindTask(), 1500, 2 * 1000); // delay


    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the backButtonTapped step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {



        private int[] imageFileNames = new int[]{
               /* R.drawable.app_loading_screen1,
                R.drawable.app_loading_screen4,
                R.drawable.app_loading_screen5,
                R.drawable.app_loading_screen6*/
        };


        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            int imageResourceId = imageFileNames[position];
            Fragment fragment = ScreenSlidePageFragment.newInstance(imageResourceId);
            return fragment;
        }

        @Override
        public int getCount() {
            return imageFileNames.length;
        }


    }


    private class RemindTask extends TimerTask {

        @Override
        public void run() {

            // As the TimerTask run on a seprate thread from UI thread we have
            // to call runOnUiThread to do work on UI thread.
            runOnUiThread(new Runnable() {
                public void run() {

                    if (page > 4) { // In my case the number of pages are 5
                        timer.cancel();
                        ScreenSlidePagerActivity.this.finish();
                        //Intent intent = new Intent(SplashActivity.this, LoginActivity_.class);
                        Intent intent = new Intent(ScreenSlidePagerActivity.this, PhoneValidationActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);

                    } else {
                        mPager.setCurrentItem(page++,true);
                    }
                }
            });
        }
    }
}
