package it.clipcall.professional.registration.views;

import it.clipcall.infrastructure.views.IView;

/**
 * Created by dorona on 24/01/2016.
 */
public interface IProfessionalRegistrationView extends IView{


    void showErrorMessage(String message);
}
