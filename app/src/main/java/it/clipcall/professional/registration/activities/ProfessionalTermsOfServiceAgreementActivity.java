package it.clipcall.professional.registration.activities;

import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FromHtml;
import org.androidannotations.annotations.ViewById;

import it.clipcall.R;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;

@EActivity(R.layout.professional_terms_of_service_agreement_activity)
public class ProfessionalTermsOfServiceAgreementActivity extends BaseActivity implements  IHasToolbar {

    @ViewById
    Toolbar toolbar;

    @AfterViews
    void afterViews(){
    }

    @ViewById
    @FromHtml(R.string.registration_tos_professional_work)
    TextView professionalWorkTextView;

    @ViewById
    @FromHtml(R.string.registration_tos_help_us)
    TextView helpUsLabelTextView;


    @ViewById
    @FromHtml(R.string.registration_tos_payment_title)
    TextView paymentTitleTextView;

    @Override
    public String getViewTitle() {
        return "TERMS OF SERVICE (PRO)";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        return false;
    }

    @Override
    public void onBackPressed() {
        onBackTapped();
    }
}
