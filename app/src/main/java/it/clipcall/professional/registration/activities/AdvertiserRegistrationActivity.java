package it.clipcall.professional.registration.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.google.android.gms.maps.MapView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import it.clipcall.R;
import it.clipcall.infrastructure.activities.BaseActivity;
import it.clipcall.infrastructure.activities.ui.IHasToolbar;
import it.clipcall.infrastructure.support.collections.IAction;
import it.clipcall.infrastructure.support.collections.Lists;
import it.clipcall.professional.profile.fragments.AdvertiserProfileAboutFragment_;
import it.clipcall.professional.profile.fragments.AdvertiserProfileDetailsFragment_;
import it.clipcall.professional.profile.fragments.AdvertiserProfileServiceAreaFragment_;
import it.clipcall.professional.profile.fragments.ProfileFragment;
import it.clipcall.professional.profile.support.IProfileStagesListener;
import it.clipcall.professional.registration.controllers.ProfessionalRegistrationPresenter;
import it.clipcall.professional.registration.views.IProfessionalRegistrationView;

@EActivity(R.layout.advertiser_registration_activity)
public class AdvertiserRegistrationActivity extends BaseActivity implements IHasToolbar, IProfessionalRegistrationView, IProfileStagesListener {

    private final List<ProfileFragment> fragments = new ArrayList<>();
    private int currentIndex = 0;

    @ViewById
    ProgressBar loadingSpinnerProgressBar;

    @ViewById
    ImageButton profileDetailsButtonImageView;

    @ViewById
    ImageButton profileAboutButtonImageView;

    @ViewById
    ImageButton profileServiceAreaButtonImageView;

    @Inject
    ProfessionalRegistrationPresenter presenter;

    @ViewById
    Toolbar toolbar;

    @ViewById
    FloatingActionButton nextFab;

    @ViewById
    CoordinatorLayout rootContainer;


    @Override
    public String getViewTitle() {
        return "Registration";
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onBackTapped() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null){
            currentIndex = savedInstanceState.getInt("currentIndex");
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MapView mv = new MapView(getApplicationContext());
                    mv.onCreate(null);
                    mv.onPause();
                    mv.onDestroy();
                }catch (Exception ignored){

                }
            }
        }).start();
    }

    @AfterViews
    void afterViews(){
        presenter.bindView(this);
        fragments.add(AdvertiserProfileDetailsFragment_.builder().build());
        fragments.add(AdvertiserProfileAboutFragment_.builder().build());
        fragments.add(AdvertiserProfileServiceAreaFragment_.builder().build());

        Lists.forEach(fragments, new IAction<ProfileFragment>() {
            @Override
            public void invoke(ProfileFragment input) {
                input.setRetainInstance(true);
            }
        });


        profileServiceAreaButtonImageView.setEnabled(true);
        profileAboutButtonImageView.setEnabled(true);
        profileDetailsButtonImageView.setEnabled(false);
        showCurrentFragment();
        initialize();
    }

    @Click(R.id.nextFab)
    void onNextTapped(){
        nextFab.setVisibility(View.GONE);
        loadingSpinnerProgressBar.setVisibility(View.VISIBLE);
        ProfileFragment profileFragment = fragments.get(currentIndex);
        profileFragment.setProfileStagesListener(this);
        profileFragment.nextButtonTapped();
    }

    public boolean handleBackPressed(){
        if(currentIndex <= 0)
            return false;

        ProfileFragment current = getCurrentProfileFragment();
        if(!current.isInValidState())
            return true;

        --currentIndex;
        showCurrentFragment();
        return true;
    }

    @Click(R.id.profileDetailsButtonImageView)
    void onProfileDetailsTapped(){
        currentIndex =0 ;
        showCurrentFragment();

    }

    @Click(R.id.profileAboutButtonImageView)
    void onProfileAboutTapped(){
        ProfileFragment current = getCurrentProfileFragment();
        if(currentIndex == 0 && !current.isInValidState())
            return;

        currentIndex =1 ;
        showCurrentFragment();
        return;
    }
    @Click(R.id.profileServiceAreaButtonImageView)
    void onProfileServiceAreaTapped(){
        int aboutSectionIndex = 2;
        if (!allOthersSectionsValid(aboutSectionIndex))
            return;

        currentIndex = 2 ;
        showCurrentFragment();
    }

    private void showCurrentFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        ProfileFragment fragment = fragments.get(currentIndex);
        fragment.setProfileStagesListener(this);
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.commit();
        //fragment.setProfileStagesListener(this);

        if(currentIndex == 0){
            profileDetailsButtonImageView.animate().scaleY(1.2f).scaleX(1.2f).start();
            profileDetailsButtonImageView.setEnabled(false);
            profileAboutButtonImageView.setEnabled(true);
            profileAboutButtonImageView.animate().scaleY(1f).scaleX(1f);
            profileServiceAreaButtonImageView.setEnabled(true);
            profileServiceAreaButtonImageView.animate().scaleY(1f).scaleX(1f);
        }
        else if(currentIndex == 1){
            profileDetailsButtonImageView.animate().scaleY(1f).scaleX(1f).start();
            profileDetailsButtonImageView.setEnabled(true);
            profileAboutButtonImageView.setEnabled(false);
            profileAboutButtonImageView.animate().scaleY(1.2f).scaleX(1.2f);
            profileServiceAreaButtonImageView.setEnabled(true);
            profileServiceAreaButtonImageView.animate().scaleY(1f).scaleX(1f);
        }else if(currentIndex == 2){
            profileDetailsButtonImageView.animate().scaleY(1f).scaleX(1f).start();
            profileDetailsButtonImageView.setEnabled(true);
            profileAboutButtonImageView.setEnabled(true);
            profileAboutButtonImageView.animate().scaleY(1f).scaleX(1f);
            profileServiceAreaButtonImageView.setEnabled(false);
            profileServiceAreaButtonImageView.animate().scaleY(1.2f).scaleX(1.2f);
        }

        if(currentIndex == 2){
            nextFab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_check_white_24dp));
        }else{
            nextFab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_forward_white_24dp));
        }
    }


    @Background
    void initialize(){
        presenter.initialize();
    }


    @Override
    public void onBackPressed() {
        if(currentIndex > 0){
            currentIndex--;
            showCurrentFragment();
            return;
        }
        else{
            presenter.backToConsumerHome();
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ProfileFragment profileFragment = fragments.get(currentIndex);
        profileFragment.onActivityResult(requestCode,resultCode,data);
    }

    public void onNextStage() {
        currentIndex++;
        showCurrentFragment();
    }

    public void onNextStageProcessingCompleted() {
        loadingSpinnerProgressBar.setVisibility(View.GONE);
        nextFab.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLastStage() {
        presenter.profileCompleted();
    }

    @Override
    public void titleChanged(String title) {

    }

    @Override
    public void showError(String message) {

    }

    private ProfileFragment getCurrentProfileFragment(){
        ProfileFragment profileFragment = fragments.get(currentIndex);
        return profileFragment;
    }

    private boolean allOthersSectionsValid(int sectionIndex){
        for(int i=0; i<fragments.size(); ++i)
        {
            if(sectionIndex == i)
                continue;

            ProfileFragment profileFragment = fragments.get(i);
            if(!profileFragment.isInValidState())
                return false;
        }

        return true;
    }

    @UiThread
    @Override
    public void showErrorMessage(String message) {
        Snackbar.make(rootContainer, message, Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        }).show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("currentIndex", currentIndex);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        currentIndex = savedInstanceState.getInt("currentIndex");
    }
}
