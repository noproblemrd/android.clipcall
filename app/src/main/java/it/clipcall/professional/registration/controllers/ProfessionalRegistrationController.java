package it.clipcall.professional.registration.controllers;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.services.MetadataProvider;
import it.clipcall.consumer.findprofessional.services.ProfessionalsManager;
import it.clipcall.professional.profile.models.Category;
import it.clipcall.professional.validation.services.AuthenticationManager;

/**
 * Created by dorona on 24/01/2016.
 */
@Singleton
public class ProfessionalRegistrationController {

    private final AuthenticationManager authenticationManager;
    private final ProfessionalsManager professionalsManager;
    private final MetadataProvider metadataProvider;



    @Inject
    public ProfessionalRegistrationController(ProfessionalsManager professionalsManager, AuthenticationManager authenticationManager, MetadataProvider metadataProvider) {
        this.professionalsManager = professionalsManager;
        this.authenticationManager = authenticationManager;
        this.metadataProvider = metadataProvider;
    }

    public List<Category> getAvailableCategories() {
        return metadataProvider.getAvailableCategories();
    }

    public void updateLastCategory(Category category) {
        professionalsManager.updateLastCategory(category);
    }

    public void switchToConsumerMode() {
        authenticationManager.switchToConsumerMode();
    }

    public boolean isProfessionalRegistered() {
        return authenticationManager.isProfessionalRegistered();
    }
}
