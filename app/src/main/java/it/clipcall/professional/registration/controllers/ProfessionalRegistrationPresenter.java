package it.clipcall.professional.registration.controllers;

import javax.inject.Inject;

import it.clipcall.infrastructure.aspects.RunOnUiThread;
import it.clipcall.professional.registration.views.IProfessionalRegistrationView;
import it.clipcall.infrastructure.di.scopes.PerActivity;
import it.clipcall.infrastructure.presenters.PresenterBase;
import it.clipcall.infrastructure.routing.services.RoutingService;

@PerActivity
public class ProfessionalRegistrationPresenter extends PresenterBase<IProfessionalRegistrationView> {
    protected final ProfessionalRegistrationController controller;
    protected final RoutingService routingService;

    @Inject
    public ProfessionalRegistrationPresenter(ProfessionalRegistrationController controller, RoutingService routingService) {
        this.controller = controller;
        this.routingService = routingService;
    }

    public boolean isProfessionalRegistered() {
        return controller.isProfessionalRegistered();
    }

    public void profileCompleted() {
        routingService.routeToHome(view.getRoutingContext());
    }

    @RunOnUiThread
    public void backToConsumerHome() {
        controller.switchToConsumerMode();
        routingService.routeToHome(view.getRoutingContext());
    }
}