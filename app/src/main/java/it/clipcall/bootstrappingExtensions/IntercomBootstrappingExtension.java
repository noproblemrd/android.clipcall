package it.clipcall.bootstrappingExtensions;

import android.app.Application;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import it.clipcall.common.environment.Environment;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.events.ConsumerProfileUpdatedEvent;
import it.clipcall.common.models.events.SwitchModeEvent;
import it.clipcall.consumer.profile.models.CustomerProfile;
import it.clipcall.consumer.profile.services.CustomerProfileManager;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.infrastructure.support.json.JsonBuilder;
import it.clipcall.professional.profile.models.ProfessionalProfile;
import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.professional.validation.services.LoginEvent;
import it.clipcall.professional.validation.services.RegistrationCompletedEvent;

/**
 * Created by dorona on 27/12/2015.
 */
@Singleton
public class IntercomBootstrappingExtension extends BootstrappingExtensionBase {

    private final AuthenticationManager authenticationManager;
    private final CustomerProfileManager profileManager;
    private final JsonBuilder jsonBuilder;

    private final ILogger logger = LoggerFactory.getLogger(IntercomBootstrappingExtension.class.getSimpleName());

    @Inject
    public IntercomBootstrappingExtension(Application application, AuthenticationManager authenticationManager, CustomerProfileManager profileManager, JsonBuilder jsonBuilder) {
        super(application);
        this.authenticationManager = authenticationManager;
        this.profileManager = profileManager;
        this.jsonBuilder = jsonBuilder;
    }

    @Override
    public void initialize() {
        if(Environment.AnalyticsEnabled)
        {
            logger.debug("initializing intercom bootstrapping extension");
            Intercom.initialize(application, "android_sdk-9703ae438c297238c644d178c77fb193a1d6a730", "qlzbpipv");
            EventBus.getDefault().register(this);
            String userId = authenticationManager.getCustomerId();
            if(Strings.isNullOrEmpty(userId)){
                logger.debug("initializing intercom with unidentified user");
                Intercom.client().registerUnidentifiedUser();
            }else{
                logger.debug("initializing intercom with identified user with user id  %s",userId);
                Intercom.client().registerIdentifiedUser(new Registration().withUserId(userId));
            }

        }
    }

    @Override
    public String toString() {
        return "Intercom bootstrapping extension";
    }


    @Subscriber(mode = ThreadMode.ASYNC)
    void onLogin(LoginEvent event){

        logger.debug("Received 'Login' event");

        String userId = authenticationManager.getUserId();
        if(!Strings.isNullOrEmpty(userId))
        {
            Map userAttributes = new HashMap();
            userAttributes.put("name", authenticationManager.getPhoneNumber());
            userAttributes.put("loginDate", new Date());
            userAttributes.put("phoneNumber", authenticationManager.getPhoneNumber());
            userAttributes.put("_APP_LOCALPHONE", authenticationManager.getPhoneNumber());
            userAttributes.put("mode", authenticationManager.getMode());
            userAttributes.put("_APP_CUSTOMERID", authenticationManager.getCustomerId());
            logger.debug("Updating intercom user with login details %s",userAttributes);

            Registration registration = Registration.create().withUserId(userId).withUserAttributes(userAttributes);
            Intercom.client().registerIdentifiedUser(registration);

            logger.debug("Updated intercom user with phone number %s",authenticationManager.getPhoneNumber());
        }
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onAppEvent(ApplicationEvent applicationEvent){
        logger.debug("Received application event - %s. Updating intercom with the event", applicationEvent.getName());
        Map<String, Object> properties = jsonBuilder.buildMap(applicationEvent.getData());
        Intercom.client().logEvent(applicationEvent.getName(),properties);
    }


    @Subscriber(mode = ThreadMode.ASYNC)
    void onRegistrationCompleted(RegistrationCompletedEvent event){
        String userId = authenticationManager.getSupplierId();
        ProfessionalProfile profile = event.getProfile();
        Map<String,Object> userAttributes = new HashMap<>();
        userAttributes.put("_APP_PRO_LOCALPHONE", authenticationManager.getPhoneNumber());
        userAttributes.put("name", profile.getBusinessName());
        userAttributes.put("email", profile.getEmail());
        userAttributes.put("phoneNumber", authenticationManager.getPhoneNumber());
        userAttributes.put("_APP_PRO_BUSINESS_ADDRESS", profile.getBusinessAddress());
        userAttributes.put("_APP_PRO_BUSINESS_DESCRIPTION",profile.getBusinessDescription());
        userAttributes.put("_APP_PRO_BUSINESS_NAME",profile.getBusinessName());
        userAttributes.put("_APP_PRO_PROFILE_IMAGE_URL",profile.getProfileImageUrl());
        userAttributes.put("_APP_PRO_EMAIL",profile.getEmail());
        userAttributes.put("_APP_PRO_CATEGORIES",Joiner.on(",").join(profile.getCategories()));
        userAttributes.put("_APP_PROFESSIONAL_ID", authenticationManager.getSupplierId());
        logger.debug("Updating intercom with the event - RegistrationCompleted for user with id %s",userId);
        Intercom.client().updateUser(userAttributes);
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onSwitchMode(SwitchModeEvent event){
        Registration.create().withUserId(event.getUserId());
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onConsumerProfileUpdated(ConsumerProfileUpdatedEvent event){

        CustomerProfile customerProfile = event.getCustomerProfile();
        Map userAttributes = new HashMap();
        if(!Strings.isNullOrEmpty(customerProfile.getName()))
            userAttributes.put("name", customerProfile.getName());

        if(!Strings.isNullOrEmpty(customerProfile.getEmail()))
        {
            userAttributes.put("email", customerProfile.getEmail());
            userAttributes.put("_APP_EMAIL", customerProfile.getEmail());
        }

        if(!Strings.isNullOrEmpty(customerProfile.getImageUrl()))
            userAttributes.put("_APP_CONSUMER_IMAGE_URL", customerProfile.getImageUrl());


        Intercom.client().updateUser(userAttributes);
    }
}
