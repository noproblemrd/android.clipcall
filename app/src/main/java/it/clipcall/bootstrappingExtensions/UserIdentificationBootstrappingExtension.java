package it.clipcall.bootstrappingExtensions;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.bootstrappingExtensions.IBootstrappingExtension;
import it.clipcall.infrastructure.repositories.Repository2;


@Singleton
public class UserIdentificationBootstrappingExtension  implements IBootstrappingExtension {

    private final Repository2 repository;

    @Inject
    public UserIdentificationBootstrappingExtension(Repository2 repository) {
        this.repository = repository;
    }

    @Override
    public void initialize() {

       /* UserEntity user = repository.getSingle(UserEntity.class);
        if(user == null){
            user = new UserEntity();
            user.setMode(ApplicationModes.Consumer);
            repository.update(user);
        }

        UserRef userRef = repository.getSingle(UserRef.class);
        if(userRef == null){
            UUID userId = UUID.randomUUID();
            userRef = new UserRef();
            userRef.setUserId(UUID.randomUUID().toString());
            repository.add(userRef);
        }*/
    }
}
