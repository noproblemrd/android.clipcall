package it.clipcall.bootstrappingExtensions;

import android.app.Application;

import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;

@Singleton
public class ObjectMapperBootstrappingExtension extends BootstrappingExtensionBase {


    private final ModelMapper modelMapper;

    @Inject
    public ObjectMapperBootstrappingExtension(Application application, ModelMapper modelMapper) {
        super(application);
        this.modelMapper = modelMapper;
    }

    @Override
    public void initialize() {

        //modelMapper.addMappings(new AddressToAddressEntityMap());
        //modelMapper.addMappings(new AddressEntityToAddressMap());
    }

    @Override
    public String toString() {
        return "Object mapper bootstrapping extension";
    }
}
