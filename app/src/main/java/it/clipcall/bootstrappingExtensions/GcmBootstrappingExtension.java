package it.clipcall.bootstrappingExtensions;

import android.app.Application;
import android.os.AsyncTask;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;
import it.clipcall.infrastructure.gcm.services.GcmTokenProvider;

@Singleton
public class GcmBootstrappingExtension extends BootstrappingExtensionBase {

    private final GcmTokenProvider gcmTokenProvider;

    @Inject
    public GcmBootstrappingExtension(Application application, GcmTokenProvider gcmTokenProvider) {
        super(application);
        this.gcmTokenProvider = gcmTokenProvider;
    }

    @Override
    public void initialize() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                gcmTokenProvider.initialize(application.getApplicationContext());
            }
        });
    }

    @Override
    public String toString() {
        return "Gcm bootstrapping extension";
    }
}