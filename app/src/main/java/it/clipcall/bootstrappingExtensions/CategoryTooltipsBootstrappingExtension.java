package it.clipcall.bootstrappingExtensions;

import android.app.Application;
import android.content.res.Resources;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.R;
import it.clipcall.consumer.findprofessional.services.ProfessionalsManager;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;

/**
 * Created by dorona on 27/12/2015.
 */
@Singleton
public class CategoryTooltipsBootstrappingExtension extends BootstrappingExtensionBase {
    private final ProfessionalsManager professionalsManager;

    @Inject
    public CategoryTooltipsBootstrappingExtension(Application application,ProfessionalsManager professionalsManager) {
        super(application);
        this.professionalsManager = professionalsManager;
    }



    @Override
    public void initialize() {
        Resources res = application.getResources();
        String[] categoryTips = res.getStringArray(R.array.default_category_tips);
        professionalsManager.setDefaultCategoryTips(categoryTips);
    }

    @Override
    public String toString() {
        return "Default category tooltips bootstrapping extension";
    }
}
