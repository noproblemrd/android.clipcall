package it.clipcall.bootstrappingExtensions;

import android.app.Application;

import com.appsflyer.AppsFlyerLib;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.environment.Environment;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;

/**
 * Created by dorona on 27/12/2015.
 */
@Singleton
public class AppsFlyerBootstrappingExtension extends BootstrappingExtensionBase {

    @Inject
    public AppsFlyerBootstrappingExtension(Application application) {
        super(application);
    }

    @Override
    public void initialize() {

        if(Environment.AnalyticsEnabled)
        {
            AppsFlyerLib.setAppsFlyerKey("pbTWqAYT3pajYzBnynUJWe");
        }
    }

    @Override
    public String toString() {
        return "Apps Flyer bootstrapping extension";
    }
}
