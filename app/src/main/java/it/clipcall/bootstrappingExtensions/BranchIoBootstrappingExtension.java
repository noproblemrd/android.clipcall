package it.clipcall.bootstrappingExtensions;

import android.app.Application;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.branch.referral.Branch;
import it.clipcall.common.environment.Environment;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;

/**
 * Created by dorona on 27/12/2015.
 */
@Singleton
public class BranchIoBootstrappingExtension extends BootstrappingExtensionBase {

    @Inject

    public BranchIoBootstrappingExtension(Application application) {
        super(application);
    }

    @Override
    public void initialize() {
        if(Environment.AnalyticsEnabled){
            Branch.getAutoInstance(application);
        }

    }

    @Override
    public String toString() {
        return "Branch io bootstrapping extension";
    }
}
