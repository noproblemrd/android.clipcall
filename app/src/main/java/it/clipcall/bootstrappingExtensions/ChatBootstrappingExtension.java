package it.clipcall.bootstrappingExtensions;

import android.app.Application;
import android.util.Log;

import com.google.common.base.Strings;
import com.layer.sdk.LayerClient;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.chat.models.ChatCreatedEvent;
import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.common.chat.support.factories.ChatMessageFactoryProvider;
import it.clipcall.common.models.GcmToken;
import it.clipcall.consumer.projects.services.ProjectsManager;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;
import it.clipcall.professional.leads.services.ProfessionalProjectsManager;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class ChatBootstrappingExtension extends BootstrappingExtensionBase {

    private static final String TAG = "ChatBootstrapping";
    private final ChatManager chatManager;
    private final AuthenticationManager authenticationManager;
    private final ProfessionalProjectsManager professionalProjectsManager;
    private final ProjectsManager projectsManager;
    private final ChatMessageFactoryProvider chatMessageFactoryProvider;

    @Inject
    public ChatBootstrappingExtension(Application application, ChatManager chatManager, AuthenticationManager authenticationManager, ProfessionalProjectsManager professionalProjectsManager, ProjectsManager projectsManager, ChatMessageFactoryProvider chatMessageFactoryProvider) {
        super(application);
        this.chatManager = chatManager;
        this.authenticationManager = authenticationManager;
        this.professionalProjectsManager = professionalProjectsManager;
        this.projectsManager = projectsManager;
        this.chatMessageFactoryProvider = chatMessageFactoryProvider;
    }

    @Override
    public void initialize() {
        LayerClient.applicationCreated(application);
        EventBus.getDefault().register(this);
    }

    @Override
    public String toString() {
        return "Layer chat bootstrapping extension";
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onDeviceTokenChanged(GcmToken gcmToken){

        Log.d(TAG, "onDeviceTokenChanged: new token - "+ gcmToken.value);
        String customerId = authenticationManager.getCustomerId();
        if(Strings.isNullOrEmpty(customerId))
            return;

        chatManager.setGcmRegistrationId(customerId, gcmToken.value);
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onChatCreated(ChatCreatedEvent createdEvent){
        if(authenticationManager.isInProfessionalMode()){
            Log.d(TAG, "onChatCreated - professional mode");
            professionalProjectsManager.setChatCreated(authenticationManager.getSupplierId(),createdEvent.getLeadAccountId());
        }else{
            Log.d(TAG, "onChatCreated - customer mode");
            projectsManager.setChatCreated(authenticationManager.getCustomerId(), createdEvent.getLeadAccountId());
        }
    }
}
