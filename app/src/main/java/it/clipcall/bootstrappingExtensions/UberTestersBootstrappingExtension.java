package it.clipcall.bootstrappingExtensions;

import android.app.Application;

import com.ubertesters.sdk.Ubertesters;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.environment.Environment;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;

/**
 * Created by dorona on 27/12/2015.
 */
@Singleton
public class UberTestersBootstrappingExtension extends BootstrappingExtensionBase {

    @Inject
    public UberTestersBootstrappingExtension(Application application) {
        super(application);
    }


    @Override
    public void initialize() {

       /* if(Environment.IsDebug)
        {
            Ubertesters.initialize(application);
            Ubertesters.disableCrashHandler();
        }*/
    }

    @Override
    public String toString() {
        return "Uber Testers bootstrapping extension";
    }




}
