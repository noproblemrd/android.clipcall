package it.clipcall.bootstrappingExtensions;

import android.app.Application;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;

@Singleton
public class RepositoryBootstrappingExtension extends BootstrappingExtensionBase {

    @Inject
    public RepositoryBootstrappingExtension(Application application) {
        super(application);
    }

    @Override
    public void initialize() {

    }

    @Override
    public String toString() {
        return "Repository bootstrapping extension";
    }
}
