package it.clipcall.bootstrappingExtensions;

import android.app.Application;

import javax.inject.Inject;
import javax.inject.Singleton;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import it.clipcall.common.activities.SplashActivity_;
import it.clipcall.consumer.findprofessional.services.ProfessionalsManager;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;

/**
 * Created by dorona on 27/12/2015.
 */
@Singleton
public class ApplicationCrashBootstrappingExtension extends BootstrappingExtensionBase {


    @Inject
    public ApplicationCrashBootstrappingExtension(Application application, ProfessionalsManager professionalsManager) {
        super(application);

    }



    @Override
    public void initialize() {

        CustomActivityOnCrash.setRestartActivityClass(SplashActivity_.class);
        CustomActivityOnCrash.setEnableAppRestart(true);
        CustomActivityOnCrash.setShowErrorDetails(true);
        CustomActivityOnCrash.install(application);
    }

    @Override
    public String toString() {
        return "Application crash bootstrapping extension";
    }
}
