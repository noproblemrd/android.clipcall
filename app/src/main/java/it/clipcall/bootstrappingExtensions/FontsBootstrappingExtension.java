package it.clipcall.bootstrappingExtensions;

import android.app.Application;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.R;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
@Singleton
public class FontsBootstrappingExtension extends BootstrappingExtensionBase {

    @Inject
    public FontsBootstrappingExtension(Application application) {
        super(application);
    }

    @Override
    public void initialize() {
//        TypefaceUtil.overrideFont(application.getApplicationContext(), "SERIF", "fonts/ProximaNova-Regular.otf"); // font from assets: "assets/fonts/Roboto-Regular.ttf
//        TypefaceUtil.overrideFont(application.getApplicationContext(), "SANS_SERIF", "fonts/ProximaNova-Regular.otf"); // font from assets: "assets/fonts/Roboto-Regular.ttf
//        TypefaceUtil.overrideFont(application.getApplicationContext(), "MONOSPACE", "fonts/ProximaNova-Regular.otf"); // font from assets: "assets/fonts/Roboto-Regular.ttf
//        TypefaceUtil.overrideFont(application.getApplicationContext(), "DEFAULT", "fonts/ProximaNova-Regular.otf"); // font from assets: "assets/fonts/Roboto-Regular.ttf
//        TypefaceUtil.overrideFont(application.getApplicationContext(), "MONOSPACE", "fonts/ProximaNova-Regular.otf"); // font from assets: "assets/fonts/Roboto-Regular.ttf

//        AssetManager assetManager = application.getApplicationContext().getAssets();
//        final Typeface bold = Typeface.createFromAsset(assetManager, "fonts/ProximaNova-Regular.otf");
//        final Typeface italic = Typeface.createFromAsset(assetManager, "fonts/ProximaNova-Regular.otf");
//        final Typeface boldItalic = Typeface.createFromAsset(assetManager, "fonts/ProximaNova-Regular.otf");
//        final Typeface regular = Typeface.createFromAsset(assetManager,"fonts/ProximaNova-Regular.otf");
//
//
//        Typeface[] defaults= new Typeface[]{
//                bold,
//                italic,
//                boldItalic,
//                regular
//        };
//
//        TypefaceUtil.overrideFont(bold,"SERIF" );
//        TypefaceUtil.overrideFont(italic,"SANS_SERIF" );
//        TypefaceUtil.overrideFont(bold,"MONOSPACE" );
//        TypefaceUtil.overrideFont(boldItalic,"DEFAULT" );
//        TypefaceUtil.overrideFont(regular,"MONOSPACE" );
//
//
//        TypefaceUtil.overrideFonts(defaults);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/ProximaNova-Regular.otf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );



    }

    @Override
    public String toString() {
        return "Font bootstrapping extension";
    }
}
