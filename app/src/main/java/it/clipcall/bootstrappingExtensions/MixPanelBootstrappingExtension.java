package it.clipcall.bootstrappingExtensions;

import android.app.Application;

import org.simple.eventbus.EventBus;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.environment.Environment;
import it.clipcall.common.models.ApplicationEvent;
import it.clipcall.common.models.CommonEventNames;
import it.clipcall.common.services.MixPanelManager;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;

@Singleton
public class MixPanelBootstrappingExtension extends BootstrappingExtensionBase {

    private final MixPanelManager mixPanelManager;

    @Inject
    public MixPanelBootstrappingExtension(Application application,MixPanelManager mixPanelManager) {
        super(application);
        this.mixPanelManager = mixPanelManager;
    }

    @Override
    public void initialize() {
        if(Environment.AnalyticsEnabled){
            mixPanelManager.initialize();
            EventBus.getDefault().post(new ApplicationEvent(CommonEventNames.ApplicationLaunch));
        }
    }

    @Override
    public String toString() {
        return "MixPanel bootstrapping extension";
    }
}
