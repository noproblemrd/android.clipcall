package it.clipcall.bootstrappingExtensions;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.environment.Environment;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;

/**
 * Created by dorona on 27/12/2015.
 */
@Singleton
public class FacebookBootstrappingExtension extends BootstrappingExtensionBase {

    @Inject
    public FacebookBootstrappingExtension(Application application) {
        super(application);
    }

    @Override
    public void initialize() {

        if(Environment.AnalyticsEnabled)
        {
            FacebookSdk.sdkInitialize(application.getApplicationContext());
            AppEventsLogger.activateApp(application);


            if(Environment.IsDebug){
                FacebookSdk.setIsDebugEnabled(true);
                FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);
            }

        }

    }

    @Override
    public String toString() {
        return "Facebook bootstrapping extension";
    }
}
