package it.clipcall.bootstrappingExtensions;

import android.app.Application;
import android.util.Log;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.environment.Environment;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;
import pl.brightinventions.slf4android.FileLogHandlerConfiguration;
import pl.brightinventions.slf4android.LoggerConfiguration;

/**
 * Created by dorona on 27/12/2015.
 */
@Singleton
public class LoggerBootstrappingExtension extends BootstrappingExtensionBase {

    @Inject
    public LoggerBootstrappingExtension(Application application) {
        super(application);
    }

    @Override
    public void initialize() {
        if(!Environment.IsDebug)
            return;

        try{
             File externalDirectory = android.os.Environment.getExternalStorageDirectory();
             File logDirectory = new File(externalDirectory.getAbsolutePath() + "/" + "it.clipcall");
            boolean success  = false;
            if (!logDirectory.exists()) {
                success = logDirectory.mkdir();
            }

            if(!success)
                return;

            String fullFilePathPattern = logDirectory.getAbsolutePath() + "/clipcalllogs.%g.%u.log";
            Log.d("loggerBootstrapper", "fullFilePathPattern: "+fullFilePathPattern);
            FileLogHandlerConfiguration fileHandler = LoggerConfiguration.fileLogHandler(application);
            fileHandler.setFullFilePathPattern(fullFilePathPattern);
            LoggerConfiguration.configuration().addHandlerToRootLogger(fileHandler);
            String logFileName = fileHandler.getCurrentFileName();
            Log.d("loggerBootstrapper", "file name: "+logFileName);
        }
        catch (Exception e){

        }
    }

    @Override
    public String toString() {
        return "Emoze bootstrapping extension";
    }
}
