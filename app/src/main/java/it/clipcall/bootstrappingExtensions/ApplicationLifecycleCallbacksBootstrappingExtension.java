package it.clipcall.bootstrappingExtensions;


import android.app.Application;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.ClipCallApplication;
import it.clipcall.common.chat.services.ChatManager;
import it.clipcall.infrastructure.activities.lifecycle.DependencyInjectionActivityLifecycleCallbacks;
import it.clipcall.infrastructure.activities.lifecycle.EventBusActivityLifecycleCallbacks;
import it.clipcall.infrastructure.activities.lifecycle.FacebookEventsLoggerLifecycleCallbacks;
import it.clipcall.infrastructure.activities.lifecycle.IsActivityInForegroundLifecycleCallbacks;
import it.clipcall.infrastructure.activities.lifecycle.LayerChatActivityLifecycleCallbacks;
import it.clipcall.infrastructure.activities.lifecycle.LoggerActivityLifecycleCallbacks;
import it.clipcall.infrastructure.activities.lifecycle.RoutingActivityLifecycleCallbacks;
import it.clipcall.infrastructure.activities.lifecycle.ThemingActivityLifecycleCallbacks;
import it.clipcall.infrastructure.activities.lifecycle.ToolBarActivityLifecycleCallbacks;
import it.clipcall.infrastructure.bootstrappingExtensions.IBootstrappingExtension;
import it.clipcall.infrastructure.gcm.services.GcmTokenProvider;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.infrastructure.support.json.JsonBuilder;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class ApplicationLifecycleCallbacksBootstrappingExtension implements IBootstrappingExtension {

    private final Application application;
    private final RoutingService routingService;
    private final AuthenticationManager authenticationManager;
    private final ChatManager chatManager;
    private final GcmTokenProvider gcmTokenProvider;
    private final JsonBuilder jsonBuilder;

    @Inject
    public ApplicationLifecycleCallbacksBootstrappingExtension(Application application, RoutingService routingService,
                                                               AuthenticationManager authenticationManager,
                                                               ChatManager chatManager,
                                                               GcmTokenProvider gcmTokenProvider,
                                                               JsonBuilder jsonBuilder) {
        this.application = application;
        this.routingService = routingService;
        this.authenticationManager = authenticationManager;
        this.chatManager = chatManager;
        this.gcmTokenProvider = gcmTokenProvider;
        this.jsonBuilder = jsonBuilder;
    }

    @Override
    public void initialize() {
        //consumer routes

        ClipCallApplication clipCallApplication = (ClipCallApplication)application;

       application.registerActivityLifecycleCallbacks(new DependencyInjectionActivityLifecycleCallbacks(clipCallApplication.getComponent()));
       application.registerActivityLifecycleCallbacks(new LoggerActivityLifecycleCallbacks());
       application.registerActivityLifecycleCallbacks(new LayerChatActivityLifecycleCallbacks(authenticationManager, chatManager, gcmTokenProvider));
       application.registerActivityLifecycleCallbacks(new ThemingActivityLifecycleCallbacks(authenticationManager));
       application.registerActivityLifecycleCallbacks(new IsActivityInForegroundLifecycleCallbacks(clipCallApplication));
       application.registerActivityLifecycleCallbacks(new FacebookEventsLoggerLifecycleCallbacks(jsonBuilder));
       application.registerActivityLifecycleCallbacks(new EventBusActivityLifecycleCallbacks());
       application.registerActivityLifecycleCallbacks(new ToolBarActivityLifecycleCallbacks());
       application.registerActivityLifecycleCallbacks(new RoutingActivityLifecycleCallbacks(routingService));

    }

    @Override
    public String toString() {
        return "Application Lifecycle callbacks bootstrapping extension";
    }
}
