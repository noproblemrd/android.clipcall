package it.clipcall.bootstrappingExtensions;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.bootstrappingExtensions.IBootstrappingExtension;
import it.clipcall.infrastructure.features.ApplicationFeature;
import it.clipcall.infrastructure.features.ApplicationFeaturesService;

@Singleton
public class ApplicationFeaturesBootstrappingExtension implements IBootstrappingExtension {

    private final ApplicationFeaturesService applicationFeaturesService;

    @Inject
    public ApplicationFeaturesBootstrappingExtension(ApplicationFeaturesService applicationFeaturesService) {
        this.applicationFeaturesService = applicationFeaturesService;

    }

    @Override
    public void initialize() {
        //consumer routes

        Field[] declaredFields = ApplicationFeature.class.getDeclaredFields();
        List<Field> staticFields = new ArrayList<Field>();
        for (Field field : declaredFields) {
            if (java.lang.reflect.Modifier.isStatic(field.getModifiers())) {
                staticFields.add(field);
                try {
                    Object feature = field.get(null);
                    if(feature instanceof ApplicationFeature){
                        applicationFeaturesService.addFeature((ApplicationFeature)feature);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }



    }

    @Override
    public String toString() {
        return "Application features bootstrapping extension";
    }
}
