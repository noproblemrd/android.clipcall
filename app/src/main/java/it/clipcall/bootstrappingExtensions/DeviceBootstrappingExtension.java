package it.clipcall.bootstrappingExtensions;

import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageManager;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.models.GcmToken;
import it.clipcall.consumer.profile.controllers.CustomerProfileController;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;
import it.clipcall.infrastructure.gcm.services.ClipCallInstanceIDListenerService;
@Singleton
public class DeviceBootstrappingExtension extends BootstrappingExtensionBase {

    private final CustomerProfileController profileController;

    @Inject
    public DeviceBootstrappingExtension(Application application, CustomerProfileController profileController) {
        super(application);
        this.profileController = profileController;
    }

    @Override
    public void initialize() {
        EventBus.getDefault().register(this);
        application.startService(new Intent(application.getApplicationContext(), ClipCallInstanceIDListenerService.class));
    }

    @Override
    public String toString() {
        return "Device bootstrapping extension";
    }

    @Subscriber(mode = ThreadMode.ASYNC)
    void onDeviceTokenChanged(GcmToken gcmToken){
        String versionName = "unknown";
        try {
            versionName = application.getPackageManager().getPackageInfo(application.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        profileController.updateCustomerDevice(versionName);
    }
}
