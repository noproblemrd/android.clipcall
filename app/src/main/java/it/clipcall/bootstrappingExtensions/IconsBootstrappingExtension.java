package it.clipcall.bootstrappingExtensions;

import android.app.Application;



import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;

@Singleton
public class IconsBootstrappingExtension extends BootstrappingExtensionBase {

    @Inject
    @Singleton
    public IconsBootstrappingExtension(Application application) {
        super(application);
    }

    @Override
    public void initialize() {

    }

    @Override
    public String toString() {
        return "Icons bootstrapping extension";
    }
}
