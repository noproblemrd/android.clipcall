package it.clipcall.bootstrappingExtensions;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.findprofessional.services.ProfessionalsManager;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;

/**
 * Created by dorona on 27/12/2015.
 */
@Singleton
public class CreateApplicationShortcutBootstrappingExtension extends BootstrappingExtensionBase {


    private static final String SETTINGS = "SETTINGS";
    private static final String SHORTCUT = "SHORTCUT";

    @Inject
    public CreateApplicationShortcutBootstrappingExtension(Application application, ProfessionalsManager professionalsManager) {
        super(application);

    }

    @Override
    public void initialize() {

        SharedPreferences preferences = application.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        if (preferences.getBoolean(SHORTCUT, false))
            return;

        Intent shortcut = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        ApplicationInfo appInfo = application.getApplicationInfo();
        // Shortcut name
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME, appInfo.name);
        shortcut.putExtra("duplicate", false); // Just create once
        // Setup activity shoud be shortcut object
        ComponentName component = new ComponentName(appInfo.packageName, appInfo.className);
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, new Intent(Intent.ACTION_MAIN).setComponent(component));
        // Set shortcut icon
        Intent.ShortcutIconResource iconResource = Intent.ShortcutIconResource.fromContext(application, appInfo.icon);
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, iconResource);
        application.sendBroadcast(shortcut);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putBoolean(SHORTCUT, true);
        edit.apply();
    }

    @Override
    public String toString() {
        return "Create applicaiton shortcut bootstrapping extension";
    }
}
