package it.clipcall.bootstrappingExtensions;

import android.app.Application;

import com.emoze.tildaLib.Database.UserDataProvider;
import com.emoze.tildaLib.ELCPClient;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.environment.Environment;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;

/**
 * Created by dorona on 27/12/2015.
 */
@Singleton
public class EmozeBootstrappingExtension extends BootstrappingExtensionBase {

    @Inject
    public EmozeBootstrappingExtension(Application application) {
        super(application);
    }

    @Override
    public void initialize() {

        if(Environment.IsDebug)
        {
            UserDataProvider.init(application.getApplicationContext().getPackageName());
            ELCPClient.SetDebugEnable(application.getApplicationContext(),true);
            ELCPClient.InitDebug(application.getApplicationContext());
        }
    }

    @Override
    public String toString() {
        return "Emoze bootstrapping extension";
    }
}
