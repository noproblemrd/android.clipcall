package it.clipcall.bootstrappingExtensions;

import android.app.Application;

import com.google.common.base.Strings;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.consumer.payment.controllers.PaymentCoordinator;
import it.clipcall.consumer.profile.models.FacebookProfile;
import it.clipcall.consumer.profile.services.CustomerProfileManager;
import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;
import it.clipcall.infrastructure.logger.ILogger;
import it.clipcall.infrastructure.logger.LoggerFactory;
import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.professional.validation.services.LoginEvent;

/**
 * Created by dorona on 27/12/2015.
 */
@Singleton
public class BonusClaimBootstrappingExtension extends BootstrappingExtensionBase {

    private final Application application;
    private final AuthenticationManager authenticationManager;
    private final CustomerProfileManager profileManager;
    private final PaymentCoordinator paymentCoordinator;

    private final ILogger logger = LoggerFactory.getLogger(BonusClaimBootstrappingExtension.class.getSimpleName());

    @Inject
    public BonusClaimBootstrappingExtension(Application application, AuthenticationManager authenticationManager, CustomerProfileManager profileManager, PaymentCoordinator paymentCoordinator) {
        super(application);
        this.application = application;
        this.authenticationManager = authenticationManager;
        this.profileManager = profileManager;
        this.paymentCoordinator = paymentCoordinator;
    }

    @Override
    public void initialize() {
        EventBus.getDefault().register(this);
    }

    @Override
    public String toString() {
        return "BonusClaim bootstrapping extension";
    }


    @Subscriber(mode = ThreadMode.ASYNC)
    void onLogin(LoginEvent event){

        logger.debug("Received 'Login' event - about to execute discount claim flow");

        String customerId = authenticationManager.getCustomerId();
        if(Strings.isNullOrEmpty(customerId))
        {
            logger.error("customer id should not be null after successful login");
            return;
        }

        FacebookProfile facebookProfile = profileManager.getFacebookProfile();
        if(facebookProfile == null){
            logger.debug("facebook profile is null, no need to update user's profile and claim discount.");
            return;
        }

        boolean updated = profileManager.updateCustomerProfile(customerId, facebookProfile);
        if(!updated){
            logger.error("failed updating customer profile based on facebook. Cannot proceed with claiming discount.");
            return;
        }

        logger.debug("about to claim discount after updating profile based on facebook");
        boolean claimed = paymentCoordinator.claimDiscount(true);
        logger.debug("claimed discount post login with result - claimed: "+ claimed);
    }
}
