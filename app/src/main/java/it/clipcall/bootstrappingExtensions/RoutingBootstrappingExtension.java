package it.clipcall.bootstrappingExtensions;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.common.invitations.services.BranchIoManager;
import it.clipcall.infrastructure.bootstrappingExtensions.IBootstrappingExtension;
import it.clipcall.infrastructure.repositories.Repository2;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.infrastructure.routing.models.RoutingRulesContext;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.ConsumerInitialRouteRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.ConsumerNewQuoteRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.ConsumerPendingSystemChatMessageRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.CustomerInvitationInitialRouteRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.DefaultActionRouteRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.PostPhoneValidationRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.RequiresBusinessRegistrationRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.RequiresPhoneValidationRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.SubRouteActivationRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.SubmitProjectToAdvertisersPhoneValidationRequiredRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.VideoChatInitialRouteRule;
import it.clipcall.infrastructure.routing.models.routes.consumer.rules.VideoChatPhoneValidationRequiredRouteActionRule;
import it.clipcall.infrastructure.routing.models.routes.professional.ProfessionalInitialRouteRule;
import it.clipcall.infrastructure.routing.models.routes.professional.rules.NewProjectRule;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.professional.validation.services.AuthenticationManager;

@Singleton
public class RoutingBootstrappingExtension implements IBootstrappingExtension {

    private final RoutingService routingService;
    private final BranchIoManager branchIoManager;
    private final AuthenticationManager authenticationManager;
    private final RoutingRulesContext routingRulesContext;
    private final RouteParams routeParams;
    private final Repository2 repository;

    @Inject
    public RoutingBootstrappingExtension(RoutingService routingService,
                                         BranchIoManager branchIoManager,
                                         AuthenticationManager authenticationManager,
                                         RoutingRulesContext routingRulesContext,
                                         RouteParams routeParams,
                                         Repository2 repository) {
        this.routingService = routingService;
        this.branchIoManager = branchIoManager;
        this.authenticationManager = authenticationManager;
        this.routingRulesContext = routingRulesContext;
        this.routeParams = routeParams;
        this.repository = repository;
    }

    @Override
    public void initialize() {
        //consumer routes

        Field[] declaredFields = Routes.class.getDeclaredFields();
        List<Field> staticFields = new ArrayList<Field>();
        for (Field field : declaredFields) {
            if (java.lang.reflect.Modifier.isStatic(field.getModifiers())) {
                staticFields.add(field);
                try {
                    Object route = field.get(null);
                    if(route instanceof Route){
                        routingService.addRoute((Route)route);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        routingService.addRoutingRule(new RequiresBusinessRegistrationRule(authenticationManager));
        routingService.addRoutingRule(new RequiresPhoneValidationRule(authenticationManager));
        routingService.addRoutingRule(new CustomerInvitationInitialRouteRule(branchIoManager,authenticationManager));
        routingService.addRoutingRule(new VideoChatInitialRouteRule(branchIoManager));
        routingService.addRoutingRule(new NewProjectRule(repository, authenticationManager));
        routingService.addRoutingRule(new ConsumerPendingSystemChatMessageRule(repository, authenticationManager,routeParams));
        routingService.addRoutingRule(new ConsumerNewQuoteRule(repository, authenticationManager,routeParams));
        routingService.addRoutingRule(new PostPhoneValidationRule(authenticationManager, routingRulesContext));
        routingService.addRoutingRule(new ProfessionalInitialRouteRule(authenticationManager));
        routingService.addRoutingRule(new ConsumerInitialRouteRule(authenticationManager));



        routingService.addRoutingRule(new VideoChatPhoneValidationRequiredRouteActionRule(authenticationManager));
        routingService.addRoutingRule(new SubmitProjectToAdvertisersPhoneValidationRequiredRule(authenticationManager));

        routingService.addRoutingRule(new SubRouteActivationRule(authenticationManager));
        routingService.addRoutingRule(new DefaultActionRouteRule());

    }

    @Override
    public String toString() {
        return "Routing bootstrapping extension";
    }
}
