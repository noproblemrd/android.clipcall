package it.clipcall.bootstrappingExtensions;

import android.app.Application;

import com.karumi.dexter.Dexter;

import javax.inject.Inject;
import javax.inject.Singleton;

import it.clipcall.infrastructure.bootstrappingExtensions.BootstrappingExtensionBase;

/**
 * Created by dorona on 27/12/2015.
 */
@Singleton
public class PermissionsBootstrappingExtension extends BootstrappingExtensionBase {

    @Inject
    public PermissionsBootstrappingExtension(Application application) {
        super(application);
    }

    @Override
    public void initialize() {
        Dexter.initialize(application.getApplicationContext());
    }

    @Override
    public String toString() {
        return "Permissions bootstrapping extension";
    }
}
