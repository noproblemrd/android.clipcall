package it.clipcall.dorona.clipcall;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.common.invitations.models.eInvitationType;
import it.clipcall.infrastructure.rules.RuleEngine;
import it.clipcall.infrastructure.rules.RuleEngineFactory;
import it.clipcall.common.invitations.services.BranchIoManager;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class NavigationRuleEngineTests extends TestCase {

    RuleEngine ruleEngine;

    @Mock
    AuthenticationManager authenticationManager;

    @Mock
    BranchIoManager branchIoManager;

    @Before
    public void setUp() throws Exception{


        RuleEngineFactory factory = new RuleEngineFactory(branchIoManager, authenticationManager);
        ruleEngine = factory.getRuleEngine();
    }

    @Test
    public void shouldRouteToValidationScreen() throws Exception{
        when(branchIoManager.hasInvitation(eInvitationType.VideoChat)).thenReturn(true);
        verify(branchIoManager).hasInvitation(eInvitationType.VideoChat);

    }
}
