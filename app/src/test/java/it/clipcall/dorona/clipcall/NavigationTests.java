package it.clipcall.dorona.clipcall;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.bootstrappingExtensions.RoutingBootstrappingExtension;
import it.clipcall.common.invitations.models.eInvitationType;
import it.clipcall.infrastructure.routing.models.routes.Route;
import it.clipcall.infrastructure.routing.models.routes.consumer.Routes;
import it.clipcall.infrastructure.routing.services.RouteActivationService;
import it.clipcall.infrastructure.routing.services.RoutingService;
import it.clipcall.common.invitations.services.BranchIoManager;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NavigationTests {


    private RoutingService routingService;

    @Mock
    AuthenticationManager authenticationManager;

    @Mock
    BranchIoManager branchIoManager;

    @Mock
    RouteActivationService routeActivationService;



    @Before
    public void setUp() throws Exception{
        routingService = new RoutingService(routeActivationService, authenticationManager);
        RoutingBootstrappingExtension routingBootstrappingExtension = new RoutingBootstrappingExtension(routingService,branchIoManager,authenticationManager,null, null,null);
        routingBootstrappingExtension.initialize();
    }

    @Test
    public void InitialNavigation_SouldNavigateToStartVideoChat_WhenBranchIoHasVideoInvitation() throws Exception{

       // Assert.assertNull(routingService.getPendingRoute());
        when(branchIoManager.hasInvitation(eInvitationType.VideoChat)).thenReturn(true);

        routingService.routeTo((Route)null,null);


        //when(branchIoManager.hasInvitation()).thenReturn(true);
        verify(routeActivationService).activate(Routes.ConsumerStartVideoChatRoute, null);
    }

    @Test
    public void InitialNavigation_SouldNavigateToConsumerMainMenu_WhenBranchIoHasNoVideoInvitation() throws Exception{

        // Assert.assertNull(routingService.getPendingRoute());
        when(branchIoManager.hasInvitation(eInvitationType.VideoChat)).thenReturn(false);

        routingService.routeTo((Route) null, null);

        //when(branchIoManager.hasInvitation()).thenReturn(true);
        verify(routeActivationService).activate(Routes.ConsumerMainMenuActivityRoute,null);
    }


    @Test
    public void NavigateToAcceptVideoChatInvitation_SouldNavigateToPhoneValidation_WhenUserIsNotAuthenticated() throws Exception{

        // Assert.assertNull(routingService.getPendingRoute());
        when(authenticationManager.isCustomerAuthenticated()).thenReturn(false);
        routingService.routeTo(Routes.ConsumerStartVideoChat2Route, null);

        //when(branchIoManager.hasInvitation()).thenReturn(true);
        verify(routeActivationService).activate(Routes.ConsumerValidationRoute,null);
        verify(routeActivationService).setPendingRoute(Routes.ConsumerStartVideoChat2Route);
    }

    @Test
    public void NavigateToPhoneValidation_SouldNavigateToPhoneValidation_WhenUserIsNotAuthenticated() throws Exception{

        // Assert.assertNull(routingService.getPendingRoute());
        when(authenticationManager.isCustomerAuthenticated()).thenReturn(false);
        routingService.routeTo(Routes.ConsumerValidationRoute, null);

        //when(branchIoManager.hasInvitation()).thenReturn(true);
        verify(routeActivationService).activate(Routes.ConsumerValidationRoute, null);
        verify(routeActivationService, never()).setPendingRoute(Routes.ConsumerStartVideoChat2Route);
    }


    @Test
    public void NavigateBackFromPhoneValidation_ShouldNavigateToStartVideoChat2_AfterUserIsAuthenticated() throws Exception{
        // Assert.assertNull(routingService.getPendingRoute());
        when(authenticationManager.isCustomerAuthenticated()).thenReturn(false);
        routingService.routeTo(Routes.ConsumerStartVideoChat2Route, null);
        routingService.routeTo(Routes.ConsumerValidation2Route, null);
        verify(routeActivationService).activate(Routes.ConsumerValidation2Route, null);
        when(routeActivationService.getPendingRoute()).thenReturn(Routes.ConsumerStartVideoChat2Route);
        when(authenticationManager.isCustomerAuthenticated()).thenReturn(true);
        //when(branchIoManager.hasInvitation()).thenReturn(true);
        routingService.routeTo((Route)null,null);
        verify(routeActivationService).activate(Routes.ConsumerStartVideoChat2Route,null);
        verify(routeActivationService).setPendingRoute(null);


    }
}
