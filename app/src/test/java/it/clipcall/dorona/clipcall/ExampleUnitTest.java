package it.clipcall.dorona.clipcall;

import org.junit.Test;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import it.clipcall.consumer.findprofessional.support.filters.CategoryByNameComparator;
import it.clipcall.consumer.findprofessional.support.filters.CategoryFilter;
import it.clipcall.professional.profile.models.Category;

import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
        String message = "_sender_name_$$$foo$$$_sender_id_$$$f8e33962-4c8f-417c-ae45-b5967775b235$$$_sender_image_url_$$$https://s3.amazonaws.com/mobileclipcall2/qa_supplier/f8e33962-4c8f-417c-ae45-b5967775b235/audioData_925.png$$$_receiver_name_$$$0542292946$$$_receiver_id_$$$ef3978b5-3a8e-4ba2-b9fd-978b077c81af$$$_receiver_image_url_$$$null$$$_accound_id_$$$f1ba800a-ce4e-49b4-9140-840dc62f6779";
        String[] messageParts = message.split("$$$");
        //return Splitter.on(" ").withKeyValueSeparator("=").split(message);

    }

    @Test
    public void testSms(){

        double feePercentage = 15;
        double reducedPrice = 155.46;
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        NumberFormat percentageFormat = new DecimalFormat("00");
        String formattedPercentage = percentageFormat.format(feePercentage);
        String reducedPriceText = formatter.format(reducedPrice);
        String reducedPriceTextFormat = "Your payment after ClipCall's fee (-%s/%) %s";
        String finalText = String.format(reducedPriceTextFormat, formattedPercentage, reducedPriceText);

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        String formattedDate = dateFormat.format(date);


    }



    @Test
    public void CategoryComparator_comparing(){


        Category paydayCategory = new Category();
        paydayCategory.setCategoryId("b8ac4386-2049-e211-9708-001517d10f6e");
        paydayCategory.setCategoryName("Pay day loan counselor");
        paydayCategory.setValue("payday");


        Category veterinarianCategory = new Category();
        veterinarianCategory.setCategoryId("e2cfa4f6-37aa-e111-9280-001517d10f6e");
        veterinarianCategory.setCategoryName("Veterinarian");
        veterinarianCategory.setValue("pet doctor");



        Category packageCategory = new Category();
        packageCategory.setCategoryId("572c191a-8d73-e211-9195-001517d1792a");
        packageCategory.setCategoryName("Package");
        packageCategory.setValue("package");


        Set<Category> items = new HashSet<>();
        items.add(paydayCategory);
        items.add(veterinarianCategory);
        items.add(packageCategory);



        CategoryByNameComparator categoryComparator = new CategoryByNameComparator("p");
        List<Category> sortedCategories = CategoryFilter.asSortedList(items, categoryComparator);


        Category firstCategory = sortedCategories.get(0);
        Category secondCategory = sortedCategories.get(1);
        Category thirdCategory = sortedCategories.get(2);




    }
}