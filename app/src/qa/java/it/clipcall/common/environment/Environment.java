package it.clipcall.common.environment;


import com.google.android.gms.wallet.WalletConstants;

public class Environment {

    public final static String BaseUrl = "http://mobileqa.clipcall.it/api/";

    public final static String StripeRedirectUrl = "http%3A%2F%2Fmobileqa.clipcall.it%2Fstripe%2FIndex";
    //public final static String BaseUrl = "http://10.0.0.96/MobileAPI/api/";


    public final static String StripeClientId = "ca_7cwmQtgVqDU8XUlgG6nRYHxjavisAVK8";

    public final static String StripPublishedKey = "pk_test_RSfJJASb8Ex0iFcovO7MR2DS";

    public final static int WalletEnvironment = WalletConstants.ENVIRONMENT_TEST;

    public final static boolean IsDebug = true;

    public final static boolean AppSeeEnabled = false;

    public final static boolean AnalyticsEnabled = true;
}
