package it.clipcall.common.environment;


import com.google.android.gms.wallet.WalletConstants;

public class Environment {
    //private final static String baseUrl = "http://10.0.0.96/MobileAPI/api/";
    public final static String BaseUrl = "https://mobileapi.clipcall.it/api/";

    public final static String StripeClientId = "ca_7cwmwEQdqeyIA2wQaX9EFIVECdADxu4O";

    public final static String StripeRedirectUrl = "https%3A%2F%2Fmobileapi.clipcall.it%2Fstripe%2FIndex";

    public final static String StripPublishedKey = "pk_live_6MFhsuj7F0VZLHUyi0QqFYkt";

    public final static int WalletEnvironment = WalletConstants.ENVIRONMENT_PRODUCTION;

    public final static boolean IsDebug = false;

    public final static boolean AppSeeEnabled = true;

    public final static boolean AnalyticsEnabled = true;
}
