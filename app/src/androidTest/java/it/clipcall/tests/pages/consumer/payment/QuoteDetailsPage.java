package it.clipcall.tests.pages.consumer.payment;

import it.clipcall.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by omega on 2/19/2016.
 */
public class QuoteDetailsPage {

    public void clickBookTheService(){
        onView(withId(R.id.bookQuoteButton))
                .perform(click());
    }
}
