package it.clipcall.tests.pages.consumer.payment;

import it.clipcall.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by omega on 2/19/2016.
 */
public class PayTheProPage {


    public void enterChargeAmount(String chargeAmount){
        onView(withId(R.id.amountEditText))
                .perform(typeText(chargeAmount), closeSoftKeyboard());
    }


    public void selectCreditCard(){
        onView(withId(R.id.payWithCreditCardButton)).perform(click());
    }
}
