package it.clipcall.tests.pages.consumer.payment;

import it.clipcall.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by omega on 2/19/2016.
 */
public class ConsumerPaymentDetailsPage {


    public void hasChargeAmountText(String expectedText){
        onView(withId(R.id.chargeAmountTextView))
                .check(matches(withText(expectedText)));
    }

    public void enterCreditCardNumber(String creditCardNumber){
        onView(withId(R.id.creditCardNumberEditText))
                .perform(typeText(creditCardNumber), closeSoftKeyboard());
    }


    public void enterCvv(String cvv){
        onView(withId(R.id.cvvEditText))
                .perform(typeText(cvv), closeSoftKeyboard());
    }


    public void enterEmail(String email){
        onView(withId(R.id.emailEditText))
                .perform(typeText(email), closeSoftKeyboard());
    }

    public void enterExpirationYear(String expirationYear){
        onView(withId(R.id.creditCardExpirationYearEditText))
                .perform(typeText(expirationYear), closeSoftKeyboard());
    }

    public void enterExpirationMonth(String expirationMonth){
        onView(withId(R.id.creditCardExpirationMonthEditText))
                .perform(typeText(expirationMonth), closeSoftKeyboard());
    }


    public void clickPayThePro(){
        onView(withId(R.id.payProButton))
                .perform(click());
    }

    public void enterFullName(String fullName) {
        onView(withId(R.id.fullNameEditText))
                .perform(typeText(fullName), closeSoftKeyboard());
    }
}
