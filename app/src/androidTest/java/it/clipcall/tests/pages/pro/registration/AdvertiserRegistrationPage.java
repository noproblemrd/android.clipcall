package it.clipcall.tests.pages.pro.registration;

import it.clipcall.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static it.clipcall.tests.support.EditTextMatchers.withErrorText;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.not;

/**
 * Created by omega on 2/19/2016.
 */
public class AdvertiserRegistrationPage {

    public void enterBusinessName(String businessName){
        onView(withId(R.id.businessNameEditText))
                .perform(typeText(businessName), closeSoftKeyboard());
    }


    public void enterWebsite(String website){
        onView(withId(R.id.websiteEditText))
                .perform(typeText(website), closeSoftKeyboard());
    }


    public void enterEmail(String email){
        onView(withId(R.id.emailEditText))
                .perform(typeText(email), closeSoftKeyboard());
    }

    public void licenseNumberIsInvisible(){
        onView(withId(R.id.licenseNumberEditText))
                .check(matches(not(isDisplayed())));
    }

    public void licenseNumberIsVisible(){
        onView(withId(R.id.licenseNumberEditText))
                .check(matches(isDisplayed()));
    }

    public void toggleHasLicense(){

        onView(withId(R.id.haveLicenseSwitch))
                .perform(click());
    }

    public void enterLicenseNumber(String licenseNumber){
        onView(withId(R.id.licenseNumberEditText))
                .perform(typeText(licenseNumber), closeSoftKeyboard());



    }

    public void enterBusinessDescription(String businessDescription){
        onView(withId(R.id.businessDescriptionEditText))
                .perform(typeText(businessDescription), closeSoftKeyboard());

    }

    public void shouldDisplayErrorMessage(String errorMessage) {
        onView(anyOf(withErrorText(errorMessage)))
                .check(matches(isDisplayed()));
    }


    public void clickNextButton(){
        /*onView(withId(R.id.nextButton))
                .perform(click());*/
    }

  /*  public void nextButtonShouldBeDisabled() {
        onView(withId(R.id.nextButton))
                .check(matches(not(isEnabled())));
    }*/


}
