package it.clipcall.tests.specs.consumer.payment;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import it.clipcall.consumer.payment.activities.PayTheProActivity_;
import it.clipcall.tests.pages.consumer.payment.ConsumerPaymentDetailsPage;
import it.clipcall.tests.pages.consumer.payment.PayTheProPage;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class PaymentSpecs {

    @Rule
    public ActivityTestRule<PayTheProActivity_> activityRule = new ActivityTestRule<>(PayTheProActivity_.class, true);


    private PayTheProPage payTheProPage;
    private ConsumerPaymentDetailsPage addCreditCardPage;

    @Before
    public void beforeEach(){
        payTheProPage = new PayTheProPage();
        addCreditCardPage = new ConsumerPaymentDetailsPage();
    }


    @Test
    public void changeText_sameActivity() {
        // Type text and then press the button.
//        String amountInput = "100";
//        onView(withId(R.id.amountEditText))
//                .perform(typeText(amountInput), closeSoftKeyboard());
//        onView(withId(R.id.payWithCreditCardButton)).perform(click());


        Intent intent = new Intent();
        intent.putExtra("projectId", "cd5f5414-a128-48ea-bb92-c543aeaa0c13");
        intent.putExtra("advertiserId","5bfa4134-8cc5-48a4-8038-a29a0ebf53ba");
        activityRule.launchActivity(intent);

        String chargeAmount = "150";

        payTheProPage.enterChargeAmount(chargeAmount);

        payTheProPage.selectCreditCard();

        addCreditCardPage.hasChargeAmountText("$150.00");


        addCreditCardPage.enterCreditCardNumber("2424242424242424");
        addCreditCardPage.enterCvv("123");
        addCreditCardPage.enterEmail("foo@bar.gaz");

        addCreditCardPage.clickPayThePro();

    }


}
