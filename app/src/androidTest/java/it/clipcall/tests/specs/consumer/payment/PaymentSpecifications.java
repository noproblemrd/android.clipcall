package it.clipcall.tests.specs.consumer.payment;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import it.clipcall.consumer.payment.activities.CustomerCreditCardDetailsActivity;
import it.clipcall.consumer.payment.activities.CustomerCreditCardDetailsActivity_;
import it.clipcall.consumer.payment.presenters.CustomerAddCreditCardPresenter;
import it.clipcall.infrastructure.di.components.ActivityComponent;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import it.clipcall.tests.pages.consumer.payment.ConsumerPaymentDetailsPage;

@RunWith(AndroidJUnit4.class)
public class PaymentSpecifications  extends ActivityInstrumentationTestCase2<CustomerCreditCardDetailsActivity_> {

    private ActivityComponent compositionRootComponent;

    private CustomerCreditCardDetailsActivity activity;

    private CustomerAddCreditCardPresenter sut;

    public PaymentSpecifications() {
        super(CustomerCreditCardDetailsActivity_.class);
    }

    ConsumerPaymentDetailsPage addCreditCardPage;

    @Before
    public void setUp() throws Exception {
        super.setUp();

        // Injecting the Instrumentation instance is required
        // for your test to run with AndroidJUnitRunner.
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        activity = getActivity();

        addCreditCardPage = new ConsumerPaymentDetailsPage();
        compositionRootComponent = activity.getActivityComponent();

        sut = compositionRootComponent.getCustomerAddCreditCardPresenter();

    }

    @Test
    public void typeOperandsAndPerformAddOperation() throws Throwable{

        final CountDownLatch signal = new CountDownLatch(1);
        addCreditCardPage.enterCreditCardNumber("4242424242424242");
        addCreditCardPage.enterCvv("123");
        addCreditCardPage.enterEmail("dorona@clipcall.it");
        addCreditCardPage.enterExpirationYear("2020");
        addCreditCardPage.enterExpirationMonth("10");



        runTestOnUiThread(new Runnable() {
                              public void run() {
                                  sut.payThePro();
                              }
                          });



        signal.await(30, TimeUnit.SECONDS);

        RoutingContext routingContext = activity.getRoutingContext();
    }
}
