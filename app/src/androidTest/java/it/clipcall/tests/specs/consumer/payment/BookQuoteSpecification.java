package it.clipcall.tests.specs.consumer.payment;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import it.clipcall.ClipCallApplication;
import it.clipcall.common.projects.models.ProjectEntityBase;
import it.clipcall.consumer.payment.services.PaymentManager;
import it.clipcall.consumer.payment.viewModel.activities.ConsumerQuoteDetailsActivity;
import it.clipcall.consumer.payment.viewModel.activities.ConsumerQuoteDetailsActivity_;
import it.clipcall.consumer.projects.models.CustomerPaymentMethod;
import it.clipcall.consumer.projects.models.ProfessionalRef;
import it.clipcall.consumer.projects.models.ProjectEntity;
import it.clipcall.consumer.projects.services.ProjectsManager;
import it.clipcall.infrastructure.di.components.ActivityComponent;
import it.clipcall.infrastructure.di.components.ApplicationComponent;
import it.clipcall.infrastructure.routing.models.RouteParams;
import it.clipcall.professional.leads.models.Quote;
import it.clipcall.professional.leads.models.eQuoteStatus;
import it.clipcall.professional.leads.models.eQuoteStatusReason;
import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.tests.pages.consumer.payment.ConsumerPaymentDetailsPage;
import it.clipcall.tests.pages.consumer.payment.QuoteDetailsPage;

@RunWith(AndroidJUnit4.class)
public class BookQuoteSpecification  extends ActivityInstrumentationTestCase2<ConsumerQuoteDetailsActivity_> {

    private ActivityComponent compositionRootComponent;

    private ConsumerQuoteDetailsActivity activity;

    PaymentManager paymentManager;


    public BookQuoteSpecification() {
        super(ConsumerQuoteDetailsActivity_.class);
    }

    QuoteDetailsPage quoteDetailsPage;
    ConsumerPaymentDetailsPage addCreditCardPage;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        ClipCallApplication application = (ClipCallApplication)this.getInstrumentation().getTargetContext().getApplicationContext();
        //ClipCallApplication application = (ClipCallApplication) activity.getApplication();
        ApplicationComponent applicationComponent = application.getComponent();
        RouteParams routeParams = applicationComponent.getRouteParams();
        paymentManager = applicationComponent.getPaymentManager();
        AuthenticationManager authenticationManager = applicationComponent.getAuthenticationManager();
        ProjectsManager projectsManager = applicationComponent.getProjectsManager();
        String customerId = authenticationManager.getCustomerId();
        List<ProjectEntityBase> projects = projectsManager.getAll(customerId, false);

        ProjectEntity project = (ProjectEntity)projects.get(0);
        List<ProfessionalRef> leadAccountData = project.getLeadAccountData();
        ProfessionalRef professionalRef = leadAccountData.get(0);

        Quote quote = new Quote();
        quote.setCreatedOn(LocalDateTime.now().toDate());
        quote.setDescription("Description text");
        quote.setId("123123132");
        quote.setNumber(1);
        quote.setPrice(200.0d);
        quote.setStartJobAt(LocalDate.now().plusDays(2).toDate());
        quote.setStatus(eQuoteStatus.Open);
        quote.setStatusReason(eQuoteStatusReason.NewQuote);


        routeParams.setParam("quote",quote);
        routeParams.setParam("project",project);
        routeParams.setParam("leadAccountId", professionalRef.getLeadAccountId());
        routeParams.setParam("routeBackToHome",new Boolean(true));

        // Injecting the Instrumentation instance is required
        // for your test to run with AndroidJUnitRunner.

        activity = getActivity();

        quoteDetailsPage = new QuoteDetailsPage();
        addCreditCardPage = new ConsumerPaymentDetailsPage();
        compositionRootComponent = activity.getActivityComponent();
    }

    @Test
    public void bookQuoteWithoutPaymentMethod() throws Throwable{


        paymentManager.setLastPaymentMethod(null);

        final CountDownLatch signal = new CountDownLatch(1);
        signal.await(2, TimeUnit.SECONDS);
        quoteDetailsPage.clickBookTheService();
        signal.await(2, TimeUnit.SECONDS);

        addCreditCardPage.enterFullName("Foo Bar");
        addCreditCardPage.enterEmail("dorona@clipcall.it");
        addCreditCardPage.enterCreditCardNumber("4242424242424242");
        addCreditCardPage.enterExpirationMonth("10");
        addCreditCardPage.enterExpirationYear("2020");
        addCreditCardPage.enterCvv("123");


        addCreditCardPage.clickPayThePro();


        signal.await(10, TimeUnit.SECONDS);
    }

    @Test
    public void bookQuoteWithPaymentMethod() throws Throwable{
        paymentManager.setLastPaymentMethod(new CustomerPaymentMethod());
        final CountDownLatch signal = new CountDownLatch(1);
        signal.await(2, TimeUnit.SECONDS);
        quoteDetailsPage.clickBookTheService();
        signal.await(10, TimeUnit.SECONDS);
        addCreditCardPage.clickPayThePro();
        signal.await(10, TimeUnit.SECONDS);
    }
}
