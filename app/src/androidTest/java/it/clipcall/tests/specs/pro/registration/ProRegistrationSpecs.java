package it.clipcall.tests.specs.pro.registration;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import it.clipcall.professional.registration.activities.AdvertiserRegistrationActivity_;
import it.clipcall.tests.pages.pro.registration.AdvertiserRegistrationPage;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ProRegistrationSpecs {

    @Rule
    public ActivityTestRule<AdvertiserRegistrationActivity_> activityRule = new ActivityTestRule<>(AdvertiserRegistrationActivity_.class, true);


    private AdvertiserRegistrationPage registrationPage;


    @Before
    public void beforeEach(){
        registrationPage = new AdvertiserRegistrationPage();
    }


    @Test
    public void givenAdvertiserBusinessDetailsFormIsMissingBusinessName_ThenAnAppropriateErrorMessageShouldBeDisplayed() {
        //registrationPage.enterBusinessName("Mike the handyman");
        registrationPage.enterEmail("foo@bar.baz");
        registrationPage.enterWebsite("http://foo-bar.baz");
        registrationPage.clickNextButton();
        registrationPage.shouldDisplayErrorMessage("Business name is required.");
        //registrationPage.nextButtonShouldBeDisabled();
    }

    @Test
    public void givenAdvertiserBusinessDetailsFormIsMissingWebsite_ThenAnAppropriateErrorMessageShouldBeDisplayed() {
        registrationPage.enterBusinessName("Mike the handyman");
        registrationPage.enterEmail("foo@bar.baz");
        //registrationPage.enterWebsite("http://foo-bar.baz");
        registrationPage.clickNextButton();
        registrationPage.shouldDisplayErrorMessage("Website is required.");
        //registrationPage.nextButtonShouldBeDisabled();
    }


    @Test
    public void givenAdvertiserBusinessDetailsFormIsMissingEmailThenAnAppropriateErrorMessageShouldBeDisplayed() {
        registrationPage.enterBusinessName("Mike the handyman");
        //registrationPage.enterEmail("foo@bar.baz");
        registrationPage.enterWebsite("http://foo-bar.baz");
        registrationPage.clickNextButton();
        registrationPage.shouldDisplayErrorMessage("Invalid email address.");
        //registrationPage.nextButtonShouldBeDisabled();
    }


    @Test
    public void changeText_sameActivity() {
        registrationPage.enterBusinessName("Mike the handyman");
        registrationPage.enterEmail("foo@bar.baz");
        registrationPage.enterWebsite("http://foo-bar.baz");

        registrationPage.clickNextButton();



        registrationPage.licenseNumberIsInvisible();
        registrationPage.toggleHasLicense();
        registrationPage.licenseNumberIsVisible();

        registrationPage.enterLicenseNumber("1234567");
        registrationPage.enterBusinessDescription("This is a business description");

        registrationPage.clickNextButton();

    }

    @Test
    public void getProfessionalProfile() {


    }


}
