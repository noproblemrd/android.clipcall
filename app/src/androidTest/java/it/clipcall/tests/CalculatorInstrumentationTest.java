package it.clipcall.tests;


import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import it.clipcall.ClipCallApplication;
import it.clipcall.professional.remoteservicecall.services.IProfessionalVideoChatService;
import it.clipcall.professional.validation.services.AuthenticationManager;
import it.clipcall.common.invitations.models.eInvitationType;
import it.clipcall.consumer.payment.activities.PayTheProActivity;
import it.clipcall.consumer.payment.activities.PayTheProActivity_;
import it.clipcall.consumer.projects.models.HistoryItem;
import it.clipcall.consumer.projects.services.ICustomerProjectsService;
import it.clipcall.consumer.vidoechat.models.CustomerVideoInvitationData;
import it.clipcall.consumer.vidoechat.services.VideoChatManager;
import it.clipcall.infrastructure.di.components.ApplicationComponent;
import it.clipcall.infrastructure.requests.VideoInvitationRequest;
import it.clipcall.infrastructure.routing.models.RoutingContext;
import retrofit2.Response;

@RunWith(AndroidJUnit4.class)
public class CalculatorInstrumentationTest
        extends ActivityInstrumentationTestCase2<PayTheProActivity_> {


    private ApplicationComponent compositionRootComponent;

    private VideoChatManager sut;


    @Inject
    VideoChatManager videoChatManager;

    @Inject
    ICustomerProjectsService projectsService;

    @Inject
    IProfessionalVideoChatService professionalVideoChatService;

    @Inject
    AuthenticationManager authenticationManager;

    private PayTheProActivity activity;

    public CalculatorInstrumentationTest() {
        super(PayTheProActivity_.class);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();

        // Injecting the Instrumentation instance is required
        // for your test to run with AndroidJUnitRunner.
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        activity = getActivity();

        ClipCallApplication application = (ClipCallApplication)activity.getApplication();

        compositionRootComponent = application.getComponent();

        sut = compositionRootComponent.getVideoChatManager();
        projectsService = compositionRootComponent.getCustomerProjectsService();
        professionalVideoChatService = compositionRootComponent.getProfessionalVideoChatService();
        authenticationManager = compositionRootComponent.getAuthenticationManager();

    }

    @Test
    public void typeOperandsAndPerformAddOperation() {
        sut.uploadVideoPreviewImage(null,null);
        RoutingContext routingContext = activity.getRoutingContext();
    }

    @Test
    public void fooTest() throws IOException {
        String customerId = "91c3924f-50aa-4acf-9fc1-28148e883c4e";
        String leadAccountId = "5c0ccdd4-51d4-4c9e-915c-258ad4173a6d";
        String advertiserId = "5bfa4134-8cc5-48a4-8038-a29a0ebf53ba";


        Response<List<HistoryItem>> items = projectsService.getAdvertiserHistoryItems(customerId, leadAccountId, advertiserId).execute();
        RoutingContext routingContext = activity.getRoutingContext();
    }


    @Test
    public void createInvitationTest() throws IOException {
        String advertiserId = authenticationManager.getSupplierId();
        VideoInvitationRequest request = new VideoInvitationRequest();
        request.setInvitationType(eInvitationType.VideoChat);
        Response<CustomerVideoInvitationData> response = professionalVideoChatService.createVideoInvitationData(advertiserId,request ).execute();
        CustomerVideoInvitationData data = response.body();
        Assert.assertNotNull("invitation data should not be null", data);
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
}